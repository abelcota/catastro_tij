<p>Alguien solicitó un restablecimiento de contraseña en esta dirección de correo electrónico para <?= site_url() ?>.</p>

<p>Para restablecer la contraseña use este código en la URL <a href="<?= site_url() . 'auth/reset-password'; ?>"><?= site_url() . 'auth/reset-password'; ?></a> y siga las instrucciones.</p>

<p>Tu codigo: <b><?= $hash ?></b></p>

<p>Visita el siguiente enlace: <a href="<?= site_url() . 'auth/reset-password?token=' . $hash ?>"> para continuar.

<br>

<p>Si no solicitó un restablecimiento de contraseña, puede ignorar este correo electrónico con seguridad.</p>