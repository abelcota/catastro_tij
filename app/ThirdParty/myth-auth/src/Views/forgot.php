<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <title> Recuperar Contraseña | Sistema Integral de Catastro Culiacán</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Sistema de Administracion Catastral" name="description" />
    <meta content="Direccion de Catastro Culiacan" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url("assets/images/favicon.ico");?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Georama:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- Bootstrap Css -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?php echo base_url("assets/css/icons.min.css");?>" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?php echo base_url("assets/css/app.min.css");?>" id="app-style" rel="stylesheet" type="text/css" />
    <style>
         body {
            background: linear-gradient(rgba(0, 0, 0, 0.8),
                            rgba(0, 0, 0, 0.5)),url(<?php echo base_url("assets/images/fondo2.png");?>);
            background-attachment: fixed;
            background-size: cover;
            padding: 1em 0;
      
        }
    </style>
</head>

<body>
    <div class="account-pages my-2 pt-sm-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-6">
                    <div class="card overflow-hidden">
                        <div class="bg-login text-center">
                             
                            <div class="bg-login-overlay"></div>
                            <div class="position-relative">
                               <img src="<?php echo base_url("assets/images/logo_hw.png");?>" alt="" height="100">
                               <h5 class="text-white font-size-20">Sistema de Administración Catastral</h5>
                                <h5 class="text-white font-size-16">Dirección de Catastro Municipal Culiacán</h5>
                                <p class="text-white mb-0"><?=lang('Auth.forgotPassword')?></p>
                                <a href="#" class="logo logo-admin mt-4">
                                    <img src="<?php echo base_url("assets/images/users/user.jpg");?>" alt="" height="40">
                                </a>
                            </div>
                        </div>
                        <div class="card-body pt-5">
                        <?= view('Myth\Auth\Views\_message_block') ?>
                        <div class="alert alert-light text-center mb-4" role="alert">
                    <p><?=lang('Auth.enterEmailForInstructions')?></p>
</div>
                    <form action="<?= route_to('forgot') ?>" method="post">
                        <?= csrf_field() ?>

                        <div class="form-group">
                            <label for="email"><?=lang('Auth.emailAddress')?></label>
                            <input type="email" class="form-control <?php if(session('errors.email')) : ?>is-invalid<?php endif ?>"
                                   name="email" aria-describedby="emailHelp" placeholder="<?=lang('Auth.email')?>">
                            <div class="invalid-feedback">
                                <?= session('errors.email') ?>
                            </div>
                        </div>

                        <br>

                        <button type="submit" class="btn btn-dark waves-effect btn-block"><i class="bx bx-mail-send font-size-16 align-middle mr-2"></i> <?=lang('Auth.sendInstructions')?></button>
                    </form>
                    <hr>
                    <p><?=lang('Auth.alreadyRegistered')?> <a href="<?= route_to('login') ?>"><?=lang('Auth.signIn')?></a></p>

                   </div>
</div>

</div>
</div>
</div>

</div>
</div>
<div class="mt-5 text-center text-white"> 
                        <p><script>document.write(new Date().getFullYear())</script> © Dirección de Catastro Municipal Culiacán.</p>
                    </div>
</div>
</div>
</div>
</div>

<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/libs/jquery/jquery.min.js");?>"></script>
<script src="<?php echo base_url("assets/libs/bootstrap/js/bootstrap.bundle.min.js");?>"></script>
<script src="<?php echo base_url("assets/libs/metismenu/metisMenu.min.js");?>"></script>
<script src="<?php echo base_url("assets/libs/simplebar/simplebar.min.js");?>"></script>
<script src="<?php echo base_url("assets/libs/node-waves/waves.min.js");?>"></script>

<script src="<?php echo base_url("assets/js/app.js");?>"></script>

</body>

</html>


