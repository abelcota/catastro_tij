<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <title> Recuperar Contraseña | Sistema Integral de Catastro Culiacán</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url("assets/images/favicon.ico");?>">

    <!-- Bootstrap Css -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?php echo base_url("assets/css/icons.min.css");?>" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?php echo base_url("assets/css/app.min.css");?>" id="app-style" rel="stylesheet" type="text/css" />
    <style>
        body {
            background: linear-gradient(rgba(0, 0, 0, 0.1),
                            rgba(72, 9, 18, 0.3)),url(<?php echo base_url("assets/images/fondo2.png");?>);
            background-attachment: fixed;
            background-size: cover;
            padding: 1em 0;
      
        }
    </style>
</head>

<body>
    <div class="account-pages my-2 pt-sm-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-6">
                    <div class="card overflow-hidden">
                        <div class="bg-login text-center">
                             
                            <div class="bg-login-overlay"></div>
                            <div class="position-relative">
                               <img src="<?php echo base_url("assets/images/logo_hw.png");?>" alt="" height="100">
                               <h5 class="text-white font-size-20">Sistema de Administración Catastral</h5>
                                <h5 class="text-white font-size-16">Dirección de Catastro Municipal Culiacán</h5>
                                <p class="text-white mb-0"><?=lang('Auth.resetYourPassword')?></p>
                                <a href="#" class="logo logo-admin mt-4">
                                    <img src="<?php echo base_url("assets/images/users/user.jpg");?>" alt="" height="40">
                                </a>
                            </div>
                        </div>
                        <div class="card-body pt-5">
                        <?= view('Myth\Auth\Views\_message_block') ?>
                        <div class="alert alert-light text-center mb-4" role="alert">
                    <p><?=lang('Auth.enterCodeEmailPassword')?></p></div>

                    <form action="<?= route_to('reset-password') ?>" method="post">
                    <?= csrf_field() ?>

                        <div class="form-group">
                            <label for="token"><?=lang('Auth.token')?></label>
                            <input type="text" class="form-control <?php if(session('errors.token')) : ?>is-invalid<?php endif ?>"
                                   name="token" placeholder="<?=lang('Auth.token')?>" value="<?= old('token', $token ?? '') ?>">
                            <div class="invalid-feedback">
                                <?= session('errors.token') ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email"><?=lang('Auth.email')?></label>
                            <input type="email" class="form-control <?php if(session('errors.email')) : ?>is-invalid<?php endif ?>"
                                   name="email" aria-describedby="emailHelp" placeholder="<?=lang('Auth.email')?>" value="<?= old('email') ?>">
                            <div class="invalid-feedback">
                                <?= session('errors.email') ?>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="password"><?=lang('Auth.newPassword')?></label>
                            <input type="password" class="form-control <?php if(session('errors.password')) : ?>is-invalid<?php endif ?>"
                                   name="password">
                            <div class="invalid-feedback">
                                <?= session('errors.password') ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pass_confirm"><?=lang('Auth.newPasswordRepeat')?></label>
                            <input type="password" class="form-control <?php if(session('errors.pass_confirm')) : ?>is-invalid<?php endif ?>"
                                   name="pass_confirm">
                            <div class="invalid-feedback">
                                <?= session('errors.pass_confirm') ?>
                            </div>
                        </div>

                        <br>

                        <button type="submit" class="btn btn-dark waves-effect btn-block"><i class="bx bx-reset font-size-16 align-middle mr-2"></i> <?=lang('Auth.resetPassword')?></button>
                        </form>
                        <hr>
                    <p>Regresar a <a href="<?= route_to('login') ?>"><?=lang('Auth.signIn')?></a></p>

</div>
</div>

</div>
</div>
</div>

</div>
</div>
<div class="mt-5 text-center text-white"> 
                        <p><script>document.write(new Date().getFullYear())</script> © Dirección de Catastro Municipal Culiacán.</p>
                    </div>
</div>
</div>
</div>
</div>

<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/libs/jquery/jquery.min.js");?>"></script>
<script src="<?php echo base_url("assets/libs/bootstrap/js/bootstrap.bundle.min.js");?>"></script>
<script src="<?php echo base_url("assets/libs/metismenu/metisMenu.min.js");?>"></script>
<script src="<?php echo base_url("assets/libs/simplebar/simplebar.min.js");?>"></script>
<script src="<?php echo base_url("assets/libs/node-waves/waves.min.js");?>"></script>

<script src="<?php echo base_url("assets/js/app.js");?>"></script>

</body>

</html>


