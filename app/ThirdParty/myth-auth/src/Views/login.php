<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <title> Ingresar | Sistema Integral de Catastro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Sistema de Administracion Catastral" name="description" />
    <meta content="Direccion de Catastro Culiacan" name="author" />
    <!-- App favicon -->
    
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Georama:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <!-- Bootstrap Css -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?php echo base_url("assets/css/icons.min.css");?>" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?php echo base_url("assets/css/app.min.css");?>" id="app-style" rel="stylesheet" type="text/css" />
    <style>
        body {
            background-color: #9A9EA9;
            background-attachment: fixed;
            background-size: cover;
            padding: 1em 0;
      
        }
    </style>
</head>

<body>
    <div class="account-pages my-2 pt-sm-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-6">
                    <div class="card overflow-hidden">
                        <div class="bg-login text-center">
                             
                            <div class="bg-login-overlay"></div>
                            <div class="position-relative">
                               <img src="<?php echo base_url("assets/images/aytoBco.png");?>" alt="" height="100">
                                <h5 class="text-white font-size-20">Sistema de Administración Catastral</h5>
                                <h5 class="text-white font-size-16">Dirección de Catastro</h5>
                                <p class="text-white mb-0">Ingresar</p>
                                <a href="#" class="logo logo-admin mt-4">
                                    <img src="<?php echo base_url("assets/images/users/user.jpg");?>" alt="" height="40">
                                </a>
                            </div>
                        </div>
                        <div class="card-body pt-5">



					<?= view('Myth\Auth\Views\_message_block') ?>

					<form action="<?= route_to('login') ?>" method="post">
						<?= csrf_field() ?>

<?php if ($config->validFields === ['email']): ?>
						<div class="form-group">
							<label for="login"><?=lang('Auth.email')?></label>
							<input type="email" class="form-control <?php if(session('errors.login')) : ?>is-invalid<?php endif ?>"
								   name="login" placeholder="<?=lang('Auth.email')?>">
							<div class="invalid-feedback">
								<?= session('errors.login') ?>
							</div>
						</div>
<?php else: ?>
						<div class="form-group">
							<label for="login"><?=lang('Auth.emailOrUsername')?></label>
							<input type="text" class="form-control <?php if(session('errors.login')) : ?>is-invalid<?php endif ?>"
								   name="login" placeholder="<?=lang('Auth.emailOrUsername')?>">
							<div class="invalid-feedback">
								<?= session('errors.login') ?>
							</div>
						</div>
<?php endif; ?>

						<div class="form-group">
							<label for="password"><?=lang('Auth.password')?></label>
							<input type="password" name="password" class="form-control  <?php if(session('errors.password')) : ?>is-invalid<?php endif ?>" placeholder="<?=lang('Auth.password')?>">
							<div class="invalid-feedback">
								<?= session('errors.password') ?>
							</div>
						</div>

<?php if ($config->allowRemembering): ?>
						<div class="form-check">
							<label class="form-check-label">
								<input type="checkbox" name="remember" class="form-check-input" <?php if(old('remember')) : ?> checked <?php endif ?>>
								<?=lang('Auth.rememberMe')?>
							</label>
						</div>
<?php endif; ?>

						<br>

						<button type="submit" class="btn btn-dark waves-effect btn-block"> <i class="bx bx-log-in font-size-16 align-middle mr-2"></i> <?=lang('Auth.loginAction')?></button>
					</form>

					<hr>

<?php if ($config->activeResetter): ?>
					<p><a href="<?= route_to('forgot') ?>"><?=lang('Auth.forgotYourPassword')?></a></p>
<?php endif; ?>
				</div>
			</div>

		</div>
	</div>
</div>

</div>
                    </div>
                    <div class="mt-5 text-center text-white"> 
                        <p><script>document.write(new Date().getFullYear())</script> © Dirección de Catastro Municipal Culiacán.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- JAVASCRIPT -->
    <script src="<?php echo base_url("assets/libs/jquery/jquery.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/bootstrap/js/bootstrap.bundle.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/metismenu/metisMenu.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/simplebar/simplebar.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/node-waves/waves.min.js");?>"></script>

    <script src="<?php echo base_url("assets/js/app.js");?>"></script>

</body>

</html>

