
<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <title> Registro | Sistema Integral de Catastro Culiacán</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url("assets/images/favicon.ico");?>">

    <!-- Bootstrap Css -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?php echo base_url("assets/css/icons.min.css");?>" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?php echo base_url("assets/css/app.min.css");?>" id="app-style" rel="stylesheet" type="text/css" />
    <style>
        body {
            background: url(<?php echo base_url("assets/images/fondo2.png");?>), rgba(141,17,17,0.1);
            background-attachment: fixed;
            background-size: cover;
            padding: 1em 0;
      
        }
    </style>
</head>

<body>
    <div class="account-pages my-2 pt-sm-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card overflow-hidden">
                        <div class="bg-login text-center">
                             
                            <div class="bg-login-overlay"></div>
                            <div class="position-relative">
                               <img src="<?php echo base_url("assets/images/logo_hw.png");?>" alt="" height="100">
                                <h5 class="text-white font-size-20">Sistema Catastral Culiacan</h5>
                                <p class="text-white mb-0"><?=lang('Auth.register')?></p>
                                <a href="#" class="logo logo-admin mt-4">
                                    <img src="<?php echo base_url("assets/images/users/user.jpg");?>" alt="" height="40">
                                </a>
                            </div>
                        </div>
                        <div class="card-body pt-5">

                    <?= view('Myth\Auth\Views\_message_block') ?>

                    <form action="<?= route_to('registers') ?>" method="">
                        <?= csrf_field() ?>

                        <div class="form-group">
                            <label for="email"><?=lang('Auth.email')?></label>
                            <input type="email" class="form-control <?php if(session('errors.email')) : ?>is-invalid<?php endif ?>"
                                   name="email" aria-describedby="emailHelp" placeholder="<?=lang('Auth.email')?>" value="<?= old('email') ?>">
                            <small id="emailHelp" class="form-text text-muted"><?=lang('Auth.weNeverShare')?></small>
                        </div>

                        <div class="form-group">
                            <label for="username"><?=lang('Auth.username')?></label>
                            <input type="text" class="form-control <?php if(session('errors.username')) : ?>is-invalid<?php endif ?>" name="username" placeholder="<?=lang('Auth.username')?>" value="<?= old('username') ?>">
                        </div>

                        <div class="form-group">
                            <label for="password"><?=lang('Auth.password')?></label>
                            <input type="password" name="password" class="form-control <?php if(session('errors.password')) : ?>is-invalid<?php endif ?>" placeholder="<?=lang('Auth.password')?>" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="pass_confirm"><?=lang('Auth.repeatPassword')?></label>
                            <input type="password" name="pass_confirm" class="form-control <?php if(session('errors.pass_confirm')) : ?>is-invalid<?php endif ?>" placeholder="<?=lang('Auth.repeatPassword')?>" autocomplete="off">
                        </div>

                        <br>

                        <button type="submit" class="btn btn-primary btn-block"><?=lang('Auth.register')?></button>
                    </form>


                    <hr>

                    <p><?=lang('Auth.alreadyRegistered')?> <a href="<?= route_to('login') ?>"><?=lang('Auth.signIn')?></a></p>
                    </div>
			</div>

		</div>
	</div>
</div>

</div>
                    </div>
                    <div class="mt-5 text-center"> 
                        <p><script>document.write(new Date().getFullYear())</script> © Catastro Culiacán.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- JAVASCRIPT -->
    <script src="<?php echo base_url("assets/libs/jquery/jquery.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/bootstrap/js/bootstrap.bundle.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/metismenu/metisMenu.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/simplebar/simplebar.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/node-waves/waves.min.js");?>"></script>

    <script src="<?php echo base_url("assets/js/app.js");?>"></script>

</body>

</html>

