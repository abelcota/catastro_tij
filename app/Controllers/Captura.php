<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\API\ResponseTrait;
use App\Models\PadronModel;
use ZipArchive;

class Captura extends Controller
{
    use \Myth\Auth\AuthTrait;
    use ResponseTrait;

    public function __construct()
	{
        // Reestringir el acceso a los usuarios que no esten en el rol de Administracion
        $this->restrictToGroups(['Administracion', 'Captura']);
        $db = db_connect();
		$this->padronModel = new PadronModel($db);
    }

    public function index()
    {    
        echo view('header', ['titulo' => 'Bitacora de Tramites para Captura']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('captura/listado_captura');
        echo view('footer');
        echo view('captura/listado_capturajs');
    }

    public function cdi()
    {    
        echo view('header', ['titulo' => 'Bitacora de Cambios de Inscripcion para Captura']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('captura/listado_captura_cdi');
        echo view('footer');
        echo view('captura/listado_captura_cdijs');
    }

    public function get_listado_captura(){
        $data = $this->padronModel->listar_barracaptura(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function listar_barra_captura_cdi(){
        $data = $this->padronModel->listar_barra_captura_cdi(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    

    public function capturar($a = null, $fol = null, $cve = null){
        echo view('header', ['titulo' => 'Captura de Tramites']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('captura/capturar', ['year' => $a, 'folio' => $fol, 'cve' => $cve]);
        echo view('footer');
        echo view('captura/capturarjs');
   
    }

    public function capturarcdi($a = null, $fol = null, $cve = null){
        echo view('header', ['titulo' => 'Captura de Cambio de Inscripción']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('captura/capturar_cdi', ['year' => $a, 'folio' => $fol, 'cve' => $cve]);
        echo view('footer');
        echo view('captura/capturar_cdijs');
   
    }

    public function actualiza_generales(){
        helper(['form', 'url']);
        $data = [
            'campo' => $this->request->getVar('campo'),
            'valor' => $this->request->getVar('valor'),
            'clave' => $this->request->getVar('clave'),
        ];

        $save = $this->padronModel->actualiza_generales($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function eliminar_construccion(){
        helper(['form', 'url']);
        $data = [
            'clave' => $this->request->getVar('clave'),
            'tipo' =>  $this->request->getVar('tipo'),
            'numero' => $this->request->getVar('numero'),
        ];

        $save = $this->padronModel->eliminar_construccion($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function eliminar_terreno(){
        helper(['form', 'url']);
        $data = [
            'clave' => $this->request->getVar('clave'),
            'tipo' =>  $this->request->getVar('tipo'),
            'numero' => $this->request->getVar('numero'),
        ];

        $save = $this->padronModel->eliminar_terreno($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function establecer_propietario(){
        helper(['form', 'url']);
        $data = [
            'CVE_PROP'  => $this->request->getVar('CVE_PROP'),
            'TIP_PERS'  => strtoupper($this->request->getVar('TIP_PERS')),
            'APE_PAT'   => strtoupper($this->request->getVar('APE_PAT')),
            'APE_MAT'   => strtoupper($this->request->getVar('APE_MAT')),
            'NOM_PROP'  => strtoupper($this->request->getVar('NOM_PROP')),
            'REG_FED'   => strtoupper($this->request->getVar('REG_FED')),
            'CLAVE'     => $this->request->getVar('clave'),
        ];

        $save = $this->padronModel->establecer_propietario($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    
    public function asignar_propietario(){
        helper(['form', 'url']);
        $data = [
            'CVE_PROP'  => $this->request->getVar('idprop'),
            'CLAVE'     => $this->request->getVar('cvecat'),
        ];

        $save = $this->padronModel->asignar_propietario($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function registro_propietario(){
        helper(['form', 'url']);
        $tipo_pers = explode(" ", $this->request->getVar('prop_tip_persona') );

        $data = [
            'CVE_PROP'  => $this->request->getVar('prop_clave'),
            'TIP_PERS'  => strtoupper($tipo_pers[0]),
            'APE_PAT'   => strtoupper($this->request->getVar('prop_apellido_paterno')),
            'APE_MAT'   => strtoupper($this->request->getVar('prop_apellido_materno')),
            'NOM_PROP'  => strtoupper($this->request->getVar('prop_nombre')),
            'REG_FED'   => strtoupper($this->request->getVar('prop_rfc')),
            'NUM_PROP'  => $this->request->getVar('prop_numero'),
            'CLAVE'     => $this->request->getVar('prop_cvecat'),
        ];

        $save = $this->padronModel->registrar_propietario($data);

        echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }
	
	public function registrar_propietario_cdi(){
        helper(['form', 'url']);
        $tipo_pers = explode(" ", $this->request->getVar('prop_tip_persona') );

        $data = [
            'CVE_PROP'  => $this->request->getVar('prop_clave'),
            'TIP_PERS'  => strtoupper($tipo_pers[0]),
            'APE_PAT'   => strtoupper($this->request->getVar('prop_apellido_paterno')),
            'APE_MAT'   => strtoupper($this->request->getVar('prop_apellido_materno')),
            'NOM_PROP'  => strtoupper($this->request->getVar('prop_nombre')),
            'REG_FED'   => strtoupper($this->request->getVar('prop_rfc')),
            'NUM_PROP'  => $this->request->getVar('prop_numero')
			];

        $save = $this->padronModel->registrar_propietario_cdi($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function actualizar_propietario(){
        helper(['form', 'url']);
        $tipo_pers = explode(" ", $this->request->getVar('prop_tip_persona') );

        $data = [
            'CVE_PROP'  => $this->request->getVar('prop_clave'),
            'TIP_PERS'  => strtoupper($tipo_pers[0]),
            'APE_PAT'   => strtoupper($this->request->getVar('prop_apellido_paterno')),
            'APE_MAT'   => strtoupper($this->request->getVar('prop_apellido_materno')),
            'NOM_PROP'  => strtoupper($this->request->getVar('prop_nombre')),
            'REG_FED'   => strtoupper($this->request->getVar('prop_rfc')),
            'NUM_PROP'  => $this->request->getVar('prop_numero'),
            'CLAVE'     => $this->request->getVar('prop_cvecat'),
        ];

        $save = $this->padronModel->actualizar_propietario($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function eliminar_propietario(){
        helper(['form', 'url']);
        $data = [
            'clave' => $this->request->getVar('clave'),
            'cve_prop' => $this->request->getVar('cve_prop'),
            'numero' => $this->request->getVar('numero'),
        ];

        $save = $this->padronModel->eliminar_propietario($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function actualizar_servicios(){
        helper(['form', 'url']);

        $s1 = explode(" ", $this->request->getVar('serv1') )[0];
        if($s1 != '0')
            $s1 = substr($s1,1,1);
        
        $s2 = explode(" ", $this->request->getVar('serv2') )[0];
        if($s2 != '0')
            $s2 = substr($s2,1,1);

        $s3 = explode(" ", $this->request->getVar('serv3') )[0];
        if($s3 != '0')
            $s3 = substr($s3,1,1);
        
        $s4 = explode(" ", $this->request->getVar('serv4') )[0];
        if($s4 != '0')
            $s4 = substr($s4,1,1);

        $s5 = explode(" ", $this->request->getVar('serv5') )[0];
        if($s5 != '0')
            $s5 = substr($s5,1,1);
        
        $s6 = explode(" ", $this->request->getVar('serv6') )[0];
        if($s6 != '0')
            $s6 = substr($s6,1,1);  
            
        $s7 = explode(" ", $this->request->getVar('serv7') )[0];
        if($s7 != '0')
            $s7 = substr($s7,1,1);
    
        $s8 = explode(" ", $this->request->getVar('serv8') )[0];
        if($s8 != '0')
            $s8 = substr($s8,1,1);

        $s9 = explode(" ", $this->request->getVar('serv9') )[0];
        if($s9 != '0')
            $s9 = substr($s9,1,1);

        $data = [
            'SERV1' =>  $s1,
            'SERV2' =>  $s2,
            'SERV3' =>  $s3,
            'SERV4' =>  $s4,
            'SERV5' =>  $s5,
            'SERV6' =>  $s6,
            'SERV7' =>  $s7,
            'SERV8' =>  $s8,
            'SERV9' =>  $s9,
            'clave' => $this->request->getVar('serv_cvecat'),
        ];

        $save = $this->padronModel->actualizar_servicios($data);
        //echo $save;
        
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
        
    }

    public function actualizar_sup_val_construcciones(){
        helper(['form', 'url']);
        $data = [
            'SUP_CONST' => $this->request->getVar('SUP_CONST') ,
            'VAL_CONST' =>  $this->request->getVar('VAL_CONST') ,
            'clave' => $this->request->getVar('clave'),
        ];

        $save = $this->padronModel->actualizar_sup_val_construcciones($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }
    

    public function actualizar_sup_val_terreno(){
        helper(['form', 'url']);
        $data = [
            'SUP_TERR' => $this->request->getVar('SUP_TERR') ,
            'VAL_TERR' =>  $this->request->getVar('VAL_TERR') ,
            'clave' => $this->request->getVar('clave'),
        ];

        $save = $this->padronModel->actualizar_sup_val_terreno($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    

    public function actualizar_construccion(){
        helper(['form', 'url']);
        $cve_categoria = explode(" ", $this->request->getVar('cat_const') );
        $cve_edo = explode(" ", $this->request->getVar('edo_const') );
        $data = [
            'NUM_CONST_ORI' => intval( $this->request->getVar('num_const_ori') ),
            'NUM_CONST' => intval( $this->request->getVar('num_const') ),
            'SUP_CONST' => $this->request->getVar('terr_sup_const'),
            'CVE_CAT_CO' => $cve_categoria[0],
            'CVE_EDO_CO' => $cve_edo[0],
            'EDD_CONST' => $this->request->getVar('edad_const'),
            'FEC_CONST' => $this->request->getVar('fecha_const'),
            'clave' => $this->request->getVar('const_cvecat'),
        ];

        $save = $this->padronModel->actualizar_construccion($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function finalizar_captura_cdi(){
        helper(['form', 'url']);
        $data = [
            'ANIO' => $this->request->getVar('anio'),
            'FOLIO' => $this->request->getVar('fol'),
            'CAPTURISTA' => $this->request->getVar('capturista'),
            'CVE_CAT' => $this->request->getVar('cve_cat'),
            'TRAMITE' => $this->request->getVar('tramite'),
            'VALOR_A' => $this->request->getVar('valor_a'),
            'VALOR_C' => $this->request->getVar('valor_c'),
        ];

        $save = $this->padronModel->finalizar_captura_cdi($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function finalizar_captura(){
        helper(['form', 'url']);
        $data = [
            'ANIO' => $this->request->getVar('anio'),
            'FOLIO' => $this->request->getVar('fol'),
            'CAPTURISTA' => $this->request->getVar('capturista'),
            'CVE_CAT' => $this->request->getVar('cve_cat'),
            'TRAMITE' => $this->request->getVar('tramite'),
            'VALOR_A' => $this->request->getVar('valor_a'),
            'VALOR_C' => $this->request->getVar('valor_c'),
        ];

        $save = $this->padronModel->finalizar_captura($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function cancelar_clave(){
        helper(['form', 'url']);
        $data = [
            'clave_m' => $this->request->getVar('clave_m'),
            'clave_c' => $this->request->getVar('clave_c'),
        ];

        $save = $this->padronModel->cancelar_clave($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function registrar_nueva_clave_catastral(){
        helper(['form', 'url']);
        $data = [
            'POBL' => $this->request->getVar('POBL'),
            'CTEL' => $this->request->getVar('CTEL'),
            'MANZ' => $this->request->getVar('MANZ'),
            'PRED' => $this->request->getVar('PRED'),
            'UNID' => $this->request->getVar('UNID'),
            'a'    => $this->request->getVar('a'),
            'fol'  => $this->request->getVar('fol'),
            'cvemaster'  => $this->request->getVar('cvemaster'),
            'verificador'  => $this->request->getVar('verificador'),
            'tramite'  => $this->request->getVar('tramite'),
        ];

        $save = $this->padronModel->registrar_nueva_clave_catastral($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function registro_construccion(){
        helper(['form', 'url']);
        $cve_categoria = explode(" ", $this->request->getVar('cat_const') );
        $cve_edo = explode(" ", $this->request->getVar('edo_const') );
        $data = [
            'NUM_CONST' => intval( $this->request->getVar('num_const') ),
            'SUP_CONST' => $this->request->getVar('terr_sup_const'),
            'CVE_CAT_CO' => $cve_categoria[0],
            'CVE_EDO_CO' => $cve_edo[0],
            'EDD_CONST' => $this->request->getVar('edad_const'),
            'FEC_CONST' => $this->request->getVar('fecha_const'),
            'clave' => $this->request->getVar('const_cvecat'),
        ];

        $save = $this->padronModel->registrar_construccion($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function cargar_vectoriales($fol = null){
        
        
        $dir = ROOTPATH.'public/documentos/'.$fol.'/vectorial';
        $results['result'] = array();

        $files = scandir($dir);
        foreach($files as $key => $value){
            $path = $dir.DIRECTORY_SEPARATOR.$value;
            if(!is_dir($path)) {
                if ($value != ".DS_Store")
                    array_push($results['result'], ['nombre'=> $value,'size'=>filesize($path), 'ext'=>pathinfo($path, PATHINFO_EXTENSION)]);          
            } else if($value != "." && $value != ".." ) {
                getDirContents($path, $results);
                $results['result'] = ['path'=>$path,'size'=>filesize($path), 'ext'=>pathinfo($path, PATHINFO_EXTENSION)];
            }
        }
          
        return $this->respond($results);
    }



    public function borrar_vectorial(){
        helper(['form', 'url']);
        $cvecat = $this->request->getVar('clave');
        $folio = $this->request->getVar('folio');
        $directorio = ROOTPATH.'public/documentos/'.$folio.'/vectorial/';
        $borrar = glob($directorio.'*');
        if(file_exists($directorio)){
            $files = $borrar; //obtenemos todos los nombres de los ficheros
            foreach($files as $file){
                if(is_file($file))
                    unlink($file); //elimino el fichero
            }
            echo "Archivos eliminados";
        }
        else{
            echo "No hay archivos para eliminar";
        }
        


    }

    public function descarga_vectorial(){
        helper(['form', 'url']);
        $cvecat = $this->request->getVar('clave');
        $folio = $this->request->getVar('folio');

        $directorio_tramite = ROOTPATH.'public/documentos/'.$folio;
        $directorio = ROOTPATH.'public/documentos/'.$folio.'/vectorial';
        
        if(!file_exists($directorio_tramite)){
            mkdir($directorio_tramite, 0775) or die("No se puede crear el directorio de extracci&oacute;n");	
        }

        if(!file_exists($directorio)){
            mkdir($directorio, 0775) or die("No se puede crear el directorio de extracci&oacute;n");	
        }


        
        $comando = '"C:\Program Files\PostgreSQL\10\bin\pgsql2shp.exe" ' . '-f "'.$directorio.'/'.$folio."_".date('YmdHis') .'" -h localhost -u postgres -P postgres catastrocln "SELECT * FROM "Culiacan_Join" WHERE dg_cat = \''.$cvecat.'\'" ';

        $salida = shell_exec($comando);
        
        //hacer el zip
    
        $zip = new ZipArchive(); 
        $nombre = '/'.$folio.'_'.date('YmdHis') .'.zip';
        $zip->open($directorio.$nombre, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        //$zip->addEmptyDir('.'); 
        $dir = opendir($directorio);
        while($file = readdir($dir)) {
            if(is_file($directorio."/".$file)) {
                  $zip -> addFile($directorio."/".$file, $file);
            }    
        }
        $zip ->close();
        
        echo $salida;

        $directorio = ROOTPATH.'public/documentos/'.$folio.'/vectorial/';
        $mantener = glob($directorio."*.zip");
        $borrar = glob($directorio.'*');
        if(file_exists($directorio)){
            $files = array_diff($borrar, $mantener); //obtenemos todos los nombres de los ficheros
            foreach($files as $file){
                if(is_file($file))
                    unlink($file); //elimino el fichero
            }
            //echo "Archivos eliminados";
        }


    }

    public function registro_usos(){
        helper(['form', 'url']);
        $cvecat = str_split($this->request->getVar('uso_cvecat'), 3);
        $pobl = intval ( $cvecat[0] );
        $ctel = intval ( $cvecat[1] );
        $manz = intval ( $cvecat[2] );
        $pred = intval ( $cvecat[3] );
        $unid = intval ( $cvecat[4] );
        $gpo_uso = explode(" ", $this->request->getVar('grupo_uso') );
        $cve_uso = explode(" ", $this->request->getVar('uso_uso') );

        $data = [
            'CVE_POBL' => $pobl,
            'NUM_CTEL' => $ctel,
            'NUM_MANZ' => $manz,
            'NUM_PRED' => $pred,
            'NUM_UNID' => $unid,
            'NUM_USO' => $this->request->getVar('num_uso'),
            'CVE_GPO_USO' => $gpo_uso[0],
            'CVE_USO' =>  $cve_uso[0] ,
            'CLAVE' => $this->request->getVar('uso_cvecat'),
        ];
        
        $save = $this->padronModel->registrar_uso($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function eliminar_uso(){
        helper(['form', 'url']);
        $data = [
            'clave' => $this->request->getVar('clave'),
            'numero' => $this->request->getVar('numero'),
        ];

        $save = $this->padronModel->eliminar_uso($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function actualizar_terreno(){
        helper(['form', 'url']);
        
        $cve_calle = explode(" ", $this->request->getVar('terr_calle') );
        $cve_tramo = explode(" ", $this->request->getVar('terr_tramo') );

        $cve_calle_e = explode(" ", $this->request->getVar('terr_calle_e') );
        $cve_tramo_e = explode(" ", $this->request->getVar('terr_tramo_e') );

        $dem_t1 = explode(" ", $this->request->getVar('dem_terr') );
        $dem_t2 = explode(" ", $this->request->getVar('dem_terr2') );
        $dem_t3 = explode(" ", $this->request->getVar('dem_terr3') );

        $data = [
            'CVE_POBL' => intval( $this->request->getVar('terr_poblacion') ),
            'NUM_TERR' => $this->request->getVar('terr_numero'),
            'NUM_TERR_ORI' => $this->request->getVar('terr_numero_ori'),
            'TIP_TERR' => $this->request->getVar('terr_tip_terr'),
            'SUP_TERR' => $this->request->getVar('terr_sup_terr'),
            'CVE_CALLE' => $cve_calle[0],
            'CVE_TRAMO' => $cve_tramo[0],
            'SUP_TERR_E' => $this->request->getVar('terr_sup_terr_e'),
            'CVE_CALLE_' => $cve_calle_e[0],
            'CVE_TRAMO_' => $cve_tramo_e[0],
            'SUP_TERR_D' => $this->request->getVar('terr_sup_terr_d'),
            'CVE_DEM_TE' => $dem_t1[0],
            'CVE_DEM_T2' => $dem_t2[0],
            'CVE_DEM_T3' => $dem_t3[0],
            'FAC_DEM_TE' => $this->request->getVar('terr_fact_dem'),
            'clave' => $this->request->getVar('terr_cvecat'),
        ];

        $save = $this->padronModel->actualizar_terreno($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }


    public function registro_terreno(){
        helper(['form', 'url']);
        
        $cve_calle = explode(" ", $this->request->getVar('terr_calle') );
        $cve_tramo = explode(" ", $this->request->getVar('terr_tramo') );

        $cve_calle_e = explode(" ", $this->request->getVar('terr_calle_e') );
        $cve_tramo_e = explode(" ", $this->request->getVar('terr_tramo_e') );

        $dem_t1 = explode(" ", $this->request->getVar('dem_terr') );
        $dem_t2 = explode(" ", $this->request->getVar('dem_terr2') );
        $dem_t3 = explode(" ", $this->request->getVar('dem_terr3') );

        $data = [
            'CVE_POBL' => intval( $this->request->getVar('terr_poblacion') ),
            'NUM_TERR' => $this->request->getVar('terr_numero'),
            'TIP_TERR' => $this->request->getVar('terr_tip_terr'),
            'SUP_TERR' => $this->request->getVar('terr_sup_terr'),
            'CVE_CALLE' => $cve_calle[0],
            'CVE_TRAMO' => $cve_tramo[0],
            'SUP_TERR_E' => $this->request->getVar('terr_sup_terr_e'),
            'CVE_CALLE_' => $cve_calle_e[0],
            'CVE_TRAMO_' => $cve_tramo_e[0],
            'SUP_TERR_D' => $this->request->getVar('terr_sup_terr_d'),
            'CVE_DEM_TE' => $dem_t1[0],
            'CVE_DEM_T2' => $dem_t2[0],
            'CVE_DEM_T3' => $dem_t3[0],
            'FAC_DEM_TE' => $this->request->getVar('terr_fact_dem'),
            'clave' => $this->request->getVar('terr_cvecat'),
        ];

        $save = $this->padronModel->registrar_terreno($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

}