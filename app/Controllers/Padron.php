<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\PadronModel;
use CodeIgniter\API\ResponseTrait;
use \Hermawan\DataTables\DataTable;
use \PhpCfdi\Credentials\Credential;
use Myth\Auth\Entities\User;
use Myth\Auth\Models\UserModel;

class Padron extends Controller
{
    use ResponseTrait;

    public function __construct() {
        //establece la memoria limite del servidor en maximo
        ini_set("memory_limit", "-1");
		$db = db_connect();
		$this->padronModel = new PadronModel($db);
    }

    public function index()
    {
        echo "Padron";
    }

    public function list_poblaciones(){
        $data['data']	= $this->padronModel->list_poblaciones();
        return $this->respond($data);
    }

    public function list_cuarteles(){
        $data['data']	= $this->padronModel->list_cuarteles();
        return $this->respond($data);
    }

    public function list_manzanas($ctel = ""){
        $data['data']	= $this->padronModel->list_manzanas($ctel);
        return $this->respond($data);
    }

    public function list_predios($ctel = "", $manz = ""){
        $data['data']	= $this->padronModel->list_predios($ctel, intval($manz));
        return $this->respond($data);
    }

    public function list_unidades($pobl = "", $ctel = "", $manz = "", $pred = ""){
        $data['data']	= $this->padronModel->list_unidades(intval($pobl), intval($ctel), intval($manz), intval($pred));
        return $this->respond($data);
    }

    public function calles()
    {    
        $data['data']	= $this->padronModel->listar_calles();
        return $this->respond($data);
    }

    public function listar_notarios()
    {    
        $data['data']	= $this->padronModel->listar_notarios();
        return $this->respond($data);
    }

    public function calles2($pobl = null)
    {    
        $data['results'] = $this->padronModel->listar_calles2($pobl);
        return $this->respond($data);
    }

    public function tramos2($pobl = null, $calle = null)
    {    
        $data['results'] = $this->padronModel->listar_tramos2($pobl, $calle);
        return $this->respond($data);
    }

    public function demeritos_terreno()
    {    
        $data['results'] = $this->padronModel->listar_demeritos_terreno();
        return $this->respond($data);
    }

    public function categorias_construccion()
    {    
        $data['results'] = $this->padronModel->listar_categorias_construccion();
        return $this->respond($data);
    }

    public function edo_construccion()
    {    
        $data['results'] = $this->padronModel->listar_edo_construccion();
        return $this->respond($data);
    }

    public function edad_construccion()
    {    
        $data['results'] = $this->padronModel->listar_edad_construccion();
        return $this->respond($data);
    }

    public function grupo_usos(){
        $data['results'] = $this->padronModel->listar_grupo_usos();
        return $this->respond($data);
    }

    public function usos($grupo = null){
        $data['results'] = $this->padronModel->listar_usos($grupo);
        return $this->respond($data);
    }

    public function poblaciones()
    {    
        $data['data']	= $this->padronModel->listar_poblaciones();
        return $this->respond($data);
    }

    public function catconstrucciones()
    {    
        $data['data']	= $this->padronModel->listar_cat_mp();
        return $this->respond($data);
    }

    public function tramos()
    {    
        
        $data['data']	= $this->padronModel->listar_tramos();
        return $this->respond($data);
    }

    public function terrenos(){
        
        $data = [
            'draw'          => $_REQUEST['draw'],
            'start'         => $_REQUEST['start'],
            'length'        => $_REQUEST['length'],
            'search_value'  => $_REQUEST['search']['value'],
            'table'         => 'a_terr',
            'searchcolumn'  => 'clave'
        ];      
          
        $data= $this->padronModel->datatable($data);
        return $this->respond($data);
    }    
    
    public function colonias($pobl = null)
    {    
        $data['result']	= $this->padronModel->listar_colonias($pobl);
        return $this->respond($data);
    }

    public function regimenes()
    {    
        $data['result']	= $this->padronModel->listar_regimenes();
        return $this->respond($data);
    }

    public function colonias2()
    {    
        $data['data']	= $this->padronModel->listar_colonias_all();
        return $this->respond($data);
    }

    public function conceptos()
    {    
        $data['result']	= $this->padronModel->listar_conceptos();
        return $this->respond($data);
    }

    public function tramites()
    {    
        $data['result']	= $this->padronModel->listar_tramites();
        return $this->respond($data);
    }

    public function construcciones()
    {    
        $data['result']	= $this->padronModel->listar_construcciones();
        return $this->respond($data);
    }

    public function construcciones2()
    {    
        $data = [
            'draw'          => $_REQUEST['draw'],
            'start'         => $_REQUEST['start'],
            'length'        => $_REQUEST['length'],
            'search_value'  => $_REQUEST['search']['value'],
            'table'         => 'a_const',
            'searchcolumn'  => 'clave'
        ];      

        $data= $this->padronModel->datatable($data);
        return $this->respond($data);
    }

    public function manzanas()
    {    
        $data['result']	= $this->padronModel->listar_manzanas();
        return $this->respond($data);
    }

    public function manzanas2()
    {    
        $data['data']	= $this->padronModel->listar_manzanas();
        return $this->respond($data);
    }

    public function zonas()
    {    
        $data['data']	= $this->padronModel->listar_zonas();
        return $this->respond($data);
    }

    public function propietarios($filtro = null, $cve = 0)
    {    
        $data['result']	= $this->padronModel->listar_propietarios($filtro, $cve);
        return $this->respond($data);
    }

    public function get_propietarios()
    {    
               
        $data['data']= $this->padronModel->listar_propietarios2();
        return $this->respond($data);
    }

    public function propietarios2()
    {    
        $data = [
            'draw'          => $_REQUEST['draw'],
            'start'         => $_REQUEST['start'],
            'length'        => $_REQUEST['length'],
            'search_value'  => $_REQUEST['search']['value'],
            'table'         => 'a_prop',
            'searchcolumn'  => '"NOM_PROP"'
        ];      
          
        $data= $this->padronModel->datatable($data);
        return $this->respond($data);
        /*$data['data']	= $this->padronModel->listar_propietarios_todos();
        return $this->respond($data);*/
    }
    
    public function consulta_servicios($clave = null)
    {    
        $data['data']	= $this->padronModel->consulta_servicios($clave);
        return $this->respond($data);
    } 

    public function genera_clave_propietario(){
        $data['result']	= $this->padronModel->genera_clave_propietario();
        return $this->respond($data);
    }

    public function propietarios_unidades($filtro = null)
    {    
        $data['result']	= $this->padronModel->listar_propietarios_unidades($filtro);
        return $this->respond($data);
    }

    public function validarcve($cve = null)
    {    
        $data["result"] = $this->padronModel->validar_clave($cve);
        
        return $this->respond($data);
    }


    

    public function validarcvecdi($cve = null)
    {    
        $data["result"] = $this->padronModel->validar_clavecdi($cve);
        return $this->respond($data);
    }

    public function validarcvecdimanz($cve = null)
    {    
        $data["result"] = $this->padronModel->validarcvecdimanz($cve);
        return $this->respond($data);
    }
    
    public function empleados($puesto=null)
    {
        $data["result"] = $this->padronModel->get_empleados($puesto);
        return $this->respond($data);
    }

    public function pdf(){
        // create new PDF document
        echo "crearemos el pdf";
    }

    public function demc()
    {    
        $data['data']	= $this->padronModel->listar_demc();
        return $this->respond($data);
    }

    public function demt()
    {    
        $data['data']	= $this->padronModel->listar_demt();
        return $this->respond($data);
    }

    public function usuarios()
    {    
        $data['data']	= $this->padronModel->listar_usuarios();
        return $this->respond($data);
    }

    public function roles()
    {    
        $data['data'] = $this->padronModel->listar_roles();
        return $this->respond($data);
    }

    public function get_listado_avaluos(){
        $data['data'] = $this->padronModel->listar_avaluos();
        return $this->respond($data);
    }

    public function get_user_rol()
    {    
        $data = $this->padronModel->get_user_rol();
        return $this->respond($data);
    }

    public function firmar(){
        helper(['form', 'url']);
        $certfile = $this->request->getFile('certfile');
        $pemKeyFile = $this->request->getFile('certkey');
        $passPhrase = $this->request->getVar('passkey');
        $mensaje = $this->request->getVar('mensaje');

        $fiel = Credential::openFiles($certfile, $pemKeyFile, $passPhrase);
        $certificado = $fiel->certificate();
        $signature = $fiel->sign($mensaje);
        $data['obj'] =(array)$certificado;
        $data['certificado'] = array(['rfc' => $certificado->rfc(), 'nombre' => $certificado->legalName(),
        'empresa' => $certificado->branchName(), 'serial' =>$certificado->serialNumber()->bytes(), 'sello' => base64_encode($signature)]);

        return $this->respond($data);

    }

    public function obtener_perfil($id = null){
        
        $data['perfil'] = $this->padronModel->consultar_usuario_perfil($id);
        return $this->respond($data);
    }

    public function registrar_perfil(){
        helper(['form', 'url']);
        $data = [
            'id' => $this->request->getVar('txtusuario'),
            'nombre' => $this->request->getVar('txtnombre'),
            'rfc' => $this->request->getVar('txtrfc'),
            'direccion' => $this->request->getVar('txtdireccion'),
            'telefono' => $this->request->getVar('txttelefono'),
            'celular' => $this->request->getVar('txtcelular'),
            'correo' => $this->request->getVar('txtcorreo')
        ];
        //return $this->respond($data);
        $save = $this->padronModel->registrar_usuario_perfil($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    /**
	 * Verifies the code with the email and saves the new password,
	 * if they all pass validation.
	 *
	 * @return mixed
	 */
	public function attemptReset()
	{
		$users = model(UserModel::class);
		$user = $users->where('id', $this->request->getPost('id'))
					  ->first();

		if (is_null($user))
		{
			echo "no se encontro el usuario";
		}

		$user->setPassword($this->request->getPost('password'));

		$user->reset_at 		= date('Y-m-d H:i:s');

		if($users->save($user))
		{
			echo "Contraseña actualizada correctamente";
		}
		else{
			echo "No se pudo actualizar la contraseña";
		}
	}


    
}