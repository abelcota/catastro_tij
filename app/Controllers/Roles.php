<?php namespace App\Controllers;
use App\Models\RolesModel;
use CodeIgniter\API\ResponseTrait;

class Roles extends BaseController
{
    use ResponseTrait;
    use \Myth\Auth\AuthTrait;

    public function __construct()
	{
        // Reestringir el acceso a los usuarios que no esten en el rol de Administracion
        $this->restrictToGroups('Administracion');

    }

	public function index()
	{
        //Devuelve el JSON con todos los roles registrados
        $model = new RolesModel();
        $data['data'] = $model->orderBy('id', 'DESC')->findAll();        
        return $this->respond($data);
    }
                
    //--------------------------------------------------------------------
    
    //funcion para registrar un nuevo rol en la base de datos
    public function registro()
    {
        helper(['form', 'url']);
        $model = new RolesModel();
        $data = [
            'name' => $this->request->getVar('nombre'),
            'description'  => $this->request->getVar('descripcion'),
            ];
        $save = $model->insert($data);
        if($save){
            echo 'Registro de Rol Correcto';
        }
        else{
            echo 'No se pudo Registrar el Rol';
        }
    }

    //funcion para actualizar un Rol existente en la base de datos
    public function editar()
    {  
        helper(['form', 'url']);  
        $model = new RolesModel();
        $id = $this->request->getVar('id');
        $data = [
             'name'  => $this->request->getVar('nombre'),
             'description'  => $this->request->getVar('descripcion'),
            ];
        $save = $model->update($id,$data);
        if($save){
            echo 'Rol Actualizado';
        }
        else{
            echo 'No se pudo Actualizar el Rol';
        }
     }
     
    //funcion para eliminar un rol de la base de datos
    public function eliminar()
    {
        helper(['form', 'url']);  
        $id = $this->request->getVar('id');
        $model = new RolesModel();
        $save = $model->where('id', $id)->delete();
        if($save){
            echo 'Rol Eliminado';
        }
        else{
            echo 'No se pudo Eliminar el Rol';
        }
    }

}
