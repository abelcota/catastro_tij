<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\API\ResponseTrait;
use App\Models\PadronModel;

class Verificacion extends Controller
{
    use \Myth\Auth\AuthTrait;
    use ResponseTrait;

    public function __construct()
	{
        // Reestringir el acceso a los usuarios que no esten en el rol de Administracion
        $this->restrictToGroups(['Administracion', 'Ventanilla', 'Seguimiento Ventanilla', 'Seguimiento', 'Inspeccion', 'Graficacion']);
        $db = db_connect();
		$this->padronModel = new PadronModel($db);

    }

    public function index()
    {    
        echo "Verficacion";
    }

    public function verificacion($a = null, $fol = null){
        echo view('header', ['titulo' => 'Verificación de Trámites']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('verificacion/verificacion_tramites', ['year' => $a, 'folio' => $fol]);
        echo view('footer');
        echo view('verificacion/mapjs'); 
        echo view('verificacion/verificacion_tramitesjs');
   
    }

    public function verificacioncdi($a = null, $fol = null){
        echo view('header', ['titulo' => 'Verificación de Cambios de Inscripción']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('verificacion/verificacion_cdi', ['year' => $a, 'folio' => $fol]);
        echo view('footer');
        echo view('verificacion/verificacion_cdijs');
   
    }

    public function avaluoscomerciales(){
        echo view('header', ['titulo' => 'Revisión y Verificación de Avaluos Comerciales']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('verificacion/avaluos_comerciales');
        echo view('footer');
        echo view('verificacion/avaluos_comercialesjs');
    }

    public function avaluocomercial($info = null, $tipo = null){
        echo view('header', ['titulo' => 'Revisión de Avaluo Comercial '.$info]);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('verificacion/avaluocomercial', ['info' => $info, 'tipo' => $tipo]);
        echo view('footer');
        echo view('verificacion/mapjs'); 
        echo view('verificacion/avaluocomercialjs');
   
    }


    

    public function consultafolio($a = null, $id= null){
        $data['data']	= $this->padronModel->get_folio_declaracion($a, $id);
        $data['status'] = $this->padronModel->get_status_folio($a, '000'.$id);
        return $this->respond($data);
    }

    public function consultafoliocdi($a = null, $id = null){
        $data['data']	= $this->padronModel->get_folio_cdi($a, $id);
        $data['status'] = $this->padronModel->get_status_folio_cdi($a, '000'.$id);
        return $this->respond($data);
    }

    public function genera_bitacora_graficacion(){
        $data['result'] = $this->padronModel->genera_bitacora_graficacion(); 
        return $this->respond($data);
    }

    public function genera_bitacora_inspeccion(){
        $data['result'] = $this->padronModel->genera_bitacora_inspeccion(); 
        return $this->respond($data);
    }

    public function get_empleados(){
        $data['result'] = $this->padronModel->get_empleados_v(); 
        return $this->respond($data);
    }

     //registrar el paquete de envio a inspeccion
     public function registrar_bitacora_inspeccion(){ 
        helper(['form', 'url']);
        $data = [
            'Bitacora' => $this->request->getVar('Bitacora'),
            'FechaEnv' => $this->request->getVar('FechaEnv'),
            'Envia' => $this->request->getVar('Envia'),
            'Recibe' => $this->request->getVar('Recibe'),
            'Folios' => $this->request->getVar('Folios'),
        ];

        $save = $this->padronModel->registrar_bitacora_inspeccion($data);
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    //registrar el paquete de envio a inspeccion
    public function registrar_bitacora_graficacion(){ 
        helper(['form', 'url']);
        $data = [
            'Bitacora' => $this->request->getVar('Bitacora'),
            'FechaEnv' => $this->request->getVar('FechaEnv'),
            'Origen' => $this->request->getVar('Origen'),
            'Envia' => $this->request->getVar('Envia'),
            'Recibe' => $this->request->getVar('Recibe'),
            'Folios' => $this->request->getVar('Folios'),
        ];

        $save = $this->padronModel->registrar_bitacora_graficacion($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function registrar_no_procede_cdi(){
        helper(['form', 'url']);

        $data = [
            'obs' => $this->request->getVar('Observaciones'),
            'id_folio' => $this->request->getVar('Folio'),
            'id_anio' => $this->request->getVar('Anio'),
            'verificador' => $this->request->getVar('Verificador'),
            'clave' => $this->request->getVar('Clave'),
        ];

        $save = $this->padronModel->guardar_seguimiento_cdi($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function registrar_no_procede(){
        helper(['form', 'url']);
        $QuienLoTiene = "CN";
        $EstadoActual = "NP";
        $Procedio = "N";

        $data = [
            'Id_QuienLoTiene' => $QuienLoTiene,
            'Id_EstadoActual' => $EstadoActual,
            'ProcedioSN' => $Procedio,
            'Observaciones' => $this->request->getVar('Observaciones'),
            'Id_FolioBarra' => $this->request->getVar('Folio'),
            'Id_Anio' => $this->request->getVar('Anio'),
        ];

        $save = $this->padronModel->guardar_seguimiento($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function enviar_a_captura_cdi(){
        helper(['form', 'url']);

        $data = [
            'id_anio' => $this->request->getVar('id_anio'),
            'id_folio' => $this->request->getVar('id_folio'),
            'cvecat' => $this->request->getVar('cvecat'),
            'verificador' => $this->request->getVar('verificador'),
        ];

        $save = $this->padronModel->enviar_tramite_captura_cdi($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function enviar_a_captura(){
        helper(['form', 'url']);

        $data = [
            'id_anio' => $this->request->getVar('id_anio'),
            'id_folio' => $this->request->getVar('id_folio'),
            'cvecat' => $this->request->getVar('cvecat'),
            'idtramite' => $this->request->getVar('idtramite'),
            'verificador' => $this->request->getVar('verificador'),
        ];

        $save = $this->padronModel->enviar_tramite_captura($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    

    public function consulta_avaluo2($cve = null, $a = null, $f = null){

        $data['unidad'] = $this->padronModel->consulta_avaluo_unidad($cve, $a.$f);
        $data['zona'] = $this->padronModel->consulta_avaluo_zona($cve, $a.$f);
        $data['terreno'] = $this->padronModel->consulta_avaluo_terrenos($cve, $a.$f);
        $data['construcciones'] = $this->padronModel->consulta_avaluo_construcciones($cve,$a.$f);
        $data['propietarios'] = $this->padronModel->consulta_avaluo_propietarios($cve,$a.$f);
        $data['tramite'] = $this->padronModel->get_folio_declaracion($a, $f);
        return $this->respond($data);

    }

    public function consulta_avaluo($cve = null, $a = null, $f = null){
        $cve_array = str_split($cve, 3);

        $data['result'] = $this->padronModel->validar_clave($cve);
        $data['tramite'] = $this->padronModel->get_folio_declaracion($a, $f);
        //$data['tramites'] = $this->padronModel->get_tramites_clave($cve);
        $data['zona'] = $this->padronModel->get_valor_zona($cve_array[0], $cve_array[1], $cve_array[2]);
        return $this->respond($data);
    }    

    public function consulta_cedulacatastral($cve = null){
        $data['unidad'] = $this->padronModel->validar_clave($cve);
        $data['terreno'] = $this->padronModel->get_terrenotj($cve);
        $data['construcciones'] = $this->padronModel->get_construccion($cve);
        $data['propietarios'] = $this->padronModel->get_propietariostj($cve);
        return $this->respond($data);
    }   
    

    public function consulta_servicios($cve = null){
        $data['data'] = $this->padronModel->get_servicios($cve);

        return $this->respond($data);
    }

    public function consulta_propietarios($cve = null){
        $data['data'] = $this->padronModel->get_propietarios($cve);

        return $this->respond($data);
    }

    public function consulta_usos($cve = null){
        $data['data'] = $this->padronModel->get_usos($cve);

        return $this->respond($data);
    }

    public function consulta_construcciones($cve = null){
        $data['data'] = $this->padronModel->get_construccion($cve);

        return $this->respond($data);
    }

    public function consulta_terreno($cve = null){
        $data['data'] = $this->padronModel->get_terreno($cve);

        return $this->respond($data);
    }

    public function cargar_documentacion_cdi($clave = null){
        $dir = ROOTPATH.'public/documentos_cdi/'.$clave;
        $results['result'] = array();

        if(is_dir($dir)){
            $files = scandir($dir);
            foreach($files as $key => $value){
                $path = $dir.DIRECTORY_SEPARATOR.$value;
                if(!is_dir($path)) {
                    array_push($results['result'], ['nombre'=> $value,'size'=>filesize($path), 'ext'=>pathinfo($path, PATHINFO_EXTENSION)]);
                } else if($value != "." && $value != "..") {
                    if(!is_dir($path)) { 
                        getDirContents($path, $results);
                        $results['result'] = ['path'=>$path,'size'=>filesize($path), 'ext'=>pathinfo($path, PATHINFO_EXTENSION)];
                    }
                }
            }
        }
       

        return $this->respond($results);
    }

    public function cargar_documentacion($clave = null){
        $dir = ROOTPATH.'public/documentos/'.$clave;
        $results['result'] = array();

        if(is_dir($dir)){
        $files = scandir($dir);
            foreach($files as $key => $value){
                $path = $dir.DIRECTORY_SEPARATOR.$value;
                if(!is_dir($path)) {
                    array_push($results['result'], ['nombre'=> $value,'size'=>filesize($path), 'ext'=>pathinfo($path, PATHINFO_EXTENSION)]);
                } else if($value != "." && $value != "..") {
                    if(!is_dir($path)) { 
                        getDirContents($path, $results);
                        $results['result'] = ['path'=>$path,'size'=>filesize($path), 'ext'=>pathinfo($path, PATHINFO_EXTENSION)];
                    }
                }
            }
        }
        
        return $this->respond($results);
    }

    public function validarcve($cve = null)
    {    
        $data["result"] = $this->padronModel->validar_clave($cve);
        return $this->respond($data);
    }

    public function eliminar_documento_cdi($a = null, $id=null, $doc = null)
    {
        $directorio = ROOTPATH.'public/documentos_cdi/'.$a.$id;

        $file = $directorio."/".$doc;
      
        if (is_readable($file) && unlink($file)) {
            echo "Archivo eliminado correctamente";
        } else {
            echo "El archivo no fue encontrado o no tiene permisos para eliminarlo";
        }
    }

    public function eliminar_documento($a = null, $id=null, $doc = null)
    {
        $directorio = ROOTPATH.'public/documentos/'.$a.$id;

        $file = $directorio."/".$doc;
      
        if (is_readable($file) && unlink($file)) {
            echo "Archivo eliminado correctamente";
        } else {
            echo "El archivo no fue encontrado o no tiene permisos para eliminarlo";
        }
    }

    public function subir_documentos_cdi($a = null, $id = null, $cve = null){
        helper(['form', 'url']);
         //subir los archivos a la carpeta
         $directorio = ROOTPATH.'public/documentos_cdi/'.$a.$id;

         if($archivos = $this->request->getFiles())
         {
             foreach($archivos['archivos'] as $arch)
             {
                 if ($arch->isValid() && ! $arch->hasMoved())
                 {
                     $nombre = $cve."_".$a.$id."_doc".$arch->getName();
                     ;
                     //Validamos si la ruta de destino existe, en caso de no existir la creamos
                    
                     if(!file_exists($directorio)){
                         mkdir($directorio, 0775) or die("No se puede crear el directorio de extracci&oacute;n");	
                     }
                     $arch->move($directorio, $nombre);
                     $i+=1;
                 }
             }
             
             echo "ok";
         }
         else{
             echo "error";
         }
    }

    public function subir_documentos($a = null, $id = null, $cve = null){
        helper(['form', 'url']);
         //subir los archivos a la carpeta
         $directorio = ROOTPATH.'public/documentos/'.$a.$id;

         if($archivos = $this->request->getFiles())
         {
             foreach($archivos['archivos'] as $arch)
             {
                 if ($arch->isValid() && ! $arch->hasMoved())
                 {
                     $nombre = $cve."_".$a.$id."_doc".$arch->getName();
                     ;
                     //Validamos si la ruta de destino existe, en caso de no existir la creamos
                    
                     if(!file_exists($directorio)){
                         mkdir($directorio, 0775) or die("No se puede crear el directorio de extracci&oacute;n");	
                     }
                     $arch->move($directorio, $nombre);
                     $i+=1;
                 }
             }
             
             echo "ok";
         }
         else{
             echo "error";
         }
    }

}
