<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;
use App\Models\PadronModel;
use App\Models\CallesModel;
use App\Models\ColoniasModel;
use App\Models\ConceptosModel;
use CodeIgniter\API\ResponseTrait;


class Ventanilla extends BaseController
{
    use ResponseTrait;
    use \Myth\Auth\AuthTrait;

    public function __construct() {
        // Reestringir el acceso a los usuarios que no esten en el rol de Administracion
        $this->restrictToGroups(['Administracion', 'Ventanilla', 'Seguimiento Ventanilla']);
		$db = db_connect();
		$this->padronModel = new PadronModel($db);
    }
    
    public function index()
    {    
        echo view('header', ['titulo' => 'Solicitud de Trámites']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/ventanilla');
        echo view('footer');
        echo view('ventanilla/mapjs'); 
        echo view('ventanilla/js_atencion'); 
    }    

    public function validar($id = null)
    {   
        
        $model = new PadronModel();  
        $data['geojson'] = $model->where('idcat', $id)->first();
        return view('ventanilla/validaclavecatastral', $data);
    }   

    function avisocdi()
    {
        echo view('header', ['titulo' => 'Cambios de Inscripción']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/avisocdi');
        echo view('footer');    
        echo view('ventanilla/mapjs'); 
        echo view('ventanilla/avisocdijs');
    }

    function avisocdimanzana()
    {
        echo view('header', ['titulo' => 'Cambios de Inscripcion por Manzana']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/avisocdimanzana');
        echo view('footer');    
        echo view('ventanilla/mapjs'); 
        echo view('ventanilla/avisocdimanzanajs');
    }

    function oficiosbonificacion()
    {
        echo view('header', ['titulo' => 'Oficios de Bonificacion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/oficiosbonificacion');
        echo view('footer');    
        echo view('ventanilla/mapjs'); 
        echo view('ventanilla/oficiosbonificacionjs');
    }

    function reporte_listado_declaraciones()
    {
        echo view('header', ['titulo' => 'Reportes Ventanilla']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/reportes/listado_declaraciones');
        echo view('footer');    
        echo view('ventanilla/reportes/listado_declaracionesjs');
    }

    function reporte_numero_declaraciones()
    {
        echo view('header', ['titulo' => 'Reportes Ventanilla']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/reportes/listado_numero_tramites');
        echo view('footer');    
        echo view('ventanilla/reportes/listado_numero_tramitesjs');
    }

    function reporte_primer_registro()
    {
        echo view('header', ['titulo' => 'Reportes Ventanilla']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/reportes/listado_primer_registro');
        echo view('footer');    
        echo view('ventanilla/reportes/listado_primer_registrojs');
    }

    function reporte_inco_inspeccionadas()
    {
        echo view('header', ['titulo' => 'Reportes Ventanilla']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/reportes/listado_inco_inspeccionadas');
        echo view('footer');    
        echo view('ventanilla/reportes/listado_inco_inspeccionadasjs');
    }

    function reporte_inco_por_clavecat()
    {
        echo view('header', ['titulo' => 'Reportes Ventanilla']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/reportes/listado_inco_porclave');
        echo view('footer');    
        echo view('ventanilla/reportes/listado_inco_porclavejs');
    }

    function reporte_cuartel()
    {
        echo view('header', ['titulo' => 'Reportes Ventanilla']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/reportes/listado_cuartel');
        echo view('footer');    
        echo view('ventanilla/reportes/listado_cuarteljs');
    }   

    function reporte_cdi()
    {
        echo view('header', ['titulo' => 'Reportes Ventanilla']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/reportes/listado_cdi');
        echo view('footer');    
        echo view('ventanilla/reportes/listado_cdijs');
    }   

    function reporte_oficiosbonificacion()
    {
        echo view('header', ['titulo' => 'Reportes Ventanilla']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('ventanilla/reportes/listado_oficiobonificacion');
        echo view('footer');    
        echo view('ventanilla/reportes/listado_oficiobonificacionjs');
    }   

    function unidadfoliocambios()
    {
        echo view('ventanilla/unidades/index');    
    }

    function unidadcalle()
    {
        $model = new CallesModel(); 
        $data['result'] = $model->findAll();
        return view('ventanilla/unidades/index', $data);
    }

    function unidadcolonia()
    {
        $model = new ColoniasModel(); 
        $data['result'] = $model->findAll();
        return view('ventanilla/unidades/index', $data);
    }

    function unidadconceptos()
    {
        $model = new ConceptosModel(); 
        $data['result'] = $model->findAll();
        return view('ventanilla/unidades/index', $data);
    }

    public function listadodeclaraciones($a = null)
    {    
        $data['data']	= $this->padronModel->listar_inconformidades($a);
        return $this->respond($data);
    }

    public function listadocdi($a = null)
    {    
        $data['data']	= $this->padronModel->listar_cdi($a);
        return $this->respond($data);
    }

    public function listadocdimanzana($a = null)
    {    
        $data['data']	= $this->padronModel->listar_cdi_manzana($a);
        return $this->respond($data);
    }

    public function consultafolio($a = null, $id= null){
        $data['data']	= $this->padronModel->get_folio_declaracion($a, $id);
        return $this->respond($data);
    }

    public function consultafoliocdi($a = null, $id= null){
        $data['data']	= $this->padronModel->get_folio_cdi($a, $id);
        return $this->respond($data);
    }

    public function generafolio($a = null)
    {
        $data['data']	= $this->padronModel->genera_folio_declaraciones($a);
        return $this->respond($data);
    }

    public function generafoliocdi($a = null)
    {
        $data['data']	= $this->padronModel->genera_folio_cdi($a);
        return $this->respond($data);
    }

    public function generafoliocdimanz($a = null)
    {
        $data['data']	= $this->padronModel->genera_folio_cdi_manz($a);
        return $this->respond($data);
    }

    public function genera_folio_bonificacion($a = null){
        $data['data']	= $this->padronModel->genera_folio_bonificacion($a);
        return $this->respond($data);
    }

    public function guardar_cdi()
    {
        helper(['form', 'url']);

         //Verificamos si ya existe ese folio en la base de datos
         $a = $this->request->getVar('year');
         $id = $this->request->getVar('folio');
         $res_fol = $this->padronModel->check_folio_cdi($a, $id);
             
         if ($res_fol == 1){
             echo "folio_existente";
         }
         else{
            $tipo = $this->request->getVar('tipo_mov');
            $tipob = "";
            $tipoc = "";
            if($tipo == "Baja"){
                $tipob = "X";
            }
            if($tipo == "Cambio"){
                $tipoc = "X";
            }

            $clave = $this->request->getVar('POBL').$this->request->getVar('CTEL').$this->request->getVar('MANZ').$this->request->getVar('PRED').$this->request->getVar('UNID');  

            $data = [
                'Folio' => $this->request->getVar('folio'),
                'Poblacion'  => $this->request->getVar('POBL'),
                'Cuartel'  => $this->request->getVar('CTEL'),
                'Manzana'  => $this->request->getVar('MANZ'),
                'Predio'  => $this->request->getVar('PRED'),
                'Unidad'  => $this->request->getVar('UNID'),    
                'NPoblacion' => $this->request->getVar('NPoblacion'),
                'PropietarioAnterior' => $this->request->getVar('prop_ant'),
                'PropietarioActual' => $this->request->getVar('prop_act')." ".$this->request->getVar('bprop_act'),
                'NombreSolicitante' => $this->request->getVar('solicitante'),
                'PoblacionDescripcion' => '',
                'DomicilioNotificacion' => $this->request->getVar('dom_not'),
                'ColoniaNotificacion' => '',
                'DomicilioUbicacion' => $this->request->getVar('ubi_pred'),
                'ColoniaUbicacion' => '',
                'TramiteNotario' => $this->request->getVar('num_notario'),
                'Concepto' => $this->request->getVar('concepto'),
                'TituloTraslativo' => $this->request->getVar('titulo'),
                'Motivo' => $this->request->getVar('cancelacioncdi'),
                'TipoB' => $tipob,
                'TipoC' => $tipoc,
                'Clave'  => $clave,
                'AnioAviso' => $this->request->getVar('year'),
                'FechaCaptura' => $this->request->getVar('dtfecha'),
                'Observaciones' => $this->request->getVar('observaciones'),
                'IdEmpleadoBarra' => $this->request->getVar('atendio'),
                'NumeroOficial' => $this->request->getVar('num_of'),
                'TelefonoSolicitante' => $this->request->getVar('telefono'),     
            ];

            $save = $this->padronModel->guardar_cdi($data);
            //echo $save;
            if($save){
                echo 'ok';
                //subir los archivos a la carpeta
                if($archivos = $this->request->getFiles())
                {
                    $i = 1;
                    foreach($archivos['archivos'] as $arch)
                    {
                        if ($arch->isValid() && ! $arch->hasMoved())
                        {
                            $nombre = $clave."_".$a.$id."_doc".$arch->getName();
                            //Validamos si la ruta de destino existe, en caso de no existir la creamos
                            $directorio = ROOTPATH.'public/documentos_cdi/'.$a.$id;
                            if(!file_exists($directorio)){
                                mkdir($directorio, 0775) or die("No se puede crear el directorio de extracci&oacute;n");	
                            }
                            $arch->move($directorio, $nombre);
                            $i+=1;
                        }
                    }
                }
            }
            else{
                echo 'error';
            }
        }

    }

    public function guardar_cdi_manz()
    {
        helper(['form', 'url']);
        $tipo = $this->request->getVar('tipomov');
        $tipob = "";
        $tipoc = "";
        if($tipo == "Baja"){
            $tipob = "X";
        }
        if($tipo == "Cambio"){
            $tipoc = "X";
        }

        $clave = $this->request->getVar('POBL').$this->request->getVar('CTEL').$this->request->getVar('MANZ');  

        $data = [
            'Folio' => $this->request->getVar('folio'),
            'Poblacion'  => $this->request->getVar('POBL'),
            'Cuartel'  => $this->request->getVar('CTEL'),
            'Manzana'  => $this->request->getVar('MANZ'),
            'PredioInicio'  => $this->request->getVar('PRED1'),
            'PredioFin'  => $this->request->getVar('PRED2'),    
            'NPoblacion' => $this->request->getVar('NPoblacion'),
            'PropietarioAnterior' => $this->request->getVar('propant'),
            'PropietarioActual' => $this->request->getVar('propact'),
            'PoblacionDescripcion' => '',
            'DomicilioNotificacion' => $this->request->getVar('domnot'),
            'ColoniaNotificacion' => '',
            'DomicilioUbicacion' => $this->request->getVar('ubipred'),
            'ColoniaUbicacion' => '',
            'Noticia' => $this->request->getVar('noticia'),
            'Concepto' => $this->request->getVar('concepto'),
            'TituloTraslativo' => $this->request->getVar('titulo'),
            'Motivo' => $this->request->getVar('cancocdi'),
            'TipoB' => $tipob,
            'Clave' => $clave,
            'TipoC' => $tipoc,
            'AnioAviso' => $this->request->getVar('year'),
            'FechaCaptura' => $this->request->getVar('dtfecha'),
            'Observaciones' => $this->request->getVar('observaciones'),
            'IdEmpleadoBarra' => $this->request->getVar('atendio'),     
        ];

        $save = $this->padronModel->guardar_cdi_manzana($data);
        //echo $save;
        if($save){
            echo 'ok';
            //subir los archivos a la carpeta
                    if($archivos = $this->request->getFiles())
                    {
                        $i = 1;
                        foreach($archivos['archivos'] as $arch)
                        {
                            if ($arch->isValid() && ! $arch->hasMoved())
                            {
                                $nombre = $this->request->getVar('Clave')."_".$a.$id."_doc".$arch->getName();
                                //Validamos si la ruta de destino existe, en caso de no existir la creamos
                                $directorio = ROOTPATH.'public/documentos/'.$a.$id;
                                if(!file_exists($directorio)){
                                    mkdir($directorio, 0775) or die("No se puede crear el directorio de extracci&oacute;n");	
                                }
                                $arch->move($directorio, $nombre);
                                $i+=1;
                            }
                        }
                    }
        }
        else{
            echo 'error';
        }

    }

    public function guardar_declaracion()
    {
        helper(['form', 'url']);
        //Verificamos si ya existe ese folio en la base de datos
        $a = $this->request->getVar('year');
        $id = $this->request->getVar('IdInconformidad');
        $res_fol = $this->padronModel->check_folio($a, $id);
            
        if ($res_fol == 1){
            echo "folio_existente";
        }
        else{
            $data = [
                'IdInconformidad' => $this->request->getVar('IdInconformidad'),
                'IdTramite'  => $this->request->getVar('tramites'),
                'Cuartel'  => $this->request->getVar('CTEL'),
                'Manzana'  => $this->request->getVar('MANZ'),
                'Predio'  => $this->request->getVar('PRED'),
                'Clave'  => $this->request->getVar('Clave'),
                'Propietario'  => $this->request->getVar('appat')." ".$this->request->getVar('apmat')." ".$this->request->getVar('nombres'),
                'Solicitante'  => $this->request->getVar('nom_sol') ,
                'TelSol'  => $this->request->getVar('tel_sol'),
                'DomSol'  => $this->request->getVar('dom_sol'),
                'IdPropietario'  => $this->request->getVar('IdPropietario'),
                'IdColonia'  => $this->request->getVar('idcolonia'),
                'IdCalle'  => $this->request->getVar('idcalle'),
                'Numero'  => $this->request->getVar('numeroof'),
                'ValorCat'  => $this->request->getVar('valorcat'),
                'Uso'  => $this->request->getVar('IdUso'),
                'Observaciones'  => $this->request->getVar('motivo'),
                'FechaCaptura'  => $this->request->getVar('dtfecha'),
                'TotalTerreno'  => $this->request->getVar('superficieterr'),
                'TotalConst'  => $this->request->getVar('superficiecons'),
                'AnioDeclaracion'  => $this->request->getVar('year'),
                'Notario'  => $this->request->getVar('tramitenot'),
                'doc1'  => $this->request->getVar('doc1'),
                'doc2'  => $this->request->getVar('doc2'),
                'doc3'  => $this->request->getVar('doc3'),
                'doc4'  => $this->request->getVar('doc4'),
                'doc5'  => $this->request->getVar('doc5'),
                'doc6'  => $this->request->getVar('doc6'),
                'IdEpleadoBarra' => $this->request->getVar('IdEpleadoBarra'),
                'IdDeclaracion' => $this->request->getVar('IdDeclaracion'),
                'foto'  => $this->request->getVar('image'),
            ];
    
            $save = $this->padronModel->guardar_declaracion($data);
            //echo $save;
            if($save){
                echo 'Declaracion registrada';
                $save_seguimiento = $this->padronModel->guardar_seguimiento_ventanilla($data);
                //echo $save_seguimiento;
                if($save_seguimiento){
                    echo ' y turnada a seguimiento correctamente';
                    $save_foto= $this->padronModel->actualiza_foto($data);
                     //subir los archivos a la carpeta
                    if($archivos = $this->request->getFiles())
                    {
                        $i = 1;
                        foreach($archivos['archivos'] as $arch)
                        {
                            if ($arch->isValid() && ! $arch->hasMoved())
                            {
                                $nombre = $this->request->getVar('Clave')."_".$a.$id."_doc".$arch->getName();
                                //Validamos si la ruta de destino existe, en caso de no existir la creamos
                                $directorio = ROOTPATH.'public/documentos/'.$a.$id;
                                if(!file_exists($directorio)){
                                    mkdir($directorio, 0775) or die("No se puede crear el directorio de extracci&oacute;n");	
                                }
                                $arch->move($directorio, $nombre);
                                $i+=1;
                            }
                        }
                    }
                }
                else{
                    echo 'y error al enviarla a Seguimiento';
                }
            }
            else{
                echo 'No se pudo Registrar la declaracion';
            }   

        }//End else*/

        
    }

    public function guardar_oficio_bonificacion()
    {
        helper(['form', 'url']);
        
        $clave = $this->request->getVar('POBL').$this->request->getVar('CTEL').$this->request->getVar('MANZ').$this->request->getVar('PRED').$this->request->getVar('UNID');  

        $clave2 = $this->request->getVar('POBL2').$this->request->getVar('CTEL2').$this->request->getVar('MANZ2').$this->request->getVar('PRED2').$this->request->getVar('UNID2'); 

        $data = [
            'IdFolio' => $this->request->getVar('folio'),
            'IdAnio' => $this->request->getVar('year'),
            'FechaCaptura' => $this->request->getVar('dtfecha'),
            'Poblacion'  => $this->request->getVar('POBL'),
            'Cuartel'  => $this->request->getVar('CTEL'),
            'Manzana'  => $this->request->getVar('MANZ'),
            'Predio'  => $this->request->getVar('PRED'),
            'Unidad'  => $this->request->getVar('UNID'),
            'ClaveIncorrecta' => $clave,
            'NombreIncorrecto' => $this->request->getVar('nombre'),
            'DomicilioIncorrecto' => $this->request->getVar('domicilio'),
            'SuperficieTerrenoIncorrecto' => $this->request->getVar('superficieterr'),
            'SuperficieConstruccionIncorrecto' => $this->request->getVar('superficiecons'),
            'ValorCatastralIncorrecto' => $this->request->getVar('valorcat'),
            'Poblacion2' => $this->request->getVar('POBL2'),
            'Cuartel2' => $this->request->getVar('CTEL2'), 
            'Manzana2' => $this->request->getVar('MANZ2'), 
            'Predio2' => $this->request->getVar('PRED2'), 
            'Unidad2' => $this->request->getVar('UNID2'), 
            'ClaveCorrecta' => $clave2, 
            'NombreCorrecto'  => $this->request->getVar('nombre2'), 
            'DomicilioCorrecto' => $this->request->getVar('domicilio2'),
            'SuperficieTerrenoCorrecto' => $this->request->getVar('superficieterr2'), 
            'SuperficieConstruccionCorrecto' => $this->request->getVar('superficiecons2'), 
            'ValorCatastralCorrecto' => $this->request->getVar('valorcat2'), 
            'Observaciones' => $this->request->getVar('observaciones'), 
            'IdEmpleadoBarra' => $this->request->getVar('IdEpleadoBarra'),
        ];

        $save = $this->padronModel->guardar_oficio_bonificacion($data);
        //echo $save;
        if($save){
            echo 'Oficio de Bonificación registrado correctamente';
        }
        else{
            echo 'Ocurrio un error al registrar el Oficio de Bonificaciòn';
        }

    }

    //METODOS PARA LOS REPORTES DE VENTANILLA
    public function reporte_listadodeclaraciones($d = null, $f1 = null, $f2 = null)
    {    
        $data['data']	= $this->padronModel->reporte_listadodeclaraciones($d, $f1, $f2);
        return $this->respond($data);
    }

    public function reporte_listado_numero_tramites($d = null)
    {    
        $data['data']	= $this->padronModel->reporte_listado_numero_tramites($d,);
        return $this->respond($data);
    }

    public function get_reporte_primer_registro($f1 = null, $f2=null)
    {    
        $data['data']	= $this->padronModel->reporte_primer_registro($f1, $f2);
        return $this->respond($data);
    }

    public function get_reporte_inco_inspeccionadas()
    {    
        $data['data']	= $this->padronModel->reporte_inco_inspeccionadas();
        return $this->respond($data);
    }

    public function get_reporte_inco_x_clavecat($d = null)
    {    
        $data['data']	= $this->padronModel->reporte_inco_x_clavecat($d);
        return $this->respond($data);
    }

    public function get_reporte_cuartel($d = null, $t= null, $f1 = null, $f2 = null)
    {    
        $data['data']	= $this->padronModel->reporte_cuartel($d, $t, $f1, $f2);
        return $this->respond($data);
    }

    public function get_reporte_cdi($f1 = null, $f2=null)
    {    
        $data['data']	= $this->padronModel->reporte_cdi($f1, $f2);
        return $this->respond($data);
    }

    public function get_reporte_oficiosbonificacion($f1 = null, $f2 = null)
    {    
        $data['data'] = $this->padronModel->reporte_oficiobonificacion($f1, $f2);
        return $this->respond($data);
    }

    public function get_oficiosbonificacion($a = null){
        $data['data'] = $this->padronModel->lisget_oficiosbonificaciontar_cdi($a);
        return $this->respond($data);
    }
    
   
}
