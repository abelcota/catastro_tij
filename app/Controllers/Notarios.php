<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\API\ResponseTrait;
use App\Models\PadronModel;
use \PhpCfdi\Credentials\Credential;
use Myth\Auth\Entities\User;
use Myth\Auth\Models\UserModel;

class Notarios extends Controller
{  
    use \Myth\Auth\AuthTrait;
    use ResponseTrait;
    
    public function __construct()
	{      
        // Reestringir el acceso a los usuarios que no esten en el rol de Inspeccion o Administracion
        $this->restrictToGroups(['Notario', 'Administracion']);
        $db = db_connect();
		//$this->peritosModel = new PeritosModel($db);
        $this->padronModel = new PadronModel($db);
    }

    public function notasisai()
    {    
        echo view('header', ['titulo' => 'Notas ISAI']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('notarios/notasisai.php');
        echo view('footer');        
        echo view('notarios/notasisaijs.php');
    }

}