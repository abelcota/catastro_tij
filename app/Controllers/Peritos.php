<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\API\ResponseTrait;
use App\Models\PeritosModel;
use App\Models\PadronModel;
use \PhpCfdi\Credentials\Credential;
use Myth\Auth\Entities\User;
use Myth\Auth\Models\UserModel;

class Peritos extends Controller
{  
    use \Myth\Auth\AuthTrait;
    use ResponseTrait;
    
    public function __construct()
	{      
        // Reestringir el acceso a los usuarios que no esten en el rol de Inspeccion o Administracion
        $this->restrictToGroups(['Perito', 'Administracion']);
        $db = db_connect();
		$this->peritosModel = new PeritosModel($db);
        $this->padronModel = new PadronModel($db);
    }

    public function avaluos()
    {    
        echo view('header', ['titulo' => 'Perito Avaluos']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('peritos/avaluos.php');
        echo view('footer');        
        echo view('peritos/avaluosjs.php');
    }

    public function solicitudavaluo($info = null, $tipo = null){
        echo view('header', ['titulo' => 'Solicitud de Avaluos']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('peritos/avaluo/solicitud', ['info' => $info, 'tipo' => $tipo]);
        echo view('footer');
        echo view('peritos/mapjs'); 
        echo view('peritos/avaluo/solicitudjs');
   
    }


    public function configuracion()
    {    
        echo view('header', ['titulo' => 'Perito Configuracion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('peritos/configuracion/configuracion.php');
        echo view('footer');        
        echo view('peritos/configuracion/configuracionjs.php');
    }

    public function firmar(){
        helper(['form', 'url']);
        $certfile = $this->request->getFile('certfile');
        $pemKeyFile = $this->request->getFile('certkey');
        $passPhrase = $this->request->getVar('passkey');
        $mensaje = $this->request->getVar('mensaje');

        $fiel = Credential::openFiles($certfile, $pemKeyFile, $passPhrase);
        $certificado = $fiel->certificate();

        $cadena_original = "||".strftime("%Y/%m/%d")."|".$certificado->rfc().$mensaje;

        $signature = $fiel->sign($cadena_original);
        $data['obj'] =(array)$certificado;
        $data['certificado'] = array(['rfc' => $certificado->rfc(), 'nombre' => $certificado->legalName(),
        'empresa' => $certificado->branchName(), 'serial' =>$certificado->serialNumber()->bytes(), 'sello' => base64_encode($signature), 'cadena_original' => $cadena_original]);

        return $this->respond($data);

    }


    public function imprimiravaluo($info = null){
        //$cerFile = "assets/firma/CSD_KARLA_FUENTE_NOLASCO_FUNK671228PH6_20190528_174243s.cer";
        //$pemKeyFile = "assets/firma/CSD_KARLA_FUENTE_NOLASCO_FUNK671228PH6_20190528_174243.key";
        //$passPhrase = base64_decode('MTIzNDU2Nzhh'); // contraseña para abrir la llave privada
        //$fiel = Credential::openFiles($cerFile, $pemKeyFile, $passPhrase);
        //$certificado = $fiel->certificate();
        echo view('header', ['titulo' => 'Impresion de Avaluo', 'page' => 'impavaluo']);
        echo view('peritos/avaluo/imprimir', ['info' => $info]); //, 'fiel' => $fiel, 'cert' => $certificado]);
        echo view('footer');
        echo view('peritos/mapjs');
        echo view('peritos/avaluo/imprimirjs');   
    }

    public function infocatastral($info = null){
        echo view('header', ['titulo' => 'Informacion Catastral']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('peritos/info_catastral/solicitud', ['info' => $info]);
        echo view('footer');
        //echo view('peritos/avaluo/solicitudjs');

    }

    public function test_pago(){
        $data = $this->peritosModel->test_pago();
        
        return $this->respond($data);
    }

    
    //MODULO DE PERITO EN LINEA

    public function get_avaluos_comercial($tipo = null){
        $data = $this->peritosModel->get_avaluos_comercial($tipo); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_bitacora_avaluos($id = null)
    {
        $data = $this->peritosModel->get_bitacora_avaluos($id); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function validar_infocat($info = null)
    {
        $data["result"] = $this->peritosModel->validar_infocat($info);
        return $this->respond($data);
    }

    public function nuevo_avaluo()
    {
        helper(['form', 'url']);
        $data = [
            'infcat'     => $this->request->getVar('infcat'),
            'numsol'     => $this->request->getVar('numsol'),
            'perito'     => $this->request->getVar('perito'),
            'cvecat'     => $this->request->getVar('cvecat')
        ];

        $res = $this->peritosModel->nuevo_avaluo($data);

        echo $res;
        //if($save){ echo 'ok'; } else{ echo 'error'; }

        //$data
        //$data["result"] = $this->peritosModel->nuevo_avaluo_web($data);
        //return $this->respond($data);
    }
    

    public function consulta_cat_const(){
        
        $data['data'] = $this->peritosModel->consulta_cat_const();
        //$data['portada'] = $this->peritosModel->consultar_avaluo_portada($info);
        return $this->respond($data);
   }

    public function consulta_factor_demerito_construccion(){
        helper(['form', 'url']);
        $claves = [
            'CVE_CAT_CONST'     => $this->request->getVar('CVE_CAT_CONST'),
            'CVE_EDO_CONS'      => $this->request->getVar('CVE_EDO_CONS'),
            'EDD_CONST'         => $this->request->getVar('EDD_CONST')
        ];

        $data['data'] = $this->peritosModel->consulta_factor_demerito_construccion($claves);
        return $this->respond($data);
    }

    public function obtener_perito_perfil($id = null){
        
        $data['perfil'] = $this->peritosModel->consultar_perito_perfil($id);
        return $this->respond($data);
    }

    public function obtener_autorizacion_perito($info = null){
        $data['autorizacion'] = $this->peritosModel->consultar_autorizacion_perito($info);
        return $this->respond($data);
    }

    public function obtener_info($info = null){
        
        $data['info'] = $this->peritosModel->consultar_avaluo_infoid($info);
        $data['detalle'] = $this->peritosModel->consultar_avaluo_detalle($info);
        $data['Terr'] = $this->peritosModel->get_avaluo_fisico_terreno($info);
        $data['Const'] = $this->peritosModel->get_avaluo_fisico_construcciones($info);
        $data['Esp'] = $this->peritosModel->get_avaluo_fisico_especiales($info);
        $data["plano"] = $this->peritosModel->obtener_plano($info);
        //$data['portada'] = $this->peritosModel->consultar_avaluo_portada($info);
        return $this->respond($data);
    }

    public function obtener_portada($info = null){
        
        $data['portada'] = $this->peritosModel->consultar_avaluo_portada($info);
        return $this->respond($data);
    }

    public function obtener_antecedentes($info = null){
        
        $data['antecedentes'] = $this->peritosModel->consultar_avaluo_antecedentes($info);
        return $this->respond($data);
    }   

    public function obtener_car_urb($info = null){
        
        $data['caracteristicas_urbanas'] = $this->peritosModel->consultar_avaluo_carurb($info);
        return $this->respond($data);
    }

    public function obtener_car_terreno($info = null){
        
        $data['caracteristicas_terreno'] = $this->peritosModel->consultar_avaluo_carterreno($info);
        return $this->respond($data);
    }

    public function obtener_descripcion_general($info = null){
        
        $data['descripcion_general'] = $this->peritosModel->consultar_avaluo_descripcion_general($info);
        return $this->respond($data);
    }
    
    public function obtener_elementos_construccion($info = null){
        
        $data['elementos_construccion'] = $this->peritosModel->consultar_avaluo_elementos_construccion($info);
        return $this->respond($data);
    }

    public function obtener_orientaciones($info = null){
        
        $data['orientaciones'] = $this->peritosModel->consultar_avaluo_orientaciones($info);
        return $this->respond($data);
   }

   public function eliminar_avaluo_orientacion($id = null){
        $save = $this->peritosModel->eliminar_avaluo_orientacion($id);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function obtener_construcciones($cve = null){
        
        $data['construcciones'] = $this->peritosModel->consultar_construcciones($cve);
        return $this->respond($data);
   }

   
    public function validarcve($cve = null)
    {    
        $data["result"] = $this->peritosModel->validar_clave($cve);

        return $this->respond($data);
    }

    public function get_clasificacion_zona()
    {    
        $data["result"] = $this->peritosModel->listar_clasificacion_zona();
        return $this->respond($data);
    }

    public function get_construccion_dominante()
    {    
        $data["result"] = $this->peritosModel->listar_construccion_dominante();
        return $this->respond($data);
    }

    public function get_saturacion_zona()
    {    
        $data["result"] = $this->peritosModel->listar_saturacion_zona();
        return $this->respond($data);
    }

    public function get_poblacion()
    {    
        $data["result"] = $this->peritosModel->listar_poblacion();
        return $this->respond($data);
    }

    public function get_suelo_permitido()
    {    
        $data["result"] = $this->peritosModel->listar_suelo_permitido();
        return $this->respond($data);
    }

    public function grupo_usos(){
        $data['results'] = $this->peritosModel->listar_grupo_usos();
        return $this->respond($data);
    }

    public function get_list_especiales()
    {    
        $data["result"] = $this->peritosModel->listar_instalaciones_especiales();
        return $this->respond($data);
    }

    public function get_list_fachadas()
    {    
        $data["result"] = $this->peritosModel->listar_fachadas();
        return $this->respond($data);
    }

    public function get_list_cerrajeria()
    {    
        $data["result"] = $this->peritosModel->listar_cerrajeria();
        return $this->respond($data);
    }

    public function get_list_vidrieria()
    {    
        $data["result"] = $this->peritosModel->listar_vidrieria();
        return $this->respond($data);
    }

    public function get_list_herreria()
    {    
        $data["result"] = $this->peritosModel->listar_herreria();
        return $this->respond($data);
    }

    public function get_list_instalacionese()
    {    
        $data["result"] = $this->peritosModel->listar_instalacionese();
        return $this->respond($data);
    }

    public function get_list_mueblesb()
    {    
        $data["result"] = $this->peritosModel->listar_mueblesb();
        return $this->respond($data);
    }

    public function get_list_puertas()
    {    
        $data["result"] = $this->peritosModel->listar_puertas();
        return $this->respond($data);
    }

    public function get_list_pintura()
    {    
        $data["result"] = $this->peritosModel->listar_pintura();
        return $this->respond($data);
    }

    public function get_list_pisos()
    {    
        $data["result"] = $this->peritosModel->listar_pisos();
        return $this->respond($data);
    }

    public function get_list_lambrines()
    {    
        $data["result"] = $this->peritosModel->listar_lambrines();
        return $this->respond($data);
    }

    public function get_list_aplanados()
    {    
        $data["result"] = $this->peritosModel->listar_aplanados();
        return $this->respond($data);
    }
    
    public function get_list_techos()
    {    
        $data["result"] = $this->peritosModel->listar_techos();
        return $this->respond($data);
    }

    public function get_list_muros()
    {    
        $data["result"] = $this->peritosModel->listar_muros();
        return $this->respond($data);
    }

    public function get_list_cimientos()
    {    
        $data["result"] = $this->peritosModel->listar_cimientos();
        return $this->respond($data);
    }

    public function consulta_terreno($cve = null){
        $data['data'] = $this->padronModel->get_terreno($cve);

        return $this->respond($data);
    }

    public function obtener_avaluo_fisico($info = null){
        $data['Terr'] = $this->peritosModel->get_avaluo_fisico_terreno($info);
        $data['Const'] = $this->peritosModel->get_avaluo_fisico_construcciones($info);
        $data['Esp'] = $this->peritosModel->get_avaluo_fisico_especiales($info);

        return $this->respond($data);
    }

    public function consulta_terreno_fisico($info = null){
        $data['data'] = $this->peritosModel->get_terreno_fisico($info);
        return $this->respond($data);
    }

    public function consulta_construcciones($cve = null){
        $data['data'] = $this->padronModel->get_construccion($cve);

        return $this->respond($data);
    }

    //GUARDADOS

    public function registrar_perito_perfil(){
        helper(['form', 'url']);
        $data = [
            'id' => $this->request->getVar('txtperito'),
            'nombre' => $this->request->getVar('txtnombre'),
            'rfc' => $this->request->getVar('txtrfc'),
            'registro' => $this->request->getVar('txtregistro'),
            'especialidad' => $this->request->getVar('txtespecialidad'),
            'direccion' => $this->request->getVar('txtdireccion'),
            'telefono' => $this->request->getVar('txttelefono'),
            'celular' => $this->request->getVar('txtcelular'),
            'correo' => $this->request->getVar('txtcorreo')
        ];
        //return $this->respond($data);
        $save = $this->peritosModel->registrar_perito_perfil($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function registrar_orientacion(){
        helper(['form', 'url']);
        $data = [
            'inf_cat' => $this->request->getVar('inf_cat'),
            'orientacion' => $this->request->getVar('orientacion'),
            'descripcion' => $this->request->getVar('descripcion')
        ];
        $save = $this->peritosModel->registrar_avaluo_orientacion($data);

        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }

    }

    public function registrar_portada(){
        helper(['form', 'url']);
        $data = [
            'avaluo'        => $this->request->getVar('avaluo'),
            'info'          => $this->request->getVar('info'),
            'inmueble'      => $this->request->getVar('inmueble'),
            'cp'            => $this->request->getVar('cp'),
            'calle'         => $this->request->getVar('calle'),
            'numero'        => $this->request->getVar('numero'),
            'colonia'       => $this->request->getVar('colonia'),
            'municipio'     => $this->request->getVar('municipio'),
            'notainterna'   => $this->request->getVar('notaInterna'),
            'cve_calle'     => $this->request->getVar('cve_calle'),
            'cve_col'       => $this->request->getVar('cve_col')
        ];

        $save = $this->peritosModel->registrar_portada($data);

        //echo $save;
        if($save){ echo 'ok'; } else{ echo 'error'; }
    }

    public function registrar_antecedentes(){
        helper(['form', 'url']);
        $data = [
            'avaluo'            => $this->request->getVar('avaluo'),
            'info'              => $this->request->getVar('info'),
            'solicitante'       => $this->request->getVar('solicitante'),
            'fechaInspeccion'   => $this->request->getVar('fechaInspeccion'),
            'regimen'           => $this->request->getVar('regimen'),
            'objeto'            => $this->request->getVar('objeto'),
            'folioRegPubProp'   => $this->request->getVar('folioRegPubProp'),
            'inscripcion'       => $this->request->getVar('inscripcion'),
            'libro'             => $this->request->getVar('libro'),
            'seccion'           => $this->request->getVar('seccion')
        ];

        $save = $this->peritosModel->registrar_antecedentes($data);

        //echo $save;
        if($save){ echo 'ok'; } else{ echo 'error'; }
    }

    public function registrar_car_urb(){
        helper(['form', 'url']);
        $data = [
            'avaluo'                    => $this->request->getVar('avaluo'),
            'info'                      => $this->request->getVar('info'),
            'clasificacionZona'         => $this->request->getVar('clasificacionZona'),
            'tipoConstDominante'        => $this->request->getVar('tipoConstDominante'),
            'indiceSatZona'             => $this->request->getVar('indiceSatZona'),
            'poblacion'                 => $this->request->getVar('poblacion'),
            'contaminacionAmbiental'    => $this->request->getVar('contaminacionAmbiental'),
            'usoSueloPermitido'         => $this->request->getVar('usoSueloPermitido'),
            'viaAccesoImportancia'      => $this->request->getVar('viaAccesoImportancia'),
            'agua'                      => $this->request->getVar('agua'),
            'drenaje'                   => $this->request->getVar('drenaje'),
            'energia'                   => $this->request->getVar('energia'),
            'alumbrado'                 => $this->request->getVar('alumbrado'),
            'basura'                    => $this->request->getVar('basura'),
            'transporte'                => $this->request->getVar('transporte'),
            'telefono'                  => $this->request->getVar('telefono'),
            'banqueta'                  => $this->request->getVar('banqueta'),
            'guarniciones'              => $this->request->getVar('guarniciones'),
            'pavimento'                 => $this->request->getVar('pavimento'),
            'vigilancia'                => $this->request->getVar('vigilancia')
        ];

        $save = $this->peritosModel->registrar_car_urb($data);

        //echo $save;
        if($save){ echo 'ok'; } else{ echo 'error'; }
    }

    public function registrar_car_terreno(){
        helper(['form', 'url']);
        $data = [
            'avaluo'                    => $this->request->getVar('avaluo'),
            'info'                      => $this->request->getVar('info'),
            'TopografiaConstruccion'    => $this->request->getVar('TopografiaConstruccion'),
            'NumeroFrentes'             => $this->request->getVar('NumeroFrentes'),
            'CaracteristicasPanoramicas' => $this->request->getVar('CaracteristicasPanoramicas'),
            'DensidadHabitacional'      => $this->request->getVar('DensidadHabitacional'),
            'IntensidadConstruccion'    => $this->request->getVar('IntensidadConstruccion'),
            'ServidumbresRestricciones' => $this->request->getVar('ServidumbresRestricciones'),
            'SuperficieEscrituras'      => $this->request->getVar('SuperficieEscrituras'),
            'SuperficieTopografico'     => $this->request->getVar('SuperficieTopografico')
        ];

        $save = $this->peritosModel->registrar_car_terreno($data);

        //echo $save;
        if($save){ echo 'ok'; } else{ echo 'error'; }
    }

    public function registrar_descripcion_general(){
        helper(['form', 'url']);
        $data = [
            'avaluo'                    => $this->request->getVar('avaluo'),
            'info'                      => $this->request->getVar('info'),
            'CVE_GPO_USO'               => $this->request->getVar('CVE_GPO_USO'),
            'EdadAproximada'            => $this->request->getVar('EdadAproximada'),
            'NumeroNiveles'             => $this->request->getVar('NumeroNiveles'),
            'EstadoConservacion'        => $this->request->getVar('EstadoConservacion'),
            'CalidadProyecto'           => $this->request->getVar('CalidadProyecto'),
            'UnidadesRentables'         => $this->request->getVar('UnidadesRentables'),
            'DescripcionGeneral'        => $this->request->getVar('DescripcionGeneral')
        ];

        $save = $this->peritosModel->registrar_descripcion_general($data);

        //echo $save;
        if($save){ echo 'ok'; } else{ echo 'error'; }
    }

    public function registrar_elementos_construccion(){
        helper(['form', 'url']);
        $data = [
            'avaluo'                        => $this->request->getVar('avaluo'),
            'info'                          => $this->request->getVar('info'),
            'A_Cimientos'                   => $this->request->getVar('A_Cimientos'),
            'A_Estructura'                  => $this->request->getVar('A_Estructura'),
            'A_Muros'                       => $this->request->getVar('A_Muros'),
            'A_EntrePisos'                  => $this->request->getVar('A_EntrePisos'),
            'A_Techos'                      => $this->request->getVar('A_Techos'),
            'A_Azoteas'                     => $this->request->getVar('A_Azoteas'),
            'A_Bardas'                      => $this->request->getVar('A_Bardas'),
            'B_AplanadosInteriores'         => $this->request->getVar('B_AplanadosInteriores'),
            'B_AplanadosExteriores'         => $this->request->getVar('B_AplanadosExteriores'),
            'B_Plafones'                    => $this->request->getVar('B_Plafones'),
            'B_Lambrines'                   => $this->request->getVar('B_Lambrines'),
            'B_Pisos'                       => $this->request->getVar('B_Pisos'),
            'B_Zoclos'                      => $this->request->getVar('B_Zoclos'),
            'B_Escaleras'                   => $this->request->getVar('B_Escaleras'),
            'B_Pintura'                     => $this->request->getVar('B_Pintura'),
            'B_RecubrimientosEspeciales'    => $this->request->getVar('B_RecubrimientosEspeciales'),
            'C_Puertas'                     => $this->request->getVar('C_Puertas'),
            'C_Closets'                     => $this->request->getVar('C_Closets'),
            'D_MueblesDeBano'               => $this->request->getVar('D_MueblesDeBano'),
            'D_EquipoDeConcina'             => $this->request->getVar('D_EquipoDeConcina'),
            'E_InstalacionesElectricas'     => $this->request->getVar('E_InstalacionesElectricas'),
            'F_Herreria'                    => $this->request->getVar('F_Herreria'),
            'G_Vidrieria'                   => $this->request->getVar('G_Vidrieria'),
            'H_Cerrajeria'                  => $this->request->getVar('H_Cerrajeria'),
            'I_Fachadas'                    => $this->request->getVar('I_Fachadas'),
            'J_InstalacionesEspeciales'     => $this->request->getVar('J_InstalacionesEspeciales')
        ];

        $save = $this->peritosModel->registrar_elementos_construccion($data);

        //echo $save;
        if($save){ echo 'ok'; } else{ echo 'error'; }
    }

    public function registrar_avaluo_fisico(){
        helper(['form', 'url']);
        $data = [
            'avaluo'             => $this->request->getVar('avaluo'),
            'info'               => $this->request->getVar('info'),
            'terrenos'           => $this->request->getVar('fisico'),
            'construcciones'     => $this->request->getVar('construcciones'),
            'especiales'         => $this->request->getVar('especiales'),
            'valor_mercado'      => $this->request->getVar('valorMercado')
        ];

        $save = $this->peritosModel->registrar_avaluo_fisico($data);

        //echo $save;
        if($save){ echo 'ok'; } else{ echo 'error'; }

    }

    public function registrar_valor_mercado(){
        helper(['form', 'url']);
        $data = [
            'avaluo'             => $this->request->getVar('avaluo'),
            'info'               => $this->request->getVar('info'),
            'valor_mercado'      => $this->request->getVar('valorMercado'),
            'consideraciones'    => $this->request->getVar('consideraciones')
        ];

        $save = $this->peritosModel->registrar_valor_mercado($data);

        //echo $save;
        if($save){ echo 'ok'; } else{ echo 'error'; }

    }

    public function registrar_sello(){
        helper(['form', 'url']);
        $data = [
            'avaluo'    => $this->request->getVar('avaluo'),
            'info'      => $this->request->getVar('info'),
            'rfc'       => $this->request->getVar('rfc'),
            'nombre'    => $this->request->getVar('nombre'),
            'serie'     => $this->request->getVar('serie'),
            'sello'     => $this->request->getVar('sello')
        ];

        $save = $this->peritosModel->registrar_sello($data);

        //echo $save;
        if($save){ echo 'ok'; } else{ echo 'error'; }



    }

  

    public function upload_plano(){
        helper(['form', 'url']);

        $avaluo = $this->request->getVar('avaluo');
        $info = $this->request->getVar('inf');     
        
        $subido = 0;
        $ruta = "";

        //$uuid = guidv4();

        if($archivos = $this->request->getFiles())
        {
            $arch = $archivos['file'];
            if ($arch->isValid() && ! $arch->hasMoved())
                {
                    $nombre = $arch->getName();
                    //Validamos si la ruta de destino existe, en caso de no existir la creamos
                    $directorio = ROOTPATH.'public/documentos_peritos/planos/'.$info.'/';
                    if(!file_exists($directorio)){
                        mkdir($directorio, 0775) or die("No se puede crear el directorio de extraccion");	
                    }
                    $arch->move($directorio, $nombre);
                    $subido = 1;
                    $save = $this->peritosModel->upload_plano($nombre, $avaluo, $info);
                }else{
                    $subido = 0;
                }
        }
        
        //echo $save;
        if($subido == 1){ echo $ruta; } else{ echo 'error'; }
    }

    public function actualizar_imagen(){
        helper(['form', 'url']);
        $data = [
            'id_imagen'     => $this->request->getVar('imgID'),
            'descripcion'   => $this->request->getVar('descripcion')
        ];

        $save = $this->peritosModel->actualizar_imagen($data);

        //echo $save;
        if($save){ echo 'ok'; } else{ echo 'error'; }
    }

    public function eliminar_imagen(){
        helper(['form', 'url']);
        $data = [
            'id_imagen'     => $this->request->getVar('imgID'),
            'inf_cat'       => $this->request->getVar('inf'),
            'nombre'        =>$this->request->getVar('nombre')
        ];

        $save = $this->peritosModel->eliminar_imagen($data);

        //echo $save;
        if($save){ echo 'ok'; } else{ echo 'error'; }
    } 
    
    public function upload_imagen(){
        helper(['form', 'url']);

        $avaluo = $this->request->getVar('avaluo');
        $info = $this->request->getVar('inf');     
        
        $subido = 0;
        $ruta = "";
        //generar uuid para la imagen
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        // Output the 36 character UUID.
        $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));

        if($archivos = $this->request->getFiles())
        {
            $arch = $archivos['file'];
            if ($arch->isValid() && ! $arch->hasMoved())
                {
                    $file_ext = pathinfo($arch->getName(), PATHINFO_EXTENSION);
                    $nombre = $uuid.".".$file_ext;
                    //Validamos si la ruta de destino existe, en caso de no existir la creamos
                    $directorio = ROOTPATH.'public/documentos_peritos/img_reporte_fotografico/'.$info.'/';
                    if(!file_exists($directorio)){
                        mkdir($directorio, 0775) or die("No se puede crear el directorio de extraccion");	
                    }
                    $arch->move($directorio, $nombre);
                    $subido = 1;
                    $save = $this->peritosModel->upload_imagen($nombre, $avaluo, $info);
                }else{
                    $subido = 0;
                }
        }
        
        //echo $save;
        if($subido == 1){ echo 'ok'; } else{ echo 'error'; }
    }

    public function obtener_imagenes($inf = null){
        $data["result"] = $this->peritosModel->obtener_imagenes($inf );
        return $this->respond($data);
    }



    /**
	 * Verifies the code with the email and saves the new password,
	 * if they all pass validation.
	 *
	 * @return mixed
	 */
	public function attemptReset()
	{
		$users = model(UserModel::class);
		$user = $users->where('id', $this->request->getPost('id'))
					  ->first();

		if (is_null($user))
		{
			echo "no se encontro el usuario";
		}

		$user->setPassword($this->request->getPost('password'));

		$user->reset_at 		= date('Y-m-d H:i:s');

		if($users->save($user))
		{
			echo "Contraseña actualizada correctamente";
		}
		else{
			echo "No se pudo actualizar la contraseña";
		}
	}

}