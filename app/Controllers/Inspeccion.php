<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\API\ResponseTrait;
use App\Models\InspeccionModel;

class Inspeccion extends Controller
{
    use \Myth\Auth\AuthTrait;
    use ResponseTrait;

    public function __construct()
	{      
        // Reestringir el acceso a los usuarios que no esten en el rol de Inspeccion o Administracion
        $this->restrictToGroups(['Administracion', 'Inspeccion']);
        $db = db_connect();
		$this->inspeccionModel = new InspeccionModel($db);
    }

    public function index()
    {    
        echo "Inspeccion";
    }
    
    public function consulta_ventanilla()
    {
        echo view('header', ['titulo' => 'Bitacora de Dictamenes Ventanilla']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('inspeccion/ventanilla/consultadictamenes');
        echo view('footer');        
        echo view('inspeccion/ventanilla/consultadictamenesjs');
    }

    public function get_envios_graficacion(){
        $data['data'] = $this->inspeccionModel->get_envios_graficacion();
        return $this->respond($data);
    }

    public function get_notin_graficacion(){
        $data = $this->inspeccionModel->get_notin_graficacion(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_notin_graficacion_reg(){
        $data = $this->inspeccionModel->get_notin_graficacion_reg(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    

    public function generar_bitacora_inspeccion_regularizacion(){
        $data['data'] = $this->inspeccionModel->generar_bitacora_inspeccion_regularizacion();
        return $this->respond($data);
    }

    public function get_brigadas_inspeccion(){
        $data['results'] = $this->inspeccionModel->get_brigadas_inspeccion();
        return $this->respond($data);
    }

    public function consultar_folio($a = null, $fol = null)
    {
        $data['results'] = $this->inspeccionModel->consultar_folio($a, $fol);
        return $this->respond($data);
    }
    

    public function get_movimientos_inspeccion(){
        $data['results'] = $this->inspeccionModel->get_movimientos_inspeccion();
        return $this->respond($data);
    }

    public function get_dictamenes_ventanilla(){
        $data = $this->inspeccionModel->get_dictamenes_ventanilla();
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_dictamenes_regularizacion(){
        $data = $this->inspeccionModel->get_dictamenes_regularizacion();
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_envios_regularizacion_graficacion()
    {
        $data = $this->inspeccionModel->get_envios_regularizacion_graficacion();
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function capturar_inspeccion_ventanilla(){
        helper(['form', 'url']);
        $data = [
            'FInspeccion' => $this->request->getVar('dtfecha'),
            'Brigada'  => $this->request->getVar('brigadas'),
            'Inspector1'  => $this->request->getVar('i1'),
            'Inspector2'  => $this->request->getVar('i2'),
            'Resultado'  => 'IN',
            'Mov1'  => $this->request->getVar('movimiento1'),
            'Mov2'  => $this->request->getVar('movimiento2'),
            'Dictamen'  => $this->request->getVar('dictamen'),
            'Observaciones'  => $this->request->getVar('observaciones'),
            'Anio'  => $this->request->getVar('year'),
            'Tipo'  => 'B',
            'Folio'  => $this->request->getVar('folio')
        ];

        $save = $this->inspeccionModel->capturar_dictamen_ventanilla($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function get_empleados(){
        $data['result'] = $this->inspeccionModel->get_empleados(); 
        return $this->respond($data);
    }

    public function genera_bitacora_graficacion(){
        $data['result'] = $this->inspeccionModel->genera_bitacora_graficacion(); 
        return $this->respond($data);
    }

    public function genera_bitacora_regularizacion(){
        $data['result'] = $this->inspeccionModel->genera_bitacora_regularizacion(); 
        return $this->respond($data);
    }

    public function genera_folio_paquete_regularizacion($a = null){
        $data['result'] = $this->inspeccionModel->genera_folio_paquete_regularizacion($a); 
        return $this->respond($data);
    }

    public function genera_bitacora_graficacion_reg(){
        $data['result'] = $this->inspeccionModel->genera_bitacora_graficacion_reg(); 
        return $this->respond($data);
    }

    public function get_regularizacion_inspeccion_sinpaquete()
    {
        $data = $this->inspeccionModel->get_regularizacion_inspeccion_sinpaquete();
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function registrar_bitacora_regularizacion_inspeccion(){
        helper(['form', 'url']);
        $data = [
            'Bitacora' => $this->request->getVar('Bitacora'),
            'FechaEnv' => $this->request->getVar('FechaEnv'),
            'Folios' => $this->request->getVar('Folios'),
        ];

        $save = $this->inspeccionModel->registrar_bitacora_regularizacion_inspeccion($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    //registrar el paquete de envio a inspeccion
    public function registrar_bitacora_graficacion(){ 
        helper(['form', 'url']);
        $data = [
            'Bitacora' => $this->request->getVar('Bitacora'),
            'FechaEnv' => $this->request->getVar('FechaEnv'),
            'Origen' => $this->request->getVar('Origen'),
            'Envia' => $this->request->getVar('Envia'),
            'Recibe' => $this->request->getVar('Recibe'),
            'Folios' => $this->request->getVar('Folios'),
        ];

        $save = $this->inspeccionModel->registrar_bitacora_graficacion($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function registrar_bitacora_regularizacion_graficacion(){
        helper(['form', 'url']);
        $data = [
            'Bitacora' => $this->request->getVar('Bitacora'),
            'FechaEnv' => $this->request->getVar('FechaEnv'),
            'Envia' => $this->request->getVar('Envia'),
            'Recibe' => $this->request->getVar('Recibe'),
            'Folios' => $this->request->getVar('Folios'),
        ];

        $save = $this->inspeccionModel->registrar_bitacora_regularizacion_graficacion($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function registrar_regularizacion_inspeccion(){

        helper(['form', 'url']);
        $clave = $this->request->getVar('POBL').$this->request->getVar('CTEL').$this->request->getVar('MANZ').$this->request->getVar('PRED').$this->request->getVar('UNID');

        $data = [
            'anio' => $this->request->getVar('year'),
            'folio' => $this->request->getVar('folio'),
            'finspec' => $this->request->getVar('dtfechain'),
            'brigada' => $this->request->getVar('brigadas'),
            'i1' => $this->request->getVar('i1'),
            'i2' => $this->request->getVar('i2'),
            'mov1' => $this->request->getVar('movimiento1'),
            'mov2' => $this->request->getVar('movimiento2'),
            'dictamen' => $this->request->getVar('dictamen'),
            'observaciones' => $this->request->getVar('observaciones'),
            'area' => $this->request->getVar('area'),
            'pobl' => $this->request->getVar('POBL'),
            'ctel' => $this->request->getVar('CTEL'),
            'manz' => $this->request->getVar('MANZ'),
            'pred' => $this->request->getVar('PRED'),
            'unid' => $this->request->getVar('UNID'),
            'clave' => $clave,
        ];

        $save = $this->inspeccionModel->registrar_regularizacion_inspeccion($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function consulta_graficacion()
    {
        echo view('header', ['titulo' => 'Bitacora de Envios a Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('inspeccion/ventanilla/consultardictamenesgraf');
        echo view('footer');        
        echo view('inspeccion/ventanilla/consultardictamenesgrafjs');
    }

    public function consulta_regularizacion()
    {
        echo view('header', ['titulo' => 'Bitacora de Dictamenes Regularizacion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('inspeccion/regularizacion/consultadictamenesreg');
        echo view('footer');        
        echo view('inspeccion/regularizacion/consultadictamenesregjs');
    }

    public function consulta_graficacionr()
    {
        echo view('header', ['titulo' => 'Bitacora de Envios de Regularización a Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('inspeccion/regularizacion/consultardictamenesgraf');
        echo view('footer');        
        echo view('inspeccion/regularizacion/consultardictamenesgrafjs');
    }

    public function reporte_movimientos_regularizacion()
    {
        echo view('header', ['titulo' => 'Reportes Inspeccion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('inspeccion/reportes/resumen_inspeccion_regularizacion');
        echo view('footer');        
        echo view('inspeccion/reportes/resumen_inspeccion_regularizacion_js');
    }

    public function reporte_inspeccion_brigada_regularizacion()
    {
        echo view('header', ['titulo' => 'Reportes Inspeccion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('inspeccion/reportes/resumen_inspeccion_regu_brigada');
        echo view('footer');        
        echo view('inspeccion/reportes/resumen_inspeccion_regu_brigada_js');
    }

    public function reporte_inspeccion_movimiento_regularizacion()
    {
        echo view('header', ['titulo' => 'Reportes Inspeccion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('inspeccion/reportes/resumen_inspeccion_por_concepto_regularizacion');
        echo view('footer');        
        echo view('inspeccion/reportes/resumen_inspeccion_por_concepto_regularizacion_js');
    }

    public function reporte_inspeccion_movimiento_brigada_regularizacion()
    {
        echo view('header', ['titulo' => 'Reportes Inspeccion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('inspeccion/reportes/reporte_inspeccion_movimiento_brigada_regularizacion');
        echo view('footer');        
        echo view('inspeccion/reportes/reporte_inspeccion_movimiento_brigada_regularizacion_js');
    }

    public function reporte_movimientos_ventanilla()
    {
        echo view('header', ['titulo' => 'Reportes Inspeccion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('inspeccion/reportes/reporte_movimientos_ventanilla');
        echo view('footer');        
        echo view('inspeccion/reportes/reporte_movimientos_ventanilla_js');
    }

    public function reporte_inspeccion_movimiento_ventanilla()
    {
        echo view('header', ['titulo' => 'Reportes Inspeccion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('inspeccion/reportes/reporte_inspeccion_movimiento_ventanilla');
        echo view('footer');        
        echo view('inspeccion/reportes/reporte_inspeccion_movimiento_ventanilla_js');
    }

    public function reporte_inspeccion_movimiento_brigada_ventanilla()
    {
        echo view('header', ['titulo' => 'Reportes Inspeccion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('inspeccion/reportes/reporte_inspeccion_movimiento_brigada_ventanilla');
        echo view('footer');        
        echo view('inspeccion/reportes/reporte_inspeccion_movimiento_brigada_ventanilla_js');
    }
    
    
    //reportes
    public function get_reporte_movimientos_regularizacion($mov1 = null, $mov2 = null, $f1 = null, $f2 = null){
        $data['data'] = $this->inspeccionModel->reporte_movimientos_regularizacion($mov1, $mov2, $f1, $f2);
        return $this->respond($data);
    }
    
    public function get_reporte_movimientos_por_brigada_regularizacion($b1 = null, $b2 = null){
        $data['data'] = $this->inspeccionModel->reporte_movimientos_por_brigada_regularizacion($b1, $b2);
        return $this->respond($data);
    }

    public function get_reporte_movimientos_por_concepto_regularizacion($b1 = null, $b2 = null, $f1 = null, $f2 = null){
        //echo $this->inspeccionModel->reporte_movimientos_por_concepto_regularizacion($b1, $b2, $f1, $f2);
        $data['data'] = $this->inspeccionModel->reporte_movimientos_por_concepto_regularizacion($b1, $b2, $f1, $f2);
        return $this->respond($data);
    }

    public function get_reporte_movimientos_por_concepto_brigada_regularizacion($b1 = null, $b2 = null, $f1 = null, $f2 = null){
        $data['data'] = $this->inspeccionModel->reporte_movimientos_por_concepto_brigada_regularizacion($b1, $b2, $f1, $f2);
        return $this->respond($data);
    }

    public function get_reporte_movimientos_ventanilla($f1 = null, $f2 = null){
        $data['data'] = $this->inspeccionModel->reporte_movimientos_ventanilla($f1, $f2);
        return $this->respond($data);
    }

    public function get_reporte_movimientos_por_concepto_ventanilla($f1 = null, $f2 = null){
        $data['data'] = $this->inspeccionModel->reporte_movimientos_por_concepto_ventanilla($f1, $f2);
        return $this->respond($data);
    }

    public function get_reporte_movimientos_por_concepto_brigada_ventanilla($b1 = null, $b2 = null, $f1 = null, $f2 = null){
        $data['data'] = $this->inspeccionModel->reporte_movimientos_por_concepto_brigada_ventanilla($b1, $b2, $f1, $f2);
        return $this->respond($data);
    }

    

}
