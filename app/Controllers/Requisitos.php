<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
//incluimos el modelo de los tramites
use App\Models\RequisitosModel;
use CodeIgniter\API\ResponseTrait;


class Requisitos extends Controller
{

    use ResponseTrait;

    public function __construct() {

		$db = db_connect();
		$this->Model = new RequisitosModel($db);
	}

    public function index()
    {    
        $data['result']	= $this->Model->listar();
        return $this->respond($data);
    }    

    public function get($id = null)
    {    
        $data['result']	= $this->Model->get($id);
        return $this->respond($data);
    }   
 
 
}
