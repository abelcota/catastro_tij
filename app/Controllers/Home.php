<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\PadronModel;

class Home extends BaseController
{
	use ResponseTrait;

	public function __construct() {

		$db = db_connect();
		$this->padronModel = new PadronModel($db);
	}

	public function index()
	{
			$rol = $this->padronModel->get_user_rol();
			
			//return $this->respond($data);
			$data = [
				'usuario' => user(),
				'rol' => $rol,
			];

			echo view('header', ['titulo' => 'Inicio']);
			echo view('menu');
			echo view('topbar', $data);
			echo view('home', $data);
			echo view('footer');
				
	}

	public function perfil()
	{
			$rol = $this->padronModel->get_user_rol();
			
			//return $this->respond($data);
			$data = [
				'usuario' => user(),
				'rol' => $rol,
			];

			echo view('header', ['titulo' => 'Perfil de Usuario']);
			echo view('menu');
			echo view('topbar', ['usuario' => user()]);
			echo view('perfil', $data);
			echo view('footer');
			echo view('perfiljs');
				
	}

	//--------------------------------------------------------------------

}
