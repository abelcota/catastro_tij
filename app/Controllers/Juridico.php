<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\API\ResponseTrait;
use App\Models\JuridicoModel;

class Juridico extends Controller
{
    use \Myth\Auth\AuthTrait;
    use ResponseTrait;

    public function __construct()
	{      
        // Reestringir el acceso a los usuarios que no esten en el rol de Inspeccion o Administracion
        $this->restrictToGroups(['Administracion', 'Seguimiento']);
        $db = db_connect();
		$this->juridicoModel = new JuridicoModel($db);
    }

    public function index()
    {    
        echo "Juridico";
    }

    public function envios_ices()
    {    
        echo view('header');
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('juridico/isai.php');
        echo view('footer');        
        echo view('juridico/isaijs.php');
    }

    public function bitacoras()
    {    
        echo view('header');
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('juridico/noticias_cambios.php');
        echo view('footer');        
        echo view('juridico/noticias_cambiosjs.php');
    }

    ///Funciones del apartado de envio a ices noticias de cambios

    public function get_bitacoras_noticias_cambios(){
        $data = $this->juridicoModel->get_bitacoras_noticias_cambios(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_noticiascambios_detalle_sinbitacora(){
        $data = $this->juridicoModel->get_noticiascambios_detalle_sinbitacora(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function genera_bitacora_noticias_cambios($a = null){
        $data['result'] = $this->juridicoModel->genera_bitacora_noticias_cambios($a); 
        return $this->respond($data);
    }

    public function registrar_bitacora_noticia_detalle(){
        helper(['form', 'url']);

        $data = [
            'Area' => $this->request->getVar('area'),
            'NumeroOficio' => $this->request->getVar('numeroof'),
            'FechaOficio' => $this->request->getVar('fechadet'),
            'Asunto' => $this->request->getVar('asunto'),
            'Tramite' => $this->request->getVar('tramite'),
            'Solucion' => $this->request->getVar('solucion'),
            'Observaciones' => $this->request->getVar('observaciones'),
        ];
        $save = $this->juridicoModel->registrar_bitacora_noticia_detalle($data);     
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function registrar_bitacora_noticia(){
        helper(['form', 'url']);
        $data = [
            'AnioBitacora' => $this->request->getVar('Anio'),
            'IdBitacora' => $this->request->getVar('Bitacora'),
            'Fecha' => $this->request->getVar('FechaEnv'),
            'Folios' => $this->request->getVar('Folios'),
            'Empleado' => $this->request->getVar('Empleado'),
        ];
        $save = $this->juridicoModel->registrar_bitacora_noticia($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }


    //Funciones del apartado de envio ices tramites de isai
    public function get_envios_ices(){
        $data = $this->juridicoModel->get_envios_ices(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_isiai_detalle_sinbitacora(){
        $data = $this->juridicoModel->get_isiai_detalle_sinbitacora(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_propietario($cve = null){
        $data['result'] = $this->juridicoModel->get_propietario($cve); 
        return $this->respond($data);
    }

    public function genera_bitacora(){
        $data['result'] = $this->juridicoModel->genera_bitacora(); 
        return $this->respond($data);
    }

    public function registrar_bitacora_envio_ices(){
        helper(['form', 'url']);
        $data = [
            'Bitacora' => $this->request->getVar('Bitacora'),
            'FechaEnv' => $this->request->getVar('FechaEnv'),
            'Folios' => $this->request->getVar('Folios'),
            'Empleado' => $this->request->getVar('Empleado'),
        ];

        $save = $this->juridicoModel->registrar_bitacora_envio_ices($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function registrar_envio_ices_detalle(){
        helper(['form', 'url']);

        $clave = $this->request->getVar('POBL').$this->request->getVar('CTEL').$this->request->getVar('MANZ').$this->request->getVar('PRED').$this->request->getVar('UNID');

        $data = [
            'Pobl' => $this->request->getVar('POBL'),
            'Ctel' => $this->request->getVar('CTEL'),
            'Manz' => $this->request->getVar('MANZ'),
            'Pred' => $this->request->getVar('PRED'),
            'Unid' => $this->request->getVar('UNID'),
            'Clave' => $clave,
            'Propietario' => $this->request->getVar('propietario'),
            'Adquiriente' => $this->request->getVar('adquiriente'),
        ];
        $save = $this->juridicoModel->registrar_envio_ices_detalle($data);     
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function get_empleados(){
        $data['result'] = $this->juridicoModel->get_empleados(); 
        return $this->respond($data);
    }
    

}