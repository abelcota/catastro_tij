<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\API\ResponseTrait;
use App\Models\GraficacionModel;

class Graficacion extends Controller
{
    use \Myth\Auth\AuthTrait;
    use ResponseTrait;

    public function __construct()
	{      
        // Reestringir el acceso a los usuarios que no esten en el rol de Graficacion o Administracion
        $this->restrictToGroups(['Administracion', 'Graficacion']);
        $db = db_connect();
		$this->graficacionionModel = new GraficacionModel($db);
    }
    
    public function index()
    {    
        echo "Graficacion";
    }
    
    public function asignacion_trabajo()
    {
        echo view('header', ['titulo' => 'Asignación de Trabajo a Graficadores']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/ventanilla/asignacion_trabajo');
        echo view('footer');
        echo view('graficacion/ventanilla/asignacion_trabajojs');        
    }

    public function dictamenes_regularizacion()
    {
        echo view('header', ['titulo' => 'Bitacora Dictamenes Regularizacion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/regularizacion/consultadictamenes');
        echo view('footer');        
        echo view('graficacion/regularizacion/consultadictamenesjs');
    }

    public function consulta_ventanilla()
    {
        echo view('header', ['titulo' => 'Bitacora Dictamenes Ventanilla']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/ventanilla/consultar_graficaciones');
        echo view('footer');        
        echo view('graficacion/ventanilla/consultar_graficacionesjs');
    }

    public function movimientos_regularizacion_ventanilla()
    {
        echo view('header', ['titulo' => 'Bitacora Dictamenes Regularizacion']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/regularizacion/movimientos');
        echo view('footer');        
        echo view('graficacion/regularizacion/movimientosjs');
    }


    public function get_ventanilla_graficacion(){
        $data = $this->graficacionionModel->get_ventanilla_graficacion();
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_dictamenes_regularizacion(){
        $data = $this->graficacionionModel->get_dictamenes_regularizacion();
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_folios_sin_graficador(){
        $data = $this->graficacionionModel->get_folios_sin_graficador();
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_resultados_graficacion()
    {
        $data['results'] = $this->graficacionionModel->get_resultados_graficacion();
        return $this->respond($data);
    }

    public function get_graficadores(){
        $data['results'] = $this->graficacionionModel->get_graficadores();
        return $this->respond($data);
    }

    public function get_movimientos_graficacion(){
        $data['results'] = $this->graficacionionModel->get_movimientos_graficacion();
        return $this->respond($data);
    }

    public function consultar_folio($a = null, $fol = null)
    {
        $data['results'] = $this->graficacionionModel->consultar_folio($a, $fol);
        return $this->respond($data);
    }

    public function consultar_folio_regularizacion($a = null, $fol = null){
        $data['results'] = $this->graficacionionModel->consultar_folio_regularizacion($a, $fol);
        return $this->respond($data);
    }

    public function get_brigadas_inspeccion(){
        $data['results'] = $this->graficacionionModel->get_brigadas_inspeccion();
        return $this->respond($data);
    }

    //TODO interfaz
    public function consultar_movimientos_reg_ventanilla(){
        $data['data'] = $this->graficacionionModel->consultar_movimientos_reg_ventanilla();
        return $this->respond($data);
    }

    public function capturar_graficacion_ventanilla(){
        helper(['form', 'url']);
        $data = [
            'FAtendido' => $this->request->getVar('dtfecha'),
            'Graficador'  => $this->request->getVar('graficador'),
            'Movimiento'  => $this->request->getVar('movimiento'),
            'Resultado'  => $this->request->getVar('resultado'),
            'Dictamen'  => $this->request->getVar('dictamen'),
            'Observaciones'  => $this->request->getVar('observaciones'),
            'Anio'  => $this->request->getVar('year'),
            'Folio'  => $this->request->getVar('folio')
        ];
        $save = $this->graficacionionModel->capturar_graficacion_ventanilla($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function capturar_graficacion_regularizacion(){
        helper(['form', 'url']);
        $data = [
            'FAtendido' => $this->request->getVar('dtfecha'),
            'Graficador'  => $this->request->getVar('graficador'),
            'Movimiento'  => $this->request->getVar('movimiento'),
            'Resultado'  => $this->request->getVar('resultado'),
            'Dictamen'  => $this->request->getVar('dictamen'),
            'Observaciones'  => $this->request->getVar('observaciones'),
            'Anio'  => $this->request->getVar('year'),
            'Folio'  => $this->request->getVar('folio')
        ];
        $save = $this->graficacionionModel->capturar_graficacion_regularizacion($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function asignar_graficador(){
        helper(['form', 'url']);
        $data = [
            'Graficador' => $this->request->getVar('graficador'),
            'Anio'  => $this->request->getVar('anio'),
            'Folio'  => $this->request->getVar('folio')
        ];
        $save = $this->graficacionionModel->asignar_graficador($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
        
    }
    
    public function get_empleados(){
        $data['result'] = $this->graficacionionModel->get_empleados(); 
        return $this->respond($data);
    }
    

    //APARTADO DE REPORTES

    public function reporte_movimientos_ventanilla()
    {
        echo view('header', ['titulo' => 'Reportes Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/reportes/resumen_movimientos_ventanilla');
        echo view('footer');        
        echo view('graficacion/reportes/resumen_movimientos_ventanilla_js');
    }

    public function get_reporte_movimientos_ventanilla($f1 = null, $f2 = null){
        //echo $this->graficacionionModel->reporte_movimientos_ventanilla($f1, $f2);
        $data['data'] = $this->graficacionionModel->reporte_movimientos_ventanilla($f1, $f2);
        return $this->respond($data);
    }

    public function reporte_movimientos_graficador_ventanilla()
    {
        echo view('header', ['titulo' => 'Reportes Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/reportes/reporte_movimientos_graficador_ventanilla');
        echo view('footer');        
        echo view('graficacion/reportes/reporte_movimientos_graficador_ventanilla_js');
    }

    public function get_reporte_movimientos_graficador_ventanilla($f1 = null, $f2 = null){
        //echo $this->graficacionionModel->reporte_movimientos_ventanilla($f1, $f2);
        $data['data'] = $this->graficacionionModel->reporte_movimientos_graficador_ventanilla($f1, $f2);
        return $this->respond($data);
    }

    public function reporte_tramites_detenidos()
    {
        echo view('header', ['titulo' => 'Reportes Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/reportes/reporte_tramites_detenidos');
        echo view('footer');        
        echo view('graficacion/reportes/reporte_tramites_detenidos_js');
    }

    public function get_reporte_tramites_detenidos($f1 = null, $f2 = null){
        //echo $this->graficacionionModel->reporte_tramites_detenidos($f1, $f2);
        $data['data'] = $this->graficacionionModel->reporte_tramites_detenidos($f1, $f2);
        return $this->respond($data);
    }

    public function reporte_tramites_enviados_ices()
    {
        echo view('header', ['titulo' => 'Reportes Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/reportes/reporte_tramites_enviados_ices_ventanilla');
        echo view('footer');        
        echo view('graficacion/reportes/reporte_tramites_enviados_ices_ventanilla_js');
    }

    public function get_reporte_tramites_enviados_ices($tram = null, $f1 = null, $f2 = null){
        //echo $this->graficacionionModel->reporte_tramites_detenidos($f1, $f2);
        $data['data'] = $this->graficacionionModel->reporte_tramites_enviados_ices($tram ,$f1, $f2);
        return $this->respond($data);
    }

    public function reporte_oficios_sin_folio()
    {
        echo view('header', ['titulo' => 'Reportes Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/reportes/reporte_oficios_sin_folio');
        echo view('footer');        
        echo view('graficacion/reportes/reporte_oficios_sin_folio_js');
    }
    
    public function get_reporte_oficios_sin_folio($f1 = null, $f2 = null){
        $data['data'] = $this->graficacionionModel->reporte_oficios_sin_folio($f1, $f2);
        return $this->respond($data);
    }

    //REPORTES DE REGULARIZACION
    public function reporte_movimientos_regularizacion()
    {
        echo view('header', ['titulo' => 'Reportes Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/reportes/resumen_movimientos_regularizacion');
        echo view('footer');        
        echo view('graficacion/reportes/resumen_movimientos_regularizacion_js');
    }

    public function get_reporte_movimientos_regularizacion($f1 = null, $f2 = null){
        $data['data'] = $this->graficacionionModel->reporte_movimientos_regularizacion($f1, $f2);
        return $this->respond($data);
    }

    public function reporte_movimientos_graficador_regularizacion()
    {
        echo view('header', ['titulo' => 'Reportes Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/reportes/reporte_movimientos_graficador_regularizacion');
        echo view('footer');        
        echo view('graficacion/reportes/reporte_movimientos_graficador_regularizacion_js');
    }

    public function get_reporte_movimientos_graficador_regularizacion($f1 = null, $f2 = null){
        //echo $this->graficacionionModel->reporte_movimientos_ventanilla($f1, $f2);
        $data['data'] = $this->graficacionionModel->reporte_movimientos_graficador_regularizacion($f1, $f2);
        return $this->respond($data);
    }

    public function reporte_resumen_tipo_resultado_regularizacion()
    {
        echo view('header', ['titulo' => 'Reportes Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/reportes/reporte_resumen_tipo_resultado_regularizacion');
        echo view('footer');        
        echo view('graficacion/reportes/reporte_resumen_tipo_resultado_regularizacion_js');
    }

    public function get_reporte_resumen_tipo_resultado_regularizacion($b1 = null, $b2 = null, $f1 = null, $f2 = null){
        //echo $this->graficacionionModel->reporte_movimientos_ventanilla($f1, $f2);
        $data['data'] = $this->graficacionionModel->reporte_resumen_tipo_resultado_regularizacion($b1, $b2,$f1, $f2);
        return $this->respond($data);
    }

    public function reporte_regularizacion_enviada_ices_con_respuesta()
    {
        echo view('header', ['titulo' => 'Reportes Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/reportes/reporte_regularizacion_enviada_ices_con_respuesta');
        echo view('footer');        
        echo view('graficacion/reportes/reporte_regularizacion_enviada_ices_con_respuesta_js');
    }

    public function get_reporte_regularizacion_enviada_ices_con_respuesta($f1 = null, $f2 = null){
        //echo $this->graficacionionModel->reporte_movimientos_ventanilla($f1, $f2);
        $data['data'] = $this->graficacionionModel->reporte_regularizacion_enviada_ices_con_respuesta($f1, $f2);
        return $this->respond($data);
    }

    public function reporte_regularizacion_enviada_ices_sin_respuesta()
    {
        echo view('header', ['titulo' => 'Reportes Graficación']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('graficacion/reportes/reporte_regularizacion_enviada_ices_sin_respuesta');
        echo view('footer');        
        echo view('graficacion/reportes/reporte_regularizacion_enviada_ices_sin_respuesta_js');
    }

    public function get_reporte_regularizacion_enviada_ices_sin_respuesta($f1 = null, $f2 = null){
        //echo $this->graficacionionModel->reporte_movimientos_ventanilla($f1, $f2);
        $data['data'] = $this->graficacionionModel->reporte_regularizacion_enviada_ices_sin_respuesta($f1, $f2);
        return $this->respond($data);
    }
    

}