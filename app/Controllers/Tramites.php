<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
//incluimos el modelo de los tramites
use App\Models\TramitesModel;
use App\Models\PadronModel;

class Tramites extends Controller
{
    public function index()
    {    
        $model = new TramitesModel();
 
        $data['tramites'] = $model->orderBy('cod_tramite', 'DESC')->findAll();
        
        return view('tramites/tramites', $data);
    }    

    public function rest()
    {    
        $model = new TramitesModel();
        $data['tramites'] = $model->orderBy('cod_tramite', 'DESC')->findAll();
        return view('tramites/get-tramites-rest', $data);
    }
 
    public function crear()
    {    
        return view('tramites/create-tramite');
    }
    
    public function validar($id = null)
    {    

        return view('tramites/validar-clave');
    }

    public function guardar()
    {  
 
        helper(['form', 'url']);
         
        $model = new TramitesModel();
 
        $data = [
            'cod_tramite' => $this->request->getVar('cod_tramite'),
            'descripcion'  => $this->request->getVar('descripcion'),
            ];
 
        $save = $model->insert($data);
 
        return redirect()->to( base_url('public/index.php/tramites') );
    }
 
    public function editar($id = null)
    {
      
     $model = new TramitesModel();
 
     $data['tramites'] = $model->where('cod_tramite', $id)->first();
 
     return view('tramites/edit-tramite', $data);
    }
 
    public function actualizar()
    {  
 
        helper(['form', 'url']);
         
        $model = new TramitesModel();
 
        $id = $this->request->getVar('cod_tramite');
 
        $data = [
             'descripcion'  => $this->request->getVar('descripcion'),
            ];
 
        $save = $model->update($id,$data);
 
        return redirect()->to( base_url('public/index.php/tramites') );
    }
 
    public function eliminar($id = null)
    {
 
     $model = new TramitesModel();
 
     $data['tramites'] = $model->where('cod_tramite', $id)->delete();
      
     return redirect()->to( base_url('public/index.php/tramites') );
    }
}
