<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;
//incluimos el modelo de los Usuarios
use App\Models\UsuariosModel;
use Myth\Auth\Entities\User;
use Myth\Auth\Models\UserModel;
//incluimos el modelo del Padron
use App\Models\PadronModel;
//incluimos el modelo Administrativo
use App\Models\AdministrativoModel;

use CodeIgniter\API\ResponseTrait;

class Administrativo extends Controller
{    
    use \Myth\Auth\AuthTrait;
    use ResponseTrait;

    protected $auth;
	/**
	 * @var Auth
	 */
	protected $config;

	/**
	 * @var \CodeIgniter\Session\Session
	 */
    protected $session;  
    

	public function __construct()
	{
		// Most services in this controller require
		// the session to be started - so fire it up!
		$this->session = service('session');

		$this->config = config('Auth');
        $this->auth = service('authentication');
        
        // Reestringir el acceso a los usuarios que no esten en el rol de Administracion
        $this->restrictToGroups('Administracion');

        $db = db_connect();
        $this->padronModel = new PadronModel($db);
		$this->administrativoModel = new AdministrativoModel($db);

    }
    
    public function index()
    {    
        echo "administrativo";
    }

    public function roles()
    {    
        echo view('header', ['titulo' => 'Administracion de Roles']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/roles/index');
        echo view('footer');
        echo view('administrativo/roles/jsindex');
    }

    public function asignar_rol_usuario()
    { 
        helper(['form', 'url']);
        $userdId = $this->request->getVar('aid');
        $rol = $this->request->getVar('aroles');
        $save = $this->padronModel->asignar_rol($rol, $userdId);
        //echo $save;
        if ($save){
            echo "Rol Asignado correctamente";
        }
        else{
            echo "Error al asignar el rol";
        }
    }
    
    public function usuarios()
    {
        $model = new UsuariosModel();
        $data['usuarios'] = $model->orderBy('id', 'DESC')->findAll();
        echo view('header', ['titulo' => 'Administracion de Usuarios']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/usuarios/index', $data);
        echo view('footer');
        echo view('administrativo/usuarios/indexjs');
    }

    public function registrar_usuario()
	{
        helper(['form', 'url']);
		// Check if registration is allowed
		if (! $this->config->allowRegistration)
		{
			echo lang('Auth.registerDisabled');
		}

		$users = model(UserModel::class);

		// Validate here first, since some things,
		// like the password, can only be validated properly here.
		$rules = [
			'username'  	=> 'required|alpha_numeric_space|min_length[3]|is_unique[users.username]',
			'email'			=> 'required|valid_email|is_unique[users.email]',
			'password'	 	=> 'required|strong_password',
			'pass_confirm' 	=> 'required|matches[password]',
		];

		if (! $this->validate($rules))
		{
            $errores = "";
            foreach(service('validation')->getErrors() as $err){
                $errores .= " ".$err;
            }
			return $errores;
		}

		// Save the user
		$allowedPostFields = array_merge(['password'], $this->config->validFields, $this->config->personalFields);
		$user = new User($this->request->getPost($allowedPostFields));

		$this->config->requireActivation !== false ? $user->generateActivateHash() : $user->activate();
        if (! empty($this->request->getVar('roles'))) {
            $users = $users->withGroup($this->request->getVar('roles'));
        }
        else if (! empty($this->config->defaultUserGroup)) {
            $users = $users->withGroup($this->config->defaultUserGroup);
        }

		if (! $users->save($user))
		{
            $errores = "";
            foreach(lang($users->errors()) as $err){
                $errores .= " ".$err;
            }
			return $errores;
        }
        else{
            // Success!
		    return lang('Auth.registerSuccess');
        }

		if ($this->config->requireActivation !== false)
		{
			$activator = service('activator');
			$sent = $activator->send($user);

			if (! $sent)
			{
				return $activator->error() ?? lang('Auth.unknownError');
			}

			// Success!
			return lang('Auth.activationSuccess');
		}

		
    }
    
    public function cuarteles()
    {
        echo view('header', ['titulo' => 'Catálogo de Calles']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/cuarteles');
        echo view('footer');
        echo view('administrativo/catalogos/cuartelesjs');
    }

    public function calles()
    {
        echo view('header', ['titulo' => 'Catálogo de Calles']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/calles');
        echo view('footer');
        echo view('administrativo/catalogos/callesjs');
    }

    public function colonias()
    {
        echo view('header', ['titulo' => 'Catálogo de Colonias']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/colonias');
        echo view('footer');
        echo view('administrativo/catalogos/coloniasjs');
    }

    public function poblaciones()
    {
        echo view('header', ['titulo' => 'Catálogo de Poblaciones']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/poblaciones');
        echo view('footer');
        echo view('administrativo/catalogos/poblacionesjs');
    }

    public function manzanas()
    {
        echo view('header', ['titulo' => 'Catálogo de Manzanas']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/manzanas');
        echo view('footer');
        echo view('administrativo/catalogos/manzanasjs');
    }

    public function zonas()
    {
        echo view('header', ['titulo' => 'Catálogo de Zonas']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/zonas');
        echo view('footer');
        echo view('administrativo/catalogos/zonasjs');
    }

    public function propietarios()
    {
        echo view('header', ['titulo' => 'Catálogo de Propietarios']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/propietarios');
        echo view('footer');
        echo view('administrativo/catalogos/propietariosjs');
    }

    public function categorias_const()
    {
        echo view('header', ['titulo' => 'Catálogo de Categorías de Construcción']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/cat_mp');
        echo view('footer');
        echo view('administrativo/catalogos/cat_mpjs');
    }

    public function tramos()
    {
        echo view('header', ['titulo' => 'Catálogo de Tramos']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/tramos');
        echo view('footer');
        echo view('administrativo/catalogos/tramosjs');
    }

    public function terrenos()
    {
        echo view('header', ['titulo' => 'Catálogo de Terrenos']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/terrenos');
        echo view('footer');
        echo view('administrativo/catalogos/terrenosjs');
    }

    public function construcciones()
    {
        echo view('header', ['titulo' => 'Catálogo de Construcciones']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/construcciones');
        echo view('footer');
        echo view('administrativo/catalogos/construccionesjs');
    }

    public function demt()
    {
        echo view('header', ['titulo' => 'Catálogo de Demeritos de Terreno']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/demt');
        echo view('footer');
        echo view('administrativo/catalogos/demtjs');
    }

    public function demc()
    {
        echo view('header', ['titulo' => 'Catálogo de Demeritos de Construccion']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/demc');
        echo view('footer');
        echo view('administrativo/catalogos/demcjs');
    }

    public function notarios()
    {
        echo view('header', ['titulo' => 'Catálogo de Notarios']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/catalogos/notarios');
        echo view('footer');
        echo view('administrativo/catalogos/notariosjs');
    }

    public function efirma()
    {
        echo view('header', ['titulo' => 'Administracion de Firma Electronica']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/efirma/index');
        echo view('footer');
        echo view('administrativo/efirma/indexjs');
    }

    public function get_envios_inspeccion(){
        $data = $this->administrativoModel->get_envios_inspeccion(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_envios_graficacion(){
        $data = $this->administrativoModel->get_envios_graficacion(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function genera_bitacora_inspeccion(){
        $data['result'] = $this->padronModel->genera_bitacora_inspeccion(); 
        return $this->respond($data);
    }

    //registrar el paquete de envio a inspeccion
    public function registrar_bitacora_inspeccion(){ 
        helper(['form', 'url']);
        $data = [
            'Bitacora' => $this->request->getVar('Bitacora'),
            'FechaEnv' => $this->request->getVar('FechaEnv'),
            'Envia' => $this->request->getVar('Envia'),
            'Recibe' => $this->request->getVar('Recibe'),
            'Folios' => $this->request->getVar('Folios'),
        ];

        $save = $this->administrativoModel->registrar_bitacora_inspeccion($data);
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    //registrar el paquete de envio a inspeccion
    public function registrar_bitacora_graficacion(){ 
        helper(['form', 'url']);
        $data = [
            'Bitacora' => $this->request->getVar('Bitacora'),
            'FechaEnv' => $this->request->getVar('FechaEnv'),
            'Origen' => $this->request->getVar('Origen'),
            'Envia' => $this->request->getVar('Envia'),
            'Recibe' => $this->request->getVar('Recibe'),
            'Folios' => $this->request->getVar('Folios'),
        ];

        $save = $this->administrativoModel->registrar_bitacora_graficacion($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function empleados(){
        echo view('header', ['titulo' => 'Administración de Empleados']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/empleados/index');
        echo view('footer');
        echo view('administrativo/empleados/indexjs');
    }

   
    public function envinspeccion(){
        echo view('header', ['titulo' => 'Administración de Envios a Inspección']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/enviosinspeccion');
        echo view('footer');
        echo view('administrativo/enviosinspeccionjs');
    }

    public function avaluos(){
        echo view('header', ['titulo' => 'Administración de Avaluos']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/avaluomanual');
        echo view('footer');
        echo view('administrativo/mapjs');
        echo view('administrativo/avaluomanualjs');
    }

    public function cedulacatastral(){
        echo view('header', ['titulo' => 'Consulta de Cedula Catastral']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/cedulacatastral');
        echo view('footer');
        echo view('administrativo/mapjs');
        echo view('administrativo/cedulacatastraljs');
    }

    public function verificacion(){
        echo view('header', ['titulo' => 'Administración de Verificación de Trámites']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/verificacion_tramites');
        echo view('footer');
        echo view('administrativo/mapjs'); 
        echo view('administrativo/verificacion_tramitesjs');
   
    }

    public function obtener_efirma(){
        $dir = ROOTPATH.'public/assets/firma';
        $results['result'] = array();
        $files = scandir($dir);
        foreach($files as $key => $value){
            $path = $dir.DIRECTORY_SEPARATOR.$value;
            if(!is_dir($path)) {
                array_push($results['result'], ['nombre'=> $value,'size'=>filesize($path), 'ext'=>pathinfo($path, PATHINFO_EXTENSION)]);
            } else if($value != "." && $value != "..") {
                if(!is_dir($path)) { 
                    getDirContents($path, $results);
                    $results['result'] = ['path'=>$path,'size'=>filesize($path), 'ext'=>pathinfo($path, PATHINFO_EXTENSION)];
                }
            }
        }

        $results['data'] = $this->administrativoModel->get_firma_data();
        
        return $this->respond($results);
    }

    public function subir_archivos_firma()
    {
        helper(['form', 'url']);
        $data = [
            'passwd' => $this->request->getVar('txt_password'),
            'director' => $this->request->getVar('txt_director'),

        ];
        //subir los archivos a la carpeta
        $directorio = ROOTPATH.'public/assets/firma';
                
        if($archivos = $this->request->getFiles())
        {
            foreach($archivos['archivos'] as $arch)
            {
                if ($arch->isValid() && ! $arch->hasMoved())
                {
                    $nombre = $arch->getName();
                    ;
                    //Validamos si la ruta de destino existe, en caso de no existir la creamos
                   
                    if(!file_exists($directorio)){
                        mkdir($directorio, 0775) or die("No se puede crear el directorio de extracci&oacute;n");	
                    }
                    $arch->move($directorio, $nombre);
                    
                    $this->administrativoModel->registrar_doc_firma($nombre);

                }
            }
            
            echo "ok";
            $this->administrativoModel->registrar_datos_firma($data);
        }
        else{
            echo "error";
        }
    }


    public function eliminar_documento_cer($a = null, $id=null, $doc = null)
    {
        $directorio = ROOTPATH.'public/assets/firma';

        $file = $directorio."/".$doc;
      
        if (is_readable($file) && unlink($file)) {
            echo "Archivo eliminado correctamente";
        } else {
            echo "El archivo $file no fue encontrado o no tiene permisos para eliminarlo";
        }

        $results['data'] = $this->administrativoModel->eliminar_doc_firma($doc);


    }


    public function cargar_documentacion($clave = null){
        $dir = ROOTPATH.'public/documentos/'.$clave;
        
        $files = scandir($dir);
        foreach($files as $key => $value){
            $path = $dir.DIRECTORY_SEPARATOR.$value;
            if(!is_dir($path)) {
                $results['result'] = ['nombre'=> $value,'size'=>filesize($path), 'ext'=>pathinfo($path, PATHINFO_EXTENSION)];
            } else if($value != "." && $value != "..") {
                getDirContents($path, $results);
                $results['result'] = ['path'=>$path,'size'=>filesize($path), 'ext'=>pathinfo($path, PATHINFO_EXTENSION)];
            }
        }
        
        return $this->respond($results);
    }


    public function get_notin_inspeccion(){
        $data = $this->administrativoModel->get_notin_inspeccion(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_notin_graficacion(){
        $data = $this->administrativoModel->get_notin_graficacion(); 
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }
    

    public function genera_bitacora_graficacion(){
        $data['result'] = $this->padronModel->genera_bitacora_graficacion(); 
        return $this->respond($data);
    }

    public function envgraficacion(){
        echo view('header', ['titulo' => 'Administración de Envios a Graficación']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('administrativo/enviosgraficacion');
        echo view('footer');
        echo view('administrativo/enviosgraficacionjs');
    }

    public function get_empleados(){
        $data['result'] = $this->administrativoModel->get_empleados(); 
        return $this->respond($data);
    }

    public function get_empleados2(){
        $data['data'] = $this->administrativoModel->get_empleados(); 
        return $this->respond($data);
    }

    public function email(){
        $email = \Config\Services::email();
        $email->setFrom('noreply@catastro.com', 'Sistema Catastro Culiacan');
        $email->setTo('abel.dex@gmail.com');

        $email->setSubject('Email Test');
        $email->setMessage('Testing the email class.');

        $email->send(false);
        echo $email->printDebugger(['headers']);

     }

     public function registrar_empleado()
    { 
        helper(['form', 'url']);
        $data = [
            'idemp' => $this->request->getVar('idemp'),
            'nombre' => $this->request->getVar('nombre'),
            'apellidopat' => $this->request->getVar('apellidopat'),
            'apellidomat' => $this->request->getVar('apellidomat'),
            'puesto' => $this->request->getVar('puesto'),
        ];

        $save = $this->administrativoModel->registrar_empleado($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function eliminar_usuario($id =  null)
    {   
        helper(['form', 'url']);
        $id = $this->request->getVar('id');

        $save = $this->administrativoModel->eliminar_usuario($id);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }


    public function eliminar_empleado($id =  null)
    {   
        helper(['form', 'url']);
        $id = $this->request->getVar('id');

        $save = $this->administrativoModel->eliminar_empleado($id);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function actualizar_empleado()
    { 
        helper(['form', 'url']);
        $data = [
            'idemp' => $this->request->getVar('idemp'),
            'idemp2' => $this->request->getVar('idemp2'),
            'nombre' => $this->request->getVar('nombre'),
            'apellidopat' => $this->request->getVar('apellidopat'),
            'apellidomat' => $this->request->getVar('apellidomat'),
            'puesto' => $this->request->getVar('puesto'),
        ];

        $save = $this->administrativoModel->actualizar_empleado($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }
    }

    public function actualizar_privilegio($priv = null, $id = null){
        if($priv == null && $id == null){
            echo "Por favor ingrese un valor para el privilegio del visor";
        }
        else{
            $save = $this->administrativoModel->actualizar_permiso_visor($priv, $id);
            //echo $save;
            if($save){
                 echo 'ok';
            }
            else{
                    echo 'error';
            }
        }
        
    }
         

}