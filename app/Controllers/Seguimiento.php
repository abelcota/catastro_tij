<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\API\ResponseTrait;
use App\Models\SeguimientoModel;

class Seguimiento extends Controller
{
    use \Myth\Auth\AuthTrait;
    use ResponseTrait;

    public function __construct()
	{
        // Reestringir el acceso a los usuarios que no esten en el rol de Administracion
        $this->restrictToGroups(['Administracion', 'Ventanilla', 'Seguimiento Ventanilla', 'Seguimiento']);
        $db = db_connect();
		$this->seguimientoModel = new SeguimientoModel($db);

    }

    public function index()
    {    
        echo "Seguimiento";
    }
    
    public function ventanilla()
    {
        echo view('header', ['titulo' => 'Seguimiento Tramites Ventanilla']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/ventanilla');
        echo view('footer');
        echo view('seguimiento/segventanillajs');
    }

    public function ventanilla_seguimiento()
    {
        $data = [
            'draw'          => $_REQUEST['draw'],
            'start'         => $_REQUEST['start'],
            'length'        => $_REQUEST['length'],
            'search_value'  => $_REQUEST['search']['value'],
            'table'         => '"Barra_Seguimiento"',
        ];      
          
        $data= $this->seguimientoModel->datatable($data);
        return $this->respond($data);
        /*
        $data = $this->seguimientoModel->get_ventanilla_seguimiento();
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);*/
    }

    public function cdi_seguimiento()
    {
        $data = $this->seguimientoModel->get_cdi_seguimiento();
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function consulta_movimientos($clave = null){
        $data['data'] = $this->seguimientoModel->consulta_movimientos_barra($clave);
        return $this->respond($data);
    }

    public function consulta_tramite($a = null, $tram= null)
    {
        $data['data'] = $this->seguimientoModel->consulta_tramite($a, $tram);
        $data['inspeccion'] = $this->seguimientoModel->consulta_inspeccion($a, $tram);
        $data['graficacion'] = $this->seguimientoModel->consulta_graficacion($a, $tram);    
        return $this->respond($data);
    }

    public function cdi()
    {
        echo view('header', ['titulo' => 'Seguimiento Cambios de Inscripción']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/cdi');
        echo view('footer');
        echo view('seguimiento/segcdijs');
    }

    public function bandeja()
    {
        echo view('header', ['titulo' => 'Bandeja de Tramites']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/semaforizacion');
        echo view('footer');
        echo view('seguimiento/semaforizacionjs');
    }

    //REPORTES
    public function reporte_movimiento_graficados()
    {
        echo view('header', ['titulo' => 'Reportes Seguimiento']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/reportes/movimientos_graficados');
        echo view('footer');
        echo view('seguimiento/reportes/movimientos_graficadosjs');
    }

    public function reporte_movimientos_graficados_enviados()
    {
        echo view('header', ['titulo' => 'Reportes Seguimiento']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/reportes/movimiento_graficacion_enviadosices');
        echo view('footer');
        echo view('seguimiento/reportes/movimiento_graficacion_enviadoicesjs');
    }

    public function reporte_tramites_detenidos()
    {
        echo view('header', ['titulo' => 'Reportes Seguimiento']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/reportes/tramites_detenidos');
        echo view('footer');
        echo view('seguimiento/reportes/tramites_detenidosjs');
    }

    public function reporte_inco_con_respuesta_ices($tipo = null)
    {    
        echo view('header', ['titulo' => 'Reportes Seguimiento']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/reportes/tramites_con_respuesta_ices');
        echo view('footer');
        echo view('seguimiento/reportes/tramites_con_respuesta_icesjs');
    }

    public function reporte_inco_sin_respuesta_ices($tipo = null)
    {    
        echo view('header');
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/reportes/tramites_sin_respuesta_ices');
        echo view('footer');
        echo view('seguimiento/reportes/tramites_sin_respuesta_icesjs');
    }

    public function reporte_tramites_sin_enviar_ices($tipo = null)
    {    
        echo view('header', ['titulo' => 'Reportes Seguimiento']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/reportes/tramites_sin_enviar_ices');
        echo view('footer');
        echo view('seguimiento/reportes/tramites_sin_enviar_icesjs');
    }

    public function reporte_seguimiento_ventanilla($tipo = null)
    {    
        echo view('header', ['titulo' => 'Reportes Seguimiento']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/reportes/tramites_seguimiento_ventanilla');
        echo view('footer');
        echo view('seguimiento/reportes/tramites_seguimiento_ventanillajs');
    }

    public function reporte_regresados_ices()
    {    
        echo view('header', ['titulo' => 'Reportes Seguimiento']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/reportes/tramites_regresados_ices');
        echo view('footer');
        echo view('seguimiento/reportes/tramites_regresados_icesjs');
    }

    public function reporte_tramites_notarios()
    {    
        echo view('header', ['titulo' => 'Reportes Seguimiento']);
		echo view('menu');
		echo view('topbar', ['usuario' => user()]);
        echo view('seguimiento/reportes/tramites_notarios');
        echo view('footer');
        echo view('seguimiento/reportes/tramites_notariosjs');
    }

    
    
    public function get_bandeja_captura($year = null){
        $data = $this->seguimientoModel->get_bandeja_captura($year);
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }
    public function get_bandeja_graficacion($year = null)
    {
        $data = $this->seguimientoModel->get_bandeja_graficacion($year);
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_bandeja_inspeccion($year = null)
    {
        $data = $this->seguimientoModel->get_bandeja_inspeccion($year);
        
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData"=>$data);

        return $this->respond($results);
    }

    public function get_brigadas_inspeccion(){
        $data['results'] = $this->seguimientoModel->get_brigadas_inspeccion();
        return $this->respond($data);
    }

    public function get_resultados_graficacion()
    {
        $data['results'] = $this->seguimientoModel->get_resultados_graficacion();
        return $this->respond($data);
    }

    public function get_graficadores(){
        $data['results'] = $this->seguimientoModel->get_graficadores();
        return $this->respond($data);
    }

    public function get_movimientos_graficacion(){
        $data['results'] = $this->seguimientoModel->get_movimientos_graficacion();
        return $this->respond($data);
    }

    public function guardar_seguimiento(){  
        helper(['form', 'url']);
        
        $QuienLoTiene = "";
        $EstadoActual = "";

        $FenvioICES = $this->request->getVar('tenvices');

        $F_AvaluoManual = $this->request->getVar('tfavaluomanual');
        $F_EntregaContribuyente = $this->request->getVar('tfentregacontr');
        $Procedio = $this->request->getVar('tprocedio');
        $ResultadoGraf = $this->request->getVar('tresultado');
        $Tramite = $this->request->getVar('ttramite');
        $ResultadoICES = $this->request->getVar('tresices');


        if($F_AvaluoManual != "" &&  $F_EntregaContribuyente == ""){
            $F_EntregaContribuyente =  $F_AvaluoManual;
        }

        if ($F_EntregaContribuyente != ""){
            $QuienLoTiene = "EI";
            $EstadoActual = "TC";
        }

        if ($F_AvaluoManual != ""){
            $QuienLoTiene = "CO";
            $EstadoActual = "CO";
        }

        if( $ResultadoGraf == "Mov. Realizado" ){
            $QuienLoTiene = "CO";
            $EstadoActual = "MR";
        }

        if( $ResultadoGraf == "No Procede" ){
            $QuienLoTiene = "CN";
            $EstadoActual = "NP";
        }

        if($Tramite == "HOMONIMOS"){
            $QuienLoTiene = "CH";
            $EstadoActual = "CO";
        }

        if($ResultadoGraf == "No hay lámina" || $ResultadoGraf == "Faltan Datos" || $ResultadoGraf == "Regresado" || $ResultadoGraf == "Falta Croquis" || $ResultadoGraf == "Enviado Inspección"){
            $QuienLoTiene = "DG";
            $EstadoActual = "FD";
        }

        if($Procedio == "N"){
            $QuienLoTiene = "CN";
            $EstadoActual = "NP";
        }

        if($ResultadoICES == "RI"){
            $QuienLoTiene = "DA";
            $EstadoActual = "RI";
        }
               
        $dateava = date('d-m-Y', strtotime($this->request->getVar('tfavaluomanual'))); 
        $Fava2 = str_replace('-','',$dateava);

        $data = [
            'F_EnvioIces' => $this->request->getVar('tenvices'),
            'NumeroOficioEnvioIces' => $this->request->getVar('tofices'),
            'F_AvaluoManual' => $this->request->getVar('tfavaluomanual'),
            'F_EntregaContribuyente' => $this->request->getVar('tfentregacontr'),
            'ProcedioSN' => $this->request->getVar('tprocedio'),
            'Observaciones' => $this->request->getVar('tobsices'),
            'Id_FolioBarra' => $this->request->getVar('tfolio'),
            'Id_Anio' => $this->request->getVar('tanio'),
            'Id_QuienLoTiene' => $QuienLoTiene,
            'Id_EstadoActual' => $EstadoActual,
            'Clave' => $this->request->getVar('icvecat'),
            'F_AvaluoManual2' => $Fava2
        ];

        $save = $this->seguimientoModel->guardar_seguimiento($data);
        //echo $save;
        if($save){
            echo 'ok';
        }
        else{
            echo 'error';
        }

    }

    //reportes

    public function get_seguimiento_tramites_barra(){
        $data['results'] = $this->seguimientoModel->get_seguimiento_tramites_barra();
        return $this->respond($data);
    }

    public function get_reporte_movimientos_graficacion()
    {    
        $data['data']	= $this->seguimientoModel->reporte_movimientos_graficacion();
        return $this->respond($data);
    }

    public function get_reporte_movimientos_graficacion_enviados($tram = null)
    {    
        $data['data']	= $this->seguimientoModel->reporte_movimientos_graficacion_enviados($tram);
        return $this->respond($data);
    }

    public function get_reporte_tramites_detenidos()
    {    
        $data['data']	= $this->seguimientoModel->reporte_tramites_detenidos($tram);
        return $this->respond($data);
    }

    public function get_reporte_inco_con_respuesta($tipo = null)
    {    
        $data['data']	= $this->seguimientoModel->reporte_inco_con_respuesta($tipo);
        return $this->respond($data);
    }

    public function get_reporte_inco_sin_respuesta($tipo = null)
    {    
        $data['data'] = $this->seguimientoModel->reporte_inco_sin_respuesta($tipo);
        return $this->respond($data);
    }
    
    public function get_reporte_sin_enviar_ices()
    {    
        $data['data'] = $this->seguimientoModel->reporte_sin_enviar_ices();
        return $this->respond($data);
    }

    public function get_reporte_seguimiento_tramites_ventanilla($tipo = null, $quien = null){
        $data['data'] = $this->seguimientoModel->reporte_seguimiento_tramites_ventanilla($tipo, $quien);
        return $this->respond($data);
    }

    public function get_reporte_inco_regresado_ices(){
        $data['data'] = $this->seguimientoModel->reporte_inco_regresado_ices();
        return $this->respond($data);
    }

    public function get_reporte_tramites_notarios($quien = null){
        $data['data'] = $this->seguimientoModel->reporte_tramites_notarios($quien);
        return $this->respond($data);
    }

    public function validarcve($cve = null)
    {    
        $data["result"] = $this->padronModel->validar_clave($cve);
        return $this->respond($data);
    }

    public function verificacion($a = null, $fol = null){
        echo view('header', ['titulo' => 'Verificación de Tramites']);
		echo view('topbar', ['usuario' => user()]);
        echo view('menu');
        echo view('seguimiento/verificacion_tramites', ['year' => $a, 'folio' => $fol]);
        echo view('footer');
        echo view('seguimiento/mapjs'); 
        echo view('seguimiento/verificacion_tramitesjs');
   
    }
    

}
