<?php namespace App\Models;
use CodeIgniter\Model;
//incluimos la interfaz para la coneccion con la base de datos
use CodeIgniter\Database\ConnectionInterface;

class InspeccionModel extends Model {

    protected $db;
	public function __construct(ConnectionInterface &$db) {
		$this->db =& $db;
    }
    

    public function get_envios_graficacion(){
        $sql = 'SELECT a."Id_PaqueteEnvioGraficacion", a."F_EnvioGraficacion", b."Id_Anio", b."Id_Folio", b."Pobl", b."Ctel", b."Manz", b."Pred", b."Unid", "F_AtendidoGraficacion" FROM "Barra_PaqueteEnvioGraficacion" a, "barra_graficacion" b WHERE a."Id_PaqueteEnvioGraficacion" = b."Id_PaqueteEnvioGraficacion"';
        return $this->db->query($sql)->getResult();
    }
    

    public function get_notin_graficacion() {
		$sql = 'SELECT "IdInconformidad","Poblacion","Cuartel","Manzana","Predio","Unidad","FechaCapturaInconformidad","AnioDeclaracion", "Clave" FROM "Inco_Barra" WHERE ("IdInconformidad","AnioDeclaracion") NOT IN (
            SELECT SUBSTRING ("Id_Folio",4,8) AS IdInconformidad,"Id_Anio" AS AnioDeclaracion FROM "Barra_Inspeccion") AND ("IdInconformidad","AnioDeclaracion") NOT IN (
            SELECT SUBSTRING ("Id_Folio",4,8) AS IdInconformidad,"Id_Anio" AS AnioDeclaracion FROM "barra_graficacion") ORDER BY "AnioDeclaracion" DESC';
        return $this->db->query($sql)->getResult();
    }

    public function get_notin_graficacion_reg(){
        $sql = 'SELECT "Id_Folio","Pobl","Ctel","Manz","Pred","Unid","F_EnvioInspeccion","Id_Anio","F_InspeccionCampo", "Clave" FROM "Regularizacion_Inspeccion" WHERE ("Id_Anio","Id_Folio") NOT IN (SELECT "Id_Anio","Id_Folio" FROM "Regularizacion_Graficacion") ORDER BY "Id_Anio" ASC';
        return $this->db->query($sql)->getResult();
    }

    public function get_dictamenes_ventanilla()
    {
        $sql = 'SELECT A."Id_PaqueteEnvioInspeccion",A."F_EnvioInspeccion",b."Id_Anio",b."Id_Folio",b."Pobl",b."Ctel",b."Manz",b."Pred",b."Unid","F_InspeccionCampo" FROM "Barra_PaqueteEnvioInspeccion" A,"Barra_Inspeccion" b WHERE A."Id_PaqueteEnvioInspeccion"=b."Id_PaqueteEnvioInspeccion" ORDER BY "Id_Anio" DESC, "Id_Folio" DESC';

        return $this->db->query($sql)->getResult();
    }

    public function get_dictamenes_regularizacion()
    {
        $sql = 'SELECT A."Id_PaqueteEnvioInspeccion",A."F_EnvioInspeccion",b."Id_Anio",b."Id_Folio",b."Pobl",b."Ctel",b."Manz",b."Pred",b."Unid","F_InspeccionCampo" FROM "Regularizacion_PaqueteEnvioIn" A,"Regularizacion_Inspeccion" b WHERE A."Id_PaqueteEnvioInspeccion"=b."Id_PaqueteEnvioInspeccion"';

        return $this->db->query($sql)->getResult();
    }

    public function get_envios_regularizacion_graficacion()
    {
        $sql = 'SELECT A."Id_PaqueteEnvioGraficacion",A."F_EnvioGraficacion",b."Id_Anio",b."Id_Folio",b."Pobl",b."Ctel",b."Manz",b."Pred",b."Unid","F_AtendidoGraficacion" FROM "Regularizacion_PaqueteEnvioGr" A,"Regularizacion_Graficacion" b WHERE A."Id_PaqueteEnvioGraficacion"=b."Id_PaqueteEnvioGraficacion";';
        return $this->db->query($sql)->getResult();
    }

    public function get_brigadas_inspeccion(){
        $sql = 'SELECT A."Id_Brigada",CONCAT (b."IdEmpleado",\' \',b."Nombre",\' \',b."ApellidoPaterno",\', \',C."IdEmpleado",\' \',C."ApellidoPaterno",\' \',C."Nombre") AS brigada, b."IdEmpleado" as i1, C."IdEmpleado" as i2 FROM "Catalogo_Brigadas" A,"Gen_Empleados" b,"Gen_Empleados" C WHERE A."Id_Empleado1"=b."IdEmpleado" AND A."Id_Empleado2"=C."IdEmpleado"';

        return $this->db->query($sql)->getResult();
    }

    public function get_movimientos_inspeccion(){
        $sql = 'SELECT "Id_MovimientoInspeccion", "Descripcion" FROM "Catalogo_MovimientoInspeccion" ORDER BY "Id_MovimientoInspeccion" ASC';
        return $this->db->query($sql)->getResult();
    }

    public function capturar_dictamen_ventanilla($data = null)
    {
        $sql = 'UPDATE "Barra_Inspeccion" SET "F_InspeccionCampo" = \''.$data["FInspeccion"].'\',"Id_Brigada" = \''.$data["Brigada"].'\' , "Id_Inspector1"= \''.$data["Inspector1"].'\',"Id_Inspector2"= \''.$data["Inspector2"].'\' ,"Id_ResultadoInspeccion" = \''.$data["Resultado"].'\',"Id_MovimientoInspeccion1"=\''.$data["Mov1"].'\',"Id_MovimientoInspeccion2"=\''.$data["Mov2"].'\',"Dictamen"=\''.$data["Dictamen"].'\',"Observaciones"=\''.$data["Observaciones"].'\' WHERE "Id_Anio"=\''.$data["Anio"].'\' AND "Id_Tipo"=\''.$data["Tipo"].'\' AND "Id_Folio"=\''.$data["Folio"].'\' ; ';

        $sql2 = 'UPDATE "Barra_Seguimiento" SET "F_InspeccionCampo"=\''.$data["FInspeccion"].'\', "Id_EstadoActual"=\' \', "Id_QuienLoTiene"=\' \' WHERE "Id_Anio"=\''.$data["Anio"].'\' AND "Id_FolioBarra"=\''.$data["Folio"].'\'';

        $q = $sql.$sql2;

        return $this->db->query($q);
      
    }

    public function generar_bitacora_inspeccion_regularizacion()
    {
        $sql = 'SELECT MAX("Id_PaqueteEnvioInspeccion") as maximo FROM "Regularizacion_PaqueteEnvioIn"';
        return $this->db->query($sql)->getResult();
    }

    public function genera_bitacora_graficacion_reg(){
        $sql = 'SELECT MAX("Id_PaqueteEnvioGraficacion") as maximo FROM "Regularizacion_PaqueteEnvioGr"';
        return $this->db->query($sql)->getResult();
    }

    public function consultar_folio($a = null, $fol = null)
    {
        $sql = 'SELECT * FROM "Barra_Inspeccion" WHERE "Id_Anio" = \''.$a.'\' AND "Id_Folio" = \''.$fol.'\''; 
        return $this->db->query($sql)->getResult();
    }

    public function get_empleados(){
        $sql = 'SELECT * FROM "Gen_Empleados"';
        return $this->db->query($sql)->getResult();
    }

    public function genera_bitacora_graficacion(){
        $sql = 'SELECT COALESCE(MAX("Id_PaqueteEnvioGraficacion"),\'0\') as maximo FROM "Barra_PaqueteEnvioGraficacion"';

        return $this->db->query($sql)->getResult();
    }

    public function genera_bitacora_regularizacion(){
        $sql = 'SELECT COALESCE(MAX("Id_PaqueteEnvioInspeccion"),\'0\') as maximo FROM "Regularizacion_PaqueteEnvioIn"';

        return $this->db->query($sql)->getResult();
    }

    public function genera_folio_paquete_regularizacion($a = null){
        $sql = 'SELECT COALESCE(MAX("Id_Folio"),\'0\') as maximo FROM "Regularizacion_Inspeccion" WHERE "Id_Anio"=\''.$a.'\'';
        return $this->db->query($sql)->getResult();
    }

    public function get_regularizacion_inspeccion_sinpaquete(){
        $sql = 'SELECT * FROM "Regularizacion_Inspeccion" WHERE "Id_PaqueteEnvioInspeccion" IS NULL;';
        return $this->db->query($sql)->getResult();
    }

    public function registrar_bitacora_graficacion($data = null){
        $folios = $data['Folios'];
        //Insertar en barra paquete envio inspeccion
        $sql1 = 'INSERT INTO "Barra_PaqueteEnvioGraficacion" ("Id_PaqueteEnvioGraficacion","F_EnvioGraficacion","Id_QuienEnvia","Id_QuienRecibe", "Origen") VALUES' . " ('".$data['Bitacora']."','".$data['FechaEnv']."','".$data['Envia']."','".$data['Recibe']."', '".$data['Origen']."'); ";

        $sqldel = 'DELETE FROM "barra_graficacion" WHERE '."\"Id_PaqueteEnvioGraficacion\"='".$data['Bitacora']."'; " ;
        $q = $sql1 . $sqldel;
        foreach($folios as $fol){
        //INSERTAR EL DETALLE DEL PAQUETE OSEA TODOS LOS FOLIOS EN BARRA INSPECCION CON EL ID DEL PAQUETE
        $sql2 = 'INSERT INTO "barra_graficacion" ("Id_Tipo","Id_PaqueteEnvioGraficacion","F_EnvioGraficacion","Id_Anio","Id_Folio","Pobl","Ctel","Manz","Pred","Unid","Clave") VALUES ';
            $sql2folios = "('B','".$data['Bitacora']."','".$data['FechaEnv']."','".$fol['AnioDeclaracion']."','000".$fol['IdInconformidad']."','".str_pad($fol['Poblacion'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Cuartel'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Manzana'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Predio'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Unidad'], 3, "0", STR_PAD_LEFT)."','".$fol['Clave']."'); ";
            //ACTUALIZAR LA TABLA BARRA_SEGUIMIENTO POR CADA FOLIO INSERTADO EN BARRA INSPECICON
            $sql3 = 'UPDATE "Barra_Seguimiento" SET "F_EnvioGraficacion" = \''.$data["FechaEnv"].'\', "Id_QuienLoTiene" = \'DG\' WHERE "Id_Anio" = \''.$fol['AnioDeclaracion'].'\' AND "Id_FolioBarra" = \'000'.$fol['IdInconformidad'].'\' AND "Pobl" = \''.str_pad($fol['Poblacion'], 3, "0", STR_PAD_LEFT).'\' AND "Ctel" = \''.str_pad($fol['Cuartel'], 3, "0", STR_PAD_LEFT).'\' AND "Manz" = \''.str_pad($fol['Manzana'], 3, "0", STR_PAD_LEFT).'\' AND "Pred" = \''.str_pad($fol['Predio'], 3, "0", STR_PAD_LEFT).'\' AND "Unid" = \''.str_pad($fol['Unidad'], 3, "0", STR_PAD_LEFT).'\'; ';
            $q.= " " . $sql2 . $sql2folios . " " . $sql3;
        }
        //return $q;
        //Ejecutar la insercion;
        return $this->db->query($q);
    }

    public function registrar_regularizacion_inspeccion($data = null){
        $sql = 'INSERT INTO "Regularizacion_Inspeccion" ("Id_PaqueteEnvioInspeccion","Id_Anio","Id_Tipo","Id_Folio","F_EnvioInspeccion","F_InspeccionCampo","Id_Brigada","Id_Inspector1","id_Inspector2","Id_ResultadoInspeccion","Id_MovimientoInspeccion1","Id_MovimientoInspeccion2","AreaMedida","Dictamen","Observaciones","Pobl","Ctel","Manz","Pred","Unid","Clave") VALUES ';

        $sqlv = "(null,'".$data["anio"]."','R','".$data['folio']."',NOW(),'".$data['finspec']."','".$data['brigada']."','".$data['i1']."','".$data['i2']."','IN','".$data['mov1']."','".$data['mov2']."',".$data['area'].",'".$data['dictamen']."','".$data['observaciones']."','".$data['pobl']."','".$data['ctel']."','".$data['manz']."','".$data['pred']."','".$data['unid']."','".$data['clave']."')";

        //return $sql.$sqlv;
        $q = $sql.$sqlv;
        return $this->db->query($q);
    }

    public function registrar_bitacora_regularizacion_inspeccion($data = null){
        $sql1 = 'INSERT INTO "Regularizacion_PaqueteEnvioIn" ("Id_PaqueteEnvioInspeccion", "F_EnvioInspeccion") VALUES (\''.$data['Bitacora'].'\', \''.$data['FechaEnv'].'\'); ';
        
        //actualizar cada uno de los folios seleccionados
        $folios = $data['Folios'];
        $sql2 = "";
        foreach($folios as $fol){
            //INSERTAR EL DETALLE DEL PAQUETE OSEA TODOS LOS FOLIOS EN BARRA INSPECCION CON EL ID DEL PAQUETE
            $sql2 .= 'UPDATE "Regularizacion_Inspeccion" SET "Id_PaqueteEnvioInspeccion" = \''.$data['Bitacora'].'\' WHERE "Id_Anio" = \''.$fol['Id_Anio'].'\' and "Id_Folio" = \''.$fol['Id_Folio'].'\' and "Clave" = \''.$fol['Clave'].'\'; ';     
        }
        $q = $sql1.$sql2;
        //return $q;
        return $this->db->query($q);
    }

    public function registrar_bitacora_regularizacion_graficacion($data = null){
        $sql1 = 'INSERT INTO "Regularizacion_PaqueteEnvioGr" ("Id_PaqueteEnvioGraficacion", "F_EnvioGraficacion", "Id_QuienEnvia", "Id_QuienRecibe") VALUES (\''.$data['Bitacora'].'\', \''.$data['FechaEnv'].'\',  \''.$data['Envia'].'\', \''.$data['Recibe'].'\'); ';
        
        //actualizar cada uno de los folios seleccionados
        $folios = $data['Folios'];
        $sql2 = "";
        foreach($folios as $fol){
            //INSERTAR EL DETALLE DEL PAQUETE OSEA TODOS LOS FOLIOS EN BARRA INSPECCION CON EL ID DEL PAQUETE
            $sql2 .= 'INSERT INTO "Regularizacion_Graficacion" ("Id_Tipo", "Id_PaqueteEnvioGraficacion", "F_EnvioGraficacion", "Id_Anio", "Id_Folio", "Pobl", "Ctel", "Manz", "Pred", "Unid", "Clave") VALUES '."('R' ,'".$data['Bitacora']."' ,'".$data['FechaEnv']."' ,'".$fol['Id_Anio']."' ,'".$fol['Id_Folio']."' ,'".$fol['Pobl']."' ,'".$fol['Ctel']."' ,'".$fol['Manz']."' ,'".$fol['Pred']."' ,'".$fol['Unid']."' ,'".$fol['Clave']."'); ";     
        }
        $q = $sql1.$sql2;
        //return $q;
        return $this->db->query($q);
    }


    //REPORTES

    public function reporte_movimientos_regularizacion($mov1 = null, $mov2 = null, $f1 = null, $f2 = null){
        $filtro = "";
        if($mov1 != null && $mov2 != null){
            $filtro = "AND A.\"Id_MovimientoInspeccion1\" BETWEEN '$mov1' AND '$mov2'";
        }
        else{
            $filtro = "AND A.\"Id_MovimientoInspeccion1\" >= '00'";
        }
        
        if($f1 != null && $f2 != null){
            $filtro .= " AND a.\"F_InspeccionCampo\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."Id_MovimientoInspeccion1" AS ID,b."Descripcion",COUNT (A."Id_Folio") AS Total FROM "Regularizacion_Inspeccion" A,"Catalogo_MovimientoInspeccion" b WHERE A."Id_MovimientoInspeccion1"=b."Id_MovimientoInspeccion" AND A."Id_ResultadoInspeccion"=\'IN\' '.$filtro.' 
        GROUP BY A."Id_MovimientoInspeccion1",b."Descripcion" ORDER BY 1';

        //return $sql ;
        return $this->db->query($sql)->getResult();

    }

    public function reporte_movimientos_por_brigada_regularizacion($b1 = null, $b2 = null){

        $filtro = "";
        if($b1 != null && $b2 != null){
            $filtro = " AND a.\"Id_Brigada\" BETWEEN '$b1' AND '$b2'";
        }

        $sql = 'DROP TABLE IF EXISTS "Regularizacion_InspeccionTMP1";
        SELECT a."Id_Brigada", c."Nombre" AS nombre1, d."Nombre" AS nombre2, 
        SUBSTRING(a."Clave", 1, 9) AS manzana INTO "Regularizacion_InspeccionTMP1" 
        FROM "Regularizacion_Inspeccion" a,   
        "Gen_Empleados" AS c, "Gen_Empleados" AS d, "Catalogo_Brigadas" AS e 
        WHERE a."Id_ResultadoInspeccion" = \'IN\' AND 
        a."Id_Brigada" = e."Id_Brigada" AND 
        e."Id_Empleado1" = c."IdEmpleado" AND 
        e."Id_Empleado2" = d."IdEmpleado" 
        GROUP BY a."Id_Brigada", c."Nombre", d."Nombre", SUBSTRING(a."Clave", 1, 9) 
        ORDER BY 1;
        
        DROP TABLE IF EXISTS "Regularizacion_InspeccionTMP2";
        
        SELECT a.manzana, COUNT(b."CLAVE") AS predios INTO "Regularizacion_InspeccionTMP2"
                FROM "Regularizacion_InspeccionTMP1" AS a, a_unid AS b 
                WHERE  a.manzana = SUBSTRING(b."CLAVE", 1, 9)
                GROUP BY a.manzana;
        
        CREATE INDEX manzanatmp1 ON "Regularizacion_InspeccionTMP1"(manzana);
        CREATE INDEX manzanatmp2 ON "Regularizacion_InspeccionTMP2"(manzana);
        
        SELECT a."Id_Brigada", a.nombre1, a.nombre2, a.manzana, b.predios
        FROM "Regularizacion_InspeccionTMP1" AS a, "Regularizacion_InspeccionTMP2" AS b
        WHERE a.manzana = b.manzana
        '.$filtro.'
        ORDER BY a."Id_Brigada", a.nombre1, a.nombre2, a.manzana;';

        return $this->db->query($sql)->getResult();
    }

    public function reporte_movimientos_por_concepto_regularizacion($b1 = null, $b2 = null, $f1 = null, $f2 = null){
        $filtro = "";
        if($b1 != null && $b2 != null){
            $filtro = "AND a.\"Id_Brigada\" BETWEEN '$b1' AND '$b2' ";
        }       
        if($f1 != null && $f2 != null){
            $filtro .= " AND a.\"F_InspeccionCampo\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."Id_MovimientoInspeccion1" AS ID,b."Descripcion",A."Clave" 
        FROM "Regularizacion_Inspeccion" A,"Catalogo_MovimientoInspeccion" b 
        WHERE A."Id_MovimientoInspeccion1"=b."Id_MovimientoInspeccion" 
        AND A."Id_ResultadoInspeccion"=\'IN\' 
        AND A."Id_MovimientoInspeccion1">=\'00\' 
        AND A."Id_Brigada"<> \'04\' 
        '.$filtro.'
        GROUP BY A."Id_MovimientoInspeccion1",b."Descripcion",A."Clave" ORDER BY 1';

        //return $sql ;
        return $this->db->query($sql)->getResult();
    }

    public function reporte_movimientos_por_concepto_brigada_regularizacion($b1 = null, $b2 = null, $f1 = null, $f2 = null){
        $filtro = "";
        if($b1 != null && $b2 != null){
            $filtro = "AND a.\"Id_Brigada\" BETWEEN '$b1' AND '$b2' ";
        }       
        if($f1 != null && $f2 != null){
            $filtro .= " AND a.\"F_InspeccionCampo\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."Id_Brigada",C."Nombre" AS nombre1,d."Nombre" AS nombre2,CONCAT (A."Pobl",A."Ctel",A."Manz") AS manzana,A."Id_MovimientoInspeccion1" AS ID,b."Descripcion",COUNT (A."Id_Folio") AS Total FROM "Regularizacion_Inspeccion" A,"Catalogo_MovimientoInspeccion" b,"Gen_Empleados" AS C,"Gen_Empleados" AS d,"Catalogo_Brigadas" AS e WHERE A."Id_MovimientoInspeccion1"=b."Id_MovimientoInspeccion" AND A."Id_ResultadoInspeccion"=\'IN\' AND A."Id_Brigada"=e."Id_Brigada" AND e."Id_Empleado1"=C."IdEmpleado" AND e."Id_Empleado2"=d."IdEmpleado" AND A."Id_MovimientoInspeccion1">=\'00\' '.$filtro.' GROUP BY A."Id_Brigada",C."Nombre",d."Nombre",CONCAT (A."Pobl",A."Ctel",A."Manz"),A."Id_MovimientoInspeccion1",b."Descripcion" ORDER BY 1';

        //return $sql ;
        return $this->db->query($sql)->getResult();
    }

    public function reporte_movimientos_ventanilla($f1 = null, $f2 = null){
        $filtro = "";
        
        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"F_InspeccionCampo\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."Id_MovimientoInspeccion1" AS ID,b."Descripcion",COUNT (A."Id_Folio") AS Total FROM "Barra_Inspeccion" A,"Catalogo_MovimientoInspeccion" b WHERE A."Id_MovimientoInspeccion1"=b."Id_MovimientoInspeccion" AND A."Id_ResultadoInspeccion"=\'IN\' '.$filtro.'
        GROUP BY A."Id_MovimientoInspeccion1",b."Descripcion"';

        //return $sql ;
        return $this->db->query($sql)->getResult();

    }

    public function reporte_movimientos_por_concepto_ventanilla($f1 = null, $f2 = null){
        $filtro = "";
        
        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"F_InspeccionCampo\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."Id_MovimientoInspeccion1" AS ID,b."Descripcion",A."Clave" FROM "Barra_Inspeccion" A,"Catalogo_MovimientoInspeccion" b WHERE A."Id_MovimientoInspeccion1"=b."Id_MovimientoInspeccion" AND A."Id_ResultadoInspeccion"=\'IN\' AND A."Id_MovimientoInspeccion1">=\'00\' AND A."Id_Brigada"<> \'04\' '.$filtro.'
        GROUP BY A."Id_MovimientoInspeccion1",b."Descripcion",A."Clave"';

        //return $sql ;
        return $this->db->query($sql)->getResult();

    }
    
    public function reporte_movimientos_por_concepto_brigada_ventanilla($b1 = null, $b2 = null, $f1 = null, $f2 = null){
        $filtro = "";
        if($b1 != null && $b2 != null){
            $filtro = "AND a.\"Id_Brigada\" BETWEEN '$b1' AND '$b2' ";
        }       
        if($f1 != null && $f2 != null){
            $filtro .= " AND a.\"F_InspeccionCampo\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."Id_Brigada",C."Nombre" AS nombre1,d."Nombre" AS nombre2,A."Id_MovimientoInspeccion1" AS ID,b."Descripcion",COUNT (A."Id_Folio") AS Total FROM "Barra_Inspeccion" A,"Catalogo_MovimientoInspeccion" b,"Gen_Empleados" AS C,"Gen_Empleados" AS d,"Catalogo_Brigadas" AS e WHERE A."Id_MovimientoInspeccion1"=b."Id_MovimientoInspeccion" AND A."Id_ResultadoInspeccion"=\'IN\' AND A."Id_Brigada"=e."Id_Brigada" AND e."Id_Empleado1"=C."IdEmpleado" AND e."Id_Empleado2"=d."IdEmpleado" '.$filtro.' GROUP BY A."Id_Brigada",C."Nombre",d."Nombre",A."Id_MovimientoInspeccion1",b."Descripcion"';

        //return $sql ;
        return $this->db->query($sql)->getResult();
    }
       

}

