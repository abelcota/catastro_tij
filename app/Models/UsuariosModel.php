<?php namespace App\Models;

//incluimos la interfaz para la coneccion con la base de datos
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
use Myth\Auth\Entities\User;
use Myth\Auth\Models\UserModel;

class UsuariosModel extends Model
{
    //hacemos referencia a la tabla de nuestra base de datos
    protected $table      = 'users';
    //hacemos referencia a la llave primaria de la tabla tramites
    protected $primaryKey = 'id';
    //definimos los campos que van a poder ser utilizables en el modelo


}