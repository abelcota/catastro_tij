<?php namespace App\Models;

//incluimos la interfaz para la coneccion con la base de datos
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class TramitesModel extends Model
{
    //hacemos referencia a la tabla de nuestra base de datos
    protected $table      = 'tramites';
    //hacemos referencia a la llave primaria de la tabla tramites
    protected $primaryKey = 'cod_tramite';
    //definimos los campos que van a poder ser utilizables en el modelo
    protected $allowedFields = ['cod_tramite','descripcion'];


}