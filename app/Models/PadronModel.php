<?php namespace App\Models;
use CodeIgniter\Model;
//incluimos la interfaz para la coneccion con la base de datos
use CodeIgniter\Database\ConnectionInterface;

class PadronModel extends Model {

    protected $db;
	public function __construct(ConnectionInterface &$db) {
		$this->db =& $db;
    }
    
    public function list_cuarteles() {

        $sql = "SELECT * FROM a_ctel order by cve_ctel ASC";
        return $this->db->query($sql)->getResult();
    }

    public function list_manzanas($ctel = null) {
		$sql = "SELECT * FROM a_manz WHERE \"NUM_CTEL\" = '$ctel' ;";
        
        return $this->db->query($sql)->getResult();
    }

    
    public function list_predios( $ctel = null, $manz = null) {
		$sql = "SELECT \"Predio\" FROM a_unid WHERE \"Zona\" = '$ctel' AND \"Manzana\" = $manz;";
        
        return $this->db->query($sql)->getResult();
    }

    public function list_unidades($pobl = null, $ctel = null, $manz = null, $pred = null) {
		$sql = "SELECT \"NUM_UNID\" FROM a_unid WHERE \"CVE_POBL\" = $pobl AND \"NUM_CTEL\" = $ctel AND \"NUM_MANZ\" = $manz AND \"NUM_PRED\" = $pred;";
        
        return $this->db->query($sql)->getResult();
    }
    
    public function list_poblaciones() {
		return $this->db
                        ->table('a_pobl')
                        ->select('*')
                        ->get()
                        ->getResult();
    }

    public function listar_calles() {
		return $this->db
                        ->table('a_calle')
                        ->select('"NOM_POBL", a_calle.*')
                        ->join('a_pobl', 'a_pobl."CVE_POBL" = a_calle."CVE_POBL"')
                        ->get()
                        ->getResult();
    }

    public function listar_calles2($pobl = 0) {
        $where = " \"CVE_POBL\" = $pobl ";
		return $this->db
                        ->table('a_calle')
                        ->select('CVE_CALLE as id, CALLE as text')
                        ->where("CVE_POBL = $pobl")
                        ->get()
                        ->getResult();
    }

    public function listar_tramos2($pobl = 0, $calle = 0) {
        
        return $this->db
                        ->table('a_tramo')
                        ->select('CVE_TRAMO as id, DES_TRAMO as text')
                        ->where(" \"CVE_POBL\" = $pobl AND \"CVE_CALLE\" = $calle")
                        ->get()
                        ->getResult();
    }
    
    
    public function listar_poblaciones()
    {
        return $this->db
                        ->table('a_pobl')
                        ->select('*')
                        ->get()
                        ->getResult();
    }

    public function listar_cat_mp()
    {
        return $this->db
                        ->table('a_cat_mp')
                        ->select('*')
                        ->get()
                        ->getResult();
    }

    public function listar_colonias($pobl = "")
    {
        $where = " \"CVE_POBL\" = $pobl ";
        return $this->db
                        ->table('a_col')
                        ->select('*')
                        ->where($where)
                        ->get()
                        ->getResult();
    }

    public function listar_regimenes()
    {
        return $this->db
                        ->table('a_regim')
                        ->select('*')
                        ->get()
                        ->getResult();
    }

    

    public function listar_colonias_all()
    {
        return $this->db
                        ->table('a_col')
                        ->select('"NOM_POBL", a_col.*')
                        ->join('a_pobl', 'a_col."CVE_POBL" = a_pobl."CVE_POBL"')
                        ->get()
                        ->getResult();
    }

    public function listar_conceptos()
    {
        return $this->db
                        ->table('a_concep')
                        ->select('CVE_CONCEP, DES_CONCEP')
                        ->get()
                        ->getResult();
    }

    public function listar_construcciones()
    {
        return $this->db
                        ->table('a_const')
                        ->select('*')
                        ->get()
                        ->getResult();
    }

    public function listar_demc()
    {
        return $this->db
                        ->table('a_demc')
                        ->select('*')
                        ->get()
                        ->getResult();
    }

    public function listar_demt()
    {
        return $this->db
                        ->table('a_demt')
                        ->select('*')
                        ->get()
                        ->getResult();
    }

    public function listar_manzanas()
    {
        return $this->db
                        ->table('a_manz')
                        ->select('"NOM_POBL", a_manz.*')
                        ->join('a_pobl', 'a_manz."CVE_POBL" = a_pobl."CVE_POBL"')
                        ->get()
                        ->getResult();
    }

    

    public function listar_zonas()
    {
        return $this->db
                        ->table('a_zona')
                        ->select('"NOM_POBL", a_zona.*')
                        ->join('a_pobl', 'a_zona."CVE_POBL" = a_pobl."CVE_POBL"')
                        ->get()
                        ->getResult();
    }

    public function listar_tramites()
    {
        return $this->db
                        ->table('Inco_Tramites')
                        ->select('*')
                        ->get()
                        ->getResult();
    }

    public function listar_propietarios_todos()
    {
        return $this->db
                        ->table('a_prop')
                        ->select('"CVE_PROP", "TIP_PERS", "APE_PAT", "APE_MAT", "NOM_PROP", "REG_FED", "STT_AFE_SU", "STT_AFE_IN", encode("FOTO", \'base64\') as "foto"')
                        ->get()
                        ->getResult();

    }

    public function listar_tramos()
    {
        return $this->db
                        ->table('a_tramo')
                        ->select('a_pobl."NOM_POBL",a_calle."NOM_CALLE", a_tramo.*')
                        ->join('a_pobl', 'a_pobl."CVE_POBL" = a_tramo."CVE_POBL"')
                        ->join('a_calle', 'a_calle."CVE_CALLE" = a_tramo."CVE_CALLE" and a_calle."CVE_POBL" = a_tramo."CVE_POBL"')
                        ->get()
                        ->getResult();

    } 

    public function listar_demeritos_terreno()
    {
        return $this->db
                        ->table('a_demt')
                        ->select('*')
                        ->get()
                        ->getResult();

    } 

    public function listar_terrenos()
    {
        return $this->db
                        ->table('a_terr')
                        ->select('*')
                        ->get()
                        ->getResult();

    } 

    public function listar_notarios()
    {
        $query = "select * from notarios order by cve_notario asc";
        return $this->db->query($query)->getResult();

    } 

    public function listar_propietarios2()
    {
        $query = "select * from a_prop";
        return $this->db->query($query)->getResult();

    } 

    public function listar_propietarios($filtro = "", $cve = 0)
    {
        if ($cve == 0){ 
            $where = "CONCAT(\"APE_PAT\", ' ', \"APE_MAT\", ' ', \"NOM_PROP\") like '%$filtro%'";
        }
        else{
            $where = "\"CVE_PROP\" = $cve";
        }  
        return $this->db
                        ->table('a_prop')
                        ->select('*')
                        ->where($where)
                        ->get()
                        ->getResult();

    }

    public function listar_propietarios_unidades($filtro = null)
    {
        return $this->db
                    ->table('a_prop_u')
                    ->select('a_prop."NOM_PROP", a_prop_u.*')
                    ->join('a_prop', 'a_prop."CVE_PROP" = a_prop_u.cve_prop')
                    ->get()
                    ->getResult();
    }

    public function consulta_avaluo_unidad($clave = null, $folio = null)
    {
        $query = "SELECT * FROM h_unidad WHERE folio = '$folio' and \"CLAVE\" = '$clave';";
        return $this->db->query($query)->getResult();
    }

    public function consulta_avaluo_zona($clave = null, $folio = null)
    {
        $query = "SELECT * FROM h_zona WHERE folio = '$folio' and clave = '$clave';";
        return $this->db->query($query)->getResult();
    }

    public function consulta_avaluo_terrenos($clave = null, $folio = null)
    {
        $query = "SELECT * FROM h_terr WHERE folio = '$folio' and clave = '$clave';";
        return $this->db->query($query)->getResult();
    }

    public function consulta_avaluo_construcciones($clave = null, $folio = null)
    {
        $query = "SELECT * FROM h_const WHERE folio = '$folio' and clave = '$clave';";
        return $this->db->query($query)->getResult();
    }

    public function consulta_avaluo_propietarios($clave = null, $folio = null)
    {
        $query = "SELECT * FROM h_prop_u WHERE folio = '$folio' and clave = '$clave';";
        return $this->db->query($query)->getResult();
    }


    public function validar_clave($clave = null)
    {

        $query2 = "SELECT *, 
        (SELECT row_to_json (fc) AS jsonb_build_object 
        FROM (SELECT 'FeatureCollection' AS TYPE,array_to_json (ARRAY_AGG (f)) AS features 
        FROM (SELECT 'Feature' AS TYPE,ST_AsGeoJSON (ST_Transform (lg.geom,4326),15,0) :: json AS geometry,row_to_json ((ccat,nombre,domicilio,sp, sct, carta)) AS properties FROM (
        SELECT * FROM predios WHERE ccat='".$clave."') AS lg) AS f) AS fc) jsonb_build_object
        FROM a_unid WHERE \"ClaveCatastral\" = '".$clave."'";


        return $this->db->query($query2)->getResult();
    }

    public function get_valor_zona($pob = null, $ctel= null, $manz = null)
    {
        $query = ' SELECT * FROM a_zona, a_manz 
        WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA")
        AND a_manz."CVE_POBL"=\''.$pob.'\'
        AND a_manz."NUM_CTEL"= \''.$ctel.'\'
        AND a_manz."NUM_MANZ"= \''.$manz.'\'
                    ';
        //echo $query;
        return $this->db->query($query)->getResult();
       
    }

    public function get_tramites_clave($cve = null)
    {
        $query = "SELECT * from \"Inco_Barra\" ib 
        inner join \"Inco_Tramites\" t on ib.\"IdTramite\" = t.\"IdTramite\"
        where \"Clave\" =  '$cve' order by \"FechaCapturaInconformidad\" DESC";
        return $this->db->query($query)->getResult();
    }

    public function get_servicios($cve = null){
        $sql = 'SELECT "STS_SERVIC", s.* from a_unid u 
        inner join "a_ServiciosInternet" s on u."STS_SERVIC" = s.campox
        WHERE u."CLAVE" = \''.$cve.'\'';

        return $this->db->query($sql)->getResult();
    }

    public function consulta_servicios($serv = null){
        $sql = "SELECT * FROM a_servayto WHERE \"CVE_SERV\" LIKE '$serv%' ;";

        return $this->db->query($sql)->getResult();
    }

    public function get_usos($cve = null){
        /*$sql = 'SELECT uso.* from a_unid u
        inner join a_usosayto uso on u."CVE_USO" = uso."CVE_USO"
        WHERE "CLAVE" = \''.$cve.'\'';*/

        $sql = "SELECT u.* , (SELECT \"DES_USO\" FROM a_usosayto WHERE \"CVE_USO\" = CONCAT(u.\"CVE_GPO_USO\", LPAD(u.\"CVE_USO\"::text, 3, '0')) ) as \"DES_USO\", CONCAT(u.\"CVE_GPO_USO\", LPAD(u.\"CVE_USO\"::text, 3, '0')) AS CVE_COMP
        FROM a_uso_u u WHERE \"CLAVE\" = '$cve'";

        return $this->db->query($sql)->getResult();
    }

    public function get_propietariostj($cve = null){
        $sql = 'SELECT * FROM a_prop WHERE "CLAVE" = \''.$cve.'\'';

        return $this->db->query($sql)->getResult();
    }

    public function get_propietarios($cve = null){
        $sql = 'SELECT  a_prop_u."NUM_PROP", a_prop.* FROM a_prop_u 
        inner join a_prop on a_prop_u.cve_prop = a_prop."CVE_PROP"
        WHERE "CLAVE" = \''.$cve.'\' ORDER BY a_prop_u."NUM_PROP" ASC';

        return $this->db->query($sql)->getResult();
    }

    public function genera_bitacora_inspeccion(){
        $sql = 'SELECT CASE WHEN MAX ("Id_PaqueteEnvioInspeccion") IS NULL THEN \'00000000\' ELSE MAX ("Id_PaqueteEnvioInspeccion") END AS "maximo" FROM "Barra_PaqueteEnvioInspeccion"';
        //$sql = 'SELECT MAX("Id_PaqueteEnvioInspeccion") as maximo FROM "Barra_PaqueteEnvioInspeccion"';
        return $this->db->query($sql)->getResult();
    }

    public function genera_bitacora_graficacion(){
        $sql = 'SELECT CASE WHEN MAX ("Id_PaqueteEnvioGraficacion") IS NULL THEN \'00000000\' ELSE MAX ("Id_PaqueteEnvioGraficacion") END AS "maximo" FROM "Barra_PaqueteEnvioGraficacion"';
        //$sql = 'SELECT MAX("Id_PaqueteEnvioGraficacion") as maximo FROM "Barra_PaqueteEnvioGraficacion"';

        return $this->db->query($sql)->getResult();
    }

    public function listar_categorias_construccion(){
        $sql = "SELECT * FROM a_cat_mp";
        return $this->db->query($sql)->getResult();
    }

    public function listar_edo_construccion(){
        $sql = "SELECT * FROM \"a_EdoC\"";
        return $this->db->query($sql)->getResult();
    }

    public function listar_edad_construccion(){
        $sql = "SELECT * FROM \"a_EdadC\"";
        return $this->db->query($sql)->getResult();
    }

    public function listar_grupo_usos(){
        $sql = "SELECT * FROM \"a_gUso\"";
        return $this->db->query($sql)->getResult();
    }   

    public function listar_usos($grupo = null){

        $sql = "SELECT * FROM a_usosayto WHERE \"CVE_USO\" LIKE '$grupo%' ; ";
        return $this->db->query($sql)->getResult();
    }   

    public function listar_propietarios_result(){

        $sql = "SELECT \"CVE_PROP\" as id, CONCAT(\"APE_PAT\" , ' ' , \"APE_MAT\", ' ', \"NOM_PROP\") as \"text\" FROM a_prop; ";
        return $this->db->query($sql)->getResult();
    }   

    public function get_construccion($cve = null){
        $cve_split = str_split($cve, 3);
        $pobl = $cve_split[0];

        /*$sql = 'SELECT
        cons."CVE_CAT_CO",
        (SELECT "DES_CAT_CO" FROM a_cat_mp WHERE "CVE_CAT_CO" = cons."CVE_CAT_CO") as "DES_CAT_CO", 
        (SELECT val_unit_c FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO") AS ValorUnitario,
        cons."SUP_CONST",
        cons."CVE_EDO_CO",
        (SELECT "DES_EDO_CO" FROM "a_EdoC" WHERE "CVE_EDO_CO" = cons."CVE_EDO_CO") as "EDO_CO",
        cons."FEC_CONST",
        cons."EDD_CONST",
        (SELECT "FAC_DEM_CO" FROM a_demc WHERE "CVE_CAT_CO" = cons."CVE_CAT_CO" AND "CVE_EDO_CO"= cons."CVE_EDO_CO" AND "EDD_CONST"= cons."EDD_CONST") AS FactorDemerito,
        (SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL" = \''.$pobl.'\') AS FactorComercializacion,
        0.00 AS ValorNeto 
    FROM
        a_const AS cons 
    WHERE
        cons.clave = \''.$cve.'\'';*/

        $sql = 'SELECT cons.*, cons."CVE_CAT_CO",(
            SELECT "DES_CAT_CO" FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO") AS "DES_CAT_CO",(
            SELECT val_unit_c FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO") AS ValorUnitario,cons."SUP_CONST",cons."CVE_EDO_CO",(
            SELECT "DES_EDO_CO" FROM "a_EdoC" WHERE "CVE_EDO_CO"=cons."CVE_EDO_CO") AS "EDO_CO",cons."FEC_CONST",cons."EDD_CONST",(
            SELECT "FAC_DEM_CO" FROM a_demc WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO" AND "CVE_EDO_CO"=cons."CVE_EDO_CO" AND "EDD_CONST"=cons."EDD_CONST") AS FactorDemerito,(
            SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL"=\''.$pobl.'\') AS FactorComercializacion,(cons."SUP_CONST"*(
            SELECT val_unit_c FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO")*(
            SELECT "FAC_DEM_CO" FROM a_demc WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO" AND "CVE_EDO_CO"=cons."CVE_EDO_CO" AND "EDD_CONST"=cons."EDD_CONST")*(
            SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL"=\''.$pobl.'\')) AS ValorNeto FROM a_const AS cons WHERE cons.clave= \''.$cve.'\'';
        return $this->db->query($sql)->getResult();
    }

    public function get_terrenotj($cve = null){
        $sql = "SELECT * from a_terr WHERE clave = '".$cve."'";
        return $this->db->query($sql)->getResult();
    }

    public function get_terreno($cve = null){

        $cve_split = str_split($cve, 3);
        $pobl = $cve_split[0];
        $ctel = $cve_split[1];
        $manz = $cve_split[2];


            $sql = 'SELECT trr.*, trr."TIP_TERR",trr."CVE_CALLE",trr."CVE_TRAMO",trr."SUP_TERR",COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\')) AS ValorUnitario1,COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),0) AS DemeritoTramo1,trr."CVE_CALLE_",trr."CVE_TRAMO_",trr."SUP_TERR_E",COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\')) AS ValorUnitario2,COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),0) AS DemeritoTramo2,"SUP_TERR_D",CONCAT ("CVE_DEM_TE" :: VARCHAR (2),\'-\',"CVE_DEM_T2" :: VARCHAR (2),\'-\',"CVE_DEM_T3" :: VARCHAR (2)) AS demeritos,"FAC_DEM_TE",(trr."SUP_TERR"*COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\'))*((100-COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),0))/100))+(trr."SUP_TERR_E"*0.25*COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\'))*((100-COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),0))/100))-("SUP_TERR_D"*COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\'))*"FAC_DEM_TE") AS valorneto FROM a_terr AS trr WHERE trr.clave=\''.$cve.'\'';

            //echo $pobl . " . " . $ctel . " . " . $manz;
            return $this->db->query($sql)->getResult();

    }

    public function terr_tramo($pob = null, $calle = null, $tramo = null){
        $sql = 'SELECT * FROM a_tramo WHERE "CVE_POBL"=\''.$pob.'\' AND "CVE_CALLE"=\''.$calle.'\' AND "CVE_TRAMO"=\''.$tramo.'\'';

        return $this->db->query($sql)->getResult();

    }
    
    

    public function validar_clavecdi($clave = null)
    {
        return $this->db->query('SELECT "CLAVE", "UBI_PRED", "DOM_NOT", "ID_REGTO", d."APE_PAT", d."APE_MAT", d."NOM_PROP", d."CVE_PROP", (SELECT "NOM_POBL" FROM a_pobl WHERE "CVE_POBL" = \''.substr($clave, 0, 3).'\'
        ) as nompobl, (SELECT row_to_json(fc) AS jsonb_build_object FROM 
        (SELECT \'FeatureCollection\' As type, array_to_json(array_agg(f))
        As features FROM 
        (SELECT 
        \'Feature\' As type, 
        ST_AsGeoJSON(ST_Transform(lg.geom, 4326),15,0)::json As geometry,
        row_to_json((id, dg_ccat)) As properties
        FROM (SELECT * FROM "Culiacan_Join" WHERE dg_ccat  = \'007'.$clave.'\') As lg) As f ) As fc) fc FROM a_unid a,   a_prop d  
        WHERE a."CLAVE" = \''.$clave.'\' AND a."CVE_PROP" = d."CVE_PROP" 
                    ')
                    ->getResult();
    }

    public function validarcvecdimanz($clave = null)
    {

        $sql = 'SELECT (
            SELECT "NOM_POBL" FROM a_pobl WHERE "CVE_POBL"=\''.substr($clave, 0, 3).'\') AS nompobl,(
            SELECT row_to_json (fc) AS jsonb_build_object FROM (
            SELECT \'FeatureCollection\' AS TYPE,array_to_json (ARRAY_AGG (f)) AS features FROM (
            SELECT \'Feature\' AS TYPE,ST_AsGeoJSON (ST_Transform (lg.geom,4326),15,0) :: json AS geometry,row_to_json ((ID,dg_mpcm)) AS properties FROM (
            SELECT*FROM manzanas WHERE dg_mpcm=\'007'.$clave.'\') AS lg) AS f) AS fc) fc';
        //return $sql;
        return $this->db->query($sql)->getResult();
    }


    public function listar_inconformidades($a = null)
    {
        return $this->db->query('SELECT 
        "AnioDeclaracion", 
        "IdInconformidad",
        CONCAT(SUBSTRING("Clave",1,3), \'-\',SUBSTRING("Clave",4,3) ,\'-\' , SUBSTRING("Clave",7,3) , \'-\' , SUBSTRING("Clave",10,3) , \'-\' , SUBSTRING("Clave",13,3)) AS clavef, 
        "DescripcionTramite", 
        "NombreSolicitante"
         FROM "Inco_Barra" b, "Inco_Tramites" tr 
         WHERE b."IdTramite" = tr."IdTramite" and "AnioDeclaracion"=\''.$a.'\'
         ORDER BY "AnioDeclaracion" DESC, "IdInconformidad" DESC')
                    ->getResult();
    }

    public function listar_cdi($a = null)
    {
        return $this->db->query('SELECT "AnioAviso", "Folio", CONCAT(SUBSTRING("Clave",1,3) , \'-\' , SUBSTRING("Clave",4,3) ,\'-\' , SUBSTRING("Clave",7,3) , \'-\' , SUBSTRING("Clave",10,3) , \'-\' , SUBSTRING("Clave",13,3)) AS clavef, "PropietarioAnterior", "PropietarioActual" FROM "Inco_AvisoCambio" WHERE "AnioAviso"=\''.$a.'\' ORDER BY "AnioAviso" DESC, "Folio" ')
                    ->getResult();
    }

    public function listar_cdi_manzana($a = null)
    {
        $sql = 'SELECT
        "AnioAviso",
        "Folio",
        CONCAT("Poblacion", \'-\' , "Cuartel" , \'-\' , "Manzana") AS "clavef",
        "PredioInicio",
        "PredioFin",
        "PropietarioAnterior",
        "PropietarioActual" 
    FROM
        "Inco_AvisoCambioManzana" 
    WHERE
        "AnioAviso" = \''.$a.'\' 
    ORDER BY
        "AnioAviso" DESC,
        "Folio"';
        return $this->db->query($sql)->getResult();
    }

    public function get_empleados($clave = "")
    {
        return $this->db->query("SELECT \"IdEmpleado\", \"Nombre\" FROM \"Gen_Empleados\" WHERE \"IdPuesto\"='$clave' ORDER BY 1")->getResult();
        
    }

    public function get_empleados_v(){
        $sql = 'SELECT * FROM "Gen_Empleados"';
        return $this->db->query($sql)->getResult();
    }

    public function listar_usuarios()
    {
        return $this->db->query("SELECT u.*, g.name as \"rol\" FROM users u
        INNER JOIN auth_groups_users aug on u.id = aug.user_id
        INNER JOIN auth_groups g on g.id = aug.group_id")->getResult();
        
    }

    public function registrar_bitacora_inspeccion($data = null){
        $folios = $data['Folios'];
        //Insertar en barra paquete envio inspeccion
        $sql1 = 'INSERT INTO "Barra_PaqueteEnvioInspeccion" ("Id_PaqueteEnvioInspeccion","F_EnvioInspeccion","Id_QuienEnvia","Id_QuienRecibe") VALUES' . " ('".$data['Bitacora']."','".$data['FechaEnv']."','".$data['Envia']."','".$data['Recibe']."'); ";

        $q = $sql1;
        foreach($folios as $fol){
            //INSERTAR EL DETALLE DEL PAQUETE OSEA TODOS LOS FOLIOS EN BARRA INSPECCION CON EL ID DEL PAQUETE
        $sql2 = 'INSERT INTO "Barra_Inspeccion" ("Id_Tipo","Id_PaqueteEnvioInspeccion","F_EnvioInspeccion","Id_Anio","Id_Folio","Pobl","Ctel","Manz","Pred","Unid","Clave") VALUES ';
            $sql2folios = "('B','".$data['Bitacora']."','".$data['FechaEnv']."','".$fol['AnioDeclaracion']."','000".$fol['IdInconformidad']."','".str_pad($fol['Poblacion'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Cuartel'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Manzana'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Predio'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Unidad'], 3, "0", STR_PAD_LEFT)."','".$fol['Clave']."'); ";
            //ACTUALIZAR LA TABLA BARRA_SEGUIMIENTO POR CADA FOLIO INSERTADO EN BARRA INSPECICON
            $sql3 = 'UPDATE "Barra_Seguimiento" SET "F_EnvioInspeccion" = \''.$data["FechaEnv"].'\', "Id_QuienLoTiene" = \'DI\' WHERE "Id_Anio" = \''.$fol['AnioDeclaracion'].'\' AND "Id_FolioBarra" = \'000'.$fol['IdInconformidad'].'\' AND "Pobl" = \''.str_pad($fol['Poblacion'], 3, "0", STR_PAD_LEFT).'\' AND "Ctel" = \''.str_pad($fol['Cuartel'], 3, "0", STR_PAD_LEFT).'\' AND "Manz" = \''.str_pad($fol['Manzana'], 3, "0", STR_PAD_LEFT).'\' AND "Pred" = \''.str_pad($fol['Predio'], 3, "0", STR_PAD_LEFT).'\' AND "Unid" = \''.str_pad($fol['Unidad'], 3, "0", STR_PAD_LEFT).'\'; ';
            $q.= " " . $sql2 . $sql2folios . " " . $sql3;
        }
        //return $q;
        //Ejecutar la insercion;
        return $this->db->query($q);
    }

    public function registrar_bitacora_graficacion($data = null){
        $folios = $data['Folios'];
        //Insertar en barra paquete envio inspeccion
        $sql1 = 'INSERT INTO "Barra_PaqueteEnvioGraficacion" ("Id_PaqueteEnvioGraficacion","F_EnvioGraficacion","Id_QuienEnvia","Id_QuienRecibe", "Origen") VALUES' . " ('".$data['Bitacora']."','".$data['FechaEnv']."','".$data['Envia']."','".$data['Recibe']."', '".$data['Origen']."'); ";

        $sqldel = 'DELETE FROM "barra_graficacion" WHERE '."\"Id_PaqueteEnvioGraficacion\"='".$data['Bitacora']."'; " ;
        $q = $sql1 . $sqldel;
        foreach($folios as $fol){
            //INSERTAR EL DETALLE DEL PAQUETE OSEA TODOS LOS FOLIOS EN BARRA INSPECCION CON EL ID DEL PAQUETE
        $sql2 = 'INSERT INTO "barra_graficacion" ("Id_Tipo","Id_PaqueteEnvioGraficacion","F_EnvioGraficacion","Id_Anio","Id_Folio","Pobl","Ctel","Manz","Pred","Unid","Clave") VALUES ';
            $sql2folios = "('B','".$data['Bitacora']."','".$data['FechaEnv']."','".$fol['AnioDeclaracion']."','000".$fol['IdInconformidad']."','".str_pad($fol['Poblacion'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Cuartel'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Manzana'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Predio'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Unidad'], 3, "0", STR_PAD_LEFT)."','".$fol['Clave']."'); ";
            
            //ACTUALIZAR LA TABLA BARRA_SEGUIMIENTO POR CADA FOLIO INSERTADO EN BARRA INSPECICON
            $sql3 = 'UPDATE "Barra_Seguimiento" SET "F_EnvioGraficacion" = \''.$data["FechaEnv"].'\', "Id_QuienLoTiene" = \'DG\' WHERE "Id_Anio" = \''.$fol['AnioDeclaracion'].'\' AND "Id_FolioBarra" = \'000'.$fol['IdInconformidad'].'\' AND "Pobl" = \''.str_pad($fol['Poblacion'], 3, "0", STR_PAD_LEFT).'\' AND "Ctel" = \''.str_pad($fol['Cuartel'], 3, "0", STR_PAD_LEFT).'\' AND "Manz" = \''.str_pad($fol['Manzana'], 3, "0", STR_PAD_LEFT).'\' AND "Pred" = \''.str_pad($fol['Predio'], 3, "0", STR_PAD_LEFT).'\' AND "Unid" = \''.str_pad($fol['Unidad'], 3, "0", STR_PAD_LEFT).'\'; ';
            $q.= " " . $sql2 . $sql2folios . " " . $sql3;
        }
        //return $q;
        //Ejecutar la insercion;
        return $this->db->query($q);
    }


    public function listar_roles()
    {
        return $this->db->query("SELECT * FROM auth_groups")->getResult();
        
    }

    public function cancelar_clave($data = null){
        $clave_master = $data["clave_m"];
        $clave_can = $data["clave_c"];

        //$sql1 = "DELETE from a_unid where \"CLAVE\" = '$clave_can'; ";
        $sql1 = "UPDATE a_unid SET \"ID_REGTO\" = 'B', \"STS_MOVTO\" = 'Baja por fusion con $clave_master' where \"CLAVE\" = '$clave_can'; ";
        $sql2 = "UPDATE a_terr set \"clave\" = '$clave_master' where \"clave\" = '$clave_can';";
        $sql3 = "UPDATE a_const set \"clave\" = '$clave_master' where \"clave\" = '$clave_can';";
        $sql4 = "DELETE from a_prop_u where \"CLAVE\" = '$clave_can';";
        $sql5 = "DELETE from a_uso_u where \"CLAVE\"	= '$clave_can';";

        $q = $sql1.$sql2.$sql3.$sql4.$sql5;
        //echo $q;
        return $this->db->query($q);
    }

    public function finalizar_captura_cdi($data = null){
        $anio = $data['ANIO'];
        $folio = $data['FOLIO'];
        $capturista = $data['CAPTURISTA'];
        $tramite = $data['TRAMITE'];
        $valor_actual = $data['VALOR_C'];
        $valor_anterior = $data['VALOR_A'];
        $cve_cat = $data['CVE_CAT'];

        $a_fol = $anio.$folio;
        $cve_split = str_split($cve_cat, 3);
        $pobl = $cve_split[0];
        $ctel = $cve_split[1];
        $manz = $cve_split[2];

        $sql = "UPDATE barra_captura_cdi SET estatus_verificacion = 'C', fecha_captura = NOW(), capturista = '$capturista' WHERE id_anio = '$anio' AND id_folio = '000$folio'; ";
        
        $sql2 = "INSERT INTO avaluos VALUES (default, '$a_fol', '$tramite', '$cve_cat', '$valor_anterior', '$valor_actual', NOW());";

        //Se guarda en la tabla h_unidad para el historico
        $sqlh = 'INSERT INTO h_unidad SELECT \''.$a_fol.'\' AS folio,A.*,b."CVE_CALLE",b."NOM_CALLE",b."TIP_CALLE",b."CALLE",C."CVE_COL",C."TIPO_COL",C."NOM_COL",C."COLONIA",C."COD_POS",d."TIP_PERS",d."APE_PAT",d."APE_MAT",d."NOM_PROP",d."REG_FED",d."FOTO",e."DES_USO",ar."DES_REGIM",ar."TIP_PROP",(
            SELECT row_to_json (fc) AS jsonb_build_object FROM (
            SELECT \'FeatureCollection\' AS TYPE,array_to_json (ARRAY_AGG (f)) AS features FROM (
            SELECT \'Feature\' AS TYPE,ST_AsGeoJSON (ST_Transform (lg.geom,4326),15,0) :: json AS geometry,row_to_json ((ID,dg_ccat,nom_comp_t,nom_calle,nom_col,num_ofic_u,sup_terr,sup_const,val_catast)) AS properties FROM (
            SELECT*FROM "Culiacan_Join" WHERE dg_ccat=\'007'.$cve_cat.'\') AS lg) AS f) AS fc) geometrico FROM a_unid A LEFT JOIN a_calle b ON A."CVE_POBL"=b."CVE_POBL" AND A."CVE_CALLE_"=b."CVE_CALLE" LEFT JOIN a_col C ON A."CVE_POBL"=C."CVE_POBL" AND A."CVE_COL_UB"=C."CVE_COL" LEFT JOIN a_prop d ON A."CVE_PROP"=d."CVE_PROP" LEFT JOIN a_usosayto e ON A."CVE_USO"=e."CVE_USO" LEFT JOIN a_regim ar ON A."CVE_REGIM"=ar."CVE_REGIM" WHERE A."CLAVE"=\''.$cve_cat.'\'; '; 
        
            $sqlterr = 'INSERT INTO h_terr SELECT \''.$a_fol.'\' AS folio,trr."CVE_POBL",trr."NUM_TERR",trr."TIP_TERR",trr."CVE_CALLE",trr."CVE_TRAMO",trr."SUP_TERR", COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\')) AS ValorUnitario1,COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),0) AS DemeritoTramo1,trr."CVE_CALLE_",trr."CVE_TRAMO_",trr."SUP_TERR_E",COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\')) AS ValorUnitario2,COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),0) AS DemeritoTramo2,"SUP_TERR_D",CONCAT ("CVE_DEM_TE" :: VARCHAR (2),\'-\',"CVE_DEM_T2" :: VARCHAR (2),\'-\',"CVE_DEM_T3" :: VARCHAR (2)) AS demeritos,"FAC_DEM_TE",(trr."SUP_TERR"*COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\'))*((100-COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),0))/100))+(trr."SUP_TERR_E"*0.25*COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\'))*((100-COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),0))/100))-("SUP_TERR_D"*COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\'))*"FAC_DEM_TE") AS valorneto, clave FROM a_terr AS trr WHERE trr.clave=\''.$cve_cat.'\'; ';

                $sqlconst = 'INSERT INTO h_const SELECT \''.$a_fol.'\' AS folio, cons."NUM_CONST", cons."CVE_CAT_CO",(
                    SELECT "DES_CAT_CO" FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO") AS "DES_CAT_CO",(
                    SELECT val_unit_c FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO") AS ValorUnitario,cons."SUP_CONST",cons."CVE_EDO_CO",(
                    SELECT "DES_EDO_CO" FROM "a_EdoC" WHERE "CVE_EDO_CO"=cons."CVE_EDO_CO") AS "EDO_CO",cons."FEC_CONST",cons."EDD_CONST",(
                    SELECT "FAC_DEM_CO" FROM a_demc WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO" AND "CVE_EDO_CO"=cons."CVE_EDO_CO" AND "EDD_CONST"=cons."EDD_CONST") AS FactorDemerito,(
                    SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL"=\''.$pobl.'\') AS FactorComercializacion,(cons."SUP_CONST"*(
                    SELECT val_unit_c FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO")*(
                    SELECT "FAC_DEM_CO" FROM a_demc WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO" AND "CVE_EDO_CO"=cons."CVE_EDO_CO" AND "EDD_CONST"=cons."EDD_CONST")*(
                    SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL"=\''.$pobl.'\')) AS ValorNeto, clave FROM a_const AS cons WHERE cons.clave= \''.$cve_cat.'\'; ';

                $sqlprop = 'INSERT INTO h_prop_u SELECT \''.$a_fol.'\' AS folio, a_prop_u."NUM_PROP", a_prop.*, a_prop_u."CLAVE" FROM a_prop_u 
                    inner join a_prop on a_prop_u.cve_prop = a_prop."CVE_PROP"
                    WHERE "CLAVE" = \''.$cve_cat.'\'; ';

                $sqlusos = "INSERT INTO h_uso_u SELECT '$a_fol' AS folio, u.*, (SELECT \"DES_USO\" FROM a_usosayto WHERE \"CVE_USO\" = CONCAT(u.\"CVE_GPO_USO\", LPAD(u.\"CVE_USO\"::text, 3, '0')) ) as \"DES_USO\", CONCAT(u.\"CVE_GPO_USO\", LPAD(u.\"CVE_USO\"::text, 3, '0')) AS CVE_COMP
                    FROM a_uso_u u WHERE \"CLAVE\" = '$cve_cat'; "; 
                    
                $sqlzona = 'INSERT INTO h_zona SELECT \''.$a_fol.'\' AS folio, a_zona.*, \''.$cve_cat.'\' as clave FROM a_zona, a_manz 
                    WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA")
                    AND a_manz."CVE_POBL"=\''.$pobl.'\'
                    AND a_manz."NUM_CTEL"= \''.$ctel.'\'
                    AND a_manz."NUM_MANZ"= \''.$manz.'\'; ';

                $q = $sql.$sql2.$sqlh.$sqlterr.$sqlconst.$sqlprop.$sqlusos.$sqlzona;
                //echo $q;
                return $this->db->query($q);

    }

    public function finalizar_captura($data = null){
        $anio = $data['ANIO'];
        $folio = $data['FOLIO'];
        $capturista = $data['CAPTURISTA'];
        $tramite = $data['TRAMITE'];
        $valor_actual = $data['VALOR_C'];
        $valor_anterior = $data['VALOR_A'];
        $cve_cat = $data['CVE_CAT'];

        $a_fol = $anio.$folio;
        $cve_split = str_split($cve_cat, 3);
        $pobl = $cve_split[0];
        $ctel = $cve_split[1];
        $manz = $cve_split[2];

        $sql = "UPDATE barra_captura SET estatus_verificacion = 'C', fecha_captura = NOW(), capturista = '$capturista' WHERE id_anio = '$anio' AND id_folio = '000$folio' and clave = '$cve_cat'; ";

        $sql2 = "INSERT INTO avaluos VALUES (default, '$a_fol', '$tramite', '$cve_cat', '$valor_anterior', '$valor_actual', NOW()); ";

        $sql_seg = 'UPDATE "Barra_Seguimiento" SET "F_AvaluoManual" = NOW(), "Id_EstadoActual" = \'CO\' , "Id_QuienLoTiene" = \'CO\' WHERE "Id_Anio" = \''.$anio.'\' AND "Id_FolioBarra" = \'000'.$folio.'\' ;';

        //Se guarda en la tabla h_unidad para el historico
        $sqlh = 'INSERT INTO h_unidad SELECT \''.$a_fol.'\' AS folio,A.*,b."CVE_CALLE",b."NOM_CALLE",b."TIP_CALLE",b."CALLE",C."CVE_COL",C."TIPO_COL",C."NOM_COL",C."COLONIA",C."COD_POS",d."TIP_PERS",d."APE_PAT",d."APE_MAT",d."NOM_PROP",d."REG_FED",d."FOTO",e."DES_USO",ar."DES_REGIM",ar."TIP_PROP",(
            SELECT row_to_json (fc) AS jsonb_build_object FROM (
            SELECT \'FeatureCollection\' AS TYPE,array_to_json (ARRAY_AGG (f)) AS features FROM (
            SELECT \'Feature\' AS TYPE,ST_AsGeoJSON (ST_Transform (lg.geom,4326),15,0) :: json AS geometry,row_to_json ((ID,dg_ccat,nom_comp_t,nom_calle,nom_col,num_ofic_u,sup_terr,sup_const,val_catast)) AS properties FROM (
            SELECT*FROM "Culiacan_Join" WHERE dg_ccat=\'007'.$cve_cat.'\') AS lg) AS f) AS fc) geometrico FROM a_unid A LEFT JOIN a_calle b ON A."CVE_POBL"=b."CVE_POBL" AND A."CVE_CALLE_"=b."CVE_CALLE" LEFT JOIN a_col C ON A."CVE_POBL"=C."CVE_POBL" AND A."CVE_COL_UB"=C."CVE_COL" LEFT JOIN a_prop d ON A."CVE_PROP"=d."CVE_PROP" LEFT JOIN a_usosayto e ON A."CVE_USO"=e."CVE_USO" LEFT JOIN a_regim ar ON A."CVE_REGIM"=ar."CVE_REGIM" WHERE A."CLAVE"=\''.$cve_cat.'\'; ';

            
        
            $sqlterr = 'INSERT INTO h_terr SELECT \''.$a_fol.'\' AS folio,trr."CVE_POBL",trr."NUM_TERR",trr."TIP_TERR",trr."CVE_CALLE",trr."CVE_TRAMO",trr."SUP_TERR", COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\')) AS ValorUnitario1,COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),0) AS DemeritoTramo1,trr."CVE_CALLE_",trr."CVE_TRAMO_",trr."SUP_TERR_E",COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\')) AS ValorUnitario2,COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),0) AS DemeritoTramo2,"SUP_TERR_D",CONCAT ("CVE_DEM_TE" :: VARCHAR (2),\'-\',"CVE_DEM_T2" :: VARCHAR (2),\'-\',"CVE_DEM_T3" :: VARCHAR (2)) AS demeritos,"FAC_DEM_TE",(trr."SUP_TERR"*COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\'))*((100-COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),0))/100))+(trr."SUP_TERR_E"*0.25*COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\'))*((100-COALESCE ((
                SELECT 100*(1-"FAC_DEM_VA" :: INT) FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE_" AND "CVE_TRAMO"=trr."CVE_TRAMO_"),0))/100))-("SUP_TERR_D"*COALESCE ((
                SELECT "VAL_UNIT_T" FROM a_tramo WHERE "CVE_POBL"=trr."CVE_POBL" AND "CVE_CALLE"=trr."CVE_CALLE" AND "CVE_TRAMO"=trr."CVE_TRAMO"),(
                SELECT "VAL_UNIT_T" FROM a_zona,a_manz WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA") AND a_manz."CVE_POBL"=\''.$pobl.'\' AND a_manz."NUM_CTEL"=\''.$ctel.'\' AND a_manz."NUM_MANZ"=\''.$manz.'\'))*"FAC_DEM_TE") AS valorneto, clave FROM a_terr AS trr WHERE trr.clave=\''.$cve_cat.'\'; ';

                $sqlconst = 'INSERT INTO h_const SELECT \''.$a_fol.'\' AS folio, cons."NUM_CONST", cons."CVE_CAT_CO",(
                    SELECT "DES_CAT_CO" FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO") AS "DES_CAT_CO",(
                    SELECT val_unit_c FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO") AS ValorUnitario,cons."SUP_CONST",cons."CVE_EDO_CO",(
                    SELECT "DES_EDO_CO" FROM "a_EdoC" WHERE "CVE_EDO_CO"=cons."CVE_EDO_CO") AS "EDO_CO",cons."FEC_CONST",cons."EDD_CONST",(
                    SELECT "FAC_DEM_CO" FROM a_demc WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO" AND "CVE_EDO_CO"=cons."CVE_EDO_CO" AND "EDD_CONST"=cons."EDD_CONST") AS FactorDemerito,(
                    SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL"=\''.$pobl.'\') AS FactorComercializacion,(cons."SUP_CONST"*(
                    SELECT val_unit_c FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO")*(
                    SELECT "FAC_DEM_CO" FROM a_demc WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO" AND "CVE_EDO_CO"=cons."CVE_EDO_CO" AND "EDD_CONST"=cons."EDD_CONST")*(
                    SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL"=\''.$pobl.'\')) AS ValorNeto, clave FROM a_const AS cons WHERE cons.clave= \''.$cve_cat.'\'; ';

                $sqlprop = 'INSERT INTO h_prop_u SELECT \''.$a_fol.'\' AS folio, a_prop_u."NUM_PROP", a_prop.*, a_prop_u."CLAVE" FROM a_prop_u 
                    inner join a_prop on a_prop_u.cve_prop = a_prop."CVE_PROP"
                    WHERE "CLAVE" = \''.$cve_cat.'\'; ';

                $sqlusos = "INSERT INTO h_uso_u SELECT '$a_fol' AS folio, u.*, (SELECT \"DES_USO\" FROM a_usosayto WHERE \"CVE_USO\" = CONCAT(u.\"CVE_GPO_USO\", LPAD(u.\"CVE_USO\"::text, 3, '0')) ) as \"DES_USO\", CONCAT(u.\"CVE_GPO_USO\", LPAD(u.\"CVE_USO\"::text, 3, '0')) AS CVE_COMP
                    FROM a_uso_u u WHERE \"CLAVE\" = '$cve_cat'; "; 
                    
                $sqlzona = 'INSERT INTO h_zona SELECT \''.$a_fol.'\' AS folio, a_zona.*, \''.$cve_cat.'\' as clave FROM a_zona, a_manz 
                    WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA")
                    AND a_manz."CVE_POBL"=\''.$pobl.'\'
                    AND a_manz."NUM_CTEL"= \''.$ctel.'\'
                    AND a_manz."NUM_MANZ"= \''.$manz.'\'; ';

                $q = $sql.$sql2.$sqlh.$sqlterr.$sqlconst.$sqlprop.$sqlusos.$sqlzona.$sql_seg;
                //echo $q;
                return $this->db->query($q);

    }

    public function listar_barra_captura_cdi(){
        $sql = 'SELECT * from barra_captura_cdi';
        return $this->db->query($sql)->getResult();
    }

    public function listar_barracaptura(){
        $sql = 'SELECT bc.*, "DescripcionTramite" FROM barra_captura bc
        left join "Inco_Tramites" it on bc.id_tramite = it."IdTramite"
        order by id_captura DESC';
        return $this->db->query($sql)->getResult();
    }

    public function listar_avaluos(){
        $sql = 'SELECT av.* from avaluos av order by id_avaluo DESC';
        return $this->db->query($sql)->getResult();
    }

    public function asignar_propietario($data = null)
    {
        $cve = $data['CLAVE'];
        $prop = $data['CVE_PROP'];
        $sql = "INSERT INTO a_prop_u VALUES($prop, 0, '$cve', null); ";
        $sql2 = "UPDATE a_unid SET \"CVE_PROP\" = $prop WHERE \"CLAVE\" = '$cve'; ";
        //return $sql;
        return $this->db->query($sql.$sql2);

    }

    public function asignar_rol($rol = null, $usuario = null)
    {
        $sql = "INSERT INTO auth_groups_users VALUES($rol, $usuario)";
        //return $sql;
        return $this->db->query($sql);

    }

    public function get_user_rol()
    {
        $query = $this->db->query("SELECT ag.name FROM auth_groups_users agu
        INNER JOIN auth_groups ag on agu.group_id = ag.id
        where agu.user_id = ".user_id());
        $rol = "";
        foreach ($query->getResult('array') as $row)
        {
            $rol .= $row['name']." ";
        }

        return $rol;
        
    }

    public function check_folio($a = null,$id = null){
        $sql = 'SELECT count(*) as fol FROM "Inco_Barra" WHERE "IdInconformidad" = \''.$id.'\' AND "AnioDeclaracion"=\''.$a.'\';';
        return $this->db->query($sql)->getRow()->fol;
    }

    public function check_folio_cdi($a = null,$id = null){
        $sql = 'SELECT count(*) as fol FROM "Inco_AvisoCambio" WHERE "Folio" = \''.$id.'\' AND "AnioAviso"=\''.$a.'\';';
        return $this->db->query($sql)->getRow()->fol;
    }


    public function get_folio_declaracion($a = null,$id = null){
        /*$sql = 'SELECT * FROM "Inco_Barra" ib
        left join a_col ac on ac."CVE_COL" = ib."IdColonia" and ac."CVE_POBL" = ib."Poblacion"
        left join a_calle aca on aca."CVE_CALLE" = ib."IdCalle" and aca."CVE_POBL" = ib."Poblacion"
        WHERE ib."IdInconformidad" = \''.$id.'\' AND "AnioDeclaracion"=\''.$a.'\';';*/
        $sql = 'SELECT * FROM "Inco_Barra" ib
        left join a_col ac on ac."CVE_COL" = ib."IdColonia" and ac."CVE_POBL" = ib."Poblacion"
        left join a_calle aca on aca."CVE_CALLE" = ib."IdCalle" and aca."CVE_POBL" = ib."Poblacion"
				left join "Inco_Tramites" it on it."IdTramite" = ib."IdTramite"
				left join a_usosayto au on ib."IdUso" = "CVE_USO"
				left join "Gen_Empleados" ge on ge."IdEmpleado" = ib."IdEmpleadoBarra"
                WHERE ib."IdInconformidad" = \''.$id.'\' AND "AnioDeclaracion"=\''.$a.'\';';
        
        return $this->db->query($sql)->getResult();
    }

    public function get_status_folio_cdi($a = null,$id = null){

        $sql = "select estatus_verificacion from barra_captura_cdi where id_folio = '$id' and id_anio = '$a';";
        return $this->db->query($sql)->getResult();
    }

    public function get_status_folio($a = null,$id = null){

        $sql = 'select ce."Descripcion" as descripcion, ce."Estado", cqt."Descripcion" as lotiene, (SELECT estatus_verificacion FROM barra_captura WHERE id_folio = \''.$id.'\' and id_anio = \''.$a.'\') as "status_captura" from "Barra_Seguimiento" bs 
        left join "Catalogo_QuienLoTiene" cqt on bs."Id_QuienLoTiene" = cqt."Id_QuienLoTiene"
        left join "Catalogo_EstadoActual" ce on bs."Id_EstadoActual" = ce."Id_EstadoActual" where "Id_FolioBarra" = \''.$id.'\' AND "Id_Anio"=\''.$a.'\';';
        //echo $sql;
        //exit();
        return $this->db->query($sql)->getResult();
    }
    

    public function get_folio_cdi($a = null,$id = null){
        $sql = 'SELECT ia.*, e.*, (SELECT CONCAT("NOM_PROP", \' \', "APE_PAT", \' \', "APE_MAT") FROM a_prop where "CVE_PROP"::text = ia."PropietarioAnterior") as prop_ant, (SELECT CONCAT("NOM_PROP", \' \', "APE_PAT", \' \', "APE_MAT") FROM a_prop where "CVE_PROP"::text = ia."PropietarioActual") as prop_act, n.* FROM "Inco_AvisoCambio" ia 
        inner join "Gen_Empleados" AS e on ia."IdEmpleadoBarra" = e."IdEmpleado"
        left join notarios n on n.cve_notario = ia."TramiteNotario"
        WHERE "Folio" = \''.$id.'\' AND "AnioAviso"=\''.$a.'\';';
        return $this->db->query($sql)->getResult();
    }

    public function genera_folio_declaraciones($a = null){
        //$sql = 'SELECT COALESCE(MAX("IdInconformidad"),\'0\') as maximo FROM "Inco_Barra"  WHERE "AnioDeclaracion"=\''.$a.'\'';
        $sql = 'SELECT COALESCE(MAX(f."Folio"),\'0\') as maximo FROM
        (SELECT "Folio" FROM "Inco_AvisoCambio" where "AnioAviso" = \''.$a.'\'
        UNION 
        SELECT "IdInconformidad" FROM "Inco_Barra" where "AnioDeclaracion" = \''.$a.'\') f';
        //return $sql;
        return $this->db->query($sql)->getResult();
    }

    public function genera_folio_cdi($a = null){
        $sql = 'SELECT COALESCE(MAX("Folio"),\'0\') as maximo FROM "Inco_AvisoCambio" WHERE "AnioAviso"=\''.$a.'\'';
        //return $sql;
        return $this->db->query($sql)->getResult();
    }

    public function genera_folio_cdi_manz($a = null){
        $sql = 'SELECT COALESCE(MAX("Folio"),\'0\') as maximo FROM "Inco_AvisoCambioManzana" WHERE "AnioAviso"=\''.$a.'\'';
        //return $sql;
        return $this->db->query($sql)->getResult();
    }

    public function genera_clave_propietario()
    {
        $sql = 'SELECT COALESCE(MAX("CVE_PROP") + 1,\'0\') as cve_prop FROM a_prop';
        return $this->db->query($sql)->getResult();
    }
    
    public function genera_folio_bonificacion($a = null){
        $sql = 'SELECT COALESCE(MAX("IdFolio"),\'0\') as maximo FROM "Barra_OficioBonificacion" WHERE "IdAnio"=\''.$a.'\'';
        //return $sql;
        return $this->db->query($sql)->getResult();
    }

    public function guardar_cdi($data = null)
    {
         //Se crea la primera parte del comando insertar
        $sql1 = 'INSERT INTO "Inco_AvisoCambio" ("Folio","Poblacion","Cuartel","Manzana","Predio","Unidad","NPoblacion", "PropietarioAnterior", "PropietarioActual", "NombreSolicitante", "PoblacionDescripcion", "DomicilioNotificacion", "ColoniaNotificacion", "DomicilioUbicacion", "ColoniaUbicacion", "Noticia", "Control", "TramiteNotario", "Concepto", "TituloTraslativo", "Motivo", "TipoB", "TipoC", "Clave", "AnioAviso", "FechaCaptura", "Observaciones", "IdEmpleadoBarra", "NumeroOficial", "TelefonoSolicitante") 
        ';
        $sqlv = " VALUES ('".$data['Folio']."', '".$data['Poblacion']."', '".$data['Cuartel']."', '".$data['Manzana']."', '".$data['Predio']."', '".$data['Unidad']."', '".$data['NPoblacion']."', '".$data['PropietarioAnterior']."', '".$data['PropietarioActual']."', '".$data['NombreSolicitante']."', '".$data['PoblacionDescripcion']."', '".$data['DomicilioNotificacion']."', '".$data['ColoniaNotificacion']."', '".$data['DomicilioUbicacion']."', '".$data['ColoniaUbicacion']."', 0, 'S/C', '".$data['TramiteNotario']."', '".$data['Concepto']."', '".$data['TituloTraslativo']."', '".$data['Motivo']."', '".$data['TipoB']."', '".$data['TipoC']."', '".$data['Clave']."', '".$data['AnioAviso']."', '".$data['FechaCaptura']."', '".$data['Observaciones']."', '".$data['IdEmpleadoBarra']."', '".$data['NumeroOficial']."', '".$data['TelefonoSolicitante']."');
        ";
        $q = $sql1 . $sqlv;
        //echo $sql1 . $sqlv;
        return $this->db->query($q);
        
    }

    public function guardar_cdi_manzana($data = null){
        $sql1 = 'INSERT INTO "Inco_AvisoCambioManzana" ("Folio","Poblacion","Cuartel","Manzana","PredioInicio","PredioFin","NPoblacion","PropietarioAnterior","PropietarioActual","PoblacionDescripcion","DomicilioNotificacion","ColoniaNotificacion","DomicilioUbicacion","ColoniaUbicacion","Noticia","Concepto","TituloTraslativo","Motivo","TipoB","TipoC","Clave","AnioAviso","FechaCaptura","Observaciones","IdEmpleadoBarra") ';

        $sqlv = "VALUES ('".$data['Folio']."', '".$data['Poblacion']."', '".$data['Cuartel']."', '".$data['Manzana']."', '".$data['PredioInicio']."', '".$data['PredioFin']."', '".$data['NPoblacion']."', '".$data['PropietarioAnterior']."', '".$data['PropietarioActual']."', '".$data['PoblacionDescripcion']."', '".$data['DomicilioNotificacion']."', '".$data['ColoniaNotificacion']."', '".$data['DomicilioUbicacion']."', '".$data['ColoniaUbicacion']."', ".$data['Noticia'].", '".$data['Concepto']."', '".$data['TituloTraslativo']."', '".$data['Motivo']."', '".$data['TipoB']."', '".$data['TipoC']."', '".$data['Clave']."', '".$data['AnioAviso']."', '".$data['FechaCaptura']."', '".$data['Observaciones']."', '".$data['IdEmpleadoBarra']."');";

        //Insertar en cada uno de los los predios el cambioi 
        $sqlfol = 'SELECT COALESCE(MAX("Folio"),\'0\') as maximo FROM "Inco_AvisoCambio" WHERE "AnioAviso"=\''.$data['AnioAviso'].'\';';
        $max = $this->db->query($sqlfol)->getRow()->maximo;
        $max = intval($max);

        $desde = intval($data['PredioInicio']);
        $hasta = intval($data['PredioFin']);

        $qpreds = "";

        for ($i = $desde; $i <= $hasta; $i++) {    
            $max++;
            $fol = str_pad($max, 5, "0", STR_PAD_LEFT);
            $pred = str_pad($desde, 3, "0", STR_PAD_LEFT);
            $unid = '001';

            $clave = $data['Clave'].$pred.$unid;

            //Se crea la primera parte del comando insertar
            $sql1d = 'INSERT INTO "Inco_AvisoCambio" ("Folio","Poblacion","Cuartel","Manzana","Predio","Unidad","NPoblacion", "PropietarioAnterior", "PropietarioActual", "NombreSolicitante", "PoblacionDescripcion", "DomicilioNotificacion", "ColoniaNotificacion", "DomicilioUbicacion", "ColoniaUbicacion", "Noticia", "Control", "TramiteNotario", "Concepto", "TituloTraslativo", "Motivo", "TipoB", "TipoC", "Clave", "AnioAviso", "FechaCaptura", "Observaciones", "IdEmpleadoBarra", "NumeroOficial", "TelefonoSolicitante") 
            ';
            $sqlvd = " VALUES ('".$fol."', '".$data['Poblacion']."', '".$data['Cuartel']."', '".$data['Manzana']."', '".$pred."', '".$unid."', '".$data['NPoblacion']."', '".$data['PropietarioAnterior']."', '".$data['PropietarioActual']."', '".$data['NombreSolicitante']."', '".$data['PoblacionDescripcion']."', '".$data['DomicilioNotificacion']."', '".$data['ColoniaNotificacion']."', '".$data['DomicilioUbicacion']."', '".$data['ColoniaUbicacion']."', ".$data['Noticia'].", '', '', '".$data['Concepto']."', '".$data['TituloTraslativo']."', '".$data['Motivo']."', '".$data['TipoB']."', '".$data['TipoC']."', '".$clave."', '".$data['AnioAviso']."', '".$data['FechaCaptura']."', '".$data['Observaciones']."', '".$data['IdEmpleadoBarra']."', '', '');
            ";
            $qpreds .= $sql1d . $sqlvd;
            //echo $sql1 . $sqlv;
            //$this->db->query($q);
            $desde++;

        }

        $q = $sql1 . $sqlv. $qpreds;
        //echo $q;
        return $this->db->query($q);
    }

    public function guardar_oficio_bonificacion($data = null)
    {
         //Se crea la primera parte del comando insertar
        $sql1 = 'INSERT INTO "Barra_OficioBonificacion" ("IdFolio", "IdAnio", "FechaCaptura", "Poblacion", "Cuartel", "Manzana", "Predio", "Unidad", "ClaveIncorrecta", "NombreIncorrecto", "DomicilioIncorrecto", "SuperficieTerrenoIncorrecto", "SuperficieConstruccionIncorrecto","ValorCatastralIncorrecto", "Poblacion2", "Cuartel2", "Manzana2", "Predio2", "Unidad2", "ClaveCorrecta", "NombreCorrecto", "DomicilioCorrecto", "SuperficieTerrenoCorrecto", "SuperficieConstruccionCorrecto", "ValorCatastralCorrecto", "Observaciones", "IdEmpleadoBarra")';

        $sqlv = " VALUES ('".$data['IdFolio']."', '".$data['IdAnio']."', '".$data['FechaCaptura']."', ".$data['Poblacion'].", ".$data['Cuartel'].", ".$data['Manzana'].", ".$data['Predio'].", ".$data['Unidad'].", '".$data['ClaveIncorrecta']."', '".$data['NombreIncorrecto']."', '".$data['DomicilioIncorrecto']."', ".$data['SuperficieTerrenoIncorrecto'].", ".$data['SuperficieConstruccionIncorrecto'].", ".$data['ValorCatastralIncorrecto'].", ".$data['Poblacion2'].", ".$data['Cuartel2'].", ".$data['Manzana2'].", ".$data['Predio2'].", ".$data['Unidad2'].", '".$data['ClaveCorrecta']."', '".$data['NombreCorrecto']."', '".$data['DomicilioCorrecto']."', ".$data['SuperficieTerrenoCorrecto'].", ".$data['SuperficieConstruccionCorrecto'].", ".$data['ValorCatastralCorrecto'].", '".$data['Observaciones']."', '".$data['IdEmpleadoBarra']."');";
        $q = $sql1 . $sqlv;
        
        //echo $q;
        return $this->db->query($q);
        
    }


    public function guardar_declaracion($data = null)
    {
        //verificar si se envian los documentos
        $doc1 = isset($data['doc1']) ? 1 : 0;
        $doc2 = isset($data['doc2']) ? 1 : 0;
        $doc3 = isset($data['doc3']) ? 1 : 0;
        $doc4 = isset($data['doc4']) ? 1 : 0;
        $doc5 = isset($data['doc5']) ? 1 : 0;
        $doc6 = isset($data['doc6']) ? 1 : 0;


        //calcular los dias de entrega
        //tramite 16.0 son 0 dias
        //tramite 18.0 son 74 dias
        //demas tramites pondremos 20 dias
        $fechacap = $data['FechaCaptura'];
        $fechaentrega = date('Y-m-d', strtotime($fechacap));
        if($data['IdTramite'] == '18.0')
        {
            $fechaentrega = date('Y-m-d', strtotime($fechacap. ' + 74 days'));
        }
        else 
        {
            $fechaentrega = date('Y-m-d', strtotime($fechacap. ' + 20 days'));
        }
        //Se crea la primera parte del comando insertar
        $sql = 'INSERT INTO "Inco_Barra" ("IdInconformidad","IdTramite","Poblacion","Cuartel","Manzana","Predio","Unidad","Clave","IdDeclaracion","NombrePropietario","NombreSolicitante","TelefonoSolicitante","DomicilioSolicitante","IdPropietario","IdColonia","IdCalle","NumeroOficial","ValorCatastral","IdUso","ObservacionesBarra","IdEmpleadoBarra","FechaCapturaInconformidad","TotalSuperficieTerreno","TotalSuperficieConstruccion","IdDocumento1","IdDocumento2","IdDocumento3","IdDocumento4","IdDocumento5","IdDocumento6","AnioDeclaracion","TramiteNotario","FechaEntregaTramite","TramiteICES" ) ';
        //Se crea la seccion de los valores para el comando insertar con los datos del arary que obtenemos del controlador 
        $sqlv = "VALUES('".$data['IdInconformidad']."', '".$data['IdTramite']."', 4, '".$data['Cuartel']."',".$data['Manzana'].",".$data['Predio'].", 1,'".$data['Cuartel'].$data['Manzana'].$data['Predio']."', '".$data['IdDeclaracion']."','".$data['Propietario']."', '".$data['Solicitante']."','".$data['TelSol']."','".$data['DomSol']."',0,0,0,'".$data['Numero']."',".$data['ValorCat'].", '".$data['Uso']."','".$data['Observaciones']."','".$data['IdEpleadoBarra']."','".$data['FechaCaptura']."','".$data['TotalTerreno']."','".$data['TotalConst']."', $doc1, $doc2, $doc3, $doc4, $doc5, $doc6, '".$data['AnioDeclaracion']."', '".$data['Notario']."',  '$fechaentrega', ' ' )";

        $q = $sql . $sqlv;
        //echo $q;
        return $this->db->query($q);
    }

    public function actualiza_foto($data = null)
    {
        $base = $data['foto'];
        $prop = $data['IdPropietario'];
        $sql = "UPDATE a_prop SET \"FOTO\" = decode('$base', 'base64') WHERE \"CVE_PROP\" = ".$prop;
        return $this->db->query($sql);
        
    }

    public function guardar_seguimiento_cdi($data = null){
        $a = $data['id_anio'];
        $fol = $data['id_folio'];
        $obs = $data['obs'];
        $clave = $data['clave'];
        $verificador = $data['verificador'];
        
        $sql = "INSERT INTO barra_captura_cdi (id_folio, id_anio, fecha_envio, estatus_verificacion, verificador, fecha_captura, capturista, clave, obs)
        SELECT '$fol' AS id_folio, '$a' AS id_anio, NOW() AS fecha_envio, 'NP' as estatus_verificacion, '$verificador' as verificador, NULL AS fecha_captura, NULL as capturista, '$clave' as clave, '$obs' as obs  FROM barra_captura_cdi
        WHERE NOT EXISTS(
                    SELECT id_folio, id_anio FROM barra_captura_cdi WHERE id_folio = '$fol' and id_anio = '$a'
            )
        LIMIT 1";

        return $this->db->query($sql);
    
    }
    public function guardar_seguimiento_ventanilla($data = null){

        $QuienLoTiene = "DA";
        $EstadoActual = "TC";
        $miClave = "";

        if($data['IdTramite'] == '16.0')
        {
            $QuienLoTiene = "EI";
            $EstadoActual = "CO";
        }

        $fechacap = $data['FechaCaptura'];
        $fechalimiteInspeccion = date('Y-m-d', strtotime($fechacap. ' + 7 days'));
        $fechalimiteGraficacion = date('Y-m-d', strtotime($fechacap. ' + 10 days'));
        $fechalimiteICES = date('Y-m-d', strtotime($fechacap. ' + 17 days'));

        $sql = 'INSERT INTO "Barra_Seguimiento" ("Id_FolioBarra", "IdDeclaracion", "F_Captura", "FechaLimiteEnInspeccion", "FechaLimiteEnGraficacion", "FechaLimiteEnIces", "Id_EstadoActual", "Id_QuienLoTiene", "Id_Anio", "Nombre_Propietario", "Pobl", "Ctel", "Manz", "Pred", "Unid", clave, "Val_Terr", "Val_Const", "ValCatInicio", "PredialInicio", "TramiteNotario", "TramiteIces") ';

        $sqlvalues = "VALUES ('000".$data['IdInconformidad']."', '".$data['IdDeclaracion']."', '".$data['FechaCaptura']."', '$fechalimiteInspeccion', '$fechalimiteGraficacion', '$fechalimiteICES', '$EstadoActual', '$QuienLoTiene', '".$data['AnioDeclaracion']."', '".$data['Propietario']."', '004', '".$data['Cuartel']."', ".$data['Manzana'].", ".$data['Predio'].", '001', '".$data['Clave']."', 0, 0,0, 0, '".$data['Notario']."', ' ')";

        $q = $sql . $sqlvalues;
        //Ejecutar la insercion;
        return $this->db->query($q);
    }
    
    //FUNCIONES PARA LOS REPORTES DE LA VENTANILLA

    public function reporte_listadodeclaraciones($d = null, $f1 = null, $f2 = null)
    {
        if($f1 != null && $f2 != null){
            $filtro = " AND A.\"FechaCapturaInconformidad\" BETWEEN '$f1' AND '$f2' ";
        }

        return $this->db->query('SELECT A.*,A."NombrePropietario" AS wpropietario,C."DescripcionTramite",d."Nombre",CONCAT (G."NOM_CALLE",\' \',e."NOM_COL") AS wubicacion,f."NOM_POBL",h."DES_USO",i."Declaracion" FROM "Inco_Barra" AS A,a_prop AS b,"Inco_Tramites" AS C,"Gen_Empleados" AS d,a_col AS e,a_pobl AS f,a_calle AS G,a_usosayto AS h,"Inco_Declaracion" AS i WHERE A."IdPropietario"=b."CVE_PROP" AND A."IdTramite"=C."IdTramite" AND A."IdEmpleadoBarra"=d."IdEmpleado" AND A."Poblacion"=G."CVE_POBL" AND A."IdCalle"=G."CVE_CALLE" AND A."Poblacion"=e."CVE_POBL" AND A."IdColonia"=e."CVE_COL" AND A."Poblacion"=f."CVE_POBL" AND A."IdUso"=h."CVE_USO" AND A."IdDeclaracion"=i."IdDeclaracion" AND A."IdDeclaracion"=\''.$d.'\' '.$filtro.' ORDER BY d."Nombre",A."IdInconformidad";')->getResult();
    }

    public function reporte_listado_numero_tramites($d = null)
    {
        $sql = 'DELETE FROM "Inco_BarraListado";

        INSERT INTO "Inco_BarraListado" 
        SELECT c."DescripcionTramite" AS descripcionTramite, a."TramiteICES" AS TramiteIces, COUNT(a."Clave") AS totalUCM, 0.00 AS totalICES  
        FROM "Inco_Barra" AS a,  "Inco_Tramites" AS c 
        WHERE a."IdTramite"=c."IdTramite" AND a."Cuartel" 
        NOT IN (9, 10, 11, 12, 13, 14, 15, 21, 22, 23, 25, 27, 28, 29, 30, 31, 32, 33, 34, 37, 39, 42, 43, 44, 45, 46, 47, 48, 53, 54) AND a."IdDeclaracion" = \''.$d.'\'  
        GROUP BY c."DescripcionTramite", a."TramiteICES";
        
        INSERT INTO "Inco_BarraListado" 
        SELECT c."DescripcionTramite" AS descripcionTramite, a."TramiteICES" AS TramiteIces, 0.00 AS totalUCM, COUNT(a."Clave") AS totalICES
        FROM "Inco_Barra" AS a,  "Inco_Tramites" AS c 
        WHERE a."IdTramite"=c."IdTramite" AND a."Cuartel" IN (9, 10, 11, 12, 13, 14, 15, 21, 22, 23, 25, 27, 28, 29, 30, 31, 32, 33, 34, 37, 39, 42, 43, 44, 45, 46, 47, 48, 53, 54) AND a."IdDeclaracion" = \''.$d.'\'  
        GROUP BY c."DescripcionTramite", a."TramiteICES";
        
        SELECT  "descripcionTramite",  SUM("totalUCM") AS tUCM,  SUM("totalICES") AS tICES, SUM("totalUCM" + "totalICES") AS totalG 
        FROM "Inco_BarraListado"
        GROUP BY "descripcionTramite";';

        return $this->db->query($sql)->getResult();
    }

    public function reporte_primer_registro($f1 = null, $f2 = null)
    {
        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"FechaCapturaInconformidad\" BETWEEN '$f1' AND '$f2' ";
        }

        $sql = 'SELECT a.*, a."NombrePropietario" AS wpropietario, c."DescripcionTramite", d."Nombre", CONCAT(g."NOM_CALLE" , \' \' , e."NOM_COL") AS wubicacion, f."NOM_POBL", h."DES_USO", i."Declaracion"
        FROM "Inco_Barra" AS a, a_prop AS b, "Inco_Tramites" AS c,"Gen_Empleados" AS d, a_col AS e, a_pobl AS f, a_calle AS g, a_usosayto AS h, "Inco_Declaracion" As i 
        WHERE a."IdPropietario"=b."CVE_PROP" AND a."IdTramite"=c."IdTramite"
        AND a."IdEmpleadoBarra" = d."IdEmpleado" 
        AND a."Poblacion"= g."CVE_POBL" 
        AND a."IdCalle" = g."CVE_CALLE" 
        AND a."Poblacion"= e."CVE_POBL" 
        AND a."IdColonia" = e."CVE_COL" 
        AND a."Poblacion"=f."CVE_POBL" 
        AND a."IdUso"=h."CVE_USO" 
        AND a."IdDeclaracion" = i."IdDeclaracion"
        and a."IdTramite" = \'20.0\'
        '.$filtro.'
        ';

        return $this->db->query($sql)->getResult();
    }

    public function reporte_inco_inspeccionadas(){

        $sql = 'SELECT ib."IdInconformidad", ib."Clave", ib."FechaCapturaInconformidad", it."DescripcionTramite", ib."TramiteICES" FROM "Inco_Barra" ib
        INNER JOIN "Inco_Tramites" it on ib."IdTramite" = it."IdTramite"
        WHERE "TramiteICES" = \'IICES\' OR "TramiteICES" = \'INCON\'
        '.$filtro.'
        ORDER BY "IdInconformidad" asc';

        return $this->db->query($sql)->getResult();
    }

    public function reporte_inco_x_clavecat($d = null){
        $sql = 'SELECT "Clave", "AnioDeclaracion", "Inco_Barra"."IdTramite", "Inco_Tramites"."DescripcionTramite", COUNT(*) as total  
        FROM "Inco_Barra", "Inco_Tramites"  
        WHERE "IdDeclaracion" = \''.$d.'\' AND "Inco_Barra"."IdTramite" = "Inco_Tramites"."IdTramite"  
        GROUP BY "Clave", "AnioDeclaracion", "Inco_Barra"."IdTramite", "Inco_Tramites"."DescripcionTramite"  
        HAVING COUNT(*) > 1 ';

        return $this->db->query($sql)->getResult();
    }

    public function reporte_cuartel($d = null, $t = null, $f1 = null, $f2 = null){  
        
        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"FechaCapturaInconformidad\" BETWEEN '$f1' AND '$f2' ";
        }
        else{
            $filtro = "";
        }

        if($d != '00'){
            $sql = '
            SELECT a."Cuartel", COUNT(a."Clave") AS total    
            FROM "Inco_Barra" AS a,  "Inco_Tramites" AS c  
            WHERE 
                    a."IdTramite"=c."IdTramite"
                    and a."IdDeclaracion" = \''.$d.'\'
                    '.$filtro.'
                    GROUP BY a."Cuartel"';

            if($t == '01'){
                        $sql = '
                        SELECT a."Cuartel", COUNT(a."Clave") AS total    
                        FROM "Inco_Barra" AS a,  "Inco_Tramites" AS c  
                        WHERE 
                                a."IdTramite"=c."IdTramite" 
                                and a."IdDeclaracion" = \''.$d.'\' 
                                AND a."Cuartel" NOT IN (9, 10, 11, 12, 13, 14, 15, 21, 22, 23, 25, 27, 28, 29, 30, 31, 32, 33, 34, 37, 39, 42, 43, 44, 45, 46, 47, 48, 53, 54)
                                '.$filtro.'
                                GROUP BY a."Cuartel"';
            }
            
            if($t == '02'){
                        $sql = '
                        SELECT a."Cuartel", COUNT(a."Clave") AS total
                        FROM "Inco_Barra" AS a, "CuartelICES" AS b, "Inco_Tramites" AS c 
                        WHERE a."IdTramite"=c."IdTramite" and a."IdDeclaracion" = \''.$d.'\'  
                        and a."Poblacion"=b."Poblacion"  AND a."Cuartel"=b."Cuartel"
                        '.$filtro.'
                        GROUP BY a."Cuartel"';
                    }        

        }else{
            $sql = '
            SELECT a."Cuartel", COUNT(a."Clave") AS total    
            FROM "Inco_Barra" AS a,  "Inco_Tramites" AS c  
            WHERE 
                    a."IdTramite"=c."IdTramite"
                    '.$filtro.'
                    GROUP BY a."Cuartel"';
        }
        

        
       
        return $this->db->query($sql)->getResult();
    }

    public function reporte_cdi($f1 = null, $f2 = null){

        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"FechaCaptura\" BETWEEN '$f1' AND '$f2' ";
        }
        else{
            $filtro = "";
        }

        $sql = 'SELECT a.*, d."Nombre" 
        FROM "Inco_AvisoCambio" AS a, "Gen_Empleados" AS d 
        WHERE a."IdEmpleadoBarra" = d."IdEmpleado" 
        '.$filtro.'
        ';

        return $this->db->query($sql)->getResult();
    }

    public function reporte_oficiobonificacion($f1 = null, $f2 = null){

        if($f1 != null && $f2 != null){
            $filtro = " WHERE \"FechaCaptura\" BETWEEN '$f1' AND '$f2' ";
        }
        else{
            $filtro = "";
        }

        $sql = 'SELECT * FROM "Barra_OficioBonificacion" '.$filtro.'';
        
        return $this->db->query($sql)->getResult();
    }

    public function lisget_oficiosbonificaciontar_cdi($a = null)
    {
        return $this->db->query('SELECT * FROM "Barra_OficioBonificacion" WHERE "IdAnio"=\''.$a.'\'')
                    ->getResult();
    }

    public function guardar_seguimiento($data = null){
        
        //Se actualiza la tabla de Barra Seguimiento 
        $sql1 = 'UPDATE "Barra_Seguimiento" 
        SET "Id_QuienLoTiene" = \''.$data["Id_QuienLoTiene"].'\', 
        "Id_EstadoActual" = \''.$data["Id_EstadoActual"].'\', 
        "ProcedioSN" = \''.$data["ProcedioSN"].'\',
        "Observaciones" = \''.$data["Observaciones"].'\' 
        WHERE "Id_FolioBarra" = \''.$data["Id_FolioBarra"].'\' AND "Id_Anio" = \''.$data["Id_Anio"].'\'; ';

       /* $sql2 = "";

        if ($data["F_EnvioIces"] != ''){
            $sql2 = 'UPDATE "segui_MovimientosBarra" SET "fec_ava" = '.$data["F_AvaluoManual2"].' WHERE "Clave"= \''.$data["Clave"].'\' AND "fec_ava" IS NULL AND "FechaOficio" <= \''.$data["F_EnvioIces"].'\'';
        }
        
        $q = $sql1 . $sql2;*/
        //return $q;
        return $this->db->query($sql1);

    }

    public function enviar_tramite_captura_cdi($data = null){
        $folio = $data['id_folio'];
        $anio = $data['id_anio'];
        $cvecat = $data['cvecat'];
        $verificador = $data['verificador'];

        $sql = "INSERT INTO barra_captura_cdi VALUES (default, '$folio', '$anio', NOW(), 'V', '$verificador', null, null, '$cvecat'); ";

        $q = $sql;
        //echo $q;
        return $this->db->query($q);
    }

    public function enviar_tramite_captura($data = null){
        $folio = $data['id_folio'];
        $anio = $data['id_anio'];
        $tramite = $data['idtramite'];
        $cvecat = $data['cvecat'];
        $verificador = $data['verificador'];

        $sql = "INSERT INTO barra_captura VALUES (default, '$folio', '$anio', '$tramite', NOW(), 'V', '$verificador', null, null, '$cvecat'); ";

        //Se actualiza la tabla de Barra Seguimiento 
        $sql2 = 'UPDATE "Barra_Seguimiento" 
        SET "Id_QuienLoTiene" = \'DC\', 
        "Id_EstadoActual" = \'TC\', 
        "ProcedioSN" = \'S\'
        WHERE "Id_FolioBarra" = \''.$folio.'\' AND "Id_Anio" = \''.$anio.'\'; ';

        $q = $sql . $sql2;
        //echo $q;
        return $this->db->query($q);
    }

    //CAPTURA

    public function registrar_nueva_clave_catastral($data = null){
        $clave = $data['POBL'].$data['CTEL'].$data['MANZ'].$data['PRED'].$data['UNID'];

        $p1 = intval($data['POBL']);
        $p2 = intval($data['CTEL']);
        $p3 = intval($data['MANZ']);
        $p4 = intval($data['PRED']);
        $p5 = intval($data['UNID']);

        $a = $data['a'];
        $fol = $data['fol'];
        $cvemaster = $data['cvemaster'];
        $verificador = $data['verificador'];

        $tramite = $data['tramite'];

        $fecha_altaymod = date("Y/m/d");

        $sql_clave = 'DELETE FROM a_unid_temp;
        INSERT INTO a_unid_temp SELECT * FROM a_unid WHERE "CLAVE" = \''.$cvemaster.'\';
        UPDATE a_unid_temp SET "CVE_POBL" = '.$p1.', "NUM_CTEL" = '.$p2.', "NUM_MANZ" = '.$p3.', "NUM_PRED" = '.$p4.', "NUM_UNID" = '.$p5.', "NUM_FTES" = 0, "LON_FTE" = 0, "LON_FONDO" = 0, "FEC_ALTA" = NOW()::date, "FEC_MOV" = NOW()::date, "SUP_TERR" = 0, "SUP_CONST" = 0, "VAL_TERR" = 0, "VAL_CONST" = 0, "SUP_TERR_A" = 0, "SUP_CONST_" = 0, "VAL_TERR_A" = 0, "VAL_CONST_" = 0, "CLAVE" = \''.$clave.'\', "CVE_PROP" = NULL;
        INSERT INTO a_unid SELECT * FROM a_unid_temp WHERE "CLAVE" = \''.$clave.'\' AND NOT EXISTS (SELECT "CLAVE" FROM a_unid WHERE "CLAVE" = \''.$clave.'\'); ';

        $sql_folio = "DELETE FROM \"Inco_Barra_temp\"  where \"AnioDeclaracion\" = '$a' and \"IdInconformidad\" = '$fol';
        INSERT INTO \"Inco_Barra_temp\" SELECT * from \"Inco_Barra\" where \"AnioDeclaracion\" = '$a' and \"IdInconformidad\" = '$fol';
        UPDATE \"Inco_Barra_temp\" SET \"Poblacion\" = $p1, \"Cuartel\" = $p2, \"Manzana\" = $p3, \"Predio\" = $p4, \"Unidad\" = $p5, \"Clave\" = '$clave', \"ValorCatastral\" = 0, \"TotalSuperficieTerreno\" = 0, \"TotalSuperficieConstruccion\" = 0, \"ObservacionesBarra\" = 'Clave Generada por Desmancomunacion de la clave $cvemaster';
        INSERT INTO \"Inco_Barra\" SELECT * from \"Inco_Barra_temp\" where \"AnioDeclaracion\" = '$a' and \"IdInconformidad\" = '$fol'; ";

        $sqlcaptura = "INSERT INTO barra_captura VALUES (DEFAULT, '000$fol', '$a', '4.0', NOW(), 'GC', '$verificador', null, null, '$clave'); ";

        /*$sql_actualizar_tablas = "update \"Inco_Barra\" set \"Poblacion\" = $p1, \"Cuartel\" = $p2, \"Manzana\" = $p3, \"Predio\" = $p4, \"Unidad\" = $p5, \"Clave\" = '$clave' where \"IdInconformidad\" = '$p7' and \"AnioDeclaracion\" = '$p6';
        update barra_captura set clave = '$clave' where id_folio = '000$p7' and id_anio = '$p6';";*/

        if($tramite == '20.0'){
            $sql_clave = "INSERT INTO a_unid (\"CVE_POBL\", \"NUM_CTEL\", \"NUM_MANZ\", \"NUM_PRED\", \"NUM_UNID\", \"FEC_ALTA\", \"FEC_MOV\", \"STT_COND\", \"CLAVE\")
            SELECT $p1, $p2, $p3, $p4, $p5, '$fecha_altaymod', '$fecha_altaymod', 'N', '$clave' 
            WHERE NOT EXISTS (SELECT '$clave' FROM a_unid WHERE \"CLAVE\" = '$clave'); ";
            $sql_folio = "";
            $sqlcaptura = "";
        }
        
        $q = $sql_clave.$sql_folio.$sqlcaptura;
        //echo $q;
        return  $this->db->query($q);
        
        //return $this->db->affectedRows();
       
    }

    public function registrar_uso($data = null){
        $p1 = $data['CVE_POBL'];
        $p2 = $data['NUM_CTEL'];
        $p3 = $data['NUM_MANZ'];
        $p4 = $data['NUM_PRED'];
        $p5 = $data['NUM_UNID'];
        $p6 = $data['NUM_USO'];
        $p7 = $data['CVE_GPO_USO'];
        $p8 = $data['CVE_USO'];
        $p9 = $data['CLAVE'];

        $p8_int = intval($p8);

        $cve_uso = $p7.$p8;

        //insertamos en la tabla de usos de unidades
        $sql = "INSERT INTO a_uso_u VALUES ($p1, $p2, $p3, $p4, $p5, $p6, '$p7', $p8_int, '$p9'); ";

        //si el uso es el principal osea el 1 actualizamos la tabla de unidad y la del geometrico
        if($p6 == "1"){
            $sql_unid = "UPDATE a_unid SET \"CVE_USO\" = '$cve_uso' WHERE \"CLAVE\" = '$p9';";
            $sql_pred = "UPDATE \"Culiacan_Join\" SET num_uso1 = '$p6', cve_gpo_us = '$p7' , cve_uso1 = '$p8_int' WHERE dg_cat = '007$p9' ;";
            $q = $sql . $sql_unid . $sql_pred;
        }
        else{ 
            //solo actualizamos el geometrico el uso 2
            $sql_pred = "UPDATE \"Culiacan_Join\" SET num_uso2 = '$p6', cve_gpo_u1 = '$p7' , cve_uso2 = '$p8_int' WHERE dg_cat = '007$p9';
            ";
            $q = $sql . $sql_pred;
        }
        
        //echo $q;
        return $this->db->query($q);
    }

    public function registrar_propietario($data = null){
        $p1 = $data['CVE_PROP'];
        $p2 = $data['TIP_PERS'];
        $p3 = $data['APE_PAT'];
        $p4 = $data['APE_MAT'];
        $p5 = $data['NOM_PROP'];
        $p6 = $data['REG_FED'];
        $p7 = $data['NUM_PROP'];
        $p8 = $data['CLAVE'];

        $sql = "INSERT INTO a_prop VALUES ($p1, '$p2', '$p3', '$p4', '$p5', '$p6', 'N', 'N'); ";

        $sql_u = "INSERT INTO a_prop_u VALUES($p1, $p7, '$p8', null); ";


        //echo $sql . $sql_u;
        return $this->db->query($sql . $sql_u );
    }
	
	public function registrar_propietario_cdi($data = null){
        $p1 = $data['CVE_PROP'];
        $p2 = $data['TIP_PERS'];
        $p3 = $data['APE_PAT'];
        $p4 = $data['APE_MAT'];
        $p5 = $data['NOM_PROP'];
        $p6 = $data['REG_FED'];
        $p7 = $data['NUM_PROP'];

        $sql = "INSERT INTO a_prop VALUES ($p1, '$p2', '$p3', '$p4', '$p5', '$p6', 'N', 'N'); ";

        //echo $sql;
        return $this->db->query($sql . $sql_u );
    }

    public function actualizar_propietario($data = null){
        $p1 = $data['CVE_PROP'];
        $p2 = $data['TIP_PERS'];
        $p3 = $data['APE_PAT'];
        $p4 = $data['APE_MAT'];
        $p5 = $data['NOM_PROP'];
        $p6 = $data['REG_FED'];
        $p7 = $data['NUM_PROP'];
        $p8 = $data['CLAVE'];

        $sql = "UPDATE a_prop SET \"TIP_PERS\" = '$p2', \"APE_PAT\" = '$p3', \"APE_MAT\" = '$p4',\"NOM_PROP\" = '$p5', \"REG_FED\" = '$p6' WHERE \"CVE_PROP\" = $p1 ;";

        //echo $sql . $sql_u;
        return $this->db->query($sql);
    }

    public function actualizar_servicios($data = null){
        $s1 = $data['SERV1'];
        $s2 = $data['SERV2'];
        $s3 = $data['SERV3'];
        $s4 = $data['SERV4'];
        $s5 = $data['SERV5'];
        $s6 = $data['SERV6'];
        $s7 = $data['SERV7'];
        $s8 = $data['SERV8'];
        $s9 = $data['SERV9'];
        $clave = $data['clave'];

        $cve = $s1.$s2.$s3.$s4.$s5.$s6.$s7.$s8.$s9."00";

        $sql = "UPDATE a_unid SET \"STS_SERVIC\" = '$cve' WHERE \"CLAVE\" = '$clave'";
        //echo $sql;
        return $this->db->query($sql);
    }

    public function actualizar_sup_val_construcciones($data = null){
        $p1 = $data['SUP_CONST'];
        $p2 = $data['VAL_CONST'];
        $p3 = $data['clave'];

        $sql = "UPDATE a_unid SET \"SUP_CONST\"=$p1,\"VAL_CONST\"=$p2,\"SUP_CONST_\"=\"SUP_CONST\",\"VAL_CONST_\"=\"VAL_CONST\" WHERE \"CLAVE\"='$p3';";

        //echo $sql;
        return $this->db->query($sql);
    }

    public function actualizar_sup_val_terreno($data = null){
        $p1 = $data['SUP_TERR'];
        $p2 = $data['VAL_TERR'];
        $p3 = $data['clave'];

        $sql = "UPDATE a_unid SET \"SUP_TERR\"=$p1,\"VAL_TERR\"=$p2,\"SUP_TERR_A\"=\"SUP_TERR\",\"VAL_TERR_A\"=\"VAL_TERR\" WHERE \"CLAVE\"='$p3';";

        //echo $sql;
        return $this->db->query($sql);
    }

    public function actualizar_construccion($data = null){
        $p1 = $data['NUM_CONST'];
        $p2 = $data['SUP_CONST'];
        $p3 = $data['CVE_CAT_CO'];
        $p4 = $data['CVE_EDO_CO'];
        $p5 = $data['EDD_CONST'];
        $p6 = $data['FEC_CONST'];
        $p7 = $data['clave'];
        $p8 = $data['NUM_CONST_ORI'];

        $sql = "UPDATE a_const SET \"NUM_CONST\" = $p1, \"SUP_CONST\" = $p2, \"CVE_CAT_CO\" = '$p3', \"CVE_EDO_CO\" = $p4, \"EDD_CONST\" = $p5, \"FEC_CONST\" = '$p6' WHERE clave = '$p7' AND \"NUM_CONST\" = $p8;";

        //echo $sql;
        return $this->db->query($sql);
    }

    public function registrar_construccion($data = null){
        $p1 = $data['NUM_CONST'];
        $p2 = $data['SUP_CONST'];
        $p3 = $data['CVE_CAT_CO'];
        $p4 = $data['CVE_EDO_CO'];
        $p5 = $data['EDD_CONST'];
        $p6 = $data['FEC_CONST'];
        $p7 = $data['clave'];

        $sql = "INSERT INTO a_const VALUES ($p1, $p2, '$p3', $p4, $p5, '$p6', '$p7', null);";

        //echo $sql;
        return $this->db->query($sql);
    }

    public function registrar_terreno($data = null){
        
        $p1 = $data['CVE_POBL'];
        $p2 = $data['NUM_TERR'];
        $p3 = $data['TIP_TERR'];
        $p4 = $data['SUP_TERR'];
        $p5 = $data['CVE_CALLE'];
        $p6 = $data['CVE_TRAMO'];
        $p7 = $data['SUP_TERR_E'];
        $p8 = $data['CVE_CALLE_'];
        $p9 = $data['CVE_TRAMO_'];
        $p10 = $data['SUP_TERR_D'];
        $p11 = $data['CVE_DEM_TE'];
        $p12 = $data['CVE_DEM_T2'];
        $p13 = $data['CVE_DEM_T3'];
        $p14 = $data['FAC_DEM_TE'];
        $p15 = $data['clave'];

        $sql = "INSERT INTO a_terr VALUES ($p1, $p2, '$p3', $p4, $p5, $p6, $p7, $p8, $p9, $p10, $p11, $p12, $p13, $p14, '$p15', null);";

        //echo $sql;
        return $this->db->query($sql);
        
    }

    public function actualizar_terreno($data = null){
        $p1 = $data['CVE_POBL'];
        $p2 = $data['NUM_TERR'];
        $p3 = $data['TIP_TERR'];
        $p4 = $data['SUP_TERR'];
        $p5 = $data['CVE_CALLE'];
        $p6 = $data['CVE_TRAMO'];
        $p7 = $data['SUP_TERR_E'];
        $p8 = $data['CVE_CALLE_'];
        $p9 = $data['CVE_TRAMO_'];
        $p10 = $data['SUP_TERR_D'];
        $p11 = $data['CVE_DEM_TE'];
        $p12 = $data['CVE_DEM_T2'];
        $p13 = $data['CVE_DEM_T3'];
        $p14 = $data['FAC_DEM_TE'];
        $p15 = $data['clave'];
        $p16 = $data['NUM_TERR_ORI'];

        $sql = "UPDATE a_terr SET \"NUM_TERR\" = $p2, \"TIP_TERR\" = '$p3', \"SUP_TERR\" = $p4, \"CVE_CALLE\" = $p5, \"CVE_TRAMO\" = $p6, \"SUP_TERR_E\" = $p7, \"CVE_CALLE_\" = $p8, \"CVE_TRAMO_\" = $p9, \"SUP_TERR_D\" = $p10, \"CVE_DEM_TE\" = $p11, \"CVE_DEM_T2\" = $p12, \"CVE_DEM_T3\" = $p13, \"FAC_DEM_TE\" = $p14 WHERE clave = '$p15' AND \"NUM_TERR\" = $p16 ";

        //echo $sql;
        return $this->db->query($sql);
    }

    public function eliminar_uso($data = null)
    {    
        $clave = $data['clave'];
        $num = $data['numero'];

        //eliminar de la tabla a_uso_u
        $sql = "DELETE FROM a_uso_u WHERE \"CLAVE\" = '$clave' AND \"NUM_USO\" = $num ;";

        if($num == '1'){

            //eliminar de la tabla de a_uso_u y actualizar la tabla a_unid y el padron geometrico
            $sql_unid = "UPDATE a_unid SET \"CVE_USO\" = null WHERE \"CLAVE\" = '$clave'; ";

            $sql_padron = "UPDATE \"Culiacan_Join\" SET num_uso1 = null, cve_gpo_us = null , cve_uso1 = null WHERE dg_cat = '007$clave'; ";

            $q = $sql . $sql_unid . $sql_padron;
        }
        else{
            //eliminar de la tabla a_uso_u
            $sql_padron = "UPDATE \"Culiacan_Join\" SET num_uso2 = null, cve_gpo_u1 = null , cve_uso2 = null WHERE dg_cat = '007$clave'; ";
            $q = $sql . $sql_padron;
        }
        //Insertar en barra paquete envio inspeccion
        
        //echo $q;
        //Ejecutar la eliminacion;
        return $this->db->query($q);
    }

    public function establecer_propietario($data = null){
        $clave      = $data['CLAVE'];
        $cve_prop   = $data['CVE_PROP'];
        $tip_per    = $data['TIP_PERS'];
        $ape_pat    = $data['APE_PAT'];
        $ape_mat    = $data['APE_MAT'];
        $nom_prop   = $data['NOM_PROP'];
        $reg_fed    = $data['REG_FED'];

        $nom_comp = $ape_pat." ".$ape_mat." ".$nom_prop;

        $sql = "UPDATE a_unid SET \"CVE_PROP\" = $cve_prop WHERE \"CLAVE\" = '$clave'; ";
        $sql2 = "UPDATE \"Culiacan_Join\" SET nom_comp_t = '$nom_comp' WHERE dg_cat = '007$clave';";
        //echo $sql.$sql2;
        return $this->db->query($sql.$sql2);
        //echo $this->db->affectedRows();
        //return $this->db->query($sql);
    }

    public function eliminar_propietario($data = null)
    {    
        $clave = $data['clave'];
        $num = $data['numero'];
        $prop = $data['cve_prop'];
        //Insertar en barra paquete envio inspeccion
        //$sql = "DELETE FROM a_prop WHERE \"CVE_PROP\" = $prop; ";

        $sql2 = "DELETE FROM a_prop_u WHERE cve_prop = $prop AND \"CLAVE\" = '$clave'; ";

        //echo $sql . $sql2;
        //Ejecutar la eliminacion;
        return $this->db->query($sql2);
    }


    public function eliminar_construccion($data = null)
    {    
        $clave = $data['clave'];
        $num = $data['numero'];
        $tipo = $data['tipo'];
        //Insertar en barra paquete envio inspeccion
        $sql = "DELETE FROM a_const WHERE clave = '$clave' AND \"NUM_CONST\" = $num AND \"CVE_CAT_CO\" = '$tipo' ;";
        //echo $sql;
        //Ejecutar la eliminacion;
        return $this->db->query($sql);
    }

    public function eliminar_terreno($data = null)
    {    
        $clave = $data['clave'];
        $num = $data['numero'];
        $tipo = $data['tipo'];
        //Insertar en barra paquete envio inspeccion
        $sql = "DELETE FROM a_terr WHERE clave = '$clave' AND \"NUM_TERR\" = $num AND \"TIP_TERR\" = '$tipo' ;";
        //echo $sql;
        //Ejecutar la eliminacion;
        return $this->db->query($sql);
    }



    public function actualiza_generales($data = null){
        $campo = $data['campo'];
        $valor = $data['valor'];
        $clave = $data['clave'];

        $sql = "UPDATE a_unid SET \"$campo\" = '$valor' WHERE \"CLAVE\" = '$clave'; ";

        if($campo == 'DOM_NOT' || $campo == 'CVE_CALLE_' || $campo == 'NUM_OFIC_U' || $campo == 'UBI_PRED' || $campo == 'CVE_COL_UB' ){
            if($campo == "CVE_CALLE_")
                $campo = "cve_calle";
            
            if($campo == "CVE_COL_UB")
                $campo = "cve_col";

            $sql .= "UPDATE \"Culiacan_Join\" SET $campo = '$valor' WHERE dg_cat = '007$clave'; ";
        }

        if($campo == "NOM_CALLE"){
            $sql = "UPDATE \"Culiacan_Join\" SET $campo = '$valor' WHERE dg_cat = '007$clave'; ";
        }

        if($campo == "NOM_COL"){
            $sql = "UPDATE \"Culiacan_Join\" SET $campo = '$valor' WHERE dg_cat = '007$clave'; ";
        }


        //echo $sql;
        return $this->db->query($sql);
    }


    public function datatable($data = null){
        $draw = $data['draw'];
        $start = $data['start'];
        $length = $data['length'];
        $search_value = $data['search_value'];
        $table = $data['table'];
        $searchcolumn = $data['searchcolumn'];

        
        if(!empty($search_value)){
            // If we have value in search, searching by id, name, email, mobile

            // count all data
            $total_count = $this->db->query("SELECT * from $table WHERE $searchcolumn like '%".$search_value."%'")->getResult();

            $data = $this->db->query("SELECT * from $table WHERE $searchcolumn like '%".$search_value."%' limit $length OFFSET $start")->getResult();
            
            if($table == 'a_prop'){
                $data = $this->db->query('SELECT "CVE_PROP", "TIP_PERS", "APE_PAT", "APE_MAT", "NOM_PROP", "REG_FED", "STT_AFE_SU", "STT_AFE_IN", encode("FOTO", \'base64\') as "foto" from a_prop WHERE CONCAT("APE_PAT",\' \',"APE_MAT",\' \',"NOM_PROP") like \'%'.$search_value.'%\' limit '.$length.' OFFSET '.$start)->getResult();
            }
        }else{
            // count all data
            $total_count = $this->db->query("SELECT * from $table")->getResult();

            $query = "SELECT * FROM $table LIMIT $length OFFSET $start";

            if($table == 'a_prop'){
                $query ='SELECT "CVE_PROP", "TIP_PERS", "APE_PAT", "APE_MAT", "NOM_PROP", "REG_FED", "STT_AFE_SU", "STT_AFE_IN", encode("FOTO", \'base64\') as "foto" FROM a_prop limit '.$length.' OFFSET '.$start;
            }
            //echo $query;
            // get per page data
            $data = $this->db->query($query)->getResult();
            
        }
        
        $json_data = array(
            "draw" => intval($draw),
            "recordsTotal" => count($total_count),
            "recordsFiltered" => count($total_count),
            "data" => $data   // total data array
        );

        return $json_data;
    }

    public function consultar_usuario_perfil($id = null){
        $query1 = "SELECT * FROM usuario_perfil WHERE id_usuario = $id;";
        return $this->db->query($query1)->getResult();
    }

    public function registrar_usuario_perfil($data = null){
        $id = $data['id'];
        $nombre = $data['nombre'];
        $rfc = $data['rfc'];
        $direccion = $data['direccion'];
        $telefono = $data['telefono'];
        $celular = $data['celular'];
        $correo = $data['correo'];

        $sql = "INSERT INTO usuario_perfil VALUES(default, $id,'$rfc','$nombre','$direccion','$telefono', '$celular' ,'$correo', null, null, null, '')
        ON CONFLICT (id_usuario) 
        DO
        UPDATE SET rfc = '$rfc',nombre = '$nombre', direccion = '$direccion',telefono = '$telefono', celular = '$celular' , correo = '$correo'; ";
        //echo $sql;
        //Ejecutar la insercion;
        return $this->db->query($sql);
    }
    


}