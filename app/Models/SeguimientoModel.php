<?php namespace App\Models;
use CodeIgniter\Model;
//incluimos la interfaz para la coneccion con la base de datos
use CodeIgniter\Database\ConnectionInterface;

class SeguimientoModel extends Model {

    protected $db;
	public function __construct(ConnectionInterface &$db) {
		$this->db =& $db;
    }

    public function get_ventanilla_seguimiento() {
		$sql = 'SELECT bs.*, ea."Descripcion", ea."Estado", ql."Descripcion" as "dpto", ql."Estado" as "dpto_estado" FROM "Barra_Seguimiento" bs
        INNER JOIN "Catalogo_EstadoActual" ea on bs."Id_EstadoActual" = ea."Id_EstadoActual"
        INNER JOIN "Catalogo_QuienLoTiene" ql on ql."Id_QuienLoTiene" = bs."Id_QuienLoTiene"
        ORDER BY bs."Id_Anio" DESC;';
        return $this->db->query($sql)->getResult();
    }

    public function get_cdi_seguimiento(){
        $sql = 'SELECT B."AnioAviso",B."Folio",B."FechaCaptura",B."PropietarioActual", B."PropietarioAnterior",B."Poblacion",B."Cuartel",B."Manzana",B."Predio",B."Unidad", CONCAT(B."Poblacion",B."Cuartel", B."Manzana", B."Predio", B."Unidad") as "Clave" FROM "Inco_AvisoCambio" AS B ORDER BY 2';
        return $this->db->query($sql)->getResult();
    }

    public function get_bandeja_captura($year = null){
        $sql = 'SELECT A."Id_Anio",
        A."Id_FolioBarra",
        A."F_Captura",
        A."Pobl",
        A."Ctel",
        A."Manz",
        A."Pred",
        A."Unid",
        C."DescripcionTramite",
        null as "F_EnvioInspeccion",
        null as "DiasInspeccion",
        null as "F_EnvioGraficacion",
        CASE WHEN A."F_EnvioIces" IS NULL THEN
            DATE_PART (\'day\', NOW()::timestamp - A."FechaLimiteEnIces"::timestamp) 
            END AS "DiasInspeccion",
         A."F_EnvioIces"
           -- A."F_EnvioIces" 
        FROM
            "Barra_Seguimiento" A,
            "Inco_Barra" b,
            "Inco_Tramites" C 
        WHERE
            A."Id_Anio" =  \''.$year.'\'
            AND A."F_AvaluoManual" IS NULL 
            AND SUBSTRING ( A."Id_FolioBarra", 4, 5 ) = b."IdInconformidad" 
            AND A."Id_Anio" = b."AnioDeclaracion" 
            AND b."IdTramite" = C."IdTramite" 
            --AND b."IdTramite" <> \'16.0\' 
            AND A."IdDeclaracion" = \'03\' 
            AND "Id_EstadoActual" NOT IN ( \'FC\', \'RE\', \'RI\', \'FD\', \'NL\', \'NE\', \'NC\', \'NP\', \'CO\', \'MR\' ) 
        ORDER BY
            2
        ';

        //return $sql;
        return $this->db->query($sql)->getResult();
    }

    public function get_bandeja_graficacion($year = null){
        $sql = 'SELECT 
        A."Id_Anio",
        A."Id_FolioBarra",
        A."F_Captura",
        A."Pobl",
        A."Ctel",
        A."Manz",
        A."Pred",
        A."Unid",
        C."DescripcionTramite",
        A."F_EnvioInspeccion",
    CASE
            
            WHEN A."F_InspeccionCampo" IS NULL THEN
            DATE_PART (\'day\', NOW()::timestamp - A."FechaLimiteEnInspeccion"::timestamp) 
        END AS "DiasInspeccion",
        A."F_EnvioGraficacion",
        A."F_EnvioIces" 
    FROM
        "Barra_Seguimiento" A,
        "Inco_Barra" b,
        "Inco_Tramites" C 
    WHERE
        A."Id_QuienLoTiene" = \'DG\' 
        AND "F_AtendidoGraficacion" IS NULL 
        AND A."Id_Anio" = \''.$year.'\'
        AND "FechaLimiteEnGraficacion" < NOW ( ) 
        AND A."ProcedioSN" <> \'N\' 
        AND A."F_AvaluoManual" IS NULL 
        AND SUBSTRING ( A."Id_FolioBarra", 4, 5 ) = b."IdInconformidad" 
        AND A."Id_Anio" = b."AnioDeclaracion" 
        AND b."IdTramite" = C."IdTramite" 
        AND b."IdTramite" <> \'16.0\' 
        AND A."IdDeclaracion" = \'03\' 
        AND "Id_EstadoActual" NOT IN (
            \'FC\',
            \'RE\',
            \'RI\',
            \'FD\',
            \'NL\',
            \'NE\',
            \'NC\',
            \'NP\',
            \'CO\',
            \'MR\' 
        ) 
    ORDER BY
        2
        ';

        //return $sql;
        return $this->db->query($sql)->getResult();

    }

    public function get_bandeja_inspeccion($year = null){
        $sql = 'SELECT 
        A."Id_Anio",
        A."Id_FolioBarra",
        A."F_Captura",
        A."Pobl",
        A."Ctel",
        A."Manz",
        A."Pred",
        A."Unid",
        C."DescripcionTramite",
        A."F_EnvioInspeccion",
    CASE
            WHEN A."F_InspeccionCampo" IS NULL THEN
            DATE_PART (\'day\', NOW()::timestamp - A."FechaLimiteEnInspeccion"::timestamp) 
        END AS "DiasInspeccion",
        A."F_EnvioGraficacion",
        A."F_EnvioIces" 
    FROM
        "Barra_Seguimiento" A,
        "Inco_Barra" b,
        "Inco_Tramites" C 
    WHERE
        A."Id_Anio" = \''.$year.'\'
        AND A."F_InspeccionCampo" IS NULL 
        AND "FechaLimiteEnInspeccion" < now( ) 
        AND A."ProcedioSN" <> \'N\' 
        AND A."F_AvaluoManual" IS NULL 
        AND SUBSTRING ( A."Id_FolioBarra", 4, 5 ) = b."IdInconformidad" 
        AND A."Id_Anio" = b."AnioDeclaracion"
        AND b."IdTramite" = C."IdTramite" 
        AND b."IdTramite" <> \'16.0\' 
        AND A."IdDeclaracion" = \'03\' 
        AND "Id_EstadoActual" NOT IN (
            \'FC\',
            \'RE\',
            \'RI\',
            \'FD\',
            \'NL\',
            \'NE\',
            \'NC\',
            \'NP\',
            \'CO\',
            \'MR\' 
        ) 
    ORDER BY
        2';
        //return $sql;
        return $this->db->query($sql)->getResult();

    }

    public function validar_clave($clave = null)
    {
        return $this->db->query('SELECT *, encode("FOTO", \'base64\') as "foto"  FROM a_unid a, a_calle b, a_col c, a_prop d, a_usosayto e, (SELECT row_to_json(fc) AS jsonb_build_object FROM 
        (SELECT \'FeatureCollection\' As type, array_to_json(array_agg(f))
        As features FROM 
        (SELECT 
        \'Feature\' As type, 
        ST_AsGeoJSON(ST_Transform(lg.geom, 4326),15,0)::json As geometry,
        row_to_json((id, dg_ccat, nom_comp_t, nom_calle, nom_col, num_ofic_u, sup_terr, sup_const, val_catast)) As properties
        FROM (SELECT * FROM "Culiacan_Join" WHERE dg_ccat  = \'007'.$clave.'\') As lg) As f ) As fc) fc WHERE a."CLAVE" = \''.$clave.'\' AND a."CVE_POBL" = b."CVE_POBL" AND a."CVE_CALLE_" = b."CVE_CALLE"  AND a."CVE_POBL" = c."CVE_POBL" AND a."CVE_COL_UB" = c."CVE_COL"  AND a."CVE_PROP" = d."CVE_PROP" AND a."CVE_USO" = e."CVE_USO"
                    ')
                    ->getResult();
    }

    public function consulta_tramite($a = null, $tram = null){
        $sql = 'SELECT
        bs.*,
        ea."Descripcion",
        ea."Estado",
        ql."Descripcion" AS "dpto",
        ql."Estado" AS "dpto_estado",
        (SELECT it."DescripcionTramite" FROM "Inco_Barra" ib
        INNER JOIN "Inco_Tramites" it ON ib."IdTramite" = it."IdTramite"
        WHERE ib."IdInconformidad" = \''.substr($tram, 3).'\' and ib."AnioDeclaracion" = \''.$a.'\') as "Tramite",
        (SELECT ib."ObservacionesBarra" FROM "Inco_Barra" ib
        INNER JOIN "Inco_Tramites" it ON ib."IdTramite" = it."IdTramite"
        WHERE ib."IdInconformidad" = \''.substr($tram, 3).'\' and ib."AnioDeclaracion" = \''.$a.'\') as "DescInco",
        (SELECT ib."TelefonoSolicitante" FROM "Inco_Barra" ib
        INNER JOIN "Inco_Tramites" it ON ib."IdTramite" = it."IdTramite"
        WHERE ib."IdInconformidad" = \''.substr($tram, 3).'\' and ib."AnioDeclaracion" = \''.$a.'\') as "TelefonoSolicitante"  
    FROM
        "Barra_Seguimiento" bs
        INNER JOIN "Catalogo_EstadoActual" ea ON bs."Id_EstadoActual" = ea."Id_EstadoActual"
        INNER JOIN "Catalogo_QuienLoTiene" ql ON ql."Id_QuienLoTiene" = bs."Id_QuienLoTiene" 
    WHERE
		 bs."Id_FolioBarra" = \''.$tram.'\' and bs."Id_Anio" = \''.$a.'\'';
        //return $sql;
        return $this->db->query($sql)->getResult();
    }

    public function consulta_inspeccion($a = null, $tram = null){
        $sql = 'SELECT
        bi.*,
        ( SELECT "Descripcion" FROM "Catalogo_MovimientoInspeccion" WHERE "Id_MovimientoInspeccion" = bi."Id_MovimientoInspeccion1" ) AS "mov1",
        ( SELECT "Descripcion" FROM "Catalogo_MovimientoInspeccion" WHERE "Id_MovimientoInspeccion" = bi."Id_MovimientoInspeccion2" ) AS "mov2",
        ( SELECT "Descripcion" FROM "Catalogo_MovimientoInspeccion" WHERE "Id_MovimientoInspeccion" = bi."Id_MovimientoInspeccion3" ) AS "mov3",
        ( SELECT CONCAT("Nombre", \' \', "ApellidoPaterno", \' \',"ApellidoMaterno") as "Nombre" FROM "Gen_Empleados" WHERE "IdEmpleado" = bi."Id_Inspector1" ) AS "inspector1",
        ( SELECT CONCAT("Nombre", \' \', "ApellidoPaterno", \' \',"ApellidoMaterno") as "Nombre" FROM "Gen_Empleados" WHERE "IdEmpleado" = bi."Id_Inspector2" ) AS "inspector2",
        ( SELECT "Descripcion" FROM "Catalogo_ResultadoInspeccion" WHERE "Id_ResultadoInspeccion" = bi."Id_ResultadoInspeccion" ) AS "resultado"
    FROM
        "Barra_Inspeccion" bi 
    WHERE
        "Id_Folio" = \''.$tram.'\' and "Id_Anio" = \''.$a.'\'';
        //return $sql;
        return $this->db->query($sql)->getResult();
    }
    
    public function consulta_graficacion($a = null, $tram = null){
        $sql = 'SELECT
        bg.*,
        ( SELECT "Descripcion" FROM "Catalogo_MovimientoGraficacion" WHERE "Id_MovimientoGraficacion" = bg."Id_MovimientoGraficacion1" ) AS "mov1",
        ( SELECT "Descripcion" FROM "Catalogo_MovimientoGraficacion" WHERE "Id_MovimientoGraficacion" = bg."Id_MovimientoGraficacion2" ) AS "mov2",
        ( SELECT "Descripcion" FROM "Catalogo_MovimientoGraficacion" WHERE "Id_MovimientoGraficacion" = bg."Id_MovimientoGraficacion3" ) AS "mov3",
        ( SELECT CONCAT("Nombre", \' \', "ApellidoPaterno", \' \',"ApellidoMaterno") AS "Nombre" FROM "Gen_Empleados" WHERE "IdEmpleado" = bg."Id_Graficador" ) AS "graficador",
        ( SELECT "Descripcion" FROM "Catalogo_ResultadoGraficacion" WHERE "Id_ResultadoGraficacion" = bg."Id_ResultadoGraficacion" ) AS "resultado"
        FROM
            "barra_graficacion" bg 
        WHERE
        "Id_Folio" = \''.$tram.'\' and "Id_Anio" = \''.$a.'\'';
        //return $sql;
        return $this->db->query($sql)->getResult();
    }

    public function consulta_movimientos_barra($clave = null){
        $sql = 'Select "NumeroOficio", \'OFICIO\' as tipo, "FechaOficio", "Observaciones", "FechaOficio" FROM "segui_MovimientosBarra" WHERE "Clave" = \''.$clave.'\'';
        return $this->db->query($sql)->getResult();
    }

    public function get_resultados_graficacion()
    {
        $sql = 'SELECT "Id_ResultadoGraficacion", "Descripcion" FROM "Catalogo_ResultadoGraficacion"';
        return $this->db->query($sql)->getResult();
    }

    public function get_graficadores(){
        $sql = 'SELECT b."IdEmpleado", b."Login", b."Nombre", b."ApellidoPaterno" FROM "Gen_Empleados" b';
        return $this->db->query($sql)->getResult();
    }

    public function get_movimientos_graficacion(){
        $sql = 'SELECT "Id_MovimientoGraficacion","Descripcion" FROM "Catalogo_MovimientoGraficacion" WHERE "Id_MovimientoGraficacion" BETWEEN \'A\' AND \'ZP\'';
        return $this->db->query($sql)->getResult();
    }

    public function get_brigadas_inspeccion(){
        $sql = 'SELECT A."Id_Brigada",CONCAT (b."IdEmpleado",\' \',b."Nombre",\' \',b."ApellidoPaterno",\', \',C."IdEmpleado",\' \',C."ApellidoPaterno",\' \',C."Nombre") AS brigada, b."IdEmpleado" as i1, C."IdEmpleado" as i2 FROM "Catalogo_Brigadas" A,"Gen_Empleados" b,"Gen_Empleados" C WHERE A."Id_Empleado1"=b."IdEmpleado" AND A."Id_Empleado2"=C."IdEmpleado"';

        return $this->db->query($sql)->getResult();
    }

    public function guardar_seguimiento($data = null){
        
        $fenv = $data["F_EnvioIces"];
        $fava = $data["F_AvaluoManual"];
        $fent = $data["F_EntregaContribuyente"];

        if($fenv == '') { $fenv = 'null'; } else { $fenv = "'".$fenv."'"; }
        if($fava == '') { $fava = 'null'; } else { $fava = "'".$fava."'"; }
        if($fent == '') { $fent = 'null'; } else { $fent = "'".$fent."'"; }

        //Se actualiza la tabla de Barra Seguimiento 
        $sql1 = 'UPDATE "Barra_Seguimiento" 
        SET "F_EnvioIces" = '.$fenv.', 
        "NumeroOficioEnvioIces" = \''.$data["NumeroOficioEnvioIces"].'\', 
        "F_AvaluoManual" = '.$fava.', 
        "F_EntregaContribuyente" = '.$fent.', 
        "Id_QuienLoTiene" = \''.$data["Id_QuienLoTiene"].'\', 
        "Id_EstadoActual" = \''.$data["Id_EstadoActual"].'\', 
        "ProcedioSN" = \''.$data["ProcedioSN"].'\',
        "Observaciones" = \''.$data["Observaciones"].'\' 
        WHERE "Id_FolioBarra" = \''.$data["Id_FolioBarra"].'\' AND "Id_Anio" = \''.$data["Id_Anio"].'\'; ';

        $sql2 = "";

        if ($data["F_EnvioIces"] != ''){
            $sql2 = 'UPDATE "segui_MovimientosBarra" SET "fec_ava" = '.$data["F_AvaluoManual2"].' WHERE "Clave"= \''.$data["Clave"].'\' AND "fec_ava" IS NULL AND "FechaOficio" <= \''.$data["F_EnvioIces"].'\'';
        }
        
        $q = $sql1 . $sql2;
        //return $q;
        return $this->db->query($q);

    }


    //reportes
    public function reporte_movimientos_graficacion(){
        $sql = 'SELECT a."Id_MovimientoGraficacion1", b."Descripcion", c."Descripcion" as Resultado, count(a."Clave") AS Total  
        FROM "barra_graficacion" AS a, "Catalogo_MovimientoGraficacion" As b, "Catalogo_ResultadoGraficacion" As c
        WHERE a."Id_MovimientoGraficacion1" = b."Id_MovimientoGraficacion" AND a."Id_ResultadoGraficacion" = c."Id_ResultadoGraficacion"  
        GROUP BY a."Id_MovimientoGraficacion1", b."Descripcion", c."Descripcion"
        ORDER BY b."Descripcion"';
        
        return $this->db->query($sql)->getResult();
    }

    public function reporte_movimientos_graficacion_enviados($tram = null){
        $filtro = "";
        if($tram != null)
            $filtro = 'AND a."IdTramite" = \''.$tram.'\'';

        $sql = 'SELECT a."NumeroOficio", a."FechaOficio", a."IdTramite", b."TramiteBarra",
        SUM(
            CASE WHEN 
                SUBSTRING(RTRIM(LTRIM(a."Clave2")), 10, 3) = \'\' THEN 1  
            ELSE 
                CAST(SUBSTRING(a."Clave2", 10, 3) AS INTEGER) - CAST(SUBSTRING(a."Clave", 10, 3) AS INTEGER) + 1 
            END
        ) AS total  
        FROM "segui_MovimientosBarra" AS a, "segui_TramitesBarra" As b  
                     WHERE a."IdTramite" = b."IdTramiteBarra"  AND b.grupo=\'1\'  
                     '.$filtro.'
                     GROUP BY a."IdTramite", b."TramiteBarra", a."FechaOficio", a."NumeroOficio"';
        
        return $this->db->query($sql)->getResult();
    }    

    public function get_seguimiento_tramites_barra(){
        $sql = 'SELECT * FROM "segui_TramitesBarra"';  
        return $this->db->query($sql)->getResult();
    }    

    public function reporte_tramites_detenidos(){
        $sql = 'SELECT A."Id_FolioBarra", A."F_Captura", A."clave", 
        A."F_AtendidoGraficacion",A."F_InspeccionCampo", C."DescripcionTramite", d."Observaciones",
        CASE WHEN A."F_AvaluoManual" IS NULL THEN DATE_PART(\'day\',NOW() :: TIMESTAMP-A."F_Captura" :: TIMESTAMP) END AS DiasAcumulados  
        FROM "Barra_Seguimiento" AS a, "Inco_Barra" AS b, "Inco_Tramites" AS c, barra_graficacion AS d
        WHERE a."F_EnvioIces" Is Null AND a."F_AvaluoManual" Is Null  
        AND "Id_EstadoActual"  IN (\'FD\', \'NL\', \'NE\', \'NC\', \'FC\', \'RI\', \'RE\') AND "Id_QuienLoTiene"=\'DG\' 
        AND SUBSTRING(A."Id_FolioBarra",4,8) = b."IdInconformidad" 
        AND b."IdTramite" = c."IdTramite" AND a."Id_Anio" = b."AnioDeclaracion" 
        AND a."Id_FolioBarra" = d."Id_Folio" AND a."Id_Anio" = d."Id_Anio" '; 

        return $this->db->query($sql)->getResult();
    }  

    public function reporte_inco_con_respuesta($tipo = null){
        if($tipo == '03')
            $tipo = '1';

        if($tipo == '08')
            $tipo = '2';

        $sql = 'SELECT a."IdTramite", c."TramiteBarra", a."FechaOficio", a."NumeroOficio",
        SUM(CASE WHEN SUBSTRING(RTRIM(LTRIM(a."Clave2")), 10, 3)= \'\' THEN 1 
        ELSE SUBSTRING(a."Clave2", 10, 3)::int - SUBSTRING(a."Clave", 10, 3)::int + 1 END) AS total 
        FROM "segui_MovimientosBarra" AS a, "segui_TramitesBarra" AS c  
        WHERE a."IdTramite" = c."IdTramiteBarra" AND a."fec_ava" IS NOT NULL AND c.grupo = \''.$tipo.'\' 
        GROUP BY a."FechaOficio", a."NumeroOficio", a."IdTramite", c."TramiteBarra"
        order by a."FechaOficio" asc'; 

        return $this->db->query($sql)->getResult();

    }

    public function reporte_inco_sin_respuesta($tipo = null){
        if($tipo == '03')
            $tipo = '1';

        if($tipo == '08')
            $tipo = '2';

        $sql = 'SELECT a."IdTramite", c."TramiteBarra", a."FechaOficio", a."NumeroOficio",
        SUM(CASE WHEN SUBSTRING(RTRIM(LTRIM(a."Clave2")), 10, 3)= \'\' THEN 1 
        ELSE REGEXP_REPLACE(SUBSTRING(a."Clave2", 10, 3),\'[[:alpha:]]\',\'0\',\'g\')::int - REGEXP_REPLACE(SUBSTRING(a."Clave", 10, 3),\'[[:alpha:]]\',\'\',\'g\')::int + 1 END) AS total, 
        a."Clave2", a."Clave" 
        FROM "segui_MovimientosBarra" AS a, "segui_TramitesBarra" AS c, "Barra_Seguimiento" AS b 
        WHERE a."IdTramite" = c."IdTramiteBarra" AND a.fec_ava IS NULL AND a."Clave"=b."clave" AND c.grupo = \''.$tipo.'\' 
        GROUP BY a."FechaOficio", a."NumeroOficio", a."IdTramite", c."TramiteBarra", a."Clave", a."Clave2"'; 

        return $this->db->query($sql)->getResult();

    }

    public function reporte_sin_enviar_ices(){
        $sql = 'SELECT a."Id_FolioBarra", a."F_Captura", a."clave", a."F_EnvioIces", a."NumeroOficioEnvioIces", c."DescripcionTramite", d."Observaciones",  
        CASE WHEN a."F_AvaluoManual" IS NULL THEN DATE_PART(\'day\',NOW() :: TIMESTAMP-A."F_Captura" :: TIMESTAMP) END AS DiasAcumulados  
        FROM "Barra_Seguimiento" AS a, "Inco_Barra" AS b, "Inco_Tramites" AS c, "barra_graficacion" AS d 
        WHERE a."F_EnvioIces" Is Null AND a."F_AvaluoManual" Is Null  
        AND ("Id_QuienLoTiene" NOT IN (\'CO\', \'CN\', \'CH\', \'CM\') 
        AND "Id_EstadoActual" NOT IN (\'NP\', \'CA\', \'CO\', \'NE\', \'FD\', \'NL\', \'FC\', \'RI\', \'NC\', \'MR\', \'RE\')) 
        AND SUBSTRING(A."Id_FolioBarra",4,8) = b."IdInconformidad" 
        AND b."IdTramite" = c."IdTramite" AND a."Id_Anio" = b."AnioDeclaracion" 
        AND a."Id_FolioBarra" = d."Id_Folio" 
        AND a."Id_Anio" = d."Id_Anio"';

        return $this->db->query($sql)->getResult();
    }

    public function reporte_seguimiento_tramites_ventanilla($tipo = null, $quien = null){

        if($tipo == '03'){
            $tipo = ' WHERE "IdDeclaracion" = \'03\'';
        }
        else if($tipo != '03'){
            $tipo = ' WHERE "IdDeclaracion" != \'03\'';
        }
        else if($tipo != '00'){
            $tipo = '';
        }

        if($quien != null){
            $quien = 'AND "Id_QuienLoTiene" = \''.$quien.'\'';
        }
        else{
            $quien = '';
        }

        $sql = 'SELECT "Id_FolioBarra", "F_Captura", "clave", 
        "F_EnvioInspeccion", DATE_PART(\'day\',NOW() :: TIMESTAMP- "F_EnvioInspeccion" :: TIMESTAMP) AS DiasInspeccion, 
        "F_EnvioGraficacion", DATE_PART(\'day\',NOW() :: TIMESTAMP- "F_EnvioGraficacion" :: TIMESTAMP) AS DiasGraficacion,
        "F_EnvioIces", DATE_PART(\'day\',NOW() :: TIMESTAMP- "F_EnvioIces" :: TIMESTAMP) AS DiasIces,
        "NumeroOficioEnvioIces", "FechaLimiteEnInspeccion", 
        "FechaLimiteEnGraficacion", "FechaLimiteEnIces", "F_AvaluoManual", "Id_QuienLoTiene", "Id_EstadoActual",
        "Observaciones", "TramiteNotario", "TramiteIces" 
        FROM "Barra_Seguimiento" 
        '.$tipo.'
         '.$quien.'
        ORDER BY "F_Captura", "Pobl", "Ctel", "Manz", "Pred", "Unid" ';
        
        return $this->db->query($sql)->getResult();
    }

    public function reporte_inco_regresado_ices(){

        $sql = 'SELECT a."Id_FolioBarra", a."F_Captura", a.clave, a."F_AtendidoGraficacion", a."F_InspeccionCampo", c."DescripcionTramite", a."Observaciones", a."Id_EstadoActual" 
        FROM "Barra_Seguimiento" AS a, "Inco_Barra" AS b, "Inco_Tramites" AS c 
        WHERE 
        NOT a."F_EnvioIces" Is Null 
        AND a."F_AvaluoManual" Is Null 
        AND ("Id_QuienLoTiene" NOT IN (\'CO\', \'CN\', \'CH\', \'CM\') AND "Id_EstadoActual" IN ( \'RI\', \'RE\' )) 
        AND SUBSTRING(A."Id_FolioBarra",4,8) = b."IdInconformidad" 
        AND b."IdTramite" = c."IdTramite" AND a."Id_Anio" = b."AnioDeclaracion" '; 

        return $this->db->query($sql)->getResult();

    }

    public function reporte_tramites_notarios($quien = null){

        if($quien != null){
            $quien = 'AND "Barra_Seguimiento"."Id_QuienLoTiene" = \''.$quien.'\'';
        }
        else{
            $quien = '';
        }

        $sql = 'SELECT 
        "Id_FolioBarra", 
        "F_Captura", 
        "clave", 
        "F_EnvioInspeccion", 
        DATE_PART(\'day\',NOW() :: TIMESTAMP- "F_EnvioInspeccion" :: TIMESTAMP) AS DiasInspeccion, 
        "F_EnvioGraficacion", 
        DATE_PART(\'day\',NOW() :: TIMESTAMP- "F_EnvioGraficacion" :: TIMESTAMP) AS DiasGraficacion, 
        "F_EnvioIces", 
        DATE_PART(\'day\',NOW() :: TIMESTAMP- "F_EnvioIces" :: TIMESTAMP) AS DiasIces, 
        "NumeroOficioEnvioIces", "FechaLimiteEnInspeccion", "FechaLimiteEnGraficacion", "FechaLimiteEnIces", "F_AvaluoManual",  "Descripcion" as "Id_QuienLoTiene", "Id_EstadoActual",
                "Observaciones", "TramiteNotario", "TramiteIces" 
        FROM "Barra_Seguimiento"
        INNER JOIN "Catalogo_QuienLoTiene" on "Catalogo_QuienLoTiene"."Id_QuienLoTiene" = "Barra_Seguimiento"."Id_QuienLoTiene"
        WHERE "IdDeclaracion" <> \'03\' 
        AND "TramiteNotario" = \'102\' 
       '.$quien.'';

        return $this->db->query($sql)->getResult();

    }

    public function datatable($data = null){
        $draw = $data['draw'];
        $start = $data['start'];
        $length = $data['length'];
        $search_value = $data['search_value'];
        $table = $data['table'];
        $searchcolumn = $data['searchcolumn'];

        
        if(!empty($search_value)){
            // If we have value in search, searching by id, name, email, mobile

            if($table == '"Barra_Seguimiento"'){
                $query = 'SELECT bs.*, ea."Descripcion", ea."Estado", ql."Descripcion" as "dpto", ql."Estado" as "dpto_estado" FROM "Barra_Seguimiento" bs
                INNER JOIN "Catalogo_EstadoActual" ea on bs."Id_EstadoActual" = ea."Id_EstadoActual"
                INNER JOIN "Catalogo_QuienLoTiene" ql on ql."Id_QuienLoTiene" = bs."Id_QuienLoTiene"
                WHERE bs."Id_FolioBarra" like \'%'.$search_value.'%\' OR bs."Nombre_Propietario" like \'%'.$search_value.'%\' OR bs.clave like \'%'.$search_value.'%\' LIMIT '.$length.' OFFSET '.$start.';';

                $total_count = $this->db->query('SELECT bs.*, ea."Descripcion", ea."Estado", ql."Descripcion" as "dpto", ql."Estado" as "dpto_estado" FROM "Barra_Seguimiento" bs
                INNER JOIN "Catalogo_EstadoActual" ea on bs."Id_EstadoActual" = ea."Id_EstadoActual"
                INNER JOIN "Catalogo_QuienLoTiene" ql on ql."Id_QuienLoTiene" = bs."Id_QuienLoTiene"
                WHERE bs."Id_FolioBarra" like \'%'.$search_value.'%\' OR bs."Nombre_Propietario" like \'%'.$search_value.'%\' OR bs.clave like \'%'.$search_value.'%\'')->getResult();
            }
            else{
                 // count all data
                $total_count = $this->db->query("SELECT * from $table WHERE $searchcolumn like '%".$search_value."%'")->getResult();
                $query = "SELECT * from $table WHERE \"$searchcolumn\" like '%".$search_value."%' limit $length OFFSET $start";
            }
            $data = $this->db->query($query)->getResult();

        }else{
            // count all data
            $total_count = $this->db->query("SELECT * from $table")->getResult();
            $query = "SELECT * FROM $table LIMIT $length OFFSET $start";

            if($table == '"Barra_Seguimiento"'){
                $query = 'SELECT bs.*, ea."Descripcion", ea."Estado", ql."Descripcion" as "dpto", ql."Estado" as "dpto_estado" FROM "Barra_Seguimiento" bs
                INNER JOIN "Catalogo_EstadoActual" ea on bs."Id_EstadoActual" = ea."Id_EstadoActual"
                INNER JOIN "Catalogo_QuienLoTiene" ql on ql."Id_QuienLoTiene" = bs."Id_QuienLoTiene"
                ORDER BY bs."Id_Anio" DESC LIMIT '.$length.' OFFSET '.$start.';';
            }
            
            //echo $query;
            // get per page data
            $data = $this->db->query($query)->getResult();
            
        }
        
        $json_data = array(
            "draw" => intval($draw),
            "recordsTotal" => count($total_count),
            "recordsFiltered" => count($total_count),
            "data" => $data   // total data array
        );

        return $json_data;
    }

    


    
}