<?php namespace App\Models;
use CodeIgniter\Model;
//incluimos la interfaz para la coneccion con la base de datos
use CodeIgniter\Database\ConnectionInterface;

class PeritosModel extends Model {

    protected $db;
	public function __construct(ConnectionInterface &$db) {
		$this->db =& $db;
    }

    public function get_bitacora_avaluos($id = null){
        $sql = 'select pa.*, pad.* from perito_avaluo pa
        inner join perito_avaluo_detalle pad on pa.informacion_id = pad.informacion_id
        WHERE pa.id_perito = '. $id;
        return $this->db->query($sql)->getResult();
    }

    public function get_avaluos_comercial($tipo = null){
        if($tipo == 'revision'){
            $sql = "select pa.*, pad.*, up.nombre, up.rfc, pap.url as plano from perito_avaluo pa
            inner join perito_avaluo_detalle pad on pa.informacion_id = pad.informacion_id
            inner join usuario_perfil up on pa.id_perito = up.id_usuario
            left join perito_avaluo_planos pap on pad.informacion_id = pap.informacion_id 
            WHERE pad.en_revision = '1'";
        }
        else{
            $sql = "select * from perito_avaluo pa
            inner join perito_avaluo_detalle pad on pa.informacion_id = pad.informacion_id
            inner join usuario_perfil up on pa.id_perito = up.id_usuario";
        }
        
        return $this->db->query($sql)->getResult();
    }

    public function validar_clave($clave = null)
    {
        $query2 = 'SELECT*,(
            SELECT row_to_json (fc) AS jsonb_build_object FROM (
            SELECT \'FeatureCollection\' AS TYPE,array_to_json (ARRAY_AGG (f)) AS features FROM (
            SELECT \'Feature\' AS TYPE,ST_AsGeoJSON (ST_Transform (lg.geom,4326),15,0) :: json AS geometry,row_to_json ((ID,dg_ccat,nom_comp_t,nom_calle,nom_col,num_ofic_u,sup_terr,sup_const,val_catast)) AS properties FROM (
            SELECT*FROM "Culiacan_Join" WHERE dg_ccat=\'007'.$clave.'\') AS lg) AS f) AS fc) jsonb_build_object FROM a_unid A LEFT JOIN a_calle b ON A."CVE_POBL"=b."CVE_POBL" AND A."CVE_CALLE_"=b."CVE_CALLE" LEFT JOIN a_col C ON A."CVE_POBL"=C."CVE_POBL" AND A."CVE_COL_UB"=C."CVE_COL" LEFT JOIN a_prop d ON A."CVE_PROP"=d."CVE_PROP" LEFT JOIN a_usosayto e ON A."CVE_USO"=e."CVE_USO" LEFT JOIN a_regim ar ON A."CVE_REGIM"=ar."CVE_REGIM" WHERE A."CLAVE"=\''.$clave.'\'';

        $query_or = 'SELECT *, encode("FOTO", \'base64\') as "foto"  FROM a_unid a, a_calle b, a_col c, a_prop d, a_usosayto e, a_regim ar, (SELECT row_to_json(fc) AS jsonb_build_object FROM 
        (SELECT \'FeatureCollection\' As type, array_to_json(array_agg(f))
        As features FROM 
        (SELECT 
        \'Feature\' As type, 
        ST_AsGeoJSON(ST_Transform(lg.geom, 4326),15,0)::json As geometry,
        row_to_json((id, dg_ccat, nom_comp_t, nom_calle, nom_col, num_ofic_u, sup_terr, sup_const, val_catast)) As properties
        FROM (SELECT * FROM "Culiacan_Join" WHERE dg_ccat  = \'007'.$clave.'\') As lg) As f ) As fc) fc WHERE a."CLAVE" = \''.$clave.'\' AND a."CVE_POBL" = b."CVE_POBL" AND a."CVE_CALLE_" = b."CVE_CALLE"  AND a."CVE_POBL" = c."CVE_POBL" AND a."CVE_COL_UB" = c."CVE_COL"  AND a."CVE_PROP" = d."CVE_PROP" AND a."CVE_USO" = e."CVE_USO" AND a."CVE_REGIM" = ar."CVE_REGIM"';

        return $this->db->query($query2)->getResult();
    }

    public function validar_infocat($info = null)
    {

        $query1 = "SELECT * FROM perito_infcat_detalle WHERE uuid = '$info';";

        return $this->db->query($query1)->getResult();
    }

    public function nuevo_avaluo($data = null)
    {
        $inf_cat = $data['infcat'];
        $numsol = $data['numsol'];
        $cvecat = $data['cvecat'];
        $user_id = $data['perito'];

        $cc = str_split($cvecat, 3);
        $muni = intval ( $cc[0] );
        $pobl = intval ( $cc[1] );
        $ctel = intval ( $cc[2] );
        $manz = intval ( $cc[3] );
        $pred = intval ( $cc[4] );
        $unid = intval ( $cc[5] );

        $sql = "SELECT picd.* FROM perito_infcat_detalle picd INNER JOIN perito_infcat pic ON pic.informacion_id=picd.informacion_id WHERE picd.uuid='$inf_cat' AND pic.cargo='$numsol'";

        $query = $this->db->query($sql);

        //validacion
        if($this->db->affectedRows() > 0){
            $sql1 = "UPDATE perito_infcat_detalle SET estatus = 'S', user_id = $user_id WHERE uuid = '$inf_cat'; ";
            $sql2 = "INSERT INTO perito_avaluo VALUES (DEFAULT, $user_id, $muni, $pobl, $ctel, $manz, $pred, $unid, '$cvecat', NOW(), NOW(), NULL, '$inf_cat') RETURNING id; ";
           
            $q = $sql1.$sql2;
            //
            $save = $this->db->query($q);
            $row = $save->getRow();
            $id = $row->id;

            if($save){

                $sql3 = " INSERT INTO perito_avaluo_detalle (id_detalle, id_avaluo, en_movimiento, en_revision, reingreso, tipo, informacion_id)
                VALUES (DEFAULT, $id, 0, 0, 0, 'URBANO', '$inf_cat');";
                $this->db->query($sql3);
                
                echo "ok";
                //echo $q;
            }
            else{
                echo "error";
            }
        }
        else{
           echo "validacion";
        }

    }

    public function consulta_cat_const($info = null)
    {
        $query1 = "select \"CVE_CAT_CO\" as \"CVE_CAT_CONST\", val_unit_c as \"VAL_UNIT_CONST\" from a_cat_mp";
        return $this->db->query($query1)->getResult();
    }

    public function consulta_factor_demerito_construccion($data = null)
    {
        $CVE_CAT_CONST = $data['CVE_CAT_CONST'];
        $CVE_EDO_CONS = $data['CVE_EDO_CONS'];
        $EDD_CONST = $data['EDD_CONST'];

        $query1 = "select \"FAC_DEM_CO\" from a_demc WHERE \"CVE_CAT_CO\" = '$CVE_CAT_CONST' and \"CVE_EDO_CO\" = $CVE_EDO_CONS and \"EDD_CONST\" = $EDD_CONST";
        return $this->db->query($query1)->getResult();
    }

    //consultar_info
    public function consultar_perito_perfil($id = null){
        $query1 = "SELECT * FROM perito_perfil WHERE id_usuario = $id;";
        return $this->db->query($query1)->getResult();
    }

    public function consultar_autorizacion_perito($info = null)
    {
        $query1 = "SELECT * FROM perito_avaluo_detalle WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }



    public function consultar_avaluo_infoid($info = null)
    {
        $query1 = "SELECT * FROM perito_avaluo WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function consultar_avaluo_detalle($info = null)
    {
        $query1 = "SELECT * FROM perito_avaluo_detalle WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function consultar_avaluo_portada($info = null){
        $query1 = "SELECT * FROM perito_avaluo_portada WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function consultar_avaluo_antecedentes($info = null){
        $query1 = "SELECT * FROM perito_avaluo_antecedentes WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function consultar_avaluo_carurb($info = null){
        $query1 = "SELECT * FROM perito_avaluo_car_urb WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function consultar_avaluo_carterreno($info = null){
        $query1 = "SELECT * FROM perito_avaluo_car_terreno WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function get_avaluo_fisico_terreno($info = null){
        $query1 = "SELECT * FROM peritos_avaluo_fisico_terreno WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function get_avaluo_fisico_construcciones($info = null){
        $query1 = "SELECT * FROM peritos_avaluo_fisico_construcciones WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function get_avaluo_fisico_especiales($info = null){
        $query1 = "SELECT * FROM peritos_avaluo_fisico_especiales WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function consultar_avaluo_descripcion_general($info = null){
        $query1 = "SELECT * FROM perito_avaluo_desc_general WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function consultar_avaluo_elementos_construccion($info = null){
        $query1 = "SELECT * FROM perito_avaluo_elementos_construccion WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function consultar_avaluo_orientaciones($info = null)
    {
        $query1 = "SELECT * FROM perito_avaluo_orientaciones WHERE informacion_id = '$info';";
        return $this->db->query($query1)->getResult();
    }

    public function eliminar_avaluo_orientacion($id = null)
    {    
        //eliminar de la tabla a_uso_u
        $sql = "DELETE FROM perito_avaluo_orientaciones WHERE id = $id;";
        //Ejecutar la eliminacion;
        return $this->db->query($sql);
        //return $sql;
    }

    public function consultar_construcciones($cve = null)
    {       
        $cve_split = str_split($cve, 3);
        $pobl = $cve_split[0];
        $sql = 'SELECT cons.*, cons."CVE_CAT_CO",(
                SELECT "DES_CAT_CO" FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO") AS "DES_CAT_CO",(
                SELECT val_unit_c FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO") AS ValorUnitario,cons."SUP_CONST",cons."CVE_EDO_CO",(
                SELECT "DES_EDO_CO" FROM "a_EdoC" WHERE "CVE_EDO_CO"=cons."CVE_EDO_CO") AS "EDO_CO",cons."FEC_CONST",cons."EDD_CONST",(
                SELECT "FAC_DEM_CO" FROM a_demc WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO" AND "CVE_EDO_CO"=cons."CVE_EDO_CO" AND "EDD_CONST"=cons."EDD_CONST") AS FactorDemerito,(
                SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL"=\''.$pobl.'\') AS FactorComercializacion,(cons."SUP_CONST"*(
                SELECT val_unit_c FROM a_cat_mp WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO")*(
                SELECT "FAC_DEM_CO" FROM a_demc WHERE "CVE_CAT_CO"=cons."CVE_CAT_CO" AND "CVE_EDO_CO"=cons."CVE_EDO_CO" AND "EDD_CONST"=cons."EDD_CONST")*(
                SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL"=\''.$pobl.'\')) AS ValorNeto FROM a_const AS cons WHERE cons.clave= \''.$cve.'\'';
        return $this->db->query($sql)->getResult();
    }

    public function get_terreno_fisico($info = null){
            $sql = "SELECT * FROM perito_avaluo_fisico_terreno WHERE informacion_id = '$info';";
            return $this->db->query($sql)->getResult();

    }

    //combos

    public function listar_clasificacion_zona()
    {
        $q = "SELECT clasificacionzona_descripcion as desc FROM cat_cru_clasificacionzona";
        return $this->db->query($q)->getResult();
    }

    public function listar_construccion_dominante()
    {
        $q = "SELECT construcciondominante_descripcion as desc FROM cat_cru_construcciondominante";
        return $this->db->query($q)->getResult();
    }

    public function listar_saturacion_zona()
    {
        $q = "SELECT indicesaturacionzona_descripcion as desc FROM cat_cru_indicesaturacionzona";
        return $this->db->query($q)->getResult();
    }

    public function listar_poblacion()
    {
        $q = "SELECT poblacion_descripcion as desc FROM cat_cru_poblacion";
        return $this->db->query($q)->getResult();
    }

    public function listar_suelo_permitido()
    {
        $q = "SELECT usosuelopermitido_descripcion as desc FROM cat_cru_usosuelopermitido";
        return $this->db->query($q)->getResult();
    }

    public function listar_grupo_usos(){
        $sql = "SELECT * FROM \"a_gUso\"";
        return $this->db->query($sql)->getResult();
    }   

    public function listar_instalaciones_especiales()
    {
        $q = "SELECT instalaciones_descripcion as desc FROM cat_crc_instalacionesespeciales";
        return $this->db->query($q)->getResult();
    }

    public function listar_fachadas()
    {
        $q = "SELECT fachada_descripcion as desc FROM cat_crc_fachadas";
        return $this->db->query($q)->getResult();
    }

    public function listar_cerrajeria()
    {
        $q = "SELECT cerrajeria_descripcion as desc FROM cat_crc_cerrajeria";
        return $this->db->query($q)->getResult();
    } 

    public function listar_vidrieria()
    {
        $q = "SELECT vidrio_descripcion as desc FROM cat_crc_vidrieria";
        return $this->db->query($q)->getResult();
    } 

    public function listar_herreria()
    {
        $q = "SELECT herreria_descripcion as desc FROM cat_crc_herreria";
        return $this->db->query($q)->getResult();
    } 

    public function listar_instalacionese()
    {
        $q = "SELECT instalacione_descripcion as desc FROM cat_crc_instalacioneselectricas";
        return $this->db->query($q)->getResult();
    }

    public function listar_mueblesb()
    {
        $q = "SELECT mueblebano_descripcion as desc FROM cat_crc_mueblesbano";
        return $this->db->query($q)->getResult();
    }

    public function listar_puertas()
    {
        $q = "SELECT puerta_descripcion as desc FROM cat_crc_puertas";
        return $this->db->query($q)->getResult();
    }

    public function listar_pintura()
    {
        $q = "SELECT pintura_descripcion as desc FROM cat_crc_pintura";
        return $this->db->query($q)->getResult();
    }

    public function listar_pisos()
    {
        $q = "SELECT piso_descripcion as desc FROM cat_crc_pisos";
        return $this->db->query($q)->getResult();
    }

    public function listar_lambrines()
    {
        $q = "SELECT lambrin_descripcion as desc FROM cat_crc_lambrines";
        return $this->db->query($q)->getResult();
    }

    public function listar_aplanados()
    {
        $q = "SELECT aplanadointerior_descripcion as desc FROM cat_crc_aplanadosinteriores";
        return $this->db->query($q)->getResult();
    }

    public function listar_techos()
    {
        $q = "SELECT techo_descripcion as desc FROM cat_crc_techos";
        return $this->db->query($q)->getResult();
    }

    public function listar_muros()
    {
        $q = "SELECT muro_descripcion as desc FROM cat_crc_muros";
        return $this->db->query($q)->getResult();
    }

    public function listar_cimientos()
    {
        $q = "SELECT cimiento_descripcion as desc FROM cat_crc_cimientos";
        return $this->db->query($q)->getResult();
    }

    //Registros
    public function registrar_perito_perfil($data = null){
        $id = $data['id'];
        $nombre = $data['nombre'];
        $rfc = $data['rfc'];
        $registro = $data['registro'];
        $especialidad = $data['especialidad'];
        $direccion = $data['direccion'];
        $telefono = $data['telefono'];
        $celular = $data['celular'];
        $correo = $data['correo'];

        $sql = "INSERT INTO perito_perfil VALUES(default, $id,'$rfc','$nombre', '$registro', '$especialidad','$direccion','$telefono', '$celular' ,'$correo')
        ON CONFLICT (id_usuario) 
        DO
        UPDATE SET rfc = '$rfc',nombre = '$nombre', clave_registro = '$registro', especialidad = '$especialidad', direccion = '$direccion',telefono = '$telefono', celular = '$celular' , correo = '$correo'; ";
        //echo $sql;
        //Ejecutar la insercion;
        return $this->db->query($sql);
    }
    

    public function registrar_avaluo_orientacion($data = null){
        $inf = $data['inf_cat'];
        $ori = $data['orientacion'];
        $desc = $data['descripcion'];
        $sql = "INSERT INTO perito_avaluo_orientaciones VALUES(default, '$inf','$ori','$desc')";
        //Ejecutar la insercion;
        return $this->db->query($sql);
    }

    public function registrar_portada($data = null){
        $p1 = $data['avaluo'];
        $p2 = $data['info'];
        $p3 = $data['inmueble'];
        $p4 = $data['calle'];
        $p5 = $data['cve_calle'];
        $p6 = $data['numero'];
        $p7 = $data['colonia'];
        $p8 = $data['cve_col'];
        $p9 = $data['municipio'];
        $p10 = $data['cp'];
        $p11 = $data['notainterna'];
        
        $sql = "INSERT INTO perito_avaluo_portada VALUES (default, $p1, '$p2', '$p3', '$p4', '$p5', '$p6', '$p7', '$p8', '$p9', '$p10', '$p11') 
        ON CONFLICT (id_avaluo) 
        DO
        UPDATE
        SET inmueble = '$p3', calle = '$p4', cve_calle = '$p5', numero = '$p6', colonia = '$p7', cve_colonia = '$p8', municipio = '$p9', cp = '$p10', nota_interna = '$p11'
        ";
        //Ejecutar la insercion;
        //return $sql;
        return $this->db->query($sql);
    }

    public function registrar_antecedentes($data = null){
        $p1 = $data['avaluo'];
        $p2 = $data['info'];
        $p3 = $data['solicitante'];
        $p4 = $data['fechaInspeccion'];
        $p5 = $data['regimen'];
        $p6 = $data['objeto'];
        $p7 = $data['folioRegPubProp'];
        $p8 = $data['inscripcion'];
        $p9 = $data['libro'];
        $p10 = $data['seccion'];
        
        $sql = "INSERT INTO perito_avaluo_antecedentes VALUES (default, $p1, '$p3', '$p4', '$p5', '$p6', '$p7', '$p8', '$p9', '$p10', '$p2') 
        ON CONFLICT (id_avaluo) 
        DO
        UPDATE
        SET solicitante = '$p3', fecha_inspeccion = '$p4', regimen = '$p5', objeto = '$p6', rpp = '$p7', inscripcion = '$p8', libro = '$p9', seccion = '$p10'
        ";
        //Ejecutar la insercion;
        //return $sql;
        return $this->db->query($sql);
    }

    public function registrar_car_urb($data = null){
        $p1 = $data['avaluo'];
        $p2 = $data['info'];
        $p3 = $data['clasificacionZona'];
        $p4 = $data['tipoConstDominante'];
        $p5 = $data['indiceSatZona'];
        $p6 = $data['poblacion'];
        $p7 = $data['contaminacionAmbiental'];
        $p8 = $data['usoSueloPermitido'];
        $p9 = $data['viaAccesoImportancia'];

        $p10 = $data['agua'];
        $p11 = $data['drenaje'];
        $p12 = $data['energia'];
        $p13 = $data['alumbrado'];
        $p14 = $data['basura'];
        $p15 = $data['transporte'];
        $p16 = $data['telefono'];
        $p17 = $data['banqueta'];
        $p18 = $data['guarniciones'];
        $p19 = $data['pavimento'];
        $p20 = $data['vigilancia'];

        
        $sql = "INSERT INTO perito_avaluo_car_urb VALUES (default, $p1, '$p2', '$p3', '$p4', '$p5', '$p6', '$p7', '$p8', '$p9', $p10,$p11,$p12,$p13,$p14,$p15,$p16,$p17,$p18,$p19,$p20) 
        ON CONFLICT (id_avaluo) 
        DO
        UPDATE
        SET clasificacionzona = '$p3', tipoconstdominante = '$p4', indicesatzona = '$p5', poblacion = '$p6', contaminacionambiental = '$p7', usosuelopermitido = '$p8', viaaccesoimportancia = '$p9', agua = $p10, drenaje = $p11, energia = $p12, alumbrado = $p13, basura = $p14, transporte = $p15, telefono = $p16, banqueta = $p17, guarniciones = $p18, pavimento = $p19, vigilancia = $p20
        ";
        //Ejecutar la insercion;
        //return $sql;
        return $this->db->query($sql);
    }

    public function registrar_car_terreno($data = null){
        $p1 = $data['avaluo'];
        $p2 = $data['info'];
        $p3 = $data['TopografiaConstruccion'];
        $p4 = $data['NumeroFrentes'];
        $p5 = $data['CaracteristicasPanoramicas'];
        $p6 = $data['DensidadHabitacional'];
        $p7 = $data['IntensidadConstruccion'];
        $p8 = $data['ServidumbresRestricciones'];
        $p9 = $data['SuperficieEscrituras'];
        $p10 = $data['SuperficieTopografico'];

        
        $sql = "INSERT INTO perito_avaluo_car_terreno VALUES (default, $p1, '$p2', '$p3', '$p4', '$p5', '$p6', '$p7', '$p8', '$p9', '$p10') 
        ON CONFLICT (id_avaluo) 
        DO
        UPDATE
        SET topografia_const = '$p3', numero_frentes = '$p4', car_panoramicas = '$p5', densidad_habitacional = '$p6', intensidad_const = '$p7', servidumbre_restricciones = '$p8', sup_escrituras = $p9, sup_topografico = $p10
        ";
        //Ejecutar la insercion;
        //return $sql;
        return $this->db->query($sql);
    }

    public function registrar_descripcion_general($data = null){
        $p1 = $data['avaluo'];
        $p2 = $data['info'];
        $p3 = $data['CVE_GPO_USO'];
        $p4 = $data['EdadAproximada'];
        $p5 = $data['NumeroNiveles'];
        $p6 = $data['EstadoConservacion'];
        $p7 = $data['CalidadProyecto'];
        $p8 = $data['UnidadesRentables'];
        $p9 = $data['DescripcionGeneral'];
        
        $sql = "INSERT INTO perito_avaluo_desc_general VALUES (default, $p1, '$p2', '$p3', $p4, $p5, '$p6', '$p7', '$p8', '$p9') 
        ON CONFLICT (id_avaluo) 
        DO
        UPDATE
        SET cve_uso = '$p3', edad_aprox = $p4, num_niveles = $p5, estado_conservacion = '$p6', calidad_proyecto = '$p7', unidades_rentables = '$p8', descripcion_general = '$p9'
        ";
        //Ejecutar la insercion;
        //return $sql;
        return $this->db->query($sql);
    }

    public function registrar_elementos_construccion($data = null){
        $p1 = $data['avaluo'];
        $p2 = $data['info'];
        $p3 = $data['A_Cimientos'];
        $p4 = $data['A_Estructura'];
        $p5 = $data['A_Muros'];
        $p6 = $data['A_EntrePisos'];
        $p7 = $data['A_Techos'];
        $p8 = $data['A_Azoteas'];
        $p9 = $data['A_Bardas'];
        $p10 = $data['B_AplanadosInteriores'];
        $p11 = $data['B_AplanadosExteriores'];
        $p12 = $data['B_Plafones'];
        $p13 = $data['B_Lambrines'];
        $p14 = $data['B_Pisos'];
        $p15 = $data['B_Zoclos'];
        $p16 = $data['B_Escaleras'];
        $p17 = $data['B_Pintura'];
        $p18 = $data['B_RecubrimientosEspeciales'];
        $p19 = $data['C_Puertas'];
        $p20 = $data['C_Closets'];
        $p21 = $data['D_MueblesDeBano'];
        $p22 = $data['D_EquipoDeConcina'];
        $p23 = $data['E_InstalacionesElectricas'];
        $p24 = $data['F_Herreria'];
        $p25 = $data['G_Vidrieria'];
        $p26 = $data['H_Cerrajeria'];
        $p27 = $data['I_Fachadas'];
        $p28 = $data['J_InstalacionesEspeciales'];

        
        
        $sql = "INSERT INTO perito_avaluo_elementos_construccion 
        VALUES (default, $p1, '$p2', '$p3', '$p4', '$p5', '$p6', '$p7', '$p8', '$p9', '$p10','$p11','$p12','$p13','$p14','$p15','$p16','$p17','$p18','$p19','$p20','$p21','$p22','$p23','$p24','$p25','$p26','$p27','$p28') 
        ON CONFLICT (id_avaluo) 
        DO
        UPDATE
        SET a_cimientos = '$p3', a_estructura = '$p4', a_muros = '$p5', a_entrepisos = '$p6', a_techos = '$p7', a_azoteas = '$p8', a_bardas = '$p9', b_aplanadosinteriores = '$p10', b_aplanadosexteriores = '$p11', b_plafones = '$p12', b_lambrines = '$p13', b_pisos = '$p14', b_zoclos = '$p15', b_escaleras = '$p16', b_pintura = '$p17', b_recubrimientosespeciales = '$p18', c_puertas = '$p19', c_closets = '$p20', d_mueblesbano = '$p21', d_equipodecocina = '$p22', e_instalacioneselectricas = '$p23', f_herreria = '$p24', g_vidrieria = '$p25', h_cerrajeria = '$p26', i_fachadas = '$p27', j_instalacionesespeciales = '$p28'
        ";
        //Ejecutar la insercion;
        //return $sql;
        return $this->db->query($sql);
    }

    public function registrar_sello($data = null){
        $p1 = $data['avaluo'];
        $p2 = $data['info'];
        $rfc = $data['rfc'];
        $nombre = $data['nombre'];
        $serie = $data['serie'];
        $sello = $data['sello'];

        $sql = "UPDATE perito_avaluo_detalle SET en_revision = 1, rfc_certificado = '$rfc', serie_certificado = '$serie', sello_digital = '$sello' WHERE id_avaluo = $p1 AND informacion_id = '$p2';";
        
        return $this->db->query($sql);
    }

    public function registrar_valor_mercado($data = null){
        $p1 = $data['avaluo'];
        $p2 = $data['info'];
        $valor = $data['valor_mercado'];
        $consideraciones = $data['consideraciones'];

        $sql = "UPDATE perito_avaluo_detalle SET valor_mercado = $valor, consideraciones_previas = '$consideraciones' WHERE id_avaluo = $p1 AND informacion_id = '$p2';";
        
        return $this->db->query($sql);
    }

    public function registrar_avaluo_fisico($data = null){
        $p1 = $data['avaluo'];
        $p2 = $data['info'];
        $terr = json_decode($data['terrenos'], true);   
        $const = json_decode($data['construcciones'], true);    
        $esp = json_decode($data['especiales'], true); 
        $valmercado = $data['valor_mercado']; 

        $result = false;

        //por cada terreno insertamos en la tabla de avaluo_fisico_terreno
        $sql_terr = "DELETE FROM peritos_avaluo_fisico_terreno WHERE id_avaluo = $p1 AND informacion_id = '$p2'; ";
        foreach($terr as $t){
            $supm2      = $t['SupMt2'];
            $val_unit   = $t['ValorUnitario'];
            $fact_dem   = $t['FactorDemerito'];
            $motivo     = $t['Motivo'];
            $val_terr   = $t['ValorTerreno'];

            $sql_terr .= "INSERT INTO peritos_avaluo_fisico_terreno VALUES (DEFAULT, $p1, '$p2', $supm2, '$val_unit', $fact_dem, '$motivo', '$val_terr'); ";
        }

        //por cada construccion insertamos en la tabla de avaluo_fisico_construcciones
        $sql_const = "DELETE FROM peritos_avaluo_fisico_construcciones WHERE id_avaluo = $p1 AND informacion_id = '$p2'; ";
        foreach($const as $c){
            $cat        = $c['Categoria'];
            $supm2      = $c['SupMt2'];
            $edad       = $c['Edad'];
            $fcom       = $c['FCom'];
            $estado     = $c['EstadoConservacion'];
            $valor_unit = $c['ValorUnitario'];
            $valor_net  = $c['ValorNeto']; 
            $fdem       = $c['FDem']; 

            $sql_const .= "INSERT INTO peritos_avaluo_fisico_construcciones VALUES (DEFAULT, $p1, '$p2', '$cat', $supm2, $edad, $fcom, $estado, '$valor_net', '$valor_unit', $fdem ); ";
        }

        //por cada instalacion especial insertamos en la tabla de avaluo_fisico_especiales
        $sql_esp = "DELETE FROM peritos_avaluo_fisico_especiales WHERE id_avaluo = $p1 AND informacion_id = '$p2'; ";
        foreach($esp as $es){
            $concepto       = $es['Concepto'];
            $unidad         = $es['Unidad'];
            $cantidad       = $es['Cantidad'];
            $fdem           = $es['FDemerito'];
            $valor_unit     = $es['ValorUnitario'];
            $vrn_unit       = $es['VRNUnitario'];
            $valor_parcial  = $es['ValorParcial'];

            $sql_esp .= "INSERT INTO peritos_avaluo_fisico_especiales VALUES (DEFAULT, $p1, '$p2', '$concepto', '$unidad', '$cantidad', $fdem, $valor_unit, '$vrn_unit', '$valor_parcial'); ";
        }

        $sqlall = $sql_terr.$sql_const.$sql_esp;
        //return $sql;
        return $this->db->query($sqlall);
    }

    public function upload_plano($ruta = null, $avaluo = null, $info = null){
        $sql = "INSERT INTO perito_avaluo_planos VALUES (DEFAULT, '$avaluo','$info','$ruta');";
        return $this->db->query($sql);
    }

    public function upload_imagen($ruta = null, $avaluo = null, $info = null){
        $sql = "INSERT INTO perito_avaluo_imagenes VALUES (DEFAULT, '$avaluo','$info','$ruta', '');";
        return $this->db->query($sql);
    }

    public function actualizar_imagen($data = null){
        $img = $data['id_imagen'];
        $desc = $data['descripcion'];
        $sql = "UPDATE perito_avaluo_imagenes SET descripcion = '$desc' WHERE id_imagen = $img;";
        return $this->db->query($sql);
    }

    public function eliminar_imagen($data = null){
        $img = $data['id_imagen'];
        $inf = $data['inf_cat'];
        $nombre = $data['nombre'];

        $sql = "DELETE FROM perito_avaluo_imagenes WHERE id_imagen = $img;";
        $response = $this->db->query($sql);
        $result = false;
        if($response){
            $directorio = ROOTPATH.'public/documentos_peritos/img_reporte_fotografico/'.$inf;
            $file = $directorio."/".$nombre;
            if (is_readable($file) && unlink($file)) {
                $result = true;
            } else {
                $result = false;
            }
        }
        return $result;
    }

    public function obtener_imagenes($inf = null){
        $sql = "SELECT id_imagen, url, descripcion FROM perito_avaluo_imagenes WHERE informacion_id = '$inf';";
        return $this->db->query($sql)->getResult();
    }

    public function obtener_plano($inf = null){
        $sql = "SELECT * FROM perito_avaluo_planos WHERE informacion_id = '$inf';";
        return $this->db->query($sql)->getResult();
    }
}