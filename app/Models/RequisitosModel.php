<?php namespace App\Models;

//incluimos la interfaz para la coneccion con la base de datos
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class RequisitosModel extends Model
{

    protected $db;
	public function __construct(ConnectionInterface &$db) {
		$this->db =& $db;
    }
    
    public function listar() {
		return $this->db
                        ->table('Inco_TramitesRequisitos')
                        ->select('*')
                        ->get()
                        ->getResult();
    }

    public function get($id = null) {
		return $this->db
                        ->table('Inco_TramitesRequisitos')
                        ->select('*')
                        ->where("IdTramite = '$id'")
                        ->get()
                        ->getResult();
    }

}