<?php namespace App\Models;
use CodeIgniter\Model;
//incluimos la interfaz para la coneccion con la base de datos
use CodeIgniter\Database\ConnectionInterface;

class GraficacionModel extends Model {

    protected $db;
	public function __construct(ConnectionInterface &$db) {
		$this->db =& $db;
    }

    public function get_ventanilla_graficacion()
    {
        $sql = 'SELECT A."Id_PaqueteEnvioGraficacion",A."F_EnvioGraficacion",b."Id_Anio",b."Id_Folio",C."Descripcion",b."Pobl",b."Ctel",b."Manz",b."Pred",b."Unid","F_AtendidoGraficacion" FROM "Barra_PaqueteEnvioGraficacion" A,"barra_graficacion" b,"Catalogo_MovimientoGraficacion" C WHERE A."Id_PaqueteEnvioGraficacion"=b."Id_PaqueteEnvioGraficacion" AND b."Id_MovimientoGraficacion1"=C."Id_MovimientoGraficacion";';
        return $this->db->query($sql)->getResult();
    }

    public function get_dictamenes_regularizacion()
    {
        $sql = 'SELECT A."Id_PaqueteEnvioGraficacion",A."F_EnvioGraficacion",b."Id_Anio",b."Id_Folio",b."Pobl",b."Ctel",b."Manz",b."Pred",b."Unid","F_AtendidoGraficacion" FROM "Regularizacion_PaqueteEnvioGr" A,"Regularizacion_Graficacion" b WHERE A."Id_PaqueteEnvioGraficacion"=b."Id_PaqueteEnvioGraficacion";';
        return $this->db->query($sql)->getResult();
    }

    //TODO interfaz
    public function consultar_movimientos_reg_ventanilla()
    {
        $sql = 'SELECT A."Clave",b."FechaCapturaInconformidad",b."IdTramite",C."DescripcionTramite","F_InspeccionCampo",d."Descripcion",A."Id_MovimientoInspeccion1" FROM "Regularizacion_Inspeccion" AS A,"Inco_Barra" AS b,"Inco_Tramites" AS C,"Catalogo_MovimientoInspeccion" AS d WHERE A."Clave"=b."Clave" AND b."IdTramite"=C."IdTramite" AND A."Id_MovimientoInspeccion1"=d."Id_MovimientoInspeccion" AND SUBSTRING (A."Clave",1,9)=SUBSTRING (b."Clave",1,9);';
        return $this->db->query($sql)->getResult();
    }

    public function get_resultados_graficacion()
    {
        $sql = 'SELECT "Id_ResultadoGraficacion", "Descripcion" FROM "Catalogo_ResultadoGraficacion"';
        return $this->db->query($sql)->getResult();
    }

    public function get_graficadores(){
        $sql = 'SELECT b."IdEmpleado", b."Login", b."Nombre", b."ApellidoPaterno" FROM "Gen_Empleados" b';
        return $this->db->query($sql)->getResult();
    }

    public function get_movimientos_graficacion(){
        $sql = 'SELECT "Id_MovimientoGraficacion","Descripcion" FROM "Catalogo_MovimientoGraficacion" WHERE "Id_MovimientoGraficacion" BETWEEN \'A\' AND \'ZP\'';
        return $this->db->query($sql)->getResult();
    }

    public function get_folios_sin_graficador(){
        $sql = 'SELECT "Id_Anio","Id_Folio","Pobl","Ctel","Manz","Pred","Unid","Id_Graficador","F_AsignadoGraficador" FROM "barra_graficacion" WHERE "Id_Graficador" IS NULL ORDER BY "Id_Anio" DESC,"Id_Folio" DESC';
        return $this->db->query($sql)->getResult();
    }

    public function asignar_graficador($data = null){
        $sql = 'UPDATE "barra_graficacion" SET "F_AsignadoGraficador"= NOW() ,"Id_Graficador"= \''.$data["Graficador"].'\' WHERE "Id_Anio"= \''.$data["Anio"].'\' AND "Id_Folio"= \''.$data["Folio"].'\'';
        //echo $sql;
        return $this->db->query($sql);     
    }

    public function capturar_graficacion_ventanilla($data = null){
        $sql = 'UPDATE "barra_graficacion" SET "F_AtendidoGraficacion"=\''.$data["FAtendido"].'\',"Id_Graficador"=\''.$data["Graficador"].'\',"Id_MovimientoGraficacion1"=\''.$data["Movimiento"].'\',"Id_ResultadoGraficacion"=\''.$data["Resultado"].'\',"Dictamen"=\''.$data["Dictamen"].'\',"Observaciones"=\''.$data["Observaciones"].'\' WHERE "Id_Anio"=\''.$data["Anio"].'\' AND "Id_Tipo"=\'B\' AND "Id_Folio"=\''.$data["Folio"].'\' AND "NumeroOficio" IS NULL; ';

        $res = $data["Resultado"];
        //Se actualiza la tabla de seguimiento dependiendo el resultado del dictamen
        $Id_QuienLoTiene = "";
        //si faltan datos o no hay lamina o falta croquis o ya esta graficado
        if($res == 'FD' || $res == 'NL' || $res == 'FC' || $res == 'GR')
            $Id_QuienLoTiene = "DG"; //lo tiene el departamento de graficacion
        //si no hay expediente o no copero o fue regresado por ices
        if($res == 'NE' || $res == 'NC' || $res == 'RI')
            $Id_QuienLoTiene = "DG"; //lo tiene el departamento de graficacion
        //si es enviado a inspeccion
        if($res == 'EI')
            $Id_QuienLoTiene = "DI"; //lo tiene el departamento de inspeccion
        //si es un movimiento realizado
        if($res == 'MR')
            $Id_QuienLoTiene = "CM"; //lo marcamos como Concluido Movimiento Realizado
        //si no procede
        if($res =='NP')
            $Id_QuienLoTiene = "CN"; //lo marcamos como Concluido No Procedio
        //si fue regresado
        if($res =='RE')
            $Id_QuienLoTiene = "DG"; //lo tiene el departamento de graficacion

        $sql2 = 'UPDATE "Barra_Seguimiento" SET "F_AtendidoGraficacion"=\''.$data["FAtendido"].'\',"Id_EstadoActual"=\''.$data["Resultado"].'\',"Id_QuienLoTiene"=\''.$Id_QuienLoTiene.'\' WHERE "Id_Anio"=\''.$data["Anio"].'\' AND "Id_FolioBarra"=\''.$data["Folio"].'\';';
        
        $q = $sql.$sql2;
        
        //echo $q;
        
        return $this->db->query($q);     
    }

    public function capturar_graficacion_regularizacion($data = null){
        $sql = 'UPDATE "Regularizacion_Graficacion" SET "F_AtendidoGraficacion"=\''.$data["FAtendido"].'\',"Id_Graficador"=\''.$data["Graficador"].'\',"Id_MovimientoGraficacion1"=\''.$data["Movimiento"].'\',"Id_ResultadoGraficacion"=\''.$data["Resultado"].'\',"Dictamen"=\''.$data["Dictamen"].'\',"Observaciones"=\''.$data["Observaciones"].'\' WHERE "Id_Anio"=\''.$data["Anio"].'\' AND "Id_Tipo"=\'R\' AND "Id_Folio"=\''.$data["Folio"].'\' AND "NumeroOficio" IS NULL; ';
        
        //echo $sql;
        
        return $this->db->query($sql);     
    }

    public function consultar_folio($a = null, $fol = null)
    {
        $sql = 'SELECT * FROM "barra_graficacion" WHERE "Id_Anio" = \''.$a.'\' AND "Id_Folio" = \''.$fol.'\''; 
        return $this->db->query($sql)->getResult();
    }

    public function consultar_folio_regularizacion($a = null, $fol = null)
    {
        $sql = 'SELECT * FROM "Regularizacion_Graficacion" WHERE "Id_Anio" = \''.$a.'\' AND "Id_Folio" = \''.$fol.'\';'; 
        return $this->db->query($sql)->getResult();
    }

    public function get_empleados(){
        $sql = 'SELECT * FROM "Gen_Empleados"';
        return $this->db->query($sql)->getResult();
    }

    public function get_brigadas_inspeccion(){
        $sql = 'SELECT A."Id_Brigada",CONCAT (b."IdEmpleado",\' \',b."Nombre",\' \',b."ApellidoPaterno",\', \',C."IdEmpleado",\' \',C."ApellidoPaterno",\' \',C."Nombre") AS brigada, b."IdEmpleado" as i1, C."IdEmpleado" as i2 FROM "Catalogo_Brigadas" A,"Gen_Empleados" b,"Gen_Empleados" C WHERE A."Id_Empleado1"=b."IdEmpleado" AND A."Id_Empleado2"=C."IdEmpleado"';

        return $this->db->query($sql)->getResult();
    }

    //APARTADO DE REPORTES

    public function reporte_movimientos_ventanilla($f1 = null, $f2 = null){
        $filtro = "";
        
        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"F_AtendidoGraficacion\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."Id_MovimientoGraficacion1" AS ID,b."Descripcion",COUNT (A."Id_Folio") AS Total FROM "barra_graficacion" A,"Catalogo_MovimientoGraficacion" b WHERE A."Id_MovimientoGraficacion1"=b."Id_MovimientoGraficacion" AND (A."Id_ResultadoGraficacion"=\'GR\' OR A."Id_ResultadoGraficacion"=\'MR\') '.$filtro.' GROUP BY A."Id_MovimientoGraficacion1",b."Descripcion"';

        //return $sql ;
        return $this->db->query($sql)->getResult();

    }

    public function reporte_movimientos_graficador_ventanilla($f1 = null, $f2 = null){
        $filtro = "";
        
        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"F_AtendidoGraficacion\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT a."Id_Graficador",  CONCAT(c."Nombre" , \' \' , c."ApellidoPaterno") AS nom,  
        a."Id_MovimientoGraficacion1" AS Id, b."Descripcion",  
        COUNT(a."Id_Folio") As Total  
        FROM barra_graficacion a,  
        "Catalogo_MovimientoGraficacion" b, "Gen_Empleados" c  
        WHERE c."IdEmpleado" = a."Id_Graficador" AND  
        a."Id_MovimientoGraficacion1" = b."Id_MovimientoGraficacion" AND  
        (a."Id_ResultadoGraficacion" = \'GR\' OR a."Id_ResultadoGraficacion" = \'MR\') '.$filtro.' 
        GROUP BY a."Id_Graficador", CONCAT(c."Nombre" , \' \' , c."ApellidoPaterno"), a."Id_MovimientoGraficacion1" , b."Descripcion" 
				order by "Id_Graficador" ASC';

        //return $sql ;
        return $this->db->query($sql)->getResult();

    }

    public function reporte_tramites_detenidos($f1 = null, $f2 = null){
        $filtro = "";
        
        if($f1 != null && $f2 != null){
            $filtro = " AND e.\"FechaCapturaInconformidad\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."Id_Folio",e."FechaCapturaInconformidad",C."DescripcionTramite",A."Id_ResultadoGraficacion",d."Descripcion",A."F_EnvioGraficacion",A."Clave",CONCAT (b."Nombre",\' \',b."ApellidoPaterno") AS Graf,A."Observaciones" FROM "barra_graficacion" AS A,"Gen_Empleados" AS b,"Inco_Tramites" AS C,"Catalogo_ResultadoGraficacion" AS d,"Inco_Barra" AS e WHERE SUBSTRING (A."Id_Folio",4,5)=e."IdInconformidad" AND C."IdTramite"=e."IdTramite" AND A."Id_Anio"=e."AnioDeclaracion" AND b."IdEmpleado"=A."Id_Graficador" AND A."Id_ResultadoGraficacion"=d."Id_ResultadoGraficacion" AND A."Id_ResultadoGraficacion" IN (\'FD\',\'NL\',\'FC\',\'RE\',\'NE\',\'NC\',\'RI\') '.$filtro."";

        //return $sql ;
        return $this->db->query($sql)->getResult();

    }

    public function reporte_tramites_enviados_ices($tram = null, $f1 = null, $f2 = null){

        $filtro = "";
        if($tram == '01'){
            $filtro = " AND SUBSTRING(a.\"IdTramite\",1,2) = '01'";
        }
        else{
            $filtro = " AND SUBSTRING(a.\"IdTramite\",1,2) >= '02' AND SUBSTRING(a.\"IdTramite\",1,2) <= '99'  ";
        }

        if($f1 != null && $f2 != null){
            $filtro .= " AND a.\"FechaOficio\" BETWEEN '$f1' AND '$f2' ";
        }

        $sql = 'SELECT a."FechaOficio", a."NumeroOficio", a."Fol_Mov", b."TramiteBarra",    
        SUM( 
        CASE WHEN SUBSTRING(RTRIM(LTRIM(a."Clave2")), 10, 3) = \'\' THEN 1 ELSE 
            CAST(SUBSTRING(a."Clave2", 10, 3) AS INTEGER) - CAST(SUBSTRING(a."Clave", 10, 3) AS INTEGER) + 1 
        END
        ) AS total  
        FROM "segui_MovimientosBarra" AS a, "segui_TramitesBarra" As b  
        WHERE a."IdTramite" = b."IdTramiteBarra"  AND b.grupo=\'1\' 
        '.$filtro.' 
        GROUP BY a."FechaOficio", a."NumeroOficio", a."Fol_Mov", b."TramiteBarra"';

        return $this->db->query($sql)->getResult();
    }

    public function reporte_oficios_sin_folio($f1 = null, $f2 = null){

        $filtro = "";

        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"FechaOficio\" BETWEEN '$f1' AND '$f2' ";
        }

        $sql = 'SELECT A."NumeroOficio",A."FechaOficio",A."Clave",C."TramiteBarra" FROM "segui_MovimientosBarra" AS A,"segui_TramitesBarra" AS C WHERE A."IdTramite"=C."IdTramiteBarra" AND C.grupo=\'2\' AND A."Fol_Mov" IS NULL AND A."fec_ava" IS NULL '.$filtro.' ';

        return $this->db->query($sql)->getResult();
    }


    //REPORTES DE REGULARIZACION
    public function reporte_movimientos_regularizacion($f1 = null, $f2 = null){
        $filtro = "";
        
        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"F_AtendidoGraficacion\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."Id_MovimientoGraficacion1" AS ID,b."Descripcion",COUNT (A."Id_Folio") AS Total FROM "Regularizacion_Graficacion" A,"Catalogo_MovimientoGraficacion" b WHERE A."Id_MovimientoGraficacion1"=b."Id_MovimientoGraficacion" AND (A."Id_ResultadoGraficacion"=\'GR\' OR A."Id_ResultadoGraficacion"=\'MR\') '.$filtro.' GROUP BY A."Id_MovimientoGraficacion1",b."Descripcion"';

        //return $sql ;
        return $this->db->query($sql)->getResult();

    }

    public function reporte_movimientos_graficador_regularizacion($f1 = null, $f2 = null){
        $filtro = "";
        
        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"F_AtendidoGraficacion\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."Id_Graficador",CONCAT (C."Nombre",\' \',C."ApellidoPaterno") AS nom,A."Id_MovimientoGraficacion1" AS ID,b."Descripcion",COUNT (A."Id_Folio") AS Total FROM "Regularizacion_Graficacion" A,"Catalogo_MovimientoGraficacion" b,"Gen_Empleados" C WHERE C."IdEmpleado"=A."Id_Graficador" AND A."Id_MovimientoGraficacion1"=b."Id_MovimientoGraficacion" AND (A."Id_ResultadoGraficacion"=\'GR\' OR A."Id_ResultadoGraficacion"=\'MR\') '.$filtro.'  GROUP BY A."Id_Graficador",CONCAT (C."Nombre",\' \',C."ApellidoPaterno"),A."Id_MovimientoGraficacion1",b."Descripcion"';

        //return $sql ;
        return $this->db->query($sql)->getResult();

    }

    public function reporte_resumen_tipo_resultado_regularizacion($b1 = null, $b2 = null, $f1 = null, $f2 = null){

        $filtro = "";
        if($b1 != null && $b2 != null){
            $filtro = " AND a.\"Id_Brigada\" BETWEEN '$b1' AND '$b2'";
        }

        if($f1 != null && $f2 != null){
            $filtro .= " AND c.\"F_AtendidoGraficacion\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."Id_Brigada",CONCAT (G."Nombre",\', \',h."Nombre") AS Nombres,b."Descripcion",COUNT (C."Clave") AS total FROM "Catalogo_Brigadas" A,"Catalogo_ResultadoGraficacion" b,"Regularizacion_Graficacion" C,"Regularizacion_PaqueteEnvioGr" e,"Gen_Empleados" G,"Gen_Empleados" h WHERE A."Id_Empleado1"=G."IdEmpleado" AND A."Id_Empleado2"=h."IdEmpleado" AND C."Id_ResultadoGraficacion"=b."Id_ResultadoGraficacion" AND C."Id_PaqueteEnvioGraficacion"=e."Id_PaqueteEnvioGraficacion" AND C."Id_Brigada"=A."Id_Brigada" '.$filtro.'
        GROUP BY A."Id_Brigada",CONCAT (G."Nombre",\', \',h."Nombre"),b."Descripcion"';

        return $this->db->query($sql)->getResult();
    }

    public function reporte_regularizacion_enviada_ices_con_respuesta($f1 = null, $f2 = null){
        $filtro = "";
        
        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"FechaOficio\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."IdTramite",C."TramiteBarra",A."FechaOficio",A."NumeroOficio",SUM (CASE WHEN SUBSTRING (RTRIM(LTRIM(A."Clave2")),10,3)=\'\' THEN 1 ELSE SUBSTRING (A."Clave2",10,3) :: INT-SUBSTRING (A."Clave",10,3) :: INT+1 END) AS total FROM "segui_MovimientosBarra" AS A,"segui_TramitesBarra" AS C WHERE A."IdTramite"=C."IdTramiteBarra" AND A.fec_ava IS NOT NULL AND C.grupo=\'2\' '.$filtro.'
        GROUP BY A."FechaOficio",A."NumeroOficio",A."IdTramite",C."TramiteBarra"';

        //return $sql ;
        return $this->db->query($sql)->getResult();

    }

    public function reporte_regularizacion_enviada_ices_sin_respuesta($f1 = null, $f2 = null){
        $filtro = "";
        
        if($f1 != null && $f2 != null){
            $filtro = " AND a.\"FechaOficio\" BETWEEN '$f1' AND '$f2'";
        }

        $sql = 'SELECT A."IdTramite",C."TramiteBarra",A."FechaOficio",A."NumeroOficio",SUM(CASE WHEN SUBSTRING(RTRIM(LTRIM(a."Clave2")), 10, 3)= \'\' THEN 1 
        ELSE REGEXP_REPLACE(SUBSTRING(a."Clave2", 10, 3),\'[[:alpha:]]\',\'0\',\'g\')::int - REGEXP_REPLACE(SUBSTRING(a."Clave", 10, 3),\'[[:alpha:]]\',\'\',\'g\')::int + 1 END) AS total FROM "segui_MovimientosBarra" AS A,"segui_TramitesBarra" AS C WHERE A."IdTramite"=C."IdTramiteBarra" AND A.fec_ava IS NOT NULL AND C.grupo=\'2\' '.$filtro.'
        GROUP BY A."FechaOficio",A."NumeroOficio",A."IdTramite",C."TramiteBarra"';

        //return $sql ;
        return $this->db->query($sql)->getResult();

    }
        

}