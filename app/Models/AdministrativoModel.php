<?php namespace App\Models;
use CodeIgniter\Model;
//incluimos la interfaz para la coneccion con la base de datos
use CodeIgniter\Database\ConnectionInterface;

class AdministrativoModel extends Model {

    protected $db;
	public function __construct(ConnectionInterface &$db) {
		$this->db =& $db;
    }


    //Obtener los tramites enviados a inspeccion
    public function get_envios_inspeccion() {
		$sql = 'SELECT A."Id_PaqueteEnvioInspeccion",A."F_EnvioInspeccion",b."Id_Anio",b."Id_Folio",b."Pobl",b."Ctel",b."Manz",b."Pred",b."Unid","F_InspeccionCampo" FROM "Barra_PaqueteEnvioInspeccion" A,"Barra_Inspeccion" b WHERE A."Id_PaqueteEnvioInspeccion"=b."Id_PaqueteEnvioInspeccion"';
        return $this->db->query($sql)->getResult();
    }

    //obtener los tramites que no han sido enviados a inspeccion
    public function get_notin_inspeccion() {
		$sql = 'SELECT "IdInconformidad","Poblacion","Cuartel","Manzana","Predio","Unidad","FechaCapturaInconformidad","AnioDeclaracion", "Clave" FROM "Inco_Barra" WHERE ("IdInconformidad","AnioDeclaracion") NOT IN (
            SELECT SUBSTRING ("Id_Folio",4,8) AS IdInconformidad,"Id_Anio" AS AnioDeclaracion FROM "Barra_Inspeccion") AND ("IdInconformidad","AnioDeclaracion") NOT IN (
            SELECT SUBSTRING ("Id_Folio",4,8) AS IdInconformidad,"Id_Anio" AS AnioDeclaracion FROM "barra_graficacion") ORDER BY "AnioDeclaracion" DESC';
        return $this->db->query($sql)->getResult();
    }

    //obtener la informacion de la configuracion de la fiel
    public function get_firma_data() {
		$sql = 'SELECT * FROM configuracion;';
        return $this->db->query($sql)->getResult();
    }

    //Elimina la configuracion del documento fiel (lo deja en blanco)
    public function eliminar_doc_firma($nombre = ""){
        if(substr($nombre, -3) == 'cer'){
            $sql = "UPDATE configuracion SET cerfile = '';";
        }
        else if(substr($nombre, -3) == 'key'){
            $sql = "UPDATE configuracion SET pemkeyfile = '';";
        }
        return $this->db->query($sql);
    }

    //Registra la configuracion del documento fiel con el nombre del archivo subido
    public function registrar_doc_firma($nombre = ""){
        if(substr($nombre, -3) == 'cer'){
            $sql = "UPDATE configuracion SET cerfile = '$nombre';";
        }
        else if(substr($nombre, -3) == 'key'){
            $sql = "UPDATE configuracion SET pemkeyfile = '$nombre';";
        }
        return $this->db->query($sql);
    }

    //Registra la configuracion de la contraseña del fiel y director de catastro
    public function registrar_datos_firma($data = null){
        $clave = base64_encode($data['passwd']);
        $director = $data['director'];
        $sql = "UPDATE configuracion SET passphrase = '$clave', director = '$director';";
        return $this->db->query($sql);
    }

    //Obtener los tramites que no estan en graficacion
    public function get_notin_graficacion() {
		$sql = 'SELECT "IdInconformidad","Poblacion","Cuartel","Manzana","Predio","Unidad","FechaCapturaInconformidad","AnioDeclaracion", "Clave" FROM "Inco_Barra" WHERE ("IdInconformidad","AnioDeclaracion") NOT IN (
            SELECT SUBSTRING ("Id_Folio",4,8) AS IdInconformidad,"Id_Anio" AS AnioDeclaracion FROM "Barra_Inspeccion") AND ("IdInconformidad","AnioDeclaracion") NOT IN (
            SELECT SUBSTRING ("Id_Folio",4,8) AS IdInconformidad,"Id_Anio" AS AnioDeclaracion FROM "barra_graficacion") ORDER BY "AnioDeclaracion" DESC';
        return $this->db->query($sql)->getResult();
    }

    //Obtener el numero del ultimo paquete enviado a inspeccion
    public function genera_bitacora_inspeccion(){
        $sql = 'SELECT MAX("Id_PaqueteEnvioInspeccion") as maximo FROM "Barra_PaqueteEnvioInspeccion"';
        return $this->db->query($sql)->getResult();
    }

    //Registra el paquete de envio a inspeccion
    public function registrar_bitacora_inspeccion($data = null){
        $folios = $data['Folios'];
        //Insertar en barra paquete envio inspeccion
        $sql1 = 'INSERT INTO "Barra_PaqueteEnvioInspeccion" ("Id_PaqueteEnvioInspeccion","F_EnvioInspeccion","Id_QuienEnvia","Id_QuienRecibe") VALUES' . " ('".$data['Bitacora']."','".$data['FechaEnv']."','".$data['Envia']."','".$data['Recibe']."'); ";

        $q = $sql1;
        foreach($folios as $fol){
            //INSERTAR EL DETALLE DEL PAQUETE OSEA TODOS LOS FOLIOS EN BARRA INSPECCION CON EL ID DEL PAQUETE
        $sql2 = 'INSERT INTO "Barra_Inspeccion" ("Id_Tipo","Id_PaqueteEnvioInspeccion","F_EnvioInspeccion","Id_Anio","Id_Folio","Pobl","Ctel","Manz","Pred","Unid","Clave") VALUES ';
            $sql2folios = "('B','".$data['Bitacora']."','".$data['FechaEnv']."','".$fol['AnioDeclaracion']."','000".$fol['IdInconformidad']."','".str_pad($fol['Poblacion'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Cuartel'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Manzana'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Predio'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Unidad'], 3, "0", STR_PAD_LEFT)."','".$fol['Clave']."'); ";
            //ACTUALIZAR LA TABLA BARRA_SEGUIMIENTO POR CADA FOLIO INSERTADO EN BARRA INSPECICON
            $sql3 = 'UPDATE "Barra_Seguimiento" SET "F_EnvioInspeccion" = \''.$data["FechaEnv"].'\', "Id_QuienLoTiene" = \'DI\' WHERE "Id_Anio" = \''.$fol['AnioDeclaracion'].'\' AND "Id_FolioBarra" = \'000'.$fol['IdInconformidad'].'\' AND "Pobl" = \''.str_pad($fol['Poblacion'], 3, "0", STR_PAD_LEFT).'\' AND "Ctel" = \''.str_pad($fol['Cuartel'], 3, "0", STR_PAD_LEFT).'\' AND "Manz" = \''.str_pad($fol['Manzana'], 3, "0", STR_PAD_LEFT).'\' AND "Pred" = \''.str_pad($fol['Predio'], 3, "0", STR_PAD_LEFT).'\' AND "Unid" = \''.str_pad($fol['Unidad'], 3, "0", STR_PAD_LEFT).'\'; ';
            $q.= " " . $sql2 . $sql2folios . " " . $sql3;
        }
        //return $q;
        //Ejecutar la insercion;
        return $this->db->query($q);
    }

    //Registra el paquete de envio a graficacion
    public function registrar_bitacora_graficacion($data = null){
        $folios = $data['Folios'];
        //Insertar en barra paquete envio inspeccion
        $sql1 = 'INSERT INTO "Barra_PaqueteEnvioGraficacion" ("Id_PaqueteEnvioGraficacion","F_EnvioGraficacion","Id_QuienEnvia","Id_QuienRecibe", "Origen") VALUES' . " ('".$data['Bitacora']."','".$data['FechaEnv']."','".$data['Envia']."','".$data['Recibe']."', '".$data['Origen']."'); ";

        $sqldel = 'DELETE FROM "barra_graficacion" WHERE '."\"Id_PaqueteEnvioGraficacion\"='".$data['Bitacora']."'; " ;
        $q = $sql1 . $sqldel;
        foreach($folios as $fol){
            //INSERTAR EL DETALLE DEL PAQUETE OSEA TODOS LOS FOLIOS EN BARRA INSPECCION CON EL ID DEL PAQUETE
        $sql2 = 'INSERT INTO "barra_graficacion" ("Id_Tipo","Id_PaqueteEnvioGraficacion","F_EnvioGraficacion","Id_Anio","Id_Folio","Pobl","Ctel","Manz","Pred","Unid","Clave") VALUES ';
            $sql2folios = "('B','".$data['Bitacora']."','".$data['FechaEnv']."','".$fol['AnioDeclaracion']."','000".$fol['IdInconformidad']."','".str_pad($fol['Poblacion'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Cuartel'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Manzana'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Predio'], 3, "0", STR_PAD_LEFT)."','".str_pad($fol['Unidad'], 3, "0", STR_PAD_LEFT)."','".$fol['Clave']."'); ";
            //ACTUALIZAR LA TABLA BARRA_SEGUIMIENTO POR CADA FOLIO INSERTADO EN BARRA INSPECICON
            $sql3 = 'UPDATE "Barra_Seguimiento" SET "F_EnvioGraficacion" = \''.$data["FechaEnv"].'\', "Id_QuienLoTiene" = \'DG\' WHERE "Id_Anio" = \''.$fol['AnioDeclaracion'].'\' AND "Id_FolioBarra" = \'000'.$fol['IdInconformidad'].'\' AND "Pobl" = \''.str_pad($fol['Poblacion'], 3, "0", STR_PAD_LEFT).'\' AND "Ctel" = \''.str_pad($fol['Cuartel'], 3, "0", STR_PAD_LEFT).'\' AND "Manz" = \''.str_pad($fol['Manzana'], 3, "0", STR_PAD_LEFT).'\' AND "Pred" = \''.str_pad($fol['Predio'], 3, "0", STR_PAD_LEFT).'\' AND "Unid" = \''.str_pad($fol['Unidad'], 3, "0", STR_PAD_LEFT).'\'; ';
            $q.= " " . $sql2 . $sql2folios . " " . $sql3;
        }
        //return $q;
        //Ejecutar la insercion;
        return $this->db->query($q);
    }
    
    //Obtener todos los envios a graficacion
    public function get_envios_graficacion(){
        $sql = 'SELECT a."Id_PaqueteEnvioGraficacion", a."F_EnvioGraficacion", b."Id_Anio", b."Id_Folio", b."Pobl", b."Ctel", b."Manz", b."Pred", b."Unid", "F_AtendidoGraficacion" FROM "Barra_PaqueteEnvioGraficacion" a, "barra_graficacion" b WHERE a."Id_PaqueteEnvioGraficacion" = b."Id_PaqueteEnvioGraficacion"';
        return $this->db->query($sql)->getResult();
    }

    //Obtener el numero del ultimo paquete enviado a graficacion
    public function genera_bitacora_graficacion(){
        $sql = 'SELECT MAX("Id_PaqueteEnvioGraficacion") as maximo FROM "Barra_PaqueteEnvioGraficacion"';

        return $this->db->query($sql)->getResult();
    }

    //Obtener el catalogo de empleados
    public function get_empleados(){
        $sql = 'SELECT * FROM "Gen_Empleados"';
        return $this->db->query($sql)->getResult();
    }

    //Registra un nuevo empleado
    public function registrar_empleado($data = null)
    {
        $IdEmp = $data["idemp"];
        $Nombre = $data["nombre"];
        $ApellidoPaterno = $data["apellidopat"];
        $ApellidoMaterno = $data["apellidomat"];
        $IdPuesto = $data["puesto"];

        //Insertar en barra paquete envio inspeccion
        $sql1 = "INSERT INTO \"Gen_Empleados\"  VALUES ('$IdEmp','$IdEmp','$IdEmp','$Nombre','$ApellidoPaterno','$ApellidoMaterno', '$IdPuesto', '$IdPuesto');";

        //Ejecutar la insercion;
        return $this->db->query($sql1);
    }

    //Actualiza la informacion de un empleado
    public function actualizar_empleado($data = null)
    {
        $IdEmp2 = $data["idemp2"];
        $IdEmp = $data["idemp"];
        $Nombre = $data["nombre"];
        $ApellidoPaterno = $data["apellidopat"];
        $ApellidoMaterno = $data["apellidomat"];
        $IdPuesto = $data["puesto"];

        //Insertar en barra paquete envio inspeccion
        $sql1 = "UPDATE \"Gen_Empleados\" SET \"IdEmpleado\" = '$IdEmp', \"Nombre\" = '$Nombre', \"ApellidoPaterno\" = '$ApellidoPaterno',\"ApellidoMaterno\" = '$ApellidoMaterno', \"IdPuesto\" = '$IdPuesto' WHERE \"IdEmpleado\" = '$IdEmp2';";

        //Ejecutar la insercion;
        return $this->db->query($sql1);
    }

    //Elimina un empleado
    public function eliminar_empleado($id = null)
    {    
        //Insertar en barra paquete envio inspeccion
        $sql1 = "DELETE FROM \"Gen_Empleados\"  WHERE \"IdEmpleado\" = '$id';";
        
        //Ejecutar la insercion;
        return $this->db->query($sql1);
    }

    //Elimina un usuario
    public function eliminar_usuario($id = null)
    {    
        //Insertar en barra paquete envio inspeccion
        $sql1 = "DELETE FROM users WHERE id = $id;";
        
        //Ejecutar la insercion;
        return $this->db->query($sql1);
    }
    
    //Actualiza el permiso para ver el visor catastral
    public function actualizar_permiso_visor($privilegio = null, $id = null){
        //Insertar en barra paquete envio inspeccion
        $sql1 = "UPDATE users SET privilegio_visor = $privilegio WHERE id = $id;";
        
        //Ejecutar la insercion;
        return $this->db->query($sql1);
    }
    

}