<?php namespace App\Models;

//incluimos la interfaz para la coneccion con la base de datos
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;

class ConceptosModel extends Model
{
    //hacemos referencia a la tabla de nuestra base de datos
    protected $table      = 'a_concep';
    //hacemos referencia a la llave primaria de la tabla tramites
    protected $primaryKey = ['CVE_CONCEP'];
}