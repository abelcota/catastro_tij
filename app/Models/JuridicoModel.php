<?php namespace App\Models;
use CodeIgniter\Model;
//incluimos la interfaz para la coneccion con la base de datos
use CodeIgniter\Database\ConnectionInterface;

class JuridicoModel extends Model {

    protected $db;
	public function __construct(ConnectionInterface &$db) {
		$this->db =& $db;
    }

    //METODOS PARA LOS ENVIOS A ICES NOTICIA DE CAMBIOS
    public function get_bitacoras_noticias_cambios(){
        $sql = 'SELECT "AnioBitacora", "IdBitacora", "Fecha", "Area", "NumeroOficio", "FechaOficio", "Asunto" FROM "Jur_BitacoraDetalle"';
        return $this->db->query($sql)->getResult();
    }

    public function genera_bitacora_noticias_cambios($a){
        $sql = 'SELECT COALESCE(MAX("IdBitacora"),\'0\') as maximo FROM "Jur_BitacoraMaestro" WHERE "AnioBitacora"=\''.$a.'\'';
        return $this->db->query($sql)->getResult();
    }

    public function get_noticiascambios_detalle_sinbitacora(){
        $sql = 'SELECT * FROM "Jur_BitacoraDetalle" WHERE "IdBitacora" IS NULL';
        return $this->db->query($sql)->getResult();
    }

    public function registrar_bitacora_noticia_detalle($data = null){
        $sql1 = 'INSERT INTO "Jur_BitacoraDetalle" ("AnioBitacora", "IdBitacora", "Fecha", "IdRenglon", "Area", "NumeroOficio", "FechaOficio", "Asunto", "Tramite", "Solucion", "Observaciones") ';

        $sqlv = "VALUES (null, null, null, null, '".$data["Area"]."', '".$data["NumeroOficio"]."', '".$data["FechaOficio"]."', '".$data["Asunto"]."', '".$data["Tramite"]."', '".$data["Solucion"]."', '".$data["Observaciones"]."');";
          
        $q = $sql1.$sqlv;
        //return $q;
        return $this->db->query($q);
    }

    public function registrar_bitacora_noticia($data = null){
        $sql1 = 'INSERT INTO "Jur_BitacoraMaestro" ("AnioBitacora", "IdBitacora", "Fecha", "IdEmpleado") VALUES (\''.$data['AnioBitacora'].'\', \''.$data['IdBitacora'].'\' , \''.$data['Fecha'].'\', \''.$data['Empleado'].'\'); ';
        
        //actualizar cada uno de los folios seleccionados
        $folios = $data['Folios'];
        $sql2 = "";
        foreach($folios as $fol){
            //ACTUALIZAR EL DETALLE DEL 
            $sql2 .= 'UPDATE "Jur_BitacoraDetalle" SET "IdBitacora" = \''.$data['IdBitacora'].'\', "AnioBitacora" = \''.$data['AnioBitacora'].'\', "Fecha" = \''.$data['Fecha'].'\' WHERE "Area" = \''.$fol['Area'].'\' and "NumeroOficio" = \''.$fol['NumeroOficio'].'\'; ';     
        }
        $q = $sql1.$sql2;
        //return $q;
        return $this->db->query($q);
    }


    
    ///METODOS PARA LOS ENVIOS A ICES CONTROLES ISAI

    public function get_envios_ices(){
        $sql = 'SELECT A."IdEnvioIsai",A."Fecha",b."Clave",b."Propietario",b."Adquiriente" FROM "Jur_IsaiMaestro" A,"Jur_IsaiDetalle" b WHERE A."IdEnvioIsai"=b."IdEnvioIsai"';
        return $this->db->query($sql)->getResult();
    }

    public function get_isiai_detalle_sinbitacora(){
        $sql = 'SELECT * FROM "Jur_IsaiDetalle" WHERE "IdEnvioIsai" IS NULL';
        return $this->db->query($sql)->getResult();
    }

    public function genera_bitacora(){
        $sql = 'SELECT COALESCE(MAX("IdEnvioIsai"),\'0\') as maximo FROM "Jur_IsaiMaestro"';
        return $this->db->query($sql)->getResult();
    }

    public function get_propietario($cve = null){
        $sql = 'SELECT "APE_PAT", "APE_MAT", "NOM_PROP" FROM a_prop AS a, a_unid AS b WHERE b."CLAVE" = \''.$cve.'\' AND b."CVE_PROP" = a."CVE_PROP"';
        return $this->db->query($sql)->getResult();
    }

    public function registrar_envio_ices_detalle($data = null){
        $sql1 = 'INSERT INTO "Jur_IsaiDetalle" ("IdEnvioIsai", "Fecha", "IdRenglon", "Pobl", "Ctel", "Manz", "Pred", "Unid", "Clave", "Propietario", "Adquiriente") ';

        $sqlv = "VALUES (null, null, null, '".$data["Pobl"]."', '".$data["Ctel"]."', '".$data["Manz"]."', '".$data["Pred"]."', '".$data["Unid"]."', '".$data["Clave"]."', '".$data["Propietario"]."', '".$data["Adquiriente"]."');";
          
        $q = $sql1.$sqlv;
        //return $q;
        return $this->db->query($q);
    }

    public function registrar_bitacora_envio_ices($data = null){
        $sql1 = 'INSERT INTO "Jur_IsaiMaestro" ("IdEnvioIsai", "Fecha", "IdEmpleado") VALUES (\''.$data['Bitacora'].'\', \''.$data['FechaEnv'].'\', \''.$data['Empleado'].'\'); ';
        
        //actualizar cada uno de los folios seleccionados
        $folios = $data['Folios'];
        $sql2 = "";
        foreach($folios as $fol){
            //INSERTAR EL DETALLE DEL PAQUETE OSEA TODOS LOS FOLIOS EN BARRA INSPECCION CON EL ID DEL PAQUETE
            $clave = $fol['Pobl'].$fol['Ctel'].$fol['Manz'].$fol['Pred'].$fol['Unid'];
            $sql2 .= 'UPDATE "Jur_IsaiDetalle" SET "IdEnvioIsai" = \''.$data['Bitacora'].'\', "Fecha" = \''.$data['FechaEnv'].'\' WHERE "Clave" = \''.$clave.'\' and "Adquiriente" = \''.$fol['Adquiriente'].'\'; ';     
        }
        $q = $sql1.$sql2;
        //return $q;
        return $this->db->query($q);
    }

    public function get_empleados(){
        $sql = 'SELECT * FROM "Gen_Empleados"';
        return $this->db->query($sql)->getResult();
    }

}
    
