<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
 <!-- Buttons examples -->
 <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
   
   <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
   

   <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


   <!-- App js -->
   <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
   <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
   <script src="<?php echo base_url("/assets/js/dataTables.fixedHeader.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jquery.repeater/jquery.repeater.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/pages/form-repeater.init.js");?>"></script>
   <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>

   
<script>
$(document).ready(function () {

var anio = new Date().getFullYear().toString(); 
var a = anio.substr(2,4);
$('#year').val(a); 

cargar_empleados()

// Setup - add a text input to each footer cell
$('#tbl-bitacoras thead tr').clone(true).appendTo( '#tbl-bitacoras thead' );
$('#tbl-bitacoras thead tr:eq(1) th').each( function (i) {
    var title = $(this).text();
    $(this).html( '<input type="text" class="form-control small" placeholder="Filtrar '+title+'" />' );

    $( 'input', this ).on( 'keyup change', function () {
        if ( table.column(i).search() !== this.value ) {
            table
                .column(i)
                .search( this.value )
                .draw();
        }
    } );
} );

var table = $('#tbl-bitacoras').DataTable({
    "orderCellsTop": true,
    "language": {
        "url": "<?php echo base_url("assets/Spanish.json")?>"
    },
    "bProcessing": true,
    "sAjaxSource": "<?php echo base_url("juridico/get_bitacoras_noticias_cambios")?>",
    "bPaginate":true,
    "sPaginationType":"full_numbers",
    "iDisplayLength": 25,
    "order": [[ 0, "desc" ], [1, "desc"]],
    "aoColumns": [
        { mData: 'AnioBitacora' } ,
        { mData: 'IdBitacora' },
        { mData: 'Fecha' },
        { mData: 'Area' },
        { mData: 'NumeroOficio' },
        { mData: 'FechaOficio' },
    ],
    rowCallback: function(row, data, index) {
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        $("td:eq(1)", row).addClass('text-primary font-weight-bold');
        $("td:eq(2)", row).text(moment(data['Fecha']).format('L'));
        $("td:eq(5)", row).text(moment(data['FechaOficio']).format('L'));
    }
});

});

$( document ).on('click', '#btn_gen_fol', function(e) {
    e.preventDefault();
    genera_bitacora();
});


$( document ).on('click', '#btn_captura', function(e) {
    e.preventDefault();
    genera_bitacora();
    $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD"));  
    folios = [];
    $(".modal-captura").modal("show");
    $('#tbldetalle').DataTable().destroy();
        //load table
        var tablef = $('#tbldetalle').DataTable({
            "language": {
                "url": "<?php echo base_url("assets/Spanish.json")?>"
            },
            "ordering": true,
            "sAjaxSource": "<?php echo base_url("juridico/get_noticiascambios_detalle_sinbitacora")?>",
            "bPaginate":true,
            "sPaginationType":"full_numbers",
            "iDisplayLength": 10,
            "aoColumns": [
                {
                    data: null,
                    defaultContent: '<input type="checkbox" class="fol" name="folios[]">'
                },
                { mData: 'Area' } ,
                { mData: 'NumeroOficio' },
                { mData: 'FechaOficio' },
                { mData: 'Asunto' },
                { mData: 'Tramite' },
                { mData: 'Solucion' },
                { mData: 'Observaciones' },
            ],
            rowCallback: function(row, data, index) {               
       
            }
        });

        $('#tbldetalle tbody').on( 'click', '.fol', function () {
                    if(tablef.row(this).child.isShown()){
                        var data = tablef.row(this).data();
                    }else{
                        var data = tablef.row($(this).parents("tr")).data();
                    }
                    if ( folios.includes(data) ){
                        folios = folios.filter(value => value !== data);
                    }
                    else{
                        folios.push(data);
                    }

                    console.log(folios);
                    
            });

}); 

$( document ).on('click', '#btn_guardar', function(e) {
    e.preventDefault();
    var bitacora = $("#bitacora").val();
    if (!$('#bitacora').val() || folios.length === 0) {
        Swal.fire({
                                    icon: 'warning',
                                    title: 'Mensaje del Sistema',
                                    text: 'Por favor Genere un Número de Bitácora y seleccione al menos una clave para realizar el Envio'
                                    
                                })
    }
    else{
    var fecha = $("#dtfecha").val();
    var empleado = $("#empleado").val();
    var anio = $("#year").val();
    //los folios seleccionados
    var selectfolios = folios;
    //alert(bitacora + " " + fecha + " " + empleado + " " + " " + selectfolios);
    $.ajax({
        type: "POST",
        url: "<?php echo base_url("juridico/registrar_bitacora_noticia"); ?>",
        type: "POST",
        data: {"Bitacora": bitacora, "FechaEnv": fecha, "Folios": selectfolios, "Empleado": empleado, "Anio": anio },
        cache: false,
        success: function(data){
            //console.log(data);
            if (data == 'ok')
            {
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'success',
                                    title: 'Mensaje del Sistema',
                                    text: 'La Bitacora de Envío a ICES se registro correctamente!'
                                    
                                })
                //cerramos el modal 
                $('#tbl-bitacoras').DataTable().ajax.reload(null, false);
                $('#tbldetalle').DataTable().ajax.reload(null, false);
                //genera una nueva folio para bitacora
                genera_bitacora();
            }
            else{
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'Ocurrió un error al registrar la bitacora de envío a ICES'
                                    
                                })
            }
        }
    });

    }
   
});   

$( document ).on('click', '#btn_guardar_detalle', function(e) {
    e.preventDefault();
    var form = $("#form-detalle");
    data = form.serialize();
    //console.log(data);
    $.ajax({
        type: "POST",
        url: "<?php echo base_url("juridico/registrar_bitacora_noticia_detalle"); ?>",
        type: "POST",
        data: data,
        cache: false,
        success: function(data){
            //console.log(data);
            if (data == 'ok')
            {
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'success',
                                    title: 'Mensaje del Sistema',
                                    text: 'La captura se Registro Correctamente'
                                    
                                })
                //cerramos el modal 
                //$('.modal-').modal('hide');
                $('#tbldetalle').DataTable().ajax.reload(null, false);
                //reset el form
                $("#form-detalle")[0].reset();
            }
            else{
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'Ocurrió un error al guardar la Captura'
                                    
                                })
            }
        }
    });
});   


    function genera_bitacora()
    {
        var anio = $('#year').val();
        $.getJSON("<?php echo base_url("juridico/genera_bitacora_noticias_cambios");?>/"+anio, function(json){ 
                    //console.log(json);
                    $.each(json.result, function(i, obj){
                        var max = parseInt(obj.maximo);
                        var max = max + 1; 
                        $('#bitacora').val(zeroPad(max, 8));
                    });
        }); 
    }  

  
    //funcion para formatear los campos de la clave catastral
    function zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }

    function cargar_empleados(){
            $.getJSON("<?php echo base_url("juridico/get_empleados");?>", function(json){
                    $('#empleado').empty();
                    $('#empleado').append($('<option>').text("Seleccionar Empleado").attr({'value':""}));
                    $.each(json.result, function(i, obj){
                            $('#empleado').append($('<option>').text(obj.Nombre + " " + obj.ApellidoPaterno + " " + obj.ApellidoMaterno).attr({ 'value' : obj.IdEmpleado}));
                    });
                    //$('#empleados').select2();
            });
         }

</script>