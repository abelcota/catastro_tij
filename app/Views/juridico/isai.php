<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">BITÁCORAS DE ENVIO AL ICES -CONTROLES ISAI</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">SEG. JURÍDICO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <!-- start row -->
                    <div class="row">
                     <div class="btn-toolbar p-3" role="toolbar">
                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_captura"><i class="fa fa-plus"></i> Captura de Envío a ICES - Controles ISAI</button>
                                        </div>

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button disabled type="button" class="btn btn-primary waves-light waves-effect dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                Más <i class="mdi mdi-dots-vertical ml-2"></i>
                                            </button>
                                            <div class="dropdown-menu" style="">
                                                <a class="dropdown-item" href="#"><i class="fa fa-edit"></i> Editar</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Borrar Registro</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-print"></i> Imprimir</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end toolbar -->
                          <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    
                                <h5 class="card-title">Bitácoras de Envío al ICES - Controles ISAI</h5>
                                <hr>
                                     <!-- start form -->

                                        <div class="row">
                                            <div class="col-lg-12 table-responsive" style="margin-top:10px;">
                                                <table class="table display table-hover table-sm nowrap" id="tbl-bitacoras">
                                                    <thead class="text-white" style="background-color: #480912;">
                                                        <th class="text-center">Bítacora</th>
                                                        <th class="text-center">Fecha</th>
                                                        <th class="text-center">Clave</th>
                                                        <th class="">Propietario</th>
                                                        <th class="">Adquiriente</th>
                                                        </thead>
                                                    </table>
                                            </div>
                                        </div> 
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->

                     <!--modal para ver el listado de solicitudes -->
                     <div class="modal fade modal-captura" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                        <div class="modal-dialog modal-lg modal-dialog-top">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title mt-0">REGISTRO DE BÍTACORAS DE ENVÍO AL ICES</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body"> 
                                            <!-- start form -->
                                            <form class="repeater" id="form-detalle">
                                           
                                               <h5 class="card-title">Registrar Detalles del Envío</h5>
                                               <div class="row">        
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral:</label>
                                                    
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="POBL" id="POBL" name="POBL" maxlength="3"> 
                                                                <input type="text" class="form-control" name="CTEL" placeholder="CTEL" id="CTEL" maxlength="3" >
                                                                <input type="text" class="form-control" placeholder="MANZ" id="MANZ" name="MANZ" maxlength="3">
                                                                <input type="text" class="form-control" name="PRED" placeholder="PRED" id="PRED" maxlength="3">
                                                                <input type="text" class="form-control" placeholder="UNID" id="UNID" name="UNID" maxlength="3">
                                                                <span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_get_prop"><i class="mdi mdi-account-search"></i></button></span>
                                                               
                                                            </div>
                                                
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Propietario:</label>
                                                    <input type="text" class="form-control" id="propietario"  name="propietario">
                                                
                                                </div>
                                            </div> <!-- end col 12 -->
                                            <div class="col-lg-12">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Adquiriente:</label>
                                                    <input type="text" class="form-control" id="adquiriente"  name="adquiriente">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                            <button type="button" class="btn btn-success float-right"  id="btn_guardar_detalle">Registrar Detalle</button>
                                            </div>
                                            

                                    </div>       
                                               <!-- end form -->
                                             
                                    </form>
                                    <hr>
                                    <div class="row">
                                            
                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="bitacora">No. Bitacora:</label>
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected"><input data-toggle="touchspin" type="text" id="bitacora" name="bitacora" class="form-control" readonly><span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_gen_fol">+</button></span></div>

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="fecha">Fecha:</label>
                                                    <input type="date" class="form-control" id="dtfecha" name="dtfecha">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="bitacora">Empleado:</label>
                                                                            <select class="form-control" id="empleado" name="empleado">
                                                                            <option>Seleccionar Empleado</option>
                                                                            </select>
                                            </div>
                                        </div>
                                           <!--formulario para agregar-->
                                           <hr>
                                                                <!--tabla de los folios para el paquete -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                        <div class="table-responsive">
                                        <table class="table table-hover nowrap table-sm" id="tbldetalle" width="100%">
                                                                        <thead class="text-white" style="background-color: #480912;">
                                                                        <tr>
                                                                            <th></th>
                                                                            <th>Pobl</th>
                                                                            <th>Ctel</th>
                                                                            <th>Manz</th>
                                                                            <th>Pred</th>
                                                                            <th>Unid</th>
                                                                            <th>Propietario</th>
                                                                            <th>Adquiriente</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                    </div>
                                        </div>
                                    </div>
                                                                </div>
                                                                <div class="modal-footer">              
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_tcancelar">Cancelar</button>
                                                                    <button type="button" class="btn btn-success"  id="btn_guardar">Registrar Bitácora</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            