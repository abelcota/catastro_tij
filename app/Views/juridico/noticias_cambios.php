<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">BITÁCORAS DE ENVIO AL ICES -NOTICIAS DE CAMBIOS</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">SEG. JURÍDICO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                   <!-- start row -->
                   <div class="row">
                     <div class="btn-toolbar p-3" role="toolbar">
                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_captura"><i class="fa fa-plus"></i> Captura de Envío a ICES - Noticias de Cambios</button>
                                        </div>

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button disabled type="button" class="btn btn-primary waves-light waves-effect dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                Más <i class="mdi mdi-dots-vertical ml-2"></i>
                                            </button>
                                            <div class="dropdown-menu" style="">
                                                <a class="dropdown-item" href="#"><i class="fa fa-edit"></i> Editar</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Borrar Registro</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-print"></i> Imprimir</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end toolbar -->
                          <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    
                                <h5 class="card-title">Bitácoras de Envío al ICES - Noticias de Cambios</h5>
                                <hr>
                                     <!-- start form -->

                                        <div class="row">
                                            <div class="col-lg-12 table-responsive" style="margin-top:10px;">
                                                <table class="table display table-hover table-sm nowrap" id="tbl-bitacoras">
                                                    <thead class="text-white" style="background-color: #480912;">
                                                        <th class="text-center">Año</th>
                                                        <th class="text-center">Bitacora</th>
                                                        <th class="text-center">Fecha</th>
                                                        <th class="">Area</th>
                                                        <th class="">Numero Oficio</th>
                                                        <th class="">Fecha Oficio</th>
                                                        </thead>
                                                    </table>
                                            </div>
                                        </div> 
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->

                     <!--modal para ver el listado de solicitudes -->
                     <div class="modal fade modal-captura" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                        <div class="modal-dialog modal-lg modal-dialog-top">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title mt-0">REGISTRO DE BÍTACORAS DE ENVÍO AL ICES - NOTICIAS DE CAMBIOS</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body"> 
                                            <!-- start form -->
                                            <form class="repeater" id="form-detalle">
                                           
                                               <h5 class="card-title">Registrar Detalles del Envío</h5>
                                               <div class="row"> 

                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Area:</label>
                                                    <input type="number" class="form-control" id="area"  name="area">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Numero Oficio:</label>
                                                    <input type="text" class="form-control" id="numeroof"  name="numeroof">
                                                
                                                </div>
                                            </div> <!-- end col 12 -->
                                            <div class="col-lg-4">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Fecha:</label>
                                                    <input type="date" class="form-control" id="fechadet"  name="fechadet">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Asunto:</label>
                                                    <textarea class="form-control" id="asunto" rows="2" name="asunto"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Trámite:</label>
                                                    <textarea class="form-control" id="tramite" rows="2" name="tramite"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Solución:</label>
                                                    <textarea class="form-control" id="solucion" rows="2" name="solucion"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Observaciones:</label>
                                                    <textarea class="form-control" id="observaciones" rows="2" name="observaciones"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                            <button type="button" class="btn btn-success float-right"  id="btn_guardar_detalle">Registrar Detalle</button>
                                            </div>
                                            

                                    </div>       
                                               <!-- end form -->
                                             
                                    </form>
                                    <hr>
                                    <div class="row">
                                    
                                            
                                            <div class="col-md-3">
                                                <div class="form-group position-relative">
                                                    <label for="bitacora">No. Bitacora:</label>
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected"><input data-toggle="touchspin" type="text" id="bitacora" name="bitacora" class="form-control" readonly><span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_gen_fol">+</button></span></div>

                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="bitacora">Año:</label>
                                                    <input data-toggle="touchspin" type="text" id="year" name="year" class="form-control">

                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group position-relative">
                                                    <label for="fecha">Fecha:</label>
                                                    <input type="date" class="form-control" id="dtfecha" name="dtfecha">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                <label for="bitacora">Empleado:</label>
                                                                            <select class="form-control" id="empleado" name="empleado">
                                                                            <option>Seleccionar Empleado</option>
                                                                            </select>
                                                </div>
                                            </div>
                                            
                                        </div>
                                           <!--formulario para agregar-->
                                           <hr>
                                                                <!--tabla de los folios para el paquete -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                        <div class="table-responsive">
                                        <table class="table table-hover nowrap table-sm" id="tbldetalle" width="100%">
                                                                        <thead class="text-white" style="background-color: #480912;">
                                                                        <tr>
                                                                            <th></th>
                                                                            <th class="text-center">Area</th>
                                                                            <th class="text-center">Numero Oficio</th>
                                                                            <th class="text-center">Fecha</th>
                                                                            <th class="">Asunto</th>
                                                                            <th class="">Trámite</th>
                                                                            <th class="">Solución</th>
                                                                            <th class="">Observaciones</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                    </div>
                                        </div>
                                    </div>
                                                                </div>
                                                                <div class="modal-footer">              
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_tcancelar">Cancelar</button>
                                                                    <button type="button" class="btn btn-success"  id="btn_guardar">Registrar Bitácora</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            