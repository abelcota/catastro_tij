            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">PÁGINA DE INICIO</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">PAGINAS</a></li>
                                        <li class="breadcrumb-item active">PAGINA DE INICIO</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <!-- start col 6 -->
                        <div class="col-lg-12 text-center">
                        <div class="card" style="opacity: 1;">
                            <div class="card-body">
                                <h2 class="display-5"><?php echo strtoupper($usuario->username); ?></h2>
                                <h5>Usuario de: <?php echo $rol; ?></h5>
                                <p class="lead">Sistema de Administración Catastral </p>
                                <!--<img class="img-fluid" src="<?php //echo base_url("assets/images/logo_v.png"); ?>" width="170">-->
                               
                                    <img class="img-fluid ml-2 mr-2" src="<?php echo base_url("assets/images/aytoColor.png"); ?>" width="350">
                               
                                
                            </div>
                                
                                
                            </div>
                            
                        </dv>
                    </div>

                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->