<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $titulo; ?> | Sistema Administrativo Catastral</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Sistema de Catastro" name="description" />
    <meta content="Direccion de Catastro Municipal" name="author" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#480912">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#480912">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#480912">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url("assets/images/favicon.ico");?>">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Georama:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    
     <!-- Select 2 -->
     <link href="<?php echo base_url("assets/libs/select2/css/select2.min.css");?>" rel="stylesheet" type="text/css" />
     <link href="<?php echo base_url("assets/css/select2-bootstrap.min.css");?>" rel="stylesheet" type="text/css" />
    <!-- Bootstrap Css -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?php echo base_url("assets/css/icons.min.css");?>" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?php echo base_url("assets/css/app.min.css");?>" id="app-style" rel="stylesheet" type="text/css" />
     <!-- DataTables -->
     <link href="<?php echo base_url("assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css");?>" rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css");?>" rel="stylesheet" type="text/css" />
     <!-- Sweet Alert-->
     <link href="<?php echo base_url("assets/libs/sweetalert2/sweetalert2.min.css");?>" rel="stylesheet" type="text/css" />
     <!-- open Layers -->
     <link rel="stylesheet" href="<?php echo base_url("assets/css/ol.css");?>" type="text/css">
     <link rel="stylesheet" href="<?php echo base_url("assets/css/ol-layerswitcher.css");?>" />

     <link href="<?php echo base_url("assets/css/ol-contextmenu.min.css");?>" rel="stylesheet">
    <!-- Lightbox css -->
    <link href="<?php echo base_url("assets/libs/magnific-popup/magnific-popup.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url("assets/css/jquery-ui.css");?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url("https://cdn.datatables.net/rowgroup/1.1.3/css/rowGroup.dataTables.min.css");?>" rel="stylesheet" type="text/css" />
    <style>
       body {
            background: linear-gradient(rgba(230, 230, 230, 0.95), rgba(255, 255, 255, 0.9)),url(<?php //echo base_url("assets/images/fondo2.png");?>);
            background-attachment: fixed;
            background-size: cover;
            padding: 1em 0;
      
        }

        .nav-pills > li > a.active {
                background-color: #480912!important;
            }

        .number-references{
            background:#f6f7f8;
            border-radius:4px;
            font-size:14px;
            font-weight:500;
            padding:8px;
            border:1px 
            dashed #949ba8
        }
        
    </style>
</head>

<body <?php if($page != 'impavaluo'): ?>data-layout="detached" data-topbar="colored" <?php endif;?> >
<div id="loading-indicator" class="d-print-none" style="display: none">
<img src="http://adroitprojects.com/images/icons/preloader.gif" />
</div>
     <!-- Loader -->
    <div id="preloader">      
       
        <div id="status">
            <small>Cargando...</small><br><br>
           
            <div class="spinner-chase">
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <!-- Begin page -->
        <div id="layout-wrapper">    