<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
    
    <!-- Datatable init js -->
    <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
    <script src="<?php echo base_url("/assets/js/jquery.validate.js");?>"></script>
    <script src="<?php echo base_url("/assets/js/messages_es.js");?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
    <!-- form mask -->
    <script src="<?php echo base_url("assets/libs/inputmask/min/jquery.inputmask.bundle.min.js");?>"></script>

    <!-- form mask init -->
    <script src="<?php echo base_url("assets/js/pages/form-mask.init.js");?>"></script>

<script>
            $(document).ready(function () {
                
                $('#POBL').blur(function () {  this.value = zeroPad(this.value, 3); });
                $('#CTEL').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#PRED').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#UNID').blur(function () { this.value = zeroPad(this.value, 3);  });


                var cve = $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();

                var pobl = $('#POBL').val();
                consulta_folio();
                

                $('#btn_actualizar_propietario').hide();
                   

            });

            function zeroPad(num, places) {
                var zero = places - num.toString().length + 1;
                return Array(+(zero > 0 && zero)).join("0") + num;
            }


        function consulta_folio()
        {
            var folio = $('#folio').val();
            var a = $('#year').val();
            if (folio == ""){
                    Swal.fire({
                        icon: 'error',
                        text: 'Ingrese un folio válido para continuar',
                        })
                 }
            else{
                    
                $.ajax({
                url: "<?php echo base_url("verificacion/consultafolio/");?>/"+a+"/"+folio+"",
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Folio...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (datasource) {
                       console.log(datasource.data[0]);
                       console.log(datasource.status[0]);
                       var data = datasource.data;
                       var status = datasource.status;

                       //console.log(datasource.status[0].status_captura);
                       validar();
                       
                   },
                   complete : function (){
                        $.unblockUI();
                    }
            });
        }
        
        

        function validar() {

            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',

            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
            });
             //limpiamos el consultado
           /* if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }*/
            //_geojson_vectorSource.clear();
             //obtener los valores de los input de la clave
             var pobl = $('#POBL').val();
             var ctel = $('#CTEL').val();
             var manz = $('#MANZ').val();
             var pred = $('#PRED').val();
             var unid = $('#UNID').val();

             var cvecat = pobl + ctel + manz + pred + unid;

             console.log("validando clave: " + cvecat)
             $.ajax({
                url: "<?php echo base_url("verificacion/consulta_avaluo");?>/"+cvecat,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Validando Clave Catastral...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                       console.log(data.result);
                        if(data.result.length > 0){

                            console.log(cvecat);
                            $('#valorcathidden').val(formatter.format(parseFloat(data.result[0].VAL_CONST) + parseFloat(data.result[0].VAL_TERR)));
                            var table_propietarios;
                            //se obtienen los propietarios
                            $("#tblpropietarios").DataTable().destroy();
                            var url_cons = "<?php echo base_url("verificacion/consulta_propietarios")?>/"+cvecat;
                            table_propietarios = $("#tblpropietarios").DataTable({
                                ajax: url_cons,
                                "language": {
                                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                                },
                                responsive: true,
                                "bPaginate": false,
                                "bFilter": false,
                                "bInfo": false,
                                "ordering": false,
                                //dom: 'Bfrtip',
                                //buttons:["excel","pdf", "print", "colvis"],
                                columns : [    
                                    {
                                        data: null,
                                        defaultContent: '<a class="activarp text-warning ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Propietario Actual"> <i class="mdi mdi mdi-star mdi-18px"></i></a> <a class="editarp text-primary ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Editar Propietario"> <i class="mdi mdi mdi-pencil-outline mdi-18px"></i></a>   <a class="eliminarp text-danger ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Eliminar Propietario"> <i class="mdi mdi mdi-delete-outline mdi-18px"></i></a>'
                                    },        
                                    { "data" : "NUM_PROP" },          
                                    { "data" : "CVE_PROP" },
                                    { "data" : "NOM_PROP" },
                                    { "data" : "APE_PAT" },
                                    { "data" : "APE_MAT" },
                                    { "data" : "TIP_PERS" },
                                    { "data" : "REG_FED" },
                                                                
                                ],
                                rowCallback: function(row, data, index) {

                                },
                            });

                            $('#tblpropietarios tbody').on( 'click', '.activarp', function () {
                                
                                if(table_propietarios.row(this).child.isShown()){
                                    var data = table_propietarios.row(this).data();
                                }else{
                                    var data = table_propietarios.row($(this).parents("tr")).data();
                                }

                                //obtener los valores de la tabla
                                var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
                                var TIP_PERS = data["TIP_PERS"];
                                var APE_PAT = data["APE_PAT"];
                                var APE_MAT = data["APE_MAT"];
                                var NOM_PROP = data["NOM_PROP"];
                                var REG_FED = data["REG_FED"];
                                var CVE_PROP = data["CVE_PROP"];

                                let NOMBRE_COMPLETO =  NOM_PROP + " " + APE_PAT + " " + APE_MAT;
                                Swal.fire({
                                title: '¿Establecer Propietario '+NOMBRE_COMPLETO+' como principal?',
                                type: 'warning',
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Establecer!'
                                }).then((result) => {
                                    if (result.value) {
                                        $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/establecer_propietario/"); ?>",
                                        data: {'clave': cvecat, 'CVE_PROP' : CVE_PROP, 'TIP_PERS' : TIP_PERS, 'APE_PAT' : APE_PAT, 'APE_MAT': APE_MAT, 'NOM_PROP' : NOM_PROP, 'REG_FED': REG_FED},
                                        success: function(data){
                                            console.log(data);
                                            if (data == 'ok'){
                                                //$('#tblpropietarios').DataTable().ajax.reload(null, false);
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Propietario establecido como Principal',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            else{ 
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al establecer el Propietario como el principal',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    
                                    }
                                })
                                    
                            });

                            $('#tblpropietarios tbody').on( 'click', '.editarp', function () {
                                if(table_propietarios.row(this).child.isShown()){
                                    var data = table_propietarios.row(this).data();
                                }else{
                                    var data = table_propietarios.row($(this).parents("tr")).data();
                                }

                                var num = data["NUM_PROP"];
                                var nombre = data["NOM_PROP"];
                                var apepat = data["APE_PAT"];
                                var apemat =  data["APE_MAT"];
                                var cve_prop = data["CVE_PROP"];
                                var rfc = data["REG_FED"];
                                var tipo = data["TIP_PERS"];
                                var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();

                                $('.modal-propietarios').modal("show");
                                $('#prop_numero').val(num);
                                $('#prop_clave').val(cve_prop);
                                $('#prop_cvecat').val(cvecat);
                                $('#prop_tip_persona').val(tipo);
                                $('#prop_nombre').val(nombre);
                                $('#prop_apellido_paterno').val(apepat);
                                $('#prop_apellido_materno').val(apemat);
                                $('#prop_rfc').val(rfc);
                                $('#btn_actualizar_propietario').show();
                                $('#btn_guardar_propietario').hide();

                            });

                            $('#tblpropietarios tbody').on( 'click', '.eliminarp', function () {
                                
                                if(table_propietarios.row(this).child.isShown()){
                                    var data = table_propietarios.row(this).data();
                                }else{
                                    var data = table_propietarios.row($(this).parents("tr")).data();
                                }
                                //alert( "Eliminar: " + data[0] );
                                var num = data["NUM_PROP"];
                                var nombre = data["NOM_PROP"] + " " + data["APE_PAT"] + " " + data["APE_MAT"] ;
                                var cve_prop = data["CVE_PROP"];
                                var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
                                
                                Swal.fire({
                                title: '¿Seguro de eliminar el Propietario # '+num+' Nombre: '+nombre+'?',
                                type: 'warning',
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Eliminar!'
                                }).then((result) => {
                                    if (result.value) {
                                    $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/eliminar_propietario/"); ?>",
                                        data: {'clave': cvecat, 'cve_prop' : cve_prop, 'numero' : num},
                                        success: function(data){
                                            console.log(data);
                                            if (data == 'ok'){
                                                $('#tblpropietarios').DataTable().ajax.reload(null, false);
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Propietario eliminado correctamente',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            else{ 
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al eliminar el Propietario',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    }
                                })
                            });
                            
                          
                            
                       }
                             
                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });
         }

        }

        $(document).on("click", "#btn_agregar_propietario", function(e) {
           e.preventDefault();
           $('.modal-propietarios').modal("show");
           var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
           $('#prop_cvecat').val(cvecat);

           $.getJSON("<?php echo base_url("padron/genera_clave_propietario");?>", function(json){
                   console.log(json);
                   $('#prop_clave').val(json.result[0].cve_prop);
            });  
           
        });


        $(document).on("click", "#btn_asignar", function(e) {
            e.preventDefault();
            var idprop = $('#prop_act').find("option:selected").val();
            var textprop = $('#prop_act').find("option:selected").text();
            var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
            //alert(idprop+ " " + cvecat);
            Swal.fire({
                                text: '¿Seguro de asignar a '+textprop+' como propietario de esta clave catastral?',
                                type: 'info',
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Asignar!'
                                }).then((result) => {
                                    if (result.value) {
                                    $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/asignar_propietario/"); ?>",
                                        data: {'idprop': idprop, 'cvecat' : cvecat},
                                        success: function(data){
                                            console.log(data);
                                            if (data == 'ok'){
                                                
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Propietario asignado correctamente',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                                $('#tblpropietarios').DataTable().ajax.reload(null, false);
                                            }
                                            else{
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al asignar el propietario',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    }
                                })

        });

        $(document).on("click", "#btn_guardar_propietario", function(e) {
            e.preventDefault();
            var form =  $("#form_propietarios"); 
            if(form.valid()){
                //console.log(form.serialize());
                $.ajax({
                url: "<?php echo base_url("captura/registro_propietario"); ?>",
                type: "POST",
                data: form.serialize(),
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    },
                    success: function (data) {
                        console.log(data);
                        if(data == 'ok'){
                            //reset el form
                            document.getElementById('form_propietarios').reset();
                            //actualizar tabla y cerrar modal
                            
                            $('#tblpropietarios').DataTable().ajax.reload(null, false);
                            $(".modal-propietarios").modal("hide");
                           
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Propietario Registrado Correctamente',
                                type:"success",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                        else{
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Ocurrio un error al registrar el Propietario',
                                type:"error",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                    
                    }
                }); 
            }
        });    
        
 
        $(document).on("click", "#btn_finalizar_captura", function(e) {
            e.preventDefault();
            var anio = $('#year').val();
            var folio = $('#folio').val();
            var cve = $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
            var tramite = "C.D.I";
            //actualizamos en la base de datos
            var capturista = "<?php echo $usuario->username; ?>";
            var valor_c = $('#valorcathidden').val();
            
            Swal.fire({
                                title: '¿Seguro de Finalizar la captura?',
                                type: 'info',
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Finalizar!'
                                }).then((result) => {
                                    if (result.value) {
                                    $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/finalizar_captura_cdi/"); ?>",
                                        data: {'anio': anio, 'fol' : folio, 'capturista' : capturista, 'cve_cat' : cve, 'tramite' : tramite, 'valor_a' : valor_c, 'valor_c':valor_c},
                                        success: function(data){
                                            console.log(data);
                                            if (data == 'ok'){
                                                console.log(data);
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Captura finalizada correctamente',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                                var url = "<?php echo base_url("administrativo/avaluos"); ?>";
                                                window.open(url, "_self");

                                            }
                                            else{
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al finalizar la captura',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    }
                                })
        });

        function listar_propietarios(text, control, id){
            $.getJSON("<?php echo base_url("padron/propietarios/");?>/"+text+"/"+id, function(json){
                    $('#'+control).empty();
                    $.each(json.result, function(i, obj){
                            var pat = "";
                            var mat = "";
                            if(obj.APE_PAT == null)
                                pat = "";
                            else
                                pat = obj.APE_PAT;
                            
                            if(obj.APE_MAT == null)
                                mat = "";
                            else
                                mat = obj.APE_MAT;

                            $('#'+control).append($('<option>').text(pat+" "+mat+ " "+obj.NOM_PROP).attr({ 'value' : obj.CVE_PROP}));
                    });
                    $('#'+control).select2();
            });
       }
        
       $(document).on("click", "#btn_agregar_propietario", function(e) {
           e.preventDefault();
           $('.modal-propietarios').modal("show");
           var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
           $('#prop_cvecat').val(cvecat);

           $.getJSON("<?php echo base_url("padron/genera_clave_propietario");?>", function(json){
                   console.log(json);
                   $('#prop_clave').val(json.result[0].cve_prop);
            });  
           
        });

        $(document).on('click','#btn_actualizar_propietario',function(e) {
            e.preventDefault();
            var form = $('#form_propietarios');
            if(form.valid()){
                //console.log(form.serialize());
                $.ajax({
                url: "<?php echo base_url("captura/actualizar_propietario"); ?>",
                type: "POST",
                data: form.serialize(),
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    },
                    success: function (data) {
                        console.log(data);
                        if(data == 'ok'){
                            //reset el form
                            document.getElementById('form_propietarios').reset();
                            //actualizar tabla y cerrar modal
                            
                            $('#tblpropietarios').DataTable().ajax.reload(null, false);
                            $(".modal-propietarios").modal("hide");
                            $('#btn_actualizar_propietario').hide();
                            $('#btn_guardar_propietario').show();
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Propietario Actualizado Correctamente',
                                type:"success",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                        else{
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Ocurrio un error al registrar el Propietario',
                                type:"error",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                    
                    }
                }); 
            }
        });

        $(document).on('click','#btn_buscar_prop_act',function(e) {
             e.preventDefault();
            var txt_prop = $('#bprop_act').val();
            listar_propietarios(txt_prop, 'prop_act', 0);
            $('#bprop_act').val("");
        });    

       </script>