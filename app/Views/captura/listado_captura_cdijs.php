<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    <!-- Datatable init js -->
    <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
    <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>



<script>
//funcion para formatear los campos de la clave catastral
function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        }

$( document ).ready(function() {

    
    var table = $('#tbltramites').DataTable({
        "language": {
            "url": "<?php echo base_url("assets/Spanish.json")?>"
        },
        "bProcessing": true,
        "ordering": false,
        "sAjaxSource": "<?php echo base_url("captura/listar_barra_captura_cdi")?>",
        "bPaginate":true,
        "sPaginationType":"full_numbers",
        "iDisplayLength": 25,
        "aoColumns": [
            {
                data: null,
                defaultContent: '<center></center>'
            },
            { mData: 'id_anio' } ,
            { mData: 'id_folio' },
            { mData: 'clave' },
            { mData: 'fecha_envio' },
            { mData: 'estatus_verificacion' },
            { mData: 'verificador' },
            { mData: 'fecha_captura' },
            { mData: 'capturista' },
            { mData: 'obs' },
        ],
        rowCallback: function(row, data, index) {
            var options = { year: 'numeric', month: 'long', day: 'numeric' };
            $("td", row).addClass('align-middle');

            $("td:eq(1)", row).addClass('text-primary ');
            $("td:eq(2)", row).addClass('text-primary ');
            $("td:eq(4)", row).addClass('font-medium ');

            $("td:eq(3)", row).text(formatFriendCode(data['clave'], 3, '-').slice(0, -1));

            if(data['fecha_envio'] != null)
                $("td:eq(4)", row).text(moment(data['fecha_envio']).format('DD/MM/YYYY'));
            
 
            if(data['estatus_verificacion'] == "V" ){
                $("td:eq(5)", row).html('<span class="badge badge-primary font-size-12">Verificado</span>');
                $("td:eq(0)", row).html('<center><a class="verificar text-dark ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Verificar Trámite"> <i class="mdi mdi-file-document-box-check-outline mdi-18px"></i></a> <a class="capturar text-primary ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Capturar Trámite"> <i class="mdi mdi-database-edit mdi-18px"></i></a></center>');
            }

            if(data['estatus_verificacion'] == "NP" ){
                $("td:eq(5)", row).html('<span class="badge badge-danger font-size-12">C.D.I NO PROCEDIO</span>');
                $("td:eq(0)", row).html('<center><a class="verificar text-dark ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Verificar Trámite"> <i class="mdi mdi-file-document-box-check-outline mdi-18px"></i></a> <i class="mdi mdi-database-lock mdi-18px text-danger ml-2"></i></center>');
            }

            if(data['fecha_captura'] != null){
                $("td:eq(7)", row).text(moment(data['fecha_captura_geom']).format('DD/MM/YYYY'));
                $("td:eq(5)", row).html('<span class="badge badge-success font-size-12">CAPTURADO</span>');
                $("td:eq(0)", row).html('<center><i class="mdi mdi mdi-database-check mdi-18px text-success"></i><i class="mdi mdi mdi-database-check mdi-18px text-success ml-2"></i></center>');
            }

            
        }
    });

    function formatFriendCode(friendCode, numDigits, delimiter) {
        return Array.from(friendCode).reduce((accum, cur, idx) => {
            return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
        }, '')
    }

    //click al boton consultar
    $('#tbltramites tbody').on( 'click', '.verificar', function () {
                if($('#tbltramites').DataTable().row(this).child.isShown()){
                    var data = $('#tbltramites').DataTable().row(this).data();
                }else{
                    var data = $('#tbltramites').DataTable().row($(this).parents("tr")).data();
                }
                var a = data["id_anio"];
                var fol = data["id_folio"].slice(3);;

                var url = "<?php echo base_url("verificacion/verificacioncdi"); ?>/"+a+"/"+fol;
                window.open(url, '_blank');
                
    });

     //click al boton capturar
     $('#tbltramites tbody').on( 'click', '.capturar', function () {
                if($('#tbltramites').DataTable().row(this).child.isShown()){
                    var data = $('#tbltramites').DataTable().row(this).data();
                }else{
                    var data = $('#tbltramites').DataTable().row($(this).parents("tr")).data();
                }
                var a = data["id_anio"];
                var fol = data["id_folio"].slice(3);
                var cve = data["clave"];

                var url = "<?php echo base_url("captura/capturarcdi"); ?>/"+a+"/"+fol+"/"+cve;
                window.open(url, '_blank');
                
    });
    

    
});


</script>