<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">C.D.I ENVIADOS PARA CAPTURA</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">CAPTURA Y VALUACIÓN</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                          <!-- start col 6 -->
                        
                         <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">  
                                        <div class="float-right">
                                        <i class="mdi mdi mdi-database-lock text-danger "></i> <small>Captura Bloqueada |</small> 
                                            <i class="mdi mdi mdi-database-check text-success "></i> <i class="mdi mdi mdi-database-check text-success"></i> <small>Capturado |</small> 

                                            <i class="mdi mdi-file-document-box-check-outline text-dark "></i> <small>Verificar Trámite |</small>

                                            <i class="mdi mdi-database-edit text-primary"></i> <small>Capturar Alfanumerico</small>
                                        </div>
                                        <h4 class="card-title">Consultar C.D.I. enviados a Captura</h4>
                                        <hr>
                                        <div class="table-responsive">   
                                        <table class="table table-hover nowrap" id="tbltramites" width="100%">
                                            <thead class="text-white" style="background-color: #480912;">
                                            <tr>
                                                <th>Acciones</th>
                                                <th>Año</th>
                                                <th>Folio</th>
                                                <th>Clave</th>
                                                <th>Fecha Envio</th>
                                                <th>Estatus</th>
                                                <th>Verificador</th>
                                                <th>F. Captura</th>
                                                <th>Capturista</th>
                                                <th>Observaciones </th>
                                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                     </div>
                     <!-- end row -->
                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            