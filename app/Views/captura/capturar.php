<style>
.card-box {
    padding: 20px;
    border-radius: 3px;
    margin-bottom: 30px;
    background-color: #fff;
}

.file-man-box {
    padding: 20px;
    border: 1px solid #e3eaef;
    border-radius: 5px;
    position: relative;
    margin-bottom: 20px
}

.file-man-box .file-close {
    color: #98a6ad;
    position: absolute;
    line-height: 24px;
    font-size: 24px;
    right: 10px;
    top: 10px;
    visibility: hidden
}

.file-man-box .file-img-box {
    line-height: 120px;
    text-align: center
}

.file-man-box .file-img-box img {
    height: 64px
}

.file-man-box .file-download {
    font-size: 24px;
    color: #98a6ad;
    position: absolute;
    right: 10px
}

.file-man-box .file-download:hover {
    color: #313a46
}

.file-man-box .file-man-title {
    padding-right: 25px
}

.file-man-box:hover {
    -webkit-box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02);
    box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02)
}

.file-man-box:hover .file-close {
    visibility: visible
}
.text-overflow {
    text-overflow: ellipsis;
    white-space: nowrap;
    font-size:12px;
    display: block;
    width: 100%;
    overflow: hidden;
}
</style>
<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">CAPTURA DEL TRÁMITE </h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">CAPTURA Y VALUACIÓN</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    
                     <!-- start row -->
                     <div class="row">
                     
                     <div class="col-lg-12">
                   
                     
                        <div class="btn-toolbar card d-print-none" role="toolbar">
                            <div class="card-body">
                                <div class="row">

                                <div class="col-lg-3">
                                    <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Año y Folio:</label>
                                                    <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="00" id="year" name="year" required maxlength="2" required value="<?php echo $year; ?>">
                                                                <input type="text" class="form-control" name="folio" placeholder="00000" id="folio" required maxlength="5" required value="<?php echo $folio; ?>">
                                                    </div>
                                    </div>
                                </div>
 
                                <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral Madre:</label>
                                                    
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="POBL" id="POBL" name="POBL" required maxlength="3" readonly value="<?php echo substr($cve, 0, 3); ?>">
                                                                <input type="text" class="form-control" name="CTEL" placeholder="CTEL" id="CTEL" readonly maxlength="3" value="<?php echo substr($cve, 3, 3); ?>">
                                                                <input type="text" class="form-control" placeholder="MANZ" id="MANZ" name="MANZ" required maxlength="3" readonly value="<?php echo substr($cve, 6, 3); ?>">
                                                                <input type="text" class="form-control" name="PRED" placeholder="PRED" id="PRED" readonly maxlength="3" required value="<?php echo substr($cve, 9, 3); ?>">
                                                                <input type="text" class="form-control" placeholder="UNID" id="UNID" name="UNID" maxlength="3" readonly value="<?php echo substr($cve, 12, 3); ?>">
                                                                
                                                            </div>
                                                   
                                                </div>
                                </div>
                                
                                <?php 
                                    //Validamos si esta en el grupo de administracion o valores
                                    if( in_groups(['Administracion', 'Valores'], user_id()) ) : ?>
                                        <div class="col-lg-3">
                                                    <br>
                                                    <a href="javascript:;" class="btn btn-success waves-effect waves-light btn-block mt-2" id="btn_finalizar_captura"><i class="mdi mdi-database-check"></i> Finalizar Captura</a>
                                                    
                                        </div>
                                    <?php endif; ?>

                                   
                                    <div id="div_nueva_clave_cat" class="row pl-3 pr-3">
                                    <div class="col-md-3" id="div_cancelar_clave">
                                            <br>
                                                    <a href="javascript:;" class="btn btn-danger waves-effect waves-light btn-block mt-2" id="btn_cancelar_clave"><i class="mdi mdi-database-minus"></i> Cancelar Clave Catastral</a>   
                                    </div>
                                      <div class="col-md-6">
                                        <div class="form-group position-relative">
                                                        <label for="validationTooltipUsername">Clave Catastral:</label>
                                                        
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" placeholder="POBL" id="RPOBL" name="RPOBL" required maxlength="3" list="list_poblaciones">
                                                                    <datalist id="list_poblaciones">
                                                                    </datalist>
                                                                    <input type="text" class="form-control" name="RCTEL" placeholder="CTEL" id="RCTEL" maxlength="3" list="list_cuarteles">
                                                                    <datalist id="list_cuarteles">
                                                                    </datalist>
                                                                    <input type="text" class="form-control" placeholder="MANZ" id="RMANZ" name="RMANZ" required maxlength="3" list="list_manzanas">
                                                                    <datalist id="list_manzanas"></datalist>
                                                                    <input type="text" class="form-control" name="RPRED" placeholder="PRED" id="RPRED" maxlength="3" required list="list_predios">
                                                                    <datalist id="list_predios"></datalist>
                                                                    <input type="text" class="form-control" placeholder="UNID" id="RUNID" name="RUNID" maxlength="3" required list="list_unidades">
                                                                    <datalist id="list_unidades"></datalist>
                                                                    
                                                                </div>
                                                    
                                                    </div>
                                        </div>

                                        <div class="col-lg-3" id="div_generar_clave">
                                                    <br>
                                                    <a href="javascript:;" class="btn btn-primary waves-effect waves-light btn-block mt-2" id="btn_generar_clave_catastral"><i class="mdi mdi-database-plus"></i> Generar Clave Catastral</a>          
                                        </div>
                                       
                                    </div><!-- div nueva clave cat -->

                                    <div class="col-lg-12 text-center">
                                        <h6>Tramite: <span id="tramite_desc"></span></h6>
                                        <small id="observaciones_tramite"> </small>
                                    </div>
                                </div>            
                            </div>
                        </div>
                        <!--end toolbar -->             
                    </div>                

                     <!-- begin tabs -->
                     <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-pills nav-fill pt-3" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#generales" role="tab">
                                                <span class="d-block d-sm-none"><i class="mdi mdi-information-outline"></i></span>
                                                <span class="d-none d-sm-block "><i class="mdi mdi-information-outline"></i> Datos Generales</span>
                                            </a>
                                        </li>
                                       
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#terreno" role="tab">
                                                <span class="d-block d-sm-none"><i class="mdi mdi-vector-rectangle"></i></span>
                                                <span class="d-none d-sm-block "><i class="mdi mdi-vector-rectangle"></i> Terreno</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#construcciones" role="tab">
                                                <span class="d-block d-sm-none"><i class="mdi mdi mdi-home-group"></i></span>
                                                <span class="d-none d-sm-block "><i class="mdi mdi mdi-home-group"></i> Construcciónes</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#servicios" role="tab">
                                                <span class="d-block d-sm-none"><i class="mdi mdi-cogs"></i></span>
                                                <span class="d-none d-sm-block "><i class="mdi mdi-cogs"></i> Servicios</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#usos" role="tab">
                                                <span class="d-block d-sm-none"><i class="mdi mdi-warehouse"></i></span>
                                                <span class="d-none d-sm-block "><i class="mdi mdi-warehouse"></i> Usos</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#propietarios" role="tab">
                                                <span class="d-block d-sm-none"><i class="mdi mdi-human-male-female"></i></span>
                                                <span class="d-none d-sm-block "><i class="mdi mdi-human-male-female"></i> Propietarios</span>
                                            </a>
                                        </li>  

                                         <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#vectorial" role="tab">
                                                <span class="d-block d-sm-none"><i class="mdi mdi-layers-outline"></i></span>
                                                <span class="d-none d-sm-block "><i class="mdi mdi-layers-outline"></i> Vectorial</span>
                                            </a>
                                        </li>              
                                       
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content p-3 text-muted">
                                    
                                        <div class="tab-pane active" id="generales" role="tabpanel">
                                           <!-- form de generales -->
                                           <div class="row border p-3">
                                           <div class="col-lg-12">
                                                <div class="form-group row">
                                                        <label for="example-text-input" class="col-md-2 col-form-label" style="color: #480912;">PROPIETARIO:</label>
                                                        <div class="col-md-10">
                                                            

                                                            <input class="form-control" type="text" id="propietario">
                                                            
                                                           


                                                        </div>
                                                        <div class="col-md-5">
                                                        </div>
                                                        <label for="example-text-input" style="text-align:right" class="col-md-4 col-form-label mt-2" id="impuestotrans_label">Impuesto Art. Transitorio:</label>
                                                        <div class="col-md-3">
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control float-right mt-2" type="text" id="impuestotrans">
                                                            <span class="input-group-btn  input-group-append mt-2"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_impuesto"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                </div>
                                                
                                                <hr>
                                                <div class="form-group row">
                                                        <label for="example-text-input" class="col-md-2 col-form-label " style="color: #480912;">NOTIFICACIÓN:</label>
                                                        <div class="col-md-10">
                                                            <label id="domicilio_label">Domicilio:</label> 
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="domicilio">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_domiclio"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-6 mt-2">
                                                            <label id="colonia_label">Colonia: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="colonia">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_colonia"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 mt-2">
                                                            <label id="cp_label">Código Postal: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="cp">
                                                            <span class="input-group-btn input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_cp"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                    <hr>
                                                <div class="form-group row">
                                                        <label for="example-text-input" class="col-md-2 col-form-label" style="color: #480912;">GEOMETRÍA:</label>
                                                        
                                                        <div class="col-md-3">
                                                            <label id="frentes_label">No. Frentes: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="frentes">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_frentes"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label id="mtsfrente_label">Mts. Frente: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="mtsfrente">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_mtsfrente"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label id="mtsfondo_label">Mts. Fondo: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="mtsfondo">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_mtsfondo"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                </div>
                                                <hr>

                                                <div class="form-group row">
                                                        <label for="example-text-input" class="col-md-2 col-form-label " style="color: #480912;">EFECTOS:</label>
                                                        
                                                        <div class="col-md-3">
                                                            <label id="tipo_efec_label">Tipo Efecto: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="tipo_efec">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_tipo_efec"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label id="tri_efec_label">No. Trimestre: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="tri_efec">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_tri_efec"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label id="anio_efec_label">Año: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="anio_efec">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_anio_efec"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                    
                                                </div>
                                                <hr>
    
                                                <div class="form-group row">
                                                        <label for="example-text-input" class="col-md-2 col-form-label" style="color: #480912;">UBICACIÓN:</label>
                                                        
                                                        <div class="col-md-6">
                                                            <label id="calle_label">Calle: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" list="calles" id="calle">
                                                            <datalist id="calles">
                                                            </datalist>
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_calle"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                        
                                                            <label id="numerooficial_label">Numero Oficial: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="numerooficial">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_numerooficial"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            
                                                        </div>
                                                        <div class="col-md-10 mt-2">
                                                            <label id="domicilio_u_label">Domicilio: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="domicilio_u">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_domicilio_u"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            
                                                        </div>
                                                        <div class="col-md-10 mt-2">
                                                            <label id="colonia_u_label">Colonia: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" list="colonias" id="colonia_u">
                                                            <datalist id="colonias">
                                                            </datalist>
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_colonia_u"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>

                                                </div>
                                                <hr>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-2 col-form-label" style="color: #480912;">RPP:</label>
                                                        
                                                        <div class="col-md-2">
                                                            <label id="libro_label">Libro: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="libro">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_libro"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <label id="escritura_label">Escritura: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="escritura">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_escritura"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <label id="seccion_label">Sección: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="seccion">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_seccion"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <label id="inscripcion_label">Inscripción: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="inscripcion">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_inscripcion"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                        </div>
                                                </div>
                                                <hr>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-2 col-form-label" style="color: #480912;">OTROS DATOS:</label>
                                                    <div class="col-md-4">
                                                        <label id="regimen_label">Régimen: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="regimen" list="regimenes">
                                                            <datalist id="regimenes">
                                                            </datalist>
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_regimen"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <label id="restringido_label">Restringido: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="restringido" list="restringidos">
                                                            <datalist id="restringidos">
                                                                <option value="S">
                                                                <option value="N">
                                                            </datalist>
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_restringido"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <label id="fechalta_label">Fecha Alta: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control form-control-sm" type="text" id="fechalta">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up btn-sm" type="button" id="btn_update_fechalta"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <label id="fechmov_label">Fecha Mvto.: </label>
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control form-control-sm" type="text" id="fechmov">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up btn-sm" type="button" id="btn_update_fechmov"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                        </div>

                                                </div>

                                                <!--<div class="form-group row">
                                                    <label for="example-text-input" class="col-md-2 col-form-label " style="color: #480912;">Terreno:</label>
                                                    <div class="col-md-5">
                                                            Superficie Total del Terreno: 
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="supterr">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                            Valor del Terreno: 
                                                            
                                                            <input class="form-control" type="text" id="valorterr">
                                                            
                                                    </div>
                                                </div>

                                               

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-2 col-form-label" style="color: #480912;">Constucción:</label>
                                                    <div class="col-md-5">
                                                            Superficie Total de Construcción: 
                                                            <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <input class="form-control" type="text" id="supcons">
                                                            <span class="input-group-btn  input-group-append"><button class="btn btn-outline-success bootstrap-touchspin-up" type="button" id="btn_update_"><i class="mdi mdi-content-save-edit-outline"></i></button></span>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                            Valor Total de Construcción: <input class="form-control" type="text" id="valorconst">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-2 col-form-label" style="color: #480912;">Valor Catastral:</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" type="text" id="valorcat">
                                                    </div>
                                                    
                                                </div>-->
                                                <input class="form-control" type="hidden" id="valorcathidden">

                                            </div>

                                           </div>
                                        </div>
                                       
                                        <div class="tab-pane" id="terreno" role="tabpanel">
                                        <a href="#" class="btn btn-success waves-effect waves-light mt-2" id="btn_agregar_terreno"><i class="mdi mdi-plus-circle-outline"></i> Agregar Terreno</a>

                                        <a href="#" class="btn btn-primary waves-effect waves-light mt-2 " id="btn_actualizar_sup_tot_terr"><i class="mdi mdi-sync"></i> Actualizar Superficie Total y Valor Total del Terreno</a>

                                        <div class="table-responsive">
                                        <table class="table table-hover" id="tblterreno" width="100%">
                                                <thead style="color: #480912;">
                                                    <tr>
                                                        <th colspan="6" class="text-center border">GENERAL</th>
                                                        <th colspan="4" class="text-center border">ESQUINA</th>
                                                        <th colspan="2" class="text-center border">GENERAL</th>
                                                        <th class="text-center border">VALOR</th>
                                                        <th></th>
                                                    </tr>
                                                    <tr class="text-white text-center" style="background-color: #480912;">
                                                        <th>#</th>
                                                        <th>T/Ren</th>
                                                        <th>Superficie</th>
                                                        <th>Calle</th>
                                                        <th>Tramo</th>
                                                        <th>Valor</th>
                                                        <th>Superficie</th>
                                                        <th>Calle</th>
                                                        <th>Tramo</th>
                                                        <th>Valor</th>
                                                        <th>Superficie</th>
                                                        <th>%Dem</th>
                                                        <th>Valor</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                <tr style="color: #480912;">
                                                        
                                                        <th class="font-medium" colspan="2" style="text-align:right;">Superficie Total:</th>
                                                        <th colspan="2"></th>
                                                        
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>

                                                        <th class="font-medium" colspan="2" style="text-align:right;">Valor Total:</th>
                                                        <th class="font-medium" colspan="2" style="text-align:right;"></th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                           </table>
                                           </div><!--table responsive-->
                                        </div>
                                        <div class="tab-pane" id="construcciones" role="tabpanel">
                                        <a href="#" class="btn btn-success waves-effect waves-light mt-2" id="btn_agregar_construccion"><i class="mdi mdi-plus-circle-outline"></i> Agregar Construcción</a>

                                        <a href="#" class="btn btn-primary waves-effect waves-light mt-2 " id="btn_actualizar_sup_tot_const"><i class="mdi mdi-sync"></i> Actualizar Superficie Total y Valor Total de Construcción</a>

                                        <div class="table-responsive">
                                        <table class="table table-hover" id="tblconstrucciones" width="100%">
                                                <thead style="color: #480912;">
                                                    <tr>
                                                        <th colspan="4" class="text-center border">CATEGORIA</th>
                                                        <th colspan="8" class="text-center">OTROS DATOS</th>
                                                       
                                                    </tr>
                                                    <tr class="text-white" style="background-color: #480912;">
                                                        <th>#</th>
                                                        <th>Cve</th>
                                                        <th>Descripcion</th>
                                                        <th>Valor M2.</th>
                                                        <th>Superficie</th>
                                                        <th>EDO</th>
                                                        <th>Fecha</th>
                                                        <th>Edad</th>
                                                        <th>F. Dem</th>
                                                        <th>F.C.</th>
                                                        <th>Valor</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                <tr style="color: #480912;">
                                                        <th colspan="4" class="text-right">Superficie Total de Construcción:</th>
                                                        
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th colspan="3" class="text-right">Valor Total de Construccion:</th>
                                                       
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                           </table>
                                           </div><!-- table responsive -->
                                        </div>

                                        <div class="tab-pane" id="servicios" role="tabpanel">
                                            <a href="#" class="btn btn-success waves-effect waves-light mt-2" id="btn_agregar_servicios"><i class="mdi mdi-plus-circle-outline"></i> Agregar o Quitar Servicios</a>
                                            <ul class="list-group list-group-hover text-primary mt-2" id="servicios_list">
                                                
                                            </ul>
                                        </div>

                                        <div class="tab-pane" id="usos" role="tabpanel">
                                        <a href="#" class="btn btn-success waves-effect waves-light mt-2" id="btn_agregar_uso"><i class="mdi mdi-plus-circle-outline"></i> Agregar Uso</a>
                                            <ul class="list-group text-primary mt-2" id="usos_list">
                                                
                                            </ul>
                                        </div>

                                        <div class="tab-pane" id="propietarios" role="tabpanel">
                                        <div class="form-group position-relative">
                                                    <input class="form-control" type="hidden" id="valorcathidden">
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected mb-2">
                                                    <input type="text" class="form-control" placeholder="Buscar Propietarios" id="bprop_act" name="bprop_act"><span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_buscar_prop_act">Buscar</button></span><span class="input-group-btn input-group-append"><button class="btn btn-success bootstrap-touchspin-up" type="button" id="btn_agregar_propietario"><i class="mdi mdi-account-multiple-plus-outline"></i> Registrar Nuevo Propietario</button></span></div>
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected mb-2">
                                                    <select class="form-control select2" id="prop_act" name="prop_act"></select>
                                                    <span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_asignar">+ Asignar</button></span></div>
                                                </div>

                                       
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tblpropietarios" width="100%">
                                                    <thead class="text-white" style="background-color: #480912;"> 
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Cve</th>
                                                            <th>Nombre (Razón Social)</th>
                                                            <th>Apellido Paterno</th>
                                                            <th>Apellido Materno</th>
                                                            <th>Reg.Fed.Cont.</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    
                                            </table>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="vectorial" role="tabpanel">
                                            <hr>
                                            <a href="#" class="btn btn-primary waves-effect waves-light mt-2 align-center" id="btn_descargar_capa"><i class="mdi mdi-download"></i> Obtener Capa del Predio</a>
                                            <a href="#" class="btn btn-danger waves-effect waves-light mt-2 align-center" id="btn_borrar_archivos"><i class="mdi mdi-delete"></i> Borrar Archivos</a>

                                            <div class="row border p-3 mt-2" id="files_vectores">
                                                
                                            </div>

                                            </div>
                                           
                                           
        
                                         

                                       
                                        

                                    </div>

                                </div>
                            </div>
                        </div>

                        <!--<div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                            <h5>UBICACIÓN GEOGRÁFICA </h5>
                            <hr>
                                <div id="map" class="map" style="height: 500px;"></div>
                            </div>
                        </div>-->
                    </div>

                    </div>
                    <!-- end tabs -->              

                  
                         
                     </div>
                     <!-- end row -->
                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            <div class="modal fade modal-servicios" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-xl modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Agregar o Quitar Servicios</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                        </div>
                        <div class="modal-body">
                        <form action="" name="form_servicios" id="form_servicios" method="post" accept-charset="utf-8">
                            <div class="row">
                                <div class="col-lg-4">
                                    
                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">1 - AGUA:</label>  
                                        <input type="hidden" name="serv_cvecat" id="serv_cvecat" class="form-control">                                
                                        <input type="text" name="serv1" id="serv1" class="form-control " required list="list_serv1">
                                        <datalist id="list_serv1">
                                        </datalist>
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">2 - ALUMBRADO:</label>                                  
                                        <input type="text" name="serv2" id="serv2" class="form-control " required list="list_serv2">
                                        <datalist id="list_serv2">
                                        </datalist>
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">3 - BANQUETA:</label>                                  
                                        <input type="text" name="serv3" id="serv3" class="form-control " required list="list_serv3">
                                        <datalist id="list_serv3">
                                        </datalist>
                                    </div>

                                </div>  <!-- col 4-->

                                <div class="col-lg-4">
                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">4 - DRENAJE:</label>                                  
                                        <input type="text" name="serv4" id="serv4" class="form-control " required list="list_serv4">
                                        <datalist id="list_serv4">
                                        </datalist>
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">5 - DESAGUE:</label>                                  
                                        <input type="text" name="serv5" id="serv5" class="form-control " required list="list_serv5">
                                        <datalist id="list_serv5">
                                        </datalist>
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">6 - ENERGIA:</label>                                  
                                        <input type="text" name="serv6" id="serv6" class="form-control " required list="list_serv6">
                                        <datalist id="list_serv6">
                                        </datalist>
                                    </div>

                                </div><!-- col 4-->

                                <div class="col-lg-4">
                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">7 - GUARNICIÓN:</label>                                  
                                        <input type="text" name="serv7" id="serv7" class="form-control " required list="list_serv7">
                                        <datalist id="list_serv7">
                                        </datalist>
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">8 - PAVIMENTO:</label>                                  
                                        <input type="text" name="serv8" id="serv8" class="form-control " required list="list_serv8">
                                        <datalist id="list_serv8">
                                        </datalist>
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">9 - TOMA DE AGUA:</label>                                  
                                        <input type="text" name="serv9" id="serv9" class="form-control " required list="list_serv9">
                                        <datalist id="list_serv9">
                                        </datalist>
                                    </div>

                                </div><!-- col 4-->

                            </div><!-- row-->
                            </form>

                        </div>
                        <div class="modal-footer">

                                                        <button type="button" class="btn btn-primary" id="btn_actualizar_servicios">Actualizar</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar_servicios">Cancelar</button>
                        </div>
                    </div>
                   
                    
                </div>
            </div>
            <!-- end modal -->

            <div class="modal fade modal-propietarios" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Agregar Nuevo Propietario</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                        </div>
                        <div class="modal-body">
                        <form action="" name="form_propietarios" id="form_propietarios" method="post" accept-charset="utf-8">
                            <div class="row">
                           
                            
                                <div class="col-lg-12">
                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Número de Propietario:</label>                                  
                                        <input type="number" name="prop_numero" id="prop_numero" class="form-control " placeholder="0" required>
                                        
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Clave de Propietario:</label>                                  
                                        <input type="number" name="prop_clave" id="prop_clave" class="form-control " placeholder="0" required readonly>
                                        <input type="hidden" name="prop_cvecat" id="prop_cvecat" class="form-control">
                                                                               
                                    </div>

                                    <div class="form-group position-relative">
                                                            
                                        <label class="form-label">Tipo de Persona: </label>
                                                           
                                        <input type="text" class="form-control" name="prop_tip_persona" id="prop_tip_persona"  placeholder="(F / M)" required list="list_tip_per">
                                        <datalist id="list_tip_per">
                                            <option value="F - FISICA">
                                             <option value="M - MORAL">                            
                                        </datalist>
                                                              
                                     </div>
                                    

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Nombre (Razon Social):</label>
                                        
                                        <input type="text" name="prop_nombre" id="prop_nombre" class="form-control " required placeholder="NOMBRE(S)">
                                    
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Apellido Paterno:</label>
                                        
                                        <input type="text" name="prop_apellido_paterno" id="prop_apellido_paterno" class="form-control " required placeholder="APELLIDO PATERNO">
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Apellido Materno:</label>
                                        
                                        <input type="text" name="prop_apellido_materno" id="prop_apellido_materno" class="form-control " required placeholder="APELLIDO MATERNO">
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04"> Reg.Fed.Cont.:</label>
                                        
                                        <input type="text" name="prop_rfc" id="prop_rfc" class="form-control " required placeholder="RFC">
                                                                            
                                    </div>

                                   

                                </div>

                               
                            </div>
                            

                        </div>
                        <div class="modal-footer">
                                                        
                                                        <button type="button" class="btn btn-success" id="btn_guardar_propietario">Registrar</button>
                                                        <button type="button" class="btn btn-primary" id="btn_actualizar_propietario">Actualizar</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar_propietario">Cancelar</button>
                        </div>
                    </div>
                    </form>
                    
                </div>
            </div>
            <!-- end modal -->

            <div class="modal fade modal-usos" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Agregar Nuevo Uso</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                        </div>
                        <div class="modal-body">
                        <form action="" name="form_usos" id="form_usos" method="post" accept-charset="utf-8">
                            <div class="row">
                           
                            
                                <div class="col-lg-12">
                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Número de Uso:</label>
                                        
                                        <input type="number" name="num_uso" id="num_uso" class="form-control " placeholder="0" required>
                                        <input type="hidden" name="uso_cvecat" id="uso_cvecat" class="form-control">
                                        
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Grupo de Uso:</label>
                                        
                                        <input type="text" name="grupo_uso" id="grupo_uso" class="form-control " required list="grupo_usos_list" placeholder="B">
                                        <datalist id="grupo_usos_list">
                                        </datalist>
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Uso:</label>
                                        
                                        <input type="text" name="uso_uso" id="uso_uso" class="form-control " list="usos_datalist" required placeholder="0">

                                        <datalist id="usos_datalist">
                                        </datalist>
                                                                            
                                    </div>
                                </div>

                               
                            </div>
                            

                        </div>
                        <div class="modal-footer">
                                                        
                                                        <button type="button" class="btn btn-success" id="btn_guardar_uso">Registrar</button>
                                                        <button type="button" class="btn btn-primary" id="btn_actualizar_uso">Actualizar</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar_uso">Cancelar</button>
                        </div>
                    </div>
                    </form>
                    
                </div>
            </div>
            <!-- end modal -->



            <div class="modal fade modal-const" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                                <h5 class="modal-title mt-0" id="const_title">Agregar Nueva Construccion</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                
                        </div>
                        <div class="modal-body">
                        <form action="" name="form_const" id="form_const" method="post" accept-charset="utf-8">
                            <div class="row">
                           
                            
                                <div class="col-lg-6">
                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Número de Construcción:</label>
                                        
                                        <input type="number" name="num_const" id="num_const" class="form-control " placeholder="0" required>
                                        <input type="hidden" name="num_const_ori" id="num_const_ori" class="form-control " placeholder="0" required>
                                        <input type="hidden" name="const_cvecat" id="const_cvecat" class="form-control">
                                        
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Superficie de Construcción:</label>
                                        
                                        <input type="number" name="terr_sup_const" id="terr_sup_const" class="form-control " placeholder="0.0" required>
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Categoría de Construcción:</label>
                                        
                                        <input type="text" name="cat_const" id="cat_const" class="form-control " list="categorias_const" required placeholder="E1">
                                        <datalist id="categorias_const">
                                        </datalist>
                                                                            
                                    </div>
                                </div>

                                <div class="col-lg-6">

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Estado de la Construcción:</label>
                                        
                                        <input type="text" name="edo_const" id="edo_const" class="form-control " list="edos_const" required placeholder="0">
                                        <datalist id="edos_const">
                                        </datalist>
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Edad de la Construcción:</label>
                                        
                                        <input type="text" name="edad_const" id="edad_const" class="form-control " list="edades_const" required placeholder="0">
                                        <datalist id="edades_const">
                                        </datalist>
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Fecha de Construcción:</label>
                                        
                                        <input id="fecha_const" name="fecha_const" class="form-control input-mask" data-inputmask="'alias': 'datetime'" data-inputmask-inputformat="yyyy/mm/dd" im-insert="false" required>
                                       
                                                                            
                                    </div>
                                </div>
                            </div>
                            

                        </div>
                        <div class="modal-footer">
                                                        
                                                        <button type="button" class="btn btn-success" id="btn_guardar_const">Registrar</button>
                                                        <button type="button" class="btn btn-primary" id="btn_actualizar_const">Actualizar</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar_const">Cancelar</button>
                        </div>
                    </div>
                    </form>
                    
                </div>
            </div>
            <!-- end modal -->


            <!-- modal para registrar un nuevo terreno -->
<div class="modal fade modal-terrenos" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
<form name="form_terreno" id="form_terreno">
                                                <div class="modal-dialog modal-xl modal-dialog-centered">
                                                    <div class="modal-content">
                                                    
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0" id="terr_title">Agregar Nuevo Terreno</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                        
                                                            <div class="form-group row mb-4">
                                                            
                                                                <label for="nombre" class="col-form-label col-lg-3">Número de Terreno: </label>
                                                                <div class="col-lg-3">
                                                    
                                                                <input type="number" class="form-control" name="terr_numero" id="terr_numero" placeholder="1" required maxlength="2">
                                                                    <input type="hidden" class="form-control" name="terr_numero_ori" id="terr_numero_ori" placeholder="1" required maxlength="2">
                                                                    <input type="hidden" class="form-control" name="terr_poblacion" id="terr_poblacion" placeholder="1" maxlength="2">  
                                                                    <input type="hidden" class="form-control" name="terr_cvecat" id="terr_cvecat">      
                                                                    </div>  
                                                                    
                                                                    
                                                                    
                                                                
                                                            </div>

                                                            <div class="form-group row mb-4">
                                                            
                                                                <label for="nombre" class="col-form-label col-lg-3">Tipo de Terreno: </label>
                                                                <div class="col-lg-3">
                                                    
                                                                <input type="text" class="form-control" name="terr_tip_terr" id="terr_tip_terr"  placeholder="(N / E / F / I)" required list="list_tip_terr">
                                                                    <datalist id="list_tip_terr">
                                                                        <option value="N">
                                                                        <option value="E">
                                                                        <option value="F">
                                                                        <option value="I">
                                                                    </datalist>
                                                                </div>
                                                                
                                                            </div>

                                                        <div class="row">
                                                
                                                    <div class="col-lg-4" style="margin-top:10px;">
                                                    
                                                    <center><h4 class="card-title">GENERALES</h4></Center>
                                                        <div class="row">
                                                           
                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Superficie:</label>
                                                                    <input type="number" name="terr_sup_terr" id="terr_sup_terr" class="form-control " placeholder="0.0" required>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Calle:   </label>
                                                                    <input type="text" name="terr_calle" id="terr_calle" class="form-control" list="calles" placeholder="0" required>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Tramo:   </label>
                                                                    <input type="text" name="terr_tramo" id="terr_tramo" class="form-control" list="tramoslist" placeholder="0" required>
                                                           
                                                                    <datalist id="tramoslist">
                                                                    </datalist>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!--/col 6 -->
                                                    
                                                
                                                    <div class="col-lg-4" style="margin-top:10px;">
                                                        <center><h4 class="card-title">ESQUINA</h4></center>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip04">Superficie Esquina:</label>
                                                                        <input type="number" name="terr_sup_terr_e" id="terr_sup_terr_e" class="form-control " placeholder="0.0" required>
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip04">Calle Esquina:   </label>
                                                                        <input type="text" name="terr_calle_e" id="terr_calle_e" class="form-control" list="calles" placeholder="0" required>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip04">Tramo Esquina:   </label>
                                                                        <input type="text" name="terr_tramo_e" id="terr_tramo_e" class="form-control" list="tramoslist_e" placeholder="0" required>
                                                            
                                                                        <datalist id="tramoslist_e">
                                                                        </datalist>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                       
                                                    </div><!--/col 6 -->

                                                    <div class="col-lg-4" style="margin-top:10px;">
                                                        <center><h4 class="card-title">DEMERITOS</h4></center>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip04">Superficie Terreno D:</label>
                                                                        <input type="number" name="terr_sup_terr_d" id="terr_sup_terr_d" class="form-control " placeholder="0.0" required>
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip04"> Demerito Terreno:   </label>
                                                                        <input type="text" name="dem_terr" id="dem_terr" class="form-control" list="demeritos_terreno_list" placeholder="0" required>
                                                                        <datalist id="demeritos_terreno_list">
                                                                        </datalist>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip04">Demerito Terreno 2:   </label>
                                                                        <input type="text" name="dem_terr2" id="dem_terr2" class="form-control" list="demeritos_terreno_list" placeholder="0" required>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip04">Demerito Terreno 3:   </label>
                                                                        <input type="text" name="dem_terr3" id="dem_terr3" class="form-control" list="demeritos_terreno_list" placeholder="0" required>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip04">Factor Demerito Terreno:   </label>
                                                                        <input type="number" name="terr_fact_dem" id="terr_fact_dem" class="form-control"  placeholder="0.0" required>
                                                                    </div>
                                                                </div>

                                                            </div>   
                                                    </div><!--/col 6 -->
                                            
                                                    </div> <!-- row --> 
                                                        
                                                    
                                                        
                                                           
                                                    
                                                        <div class="modal-footer">
                                                        
                                                            <button type="button" class="btn btn-success" id="btn_guardar_terreno">Registrar</button>
                                                            <button type="button" class="btn btn-primary" id="btn_actualizar_terreno">Actualizar</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar_terreno">Cancelar</button>
                                                        </div>
                                                    
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                                </form>
                                            </div>
                                            <!-- /.modal -->


 