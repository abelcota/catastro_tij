<!-- Required datatable js -->
    <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
    
    <!-- Datatable init js -->
    <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
    <script src="<?php echo base_url("/assets/js/jquery.validate.js");?>"></script>
    <script src="<?php echo base_url("/assets/js/messages_es.js");?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
    <!-- form mask -->
    <script src="<?php echo base_url("assets/libs/inputmask/min/jquery.inputmask.bundle.min.js");?>"></script>

    <!-- form mask init -->
    <script src="<?php echo base_url("assets/js/pages/form-mask.init.js");?>"></script>

<script>
    var const_tramite = "";
            $(document).ready(function () {
                
                $('#POBL').blur(function () {  this.value = zeroPad(this.value, 3); });
                $('#CTEL').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#PRED').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#UNID').blur(function () { this.value = zeroPad(this.value, 3);  });


                var cve = $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();

                var pobl = $('#POBL').val();
                consulta_folio();
                validar(cve);
                cargar_colonias(pobl);
                cargar_calles(pobl);
                cargar_regimenes();

                get_vectores();

                $('#btn_actualizar_terreno').hide();
                $('#btn_actualizar_const').hide();
                $('#btn_actualizar_uso').hide();
                $('#btn_actualizar_propietario').hide();

                cargar_poblaciones();
                   

            });

            function zeroPad(num, places) {
                var zero = places - num.toString().length + 1;
                return Array(+(zero > 0 && zero)).join("0") + num;
            }


        function consulta_folio()
            {
            var folio = $('#folio').val();
            var a = $('#year').val();
            if (folio == ""){
                    Swal.fire({
                        icon: 'error',
                        text: 'Ingrese un folio válido para continuar',
                        })
                 }
                else{
                    
                $.ajax({
                url: "<?php echo base_url("verificacion/consultafolio/");?>/"+a+"/"+folio+"",
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Folio...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (datasource) {
                       console.log(datasource.data[0]);
                       console.log(datasource.status[0]);
                       var data = datasource.data;
                       var status = datasource.status;

                        //console.log(datasource.status[0].status_captura);
                        if(status.lenght > 0){
                            if(datasource.status[0].status_captura != 'GC'){
                            Swal.fire({
                                type: 'error',
                                confirmButtonText: 'Cerrar',
                                title: 'NO TIENE AUTORIZACION PARA CAPTURAR',                               
                            }).then((result) => {
                                
                                    window.close();
                               
                            })
          

                            }    
                        }
                       

                       if(data.length > 0){
                           
                            //CARGAR CALLES Y COLONIAS DEPENDIENDO LA POBLACION
                                
                                //fecha de captura
                                //$("#dtfecha").val(moment(data[0].FechaCapturaInconformidad).format('YYYY-MM-DD'));
                                //Calle y colonia
                                /*$('#nombres').val(data[0].NombrePropietario);

                                $('#idcolonia').val(data[0].NOM_COL);
                                //$('#idcolonia').val(data[0].IdColonia).trigger('change.select2')
                                $('#idcalle').val(data[0].NOM_CALLE);
                                //Numero interior y uso
                                $("#numeroof").val(data[0].NumeroOficial);
                                $("#uso").val(data[0].DES_USO);
                                //superficies y valor
                                $("#superficiecons").val(data[0].TotalSuperficieConstruccion);
                                $("#superficieterr").val(data[0].TotalSuperficieTerreno);
                                $("#valorcat").val(data[0].ValorCatastral);
                                //nombre del solicitante, domicilio y telefono
                                $("#nom_sol").val(data[0].NombreSolicitante);
                                $("#dom_sol").val(data[0].DomicilioSolicitante);
                                $("#tel_sol").val(data[0].TelefonoSolicitante);*/
                                //tramites y motivo
                                $('#tramite_desc').text(data[0].IdTramite + " - " + data[0].DescripcionTramite);
                                $('#observaciones_tramite').text(data[0].ObservacionesBarra);

                                $('#div_nueva_clave_cat').hide();

                                //si es primer registro mostramos la otra barra
                                if(data[0].IdTramite == '20.0'){
                                    $('#div_nueva_clave_cat').show();
                                    $('#div_generar_clave').show(); 
                                    $('#div_cancelar_clave').hide();
                                    const_tramite = '20.0';
                                }  
                                if(data[0].IdTramite == '6.0'){
                                    $('#div_nueva_clave_cat').show();
                                    $('#div_cancelar_clave').show();
                                    $('#div_generar_clave').hide(); 
                                    const_tramite = '6.0';
                                }
                                if(data[0].IdTramite == '4.0'){
                                    $('#div_nueva_clave_cat').show();
                                    $('#div_cancelar_clave').hide();
                                    $('#div_generar_clave').show(); 
                                    const_tramite = '4.0';
                                }
                                


                                
                                
                                
                       }
                       else {
                           Swal.fire({
                               icon: 'error',
                               title: 'Folio no encontrado',
                               text: 'El folio no se encuentra registrado!'
                               
                           })
                       }
                       
                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });
        }
        }  

        function actualiza_generales(campo, valor, label){
            var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
            //ejecutamos actualizar generales
            $.ajax(
                {
                    type: 'post',
                    url: "<?php echo base_url("captura/actualiza_generales/");?>",
                    data: { 
                        "campo": campo,
                        "valor": valor,
                        "clave": cvecat
                    },
                    success: function (response) {
                        if(response == 'ok'){
                            Swal.fire({
                               type: 'success',
                               title: label + ' Actualizado',                               
                           })
                        }
                        else{

                        }
                    },
                    error: function () {
                        alert("Error !!");
                    }
                }
                );

        }
         

        function validar() {

            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',

            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
            });
             //limpiamos el consultado
           /* if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }*/
            //_geojson_vectorSource.clear();
             //obtener los valores de los input de la clave
             var pobl = $('#POBL').val();
             var ctel = $('#CTEL').val();
             var manz = $('#MANZ').val();
             var pred = $('#PRED').val();
             var unid = $('#UNID').val();

             var cvecat = pobl + ctel + manz + pred + unid;

             console.log("validando clave: " + cvecat)
             $.ajax({
                url: "<?php echo base_url("verificacion/consulta_avaluo");?>/"+cvecat,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Validando Clave Catastral...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                       console.log(data.result);
                    if(data.result.length > 0){
                        if(const_tramite === '20.0'){
                            $('#div_nueva_clave_cat').hide();
                        }
                         
                        if(data.result[0].ID_REGTO == "B"){
                            Swal.fire({
                               icon: 'error',
                               title: 'Clave dada de Baja...',                               
                           })
                           //$("#btn_limpiar").click();
                        }
                        else if(data.result[0].DOM_NOT == ""){
                            Swal.fire({
                               icon: 'error',
                               title: 'Este trámite no puede seguir, requiere Domicilio de Notificación!!',                               
                           })
                           
                        }
                        else{
                            if(data.result[0].APE_PAT == null){
                                    //$('#div_nueva_clave_cat').hide();
                            }
                            var fealta = data.result[0].FEC_ALTA.replaceAll("/","-");
                            var femov = data.result[0].FEC_MOV.replaceAll("/","-");
                            var apep = "";
                            var apem = "";
                            var nomprop = "";
                            var calle = "";
                            var colonia = "";
                            var regimen = "";
                            if( data.result[0].APE_PAT != null )
                                apep = data.result[0].APE_PAT;
                            if( data.result[0].APE_MAT != null )
                                apem = data.result[0].APE_MAT;
                            if(data.result[0].NOM_PROP)
                                nomprop = data.result[0].NOM_PROP;

                            if(data.result[0].CVE_CALLE)
                                calle = data.result[0].CVE_CALLE + " - " +data.result[0].NOM_CALLE;
                            
                            if(data.result[0].CVE_COL)
                                colonia = data.result[0].CVE_COL + " - " + data.result[0].NOM_COL;
                            
                            if(data.result[0].CVE_REGIM)
                                regimen = data.result[0].CVE_REGIM + " - " + data.result[0].DES_REGIM;
                            //NOMBRE COMPLETO    
                            $('#propietario').val(apep+ " " + apem+ " " + nomprop);
                            $('#impuestotrans').val(data.result[0].IMPTO_ANIO);
                            $('#domicilio').val(data.result[0].DOM_NOT);
                            $('#colonia').val(data.result[0].NOM_COL);
                            $('#cp').val(data.result[0].COD_POST_N);
                            $('#frentes').val(data.result[0].NUM_FTES);
                            $('#mtsfrente').val(data.result[0].LON_FTE);
                            $('#mtsfondo').val(data.result[0].LON_FONDO);
                            $('#tipo_efec').val(data.result[0].TIP_EFEC);
                            $('#tri_efec').val(data.result[0].TRI_EFEC);
                            $('#anio_efec').val(data.result[0].ATO_EFEC);
                            $('#calle').val(calle);
                            $('#numerooficial').val(data.result[0].NUM_OFIC_U);
                            $('#domicilio_u').val(data.result[0].UBI_PRED);
                            $('#colonia_u').val(colonia);
                            $('#libro').val(data.result[0].NUM_LIB);
                            $('#escritura').val(data.result[0].NUM_ESC);
                            $('#seccion').val(data.result[0].NUM_SECC);
                            $('#inscripcion').val(data.result[0].NUM_INSC);
                            $('#regimen').val(regimen);
                            $('#restringido').val(data.result[0].STT_REST);
                            $('#fechalta').val(data.result[0].FEC_ALTA);//moment(fealta).format('DD/MM/YYYY'));
                            $('#fechmov').val(data.result[0].FEC_MOV);//moment(femov).format('DD/MM/YYYY'));
                            $('#supterr').val(new Intl.NumberFormat().format(data.result[0].SUP_TERR));
                            $('#valorterr').val(formatter.format(data.result[0].VAL_TERR));
                            $('#supcons').val(data.result[0].SUP_CONST);
                            $('#valorconst').val(formatter.format(data.result[0].VAL_CONST));
                            $('#valorcat').val(formatter.format(parseFloat(data.result[0].VAL_CONST) + parseFloat(data.result[0].VAL_TERR)));
                            $('#valorcathidden').val(formatter.format(parseFloat(data.result[0].VAL_CONST) + parseFloat(data.result[0].VAL_TERR)));

                            //llenar la tabla de terreno
                            var table_terreno;
                            $("#tblterreno").DataTable().destroy();
                            var url_terr = "<?php echo base_url("verificacion/consulta_terreno")?>/"+cvecat;
                            
                            table_terreno = $("#tblterreno").DataTable({
                                ajax: url_terr,
                                "language": {
                                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                                },
                                responsive: false,
                                "bPaginate": false,
                                "bFilter": false,
                                "bInfo": false,
                                "ordering": false,
                                //dom: 'Bfrtip',
                                //buttons:["excel","pdf", "print", "colvis"],
                                columns : [        
                                    { "data" : "NUM_TERR" },      
                                    { "data" : "TIP_TERR" },
                                    { "data" : "SUP_TERR" },
                                    { "data" : "CVE_CALLE" },
                                    { "data" : "CVE_TRAMO" },
                                    { "data" : "valorunitario1" },
                                    { "data" : "SUP_TERR_E" },
                                    { "data" : "CVE_CALLE_" },
                                    { "data" : "CVE_TRAMO_" },
                                    { "data" : "valorunitario2" },
                                    { "data" : "SUP_TERR_D" },
                                    { "data" : "FAC_DEM_TE" },
                                    { "data" : "valorneto" },
                                    {
                                        data: null,
                                        defaultContent: '<a class="editart text-primary ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Editar Terreno"> <i class="mdi mdi mdi-pencil-outline mdi-18px"></i></a>   <a class="eliminart text-danger ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Eliminar Terreno"> <i class="mdi mdi mdi-delete-outline mdi-18px"></i></a>'
                                    },
                                ],
                                rowCallback: function(row, data, index) {
                                    /*var valneto = 0.0;
                                  
                                    valneto = (data["SUP_TERR"] * data["valorunitario1"] * ((100 - data["demeritotramo1"]) / 100) ) + (data["SUP_TERR_E"] * 0.25 * data["valorunitario2"] * ((100 - data["demeritotramo2"]) / 100) ) - (data["SUP_TERR_D"]  * data["valorunitario1"] * data["FAC_DEM_TE"]);*/
                                    $("td", row).addClass('text-center'); 
                                   
                                    $("td:eq(5)", row).text(formatter.format(data["valorunitario1"])); 
                                    $("td:eq(9)", row).text(formatter.format(data["valorunitario2"])); 
                                    $("td:eq(12)", row).text(formatter.format(data["valorneto"])); 
                                    //$("td:eq(1)", row).addClass("font-weight-bold"); 
                                },
                                footerCallback: function ( row, data, start, end, display ) {
                                    var api = this.api(), data;
            
                                    // Remove the formatting to get integer data for summation
                                    var intVal = function ( i ) {
                                        return typeof i === 'string' ?
                                            i.replace(/[\$,]/g, '')*1 :
                                            typeof i === 'number' ?
                                                i : 0;
                                    };

                                    // Total over all pages
                                    total_sup = api
                                        .column( 2 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );

                                       // Total over all pages
                                       total_valor = api
                                        .column( 12 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                                        
                                        
                                     // Update footer
                                        $( api.column( 2 ).footer() ).html(
                                            '<label class="text-center"><b>'+total_sup+'</b></label><input type="hidden" value="'+total_sup+'" id="total_sup_terr">'
                                        );

                                        // Update footer
                                        $( api.column( 12 ).footer() ).html(
                                            '<label class="text-center"><b>'+formatter.format(total_valor)+'</b></label><input type="hidden" value="'+total_valor+'" id="total_val_terr">'
                                        );
                                }
                            });

                            $('#tblterreno tbody').on( 'click', '.editart', function () {
                                if(table_terreno.row(this).child.isShown()){
                                    var data = table_terreno.row(this).data();
                                }else{
                                    var data = table_terreno.row($(this).parents("tr")).data();
                                }
      
                                $('#terr_title').text("Editar datos del Terreno");  
                                cargar_demeritos_terreno();
                                $('.modal-terrenos').modal("show");
                                var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
                                $('#terr_cvecat').val(cvecat);
                                $('#terr_poblacion').val($('#POBL').val());

                                $('#terr_numero').val(data["NUM_TERR"]);
                                $('#terr_numero_ori').val(data["NUM_TERR"]);
                                $('#terr_tip_terr').val(data["TIP_TERR"]);
                                $('#terr_sup_terr').val(data["SUP_TERR"]);
                                $('#terr_calle').val(data["CVE_CALLE"]);
                                cargar_tramos($('#terr_poblacion').val(), $('#terr_calle').val());
                                $('#terr_tramo').val(data["CVE_TRAMO"]);
                                $('#terr_sup_terr_e').val(data["SUP_TERR_E"]);
                                $('#terr_calle_e').val(data["CVE_CALLE_"]);
                                cargar_tramos_e($('#terr_poblacion').val(), $('#terr_calle_e').val());
                                $('#terr_tramo_e').val(data["CVE_TRAMO_"]);
                                $('#terr_sup_terr_d').val(data["SUP_TERR_D"]);
                                $('#dem_terr').val(data["CVE_DEM_TE"]);
                                $('#dem_terr2').val(data["CVE_DEM_T2"]);
                                $('#dem_terr3').val(data["CVE_DEM_T3"]);
                                $('#terr_fact_dem').val(data["FAC_DEM_TE"]);
                                
                                //visualizar el boton de actualizar y esconder el de registrar
                                $('#btn_guardar_terreno').hide();
                                $('#btn_actualizar_terreno').show();
                            });

                            $('#tblterreno tbody').on( 'click', '.eliminart', function () {
                                
                                if(table_terreno.row(this).child.isShown()){
                                    var data = table_terreno.row(this).data();
                                }else{
                                    var data = table_terreno.row($(this).parents("tr")).data();
                                }
                                //alert( "Eliminar: " + data[0] );
                                var num = data["NUM_TERR"];
                                var tipo = data["TIP_TERR"];
                                var clavecat = data["clave"];
                                
                                Swal.fire({
                                title: '¿Seguro de eliminar el Terreno # '+num+'?',
                                type: 'warning',
                                //imageUrl:"<?php echo base_url("assets/images/logo_c.png") ?>",
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Eliminar!'
                                }).then((result) => {
                                    if (result.value) {
                                    $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/eliminar_terreno/"); ?>",
                                        data: {'clave': clavecat, 'tipo' : tipo, 'numero' : num},
                                        success: function(data){
                                            console.log(data);
                                            if (data == 'ok'){
                                                $('#tblterreno').DataTable().ajax.reload(null, false);
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Terreno eliminado correctamente',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            else{
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al eliminar el Terreno',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    }
                                })
                            });

                            var table_const;

                            $("#tblconstrucciones").DataTable().destroy();
                            var url_cons = "<?php echo base_url("verificacion/consulta_construcciones")?>/"+cvecat;
                            table_const = $("#tblconstrucciones").DataTable({
                                ajax: url_cons,
                                "language": {
                                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                                },
                                responsive: true,
                                "bPaginate": false,
                                "bFilter": false,
                                "bInfo": false,
                                "ordering": false,
                                //dom: 'Bfrtip',
                                //buttons:["excel","pdf", "print", "colvis"],
                                columns : [         
                                    { "data" : "NUM_CONST" },     
                                    { "data" : "CVE_CAT_CO" },
                                    { "data" : "DES_CAT_CO" },
                                    { "data" : "valorunitario" },
                                    { "data" : "SUP_CONST" },
                                    { "data" : "EDO_CO" },
                                    { "data" : "FEC_CONST" },
                                    { "data" : "EDD_CONST" },
                                    { "data" : "factordemerito" },
                                    { "data" : "factorcomercializacion" },
                                    { "data" : "valorneto" },
                                    {
                                        data: null,
                                        defaultContent: '<a class="editarc text-primary ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Editar Construccion"> <i class="mdi mdi mdi-pencil-outline mdi-18px"></i></a>   <a class="eliminarc text-danger ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Eliminar Construccion"> <i class="mdi mdi mdi-delete-outline mdi-18px"></i></a>'
                                    },
                                ],
                                rowCallback: function(row, data, index) {
                                    /*var valneto = 0.0;
                                    valneto = data["SUP_CONST"] * data["valorunitario"] *  data["factordemerito"] *  data["factorcomercializacion"];*/
                                    $("td:eq(3)", row).text(formatter.format(data["valorunitario"])); 
                                    //$("td:eq(1)", row).addClass("font-weight-bold"); 
                                    $("td:eq(10)", row).text(formatter.format(data["valorneto"]));  
                                },
                                footerCallback: function ( row, data, start, end, display ) {
                                    var api = this.api(), data;
            
                                    // Remove the formatting to get integer data for summation
                                    var intVal = function ( i ) {
                                        return typeof i === 'string' ?
                                            i.replace(/[\$,]/g, '')*1 :
                                            typeof i === 'number' ?
                                                i : 0;
                                    };

                                    // Total over all pages
                                    total_sup = api
                                        .column( 4 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );

                                       // Total over all pages
                                       total_valor = api
                                        .column( 10 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                                        
                                        // Update footer
                                        $( api.column( 4 ).footer() ).html(
                                            '<label class="text-right"> <b>'+total_sup+'</b></label><input type="hidden" value="'+total_sup+'" id="total_sup_const">'
                                        );

                                        // Update footer
                                        $( api.column( 10 ).footer() ).html(
                                            '<label class="text-center"><b>'+formatter.format(total_valor)+'</b></label><input type="hidden" value="'+total_valor+'" id="total_val_const">'
                                        );

                                        /*if(total_sup != 0){
                                            console.log("Total superficie: " + total_sup);
                                        }

                                        if(total_valor != 0){
                                            console.log("Total Valor: " + total_valor);
                                        }*/


                                }
                                
                            });

                            $('#tblconstrucciones tbody').on( 'click', '.editarc', function () {

                                if(table_const.row(this).child.isShown()){
                                    var data = table_const.row(this).data();
                                }else{
                                    var data = table_const.row($(this).parents("tr")).data();
                                }
                                //alert( "Eliminar: " + data[0] );
                                //var num = data["NUM_CONST"];
                                //var tipo = data["CVE_CAT_CO"];
                                //var clavecat = data["clave"];
                                $('.modal-const').modal("show");
                                $('#const_title').text("Editar datos de la Construcción");  
                                var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
                                       
                                cargar_categorias_construccion();
                                cargar_edo_construccion();
                                cargar_edad_construccion();

                                $('#num_const').val(data["NUM_CONST"]);
                                $('#num_const_ori').val(data["NUM_CONST"]);
                                $('#const_cvecat').val(cvecat); 
                                $('#terr_sup_const').val(data["SUP_CONST"]); 
                                $('#cat_const').val(data["CVE_CAT_CO"]); 
                                $('#edo_const').val(data["CVE_EDO_CO"]); 
                                $('#edad_const').val(data["EDD_CONST"]); 
                                $('#fecha_const').val(data["FEC_CONST"]); 

                                //visualizar el boton de actualizar y esconder el de registrar
                                $('#btn_guardar_const').hide();
                                $('#btn_actualizar_const').show();
                            });

                            
                            

                            $('#tblconstrucciones tbody').on( 'click', '.eliminarc', function () {
                                
                                if(table_const.row(this).child.isShown()){
                                    var data = table_const.row(this).data();
                                }else{
                                    var data = table_const.row($(this).parents("tr")).data();
                                }
                                //alert( "Eliminar: " + data[0] );
                                var num = data["NUM_CONST"];
                                var tipo = data["CVE_CAT_CO"];
                                var clavecat = data["clave"];
                                
                                Swal.fire({
                                title: '¿Seguro de eliminar la Construccion # '+num+' tipo '+tipo+'?',
                                type: 'warning',
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Eliminar!'
                                }).then((result) => {
                                    if (result.value) {
                                    $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/eliminar_construccion/"); ?>",
                                        data: {'clave': clavecat, 'tipo' : tipo, 'numero' : num},
                                        success: function(data){
                                            console.log(data);
                                            if (data == 'ok'){
                                                $('#tblconstrucciones').DataTable().ajax.reload(null, false);
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Contrucción eliminado correctamente',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            else{
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al eliminar la Contrucción',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    }
                                })
                            });

                            consulta_servicios(cvecat);

                            consulta_usos_predio(cvecat);
                           
                            var table_propietarios;
                            //se obtienen los propietarios
                            $("#tblpropietarios").DataTable().destroy();
                            var url_cons = "<?php echo base_url("verificacion/consulta_propietarios")?>/"+cvecat;
                            table_propietarios = $("#tblpropietarios").DataTable({
                                ajax: url_cons,
                                "language": {
                                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                                },
                                responsive: true,
                                "bPaginate": false,
                                "bFilter": false,
                                "bInfo": false,
                                "ordering": false,
                                //dom: 'Bfrtip',
                                //buttons:["excel","pdf", "print", "colvis"],
                                columns : [    
                                    { "data" : "NUM_PROP" },          
                                    { "data" : "CVE_PROP" },
                                    { "data" : "NOM_PROP" },
                                    { "data" : "APE_PAT" },
                                    { "data" : "APE_MAT" },
                                    { "data" : "REG_FED" },
                                    {
                                        data: null,
                                        defaultContent: '<a class="activarp text-warning ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Propietario Actual"> <i class="mdi mdi mdi-star mdi-18px"></i></a> <a class="editarp text-primary ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Editar Propietario"> <i class="mdi mdi mdi-pencil-outline mdi-18px"></i></a>   <a class="eliminarp text-danger ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Eliminar Propietario"> <i class="mdi mdi mdi-delete-outline mdi-18px"></i></a>'
                                    },                                    
                                ],
                                rowCallback: function(row, data, index) {

                                },
                            });

                            $('#tblpropietarios tbody').on( 'click', '.activarp', function () {
                                
                                if(table_propietarios.row(this).child.isShown()){
                                    var data = table_propietarios.row(this).data();
                                }else{
                                    var data = table_propietarios.row($(this).parents("tr")).data();
                                }

                                //obtener los valores de la tabla
                                var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
                                let TIP_PERS = data["TIP_PERS"];
                                let APE_PAT = data["APE_PAT"];
                                let APE_MAT = data["APE_MAT"];
                                let NOM_PROP = data["NOM_PROP"];
                                let REG_FED = data["REG_FED"];
                                let CVE_PROP = data["CVE_PROP"];

                                let NOMBRE_COMPLETO =  NOM_PROP + " " + APE_PAT + " " + APE_MAT;
                                Swal.fire({
                                title: '¿Establecer Propietario '+NOMBRE_COMPLETO+' como principal?',
                                type: 'warning',
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Establecer!'
                                }).then((result) => {
                                    if (result.value) {
                                        $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/establecer_propietario/"); ?>",
                                        data: {'clave': cvecat, 'CVE_PROP' : CVE_PROP, 'TIP_PERS' : TIP_PERS, 'APE_PAT' : APE_PAT, 'APE_MAT': APE_MAT, 'NOM_PROP' : NOM_PROP, 'REG_FED': REG_FED},
                                        success: function(data){
                                            console.log(data);
                                            if (data == '1'){
                                                //$('#tblpropietarios').DataTable().ajax.reload(null, false);
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Propietario establecido como Principal',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            else{ 
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al establecer el Propietario como el principal',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    
                                    }
                                })
                                    
                            });

                            $('#tblpropietarios tbody').on( 'click', '.eliminarp', function () {
                                
                                if(table_propietarios.row(this).child.isShown()){
                                    var data = table_propietarios.row(this).data();
                                }else{
                                    var data = table_propietarios.row($(this).parents("tr")).data();
                                }
                                //alert( "Eliminar: " + data[0] );
                                var num = data["NUM_PROP"];
                                var nombre = data["NOM_PROP"] + " " + data["APE_PAT"] + " " + data["APE_MAT"] ;
                                var cve_prop = data["CVE_PROP"];
                                var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
                                
                                Swal.fire({
                                title: '¿Seguro de eliminar el Propietario # '+num+' Nombre: '+nombre+'?',
                                type: 'warning',
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Eliminar!'
                                }).then((result) => {
                                    if (result.value) {
                                    $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/eliminar_propietario/"); ?>",
                                        data: {'clave': cvecat, 'cve_prop' : cve_prop, 'numero' : num},
                                        success: function(data){
                                            console.log(data);
                                            if (data == 'ok'){
                                                $('#tblpropietarios').DataTable().ajax.reload(null, false);
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Propietario eliminado correctamente',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            else{ 
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al eliminar el Propietario',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    }
                                })
                            });
                            
                            //Se obtiene el array del poligono 
                            //jsonobj = jQuery.parseJSON(data.result[0].jsonb_build_object);
                            //si encuentra el poligono en el vectorial
                            /*if (typeof _geojson_vectorSource != "undefined") {
                                _geojson_vectorSource.clear();
                            }
                            if (jsonobj.features != null) {
                                //Se visualiza el poligono en el mapa
                                
                                visualizar_capa(jsonobj);

                                map.updateSize(); 
                                //llenar datos del geometrico
                                var nombre = jsonobj.features[0].properties.f3 != null ? jsonobj.features[0].properties.f3 : "";
                               
                                var calle = jsonobj.features[0].properties.f4 != null ? jsonobj.features[0].properties.f4 : "";

                                var colonia = jsonobj.features[0].properties.f5 != null ? jsonobj.features[0].properties.f5 : "";

                                var numoficial = jsonobj.features[0].properties.f6 != null ? jsonobj.features[0].properties.f6 : "";

                                var supterr = jsonobj.features[0].properties.f7 != null ? jsonobj.features[0].properties.f7 : "";

                                var supcons = jsonobj.features[0].properties.f8 != null ? jsonobj.features[0].properties.f8 : "";

                                var valcat = jsonobj.features[0].properties.f9 != null ? jsonobj.features[0].properties.f9 : "";

                                
                                /*$('#nombres').val($('#nombres').val() + " / " + nombre);
                                
                                $('#idcalle').val($('#idcalle').val() + " / " + calle);

                                $('#idcolonia').val($('#idcolonia').val() + " / " + colonia);

                                $('#numeroof').val($('#numeroof').val() + " / " + numoficial);

                                $('#superficieterr').val($('#superficieterr').val() + " / " + supterr );

                                $('#superficiecons').val($('#superficiecons').val() + " / " + supcons );

                                $('#valorcat').val($('#valorcat').val() + " / " + valcat );
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Vectorial no encontrado',
                                    text: 'La clave catastral no se encontró en el vectorial!'
                                    
                                })
                            }*/
                        }

                        //$('#div_nueva_clave_cat').hide();
                            
                       }
                       else {

                           //verificar si se encuentra de primer registro
                           /*Swal.fire({
                            title: 'Clave Catastral no encontrada',
                            icon: 'error',
                            }).then((result) => {
                               
                            })
                            */

                            //mostraoms el cuadro para registrar nueva clave catastral
                            //$('#div_nueva_clave_cat').show();
                       }          
                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });
         }

         function consulta_servicios(){
                    var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
                    //se obtienen los servicios
                            var url = "<?php echo base_url("verificacion/consulta_servicios/");?>/"+cvecat;
                            var elements = "";
                            var campox = "";
                            $.getJSON(url, function(json){
                                    $('#servicios_list').empty();
                                    $.each(json.data, function(i, obj){
                                            /*var html = '<div class="form-check mb-2"><input class="form-check-input" type="checkbox" checked id="'+obj.IdRequisito+'" name="doc'+(i+1)+'"><label class="form-check-label" for="defaultCheck1">'+obj.DescripcionRequisito+'</label></div>';
                                            $('#requisitos_list').append(html);*/
                                            elements = obj.descripcionx;
                                            campox = obj.campox;
                                    });
                                    var res = elements.split(",");
                                    var vals = campox.split('');
                                    var i = 0;
                                    for (var serv in res) {

                                        var html = '  <li class="list-group-item d-flex justify-content-between align-items-left align-middle" style="color: #480912;"> <b>'+res[serv]+'</b> </li>';
                                        $('#servicios_list').append(html);
                                        i ++;
                                    }
                            });
         }

         function consulta_usos_predio(cvecat){
            var url = "<?php echo base_url("verificacion/consulta_usos");?>/"+cvecat;
                            //se obtienen los usos
                            $.getJSON(url, function(json){
                                    $('#usos_list').empty();
                                    $.each(json.data, function(i, obj){
                                        var html = '  <li class="list-group-item d-flex justify-content-between align-items-left align-middle" style="color: #480912;"><b>'+obj.NUM_USO + " - " +obj.cve_comp+' '+obj.DES_USO+' </b> <span class="pull-right"> <i class="deluso mdi mdi-delete-outline mdi-18px text-danger" style="cursor:pointer;" num_uso="'+obj.NUM_USO +'" cve_uso="'+obj.cve_comp+'"></i></span></li>';
                                        $('#usos_list').append(html);
                                    });
                                   
                            });
         }

         function cargar_colonias(pobl){
            $('#colonias').empty();
            $.getJSON("<?php echo base_url("padron/colonias");?>/"+pobl, function(json){
                    
                    $.each(json.result, function(i, obj){                            
                            $('#colonias').append($('<option>').val(obj.CVE_COL + " - " +obj.COLONIA));
                    });     
            });
            
         }

         function cargar_regimenes(){
            $('#regimenes').empty();
            $.getJSON("<?php echo base_url("padron/regimenes");?>", function(json){
                    
                    $.each(json.result, function(i, obj){                            
                            $('#regimenes').append($('<option>').attr({'value': obj.CVE_REGIM + " - " +obj.DES_REGIM}));
                    });     
            });
            
         }

         function cargar_calles(pobl){
            $('#calles').empty();
            $.getJSON("<?php echo base_url("padron/calles2");?>/"+pobl, function(json){
                    $.each(json.results, function(i, obj){
                            $('#calles').append($('<option>').val(obj.id + " - " +obj.text));
                           
                    });    
            });              
         }

         function cargar_tramos(pobl, calle){
            $('#tramoslist').empty();
            $.getJSON("<?php echo base_url("padron/tramos2");?>/"+pobl+"/"+calle, function(json){
                    $('#tramoslist').append($('<option>').val("0 - Sin Tramo"));
                    $.each(json.results, function(i, obj){
                            $('#tramoslist').append($('<option>').val(obj.id + " - " +obj.text));
                           
                    });    
            });       
         }

         function cargar_tramos_e(pobl, calle){
            $('#tramoslist_e').empty();
            $.getJSON("<?php echo base_url("padron/tramos2");?>/"+pobl+"/"+calle, function(json){
                    $('#tramoslist_e').append($('<option>').val("0 - Sin Tramo"));
                    $.each(json.results, function(i, obj){
                            $('#tramoslist_e').append($('<option>').val(obj.id + " - " +obj.text));
                           
                    });    
            });       
         }

         function cargar_demeritos_terreno(){
            $('#demeritos_terreno_list').empty();
            $.getJSON("<?php echo base_url("padron/demeritos_terreno");?>", function(json){
                    $('#demeritos_terreno_list').append($('<option>').val("0 - Sin Demerito"));
                    //console.log(json);
                    $.each(json.results, function(i, obj){
                            $('#demeritos_terreno_list').append($('<option>').val(obj.CVE_DEM_TE + " - " +obj.DES_DEM_TE + " - " + obj.FAC_DEM_TE));
                           
                    });    
            });                       
         }

         function cargar_categorias_construccion(){
            $('#categorias_const').empty();
            $.getJSON("<?php echo base_url("padron/categorias_construccion");?>", function(json){
                    //console.log(json);
                    $.each(json.results, function(i, obj){
                            $('#categorias_const').append($('<option>').val(obj.CVE_CAT_CO + " - " +obj.DES_CAT_CO));
                           
                    });    
            });                       
         }

        function cargar_edo_construccion(){
            $('#edos_const').empty();
            $.getJSON("<?php echo base_url("padron/edo_construccion");?>", function(json){
                    //console.log(json);
                    $.each(json.results, function(i, obj){
                            $('#edos_const').append($('<option>').val(obj.CVE_EDO_CO + " - " +obj.DES_EDO_CO));
                           
                    });    
            });                       
         }

         function cargar_edad_construccion(){
            $('#edades_const').empty();
            $.getJSON("<?php echo base_url("padron/edad_construccion");?>", function(json){
                    //console.log(json);
                    $.each(json.results, function(i, obj){
                            $('#edades_const').append($('<option>').val(obj.EDD_CONST));
                           
                    });    
            });                       
         }

         function cargar_grupos_usos(){
            $('#grupo_usos_list').empty();
            $.getJSON("<?php echo base_url("padron/grupo_usos");?>", function(json){
                    //console.log(json);
                    $.each(json.results, function(i, obj){
                            $('#grupo_usos_list').append($('<option>').val(obj.CVE_GPO_US + " - " + obj.DES_GPO_US));
                           
                    });    
            });                       
         }

         function cargar_servicios(){
            for (var i = 1; i <= 9; i++) {
                $('#list_serv'+i).empty();      
            }

            $.getJSON("<?php echo base_url("padron/consulta_servicios");?>/1", function(json){
                    $('#list_serv1').append($('<option>').val("0 - SIN SERVICIO"));
                    $.each(json.data, function(i, obj){
                            if( obj.CVE_SERV.charAt(1) != '0')
                                $('#list_serv1').append($('<option>').val(obj.CVE_SERV + " - " + obj.DES_SERV));
                           
                    });    
            }); 

            $.getJSON("<?php echo base_url("padron/consulta_servicios");?>/2", function(json){
                    $('#list_serv2').append($('<option>').val("0 - SIN SERVICIO"));
                    $.each(json.data, function(i, obj){
                        if( obj.CVE_SERV.charAt(1) != '0')
                            $('#list_serv2').append($('<option>').val(obj.CVE_SERV + " - " + obj.DES_SERV));
                           
                    });    
            });

            $.getJSON("<?php echo base_url("padron/consulta_servicios");?>/3", function(json){
                    $('#list_serv3').append($('<option>').val("0 - SIN SERVICIO"));
                    $.each(json.data, function(i, obj){
                        if( obj.CVE_SERV.charAt(1) != '0')
                            $('#list_serv3').append($('<option>').val(obj.CVE_SERV + " - " + obj.DES_SERV));
                           
                    });    
            });

            $.getJSON("<?php echo base_url("padron/consulta_servicios");?>/4", function(json){
                    $('#list_serv4').append($('<option>').val("0 - SIN SERVICIO"));
                    $.each(json.data, function(i, obj){
                        if( obj.CVE_SERV.charAt(1) != '0')
                            $('#list_serv4').append($('<option>').val(obj.CVE_SERV + " - " + obj.DES_SERV));
                           
                    });    
            });

            $.getJSON("<?php echo base_url("padron/consulta_servicios");?>/5", function(json){
                    $('#list_serv5').append($('<option>').val("0 - SIN SERVICIO"));
                    $.each(json.data, function(i, obj){
                        if( obj.CVE_SERV.charAt(1) != '0')
                            $('#list_serv5').append($('<option>').val(obj.CVE_SERV + " - " + obj.DES_SERV));
                           
                    });    
            });

            $.getJSON("<?php echo base_url("padron/consulta_servicios");?>/6", function(json){
                    $('#list_serv6').append($('<option>').val("0 - SIN SERVICIO"));
                    $.each(json.data, function(i, obj){
                        if( obj.CVE_SERV.charAt(1) != '0')
                            $('#list_serv6').append($('<option>').val(obj.CVE_SERV + " - " + obj.DES_SERV));
                           
                    });    
            });

            $.getJSON("<?php echo base_url("padron/consulta_servicios");?>/7", function(json){
                    $('#list_serv7').append($('<option>').val("0 - SIN SERVICIO"));
                    $.each(json.data, function(i, obj){
                        if( obj.CVE_SERV.charAt(1) != '0')
                            $('#list_serv7').append($('<option>').val(obj.CVE_SERV + " - " + obj.DES_SERV));
                           
                    });    
            });

            $.getJSON("<?php echo base_url("padron/consulta_servicios");?>/8", function(json){
                    $('#list_serv8').append($('<option>').val("0 - SIN SERVICIO"));
                    $.each(json.data, function(i, obj){
                        if( obj.CVE_SERV.charAt(1) != '0')
                            $('#list_serv8').append($('<option>').val(obj.CVE_SERV + " - " + obj.DES_SERV));
                           
                    });    
            });

            $.getJSON("<?php echo base_url("padron/consulta_servicios");?>/9", function(json){
                    $('#list_serv9').append($('<option>').val("0 - SIN SERVICIO"));
                    $.each(json.data, function(i, obj){
                        if( obj.CVE_SERV.charAt(1) != '0')
                            $('#list_serv9').append($('<option>').val(obj.CVE_SERV + " - " + obj.DES_SERV));
                           
                    });    
            });
   



         }

        function cargar_poblaciones(){
            $('#list_poblaciones').empty();
            $.getJSON("<?php echo base_url("padron/list_poblaciones");?>", function(json){
                    //console.log(json);
                    $.each(json.data, function(i, obj){
                            let pobl = zeroPad(obj.CVE_POBL.trim(), 3);
                            $('#list_poblaciones').append($('<option>').val(pobl + "  " + obj.NOM_POBL));     
                    });    
            });                       
         }

         function cargar_cuarteles(pobl){
            $('#list_cuarteles').empty();
            var url = "<?php echo base_url("padron/list_cuarteles");?>/"+pobl;
            //console.log(url);
            $.getJSON(url, function(json){
                    //console.log(json);
                    $.each(json.data, function(i, obj){
                            let ctel = zeroPad(obj.NUM_CTEL.trim(), 3);
                            $('#list_cuarteles').append($('<option>').val(ctel));
                           
                    });    
            });                       
         }

         function cargar_manzanas(pobl, ctel){
            $('#list_manzanas').empty();
            var url = "<?php echo base_url("padron/list_manzanas");?>/"+pobl+"/"+ctel;
            //console.log(url);
            $.getJSON(url, function(json){
                    //console.log(json);
                    $.each(json.data, function(i, obj){
                            let manz = zeroPad(obj.NUM_MANZ.trim(), 3);
                            $('#list_manzanas').append($('<option>').val(manz));      
                    });    
            });                       
         }

         function cargar_predios(pobl, ctel, manz){
            $('#list_predios').empty();
            var url = "<?php echo base_url("padron/list_predios");?>/"+pobl+"/"+ctel+"/"+manz;
            //console.log(url);
            $.getJSON(url, function(json){
                    //console.log(json);
                    $.each(json.data, function(i, obj){
                            let pred = zeroPad(obj.NUM_PRED.trim(), 3);
                            $('#list_predios').append($('<option>').val(pred));      
                    });    
            });                       
         }

         function cargar_unidades(pobl, ctel, manz, pred){
            $('#list_unidades').empty();
            var url = "<?php echo base_url("padron/list_unidades");?>/"+pobl+"/"+ctel+"/"+manz+"/"+pred;
            //console.log(url);
            $.getJSON(url, function(json){
                    //console.log(json);
                    $.each(json.data, function(i, obj){
                            let unid = zeroPad(obj.NUM_UNID.trim(), 3);
                            $('#list_unidades').append($('<option>').val(unid));      
                    });    
            });                       
         }


         function cargar_usos(cve_grupo){
            $('#usos_datalist').empty();
            $.getJSON("<?php echo base_url("padron/usos");?>/"+cve_grupo, function(json){
                    console.log(json);
                    $.each(json.results, function(i, obj){
                            $('#usos_datalist').append($('<option>').val(obj.CVE_USO.substring(1) + " - " + obj.DES_USO));
                           
                    });    
            });                       
         }

         function get_vectores(){
            var folio = $('#year').val() + $('#folio').val();
            $('#files_vectores').empty();
            var url = "<?php echo base_url("captura/cargar_vectoriales/"); ?>/"+folio;
            $.getJSON(url, function(json){
                    //console.log("archivos: " + json['result']);
                    var img = "";
                    $.each(json['result'], function(i, obj){

                            if(obj.ext == 'shp')
                                img = "<?php echo base_url("https://www.mapdiva.com/wp-content/uploads/2017/07/what-are-shapefiles-.png") ?>";
                            else if(obj.ext == 'zip')
                                img = "<?php echo base_url("assets/images/rar.jpeg") ?>";
                            else
                                img = "<?php echo base_url("assets/images/file_empty.svg") ?>";

                            var html = ' <div class="col-lg-4 col-xl-3"> <div class="file-man-box"><div class="file-img-box"><img src="'+img+'" alt="icon"></div><a href="<?php echo base_url("documentos"); ?>/'+folio+'/vectorial/'+obj.nombre+'" class="file-download btnShow" target="_new"><i class="bx bx-cloud-download"></i></a><div class="file-man-title"><h5 class="mb-0 text-overflow">'+obj.nombre+'</h5><p class="mb-0"><small>'+parseFloat(obj.size)/1000+' KB</small></p></div> </div></div>';
                            $('#files_vectores').append(html);
                    });
                    }).fail(function(jqXHR){
                if(jqXHR.status==500 || jqXHR.status==0){
                    // internal server error or internet connection broke 
                    var html = ' <div class="col-lg-4 col-xl-3"> <div class="file-man-box"><div class="file-img-box"><img src="https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-9/256/file-warning-icon.png" alt="icon"><div class="file-man-title"><h5 class="mb-0 text-overflow">Sin Documentos Cargados</h5></div></div> </div></div>';
                                    $('#files').append(html);
                }
            });
        }

        
         /* Descarga del poligono */
         $(document).on("click", "#btn_descargar_capa", function(e) {
            e.preventDefault();

            var cvecat =  '007' + $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();

            var folio = $('#year').val() + $('#folio').val();

            console.log(cvecat + " " + folio);

            //intentamos descargar el shape
            $.ajax({
                url: "<?php echo base_url("captura/descarga_vectorial"); ?>",
                type: "POST",
                data: {"clave" : cvecat, "folio": folio},
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    },
                    success: function (data) {
                        //console.log(data);
                        get_vectores();
                        Swal.fire({
                                text: data,
                                type:"info",
                                confirmButtonColor:"#3b5de7",
                            })
                    
                    }
                }); 


        });

        $(document).on("click", "#btn_borrar_archivos", function(e) {
            e.preventDefault();
            var cvecat =  '007' + $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();

            var folio = $('#year').val() + $('#folio').val();

            //intentamos descargar el shape
            $.ajax({
                url: "<?php echo base_url("captura/borrar_vectorial"); ?>",
                type: "POST",
                data: {"clave" : cvecat, "folio": folio},
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    },
                    success: function (data) {
                        //console.log(data);
                        get_vectores();
                        Swal.fire({
                                text: data,
                                type:"info",
                                confirmButtonColor:"#3b5de7",
                            })
                    
                    }
                }); 

        
        });

         /* CAMBIOS AL PADRON INDIVIDUALES */
         $(document).on("click", "#btn_update_impuesto", function(e) {
            e.preventDefault();
            var valor = $('#impuestotrans').val();
            var label = $('#impuestotrans_label').text();

            actualiza_generales("IMPTO_ANIO", valor, label);
        });

        $(document).on("click", "#btn_update_domiclio", function(e) {
            e.preventDefault();
            var valor = $('#domicilio').val();
            var label = $('#domicilio_label').text();

            actualiza_generales("DOM_NOT", valor, label);
        });

        $(document).on("click", "#btn_update_colonia", function(e) {
            e.preventDefault();
            var valor = $('#colonia').val();
            var label = $('#colonia_label').text();

            actualiza_generales("NOM_COL_NO", valor, label);
        });

        $(document).on("click", "#btn_update_cp", function(e) {
            e.preventDefault();
            var valor = $('#cp').val();
            var label = $('#cp_label').text();

            actualiza_generales("COD_POST_N", valor, label);
        });

        $(document).on("click", "#btn_update_frentes", function(e) {
            e.preventDefault();
            var valor = $('#frentes').val();
            var label = $('#frentes_label').text();

            actualiza_generales("NUM_FTES", valor, label);
        });

        $(document).on("click", "#btn_update_mtsfrente", function(e) {
            e.preventDefault();
            var valor = $('#mtsfrente').val();
            var label = $('#mtsfrente_label').text();

            actualiza_generales("LON_FTE", valor, label);
        });

        $(document).on("click", "#btn_update_mtsfondo", function(e) {
            e.preventDefault();
            var valor = $('#mtsfondo').val();
            var label = $('#mtsfondo_label').text();

            actualiza_generales("LON_FONDO", valor, label);
        });
        
        $(document).on("click", "#btn_update_tipo_efec", function(e) {
            e.preventDefault();
            var valor = $('#tipo_efec').val();
            var label = $('#tipo_efec_label').text();

            actualiza_generales("TIP_EFEC", valor, label);
        });

        $(document).on("click", "#btn_update_tri_efec", function(e) {
            e.preventDefault();
            var valor = $('#tri_efec').val();
            var label = $('#tri_efec_label').text();

            actualiza_generales("TRI_EFEC", valor, label);
        });

        $(document).on("click", "#btn_update_anio_efec", function(e) {
            e.preventDefault();
            var valor = $('#anio_efec').val();
            var label = $('#anio_efec_label').text();

            actualiza_generales("ATO_EFEC", valor, label);
        });

        $(document).on("click", "#btn_update_calle", function(e) {
            e.preventDefault();
            var valor = $('#calle').val().split("-");
            var label = $('#calle_label').text();

            var cve_calle = valor[0].trim();
            var nom_calle = valor[1].trim();

            actualiza_generales("CVE_CALLE_", cve_calle, label);
            actualiza_generales("NOM_CALLE", nom_calle, label);
        });

        $(document).on("click", "#btn_update_numerooficial", function(e) {
            e.preventDefault();
            var valor = $('#numerooficial').val();
            var label = $('#numerooficial_label').text();

            actualiza_generales("NUM_OFIC_U", valor, label);
        });

        $(document).on("click", "#btn_update_domicilio_u", function(e) {
            e.preventDefault();
            var valor = $('#domicilio_u').val();
            var label = $('#domicilio_u_label').text();

            actualiza_generales("UBI_PRED", valor, label);
        });

        $(document).on("click", "#btn_update_colonia_u", function(e) {
            e.preventDefault();
            var valor = $('#colonia_u').val().split("-");
            var label = $('#colonia_u_label').text();

            var cve_col = valor[0].trim();
            var nom_col = valor[1].trim();

            actualiza_generales("CVE_COL_UB", cve_col, label);
            actualiza_generales("NOM_COL", nom_col, label);

        });

        $(document).on("click", "#btn_update_libro", function(e) {
            e.preventDefault();
            var valor = $('#libro').val();
            var label = $('#libro_label').text();

            actualiza_generales("NUM_LIB", valor, label);
        });

        $(document).on("click", "#btn_update_escritura", function(e) {
            e.preventDefault();
            var valor = $('#escritura').val();
            var label = $('#escritura_label').text();

            actualiza_generales("NUM_ESC", valor, label);
        });

        $(document).on("click", "#btn_update_seccion", function(e) {
            e.preventDefault();
            var valor = $('#seccion').val();
            var label = $('#seccion_label').text();

            actualiza_generales("NUM_SECC", valor, label);
        });

        $(document).on("click", "#btn_update_inscripcion", function(e) {
            e.preventDefault();
            var valor = $('#inscripcion').val();
            var label = $('#inscripcion_label').text();

            actualiza_generales("NUM_INSC", valor, label);
        });

        $(document).on("click", "#btn_update_regimen", function(e) {
            e.preventDefault();
            var valor = $('#regimen').val().split("-");
            var label = $('#regimen_label').text();

            var cve_reg = valor[0].trim();
            actualiza_generales("CVE_REGIM", cve_reg, label);
            /*actualiza_generales("NOM_COL", nom_col, label);*/

        });

        $(document).on("click", "#btn_update_restringido", function(e) {
            e.preventDefault();
            var valor = $('#restringido').val();
            var label = $('#restringido_label').text();

            actualiza_generales("STT_REST", valor, label);
        });

        $(document).on("click", "#btn_update_fechalta", function(e) {
            e.preventDefault();
            var valor = $('#fechalta').val();
            var label = $('#fechalta_label').text();

            actualiza_generales("FEC_ALTA", valor, label);
        });

        $(document).on("click", "#btn_update_fechmov", function(e) {
            e.preventDefault();
            var valor = $('#fechmov').val();
            var label = $('#fechmov_label').text();

            actualiza_generales("FEC_MOV", valor, label);
        });

        
        $(document).on("click", "#btn_agregar_uso", function(e) {
           e.preventDefault();
           $('.modal-usos').modal("show");
           var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
           $('#uso_cvecat').val(cvecat);
           cargar_grupos_usos();
           
        });

        $(document).on("click", "#btn_agregar_servicios", function(e) {
           e.preventDefault();
           cargar_servicios();
           $('.modal-servicios').modal("show");
           var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
           $('#serv_cvecat').val(cvecat);
          
        });

       

        $(document).on("click", "#btn_agregar_propietario", function(e) {
           e.preventDefault();
           $('.modal-propietarios').modal("show");
           var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
           $('#prop_cvecat').val(cvecat);

           $.getJSON("<?php echo base_url("padron/genera_clave_propietario");?>", function(json){
                   console.log(json);
                   $('#prop_clave').val(json.result[0].cve_prop);
            });  
           
        });


        $(document).on("click", ".deluso", function(e) {
           e.preventDefault();
           var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();

            let num_uso = $(this).attr("num_uso");
            let cve_uso = $(this).attr("cve_uso");

            //alert("eliminar el uso numero : " + num_uso + " con clave: " + cve_uso);
            Swal.fire({
                                title: '¿Seguro de eliminar el Uso # '+num_uso+' Clave '+cve_uso+'?',
                                type: 'warning',
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Eliminar!'
                                }).then((result) => {
                                    if (result.value) {
                                    $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/eliminar_uso/"); ?>",
                                        data: {'clave': cvecat, 'numero' : num_uso},
                                        success: function(data){
                                            console.log(data);
                                            if (data == 'ok'){
                                                consulta_usos_predio(cvecat);
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Uso eliminado correctamente',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            else{
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al eliminar el Uso',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    }
                                })
           
        });

        $(document).on("click", "#btn_agregar_terreno", function(e) {
            e.preventDefault();
            cargar_demeritos_terreno();
           $('.modal-terrenos').modal("show");
           var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
           $('#terr_cvecat').val(cvecat);
           $('#terr_poblacion').val($('#POBL').val());
           
        });
        
        $(document).on("click", "#btn_agregar_construccion", function(e) {
           e.preventDefault(); 
           $('.modal-const').modal("show");
           $('#const_title').val("Agregar Construcción");  
           var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
           $('#const_cvecat').val(cvecat);        
           cargar_categorias_construccion();
           cargar_edo_construccion();
           cargar_edad_construccion();
           
        });  

        $(document).on("click", "#btn_guardar_propietario", function(e) {
            e.preventDefault();
            var form =  $("#form_propietarios"); 
            if(form.valid()){
                //console.log(form.serialize());
                $.ajax({
                url: "<?php echo base_url("captura/registro_propietario"); ?>",
                type: "POST",
                data: form.serialize(),
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    },
                    success: function (data) {
                        console.log(data);
                        if(data == 'ok'){
                            //reset el form
                            document.getElementById('form_propietarios').reset();
                            //actualizar tabla y cerrar modal
                            
                            $('#tblpropietarios').DataTable().ajax.reload(null, false);
                            $(".modal-propietarios").modal("hide");
                           
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Propietario Registrado Correctamente',
                                type:"success",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                        else{
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Ocurrio un error al registrar el Propietario',
                                type:"error",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                    
                    }
                }); 
            }
        });    
        

        $(document).on("click", "#btn_guardar_uso", function(e) {
            e.preventDefault();
            var form =  $("#form_usos"); 
            if(form.valid()){
                //console.log(form.serialize()); 
                $.ajax({
                url: "<?php echo base_url("captura/registro_usos"); ?>",
                type: "POST",
                data: form.serialize(),
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    },
                    success: function (data) {
                        console.log(data);
                        if(data == 'ok'){
                            //reset el form
                            document.getElementById('form_usos').reset();
                            //actualizar tabla y cerrar modal
                            
                            //$('#tblconstrucciones').DataTable().ajax.reload(null, false);
                            $(".modal-usos").modal("hide");
                            var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();

                            consulta_usos_predio(cvecat);

                            Swal.fire({
                                //title:"Good job!",
                                text: 'Uso Registrado Correctamente',
                                type:"success",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                        else{
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Ocurrio un error al registrar el Uso',
                                type:"error",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                    
                    }
                }); 

            }

        }); 

        
        $(document).on("click", "#btn_actualizar_servicios", function(e) {
            e.preventDefault(); 
            var form =  $("#form_servicios"); 
            if(form.valid()){
                //console.log(form.serialize());
                $.ajax({
                url: "<?php echo base_url("captura/actualizar_servicios"); ?>",
                type: "POST",
                data: form.serialize(),
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    },
                    success: function (data) {
                        //console.log(data);
                        if(data == 'ok'){
                            //reset el form
                            document.getElementById('form_servicios').reset();
                            //actualizar tabla y cerrar modal
                            
                            $(".modal-servicios").modal("hide");

                            consulta_servicios();
                           
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Servicios Actualizados Correctamente',
                                type:"success",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                        else{
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Ocurrio un error al actualizar los servicios',
                                type:"error",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                    
                    }
                }); 
            } 
            
        }); 

        $(document).on("click", "#btn_actualizar_sup_tot_terr", function(e) {
            e.preventDefault();
            //actualizar valor de superficie y total de la construccione en a_unid
            var cvecve = $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
            var total_sup = $('#total_sup_terr').val();
            var total_valor = $('#total_val_terr').val();

            $.ajax({
                type: 'POST',
                url: "<?php echo base_url("captura/actualizar_sup_val_terreno/"); ?>",
                data: {'clave': cvecve, 'SUP_TERR' : total_sup, 'VAL_TERR' : total_valor},
                success: function(data){
                    //console.info(data); 
                    if(data == 'ok'){
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Totales de Terreno actualizado Correctamente',
                                type:"success",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                        else{
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Ocurrio un error al actualizar los totales del Terreno',
                                type:"error",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }   
                }
            });

        });    

        $(document).on("click", "#btn_actualizar_sup_tot_const", function(e) {
            e.preventDefault();
            
            //actualizar valor de superficie y total de la construccione en a_unid
            var cvecve = $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
            var total_sup = $('#total_sup_const').val();
            var total_valor = $('#total_val_const').val();

            $.ajax({
                type: 'POST',
                url: "<?php echo base_url("captura/actualizar_sup_val_construcciones/"); ?>",
                data: {'clave': cvecve, 'SUP_CONST' : total_sup, 'VAL_CONST' : total_valor},
                success: function(data){
                    //console.info(data); 
                    if(data == 'ok'){
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Totales de Construccion actualizado Correctamente',
                                type:"success",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                        else{
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Ocurrio un error al actualizar los totales de la construccion',
                                type:"error",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }   
                }
            });

        }); 

        

        $(document).on("click", "#btn_actualizar_terreno", function(e) {
            e.preventDefault();
            var form =  $("#form_terreno");
            //console.log(form.serialize());
            if(form.valid()){ 
            //ejecutar el post ajax
            $.ajax({
            url: "<?php echo base_url("captura/actualizar_terreno"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //console.log(data);
                    if(data == 'ok'){
                         //reset el form
                        document.getElementById('form_terreno').reset();
                        //actualizar tabla y cerrar modal
                        $('#tblterreno').DataTable().ajax.reload(null, false);
                        $(".modal-terrenos").modal("hide");
                        //visualizar el boton de actualizar y esconder el de registrar
                        $('#btn_guardar_terreno').show();
                        $('#btn_actualizar_terreno').hide();
                        //desplegar mensaje del controlador
                        //alert(data);
                        Swal.fire({
                            //title:"Good job!",
                            text: "Terreno actualizado correctamente",
                            type:"success",
                            showCancelButton:!0,
                            confirmButtonColor:"#3b5de7",
                            cancelButtonColor:"#f46a6a"
                        })
                    }
                    else{
                        Swal.fire({
                            //title:"Good job!",
                            text: 'Ocurrio un error al actualizar el Terreno',
                            type:"error",
                            showCancelButton:!0,
                            confirmButtonColor:"#3b5de7",
                            cancelButtonColor:"#f46a6a"
                        })
                    }
                   
                }
            }); 
            
            }

        }); 
        $(document).on("click", "#btn_actualizar_const", function(e) {
            e.preventDefault();
            var form =  $("#form_const"); 
            if(form.valid()){
                //alert("actualizamos");
                $.ajax({
                url: "<?php echo base_url("captura/actualizar_construccion"); ?>",
                type: "POST",
                data: form.serialize(),
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    },
                    success: function (data) {
                        //alert(data);
                        if(data == 'ok'){
                            //reset el form
                            document.getElementById('form_const').reset();
                            //actualizar tabla y cerrar modal
                            $('#tblconstrucciones').DataTable().ajax.reload(null, false);
                            $(".modal-const").modal("hide");
                            //desplegar mensaje del controlador
                            //visualizar el boton de actualizar y esconder el de registrar
                            $('#btn_guardar_const').show();
                            $('#btn_actualizar_const').hide();
                            
                            
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Construcción Actualizada Correctamente',
                                type:"success",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })         
                        }
                        else{
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Ocurrio un error al Actualizar la Construcción',
                                type:"error",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        } 
                    }
                }); 

            }
        });  

        
        $(document).on("click", "#btn_guardar_const", function(e) {
            e.preventDefault();
            var form =  $("#form_const"); 
            //form.valid();
            if(form.valid()){
                //console.log(form.serialize()); 
                $.ajax({
                url: "<?php echo base_url("captura/registro_construccion"); ?>",
                type: "POST",
                data: form.serialize(),
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    },
                    success: function (data) {
                        if(data == 'ok'){
                            //reset el form
                            document.getElementById('form_const').reset();
                            //actualizar tabla y cerrar modal
                            $('#tblconstrucciones').DataTable().ajax.reload(null, false);
                            $(".modal-const").modal("hide");
                            //desplegar mensaje del controlador
                            //alert(data);
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Construcción Registrada Correctamente',
                                type:"success",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                        else{
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Ocurrio un error al registrar la Construcción',
                                type:"error",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                    
                    }
                }); 
            }
            
           
        });    

        $(document).on("click", "#btn_guardar_terreno", function(e) {
            e.preventDefault();
            var form =  $("#form_terreno");
            console.log(form.serialize());
            if(form.valid()){ 
            //ejecutar el post ajax
            $.ajax({
            url: "<?php echo base_url("captura/registro_terreno"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    if(data == 'ok'){
                         //reset el form
                        document.getElementById('form_terreno').reset();
                        //actualizar tabla y cerrar modal
                        $('#tblterreno').DataTable().ajax.reload(null, false);
                        $(".modal-terrenos").modal("hide");
                        //desplegar mensaje del controlador
                        //alert(data);
                        Swal.fire({
                            //title:"Good job!",
                            text: "Terreno registrado correctamente",
                            type:"success",
                            showCancelButton:!0,
                            confirmButtonColor:"#3b5de7",
                            cancelButtonColor:"#f46a6a"
                        })
                    }
                    else{
                        Swal.fire({
                            //title:"Good job!",
                            text: 'Ocurrio un error al registrar el Terreno',
                            type:"error",
                            showCancelButton:!0,
                            confirmButtonColor:"#3b5de7",
                            cancelButtonColor:"#f46a6a"
                        })
                    }
                   
                }
            }); 
            }
        });

        
        $('#RPOBL').blur(function() {
            var pobl = $('#RPOBL').val().trim();
            pobl = zeroPad(pobl, 3);
            cargar_cuarteles(pobl);
        });

        $('#RPOBL').change(function() {
            $('#RCTEL').val("");
        });


        $('#RCTEL').blur(function() {
            var pobl = $('#RPOBL').val().trim();
            var ctel = $('#RCTEL').val().trim();
            
            cargar_manzanas(pobl, ctel);
        });

        $('#RCTEL').change(function() {
            $('#RMANZ').val("");
        });

        $('#RMANZ').blur(function() {
            var pobl = $('#RPOBL').val().trim();
            var ctel = $('#RCTEL').val().trim();
            var manz = $('#RMANZ').val().trim();
            
            cargar_predios(pobl, ctel, manz);
        });

        $('#RMANZ').change(function() {
            $('#RPRED').val("");
        });

        $('#RPRED').blur(function() {
            var pobl = $('#RPOBL').val().trim();
            var ctel = $('#RCTEL').val().trim();
            var manz = $('#RMANZ').val().trim();
            var pred = $('#RPRED').val().trim();
            
            cargar_unidades(pobl, ctel, manz, pred);
        });

        $('#RPRED').change(function() {
            $('#RUNID').val("");
        });


        $('#terr_calle').blur(function() {
            var pobl = $('#POBL').val().charAt(0);
            var calle = $('#terr_calle').val().split("-");
            console.log("tramo: " + pobl + " calle: "+ calle[0].trim());
            cargar_tramos(pobl, calle[0].trim());
            $('#terr_tramo').val("");
        });

        

        $('#terr_calle_e').blur(function() {
            var pobl = $('#POBL').val().charAt(0);
            var calle = $('#terr_calle_e').val().split("-");
            cargar_tramos_e(pobl, calle[0].trim());
            $('#terr_tramo_e').val("");
        });

        $('#grupo_uso').blur(function() {
            var cve_uso = $('#grupo_uso').val().split("-");
            cargar_usos(cve_uso[0].trim());
            $('#uso_uso').val("");
        });
        

        $(document).on("click", "#div_generar_clave", function(e) {
            e.preventDefault();
           //obtener los valores de la nueva clave
           let pobl = $('#RPOBL').val();
           let ctel = $('#RCTEL').val();
           let manz = $('#RMANZ').val();
           let pred = $('#RPRED').val();
           let unid = $('#RUNID').val();
           let tramite = $('#TRAMITE').val();
           var a = $('#year').val();
           var fol =  $('#folio').val();
           var cvemaster = $('#POBL').val() + $('#CTEL').val() + $('#MANZ').val() + $('#PRED').val() + $('#UNID').val();
           var verificador = "<?php echo $usuario->username; ?>";

           $.ajax({
            url: "<?php echo base_url("captura/registrar_nueva_clave_catastral"); ?>",
            type: "POST",
            data: {'POBL' : pobl, 'CTEL': ctel, 'MANZ' : manz, 'PRED' : pred, 'UNID' : unid, 'a': a, 'fol': fol, 'cvemaster': cvemaster, 'verificador': verificador, "tramite": const_tramite},
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //console.log(data);
                    if(data == 'ok'){
                        if(const_tramite === '20.0'){
                            Swal.fire({
                            text: 'La Clave Catastral fue creada correctamente, capture la información de la nueva clave',
                            type:"success",
                            showCancelButton:!0,
                            confirmButtonColor:"#3b5de7",
                            cancelButtonColor:"#f46a6a"
                            })
                            validar();
                        }
                        else{
                            let a = $('#year').val();
                            let fol =  $('#folio').val();
                            let cve = pobl + ctel + manz + pred + unid;
                            var url = "<?php echo base_url("captura/capturar"); ?>"+"/"+a+"/"+fol+"/"+cve;
                            //window.location.replace(url);
                            window.open(url, "_new");
                        }
                        
                    }
                    else{
                        Swal.fire({
                            text: 'La Clave Catastral ya existe o no pudo ser registrada!',
                            type:"error",
                            showCancelButton:!0,
                            confirmButtonColor:"#3b5de7",
                            cancelButtonColor:"#f46a6a"
                        })
                    }
                    
                   
                }
            }); 

        });

        $(document).on("click", "#btn_finalizar_captura", function(e) {
            e.preventDefault();
             // Create our number formatter.
             var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',

            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
            });
            var anio = $('#year').val();
            var folio = $('#folio').val();
            var cve = $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
            var tramite = $('#tramite_desc').text();
            //actualizamos en la base de datos
            var capturista = "<?php echo $usuario->username; ?>";
            var valor_a = $('#valorcathidden').val();
            var valor_c = formatter.format(parseFloat( $('#total_val_terr').val()) + parseFloat($('#total_val_const').val()));

            Swal.fire({
                                title: '¿Seguro de Finalizar la captura?',
                                type: 'info',
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Finalizar!'
                                }).then((result) => {
                                    if (result.value) {
                                    $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/finalizar_captura/"); ?>",
                                        data: {'anio': anio, 'fol' : folio, 'capturista' : capturista, 'cve_cat' : cve, 'tramite' : tramite, 'valor_a' : valor_a, 'valor_c':valor_c},
                                        success: function(data){
                                            console.log(data);
                                            if (data == 'ok'){
                                                
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Captura finalizada correctamente',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                                var url = "<?php echo base_url("administrativo/avaluos"); ?>";
                                                window.open(url, "_self");

                                            }
                                            else{
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al finalizar la captura',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    }
                                })
        });


        $(document).on("click", "#btn_cancelar_clave", function(e) {
            e.preventDefault();

            var clave_m = $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();

            var clave_c = $('#RPOBL').val()+$('#RCTEL').val()+$('#RMANZ').val()+$('#RPRED').val()+$('#RUNID').val();

            Swal.fire({
                                title: '¿Cancelar la clave catastral?',
                                text: 'Una vez cancelada, los datos del terreno y construccion pasaran a ser parte de la clave maestra y no se podra revertir esta accion',
                                type: 'warning',
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Cancelar!'
                                }).then((result) => {
                                    if (result.value) {
                                    $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/cancelar_clave/"); ?>",
                                        data: {'clave_m': clave_m, 'clave_c' : clave_c},
                                        success: function(data){
                                            console.log(data);
                                            if (data == 'ok'){
                                                
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Clave cancelada correctamente',
                                                    text: 'Verifique que los valores del terreno y construccion nuevos de la clave madre',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                });

                                                $('#tblterreno').DataTable().ajax.reload(null, false);
                                                $('#tblconstrucciones').DataTable().ajax.reload(null, false);
                                                

                                            }
                                            else{
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al cancelar la clave',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    }
                })
        });

        $(document).on('click','#btn_buscar_prop_act',function(e) {
             e.preventDefault();
            var txt_prop = $('#bprop_act').val();
            listar_propietarios(txt_prop, 'prop_act', 0);
            $('#bprop_act').val("");
        });    

        function listar_propietarios(text, control, id){
            $.getJSON("<?php echo base_url("padron/propietarios/");?>/"+text+"/"+id, function(json){
                    $('#'+control).empty();
                    $.each(json.result, function(i, obj){
                            var pat = "";
                            var mat = "";
                            if(obj.APE_PAT == null)
                                pat = "";
                            else
                                pat = obj.APE_PAT;
                            
                            if(obj.APE_MAT == null)
                                mat = "";
                            else
                                mat = obj.APE_MAT;

                            $('#'+control).append($('<option>').text(pat+" "+mat+ " "+obj.NOM_PROP).attr({ 'value' : obj.CVE_PROP}));
                    });
                    $('#'+control).select2();
            });
       }

       $(document).on("click", "#btn_asignar", function(e) {
            e.preventDefault();
            var idprop = $('#prop_act').find("option:selected").val();
            var textprop = $('#prop_act').find("option:selected").text();
            var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
            //alert(idprop+ " " + cvecat);
            Swal.fire({
                                text: '¿Seguro de asignar a '+textprop+' como propietario de esta clave catastral?',
                                type: 'info',
                                imageHeight:80,
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#d33',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Si, Asignar!'
                                }).then((result) => {
                                    if (result.value) {
                                    $.ajax({
                                        type: 'POST',
                                        url: "<?php echo base_url("captura/asignar_propietario/"); ?>",
                                        data: {'idprop': idprop, 'cvecat' : cvecat},
                                        success: function(data){
                                            console.log(data);
                                            if (data == 'ok'){
                                                
                                                Swal.fire({
                                                    type: 'success',
                                                    title: 'Propietario asignado correctamente',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                                $('#tblpropietarios').DataTable().ajax.reload(null, false);
                                            }
                                            else{
                                                Swal.fire({
                                                    type: 'error',
                                                    title: 'Ocurrio un error al asignar el propietario',
                                                    showConfirmButton: false,
                                                    timer: 1500
                                                })
                                            }
                                            
                                        }
                                    });
                                    }
                                })

        });
         

       </script>