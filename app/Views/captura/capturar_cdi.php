<style>
.card-box {
    padding: 20px;
    border-radius: 3px;
    margin-bottom: 30px;
    background-color: #fff;
}

.file-man-box {
    padding: 20px;
    border: 1px solid #e3eaef;
    border-radius: 5px;
    position: relative;
    margin-bottom: 20px
}

.file-man-box .file-close {
    color: #98a6ad;
    position: absolute;
    line-height: 24px;
    font-size: 24px;
    right: 10px;
    top: 10px;
    visibility: hidden
}

.file-man-box .file-img-box {
    line-height: 120px;
    text-align: center
}

.file-man-box .file-img-box img {
    height: 64px
}

.file-man-box .file-download {
    font-size: 24px;
    color: #98a6ad;
    position: absolute;
    right: 10px
}

.file-man-box .file-download:hover {
    color: #313a46
}

.file-man-box .file-man-title {
    padding-right: 25px
}

.file-man-box:hover {
    -webkit-box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02);
    box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02)
}

.file-man-box:hover .file-close {
    visibility: visible
}
.text-overflow {
    text-overflow: ellipsis;
    white-space: nowrap;
    font-size:12px;
    display: block;
    width: 100%;
    overflow: hidden;
}
</style>
<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">CAPTURA DEL C.D.I. </h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">CAPTURA Y VALUACIÓN</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    
                     <!-- start row -->
                     <div class="row">
                     
                     <div class="col-lg-12">
                   
                     
                        <div class="btn-toolbar card d-print-none" role="toolbar">
                            <div class="card-body">
                                <div class="row">

                                <div class="col-lg-3">
                                    <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Año y Folio:</label>
                                                    <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="00" id="year" name="year" required maxlength="2" required value="<?php echo $year; ?>">
                                                                <input type="text" class="form-control" name="folio" placeholder="00000" id="folio" required maxlength="5" required value="<?php echo $folio; ?>">
                                                    </div>
                                    </div>
                                </div>
 
                                <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral:</label>
                                                    
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="POBL" id="POBL" name="POBL" required maxlength="3" readonly value="<?php echo substr($cve, 0, 3); ?>">
                                                                <input type="text" class="form-control" name="CTEL" placeholder="CTEL" id="CTEL" readonly maxlength="3" value="<?php echo substr($cve, 3, 3); ?>">
                                                                <input type="text" class="form-control" placeholder="MANZ" id="MANZ" name="MANZ" required maxlength="3" readonly value="<?php echo substr($cve, 6, 3); ?>">
                                                                <input type="text" class="form-control" name="PRED" placeholder="PRED" id="PRED" readonly maxlength="3" required value="<?php echo substr($cve, 9, 3); ?>">
                                                                <input type="text" class="form-control" placeholder="UNID" id="UNID" name="UNID" maxlength="3" readonly value="<?php echo substr($cve, 12, 3); ?>">
                                                                
                                                            </div>
                                                   
                                                </div>
                                </div>
                                   
                                    <div class="col-lg-3">
                                                <br>
                                                <a href="javascript:;" class="btn btn-success waves-effect waves-light btn-block mt-2" id="btn_finalizar_captura"><i class="mdi mdi-database-check"></i> Finalizar Captura</a>
                                                
                                               
                                    </div>

                                    
                                </div>            
                            </div>
                        </div>
                        <!--end toolbar -->             
                    </div>                

                     <!-- begin tabs -->
                     <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-pills nav-fill pt-3" role="tablist">
  
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#propietarios" role="tab">
                                                <span class="d-block d-sm-none"><i class="mdi mdi-human-male-female"></i></span>
                                                <span class="d-none d-sm-block "><i class="mdi mdi-human-male-female"></i> Gestión de Propietarios</span>
                                            </a>
                                        </li>  

                                         
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content p-3 text-muted">
                                          

                                        <div class="tab-pane active" id="propietarios" role="tabpanel">
                                        
                                        <div class="form-group position-relative">
                                                    <input class="form-control" type="hidden" id="valorcathidden">
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected mb-2">
                                                    <input type="text" class="form-control" placeholder="Buscar Propietarios" id="bprop_act" name="bprop_act"><span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_buscar_prop_act">Buscar</button></span><span class="input-group-btn input-group-append"><button class="btn btn-success bootstrap-touchspin-up" type="button" id="btn_agregar_propietario"><i class="mdi mdi-account-multiple-plus-outline"></i> Registrar Nuevo Propietario</button></span></div>
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected mb-2">
                                                    <select class="form-control select2" id="prop_act" name="prop_act"></select>
                                                    <span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_asignar">+ Asignar</button></span></div>
                                                </div>
                                            <div class="table-responsive">
                                                <table class="table table-hover" id="tblpropietarios" width="100%">
                                                    <thead class="text-white" style="background-color: #480912;"> 
                                                        <tr>
                                                            <th>Acciones</th>
                                                            <th>Num.</th>
                                                            <th>Clave</th>
                                                            <th>Nombre (Razón Social)</th>
                                                            <th>Apellido Paterno</th>
                                                            <th>Apellido Materno</th>
                                                            <th>Tipo Persona</th>
                                                            <th>RFC</th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    
                                            </table>
                                            </div>
                                        </div>

                                
                    </div>
                    <!-- end tabs -->              

                  
                         
                     </div>
                     <!-- end row -->
                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

           

            <div class="modal fade modal-propietarios" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Agregar Nuevo Propietario</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                        </div>
                        <div class="modal-body">
                        <form action="" name="form_propietarios" id="form_propietarios" method="post" accept-charset="utf-8">
                            <div class="row">
                           
                            
                                <div class="col-lg-12">
                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Número de Propietario:</label>                                  
                                        <input type="number" name="prop_numero" id="prop_numero" class="form-control " placeholder="0" required>
                                        
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Clave de Propietario:</label>                                  
                                        <input type="number" name="prop_clave" id="prop_clave" class="form-control " placeholder="0" required readonly>
                                        <input type="hidden" name="prop_cvecat" id="prop_cvecat" class="form-control">
                                                                               
                                    </div>

                                    <div class="form-group position-relative">
                                                            
                                        <label class="form-label">Tipo de Persona: </label>
                                                           
                                        <input type="text" class="form-control" name="prop_tip_persona" id="prop_tip_persona"  placeholder="(F / M)" required list="list_tip_per">
                                        <datalist id="list_tip_per">
                                            <option value="F - FISICA">
                                             <option value="M - MORAL">                            
                                        </datalist>
                                                              
                                     </div>
                                    

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Nombre (Razon Social):</label>
                                        
                                        <input type="text" name="prop_nombre" id="prop_nombre" class="form-control " required placeholder="NOMBRE(S)">
                                    
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Apellido Paterno:</label>
                                        
                                        <input type="text" name="prop_apellido_paterno" id="prop_apellido_paterno" class="form-control " required placeholder="APELLIDO PATERNO">
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Apellido Materno:</label>
                                        
                                        <input type="text" name="prop_apellido_materno" id="prop_apellido_materno" class="form-control " required placeholder="APELLIDO MATERNO">
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04"> Reg.Fed.Cont.:</label>
                                        
                                        <input type="text" name="prop_rfc" id="prop_rfc" class="form-control " required placeholder="RFC">
                                                                            
                                    </div>

                                   

                                </div>

                               
                            </div>
                            

                        </div>
                        <div class="modal-footer">
                                                        
                                                        <button type="button" class="btn btn-success" id="btn_guardar_propietario">Registrar</button>
                                                        <button type="button" class="btn btn-primary" id="btn_actualizar_propietario">Actualizar</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar_propietario">Cancelar</button>
                        </div>
                    </div>
                    </form>
                    
                </div>
            </div>
            <!-- end modal -->

          