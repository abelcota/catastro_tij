<script>

$(document).ready(function () {
    cargar_perfil();
});

$(document).on('click', '#btn-guardar', function(e) {
    e.preventDefault();
    var form = $('#form-info');
    $.ajax({
        url: "<?php echo base_url("padron/registrar_perfil"); ?>",
        type: "POST",
        data: form.serialize(),
        success: function(data) {
          console.log(data);
          if (data === 'ok') {
            Swal.fire({
              text: 'Información Guardada Correctamente',
              type: "success",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
            cargar_perfil();

          } else {
            Swal.fire({
              text: "Ocurrio un error al registrar la Información",
              type: "warning",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
          }

        }
      });

});

$(document).on('click', '#btn_actualizar_password', function(e) {
    e.preventDefault();
    var json = new Object();
    json.id = "<?php echo $usuario->id; ?>";
    json.password = $('#txtpassword').val();
    console.log(json);
    $.ajax({
        url: "<?php echo base_url("padron/attemptReset"); ?>",
        type: "POST",
        data: json,
        success: function(data) {
            Swal.fire({
              text: data,
              type: "info",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
        }
      });
});

function cargar_perfil(){
    $.ajax({
      url: "<?php echo base_url("padron/obtener_perfil"); ?>/<?php echo $usuario->id; ?>",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        //console.log(response);
        if (response.perfil.length > 0) {
          var json = response.perfil[0];
            console.log(json);
          $("#txtnombre").val(json.nombre);
          $("#txtrfc").val(json.rfc);
          $("#txtdireccion").val(json.direccion);
          $("#txttelefono").val(json.telefono);
          $("#txtcelular").val(json.celular);
          $("#txtcorreo").val(json.correo);
        }

      }
    });
}


</script>