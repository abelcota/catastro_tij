<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    <!-- Datatable init js -->
    <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


<script>
       
$(document).on('click', '#btn_inspeccion',  function(e){
            e.preventDefault();
            consulta('inspeccion');
            
});

$(document).on('click', '#btn_graficacion',  function(e){
            e.preventDefault();
            consulta('graficacion');
            
});

$(document).on('click', '#btn_captura',  function(e){
            e.preventDefault();
            consulta('captura');
            
});

function consulta(bandeja)
{
    var year = $("#year").val();
    $('#table_semaforizacion').DataTable().destroy();
    if(bandeja === 'inspeccion'){
        url = "<?php echo base_url("seguimiento/get_bandeja_inspeccion")?>/"+year;
    }
    if(bandeja === 'graficacion'){
        url = "<?php echo base_url("seguimiento/get_bandeja_graficacion")?>/"+year;
    }
    if(bandeja === 'captura'){
        url = "<?php echo base_url("seguimiento/get_bandeja_captura")?>/"+year;
    }
    $('#table_semaforizacion').dataTable({
        "language": {
            "url": "<?php echo base_url("assets/Spanish.json")?>"
        },
        "bProcessing": true,
        "sAjaxSource": url,
        "bPaginate":true,
        "sPaginationType":"full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { mData: 'Id_Anio' } ,
            { mData: 'Id_FolioBarra' },
            { mData: 'F_Captura' },
            { mData: 'Pobl' },
            { mData: 'Ctel' },
            { mData: 'Manz' },
            { mData: 'Pred' },
            { mData: 'Unid' },
            { mData: 'DescripcionTramite' },
            { mData: 'F_EnvioInspeccion' },
            { mData: 'DiasInspeccion' },
            { mData: 'F_EnvioGraficacion' },
            { mData: 'F_EnvioIces' },
        ],
        rowCallback: function(row, data, index) {
            var options = { year: 'numeric', month: 'long', day: 'numeric' };
            $("td:eq(1)", row).addClass('text-primary');
            $("td:eq(2)", row).text(new Date(data['F_Captura']).toLocaleDateString('es-MX', options));
            $("td:eq(8)", row).addClass('font-weight-bold');
            $("td:eq(10)", row).addClass('text-warning');
            if(data['F_EnvioInspeccion'] != null)
                $("td:eq(9)", row).text(new Date(data['F_EnvioInspeccion']).toLocaleDateString('es-MX', options));
            if(data['F_EnvioGraficacion'] != null)
                $("td:eq(11)", row).text(new Date(data['F_EnvioGraficacion']).toLocaleDateString('es-MX', options));
            if(data['F_EnvioIces'] != null)
                $("td:eq(12)", row).text(new Date(data['F_EnvioIces']).toLocaleDateString('es-MX', options));
        }
    });

}


</script>