<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
 <!-- Buttons examples -->
 <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
   
   <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
   

   <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


   <!-- App js -->
   <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
   <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
   <script src="<?php echo base_url("/assets/js/dataTables.fixedHeader.min.js");?>"></script>
   <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
   
<script>

var table;

$(document).on('click', '#btn_consultar', function (e) {
    e.preventDefault();
    //obtener el radio seleccionado 
    var rad = $('input[name=radio_tipo]:checked').val();
    var tip = $('input[name=radio_fil]:checked').val();
    console.log(tip);

    if(rad === 'inco'){
        rad = '03';
    }
    else if(rad === 'otros'){
        rad = '08';
    }
    else{
        rad = '00';
    }
    var url = "<?php echo base_url("seguimiento/get_reporte_seguimiento_tramites_ventanilla")?>/"+rad+"/"+tip;
    console.log(url);
    //Destroy the old Datatable
    $('#tbl-rpt').DataTable().destroy();
    $('#tbl-rpt').DataTable({
    "language": {
        "url": "<?php echo base_url("assets/Spanish.json")?>"
    },
    "sAjaxSource": url,
    "bPaginate": false,
    "bFilter": false,
    "bInfo": false,
    //"order": [[ 0, "asc" ]],
    "aoColumns": [
        { mData: 'Id_FolioBarra' } ,
        { mData: 'F_Captura' },
        { mData: 'clave' },
        { mData: 'F_EnvioInspeccion' },
        { mData: 'diasinspeccion' },
        { mData: 'F_EnvioGraficacion' },
        { mData: 'diasgraficacion' },
        { mData: 'F_EnvioIces' },
        { mData: 'NumeroOficioEnvioIces' },
        { mData: 'diasices' },
        { mData: 'F_AvaluoManual' },
        { mData: 'TramiteNotario' },
        { mData: 'Id_QuienLoTiene' },
    ],
    rowCallback: function(row, data, index) {
            if(data['F_Captura'] != null)
                $("td:eq(1)", row).text(moment(data['F_Captura']).locale('es').format('L'));
            $("td:eq(2)", row).text(formatFriendCode( data['clave'], 3, '-').slice(0, -1)); 
            if(data['F_EnvioInspeccion'] != null)
                $("td:eq(3)", row).text(moment(data['F_EnvioInspeccion']).locale('es').format('L'));
            if(data['F_EnvioGraficacion'] != null)
                $("td:eq(5)", row).text(moment(data['F_EnvioGraficacion']).locale('es').format('L'));
            if(data['F_EnvioIces'] != null)
                $("td:eq(7)", row).text(moment(data['F_EnvioIces']).locale('es').format('L'));
            if(data['F_AvaluoManual'] != null)
                $("td:eq(10)", row).text(moment(data['F_AvaluoManual']).locale('es').format('L'));
            $("td:eq(0)", row).addClass('small');  
            $("td:eq(1)", row).addClass('small'); 
            $("td:eq(2)", row).addClass('small'); 
            $("td:eq(3)", row).addClass('small'); 
            $("td:eq(4)", row).addClass('small'); 
            $("td:eq(5)", row).addClass('small'); 
            $("td:eq(6)", row).addClass('small'); 
            $("td:eq(7)", row).addClass('small'); 
            $("td:eq(8)", row).addClass('small'); 
            $("td:eq(9)", row).addClass('small'); 
            $("td:eq(10)", row).addClass('small'); 
            $("td:eq(11)", row).addClass('small'); 
            $("td:eq(12)", row).addClass('small');            
        }
    });

});

$(document).ready(function () {

 

});

function formatFriendCode(friendCode, numDigits, delimiter) {
  return Array.from(friendCode).reduce((accum, cur, idx) => {
    return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
  }, '')
}

</script>