<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    <!-- Datatable init js -->
    <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>



<script>
//funcion para formatear los campos de la clave catastral
function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        }

/*$(document).on('change', '#checkColumna', function() {
    if ($(this)[0].checked) { 
        $('#tbltramites thead tr:eq(1) th').show();
    } else { 
        $('#tbltramites thead tr:eq(1) th').hide();
    }
});*/

$( document ).ready(function() {

    cargar_brigadas();

    $('#POBL').blur(function () {  this.value = zeroPad(this.value, 3); });
    $('#CTEL').blur(function () { this.value = zeroPad(this.value, 3);  });
    $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
    $('#PRED').blur(function () { this.value = zeroPad(this.value, 3);  });
    $('#UNID').blur(function () { this.value = zeroPad(this.value, 3);  });

    // Setup - add a text input to each footer cell
    /*$('#tbltramites thead tr').clone(true).appendTo( '#tbltramites thead' );
    $('#tbltramites thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        
        $(this).html( '<input type="text" class="form-control small" placeholder="Filtrar '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );

    $('#tbltramites thead tr:eq(1) th').hide();*/
    
    var table = $('#tbltramites').DataTable({
        procesing: true,
        serverSide: true,
        ajax:  "<?php echo base_url("seguimiento/ventanilla_seguimiento")?>",
        "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
        deferRender: true,
        pageLength: 10,
        lengthMenu: [[ 10, 25, 50, 100], [ 10, 25, 50, 100]],
        columns: [
            {
                data: null,
                defaultContent: '<a class="consultar text-primary" style="cursor:pointer !important;"> <i class="dripicons-information"></i></a> <a class="verificar text-success ml-2" style="cursor:pointer !important;"> <i class="mdi mdi-file-document-box-check-outline"></i></a>'
            },
            { "data": 'Id_Anio' } ,
            { "data": 'Id_FolioBarra' },
            { "data": 'F_Captura' },
            { "data": 'Nombre_Propietario' },
            { "data": 'clave' },
            { "data": 'Estado' },
            { "data": 'dpto' },
            { "data": 'F_EnvioInspeccion' },
            { "data": 'F_EnvioGraficacion' },
            { "data": 'F_EnvioIces' },
            { "data": 'NumeroOficioEnvioIces' },
            { "data": 'F_AvaluoManual' },
            { "data": 'F_EntregaContribuyente' },
            { "data": 'Observaciones' },
        ],
        rowCallback: function(row, data, index) {
            var options = { year: 'numeric', month: 'long', day: 'numeric' };
            $("td:eq(2)", row).addClass('text-primary');
            $("td:eq(3)", row).text(new Date(data['F_Captura']).toLocaleDateString('es-MX', options));
            $("td:eq(4)", row).addClass('font-weight-bold');
            if(data['Estado'] == 'Trámite Concluido')
                $("td:eq(6)", row).addClass('text-success');
            else if(data['Estado'] == 'Trámite en Curso')
                $("td:eq(6)", row).addClass('text-warning');
            else if (data['Estado'] == 'Trámite Detenido')
                $("td:eq(6)", row).addClass('text-danger');

            if(data['F_EnvioInspeccion'] != null)
                $("td:eq(8)", row).text(new Date(data['F_EnvioInspeccion']).toLocaleDateString('es-MX', options));
            if(data['F_EnvioGraficacion'] != null)
                $("td:eq(9)", row).text(new Date(data['F_EnvioGraficacion']).toLocaleDateString('es-MX', options));
            if(data['F_EnvioIces'] != null)
                $("td:eq(10)", row).text(new Date(data['F_EnvioIces']).toLocaleDateString('es-MX', options));
            if(data['F_AvaluoManual'] != null)
                $("td:eq(12)", row).text(new Date(data['F_AvaluoManual']).toLocaleDateString('es-MX', options));
            if(data['F_EntregaContribuyente'] != null)
                $("td:eq(13)", row).text(new Date(data['F_AvaluoManual']).toLocaleDateString('es-MX', options));
        }
    });

    //click al boton consultar
    $('#tbltramites tbody').on( 'click', '.verificar', function () {
                if($('#tbltramites').DataTable().row(this).child.isShown()){
                    var data = $('#tbltramites').DataTable().row(this).data();
                }else{
                    var data = $('#tbltramites').DataTable().row($(this).parents("tr")).data();
                }
                var a = data["Id_Anio"];
                var fol = data["Id_FolioBarra"].slice(3);;

                var url = "<?php echo base_url("verificacion/verificacion"); ?>/"+a+"/"+fol;
                window.open(url, '_blank');
                
    });


    //click al boton consultar
    $('#tbltramites tbody').on( 'click', '.consultar', function () {
                if($('#tbltramites').DataTable().row(this).child.isShown()){
                    var data = $('#tbltramites').DataTable().row(this).data();
                }else{
                    var data = $('#tbltramites').DataTable().row($(this).parents("tr")).data();
                }
                var a = data["Id_Anio"];
                var fol = data["Id_FolioBarra"];
                var cve = data["clave"];

                Swal.fire({
                title: '¿Consultar el folio: '+fol+' del año 20'+a+'?',
                type: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Consultar!'
                }).then((result) => {
                    if (result.value) {
                    consulta_tramite(a, fol, cve);
                    $(".modal-tramite").modal("show");
                    }
                })
        });
 
});

function consulta_tramite(anio, folio, clave)
{
    $('#form-seguimiento')[0].reset();
    //consultar el tramite
    $.ajax({
        url: "<?php echo base_url("seguimiento/consulta_tramite/");?>/"+anio+"/"+folio+"",
        dataType: "json",
        //se envia un mensaje bloqueando la interfaz para consultar los datos de la base de datos
        beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Tramite...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
        },
        success: function (data) {
            console.log(data);
            //mandamos un log con los datos que retorno la base de datos
            console.log(data.data[0]);
            console.log(data.inspeccion[0]);
            console.log(data.graficacion[0]);
            var tramite = data.data;
            var inspeccion = data.inspeccion;
            var graficacion = data.graficacion;

             //lleno la tabla de movimientos
             console.log("Consultando la clave: " + "<?php echo base_url("seguimiento/consulta_movimientos")?>/"+clave)
            $('#tbl_tmovs').DataTable().destroy();
            $('#tbl_tmovs').DataTable({
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                "sAjaxSource": "<?php echo base_url("seguimiento/consulta_movimientos")?>/"+clave,
                "bPaginate": false,
                "bFilter": false,
                "bInfo": false,
                "aoColumns": [
                    { mData: 'NumeroOficio' } ,
                    { mData: 'tipo' },
                    { mData: 'FechaOficio' },
                    { mData: 'Observaciones' },
                ],
                rowCallback: function(row, data, index) {
                    $("td:eq(2)", row).text(moment(data['FechaOficio']).locale('es').format('L'));
                }
            });

            //Asignamos los valores a los controles del modal
            //fecha de captura
            $("#tfolio").val(tramite[0].Id_FolioBarra);
            $("#tanio").val(tramite[0].Id_Anio);      
            if(tramite[0].F_Captura != null){ $("#tfecha").val(tramite[0].F_Captura.substring(0, 10)); }
            $("#ttramite").val(tramite[0].Tramite);
            $("#tdescinco").val(tramite[0].DescInco);
            $("#icvecat").val(tramite[0].clave);
            $("#inomprop").val(tramite[0].Nombre_Propietario);
            $("#itelefono").val(tramite[0].TelefonoSolicitante);
            //llenamos los valores de inspeccion
            if(tramite[0].F_EnvioInspeccion != null){ $("#tfenvins").val(tramite[0].F_EnvioInspeccion.substring(0, 10)); } 
            if(tramite[0].F_InspeccionCampo != null){ $("#tfeins").val(tramite[0].F_InspeccionCampo.substring(0, 10)); }
            if(inspeccion[0] != null){
                if(inspeccion[0].Id_Brigada != null)
                    $('#tbrigada').val(inspeccion[0].Id_Brigada);
                $("#tdictamenins").val(inspeccion[0].Dictamen);
                $("#tobservaciones").val(inspeccion[0].Observaciones);
            }
            //llenamos los valores de graficacion
            if(tramite[0].F_EnvioGraficacion != null){ $("#tfenvgra").val(tramite[0].F_EnvioGraficacion.substring(0, 10)); }
            if(graficacion[0] != null){
                if(tramite[0].F_AsignadoGraficador != null){ $("#fasigngra").val(graficacion[0].F_AsignadoGraficador.substring(0, 10)); }
                $("#tgraficador").val(graficacion[0].graficador);
                $("#tfgra").val(graficacion[0].F_AtendidoGraficacio);
                $("#tmov").val(graficacion[0].mov1);
                $("#tdictamengra").val(graficacion[0].Dictamen);
                $("#tresultado").val(graficacion[0].resultado);
                $("#tobsgra").val(graficacion[0].Observaciones);
            }
            //llenamos los valores del tramite respecto al envio a ICES
            if(tramite[0].F_EnvioIces != null){ $("#tenvices").val(tramite[0].F_EnvioIces.substring(0, 10)); }
            $("#tofices").val(tramite[0].NumeroOficioEnvioIces);

            //fecha de avaluo manual
            if(tramite[0].F_AvaluoManual != null){ $("#tfavaluomanual").val(tramite[0].F_AvaluoManual.substring(0, 10)); }
            if(tramite[0].F_EntregaContribuyente != null){ $("#tfentregacontr").val(tramite[0].F_EntregaContribuyente.substring(0, 10)); }
            

        },
        //habilitamos la interfaz una vez completada la consulta a la base de datos
        complete : function (){
                        $.unblockUI();
        }
        
    });
}

function cargar_brigadas(){
            $('#tbrigada').empty();
            var url = "<?php echo base_url("seguimiento/get_brigadas_inspeccion");?>";
            $.getJSON(url, function(json){
                console.log(json);
                    $('#tbrigada').append($('<option>').text("Brigada").attr({'value': 0}));
                    $.each(json.results, function(i, obj){
                            $('#tbrigada').append($('<option>').text(obj.Id_Brigada + " - " +obj.brigada).attr({'value': obj.Id_Brigada, 'i1': obj.i1, 'i2': obj.i2}));
                    });     
            });
}


$(document).on('click', '#btn_tgrabar', function (e) {
    e.preventDefault();
    var form = $('#form-seguimiento');
    $.ajax({
            url: "<?php echo base_url("seguimiento/guardar_seguimiento"); ?>",
            type: "POST",
            data: form.serialize(),//form.serialize(),
            success: function (data) {
                console.log(data);
                $('#form-seguimiento')[0].reset();
                if (data == 'ok')
                {
                    //mandamos mensaje de 
                    Swal.fire({
                                        type: 'success',
                                        title: 'Mensaje del Sistema',
                                        text: 'La captura se Registro Correctamente'
                                        
                                    })
                    //cerramos el modal 
                    $('.modal-tramite').modal('hide');
                    $('#tbltramites').DataTable().ajax.reload(null, false);
                }
                else{
                    //mandamos mensaje de 
                    Swal.fire({
                                        type: 'error',
                                        title: 'Mensaje del Sistema',
                                        text: 'Ocurrió un error al guardar la Captura'
                                        
                                    })
                }
            
            
            }
    });

});

</script>