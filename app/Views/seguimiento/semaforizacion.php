<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">SEMAFORIZACIÓN DE TRÁMITES</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">SEGUIMIENTO TRÁMITES</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     
                          <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                     <h4 class="card-title">Semaforización de Trámites</h4>
                                     <p class="card-title-desc"></p>
                                     <!-- start form -->
                                     <form class="needs-validation" novalidate>

                                        <div class="row" style="margin-botton:20px;">
                                            
                                            <div class="col-md-3 text-center">
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-4 col-form-label"> Año:</label>
                                                    <div class="col-md-8">
                                                        <select class="form-control" id="year" name="year">
                                                            <option value="21">21</option>
                                                            <option value="20">20</option>
                                                            <option value="19">19</option>
                                                            <option value="18">18</option>
                                                            <option value="17">17</option>
                                                            <option value="16">16</option>
                                                            <option value="15">15</option>
                                                        </select>
                                                    </div>
                                                </div>
 
                                            </div>
                                            <div class="col-md-3 text-center">
                                               <a href="javascript:;" class="btn btn-primary" id="btn_inspeccion">Bandeja de Inspección</a>
                                            </div> 
                                            <div class="col-md-3 text-center">
                                                <a href="javascript:;" class="btn btn-primary" id="btn_graficacion">Bandeja de Graficación</a>
                                            </div>
                                            <div class="col-md-3 text-center">
                                                <a href="javascript:;" class="btn btn-primary" id="btn_captura">Bandeja Captura</a>
                                            </div>
                                        </div>

                                        <!--<div class="row">
                                            <div class="col-lg-12" style="margin-top:10px;">
                                                <div class="row">
                                                    <div class="col-md-6 text-center">
                                                        <div class="form-group position-relative">
                                                            <label for="validationTooltip03">Fecha Desde:</label>
                                                            <input type="date" class="form-control"  id="fecha_desde">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 text-center">
                                                        <div class="form-group position-relative">
                                                            <label for="validationTooltip04">Fecha Hasta:</label>
                                                            <input type="date" class="form-control"  id="fecha_hasta">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                                                      
                                        </div>-->
                                          <br><br>
                                        <div class="row">
                                            <div class="col-lg-12" style="margin-top:10px;">
                                            <table class="table table-striped table-compat nowrap table-responsive" id="table_semaforizacion" width="100%">
                                                    <thead class="text-white" style="background-color: #480912;">
                                                        <th>Año</th>
                                                        <th>Folio</th>
                                                        <th>Fecha</th>
                                                        <th>Pobl</th>
                                                        <th>Ctel</th>
                                                        <th>Manz</th>
                                                        <th>Pred</th>
                                                        <th>Unid</th>
                                                        <th>Trámite</th>
                                                        <th>F. Envio</th>
                                                        <th>Días.</th>
                                                        <th>F. Envio Graf.</th>
                                                        <th>F. Envio Capt</th>
                                                        </thead>
                                                    </table>
                                            </div>
                                        </div>
                                        
                                                                               
                                    </form>
                                    <!-- end form -->
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            