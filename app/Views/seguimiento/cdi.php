<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">CONSULTA CAMBIOS DE INSCRIPCIÓN</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">SEGUIMIENTO TRÁMITES</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                                        <!-- start row -->
                                        <div class="row">
                          <!-- start col 6 -->
                       
                         <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">  
                                        <div class="float-right">
                                            <i class="dripicons-information text-primary"></i><small> Consultar Trámite</small>
                                        </div>
                                        <h4 class="card-title">Consultar Trámites de Cambios de Inscripción</h4>
                                        <hr>
                                        <table class="table table-hover table-sm nowrap table-responsive" id="tbltramites" width="100%">
                                            <thead class="text-white" style="background-color: #480912;">
                                            <tr>
                                                <th></th>
                                                <th>Año</th>
                                                <th>Folio</th>
                                                <th>Fecha Captura</th>
                                                <th>Propietario</th>
                                                <th>Propietario Anterior</th>
                                                <th>POBL</th>
                                                <th>CTEL</th>
                                                <th>MANZ</th>
                                                <th>PRED</th>
                                                <th>UNID</th>
                                                
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        </div>
                                        </div>
                                        </div>
                     </div>
                     <!-- end row -->


                     <!--modal para ver el listado de solicitudes -->
                    <div class="modal fade modal-tramite" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                        <div class="modal-dialog modal-xl modal-dialog-centered">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title mt-0">Información del Trámite </h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">  
                                                                <form id="form-tramite">
                                                                <div class="row border">
                                                                   
                                                                        <div class="col-lg-12" style="margin-top:10px;">
                                                                            <div class="row">
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group position-relative">
                                                                                        <label for="validationTooltip03">Folio Trámite:</label>
                                                                                        <input type="text" class="form-control"  id="tfolio" name="tfolio">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group position-relative">
                                                                                        <label for="validationTooltip04">Fecha:</label>
                                                                                        <input type="date" class="form-control"  id="tfecha" name="tfecha">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group position-relative">
                                                                                        <label for="validationTooltip04">Trámite</label>
                                                                                        <textarea rows="2" class="form-control" id="ttramite" name="ttramite"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-12" style="margin-top:10px;">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                            <div class="form-group position-relative">
                                                                                                <label for="validationTooltip04">Descripción de la Inconformidad:</label>
                                                                                                <textarea rows="6" class="form-control" id="tdescinco" name="tdescinco"></textarea>
                                                        
                                                                                            </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    
                                                                    </div>
                                          
                                            <div class="row">
                                                
                                                    <div class="col-lg-6" style="margin-top:10px;">
                                                    
                                                    <center><h4 class="card-title">INSPECCIÓN</h4></Center>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip03">Fecha Envio Inspección:</label>
                                                                    <input type="date" class="form-control" id="tfenvins" name="tfenvins">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Fecha Inspección:</label>
                                                                    <input type="date" class="form-control" id="tfeins" name="tfeins">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Brigada:</label>
                                                                    <input type="text" class="form-control" id="tbrigada" name="tbrigada">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Dictamen:   </label>
                                                                    <textarea rows="2" class="form-control" name="tdictamenins" id="tdictamenins"></textarea>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Observaciones:   </label>
                                                                    <textarea rows="4" class="form-control" name="tobservaciones" id="tobservaciones"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!--/col 6 -->
                                                    
                                                
                                                    <div class="col-lg-6" style="margin-top:10px;">
                                                        <center><h4 class="card-title">GRAFICACIÓN</h4></center>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip03">Fecha Envio Graficación:</label>
                                                                    <input type="date" class="form-control" id="tfenvgra" name="tfenvgra">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Fecha Asignada a Graficador:</label>
                                                                    <input type="date" class="form-control" id="fasigngra" name="fasigngra">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Graficador:</label>
                                                                    <input type="text" class="form-control"  id="tgraficador" name="tgraficador">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Fecha Graficación:</label>
                                                                    <input type="date" class="form-control" id="tfgra" name="tfgra">
                                                                </div>
                                                            </div>
                                                        
                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Movimiento:</label>
                                                                    <input type="text" class="form-control" id="tmov" name="tmov"> 
                                                                </div>
                                                            </div>  
                                                            
                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Dictamen:   </label>
                                                                    <textarea rows="2" class="form-control" id="tdictamengra" name="tdictamengra"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Resultado:</label>
                                                                    <input type="text" class="form-control" id="tresultado" name="tresultado">
                                                                </div>
                                                            </div>  
                                                            <div class="col-md-12">
                                                                <div class="form-group position-relative">
                                                                    <label for="validationTooltip04">Observaciones:   </label>
                                                                    <textarea rows="4" class="form-control" id="tobsgra" name="tobsgra"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!--/col 6 -->
                                            
                                                    </div> <!-- row -->
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip03">Fecha Envio ICES:</label>
                                                                        <input type="date" class="form-control"  id="tenvices" name="tenvices">
                                                                    </div> 
                                                                </div> 
                                                                <div class="col-lg-6">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip03">Oficio Envio ICES:</label>
                                                                            <input type="text" class="form-control"  id="tofices" name="tofices">
                                                                    </div>  
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip03">Folio Mov. ICES:</label>
                                                                            <input type="text" class="form-control"  id="tfolmovices" name="tfolmovices">
                                                                    </div>  
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group position-relative">
                                                                    <label for="validationTooltip03"></label>
                                                                        <button class="btn btn-success btn-block">Movimientos</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <h5>Movimientos Clave Catastral:</h5>
                                                                    <table class="table table-hover" width="100%" id="tbl_tmovs">
                                                                        <thead>
                                                                            <th>COD</th>
                                                                            <th>Clave Catastral</th>
                                                                            <th>Movimiento</th>
                                                                            <th>Fecha</th>
                                                                        </thead>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                        <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip03">Fecha Avaluo Manual:</label>
                                                                        <input type="date" class="form-control" id="tfavaluomanual" name="tfavaluomanual">
                                                                    </div> 
                                                                </div> 
                                                                <div class="col-lg-6">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip03">Fecha Entrega Contribuyente:</label>
                                                                        <input type="date" class="form-control"  id="tfentregacontr" name="tfentregacontr">
                                                                    </div>  
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip03">Procedió (S/N):</label>
                                                                        <select class="form-control" name="tprocedio" id="tprocedio">
                                                                            <option value="S" selected>Si</option>
                                                                            <option value="N">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip03">Resultado ICES:</label>
                                                                        <input type="text" class="form-control"  id="tresices" name="tresices">
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip03">Observaciones:</label>
                                                                        <textarea rows="3" class="form-control" name="tobsices" id="tobsices"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group position-relative">
                                                                        <label for="validationTooltip03"></label>
                                                                        <button class="btn btn-success btn-block" type="button" id="btn_tgrabar">Grabar</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                                                        
                                           
                                     
                                                                </div>
                                                                </form>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_tcancelar">Cerrar</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->

                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            