<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    <!-- Datatable init js -->
    <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


<script>
//funcion para formatear los campos de la clave catastral
function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        }

$( document ).ready(function() {
    $('#POBL').blur(function () {  this.value = zeroPad(this.value, 3); });
    $('#CTEL').blur(function () { this.value = zeroPad(this.value, 3);  });
    $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
    $('#PRED').blur(function () { this.value = zeroPad(this.value, 3);  });
    $('#UNID').blur(function () { this.value = zeroPad(this.value, 3);  });

     // Setup - add a text input to each footer cell
     $('#tbltramites thead tr').clone(true).appendTo( '#tbltramites thead' );
    $('#tbltramites thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        
        $(this).html( '<input type="text" class="form-control small" placeholder="Filtrar '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
    
    var table = $('#tbltramites').DataTable({
        "language": {
            "url": "<?php echo base_url("assets/Spanish.json")?>"
        },
        "bProcessing": true,
        "ordering": false,
        "sAjaxSource": "<?php echo base_url("seguimiento/cdi_seguimiento")?>",
        "bPaginate":true,
        "sPaginationType":"full_numbers",
        "iDisplayLength": 25,
        "aoColumns": [
            {
                data: null,
                defaultContent: '<a class="consultar text-primary" style="cursor:pointer !important;"> <i class="dripicons-information"></i></a>'
            },
            { mData: 'AnioAviso' } ,
            { mData: 'Folio' },
            { mData: 'FechaCaptura' },
            { mData: 'PropietarioActual' },
            { mData: 'PropietarioAnterior' },
            { mData: 'Poblacion' },
            { mData: 'Cuartel' },
            { mData: 'Manzana' },
            { mData: 'Predio' },
            { mData: 'Unidad' },
        ],
        rowCallback: function(row, data, index) {
            var options = { year: 'numeric', month: 'long', day: 'numeric' };
            $("td:eq(2)", row).addClass('text-primary');
            $("td:eq(3)", row).text(new Date(data['FechaCaptura']).toLocaleDateString('es-MX', options));
            $("td:eq(4)", row).addClass('font-weight-bold');
            
        }
    });

    //click al boton consultar
    $('#tbltramites tbody').on( 'click', '.consultar', function () {
                if($('#tbltramites').DataTable().row(this).child.isShown()){
                    var data = $('#tbltramites').DataTable().row(this).data();
                }else{
                    var data = $('#tbltramites').DataTable().row($(this).parents("tr")).data();
                }
                var a = data["AnioAviso"];
                var fol = data["Folio"];

                Swal.fire({
                title: '¿Consultar el folio: '+fol+' del año 20'+a+'?',
                type: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Consultar!'
                }).then((result) => {
                    if (result.value) {
                    //consulta_tramite(a, fol);
                    //$(".modal-tramite").modal("show");
                    }
                })
        });
 
});

function consulta_tramite(anio, folio)
{
    //consultar el tramite
    $.ajax({
        url: "<?php echo base_url("seguimiento/consulta_tramite/");?>/"+anio+"/"+folio+"",
        dataType: "json",
        //se envia un mensaje bloqueando la interfaz para consultar los datos de la base de datos
        beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Tramite...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
        },
        success: function (data) {
            //mandamos un log con los datos que retorno la base de datos
            console.log(data.data[0]);
            console.log(data.inspeccion[0]);
            console.log(data.graficacion[0]);
            var tramite = data.data;
            var inspeccion = data.inspeccion;
            var graficacion = data.graficacion;
            //Asignamos los valores a los controles del modal
            //fecha de captura
            $("#tfolio").val(tramite[0].Id_FolioBarra);
            if(tramite[0].F_Captura != null){ $("#tfecha").val(tramite[0].F_Captura.substring(0, 10)); }
            $("#ttramite").val(tramite[0].Tramite);
            $("#tdescinco").val(tramite[0].DescInco);
            //llenamos los valores de inspeccion
            if(tramite[0].F_EnvioInspeccion != null){ $("#tfenvins").val(tramite[0].F_EnvioInspeccion.substring(0, 10)); } 
            if(tramite[0].F_InspeccionCampo != null){ $("#tfeins").val(tramite[0].F_InspeccionCampo.substring(0, 10)); }
            if(inspeccion[0] > 0){
                $("#tbrigada").val(inspeccion[0].Id_Brigada);
                $("#tdictamenins").val(inspeccion[0].Dictamen);
                $("#tobservaciones").val(inspeccion[0].Observaciones);
            }
            //llenamos los valores de graficacion
            if(tramite[0].F_EnvioGraficacion != null){ $("#tfenvgra").val(tramite[0].F_EnvioGraficacion.substring(0, 10)); }
            if(graficacion[0] > 0){
                if(tramite[0].F_AsignadoGraficador != null){ $("#fasigngra").val(graficacion[0].F_AsignadoGraficador.substring(0, 10)); }
                $("#tgraficador").val(graficacion[0].graficador);
                $("#tfgra").val(graficacion[0].F_AtendidoGraficacio);
                $("#tmov").val(graficacion[0].mov1);
                $("#tdictamengra").val(graficacion[0].Dictamen);
                $("#tresultado").val(graficacion[0].resultado);
                $("#tobsgra").val(graficacion[0].Observaciones);
            }
            //llenamos los valores del tramite respecto al envio a ICES
            if(tramite[0].F_EnvioIces != null){ $("#tenvices").val(tramite[0].F_EnvioIces.substring(0, 10)); }
            $("#tofices").val(tramite[0].NumeroOficioEnvioIces);

            //fecha de avaluo manual
            if(tramite[0].F_AvaluoManual != null){ $("#tfavaluomanual").val(tramite[0].F_AvaluoManual.substring(0, 10)); }
            if(tramite[0].F_EntregaContribuyente != null){ $("#tfentregacontr").val(tramite[0].F_EntregaContribuyente.substring(0, 10)); }
            
             
            
            
            
            

        },
        //habilitamos la interfaz una vez completada la consulta a la base de datos
        complete : function (){
                        $.unblockUI();
        }
        
    });
}

    


</script>