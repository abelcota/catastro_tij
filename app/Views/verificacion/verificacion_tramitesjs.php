<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>


    <!-- Required datatable js -->
    <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
     <!-- Buttons examples -->
     <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
       
   
       <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
       <!-- webcam -->
       <script src="<?php echo base_url("assets/js/webcam.min.js");?>"></script>
   
       <!-- App js -->
       <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
       <script src="<?php echo base_url("/assets/js/jquery.validate.js");?>"></script>
       <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>

       <script>

            var folios = [];

            $(document).ready(function () {
                $('#toolbar').hide();
                var anio = $("#year").val();
                var folio = $("#folio").val();
                if(anio != "" && folio != ""){
                    $('#btn_consultar').click();
                }
                else{
                    get_anio();
                }
                
                $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#PRED').blur(function () { this.value = zeroPad(this.value, 3);  });

            });

            $(document).on( 'click', '#btn_consultar', function () {
                var anio = $("#year").val();
                var folio = $("#folio").val();
                consulta_folio(anio, folio);
            });

            function zeroPad(num, places) {
                var zero = places - num.toString().length + 1;
                return Array(+(zero > 0 && zero)).join("0") + num;
            }

            function consulta_folio(a, folio)
            {
            if (folio == ""){
                    Swal.fire({
                        icon: 'error',
                        text: 'Ingrese un folio válido para continuar',
                        })
                 }
                else{

                get_documentos(a+folio);
                    
                $.ajax({
                url: "<?php echo base_url("verificacion/consultafolio/");?>/"+a+"/"+folio+"",
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Folio...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (datasource) {
                       console.log(datasource.data[0]);
                       console.log(datasource.status[0]);
                       var data = datasource.data;
                       var status = datasource.status;

                       if(data.length > 0){
                           
                            //CARGAR CALLES Y COLONIAS DEPENDIENDO LA POBLACION
                            
                            //agregar los valores a los controles
                                $("#CTEL").val(data[0].Clave.substr(0, 2));
                                $("#MANZ").val(data[0].Clave.substr(2, 3));
                                $("#PRED").val(data[0].Clave.substr(5, 3));

                                /*if(status.length > 0){
                                    $('#statustram').html("<b>Departamento:</b> " + status[0].lotiene + " | " + "<b>Estado: </b>" + status[0].Estado + " | <b>Descripcion:</b> " + status[0].descripcion + "" );
                                }*/

                                if(status.length > 0){
                                    if(status[0].status_captura == 'C'){
                                        $('#statustram').html("<b>Departamento:</b> <i class=\"text-success\">" + status[0].lotiene + "</i> | " + "<b>Estado: </b><i class=\"text-success\">" + status[0].Estado + "</i> | <b>Descripcion:</b> <i class=\"text-success\">" + status[0].descripcion + "</i>");
                                        $('#toolbar').hide();
                                    }
                                    else if(status[0].estatus_verificacion == 'NP'){
                                        $('#statustram').html("<b>Departamento:</b> <i class=\"text-danger\">" + status[0].lotiene + " </i>| " + "<b>Estado: </b><i class=\"text-danger\">" + status[0].Estado + "</i> | <b>Descripcion:</b> <i class=\"text-danger\">" + status[0].descripcion + "</i>");
                                        $('#toolbar').hide();
                                    }
                                    else{
                                        $('#statustram').html("<b>Departamento:</b> <i class=\"text-info\">" + status[0].lotiene + " </i>| " + "<b>Estado: </b><i class=\"text-info\">" + status[0].Estado + "</i> | <b>Descripcion:</b> <i class=\"text-info\">" + status[0].descripcion + "</i>" );
                                        $('#toolbar').show();
                                    }
                                }
                                
                                
                                //fecha de captura
                                //$("#dtfecha").val(moment(data[0].FechaCapturaInconformidad).format('YYYY-MM-DD'));
                                //Calle y colonia
                                $('#nombres').val(data[0].NombrePropietario);

                                $('#idcolonia').val(data[0].NOM_COL);
                                //$('#idcolonia').val(data[0].IdColonia).trigger('change.select2')
                                $('#idcalle').val(data[0].NOM_CALLE);
                                //Numero interior y uso
                                $("#numeroof").val(data[0].NumeroOficial);
                                $("#uso").val(data[0].DES_USO);
                                //superficies y valor
                                $("#superficiecons").val(data[0].TotalSuperficieConstruccion);
                                $("#superficieterr").val(data[0].TotalSuperficieTerreno);
                                $("#valorcat").val(formatter.format(data[0].ValorCatastral));
                                //nombre del solicitante, domicilio y telefono
                                $("#nom_sol").val(data[0].NombreSolicitante);
                                $("#dom_sol").val(data[0].DomicilioSolicitante);
                                $("#tel_sol").val(data[0].TelefonoSolicitante);
                                //tramites y motivo
                                $('#idtramites').val(data[0].IdTramite);
                                $('#tramites').val(data[0].DescripcionTramite);
                                cargar_requisitos(data[0].IdTramite);
                                $('#empleados').val(data[0].Nombre);
                                $("#tramitenot").val(data[0].TramiteNotario);
                                $("#motivo").text(data[0].ObservacionesBarra);
                                //validar el vectorial
                                validar();
                                
                                
                       }
                       else {
                           Swal.fire({
                               icon: 'error',
                               title: 'Folio no encontrado',
                               text: 'El folio no se encuentra registrado!'
                               
                           })
                       }
                       
                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });
                    
                    

            }

        }

        function get_anio(){
            var anio = new Date().getFullYear().toString(); 
            var a = anio.substr(2,4);
            $('#year').val(a); 
            return a;
        }

        function get_documentos(cve){
            $('#files').empty();
            var url = "<?php echo base_url("verificacion/cargar_documentacion/"); ?>/"+cve;
            $.getJSON(url, function(json){
                    //console.log("archivos: " + json['result']);
                    var img = "";
                    $.each(json['result'], function(i, obj){

                            if(obj.ext == 'pdf')
                                img = "<?php echo base_url("assets/images/file_pdf.svg") ?>";
                            else if(obj.ext == 'doc' || obj.text == 'docx')
                                img = "<?php echo base_url("assets/images/file_word.svg") ?>";
                            else if(obj.ext == 'xlsx' || obj.text == 'xls')
                                img = "<?php echo base_url("assets/images/file_excel.svg") ?>";
                            else if (obj.ext == 'jpg' || obj.ext == 'png' || obj.ext == 'gif' || obj.ext == 'jpeg')
                                img = "<?php echo base_url("assets/images/file_img.svg") ?>";
                            else
                                img = "<?php echo base_url("assets/images/file_empty.svg") ?>";

                            var html = ' <div class="col-lg-4 col-xl-3"><div class="file-man-box"><a href="javascript:;" class="del_doc" nombre="'+obj.nombre+'"><i class="bx bx-x-circle text-danger"></i></a><div class="file-img-box"><img src="'+img+'" alt="icon"></div><a href="javascript:;" class="file-download btnShow" url="<?php echo base_url("documentos"); ?>/'+cve+'/'+obj.nombre+'"><i class="bx bx-cloud-download"></i></a><div class="file-man-title"><h5 class="mb-0 small">'+obj.nombre+'</h5><p class="mb-0"><small>'+parseFloat(obj.size)/1000+' KB</small></p></div> </div></div>';
                            $('#files').append(html);
                    });
            }).fail(function(jqXHR){
        if(jqXHR.status==500 || jqXHR.status==0){
            // internal server error or internet connection broke 
            var html = ' <div class="col-lg-4 col-xl-3"> <div class="file-man-box"><div class="file-img-box"><img src="https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-9/256/file-warning-icon.png" alt="icon"><div class="file-man-title"><h5 class="mb-0 text-overflow">Sin Documentos Cargados</h5></div></div> </div></div>';
                            //$('#files').append(html);
        }
    });
        }

        function cargar_requisitos(id){
            var url = "<?php echo base_url("requisitos/get/");?>/"+id;
            $.getJSON(url, function(json){
                    $('#requisitos_list').empty();
                    //console.log(json);
                    $.each(json.result, function(i, obj){
                            var html = '<div class="form-check mb-2"><input class="form-check-input" type="checkbox" checked id="'+obj.IdRequisito+'" name="doc'+(i+1)+'"><label class="form-check-label" for="defaultCheck1">'+obj.DescripcionRequisito+'</label></div>';
                            $('#requisitos_list').append(html);
                    });
            });
         }

         

        function validar() {
             //limpiamos el consultado
            if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }
            //_geojson_vectorSource.clear();
             //obtener los valores de los input de la clave
             var ctel = $('#CTEL').val();
             var manz = $('#MANZ').val();
             var pred = $('#PRED').val();

             var cvecat =  ctel + manz + pred;

             console.log("validando clave: " + cvecat)
             $.ajax({
                url: "<?php echo base_url("verificacion/validarcve");?>/"+cvecat,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Validando Clave Catastral...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                    console.info("data",data.result);
                    if(data.result.length > 0){

                        if(data.result[0].ID_REGTO == "B"){
                            Swal.fire({
                               icon: 'error',
                               title: 'Clave dada de Baja...',                               
                           })
                           $("#btn_limpiar").click();
                        }
                        else if(data.result[0].DOM_NOT == ""){
                            Swal.fire({
                               icon: 'error',
                               title: 'Este trámite no puede seguir, requiere Domicilio de Notificación!!',                               
                           })
                           $("#btn_limpiar").click();
                        }
                        else{
                            //NOMBRE COMPLETO    
                            //se obtiene la foto y se decodifica
                            var foto = data.result[0].foto;
                            //console.log(foto);
                            if(foto != null){
                                $(".image-tag").val(foto);
                                $("#foto").attr('src', "data:image/jpeg;base64," + foto );
                            }
                            //Se obtiene el array del poligono 
                            jsonobj = jQuery.parseJSON(data.result[0].jsonb_build_object);
                            //si encuentra el poligono en el vectorial
                            console.log(jsonobj);
                            if (jsonobj.features != null) {
                                //Se visualiza el poligono en el mapa
                                visualizar_capa(jsonobj);
                                //llenar datos del geometrico
                                var nombre = jsonobj.features[0].properties.f3 != null ? jsonobj.features[0].properties.f3 : "";
                               
                                var calle = jsonobj.features[0].properties.f4 != null ? jsonobj.features[0].properties.f4 : "";

                                var colonia = jsonobj.features[0].properties.f5 != null ? jsonobj.features[0].properties.f5 : "";

                                var numoficial = jsonobj.features[0].properties.f6 != null ? jsonobj.features[0].properties.f6 : "";

                                var supterr = jsonobj.features[0].properties.f7 != null ? jsonobj.features[0].properties.f7 : "";

                                var supcons = jsonobj.features[0].properties.f8 != null ? jsonobj.features[0].properties.f8 : "";

                                var valcat = jsonobj.features[0].properties.f9 != null ? jsonobj.features[0].properties.f9 : "";

                                /*$('#nombres').val($('#nombres').val() + " / " + nombre);
                                
                                $('#idcalle').val($('#idcalle').val() + " / " + calle);

                                $('#idcolonia').val($('#idcolonia').val() + " / " + colonia);

                                $('#numeroof').val($('#numeroof').val() + " / " + numoficial);

                                $('#superficieterr').val($('#superficieterr').val() + " / " + supterr );

                                $('#superficiecons').val($('#superficiecons').val() + " / " + supcons );

                                $('#valorcat').val($('#valorcat').val() + " / " + valcat );*/
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Vectorial no encontrado',
                                    text: 'La clave catastral no se encontró en el vectorial!'
                                    
                                })
                            }
                        }
                            
                       }
                       else {
  
                           //verificar si se encuentra de primer registro
                           Swal.fire({
                            title: 'Clave Catastral no encontrada',
                            icon: 'error',
                            showCancelButton: true,
                            cancelButtonText: 'Notificar',
                            confirmButtonColor: '#00b300',
                            cancelButtonColor: '#0000FF',
                            confirmButtonText: 'Primer Registro'
                            }).then((result) => {
                                if (result.value) {
                                    $('#tramites').val("20.0").trigger('change');
                                    $('#tramites').focus();
                                    $("#Clave").val(cvecat);
                                }
                                else{
                                    $("#btn_limpiar").click();
                                }
                            })

                           
                       }          
                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });
         }
        

        $(document).on('click', '.btnShow', function(e){
            e.preventDefault();
            var url = $(this).attr('url');
            //alert(url);
            /*$('#objdata').removeAttr( "data" );
            $('#objdata').attr('data', url);
            $('#modal-doc').modal('show');*/
            // coords is a div in HTML below the map to display
            //document.getElementById('coords').innerHTML = locTxt;
            //console.log(locTxt);
            //console.log(url);
            window.open(url,'popup','width=800,height=600,scrollbars=no,resizable=no');
            
        });

        
        $( document ).on('click', '#btn_autorizar', function(e) {
            let rfc =  $('#txtRFC').val();
            let nombre = $('#txtNombre').val();
            let serie = $('#txtSerie').val();
            let sello = $('#txtSello').val();
            if(rfc != "" && serie != "" && sello != ""){
                var json = new Object();
                json.rfc = rfc;
                json.nombre = nombre;
                json.serie = serie;
                json.sello = sello;
                console.log(json); 
            }
            else{
                Swal.fire({
                    text: 'Para realizar la Autorización debe Firmar el movimiento',
                    icon: 'warning'
                })
            }
        });

        $( document ).on('click', '#btn_cancelar_autorizar', function(e) {
            $('.modal-autorizar').find('input:text').val('');
            $('.modal-autorizar').find('input:file').val('');
            $('.modal-autorizar').find('input:password').val('');
            $('.modal-autorizar').find('textarea').val('');
        });


        $( document ).on('click', '#btn_envcaptura', function(e) {
            if($("#folio").val() != ""){
                var folio = '000' + $("#folio").val();
                /*$("#datofirmar").val("Autorizo folio " +folio);
                $(".modal-autorizar").modal('show');*/
                 //datos del folio
                 var anio = $("#year").val();
                var folio = '000' + $("#folio").val();
                var ctel = $("#CTEL").val();
                var manz = $("#MANZ").val();
                var pred = $("#PRED").val();

                var cvecat = ctel + manz + pred;

                Swal.fire({
                title: '¿Seguro de enviar el Trámite al módulo de Captura?',
                text: "!Una vez enviado el trámite se procederá a actualizar el padrón!",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Enviar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        
                            //actualizamos en la base de datos
                            var verificador = "<?php echo $usuario->username; ?>";
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url("verificacion/enviar_a_captura"); ?>",
                                type: "POST",
                                data: {'id_anio' : anio, 'id_folio': folio, 'idtramite' : idtramite, 'cvecat' : cvecat, 'verificador' : verificador},
                                cache: false,
                                success: function(data){

                                    console.log(data);
                                    if (data == 'ok')
                                    {
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'info',
                                                            title: 'Mensaje del Sistema',
                                                            text: 'El folio se envió a captura correctamente'
                                                            
                                                        })
                                        //cerramos el modal 
                                        $("#btn_consultar").click();
                                        //$("#btn_cancelar_autorizar").click();
                                       
                                    }
                                    else{
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'error',
                                                            title: 'Mensaje del Sistema',
                                                            text: 'Ocurrió un error al enviar el folio a captura'
                                                            
                                                        })
                                                        
                                    }
                                }
                            });
                            
                        
                    }
                })


                var idtramite = $("#idtramites").val();
            }
            else{
                Swal.fire({
                text: 'El folio se encuentra vacio... verifique por favor',
                icon: 'warning'
                })
            }

        });

        
        $( document ).on('click', '.del_doc', function(e) {
            e.preventDefault();
            var a = $("#year").val();
            var fol = $("#folio").val();
            var name = $(this).attr('nombre');
            //alert(name);
            //eliminar_documento
            //preguntar si en realidad se desea cancelar el tramite
            Swal.fire({
                text: '¿Seguro de eliminar el documento '+name+'?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si Eliminar'
                }).then((result) => {
                    if (result.isConfirmed) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url("verificacion/eliminar_documento"); ?>/"+a+"/"+fol+"/"+name,
                                type: "POST",
                                success: function(data){
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'info',
                                                            title: 'Mensaje del Sistema',
                                                            text: data
                                                            
                                                })
                                        
                                        get_documentos(a+fol);
                                   
                                }
                            });
                    }
                })

        });

        $( document ).on('click', '#btn_subir', function(e) {
            e.preventDefault();
            var a = $("#year").val();
            var fol = $("#folio").val();
            var form = $("#form_archivos")[0];
            var pobl = $("#POBL").val();
            var ctel = $("#CTEL").val();
            var manz = $("#MANZ").val();
            var pred = $("#PRED").val();
            var unid = $("#UNID").val();

            var cve = pobl + ctel + manz + pred + unid;
            // Create an FormData object 
            var data = new FormData(form);

            $.ajax({
            url: "<?php echo base_url("verificacion/subir_documentos"); ?>/"+a+"/"+fol+"/"+cve,
            enctype: 'multipart/form-data',
            type: "POST",
            data: data,//form.serialize(),
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);          
                },
                success: function (data) {
                    if(data == 'ok'){
                         //mandamos mensaje de 
                         Swal.fire({
                                icon: 'info',
                                title: 'Mensaje del Sistema',
                                text: 'Los documentos fueron cargados correctamente'
                        })

                        //actalizamos los archivos
                        get_documentos(a+fol);
                        $(this).next('.custom-file-label').html("");
                    }
                    else{
                        Swal.fire({
                                icon: 'error',
                                title: 'Mensaje del Sistema',
                                text: 'No se adjunto ningun documento'
                        })
                    }
                }
            });
        });  

        $( document ).on('click', '#btn_noprocede', function(e) {
            if($("#folio").val() == ""){
                Swal.fire({
                text: 'El folio se encuentra vacio... verifique por favor',
                icon: 'warning'
                })
            }
            else{
               
            
            //alert('Cancelar tramite');
            var anio = $("#year").val();
            var folio = '000' + $("#folio").val();
            var ctel = $("#CTEL").val();
            var manz = $("#MANZ").val();
            var pred = $("#PRED").val();
            

            //preguntar si en realidad se desea cancelar el tramite
            Swal.fire({
                title: '¿Seguro que trámite no procede?',
                text: "!Una vez marcado el trámite se verá reflejado en seguimiento como Trámite Concluido!\nEscriba las observaciones!",
                icon: 'warning',
                input: 'textarea',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Confirmar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        if (result.value) {
                            //actualizamos en la base de datos

                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url("verificacion/registrar_no_procede"); ?>",
                                type: "POST",
                                data: {'Anio' : anio, 'Folio': folio, 'Observaciones' : result.value},
                                cache: false,
                                success: function(data){
                                    if (data == 'ok')
                                    {
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'info',
                                                            title: 'Mensaje del Sistema',
                                                            text: 'El folio se actualizó correctamente'
                                                            
                                                        })
                                        //cerramos el modal 
                                       
                                    }
                                    else{
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'error',
                                                            title: 'Mensaje del Sistema',
                                                            text: 'Ocurrió un error al actualizar el folio'
                                                            
                                                        })
                                    }
                                }
                            });
                            
                        }
                    }
                })
            }
        });


        $( document ).on('click', '#btn_envgraf', function(e) {
            if($("#folio").val() == ""){
                Swal.fire({
                text: 'El folio se encuentra vacio... verifique por favor',
                icon: 'warning'
                })
            }
            else{
            folios = [];
            var anio = $("#year").val();
            var folio = $("#folio").val();
            var ctel = $("#CTEL").val();
            var manz = $("#MANZ").val();
            var pred = $("#PRED").val();

            $('.modal-enviograf').modal('show');

            cargar_empleados();
            genera_bitacora_graficacion();

            $('#fechaenviograf').val(moment(new Date()).format("YYYY-MM-DD")); 
            var data = {'AnioDeclaracion' : anio, 'IdInconformidad': folio, 'Poblacion' : '004', 'Cuartel' : ctel, 'Manzana' : manz, 'Predio': pred, 'Unidad': '001'}

            $("#yeargraf").val(anio);
            $("#foliograf").val(folio);
            $("#cvecatgraf").val(ctel+"-"+manz+"-"+pred);
            folios.push(data);
            console.log(folios);
            }

        });

        

        
        $( document ).on('click', '#btn_envinsp', function(e) {
            if($("#folio").val() == ""){
                Swal.fire({
                text: 'El folio se encuentra vacio... verifique por favor',
                icon: 'warning'
                })
            }
            else{
            folios = [];
            var anio = $("#year").val();
            var folio = $("#folio").val();
            var ctel = $("#CTEL").val();
            var manz = $("#MANZ").val();
            var pred = $("#PRED").val();

            $('.modal-envinsp').modal('show');
            cargar_empleados();
            genera_bitacora_inspeccion();
            $('#fechaenvio').val(moment(new Date()).format("YYYY-MM-DD"));  
            var data = {'AnioDeclaracion' : anio, 'IdInconformidad': folio, 'Poblacion' : '004', 'Cuartel' : ctel, 'Manzana' : manz, 'Predio': pred, 'Unidad': '001'}
            
            $("#year2").val(anio);
            $("#folio2").val(folio);
            $("#cvecat").val(ctel+"-"+manz+"-"+pred);
            folios.push(data);
            console.log(folios);
            }
        });

        function genera_bitacora_inspeccion()
        {
            $.getJSON("<?php echo base_url("verificacion/genera_bitacora_inspeccion");?>", function(json){ 
                        $.each(json.result, function(i, obj){
                            var max = parseInt(obj.maximo);
                            var max = max + 1; 
                            $('#bitacora').val(zeroPad(max, 8));
                        });
            }); 
        }

        function genera_bitacora_graficacion()
        {
            $.getJSON("<?php echo base_url("verificacion/genera_bitacora_graficacion");?>", function(json){ 
                        $.each(json.result, function(i, obj){
                            var max = parseInt(obj.maximo);
                            var max = max + 1; 
                            $('#bitacoragraf').val(zeroPad(max, 8));
                        });
            }); 
        }

        function cargar_empleados(){
            $.getJSON("<?php echo base_url("verificacion/get_empleados");?>", function(json){
                    $('#envia').empty();
                    $('#recibe').empty();
                    $('#enviagraf').empty();
                    $('#recibegraf').empty();
                    $('#envia').append($('<option>').text("Seleccionar Empleado").attr({'value':""}));
                    $('#recibe').append($('<option>').text("Seleccionar Empleado").attr({'value':""}));
                    $('#enviagraf').append($('<option>').text("Seleccionar Empleado").attr({'value':""}));
                    $('#recibegraf').append($('<option>').text("Seleccionar Empleado").attr({'value':""}));
                    $.each(json.result, function(i, obj){
                            $('#envia').append($('<option>').text(obj.Nombre + " " + obj.ApellidoPaterno + " " + obj.ApellidoMaterno).attr({ 'value' : obj.IdEmpleado}));
                            $('#recibe').append($('<option>').text(obj.Nombre + " " + obj.ApellidoPaterno + " " + obj.ApellidoMaterno).attr({ 'value' : obj.IdEmpleado}));
                            $('#enviagraf').append($('<option>').text(obj.Nombre + " " + obj.ApellidoPaterno + " " + obj.ApellidoMaterno).attr({ 'value' : obj.IdEmpleado}));
                            $('#recibegraf').append($('<option>').text(obj.Nombre + " " + obj.ApellidoPaterno + " " + obj.ApellidoMaterno).attr({ 'value' : obj.IdEmpleado}));
                    });
                    //$('#empleados').select2();
            });
         }

         var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',

            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
            });

         $( document ).on('click', '#btn_guardar', function(e) {
            //obtener los elementos que se van a enviar por post
            var bitacora = $("#bitacora").val();
            var fecha = $("#fechaenvio").val();
            var envia = $("#envia").val();
            var recibe = $("#recibe").val();
            //los folios seleccionados
            var selectfolios = folios;
            //alert(bitacora + " " + fecha + " " + envia + " " + recibe + " " + selectfolios);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url("verificacion/registrar_bitacora_inspeccion"); ?>",
                type: "POST",
                data: {"Bitacora": bitacora, "FechaEnv": fecha, "Envia": envia, "Recibe":recibe, "Folios": selectfolios },
                cache: false,
                success: function(data){
                    if (data == 'ok')
                    {
                        //mandamos mensaje de 
                        Swal.fire({
                                            icon: 'info',
                                            title: 'Mensaje del Sistema',
                                            text: 'El paquete se envió a Inspección Correctamente'
                                            
                                        })
                        //cerramos el modal 
                        $('.modal-envinsp').modal('hide');
                    }
                    else{
                        //mandamos mensaje de 
                        Swal.fire({
                                            icon: 'error',
                                            title: 'Mensaje del Sistema',
                                            text: 'Ocurrió un error al enviar el paquete a Inspección'
                                            
                                        })
                    }
                }
            });
            
        });

         // Add the following code if you want the name of the file appear on select
         $(".custom-file-input").on("change", function() {
            var files = [];
            for (var i = 0; i < $(this)[0].files.length; i++) {
                files.push($(this)[0].files[i].name);
            }
            $(this).next('.custom-file-label').html(files.join(', '));
        });

        $( document ).on('click', '#btn_guardargraf', function(e) {
            //obtener los elementos que se van a enviar por post
            var bitacora = $("#bitacoragraf").val();
            var fecha = $("#fechaenviograf").val();
            var envia = $("#enviagraf").val();
            var origen = $("#origengraf").val();
            var recibe = $("#recibegraf").val();
            //los folios seleccionados
            var selectfolios = folios;
            //alert(bitacora + " " + fecha + " " + envia + " " + recibe + " " + selectfolios);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url("verificacion/registrar_bitacora_graficacion"); ?>",
                type: "POST",
                data: {"Bitacora": bitacora, "FechaEnv": fecha, "Origen":origen, "Envia": envia, "Recibe":recibe, "Folios": selectfolios },
                cache: false,
                success: function(data){
                    //console.log(data);
                    if (data == 'ok')
                    {
                        //mandamos mensaje de 
                        Swal.fire({
                                            icon: 'info',
                                            title: 'Mensaje del Sistema',
                                            text: 'El paquete se envió a Graficación Correctamente'
                                            
                                        })
                        //cerramos el modal 
                        $('.modal-enviograf').modal('hide');
                    }
                    else{
                        //mandamos mensaje de 
                        Swal.fire({
                                            icon: 'error',
                                            title: 'Mensaje del Sistema',
                                            text: 'Ocurrió un error al enviar el paquete a Graficación'
                                            
                                        })
                    }
                }
            });
            
        });


       </script>