<style>
.card-box {
    padding: 20px;
    border-radius: 3px;
    margin-bottom: 30px;
    background-color: #fff;
}

.file-man-box {
    padding: 20px;
    border: 1px solid #e3eaef;
    border-radius: 5px;
    position: relative;
    margin-bottom: 20px
}

.file-man-box .file-close {
    color: #98a6ad;
    position: absolute;
    line-height: 24px;
    font-size: 24px;
    right: 10px;
    top: 10px;
    visibility: hidden
}

.file-man-box .file-img-box {
    line-height: 120px;
    text-align: center
}

.file-man-box .file-img-box img {
    height: 64px
}

.file-man-box .file-download {
    font-size: 24px;
    color: #98a6ad;
    position: absolute;
    right: 10px
}

.file-man-box .file-download:hover {
    color: #313a46
}

.file-man-box .file-man-title {
    padding-right: 25px
}

.file-man-box:hover {
    -webkit-box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02);
    box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02)
}

.file-man-box:hover .file-close {
    visibility: visible
}
.text-overflow {
    text-overflow: ellipsis;
    white-space: nowrap;
    font-size:12px;
    display: block;
    width: 100%;
    overflow: hidden;
}
</style>
<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">VERIFICACIÓN DE CAMBIOS DE INSCRIPCIÒN</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">ADMINISTRATIVO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="col-lg-12">
                        <div class="btn-toolbar card d-print-none" role="toolbar">
                            <div class="card-body">
                                <div class="row">

                                <div class="col-lg-3">
                                    <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Año y Folio:</label>
                                                    <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="00" id="year" name="year" required maxlength="2" required value="<?php echo $year; ?>">
                                                                <input type="text" class="form-control" name="folio" placeholder="00000" id="folio" required maxlength="5" required value="<?php echo $folio; ?>">
                                                    </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral:</label>
                                                    
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="CTEL" placeholder="CTEL" id="CTEL" readonly maxlength="3">
                                                                <input type="text" class="form-control" placeholder="MANZ" id="MANZ" name="MANZ" required maxlength="3" readonly>
                                                                <input type="text" class="form-control" name="PRED" placeholder="PRED" id="PRED" readonly maxlength="3" required>
                                                                
                                                            </div>
                                                   
                                                </div>
                                </div>
                                   
                                    <div class="col-lg-3">
                                                <br>
                                                <a href="javascript:;" class="btn btn-primary waves-effect waves-light btn-block mt-2" id="btn_consultar"><i class="fa fa-search"></i> Consultar</a>
                                                
                                               
                                    </div>
                                </div>            
                            </div>
                        </div>
                        <!--end toolbar -->             
                    </div>                
                    
                    <div class="btn-toolbar p-3 text-center" role="toolbar" style="margin-top:-30px;" id="toolbar">

                                            <button type="button" id="btn_envcaptura" class="btn btn-success waves-light waves-effect"><i class="mdi mdi-database-check"></i> Autorizar C.D.I. y Enviar a Captura</button>

                                            <button type="button" class="btn btn-danger waves-light waves-effect ml-2" id="btn_noprocede"><i class="fa fa-ban"></i> C.D.I. no Procede</button>                                       
                                            

                                    </div>
                                    <!--end toolbar -->
                                    
                    <!-- begin tabs -->
                    <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                <center><span id="statustram" class="text-medium"> Estatus del Trámite: </span></center><br>
                                
                                    <h4 class="card-title">Consulta de Datos del Cambio de Inscripción </h4>
                                    
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-pills nav-justified pt-3" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#generales" role="tab">
                                                <span class="d-none d-sm-block "><i class="bx bx-info-circle"></i> Generales</span>
                                            </a>
                                        </li>
                        
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#documentacion" role="tab">
                                                <span class="d-block d-sm-none"></span>
                                                <span class="d-none d-sm-block "><i class="bx bx-folder"></i> Documentación del Trámite</span>
                                            </a>
                                        </li>
                                       
                                       
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content p-0 text-muted">
                                        <div class="tab-pane active border p-2" id="generales" role="tabpanel">
                                           <!-- form de generales -->
                                           <h4 class="card-title">Información del Solicitante</h4>
                                           <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group position-relative">
                                                    <label for="solicitante">Nombre del Solicitante:</label>
                                                    <input type="text" class="form-control border-0" id="solicitante" name="solicitante" required>
                                                </div>
                                            </div>
                                        </div>
                                          <div class="row">
                                            <div class="col-md-6 border-right">
                                                <div class="form-group position-relative">
                                                    <label for="prop_ant">Propietario Anterior:</label>
                                                    
                                                    <input type="text" class="form-control border-0" id="prop_ant" name="prop_ant">
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="prop_act">Propietario Actual:</label>
                                                   
                                                    <input type="text" class="form-control border-0"  id="prop_act" name="prop_act">
                                                    
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="ubi_pred">Ubicación del Predio:</label>
                                                    <input type="text" class="form-control border-0" id="ubi_pred"  name="ubi_pred" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="num_of">Numero Oficial:</label>
                                                    <input type="text" class="form-control border-0" id="num_of" name="num_of" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="telefono">Telefono:</label>
                                                    <input type="text" class="form-control border-0" id="telefono"  name="telefono" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="dom_not">Domicilio de Notificacion:</label>
                                                    <input type="text" class="form-control border-0" id="dom_not" name="dom_not" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="concepto">Concepto:</label>
                                                    <input type="text" class="form-control border-0" id="concepto" name="concepto" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="tipo_mov">Tipo de Movimiento:</label>
                                                    <input type="text" class="form-control border-0" id="tipo_mov" name="tipo_mov" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="observaciones">Observaciones:</label>
                                                    <textarea rows="4" class="form-control border-0" id="observaciones" name="observaciones" required></textarea>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip03">Atendió:</label>
                                                    <input type="text" class="form-control border-0" id="atendio" name="atendio" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="notario_num">Número de Notario:</label>
                                                    <input type="text" class="form-control border-0" id="num_notario" name="num_notario" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="notario_nom">Nombre de Notario:</label>
                                                    <input type="text" class="form-control border-0" id="notario_nom" name="notario_nom">
                                                </div>
                                                
                                                <div class="form-group position-relative">
                                                    <label for="titulo">Titulo Traslativo:</label>
                                                    <textarea rows="5" class="form-control border-0" id="titulo" name="titulo"></textarea>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="cancelacioncdi">Cancelacion o Cambio de Inscripcion:</label>
                                                    <textarea rows="5" class="form-control border-0" id="cancelacioncdi" name="cancelacioncdi"></textarea>
                                            </div>
                                           
                                        </div>
                                            
                                        </div>
                                         
                                        </div>


                                        <div class="tab-pane" id="documentacion" role="tabpanel">
                                            <div class="row border p-3" id="files">
                                              
                                            </div>

                                            <hr>
                                            <div class="col-md-12">
                                            <div class="form-group position-relative">
                                            <label for="validationTooltip02">Adjuntar Documentos Adicionales:</label>
                                            
                                            <div class="custom-file">
                                            <form id="form_archivos">
                                                <input type="file" class="custom-file-input" id="archivos[]" name="archivos[]" multiple accept="pdf">
                                                <label class="custom-file-label text-truncate" for="customFile" data-browse="Buscar...">Seleccionar Archivos</label>
                                                </form>
                                            </div>
                                               
                                                <button type="button" class="btn btn-success mt-2" id="btn_subir">Adjuntar Archivos</button>
                                                                                                
                                            </div>
                                        </div>
                                       
                                        
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end tabs -->
                     
                     
                     </div>
                     <!-- end row -->

                     <div class="modal fade" id="modal-doc">
                        <div class="modal-dialog modal-xl" role="document">
                            <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                                <object type="application/pdf" data="path/to/pdf" width="100%" height="800" style="height: 85vh;" id="objdata">No Support</object>
                            </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->



 


                    

                   