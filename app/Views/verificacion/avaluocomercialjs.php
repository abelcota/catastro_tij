<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>


    <!-- Required datatable js -->
    <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
     <!-- Buttons examples -->
     <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
       
   
       <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
       <!-- webcam -->
       <script src="<?php echo base_url("assets/js/webcam.min.js");?>"></script>
   
       <!-- App js -->
       <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
       <script src="<?php echo base_url("/assets/js/jquery.validate.js");?>"></script>
       <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery-ui-1.12.1.js"); ?>"></script>
       <script src="<?php echo base_url("assets/js/peritos/utils.js")."?v".rand(); ?>" ></script>


<script>
  //variables glonales
  var editindx = 0;
  var editrowindx = 0;
  var valor = 0.00;
  var editando = 0;
  var cat_const_list;

  var valTotT_ws = 0.0;
  
  $.getJSON("<?php echo base_url("peritos/consulta_cat_const"); ?>", function(json){ cat_const_list=json.data; });

  var estatusEdoConservCalidadProy = ['BUENO', 'REGULAR', 'MALO'];
  var orientaciones = ['Norte', 'Sur', 'Este', 'Oeste', 'Noroeste', 'Suroeste', 'Noreste', 'Sureste', 'Oriente', 'Poniente', 'Arriba', 'Abajo'];
  var avaluo_id = 0;
  $(document).ready(function() {

    $("#divRevision :input").prop("disabled", true);
                $("#divRevision :input").addClass("number-references");
                $("#divRevision :input").css("background-color", "#F0F0F0");
                $("#divRevision :input").css("color", "#480912");
                $("#divRevision :input").css("font-weight", "550");
    $("#map :input").prop("disabled", false);
   
    var info = $('#txtInfCat').val();

    $("#txtEstadoConservacion").focus(function() { $("#txtEstadoConservacion").keydown(); });
    $("#txtEstadoConservacion").autocomplete({ source: estatusEdoConservCalidadProy, minLength: 0 });
    $("#txtCalidadProyecto").focus(function() { $("#txtCalidadProyecto").keydown(); });
    $("#txtCalidadProyecto").autocomplete({ source: estatusEdoConservCalidadProy, minLength: 0 });
    $("#txtSupEscrituras").change(function() { if ($("#txtSupEscrituras").val() != $("#txtSupConst").val()) { $("#txtSupEscrituras")[0].style.backgroundColor = "#e6b8b8"; } else { $("#txtSupEscrituras")[0].style.backgroundColor = "white" } });

    //cargar la informacion de ese folio
    cargar_info(info);
    cargar_portada(info);
    cargar_antecedentes(info);
    cargar_car_urb(info);
    cargar_car_terreno(info);
    cargar_descripcion_general(info);
    cargar_elementos_construccion(info);
    cargar_sello(info);

    $("#selectOrientacion").focus(function() { $("#selectOrientacion").keydown(); });
    $("#selectOrientacion").autocomplete({ source: orientaciones, minLength: 0 });
    cargar_combo("clasificacion_zonas", "get_clasificacion_zona");
    cargar_combo("construccion_dominante", "get_construccion_dominante");
    cargar_combo("saturacion_zona", "get_saturacion_zona");
    cargar_combo("list_poblacion", "get_poblacion");
    cargar_combo("suelo_permitido", "get_suelo_permitido");

    cargar_combo_usos();
    cargar_combo("list_especiales", "get_list_especiales");
    cargar_combo("list_fachadas", "get_list_fachadas");
    cargar_combo("list_cerrajeria", "get_list_cerrajeria");
    cargar_combo("list_vidrieria", "get_list_vidrieria");
    cargar_combo("list_herreria", "get_list_herreria");
    cargar_combo("list_instalacionese", "get_list_instalacionese");
    cargar_combo("list_mueblesb", "get_list_mueblesb");
    cargar_combo("list_puertas", "get_list_puertas");
    cargar_combo("list_pintura", "get_list_pintura");
    cargar_combo("list_pisos", "get_list_pisos");
    cargar_combo("list_aplanados", "get_list_aplanados");
    cargar_combo("list_techos", "get_list_techos");
    cargar_combo("list_muros", "get_list_muros");
    cargar_combo("list_lambrines", "get_list_lambrines");
    cargar_combo("list_cimientos", "get_list_cimientos");

  });

  var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });

  //funciones

  function cargar_construcciones_fisico(data){
    var suptotalconst = 0.0;
    var valortotalconst = 0.0;
    $.each(data, function(i, obj) {
        let sup = parseFloat(obj.superficie);
        let valneto = parseFloat(obj.valor_construccion.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        $('#tablaFisicoConstrucciones > tbody').append('<tr><td>'+obj.categoria+'</td><td>'+sup+'</td><td>'+obj.valor_unitario+'</td><td>'+obj.estado+'</td><td>'+obj.edad+'</td><td>'+obj.fact_dem+'</td><td>'+obj.fact_com+'</td><td>'+obj.valor_construccion+'</td><td></td></tr>');
        $('#tablaConstrucciones3 > tbody').append('<tr><td>'+obj.categoria+'</td><td>'+sup+'</td></tr>');
        suptotalconst += sup;
        valortotalconst += valneto;
      });
      $('#txtsuptotalconst').text(suptotalconst.toFixed(2));
      $('#txtvalortotalconst').text(aPesos(valortotalconst));

      
  }

  function cargar_especiales_fisico(data){
    var valortotal = 0.0;
    $.each(data, function(i, obj) {
        let valparcial = parseFloat(obj.valor_parcial.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        $('#tablaEspeciales > tbody').append('<tr><td>'+obj.concepto+'</td><td>'+obj.unidad+'</td><td>'+obj.cantidad+'</td><td>'+obj.valor_unit+'</td><td>'+obj.fact_dem+'</td><td>'+obj.vrn_unitario+'</td><td>'+obj.valor_parcial+'</td><td></td></tr>');
        valortotal += valparcial;
      });
      $('#txtvalortotalespeciales').text(aPesos(valortotal));

     
  }

  function cargar_const_anteriores(cvecat){
    var suptotalconst = 0.0;
    var valortotalconst = 0.0;
    $.ajax({
      url: "<?php echo base_url("peritos/consulta_construcciones"); ?>/"+ cvecat,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        for(var i in response.data){
          var obj = response.data[i];
          let sup = parseFloat(obj.SUP_CONST);
          let val = parseFloat(obj.valorunitario);
          let valneto = parseFloat(obj.valorneto);
          $('#tablaConstrucciones2 > tbody').append('<tr><td>'+obj.CVE_CAT_CO+'</td><td>'+sup+'</td></tr>');
          suptotalconst += sup;
          valortotalconst += valneto;
        }

        $('#supConstRev1').text(suptotalconst.toFixed(2));
        $('#valConstRev1').text(aPesos(valortotalconst));


        calculaTotalAnteriores();
      }
    });
  }

  function cargar_construcciones(cvecat){
    var suptotalconst = 0.0;
    var valortotalconst = 0.0;
    $.ajax({
      url: "<?php echo base_url("peritos/consulta_construcciones"); ?>/"+ cvecat,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        for(var i in response.data){
          var obj = response.data[i];
          let sup = parseFloat(obj.SUP_CONST);
          let val = parseFloat(obj.valorunitario);
          let valneto = parseFloat(obj.valorneto);
          $('#tablaFisicoConstrucciones > tbody').append('<tr><td>'+obj.CVE_CAT_CO+'</td><td>'+sup+'</td><td>'+aPesos(val)+'</td><td>'+obj.CVE_EDO_CO+'</td><td>'+obj.EDD_CONST+'</td><td>'+obj.factordemerito+'</td><td>'+obj.factorcomercializacion+'</td><td>'+aPesos(valneto)+'</td><td></td></tr>');
          $('#tablaConstrucciones3 > tbody').append('<tr><td>'+obj.CVE_CAT_CO+'</td><td>'+sup+'</td></tr>');
          suptotalconst += sup;
          valortotalconst += valneto;
        }

        $('#txtsuptotalconst').text(suptotalconst.toFixed(2));
        $('#txtvalortotalconst').text(aPesos(valortotalconst));
        calculaTotal();
        
      }
    });
   
    
  }

  function cargar_avaluo_fisico(){
    //TODO:
  }

  function cargar_terreno_fisico(data){
    console.log(data);
    suptotalterreno = 0.0;
    valortotalterreno = 0.0;
    $.each(data, function(i, obj) {
        suptotalterreno += parseFloat(obj.superficie);
        valortotalterreno += parseFloat(obj.valor_terreno.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        $('#tablaFisicoTerreno > tbody').append('<tr><td>'+obj.superficie+'</td><td>'+obj.valor_unitario+'</td><td>'+obj.factor_demerito+'</td><td>'+obj.motivo+'</td><td>'+obj.valor_terreno+'</td></tr>');
      });
    $('#txtsuptotalterreno').text(suptotalterreno.toFixed(2));
    $('#txtvalortotalterreno').text(aPesos(valortotalterreno));
   
  }

  
  function cargar_terreno_anteriores(cvecat)
  {
    var suptotalterreno = 0.0;
    var valortotalterreno = 0.0;
    $.ajax({
      url: "<?php echo base_url("peritos/consulta_terreno"); ?>/"+ cvecat,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        for(var i in response.data){
          var obj = response.data[i];
          let sup = parseFloat(obj.SUP_TERR);
          let val = parseFloat(obj.valorunitario1);
          let valneto = parseFloat(obj.valorneto);
          suptotalterreno += sup;
          valortotalterreno += valneto;
        }
        $('#supTerrRev1').text(suptotalterreno.toFixed(2));
        $('#valTerrRev1').text(aPesos(valortotalterreno));
        calculaTotalAnteriores();
      }
    });
  }

  function cargar_terreno(cvecat){
    var suptotalterreno = 0.0;
    var valortotalterreno = 0.0;
    $.ajax({
      url: "<?php echo base_url("peritos/consulta_terreno"); ?>/"+ cvecat,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        for(var i in response.data){
          var obj = response.data[i];
          let sup = parseFloat(obj.SUP_TERR);
          let val = parseFloat(obj.valorunitario1);
          let valneto = parseFloat(obj.valorneto);
          $('#tablaFisicoTerreno > tbody').append('<tr><td>'+sup+'</td><td>'+aPesos(val)+'</td><td>'+obj.FAC_DEM_TE+'</td><td></td><td>'+aPesos(valneto)+'</td></tr>');
          suptotalterreno += sup;
          valortotalterreno += valneto;
        }
        $('#txtsuptotalterreno').text(suptotalterreno.toFixed(2));
        $('#txtvalortotalterreno').text(aPesos(valortotalterreno));
        calculaTotal();
      }
    });
    
              
    
  }


  function cargar_portada(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_portada"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.portada.length > 0) {
          var json = response.portada[0];
          $("#txtInmueble").val(json.inmueble);
          $("#txtCP").val(json.cp);
          $("#txtCalle").val(json.calle);
          $("#txtNumero").val(json.numero);
          $("#txtColonia").val(json.colonia);
          $("#txtMunicipio").val(json.municipio);
          $("#txtNotaInterna").val(json.nota_interna);
          $("#labelNotaInterna").text(json.nota_interna); 
          //$("#txtCalleCVE").val();
          //$("#txtColoniaCVE").val();
        }else {
          $("#portada_ok").hide();
        }
      }
    });
  }

  function cargar_antecedentes(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_antecedentes"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.antecedentes.length > 0) {
          var json = response.antecedentes[0];
          $("#txtSolicitante").val(json.solicitante);
          $("#txtFechaInspeccion").val(moment(json.fecha_inspeccion).format('YYYY-MM-DD'));
          $("#ContentPlaceHolder1_txtRegimen").val(json.regimen);
          $("#selObjectoAvaluo").val(json.objeto);
          $("#txtRegPubPro").val(json.rpp);
          $("#txtInscripcion").val(json.inscripcion);
          $("#txtLibro").val(json.libro);
          $("#txtSeccion").val(json.seccion);
        }else {
          $("#antecedentes_ok").hide();
        }
      }
    });
  }

  function cargar_car_urb(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_car_urb"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.caracteristicas_urbanas.length > 0) {
         
          var json = response.caracteristicas_urbanas[0];

          $("#clasificacion_zonas").val(json.clasificacionzona);
          $("#construccion_dominante").val(json.tipoconstdominante);
          $("#saturacion_zona").val(json.indicesatzona);
          $("#list_poblacion").val(json.poblacion);
          $("#txtContaminacionAmbiental").val(json.contaminacionambiental);
          $("#suelo_permitido").val(json.usosuelopermitido);
          $("#txtViaAccesoImportancia").val(json.viaaccesoimportancia);

          $("#chkAgua")[0].checked = stringToBoolean(json.agua);
          $("#chkDrenaje")[0].checked = stringToBoolean(json.drenaje);
          $("#chkElectricidad")[0].checked = stringToBoolean(json.energia);
          $("#chkAlumbrado")[0].checked = stringToBoolean(json.alumbrado);
          $("#chkBasura")[0].checked = stringToBoolean(json.basura);
          $("#chkTransporte")[0].checked = stringToBoolean(json.transporte);
          $("#chkTelefono")[0].checked = stringToBoolean(json.telefono);
          $("#chkBanqueta")[0].checked = stringToBoolean(json.banqueta);
          $("#chkGuarniciones")[0].checked = stringToBoolean(json.guarniciones);
          $("#chkPavimento")[0].checked = stringToBoolean(json.pavimento);
          $("#chkVigilancia")[0].checked = stringToBoolean(json.vigilancia);

        }else {
          $("#car_urb_ok").hide();
        }
      }
    });
  }

  function cargar_car_terreno(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_car_terreno"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.caracteristicas_terreno.length > 0) {
          
          var json = response.caracteristicas_terreno[0];

          $("#txtTopografiaConst").val(json.topografia_const);
          $("#txtNumeroFrentes").val(json.numero_frentes);
          $("#txtCaracteristicasPan").val(json.car_panoramicas);
          $("#txtDensidadHabitacional").val(json.densidad_habitacional);
          $("#txtIntensidadConstruccion").val(json.intensidad_const);
          $("#txtServidumbresConstrucciones").val(json.servidumbre_restricciones);
          $("#txtSupEscrituras").val(json.sup_escrituras);
          $("#txtSupLevantamiento").val(json.sup_topografico);

        }else {
          $("#car_terr_ok").hide();
        }
      }
    });
  }

  function cargar_sello(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_autorizacion_perito"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log("SELLO" + response);
        if (response.autorizacion.length > 0) {
          var json = response.autorizacion[0];
          $("#txtRFCPer").val(json.rfc_certificado);
          $("#txtSerie").val(json.serie_certificado);
          $("#txtSello").val(json.sello_digital);
          
        }else {
        
        }
      }
    });
  }

  function cargar_valor_mercado(info){
    //TODO
  }

  function cargar_descripcion_general(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_descripcion_general"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.descripcion_general.length > 0) {

          var json = response.descripcion_general[0];

          $("#selUso").val(json.cve_uso);
          $("#txtEdadAprox").val(json.edad_aprox);
          $("#txtNumeroNiveles").val(json.num_niveles);
          $("#txtEstadoConservacion").val(json.estado_conservacion);
          $("#txtCalidadProyecto").val(json.calidad_proyecto);
          $("#txtUnidadesRentables").val(json.unidades_rentables);
          $("#txtDescGeneralInmueble").val(json.descripcion_general);
        }else {
          $("#desc_gen_ok").hide();
        }
      }
    });
  }

  function cargar_elementos_construccion(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_elementos_construccion"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.elementos_construccion.length > 0) {
          var json = response.elementos_construccion[0];

          $("#list_cimientos").val(json.a_cimientos);
          $("#txtEstructura").val(json.a_estructura);
          $("#list_muros").val(json.a_muros);
          $("#txtEntrepisos").val(json.a_entrepisos);
          $("#list_techos").val(json.a_techos);
          $("#txtAzoteas").val(json.a_azoteas);
          $("#txtBardas").val(json.a_bardas); 
          $("#list_aplanados").val(json.b_aplanadosinteriores);
          $("#txtAplanadosExteriores").val(json.b_aplanadosexteriores);
          $("#txtPlafones").val(json.b_plafones);
          $("#list_lambrines").val(json.b_lambrines);
          $("#list_pisos").val(json.b_pisos);
          $("#txtZoclos").val(json.b_zoclos);
          $("#txtEscaleras").val(json.b_escaleras);
          $("#list_pintura").val(json.b_pintura);
          $("#txtRecubrimientosEspeciales").val(json.b_recubrimientosespeciales);
          $("#list_puertas").val(json.c_puertas);
          $("#txtClosets").val(json.c_closets);
          $("#list_mueblesb").val(json.d_mueblesbano);
          $("#txtEquipoCocina").val(json.d_equipodecocina);
          $("#list_instalacionese").val(json.e_instalacioneselectricas);
          $("#list_herreria").val(json.f_herreria);
          $("#list_vidrieria").val(json.g_vidrieria);
          $("#list_cerrajeria").val(json.h_cerrajeria);
          $("#list_fachadas").val(json.i_fachadas);
          $("#list_especiales").val(json.j_instalacionesespeciales);

        }else {
          $("#elem_const_ok").hide();
        }
      }
    });
  }
  

  function cargar_info(info) {
    var cve_cat = "";
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_info"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      beforeSend : function (){
        $.blockUI({ 
          fadeIn : 0,
          fadeOut : 0,
          showOverlay : false,
          message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Cargando Información...</h3><br><h5>Espere un momento por favor</h5>', 
          css: {
              backgroundColor: 'white',
              border: '1', 
          }, 
        });
      },
      success: function(response) {
        console.log(response);
       
        if (response.info.length > 0) {
          avaluo_id = response.info[0].id;
          perito_id = response.info[0].id_perito;
          cargar_info_perito(perito_id);
          $('#labelRevision').text(response.detalle[0].en_revision);
          $('#labelReIngreso').text(response.detalle[0].reingreso);
          //Clave catastral compuesta
          $('#MUNI').val(zeroPad(response.info[0].cve_mpio, 3));
          $('#POBL').val(zeroPad(response.info[0].cve_pobl, 3));
          $('#CTEL').val(zeroPad(response.info[0].num_ctel, 3));
          $('#MANZ').val(zeroPad(response.info[0].num_manz, 3));
          $('#PRED').val(zeroPad(response.info[0].num_pred, 3));
          $('#UNID').val(zeroPad(response.info[0].num_unid, 3));
          //fechas
          if (response.info[0].ultima_modificacion != null)
            $("#txtFecha").val(moment(response.info[0].ultima_modificacion).locale('es').format('L LTS'));
          if (response.info[0].fecha_autorizacion != null)
            $("#txtFechaModificacion").val(moment(response.info[0].fecha_autorizacion).locale('es').format('L LTS'));
          
          var pobl = $('#POBL').val();
          var ctel = $('#CTEL').val();
          var manz = $('#MANZ').val();
          var pred = $('#PRED').val();
          var unid = $('#UNID').val();

          var cvecat = pobl + ctel + manz + pred + unid;
          //cargamos informacion de la clave catastral 
          $.ajax({
            url: "<?php echo base_url("peritos/validarcve"); ?>/" + cvecat,
            dataType: "json",
            success: function(data) {
              console.log(data.result);
              if (data.result.length > 0) {
                if (data.result[0].ID_REGTO == "B") {
                  Swal.fire({
                    icon: 'error',
                    title: 'Clave dada de Baja...'
                  })
                  //$("#btn_limpiar").click();
                } else {
                  //llenamos los datos correspondientes 
                  var prop = "";
                  if(data.result[0].NOM_PROP != null) { prop = prop + data.result[0].NOM_PROP; }
                  if(data.result[0].APE_PAT != null) { prop = prop + " " +data.result[0].APE_PAT; }
                  if(data.result[0].APE_MAT != null) { prop = prop + " " +data.result[0].APE_MAT; }
                  $("#txtPropietario").val(prop);

                  if (data.result[0].TIP_PERS === 'F')
                    $("#txtTipoPersona").val("Física");
                  else if(data.result[0].TIP_PERS === 'F')
                    $("#txtTipoPersona").val("Moral");
                  else
                    $("#txtTipoPersona").val("");

                  $("#txtCalleCVE").val(data.result[0].CVE_CALLE);
                  $("#txtCalle").val(data.result[0].CALLE);
                  $("#txtColoniaCVE").val(data.result[0].CVE_COL);
                  $("#txtColonia").val(data.result[0].COLONIA);
                  $("#txtSupConst").val(data.result[0].SUP_TERR);

                  valTotT_ws = parseFloat(data.result[0].VAL_TERR);

                  cargar_tabla_orientaciones();

                  if(response.plano.length > 0) {
                    var url_base = "<?php echo base_url("documentos_peritos/planos"); ?>/"+info+"/";
                    $("#aPlano").prop('href', url_base + response.plano[0].url);
                    $("#aVerPlano").prop('href', url_base + response.plano[0].url);
                    $("#aAfecta").text("Si");
                  }
                  else{
                    $("#aAfecta").text("No");
                  }

                  if (response.Terr.length > 0) {
                    cargar_terreno_fisico(response.Terr);
                  } else { cargar_terreno(cvecat); }
                  
                  if (response.Const.length > 0) {
                    cargar_construcciones_fisico(response.Const);
                  } else { cargar_construcciones(cvecat); }

                  if (response.Esp.length > 0) {
                    cargar_especiales_fisico(response.Esp);
                  }
                  else{
                    $("#tablaEspeciales")[0].addEventListener("dblclick", function(e) { if (e.target && e.target.nodeName == "TD" && e.target.cellIndex < 5) { activaEditarEspeciales(e.target.cellIndex, e.target); } });
                  }

                  cargar_terreno_anteriores(cvecat);
                  cargar_const_anteriores(cvecat);
                  //cargar_terreno(cvecat);
                  //cargar_construcciones(cvecat);

                  cargar_categorias_construccion(cvecat);

                  if (response.detalle.length > 0) {
                    if(response.detalle[0].valor_mercado != null)
                    { 
                      $("#txtValorMercado").val(aPesos(parseFloat(response.detalle[0].valor_mercado))); 
                      $("#txtValorFisico").val(aPesos(parseFloat(response.detalle[0].valor_mercado))); 
                    }
                    if(response.detalle[0].consideraciones_previas != null)
                    { 
                      $("#txtConsideraciones").val(response.detalle[0].consideraciones_previas);
                      $("#labelConsideraciones").text(response.detalle[0].consideraciones_previas); 
                    }
                  }
 
                  cargarImagen();
                  calculaTotal();
                  //Se obtiene el array del poligono 
                  jsonobj = jQuery.parseJSON(data.result[0].jsonb_build_object);
                  //si encuentra el poligono en el vectorial
                  if (jsonobj.features != null) {
                    //Se visualiza el poligono en el mapa
                    visualizar_capa(jsonobj);
                  } else {
                    Swal.fire({
                      icon: 'error',
                      title: 'Vectorial no encontrado',
                      text: 'La clave catastral no se encontró en el vectorial!'

                    })
                  }
                  
                }

              } else {

                //verificar si se encuentra de primer registro
                Swal.fire({
                  title: 'Clave Catastral no encontrada',
                  icon: 'error',
                  showCancelButton: true,
                  cancelButtonText: 'Notificar',
                  confirmButtonColor: '#00b300',
                  cancelButtonColor: '#0000FF',
                  confirmButtonText: 'Primer Registro'
                }).then((result) => {
                  if (result.value) {

                  } else {

                  }
                })


              }
            }
          });
        }
      },
      complete : function (){
        $.unblockUI();
      }
    });

  }

  function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
  }

  function cargar_combo(control, url) {
      $("#" + control).focus(function() { $("#" + control).keydown(); });
      $("#" + control).autocomplete({ source: function(request, response) { $.ajax({ type: "POST", contentType: "application/json; charset=utf-8", url: "<?php echo base_url("peritos/"); ?>/" + url, dataType: "json", success: function(data) { response($.map( data.result, function( item ) {
                return {
                    label: item.desc,
                    value: item.desc
                }
            }));
       }, error: function(result) {} }); }, minLength: 0 });
  }

  function cargar_combo_usos() {
    $('#selUso').empty();
    $.getJSON("<?php echo base_url("peritos/grupo_usos"); ?>", function(json) {
      $.each(json.results, function(i, obj) {
        $('#selUso').append($('<option>').text(obj.DES_GPO_US).attr({
          'value': obj.CVE_GPO_US
        }));
      });
    });
  }

  function cargar_categorias_construccion(cvecat) { //mostramos la tabla con las categorias de construccion
    var urlconst = "<?php echo base_url("peritos/consulta_construcciones/"); ?>/" + cvecat;
    var html = "";
    $.getJSON(urlconst, function(json) {
      $('#tablaConstrucciones').empty();
      html += '<thead class="text-white" style="background-color: #480912;"><th>Categoria</th><th>Superficie M2</th><th>Valor Unitario</th></thead>';
      $.each(json.data, function(i, obj) {
        html += '<tr><td>' + obj.CVE_CAT_CO + '</td><td>' + obj.SUP_CONST + '</td><td>' + formatter.format(obj.valorunitario) + '</td></tr>';
      });

      $('#tablaConstrucciones').append(html);
    });
  }

  function cargar_tabla_orientaciones() {
    var url = "<?php echo base_url("peritos/obtener_orientaciones/"); ?>/<?php echo $info; ?>";
    var html = "";
    $.getJSON(url, function(json) {
      $('#tablaOrientacion').empty();
      html += '<thead class="text-white" style="background-color: #480912;"><th>Orientación</th><th>Descripción</th><th></th></thead>';
      $.each(json.orientaciones, function(i, obj) {
        html += '<tr><td>' + obj.orientacion + '</td><td>' + obj.descripcion + '</td></tr>';
      });

      $('#tablaOrientacion').append(html);
    });
  }
  
  $(document).on('click', '#botonImprimirAvaluo', function(e) {
        var inf = "<?php echo $info; ?>";
        var url = "<?php echo base_url("peritos/imprimiravaluo"); ?>/" + inf;
        window.open(url,'popup','width=1200,height=800,scrollbars=yes,resizable=yes');
        return false;
  });


  $("input[data-type='currency']").on({ keyup: function() { formatCurrency($(this)); }, blur: function() { formatCurrency($(this), "blur"); } });

 

  //utilerias
  function stringToBoolean(string){
    switch (string.toLowerCase().trim()) {
      case "t":
        //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor1
        return true;
        break;
      case "f":
        //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor2
        return false;
        break;
      default:
        return false;
    }
  }


 
function crearComboCategoriaMpio(va) {
    var ht = '<select class="form-control selCatMpio">';
    for (var i = 0; i < cat_const_list.length; i++) { if (va != cat_const_list[0].CVE_CAT_CONST) { ht = ht + '<option value="' + cat_const_list[i].CVE_CAT_CONST + '" >' + cat_const_list[i].CVE_CAT_CONST + '</option>'; } else { ht = ht + '<option value="' + cat_const_list[i].CVE_CAT_CONST + '" selected="selected">' + cat_const_list[i].CVE_CAT_CONST + '</option>'; } }
    ht = ht + '<select class="form-control">';
    return ht;
}

function calculaTotalAnteriores(){

    var val_terr = parseFloat($("#valTerrRev1").text().replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
    var val_cons = parseFloat($("#valConstRev1").text().replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));

    var suma = val_terr + val_cons;

    $("#valTOTALRev1").text(aPesos(parseFloat(suma)));
}


function calculaTotal() {
    //acumuladores
    var sum = 0.0;
    var sumTerr = 0.0;
    var valTot = 0.00;
    var sumConst = 0.0;
    var sumFisico = 0.0;
    
    //se recorren cada una de las filas del terreno
    for (var i = 2; i < $("#tablaFisicoTerreno")[0].rows.length -1; i++) {
        //obtenemos el valor del terreno
        sum += parseFloat($("#tablaFisicoTerreno")[0].rows[i].cells[4].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        sumTerr += parseFloat($("#tablaFisicoTerreno")[0].rows[i].cells[0].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));;
        valTot += parseFloat($("#tablaFisicoTerreno")[0].rows[i].cells[4].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
    }
    //si el valor del terreno es menor que el valor comercial segun catastro mandamos una alerta
    if (valTot < valTotT_ws) { //Swal.fire({ icon: 'info', text: 'El valor comercial del terreno no puede ser menor que el valor catastral'}) 
    }
    //se actualizan los controles
    $("#txtvalortotalterreno").text(aPesos(parseFloat(sum)));
    $("#txtsuptotalterreno").text(parseFloat(sumTerr).toFixed(2));
    $("#supTerrRev2").text(parseFloat(sumTerr).toFixed(2));
    $("#valTerrRev2").text($("#txtvalortotalterreno").text());
    
    sumFisico = sumFisico + sum;
    
    //se recorren cada una de las filas de
    sum = 0.0;
    for (var i = 2; i < $("#tablaFisicoConstrucciones")[0].rows.length -1; i++) {
        sum = sum + parseFloat($("#tablaFisicoConstrucciones")[0].rows[i].cells[7].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        sumConst = sumConst + parseFloat($("#tablaFisicoConstrucciones")[0].rows[i].cells[1].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
    }
    $("#txtvalortotalconst").text(aPesos(parseFloat(sum)));
    $("#txtsuptotalconst").text(parseFloat(sumConst).toFixed(2));
    $("#supConstRev2").text(parseFloat(sumConst).toFixed(2));
    $("#valConstRev2").text($("#txtvalortotalconst").text());
    sumFisico = sumFisico + sum;
    
    //se recorren cada una de las filas del terreno
    sum = 0.0;
    for (var i = 2; i < $("#tablaEspeciales")[0].rows.length -1; i++) { 
      sum = sum + parseFloat($("#tablaEspeciales")[0].rows[i].cells[6].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", "")); 
    }
    $("#txtvalortotalespeciales").text(aPesos(parseFloat(sum)));

    sumFisico = sumFisico + sum;

    console.log("Sum fisico: " + sumFisico);

    $("#txtValorFisico").val(aPesos(parseFloat(sumFisico)));
    $("#txtValorFisico2").val(aPesos(parseFloat(sumFisico)));
    $("#txtValorMercado").val(aPesos(parseFloat(sumFisico)));
    $("#valTOTALRev2").text(aPesos(parseFloat(sumFisico)));
}

function sumatotal(){
    var sumFisico = 0.0;
    sumFisico += parseFloat($("#txtvalortotalterreno").text().replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
    sumFisico += parseFloat($("#txtvalortotalconst").text().replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
    sumFisico += parseFloat($("#txtvalortotalespeciales").text().replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));

    $("#txtValorFisico").val(aPesos(parseFloat(sumFisico)));
    $("#txtValorFisico2").val(aPesos(parseFloat(sumFisico)));
    $("#txtValorMercado").val(aPesos(parseFloat(sumFisico)));
    $("#valTOTALRev2").text(aPesos(parseFloat(sumFisico)));
}

function cargarImagen() {
    var inf = "<?php echo $info; ?>";
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_imagenes"); ?>/" + inf,
      dataType: "json",
      success: function(data) {
        console.log(data.result);
        if (data.result.length > 0) {
          var info1 = eval(data.result);
            var imHtml = "";
            var imHtmlRev = "";
            $("#divImages").html("");
            $("#divImagesRevision").html("");
            var img = true;
            for (var i = 0; i < info1.length; i++) {
                imHtml = imHtml + "<div class='col-md-6 '><div class=''><img class='imgAv img-thumbnail' src='<?php echo base_url("documentos_peritos/img_reporte_fotografico");?>/"+inf+ "/"+ info1[i].url + "' alt='...'><div class='caption'><div class='input-group '><input id='img_" + info1[i].id_imagen + "' type='text' class='form-control' placeholder='...' value='" + info1[i].descripcion + "' disabled> </div></div></div></div>";
                imHtmlRev = imHtmlRev + "<div class='col-md-4 '><img class='imgAv img-thumbnail' src='<?php echo base_url("documentos_peritos/img_reporte_fotografico");?>/"+inf+"/"+info1[i].url + "' alt='...'></div>";
            }
            if (img) {             
                $("#imprimir_ok").show();
                /*activarTab($("#liFoto"), true);
                activarTab($("#liReporte"), true);
                activarTab($("#liAutorizar"), true);*/
            } else { 
              $("#reporte_foto_ok").hide();
            }
            $("#divImages").html(imHtml);
            $("#divImagesRevision").html(imHtmlRev);
        }
      }
    });

}

function cargar_info_perito(id) {
  $.ajax({
      url: "<?php echo base_url("peritos/obtener_perito_perfil"); ?>/"+id,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        //console.log(response);
        if (response.perfil.length > 0) {
          var json = response.perfil[0];
            console.log(json);
            $("#labelNomPerito").text(json.nombre);
        }

      }
    });
}

function fimar(depa){
    var avaluo = avaluo_id;
      var certfile = document.getElementById('fileCer'+depa).files[0];
      var certkey = document.getElementById('fileKey'+depa).files[0];
      var passkey = document.getElementById('txtPAssKey'+depa).value;
      //var movimiento = document.getElementById('selMovimiento'+depa).options[index].innerHTML;

      var sel = document.getElementById('selMovimiento'+depa);

      var movimiento= sel.options[sel.selectedIndex].text;

      var mensaje = "|"+movimiento.toUpperCase()+" AVALUO COMERCIAL|<?php echo $info; ?>||";

      console.log(mensaje);

      var formData = new FormData();
      formData.append('certfile', certfile);
      formData.append('certkey', certkey);
      formData.append('passkey', passkey);
      formData.append('mensaje', mensaje);

      $.ajax({
            url: "<?php echo base_url("peritos/firmar"); ?>",
            type: "POST",
            data: formData,
            processData: false,  
            contentType: false,
                success: function (data) {
                  console.log(data);
                  if(data.certificado.length > 0){
                    $('#txtRFC'+depa).val(data.certificado[0].rfc);
                    $('#txtSerie'+depa).val(data.certificado[0].serial);
                    $('#txtSello'+depa).val(data.certificado[0].sello);
                  }
                  
                },
                error: function (jqXHR, status) {
                                Swal.fire({
                                    title: "Error al Firmar",
                                    text: 'Por favor verifique los archivos y contraseña de su firma electrónica',
                                    icon: 'error'
                                })
                            }
        });
}

$(document).on('click', '#btnFirmarAux', function(e) {
  fimar("Aux");
});

$(document).on('click', '#btnFirmarVa2', function(e) {
  fimar("Va2");
});

$(document).on('click', '#btnFirmarVal', function(e) {
  fimar("Val");
});

$(document).on('click', '#btnFirmarEnl', function(e) {
  fimar("Enl");
});

$(document).on('click', '#btnFirmarDir', function(e) {
  fimar("Dir");
});


</script>


