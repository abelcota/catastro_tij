<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>


    <!-- Required datatable js -->
    <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
     <!-- Buttons examples -->
     <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
       
   
       <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
       <!-- webcam -->
       <script src="<?php echo base_url("assets/js/webcam.min.js");?>"></script>
   
       <!-- App js -->
       <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
       <script src="<?php echo base_url("/assets/js/jquery.validate.js");?>"></script>
       <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>

       <script>

            var folios = [];

            $(document).ready(function () {
                var anio = $("#year").val();
                var folio = $("#folio").val();
                $('#toolbar').hide();
                if(anio != "" && folio != ""){
                    $('#btn_consultar').click();
                }
                else{
                    get_anio();
                }
                
              
                $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#PRED').blur(function () { this.value = zeroPad(this.value, 3);  });

            });

            $(document).on( 'click', '#btn_consultar', function () {
                var anio = $("#year").val();
                var folio = $("#folio").val();
                consulta_folio(anio, folio);
            });

            function zeroPad(num, places) {
                var zero = places - num.toString().length + 1;
                return Array(+(zero > 0 && zero)).join("0") + num;
            }

            function consulta_folio(a, folio)
            {
            if (folio == ""){
                    Swal.fire({
                        icon: 'error',
                        text: 'Ingrese un folio válido para continuar',
                        })
                 }
                else{

                get_documentos(a+folio);
                    
                $.ajax({
                url: "<?php echo base_url("verificacion/consultafoliocdi/");?>/"+a+"/"+folio+"",
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Folio...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (datasource) {
                       console.log(datasource.data[0]);
                       //console.log(datasource.status[0]);
                       var data = datasource.data;
                       var status = datasource.status;

                       if(data.length > 0){
                           
                            //CARGAR CALLES Y COLONIAS DEPENDIENDO LA POBLACION
                            
                            //agregar los valores a los controles
                                $("#CTEL").val(data[0].Clave.substr(0, 2));
                                $("#MANZ").val(data[0].Clave.substr(2, 5));
                                $("#PRED").val(data[0].Clave.substr(5, 8));

                                $("#solicitante").val(data[0].NombreSolicitante);

                                if(data[0].prop_ant == null){
                                    $("#prop_ant").val(data[0].PropietarioAnterior);
                                }
                                else{
                                    $("#prop_ant").val("["+data[0].PropietarioAnterior + "] " + data[0].prop_ant );
                                }

                                if(data[0].prop_act == null){
                                    $("#prop_act").val(data[0].PropietarioActual);
                                }
                                else{
                                    $("#prop_act").val("["+data[0].PropietarioActual + "] " + data[0].prop_act );
                                }

                                $("#ubi_pred").val(data[0].DomicilioUbicacion);
                                $("#num_of").val(data[0].NumeroOficial);
                                $("#telefono").val(data[0].TelefonoSolicitante);
                                $("#dom_not").val(data[0].DomicilioNotificacion);
                                $("#concepto").val(data[0].Concepto);
                                if(data[0].TipoB == "X")
                                    $("#tipo_mov").val("Baja");
                                else
                                    $("#tipo_mov").val("Cambio");

                                $("#observaciones").val(data[0].Observaciones); 
                                $("#atendio").val(data[0].Nombre + " " + data[0].ApellidoPaterno);   
                                
                                $("#num_notario").val(data[0].TramiteNotario);

                                if(data[0].nombre == null)
                                    $("#nombre_notario").val("");
                                else
                                    $("#nombre_notario").val(data[0].nombre);
                                
                                $("#titulo").val(data[0].TituloTraslativo);
                                $("#cancelacioncdi").val(data[0].Motivo);

                                if(status.length > 0){
                                    if(status[0].estatus_verificacion == 'V'){
                                        $('#statustram').html("<b>Estatus del Cambio de Inscripcion :</b> <i class=\"text-success\">Verificado y enviado a Captura");
                                        $('#toolbar').hide();
                                    }    
                                    else if(status[0].estatus_verificacion == 'NP'){
                                        $('#statustram').html("<b>Estatus del Cambio de Inscripcion :</b> <i class=\"text-danger\">No procedió");
                                        $('#toolbar').hide();
                                    }

                                }
                                else{
                                    $('#statustram').html("<b>Estatus del Cambio de Inscripcion : </b> <i class=\"text-primary\">No verificado</i> ");
                                    $('#toolbar').show();
                                }
                                    
                                
                       }
                       else {
                           Swal.fire({
                               icon: 'error',
                               title: 'Folio no encontrado',
                               text: 'El folio no se encuentra registrado!'
                               
                           })

                           //limpiar
                           $(':input').val('');
                           get_anio();

                       }
                       
                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });
                    
                    

            }

        }

        function get_anio(){
            var anio = new Date().getFullYear().toString(); 
            var a = anio.substr(2,4);
            $('#year').val(a); 
            return a;
        }

        function get_documentos(cve){
            $('#files').empty();
            var url = "<?php echo base_url("verificacion/cargar_documentacion_cdi/"); ?>/"+cve;
            $.getJSON(url, function(json){
                    //console.log("archivos: " + json['result']);
                    var img = "";
                    $.each(json['result'], function(i, obj){

                            if(obj.ext == 'pdf')
                                img = "<?php echo base_url("assets/images/file_pdf.svg") ?>";
                            else if(obj.ext == 'doc' || obj.text == 'docx')
                                img = "<?php echo base_url("assets/images/file_word.svg") ?>";
                            else if(obj.ext == 'xlsx' || obj.text == 'xls')
                                img = "<?php echo base_url("assets/images/file_excel.svg") ?>";
                            else if (obj.ext == 'jpg' || obj.ext == 'png' || obj.ext == 'gif' || obj.ext == 'jpeg')
                                img = "<?php echo base_url("assets/images/file_img.svg") ?>";
                            else
                                img = "<?php echo base_url("assets/images/file_empty.svg") ?>";

                            var html = ' <div class="col-lg-4 col-xl-3"><div class="file-man-box"><a href="javascript:;" class="del_doc" nombre="'+obj.nombre+'"><i class="bx bx-x-circle text-danger"></i></a><div class="file-img-box"><img src="'+img+'" alt="icon"></div><a href="javascript:;" class="file-download btnShow" url="<?php echo base_url("documentos_cdi"); ?>/'+cve+'/'+obj.nombre+'"><i class="bx bx-cloud-download"></i></a><div class="file-man-title"><h5 class="mb-0 small">'+obj.nombre+'</h5><p class="mb-0"><small>'+parseFloat(obj.size)/1000+' KB</small></p></div> </div></div>';
                            $('#files').append(html);
                    });
            }).fail(function(jqXHR){
        if(jqXHR.status==500 || jqXHR.status==0){
            // internal server error or internet connection broke 
            var html = ' <div class="col-lg-4 col-xl-3"> <div class="file-man-box"><div class="file-img-box"><img src="https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-9/256/file-warning-icon.png" alt="icon"><div class="file-man-title"><h5 class="mb-0 text-overflow">Sin Documentos Cargados</h5></div></div> </div></div>';
                            $('#files').append(html);
        }
    });
        }
         


        $(document).on('click', '.btnShow', function(e){
            e.preventDefault();
            var url = $(this).attr('url');
            window.open(url,'popup','width=800,height=600,scrollbars=no,resizable=no');
            
        });


        $( document ).on('click', '#btn_envcaptura', function(e) {
            //alert('Cancelar tramite');
            var anio = $("#year").val();
            var folio = '000' + $("#folio").val();
            var pobl = $("#POBL").val();
            var ctel = $("#CTEL").val();
            var manz = $("#MANZ").val();
            var pred = $("#PRED").val();
            var unid = $("#UNID").val();

            var cvecat = pobl + ctel + manz + pred + unid;            

            //preguntar si en realidad se desea cancelar el tramite
            Swal.fire({
                title: '¿Seguro de enviar el Cambio de Inscripción al módulo de Captura?',
                text: "!Una vez enviado el C.D.I. se procederá a actualizar el padrón!",
                icon: 'success',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Enviar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        
                            //actualizamos en la base de datos
                            var verificador = "<?php echo $usuario->username; ?>";
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url("verificacion/enviar_a_captura_cdi"); ?>",
                                type: "POST",
                                data: {'id_anio' : anio, 'id_folio': folio, 'cvecat' : cvecat, 'verificador' : verificador},
                                cache: false,
                                success: function(data){

                                    console.log(data);
                                    if (data == 'ok')
                                    {
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'info',
                                                            title: 'Mensaje del Sistema',
                                                            text: 'El folio se envió a captura correctamente'
                                                            
                                                        }).then((result) => {
                                                            //window.close();
                                                            $("#btn_consultar").click();
                                                    })
                                       
                                    }
                                    else{
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'error',
                                                            title: 'Mensaje del Sistema',
                                                            text: 'Ocurrió un error al enviar el folio a captura'
                                                            
                                                        })
                                    }
                                }
                            });
                            
                        
                    }
                })
        });

        
        $( document ).on('click', '.del_doc', function(e) {
            e.preventDefault();
            var a = $("#year").val();
            var fol = $("#folio").val();
            var name = $(this).attr('nombre');
            //alert(name);
            //eliminar_documento
            //preguntar si en realidad se desea cancelar el tramite
            Swal.fire({
                text: '¿Seguro de eliminar el documento '+name+'?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si Eliminar'
                }).then((result) => {
                    if (result.isConfirmed) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url("verificacion/eliminar_documento_cdi"); ?>/"+a+"/"+fol+"/"+name,
                                type: "POST",
                                success: function(data){
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'info',
                                                            title: 'Mensaje del Sistema',
                                                            text: data
                                                            
                                                })
                                        
                                        get_documentos(a+fol);
                                   
                                }
                            });
                    }
                })

        });

        $( document ).on('click', '#btn_subir', function(e) {
            e.preventDefault();
            var a = $("#year").val();
            var fol = $("#folio").val();
            var form = $("#form_archivos")[0];
            var pobl = $("#POBL").val();
            var ctel = $("#CTEL").val();
            var manz = $("#MANZ").val();
            var pred = $("#PRED").val();
            var unid = $("#UNID").val();

            var cve = pobl + ctel + manz + pred + unid;
            // Create an FormData object 
            var data = new FormData(form);
            $.ajax({
            url: "<?php echo base_url("verificacion/subir_documentos_cdi"); ?>/"+a+"/"+fol+"/"+cve,
            enctype: 'multipart/form-data',
            type: "POST",
            data: data,//form.serialize(),
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);          
                },
                success: function (data) {
                    if(data == 'ok'){
                         //mandamos mensaje de 
                         Swal.fire({
                                icon: 'info',
                                title: 'Mensaje del Sistema',
                                text: 'Los documentos fueron cargados correctamente'
                        })

                        //actalizamos los archivos
                        get_documentos(a+fol);
                        $(this).next('.custom-file-label').html("");
                    }
                    else{
                        Swal.fire({
                                icon: 'error',
                                title: 'Mensaje del Sistema',
                                text: 'No se adjunto ningun documento'
                        })
                    }
                }
            });
        });  

        $( document ).on('click', '#btn_noprocede', function(e) {
            //alert('Cancelar tramite');
            var anio = $("#year").val();
            var folio = '000' + $("#folio").val();
            var pobl = $("#POBL").val();
            var ctel = $("#CTEL").val();
            var manz = $("#MANZ").val();
            var pred = $("#PRED").val();
            var unid = $("#UNID").val();
            
            var clave = pobl + ctel + manz + pred + unid;

            var verificador = "<?php echo $usuario->username; ?>";

            //preguntar si en realidad se desea cancelar el tramite
            Swal.fire({
                title: '¿Seguro que trámite no procede?',
                text: "!Una vez marcado el trámite se verá reflejado en seguimiento como C.D.I no procedio !\nEscriba las observaciones!",
                icon: 'warning',
                input: 'textarea',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Confirmar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        if (result.value) {
                            //actualizamos en la base de datos

                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url("verificacion/registrar_no_procede_cdi"); ?>",
                                type: "POST",
                                data: {'Anio' : anio, 'Folio': folio, 'Observaciones' : result.value, 'Clave' : clave, 'Verificador' : verificador},
                                cache: false,
                                success: function(data){
                                    if (data == 'ok')
                                    {
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'info',
                                                            title: 'Mensaje del Sistema',
                                                            text: 'El C.D.I. se actualizó correctamente'
                                                            
                                                        })
                                        //cerramos el modal 
                                        $('#btn_consultar').click();
                                    }
                                    else{
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'error',
                                                            title: 'Mensaje del Sistema',
                                                            text: 'Ocurrió un error al actualizar el folio'
                                                            
                                                        })
                                    }
                                }
                            });
                            
                        }
                    }
                })
        });
        
         
         // Add the following code if you want the name of the file appear on select
         $(".custom-file-input").on("change", function() {
            var files = [];
            for (var i = 0; i < $(this)[0].files.length; i++) {
                files.push($(this)[0].files[i].name);
            }
            $(this).next('.custom-file-label').html(files.join(', '));
        });

        
       </script>