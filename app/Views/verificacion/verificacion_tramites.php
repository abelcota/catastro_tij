<style>
.card-box {
    padding: 20px;
    border-radius: 3px;
    margin-bottom: 30px;
    background-color: #fff;
}

.file-man-box {
    padding: 20px;
    border: 1px solid #e3eaef;
    border-radius: 5px;
    position: relative;
    margin-bottom: 20px
}

.file-man-box .file-close {
    color: #98a6ad;
    position: absolute;
    line-height: 24px;
    font-size: 24px;
    right: 10px;
    top: 10px;
    visibility: hidden
}

.file-man-box .file-img-box {
    line-height: 120px;
    text-align: center
}

.file-man-box .file-img-box img {
    height: 64px
}

.file-man-box .file-download {
    font-size: 24px;
    color: #98a6ad;
    position: absolute;
    right: 10px
}

.file-man-box .file-download:hover {
    color: #313a46
}

.file-man-box .file-man-title {
    padding-right: 25px
}

.file-man-box:hover {
    -webkit-box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02);
    box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02)
}

.file-man-box:hover .file-close {
    visibility: visible
}
.text-overflow {
    text-overflow: ellipsis;
    white-space: nowrap;
    font-size:12px;
    display: block;
    width: 100%;
    overflow: hidden;
}
</style>
<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">VERIFICACIÓN DE TRÁMITES DE VENTANILLA</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">ADMINISTRATIVO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="col-lg-12">
                        <div class="btn-toolbar card d-print-none" role="toolbar">
                            <div class="card-body">
                                <div class="row">

                                <div class="col-lg-3">
                                    <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Año y Folio:</label>
                                                    <div class="input-group">
                                                                <input type="text" class="form-control number-references" placeholder="00" id="year" name="year" required maxlength="2" required value="<?php echo $year; ?>">
                                                                <input type="text" class="form-control number-references" name="folio" placeholder="00000" id="folio" required maxlength="5" required value="<?php echo $folio; ?>">
                                                    </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral:</label>
                                                    
                                                            <div class="input-group">
                                                              
                                                                <input type="text" class="form-control number-references" name="CTEL" placeholder="CTEL" id="CTEL" readonly maxlength="3">
                                                                <input type="text" class="form-control number-references" placeholder="MANZ" id="MANZ" name="MANZ" required maxlength="3" readonly>
                                                                <input type="text" class="form-control number-references" name="PRED" placeholder="PRED" id="PRED" readonly maxlength="3" required>
                                                                
                                                                
                                                            </div>
                                                   
                                                </div>
                                </div>
                                   
                                    <div class="col-lg-3">
                                                <br>
                                                <a href="javascript:;" class="btn btn-primary waves-effect waves-light btn-block mt-2" id="btn_consultar"><i class="fa fa-search"></i> Consultar</a>
                                                
                                               
                                    </div>
                                </div>            
                            </div>
                        </div>
                        <!--end toolbar -->             
                    </div>                
                    
                    <div class="btn-toolbar p-3 text-center" role="toolbar" style="margin-top:-30px;" id="toolbar">
                                           <button type="button" class="btn btn-info waves-light waves-effect" id="btn_envinsp"><i class="mdi mdi-image-search-outline"></i> Enviar a Inspección</button>

                                            <button type="button" id="btn_envgraf" class="btn btn-info waves-light waves-effect ml-5"><i class="mdi mdi-vector-polyline-edit"></i> Enviar a Graficación</button>

                                            <button type="button" id="btn_envcaptura" class="btn btn-success waves-light waves-effect ml-5"><i class="mdi mdi-database-check"></i> Enviar a Captura y Autorización</button>

                                            <button type="button" class="btn btn-danger waves-light waves-effect ml-5" id="btn_noprocede"><i class="fa fa-ban"></i> Trámite no Procede</button>                                       
                                            

                                    </div>
                                    <!--end toolbar -->
                                    
                    <!-- begin tabs -->
                    <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                <center><span id="statustram" class="text-medium"> Estatus del Trámite: </span></center><br>
                                
                                    <h4 class="card-title">Consulta de Datos del Trámite </h4>
                                    
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs nav-justified pt-3" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#generales" role="tab">
                                                <span class="d-none d-sm-block "><i class="bx bx-info-circle"></i> Generales</span>
                                            </a>
                                        </li>
                        
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#documentacion" role="tab">
                                                <span class="d-block d-sm-none"></span>
                                                <span class="d-none d-sm-block "><i class="bx bx-folder"></i> Documentación del Trámite</span>
                                            </a>
                                        </li>
                                       
                                       
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content p-0 text-muted">
                                        <div class="tab-pane active border p-2" id="generales" role="tabpanel">
                                           <!-- form de generales -->
                                           <h4 class="card-title">Información del Solicitante</h4>
                                          <div class="row">
                                               <div class="col-lg-9">
                                                  <div class="row">
                                                  
                                           <div class="col-md-12">
                                               <div class="form-group position-relative">
                                                   <label for="validationTooltip02">Nombre:</label>
                                                   <input type="text" class="form-control number-references  card-title" id="nom_sol" name="nom_sol" required>
                                               </div>
                                           </div>
                                             <div class="col-md-9">
                                               <div class="form-group position-relative">
                                                  <label for="validationTooltip02">Domicilio:</label>
                                                   <input type="text" class="form-control number-references  card-title" placeholder="" id="dom_sol" name="dom_sol" required>
                                               </div>
                                           </div>
                                             <div class="col-md-3">
                                               <div class="form-group position-relative">
                                                  <label for="validationTooltip02">Telefono:</label>
                                                   <input type="text" class="form-control number-references  card-title" id="tel_sol" name="tel_sol" required>
                                               </div>
                                           </div>

                                           <div class="col-md-9">
                                               <div class="form-group">
                                                   <label for="tramites">Tramite:</label>
                                                    <input class="form-control number-references  card-title" id="idtramites" name="idtramites" required type="hidden">
                                                     <input class="form-control number-references  card-title" id="tramites" name="tramites" required >
                                                        
                                               </div>
                                           </div>

                                           <div class="col-md-3">
                                               <div class="form-group">
                                                  <label for="tramitenot">Tramite por Notario:</label>
                                                   <input type="text" class="form-control number-references  card-title" id="tramitenot" name="tramitenot" maxlength="3" required>
                                               </div>
                                           </div>
                                           
                                             <div class="col-md-12">
                                               <div class="form-group position-relative">
                                                  <label for="motivo">Motivo de la Declaración:</label>
                                                    <textarea class="form-control number-references  card-title" rows="3" id="motivo" name="motivo" ></textarea>
                                               </div>
                                           </div>

                                           <div class="col-md-12">
                                               <div class="form-group">
                                                  <label for="IdEpleadoBarra">Atendidó por:</label>
                                                  
                                                  <input type="text" class="form-control number-references  card-title" id="empleados" name="IdEpleadoBarra" required>
                                                    
                                               </div>
                                           </div>
                                           
                                               </div>
                                           </div>
                                               <div class="col-lg-3">
                                                   <h4 class="card-title">Requisitos</h4>
                                                   <hr>
                                                   <div id="requisitos_list" name="requisitos_list">
                                                    
                                                   </div>
                                                   
                                               </div>
                                       </div>
                                       <hr>
                                       
                                           <div class="row">
                                           
                                            <div class="col-lg-9">
                                            <h4 class="card-title">Información del Propietario</h4>
                                                        
                                                        <div class="form-group">
                                                            <label for="validationTooltip04">Nombre(s):</label>
                                                            <input type="text" class="form-control number-references  card-title" readonly id="nombres"  value="">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="validationTooltip03">Curp:</label>
                                                            <input type="text" class="form-control number-references  card-title" id="curp" name="curp" >
                                                        </div>
                                                   
                                               
                                            </div>

                                            <div class="col-lg-3 text-center">
                                                
                                                <label for="validationTooltip04">Fotografía:</label><br>
                                                <img id="foto" src="<?php echo base_url("assets/images/users/user.jpg");?>" alt="" width="200" height="150" class="img-thumbnail circle" style="cursor: pointer;">
                                                
                                                <input type="hidden" class="form-control number-references image-tag" name="image">
                                        </div>
                                            
                                                
                                        </div>
                                          <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="idcalle">Calle:</label>
                                                    <input type="text" class="form-control number-references  card-title" id="idcalle" name="idcalle" readonly>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="idcolonia">Colonia:</label>
                                                    <input type="text" class="form-control number-references  card-title" id="idcolonia" name="idcolonia" readonly>
                                                   
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip04">Numero Oficial:</label>
                                                    <input type="text" class="form-control number-references  card-title" readonly id="numeroof" name="numeroof" value="">
                                                </div>
                                            </div>
                                              <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip04">Uso</label>
                                                    <input type="text" class="form-control number-references  card-title" readonly id="uso" value="">
                                                </div>
                                            </div>
                                        </div>
                                          <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Superficie Terreno:</label>
                                                    <input type="text" class="form-control number-references  card-title" id="superficieterr" name="superficieterr" readonly >
                                                </div>
                                            </div>
                                              <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                   <label for="validationTooltip02">Superficie Construcción:</label>
                                                    <input type="text" class="form-control number-references  card-title"  id="superficiecons" name="superficiecons" readonly >
                                                </div>
                                            </div>
                                              <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                   <label for="validationTooltip02">Valor Catastral:</label>
                                                    <input type="text" class="form-control number-references  card-title"  id="valorcat" name="valorcat" readonly >
                                                </div>
                                            </div>
                                            
                                           
                                            <div class="col-md-12">
                                            <h4 class="card-title">Ubicación Geográfica</h4>
                                            <hr>
                                            <div id="map" class="map" style="height: 400px;"></div>
                                            </div>
                                            
                                        </div>
                                         
                                        </div>


                                        <div class="tab-pane" id="documentacion" role="tabpanel">
                                            <div class="row border p-3" id="files">
                                              
                                            </div>

                                            <hr>
                                            <div class="col-md-12">
                                            <div class="form-group position-relative">
                                            <label for="validationTooltip02">Adjuntar Documentos Adicionales:</label>
                                            
                                            <div class="custom-file">
                                            <form id="form_archivos">
                                                <input type="file" class="custom-file-input" id="archivos[]" name="archivos[]" multiple accept="pdf">
                                                <label class="custom-file-label text-truncate" for="customFile" data-browse="Buscar...">Seleccionar Archivos</label>
                                                </form>
                                            </div>
                                               
                                                <button type="button" class="btn btn-success mt-2" id="btn_subir">Adjuntar Archivos</button>
                                                                                                
                                            </div>
                                        </div>
                                       
                                        
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end tabs -->
                     
                     
                     </div>
                     <!-- end row -->

                     <div class="modal fade" id="modal-doc">
                        <div class="modal-dialog modal-xl" role="document">
                            <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                                <object type="application/pdf" data="path/to/pdf" width="100%" height="800" style="height: 85vh;" id="objdata">No Support</object>
                            </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->



 <!--modal para ver el listado de solicitudes -->
 <div class="modal fade modal-envinsp" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                        <div class="modal-dialog modal-lg modal-dialog-top">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title mt-0">Registrar Bitácora de Envío a Inspección</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">  
                                                                <form id="form-envio">
                                                                <div class="row">
                                                                <div class="col-md-2">
                                                                        <div class="form-group position-relative">
                                                                            <label for="bitacora" class="ml-2">  Año:</label>
                                                                            <input type="number" class="form-control number-references " placeholder="00" id="year2" name="year2" required readonly>
                                                                        </div>
                                                                    </div>
                                                                <div class="col-md-2">
                                                                        <div class="form-group position-relative">
                                                                            <label for="bitacora" class="ml-2">  Folio:</label>
                                                                            <input type="number" class="form-control number-references " placeholder="00000" id="folio2" name="folio2" required readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group position-relative">
                                                                            <label for="bitacora" class="ml-2">  Clave Catastral:</label>
                                                                            <input type="text" class="form-control number-references " placeholder="00000000" id="cvecat" name="cvecat" required readonly>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2">
                                                                        <div class="form-group position-relative">
                                                                            <label for="bitacora" class="ml-2">  No. Bitácora:</label>
                                                                            <input type="number" class="form-control number-references " placeholder="00000000" id="bitacora" name="bitacora" required readonly>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="col-md-3">
                                                                    <div class="form-group position-relative">
                                                                            <label for="bitacora" class="ml-2">Fecha de Envío:</label>
                                                                            <input type="date" class="form-control number-references " id="fechaenvio" name="fechaenvio" required readonly>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="col-md-6">
                                                                        <label for="bitacora">Envía:</label>
                                                                            <select class="form-control number-references" id="envia" name="envia">
                                                                            <option>Seleccionar Empleado</option>
                                                                            </select>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label for="bitacora">Recibe:</label>
                                                                            <select class="form-control number-references" id="recibe" name="recibe">
                                                                            <option>Seleccionar Empleado</option>
                                                                            </select>
                                                                    </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row">
                                                                    <div class="col-lg-12">
                                                                   
                                                                    </div>
                                              
                                                                </div>
                                                                </form>
                                                                </div>
                                                                <div class="modal-footer">              
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_tcancelar">Cerrar</button>
                                                                    <button type="button" class="btn btn-success"  id="btn_guardar">Guardar</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->


                    <div class="modal fade modal-enviograf" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                        <div class="modal-dialog modal-lg modal-dialog-top">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title mt-0">Registrar Bitácora de Envío a Graficación</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">  
                                                                <form id="form-envio">
                                                                <div class="row">
                                                                <div class="col-md-2">
                                                                        <div class="form-group position-relative">
                                                                            <label for="bitacora" class="ml-2">  Año:</label>
                                                                            <input type="number" class="form-control number-references " placeholder="00" id="yeargraf" name="yeargraf" required readonly>
                                                                        </div>
                                                                    </div>
                                                                <div class="col-md-2">
                                                                        <div class="form-group position-relative">
                                                                            <label for="bitacora" class="ml-2">  Folio:</label>
                                                                            <input type="number" class="form-control number-references " placeholder="00000" id="foliograf" name="foliograf" required readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group position-relative">
                                                                            <label for="bitacora" class="ml-2">  Clave Catastral:</label>
                                                                            <input type="text" class="form-control number-references " placeholder="00000000" id="cvecatgraf" name="cvecatgraf" required readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group position-relative">
                                                                            <label for="bitacora" class="ml-2"> No. Bitácora:</label>
                                                                            <input type="number" class="form-control number-references " placeholder="00000000" id="bitacoragraf" name="bitacoragraf" required readonly>
                                                                        </div>
                                                                    </div>
                                                                   
                                                                    <div class="col-md-3">
                                                                    <div class="form-group position-relative">
                                                                            <label for="bitacora" class="ml-2">Fecha de Envío:</label>
                                                                            <input type="date" class="form-control number-references " id="fechaenviograf" name="fechaenviograf" required readonly>
                                                                            <input type="hidden" class="form-control number-references " id="origengraf" name="origengraf" value="A" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="col-md-6">
                                                                        <label for="bitacora">Envía:</label>
                                                                            <select class="form-control number-references" id="enviagraf" name="enviagraf">
                                                                            <option>Seleccionar Empleado</option>
                                                                            </select>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label for="bitacora">Recibe:</label>
                                                                            <select class="form-control number-references" id="recibegraf" name="recibegraf">
                                                                            <option>Seleccionar Empleado</option>
                                                                            </select>
                                                                    </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row">
                                                                    <div class="col-lg-12">
                                                                    
                                                                    </div>
                                              
                                                                </div>
                                                                </form>
                                                                </div>
                                                                <div class="modal-footer">              
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_tcancelar">Cerrar</button>
                                                                    <button type="button" class="btn btn-success"  id="btn_guardargraf">Guardar</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->

                   