<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
 <!-- Buttons examples -->
 <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
   
   <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
   

   <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


   <!-- App js -->
   <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/peritos/utils.js")."?v".rand(); ?>" ></script>

   
<script>

function formatFriendCode(friendCode, numDigits, delimiter) {
        return Array.from(friendCode).reduce((accum, cur, idx) => {
            return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
        }, '')
    }

function consultar(folio){
    document.location = "/verificacion/avaluocomercial/" + folio+"/revision";
}

$(document).ready(function () {

var table = $('#tbl-bitacoras').DataTable({
    "orderCellsTop": true,
    "language": {
        "url": "<?php echo base_url("assets/Spanish.json")?>"
    },
    "bProcessing": true,
    "sAjaxSource": "<?php echo base_url("peritos/get_avaluos_comercial")."/revision"?>",
    "bPaginate":true,
    "sPaginationType":"full_numbers",
    "iDisplayLength": 25,
    "order": [[ 0, "desc" ], [1, "desc"]],
    "aoColumns": [
        {
                data: null,
                defaultContent: '<a class="consultar text-primary" style="cursor:pointer !important;"> <i class="dripicons-information"></i></a>'
        },
        { mData: 'informacion_id' } ,
        { mData: 'clave_cat' },
        { mData: 'nombre' },
        { mData: 'graficado' },
        { mData: 'estatus' },
        { mData: 'fecha_creacion' },
        { mData: 'ultima_modificacion' },
        { mData: 'fecha_autorizacion' },
    ],
    rowCallback: function(row, data, index) {
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        $("td:eq(1)", row).addClass('text-primary font-weight-bold');
        $("td:eq(2)", row).text(formatFriendCode(data['clave_cat'], 3, '-').slice(0, -1));

        if(data['graficado'] == '1')
            $("td:eq(4)", row).html('<span class="badge badge-info font-size-12">Requiere graficación</span>'); 
        else{
            $("td:eq(4)", row).html('<span class="badge badge-secondary font-size-12">No requiere graficación</span>'); 
        }  

        if(data['estatus'] == 'Autorizado')
            $("td:eq(5)", row).html('<span class="badge badge-primary font-size-12">Autorizado</span>'); 
        else if(data['estatus'] == 'Enviado'){
            $("td:eq(5)", row).html('<span class="badge badge-primary font-size-12">Revisión</span>'); 
        }else if(data['estatus'] == 'Cancelado'){
            $("td:eq(5)", row).html('<span class="badge badge-danger font-size-12">Cancelado</span>'); 
        }else if(data['estatus'] == 'Regresado'){
            $("td:eq(5)", row).html('<span class="badge badge-warning font-size-12">Regresado</span>'); 
        }   

        $("td:eq(2)", row).addClass('text-center font-weight-bold');
        if(data['fecha_creacion'] != null)
            $("td:eq(6)", row).text(moment(data['fecha_creacion']).locale('es').format('DD/MM/YYYY'));
        if(data['ultima_modificacion'] != null)
            $("td:eq(7)", row).text(moment(data['ultima_modificacion']).locale('es').format('DD/MM/YYYY'));
        if(data['fecha_autorizacion'] != null)
            $("td:eq(8)", row).text(moment(data['fecha_autorizacion']).locale('es').format('DD/MM/YYYY'));
    }
}); 


    //click al boton consultar
    $('#tbl-bitacoras tbody').on( 'click', '.consultar', function () {
                if($('#tbl-bitacoras').DataTable().row(this).child.isShown()){
                    var data = $('#tbl-bitacoras').DataTable().row(this).data();
                }else{
                    var data = $('#tbl-bitacoras').DataTable().row($(this).parents("tr")).data();
                }
                var fol = data["informacion_id"];

                Swal.fire({
                title: 'Consultar el folio: '+fol+'?',
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Consultar!'
                }).then((result) => {
                    if (result.value) {
                        consultar(fol);
                    }
                })
        });


});

    //funcion para formatear los campos de la clave catastral
    function zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }

    function formatFriendCode(friendCode, numDigits, delimiter) {
        return Array.from(friendCode).reduce((accum, cur, idx) => {
            return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
        }, '')
    }

</script>