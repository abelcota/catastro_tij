<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">AVALUOS COMERCIALES</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">PERITOS</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <!-- start row -->
                    <div class="row">
                    <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    
                                <h5 class="card-title">Bitacota de Avaluos Comerciales Enviados a Revisión</h5>
                                <hr>
                                     <!-- start form -->

                                        <div class="row">
                                            <div class="col-lg-12 table-responsive" style="margin-top:10px;">
                                                <table class="table display table-hover nowrap" id="tbl-bitacoras">
                                                    <thead class="text-white" style="background-color: #480912;">
                                                        <th class="text-center"></th>
                                                        <th class="text-center">Info. Catastral</th>
                                                        <th class="text-center">Clave Catastral</th>
                                                        <th class="text-center">Perito</th>
                                                        <th class="text-center">Graficación</th>
                                                        <th class="text-center">Estatus</th>
                                                        <th class="text-center">Fecha Creación</th>
                                                        <th class="text-center">Última Modificacion</th>
                                                        <th class="text-center">Fecha Autorización</th>
                                                    </thead>
                                                    
                                                    </table>
                                            </div>
                                        </div> 
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->

                     
                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            