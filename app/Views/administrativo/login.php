<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <title> Ingresar | Sistema Integral de Catastro Culiacán</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- Bootstrap Css -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?php echo base_url();?>assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?php echo base_url();?>assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

</head>

<body>
    <div class="home-btn d-none d-sm-block">
        <a href="#" class="text-dark"><i class="fas fa-home h2"></i></a>
    </div>
    <div class="account-pages my-5 pt-sm-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card overflow-hidden">
                        <div class="bg-login text-center">
                             
                            <div class="bg-login-overlay"></div>
                            <div class="position-relative">
                               <img src="<?php echo base_url();?>/assets/images/logo_hw.png" alt="" height="100">
                                <h5 class="text-white font-size-20">Sistema Catastral Culiacan</h5>
                                <p class="text-white mb-0"><?php echo lang('Auth.login_heading');?></p>
                                <a href="#" class="logo logo-admin mt-4">
                                    <img src="<?php echo base_url();?>/assets/images/users/user.jpg" alt="" height="40">
                                </a>
                            </div>
                        </div>
                        <div class="card-body pt-5">
                        
                            <p style="text-align:center;"><?php echo lang('Auth.login_subheading');?></p>
                            <div class="p-2">
                            <div id="infoMessage"><?php echo $message;?></div>

                                <?php echo form_open('auth/login', array('class' => 'form-horizontal'));?>

                                <div class="form-group">
                                    <?php echo form_label(lang('Auth.login_identity_label'), 'identity');?>
                                    <?php echo form_input($identity);?>
                                  </div>

                                  <div class="form-group">
                                    <?php echo form_label(lang('Auth.login_password_label'), 'password');?>
                                    <?php echo form_input($password);?>
                                  </div>

                                  
                                  <div class="custom-control custom-checkbox">       
                                    <?php echo form_checkbox('remember', '1', false, array('id'=>"remember",'class'=>"custom-control-input"));?>
                                    <?php echo form_label(lang('Auth.login_remember_label'), 'remember');?>
                                   
                                  </div>


                                  <div class="mt-3"><?php echo form_submit('submit', lang('Auth.login_submit_btn'), array('class'=>"btn btn-success btn-block waves-effect waves-light"));?></div>

                                <?php echo form_close();?>

                                <p><a href="forgot_password"><?php echo lang('Auth.login_forgot_password');?></a></p>
                            </div>

                        </div>
                    </div>
                    <div class="mt-5 text-center"> 
                        <p><script>document.write(new Date().getFullYear())</script> © Catastro Culiacán.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- JAVASCRIPT -->
    <script src="<?php echo base_url();?>/assets/libs/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url();?>/assets/libs/metismenu/metisMenu.min.js"></script>
    <script src="<?php echo base_url();?>/assets/libs/simplebar/simplebar.min.js"></script>
    <script src="<?php echo base_url();?>/assets/libs/node-waves/waves.min.js"></script>

    <script src="<?php echo base_url();?>/assets/js/app.js"></script>

</body>

</html>





