<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    <!-- Datatable init js -->
    <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>

    <script src="<?php echo base_url("https://cdn.datatables.net/rowgroup/1.1.3/js/dataTables.rowGroup.min.js");?>"></script>

    

    <script>

        $(document).ready(function () {
            cargar_roles();
            $("#btn_actualizar").hide();
            
            var table = $("#tbl_usuarios").DataTable({
                ajax: "<?php echo base_url("padron/usuarios")?>",
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                responsive: false,
                columns : [
                    { "data" : "id" },
                    { "data" : "username" },  
                    { "data" : "email" },
                    { "data" : "rol" },
                    { "data" : "privilegio_visor" },
                    {
                        targets: -1,
                        data: null,
                        defaultContent: '<a class="roles text-success" style="cursor:pointer !important;"> <i class="bx bx-check-shield bx-xs bx-burst-hover"></i></a><a class="eliminar text-danger ml-2" style="cursor:pointer !important; margin-left:5px;"><i class="bx bx-trash-alt bx-xs bx-burst-hover"></i> </a>'
                    } 
                ],
                rowGroup: {
                    dataSrc: "rol"
                },
                rowCallback: function(row, data, index) {

                    $("td:eq(-1)", row).addClass("text-center");
                    $("td:eq(4)", row).addClass("text-center");
                    $("td:eq(1)", row).addClass("font-weight-bold");

                    if(data['privilegio_visor'] === '1')
                    {
                        $("td:eq(4)", row).html('<a href="javascript:;" val="0" class="priv">Si</a>');
                    }
                    else{
                        $("td:eq(4)", row).html('<a href="javascript:;" val="1" class="priv">No</a>');
                    }
                    
                    
                }
            });

            $('#tbl_usuarios tbody').on( 'click', '.roles', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //llenar los campos del modal con los de la tabla
                $("#aid").val(data["id"]);
                $("#ausuario").val(data["username"]);                
                $(".modal-asignar-rol").modal("show");
                
            });

            $('#tbl_usuarios tbody').on( 'click', '.priv', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }

                var id = data["id"];
                var val = $(this).attr('val');
                var tex = "";
                if(val == '1'){
                    text = "Asignar";
                }
                else{
                    text = "Quitar";
                }
                Swal.fire({
                title: '¿'+text+' privilegio de impresion para el visor?',
                type: 'question',
                imageHeight:80,
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Si!'
                }).then((result) => {
                    if (result.value) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url("administrativo/actualizar_privilegio"); ?>/"+val+"/"+id,
                        success: function(data){
                            $('#tbl_usuarios').DataTable().ajax.reload(null, false);
                            if(data == 'ok'){
                                Swal.fire({
                                type: 'success',
                                title: "Privilegio asignado correctamente",
                                showConfirmButton: false,
                                timer: 1500
                            })
                            }
                            else{
                                Swal.fire({
                                type: 'error',
                                title: "Ocurrio un error al asignar el privilegio",
                                showConfirmButton: false,
                                timer: 1500
                            })
                            }
                            
                           
                        }
                    });
                    }
                })
                
                
            });

            $('#tbl_usuarios tbody').on( 'click', '.eliminar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //alert( "Eliminar: " + data[0] );
                var id = data["id"];
                var nombre = data["username"];
                
                Swal.fire({
                title: '¿Seguro de eliminar el usuario: '+nombre+'?',
                type: 'question',
                imageHeight:80,
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Si, Eliminar!'
                }).then((result) => {
                    if (result.value) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url("administrativo/eliminar_usuario/"); ?>",
                        data: {'id': id},
                        success: function(data){
                            $('#tbl_usuarios').DataTable().ajax.reload(null, false);
                            if(data == 'ok'){
                                Swal.fire({
                                type: 'success',
                                title: "Usuario eliminado correctamente",
                                showConfirmButton: false,
                                timer: 1500
                            })
                            }
                            else{
                                Swal.fire({
                                type: 'error',
                                title: "Ocurrio un error al eliminar el usuario",
                                showConfirmButton: false,
                                timer: 1500
                            })
                            }
                            
                           
                        }
                    });
                    }
                })
            });


        });
        
        $(document).on('click', '#btn_nuevo',  function(e){
            e.preventDefault();
            $("#form_usuarios")[0].reset();
            $(".modal-title").text("Registrar Nuevo Usuario");
            $("#btn_guardar").show();
            $(".modal-registro").modal("show");
        });

        function cargar_roles()
        {
            $.getJSON("<?php echo base_url("padron/roles/");?>", function(json){
                    $('#roles').empty();
                    $('#roles').append($('<option>').text("Seleccionar Rol"));
                    $.each(json.data, function(i, obj){
                            $('#roles').append($('<option>').text(obj.name).attr({ 'value' : obj.name, 'rolid':obj.id}));
                    });
                    $('#aroles').empty();
                    $.each(json.data, function(i, obj){
                            $('#aroles').append($('<option>').text(obj.name).attr({ 'value' : obj.id, 'rolid':obj.id}));
                    });
            });
        }

        $(document).on('click', '#btn_asignar_rol',  function(e){
            e.preventDefault();
            var form =  $("#form_asignacionrol");
            form.validate({
                messages: {
                ausuario:  "* Usuario Requerido",
                aroles:  "* Rol Requerido",

            }});
            //console.log(form.serialize());
            if(form.valid()){
                $.ajax({
                url: "<?php echo base_url("administrativo/asignar_rol_usuario"); ?>",
                type: "POST",
                data: form.serialize(),
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    },
                    success: function (data) {
                        //reset el form
                        document.getElementById('form_asignacionrol').reset();
                        //actualizar tabla y cerrar modal
                        $('#tbl_usuarios').DataTable().ajax.reload(null, false);
                        $(".modal-asignar-rol").modal("hide");
                        //desplegar mensaje del controlador
                        //alert(data);
                        //console.log(data);
                        Swal.fire({
                            //title:"Good job!",
                            text: data,
                            type:"info",
                            showCancelButton:!0,
                            confirmButtonColor:"#3b5de7",
                            cancelButtonColor:"#f46a6a"
                        })
                    }
                });
            }

        });

        $(document).on('click', '#btn_guardar',  function(e){
            e.preventDefault();
          var form =  $("#form_usuarios");
          form.validate({
            messages: {
            username:  "* Nombre de Usuario Requerido",
            email:  "* Email Requerido",
            password:    "* Contraseña Requerida",
            pass_confirm: "* Confirmación de Contraseña Requerida"
        }
          });
          console.log(form);
          if(form.valid()){
          $.ajax({
            url: "<?php echo base_url("administrativo/registrar_usuario"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //reset el form
                    document.getElementById('form_usuarios').reset();
                    //actualizar tabla y cerrar modal
                    $('#tbl_usuarios').DataTable().ajax.reload(null, false);
                    $(".modal-registro").modal("hide");
                    //desplegar mensaje del controlador
                    //alert(data);
                    console.log(data);
                    Swal.fire({
                        //title:"Good job!",
                        text: data,
                        type:"info",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                }
            });
          }

          
        });

        

         
    </script>