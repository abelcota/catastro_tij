 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">ADMINISTRACIÓN DE USUARIOS</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">ADMINISTRATIVO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="btn-toolbar p-3" role="toolbar">

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_nuevo">
                                            <i class="dripicons-user-group"></i> Registrar Usuario
                                            </button>
                                            
                                        </div>
                                        <?= view('Myth\Auth\Views\_message_block') ?>
                                    </div>
                                    <!--end toolbar -->

                      <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">       
                                <div class="float-right">
                                    <i class="bx bx-check-shield bx-xs text-success"></i><small> Asignar Rol</small>
                                    <i class="bx bx-trash-alt bx-xs text-danger"></i><small> Eliminar</small>
                                </div>
                                
                <table class="table table-hover" id="tbl_usuarios">
                <thead>
                    <tr>
                        <th>ID</th> 
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Rol</th>
                        <th>Visor</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                </table>

                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->


       
                            
<!-- modal para registrar un nuevo rol -->
<div class="modal fade modal-registro" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                    <form action="<?= route_to('register') ?>" name="form_usuarios" id="form_usuarios" method="post" accept-charset="utf-8">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Registrar Nuevo Usuario</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                        <?= csrf_field() ?>
                                                        <div class="form-group row mb-4">
                                                            
                                                                <label for="nombre" class="col-form-label col-lg-3">Rol del Usuario</label>
                                                                <div class="col-lg-9">
                                                    
                                                                <select class="form-control select2 w-full" id="roles" name="roles"></select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-4">
                                                            
                                                                <label for="nombre" class="col-form-label col-lg-3"><?=lang('Auth.username')?></label>
                                                                <div class="col-lg-9">
                                                    
                                                                <input type="text" class="form-control <?php if(session('errors.username')) : ?>is-invalid<?php endif ?>" name="username" placeholder="<?=lang('Auth.username')?>" value="<?= old('username') ?>" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-4">
                                                                <label class="col-form-label col-lg-3"><?=lang('Auth.email')?></label>
                                                                <div class="col-lg-9">
                                                                <input type="email" class="form-control <?php if(session('errors.email')) : ?>is-invalid<?php endif ?>"
                                   name="email" aria-describedby="emailHelp" placeholder="<?=lang('Auth.email')?>" value="<?= old('email') ?>" required>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-4">
                                                            
                                                                <label for="nombre" class="col-form-label col-lg-3"><?=lang('Auth.password')?></label>
                                                                <div class="col-lg-9">
                                                    
                                                                <input type="password" name="password" class="form-control <?php if(session('errors.password')) : ?>is-invalid<?php endif ?>" placeholder="<?=lang('Auth.password')?>" autocomplete="off" required>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-4">
                                                            
                                                                <label for="nombre" class="col-form-label col-lg-3"><?=lang('Auth.repeatPassword')?></label>
                                                                <div class="col-lg-9">
                                                    
                                                                <input type="password" name="pass_confirm" class="form-control <?php if(session('errors.pass_confirm')) : ?>is-invalid<?php endif ?>" placeholder="<?=lang('Auth.repeatPassword')?>" autocomplete="off" required>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="modal-footer">
                                                        
                                                            <button type="button" class="btn btn-success" id="btn_guardar"><?=lang('Auth.register')?></button>
                                                            <button type="button" class="btn btn-primary" id="btn_actualizar">Actualizar</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar">Cancelar</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->

<!-- modal para registrar un nuevo rol -->
<div class="modal fade modal-asignar-rol" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                    <form name="form_asignacionrol" id="form_asignacionrol" method="post" accept-charset="utf-8">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Asignación de Rol</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                        <div class="form-group row mb-4">
                                                            
                                                                <label for="nombre" class="col-form-label col-lg-3">Usuario:</label>
                                                                <div class="col-lg-9">
                                                               
                                                                <input type="hidden" id="aid" name="aid" readonly>
                                                                <input type="text" class="form-control" value="" id="ausuario" name="ausuario" readonly>
                                                                </div>
                                                            </div>
                                                        
                                                        <div class="form-group row mb-4">
                                                                         
                                                                <label for="nombre" class="col-form-label col-lg-3">Rol:</label>
                                                                <div class="col-lg-9">
                                                    
                                                                <select class="form-control select2 w-full" id="aroles" name="aroles"></select>
                                                                </div>
                                                            </div>
                                                            

                                                        </div>

                                                        <div class="modal-footer">
                                                        
                                                            <button type="button" class="btn btn-success" id="btn_asignar_rol">Asignar Rol</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->                                            