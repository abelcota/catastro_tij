<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>


    <!-- Required datatable js -->
    <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
     <!-- Buttons examples -->
     <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
       
   
       <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
       <!-- webcam -->
       <script src="<?php echo base_url("assets/js/webcam.min.js");?>"></script>
   
       <!-- App js -->
       <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
       <script src="<?php echo base_url("/assets/js/jquery.validate.js");?>"></script>
       <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>

       <script>
            $(document).ready(function () {
                
               
                $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#PRED').blur(function () { this.value = zeroPad(this.value, 3);  });
              
                $('#vertabs').hide();
                $('#vertoolbar').hide();


                var table = $('#tblavaluos').DataTable({
                    "language": {
                        "url": "<?php echo base_url("assets/Spanish.json")?>"
                    },
                    "bProcessing": true,
                    "ordering": false,
                    "sAjaxSource": "<?php echo base_url("padron/get_listado_avaluos")?>",
                    "bPaginate":true,
                    "sPaginationType":"full_numbers",
                    "iDisplayLength": 25,
                    "aoColumns": [
                        {
                            data: null,
                            defaultContent: '<center><a class="ver text-info ml-2" style="cursor:pointer !important;" data-toggle="tooltip" data-placement="top" title="Ver Avaluo"> <i class="mdi mdi-file-export-outline mdi-18px"></i></a></center>'
                        },
                        { mData: 'id_folio' } ,
                        { mData: 'clavecat' },
                        { mData: 'id_tramite' },
                        { mData: 'valor_anterior' },   
                        { mData: 'valor_catastral' },
                        { mData: 'fecha' },
    
                    ],
                    rowCallback: function(row, data, index) {
                        var options = { year: 'numeric', month: 'long', day: 'numeric' };
                        var formatter = new Intl.NumberFormat('en-US', {
                            style: 'currency',
                            currency: 'USD',
                            });
                        $("td", row).addClass('align-middle');
                        $("td:eq(1)", row).addClass('text-primary ');
                        $("td:eq(2)", row).text(formatFriendCode(data['clavecat'], 3, '-').slice(0, -1));

                        if(data['fecha'] != null)
                            $("td:eq(6)", row).text(moment(data['fecha']).format('DD/MM/YYYY'));
                        
                        if(data['valor_anterior'] == '$NaN')
                            $("td:eq(4)", row).text("$0.00");

                    }
                });

                //click al boton consultar
                $('#tblavaluos tbody').on( 'click', '.ver', function () {
                            if($('#tblavaluos').DataTable().row(this).child.isShown()){
                                var data = $('#tblavaluos').DataTable().row(this).data();
                            }else{
                                var data = $('#tblavaluos').DataTable().row($(this).parents("tr")).data();
                            }
                            var anio = data["id_folio"].substr(0, 2);
                            var fol = data["id_folio"].slice(2);
                            $('#year').val(anio);
                            $('#folio').val(fol);

                            $('#CTEL').val(data["clavecat"].substr(0, 2));
                            $('#MANZ').val(data["clavecat"].substr(2, 3));
                            $('#PRED').val(data["clavecat"].substr(5, 3));
                            //validar(data["clavecat"]);
                            //$('#btn_consultar').click();
                             //ocultar el div
                            $('#vertabs').show();
                            $('#vertoolbar').show();

                            var cve = $('#CTEL').val()+$('#MANZ').val()+$('#PRED').val();
                            validar(cve);

                            
                });

                //click al boton imprimir
                $('#tblavaluos tbody').on( 'click', '.print', function () {
                            if($('#tblavaluos').DataTable().row(this).child.isShown()){
                                var data = $('#tblavaluos').DataTable().row(this).data();
                            }else{
                                var data = $('#tblavaluos').DataTable().row($(this).parents("tr")).data();
                            }
                            /*var anio = data["id_folio"].substr(0, 2);
                            var fol = data["id_folio"].slice(2);
                            $('#year').val(anio);
                            $('#folio').val(fol);*/

                            //var folio = data["id_folio"];

                            /*$('#POBL').val(data["clavecat"].substr(0, 3));
                            $('#CTEL').val(data["clavecat"].substr(3, 3));
                            $('#MANZ').val(data["clavecat"].substr(6, 3));
                            $('#PRED').val(data["clavecat"].substr(9, 3));
                            $('#UNID').val(data["clavecat"].substr(12, 3));*/
                            //validar();
                           
                            setTimeout(function(){
                            //Crear la captura del mapa
                            var img = "";
                            map.once('rendercomplete', function () {
                                var mapCanvas = document.createElement('canvas');
                                var size = map.getSize();
                                mapCanvas.width = size[0];
                                mapCanvas.height = size[1];
                                var mapContext = mapCanvas.getContext('2d');
                                Array.prototype.forEach.call(
                                document.querySelectorAll('.ol-layer canvas'),
                                function (canvas) {
                                    if (canvas.width > 0) {
                                    var opacity = canvas.parentNode.style.opacity;
                                    mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
                                    var transform = canvas.style.transform;
                                    // Get the transform parameters from the style's transform matrix
                                    var matrix = transform
                                        .match(/^matrix\(([^\(]*)\)$/)[1]
                                        .split(',')
                                        .map(Number);
                                    // Apply the transform to the export map context
                                    CanvasRenderingContext2D.prototype.setTransform.apply(
                                        mapContext,
                                        matrix
                                    );
                                    mapContext.drawImage(canvas, 0, 0);
                                    }
                                }
                                );
                                if (navigator.msSaveBlob) {
                                // link download attribuute does not work on MS browsers
                                navigator.msSaveBlob(mapCanvas.msToBlob(), 'map.png');
                                } else {
                                img = mapCanvas.toDataURL('image/png');
                                var url = "/reportes/pdfavaluo.php";
                                //console.log(img);
                                //var url = "/reportes/pdficonformidad.php?a="+a+"&fol="+f+"&img="+img;
                                //window.open(url,'_new');
                                var form = document.createElement("form");
                                form.setAttribute("method", "post");
                                form.setAttribute("target", "_blank");
                                form.setAttribute("action", url);
                                var params = { 'folio': data["id_folio"], 'img':img };
                                for(var key in params) {
                                    if(params.hasOwnProperty(key)) {
                                        var hiddenField = document.createElement("input");
                                        hiddenField.setAttribute("type", "hidden");
                                        hiddenField.setAttribute("name", key);
                                        hiddenField.setAttribute("value", params[key]);

                                        form.appendChild(hiddenField);
                                    }
                                }
                                document.body.appendChild(form);
                                form.submit();

                                }
                            });
                            map.renderSync();
                        
                            }, 2000);
                                        
                            });

                            //ocultar el div
                            $('#vertabs').hide();
                            $('#vertoolbar').hide();
                            $('#vershow').hide();


            });

            function formatFriendCode(friendCode, numDigits, delimiter) {
        return Array.from(friendCode).reduce((accum, cur, idx) => {
            return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
        }, '')
    }

            /*$(document).on( 'click', '#btn_consultar', function () {
                var cve = $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
                validar(cve);
            });*/

            $(document).on( 'click', '#btn_imprimir_avaluo', function () {
                //var cve = $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
                var a = $('#year').val();
                var fol = $('#folio').val();
                var cve = $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
                var folio = a+fol;
                //Crear la captura del mapa
                var img = "";
                map.once('rendercomplete', function () {
                    var mapCanvas = document.createElement('canvas');
                    var size = map.getSize();
                    mapCanvas.width = size[0];
                    mapCanvas.height = size[1];
                    var mapContext = mapCanvas.getContext('2d');
                    Array.prototype.forEach.call(
                    document.querySelectorAll('.ol-layer canvas'),
                    function (canvas) {
                        if (canvas.width > 0) {
                        var opacity = canvas.parentNode.style.opacity;
                        mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
                        var transform = canvas.style.transform;
                        // Get the transform parameters from the style's transform matrix
                        var matrix = transform
                            .match(/^matrix\(([^\(]*)\)$/)[1]
                            .split(',')
                            .map(Number);
                        // Apply the transform to the export map context
                        CanvasRenderingContext2D.prototype.setTransform.apply(
                            mapContext,
                            matrix
                        );
                        mapContext.drawImage(canvas, 0, 0);
                        }
                    }
                    );
                    if (navigator.msSaveBlob) {
                    // link download attribuute does not work on MS browsers
                    navigator.msSaveBlob(mapCanvas.msToBlob(), 'map.png');
                    } else {
                    img = mapCanvas.toDataURL('image/png');
                    var url = "/reportes/avaluo.php";
                    //console.log(img);
                    //var url = "/reportes/pdficonformidad.php?a="+a+"&fol="+f+"&img="+img;
                    //window.open(url,'_new');
                    var form = document.createElement("form");
                    form.setAttribute("method", "post");
                    
                    form.setAttribute("action", url);
                    form.setAttribute("target", "popupwindow");
                    exportwindow = window.open("", "popupwindow", "width=800,height=600,resizable=yes");
                    var params = { 'folio': folio, 'img':img, 'ccat': cve};
                    for(var key in params) {
                        if(params.hasOwnProperty(key)) {
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", key);
                            hiddenField.setAttribute("value", params[key]);

                            form.appendChild(hiddenField);
                        }
                    }
                    document.body.appendChild(form);
                    form.submit();

                    }
                });
                map.renderSync();
                //console.log("ad");
                $('#vertabs').hide();
                $('#vertoolbar').hide();
                $('#vershow').hide();

            });

            $(document).on( 'click', '#ubicacion', function () {
                map.updateSize(); 
            });

            setTimeout(function() {
                map.updateSize();
            }, 100);

            

            function zeroPad(num, places) {
                var zero = places - num.toString().length + 1;
                return Array(+(zero > 0 && zero)).join("0") + num;
            }
         

        function validar() {

            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            });

             //obtener los valores de los input de la clave
            
             var ctel = $('#CTEL').val();
             var manz = $('#MANZ').val();
             var pred = $('#PRED').val();

             var a = $('#year').val();
             var fol = $('#folio').val();

             var cvecat = pobl + ctel + manz + pred + unid;
             var endpoint = "<?php echo base_url("verificacion/consulta_avaluo2");?>/"+cvecat+"/"+a+"/"+fol;
             console.log(endpoint);
             console.log("validando clave: " + cvecat)
             $.ajax({
                url: endpoint,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Avaluo...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                       console.log(data.unidad);
                    if(data.unidad.length > 0){

                        if(data.unidad[0].ID_REGTO == "B"){
                            Swal.fire({
                               icon: 'error',
                               title: 'Clave dada de Baja...',                               
                           })
                           $("#btn_limpiar").click();
                        }
                        else if(data.unidad[0].DOM_NOT == ""){
                            Swal.fire({
                               icon: 'error',
                               title: 'Este trámite no puede seguir, requiere Domicilio de Notificación!!',                               
                           })
                           
                        }
                        /*else if(data.tramite[0].Clave != cvecat){
                            Swal.fire({
                               icon: 'error',
                               title: 'El folio no coincide con la clave catastral!!',                               
                           })
                           
                        }*/
                        else{
                            var fealta = data.unidad[0].FEC_ALTA.replaceAll("/","-");
                            var femov = data.unidad[0].FEC_MOV.replaceAll("/","-");
                            var apep = "";
                            var apem = "";
                            if( data.unidad[0].APE_PAT != null )
                                apep = data.unidad[0].APE_PAT;
                            if( data.unidad[0].APE_MAT != null )
                                apem = data.unidad[0].APE_MAT;
                            //NOMBRE COMPLETO    
                            $('#propietario').val(apep+ " " + apem+ " " + data.unidad[0].NOM_PROP);
                            $('#impuestotrans').val(data.unidad[0].IMPTO_ANIO);
                            $('#domicilio').val(data.unidad[0].DOM_NOT);
                            $('#colonia').val(data.unidad[0].NOM_COL);
                            $('#cp').val(data.unidad[0].COD_POST_N);
                            $('#frentes').val(data.unidad[0].NUM_FTES);
                            $('#mtsfrente').val(data.unidad[0].LON_FTE);
                            $('#mtsfondo').val(data.unidad[0].LON_FONDO);
                            $('#tipo_efec').val(data.unidad[0].TIP_EFEC);
                            $('#tri_efec').val(data.unidad[0].TRI_EFEC);
                            $('#anio_efec').val(data.unidad[0].ATO_EFEC);
                            $('#calle').val(data.unidad[0].CVE_CALLE + " - " +data.unidad[0].NOM_CALLE);
                            $('#numerooficial').val(data.unidad[0].NUM_OFIC_U);
                            $('#domiclio').val(data.unidad[0].UBI_PRED);
                            $('#colonia_u').val(data.unidad[0].CVE_COL + " - " + data.unidad[0].NOM_COL);
                            $('#libro').val(data.unidad[0].NUM_LIB);
                            $('#escritura').val(data.unidad[0].NUM_ESC);
                            $('#seccion').val(data.unidad[0].NUM_SECC);
                            $('#inscripcion').val(data.unidad[0].NUM_INSC);
                            $('#regimen').val(data.unidad[0].CVE_REGIM + " - " + data.unidad[0].DES_REGIM);
                            $('#restringido').val(data.unidad[0].STT_REST);
                            $('#fechalta').val(moment(fealta).format('DD/MM/YYYY'));
                            $('#fechmov').val(moment(femov).format('DD/MM/YYYY'));
                            $('#supterr').val(new Intl.NumberFormat().format(data.unidad[0].SUP_TERR));
                            $('#valorterr').val(formatter.format(data.unidad[0].VAL_TERR));
                            $('#supcons').val(data.unidad[0].SUP_CONST);
                            $('#valorconst').val(formatter.format(data.unidad[0].VAL_CONST));
                            $('#valorcat').val(formatter.format(parseFloat(data.unidad[0].VAL_CONST) + parseFloat(data.unidad[0].VAL_TERR)));

                            $('#cvezona').val(data.zona[0].CVE_ZONA);
                            $('#valorzona').val(formatter.format(parseFloat(data.zona[0].VAL_UNIT_T)));
                            $('#demzona').val(data.zona[0].FAC_DEM_VA);

                            //llenar la tabla de terreno

                            $("#tblterreno").DataTable().destroy();
                            var url_terr = "<?php echo base_url("verificacion/consulta_terreno")?>/"+cvecat;
                            
                            $("#tblterreno").DataTable({
                                data : data.terreno,
                                //ajax: url_terr,
                                "language": {
                                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                                },
                                responsive: true,
                                "bPaginate": false,
                                "bFilter": false,
                                "bInfo": false,
                                "ordering": false,
                                //dom: 'Bfrtip',
                                //buttons:["excel","pdf", "print", "colvis"],
                                columns : [              
                                    { "data" : "TIP_TERR" },
                                    { "data" : "SUP_TERR" },
                                    { "data" : "valorunitario1" },
                                    { "data" : "SUP_TERR_E" },
                                    { "data" : "valorunitario2" },
                                    { "data" : "SUP_TERR_D" },
                                    { "data" : "FAC_DEM_TE" },
                                    { "data" : "valorneto" },
                                ],
                                rowCallback: function(row, data, index) {
                                    /*var valneto = 0.0;
                                  
                                    valneto = (data["SUP_TERR"] * data["valorunitario1"] * ((100 - data["demeritotramo1"]) / 100) ) + (data["SUP_TERR_E"] * 0.25 * data["valorunitario2"] * ((100 - data["demeritotramo2"]) / 100) ) - (data["SUP_TERR_D"]  * data["valorunitario1"] * data["FAC_DEM_TE"]);*/

                                    $("td:eq(2)", row).text(formatter.format(data["valorunitario1"])); 
                                    $("td:eq(4)", row).text(formatter.format(data["valorunitario2"])); 
                                    $("td:eq(7)", row).text(formatter.format(data["valorneto"])); 
                                    //$("td:eq(1)", row).addClass("font-weight-bold"); 
                                    //$("td:eq(7)", row).text(formatter.format(valneto)); 
                                },
                                footerCallback: function ( row, data, start, end, display ) {
                                    var api = this.api(), data;
            
                                    // Remove the formatting to get integer data for summation
                                    var intVal = function ( i ) {
                                        return typeof i === 'string' ?
                                            i.replace(/[\$,]/g, '')*1 :
                                            typeof i === 'number' ?
                                                i : 0;
                                    };

                                    // Total over all pages
                                    total_sup = api
                                        .column( 1 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );

                                       // Total over all pages
                                       total_valor = api
                                        .column( 7 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                                        
                                     // Update footer
                                        $( api.column( 1 ).footer() ).html(
                                            '<label class="text-right"> '+total_sup+'</label>'
                                        );

                                        // Update footer
                                        $( api.column( 7 ).footer() ).html(
                                            '<label class="text-center"><b>'+formatter.format(total_valor)+'</b></label>'
                                        );
                                }
                            });

                            $("#tblconstrucciones").DataTable().destroy();
                            var url_cons = "<?php echo base_url("verificacion/consulta_construcciones")?>/"+cvecat;
                            $("#tblconstrucciones").DataTable({
                                //ajax: url_cons,
                                data : data.construcciones,
                                "language": {
                                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                                },
                                responsive: true,
                                "bPaginate": false,
                                "bFilter": false,
                                "bInfo": false,
                                "ordering": false,
                                //dom: 'Bfrtip',
                                //buttons:["excel","pdf", "print", "colvis"],
                                columns : [              
                                    { "data" : "CVE_CAT_CO" },
                                    { "data" : "DES_CAT_CO" },
                                    { "data" : "valorunitario" },
                                    { "data" : "SUP_CONST" },
                                    { "data" : "EDO_CO" },
                                    { "data" : "FEC_CONST" },
                                    { "data" : "EDD_CONST" },
                                    { "data" : "factordemerito" },
                                    { "data" : "factorcomercializacion" },
                                    { "data" : "valorneto" },
                                ],
                                rowCallback: function(row, data, index) {
                                    /*var valneto = 0.0;
                                    valneto = data["SUP_CONST"] * data["valorunitario"] *  data["factordemerito"] *  data["factorcomercializacion"];*/

                                    $("td:eq(2)", row).text(formatter.format(data["valorunitario"])); 
                                    //$("td:eq(1)", row).addClass("font-weight-bold"); 
                                    $("td:eq(9)", row).text(formatter.format(data["valorneto"]));  
                                },
                                footerCallback: function ( row, data, start, end, display ) {
                                    var api = this.api(), data;
            
                                    // Remove the formatting to get integer data for summation
                                    var intVal = function ( i ) {
                                        return typeof i === 'string' ?
                                            i.replace(/[\$,]/g, '')*1 :
                                            typeof i === 'number' ?
                                                i : 0;
                                    };

                                    // Total over all pages
                                    total_sup = api
                                        .column( 3 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );

                                       // Total over all pages
                                       total_valor = api
                                        .column( 9 )
                                        .data()
                                        .reduce( function (a, b) {
                                            return intVal(a) + intVal(b);
                                        }, 0 );
                                        
                                     // Update footer
                                        $( api.column( 3 ).footer() ).html(
                                            '<label class="text-right"> '+total_sup+'</label>'
                                        );

                                        // Update footer
                                        $( api.column( 9 ).footer() ).html(
                                            '<label class="text-center"><b>'+formatter.format(total_valor)+'</b></label>'
                                        );
                                }


                            });

                            //se obtienen los servicios
                            var url = "<?php echo base_url("verificacion/consulta_servicios/");?>/"+cvecat;
                            var elements = "";
                            $.getJSON(url, function(json){
                                    $('#servicios_list').empty();
                                    $.each(json.data, function(i, obj){
                                            /*var html = '<div class="form-check mb-2"><input class="form-check-input" type="checkbox" checked id="'+obj.IdRequisito+'" name="doc'+(i+1)+'"><label class="form-check-label" for="defaultCheck1">'+obj.DescripcionRequisito+'</label></div>';
                                            $('#requisitos_list').append(html);*/
                                            elements = obj.descripcionx;
                                    });
                                    var res = elements.split(",");
                                    for (var serv in res) {
                                        var html = '<li class="list-group-item">'+res[serv]+'</li>';
                                            $('#servicios_list').append(html);
                                    }
                            });

                            var url = "<?php echo base_url("verificacion/consulta_usos");?>/"+cvecat;
                            //se obtienen los usos
                            $.getJSON(url, function(json){
                                    $('#usos_list').empty();
                                    $.each(json.data, function(i, obj){
                                        var html = '<li class="list-group-item">'+obj.CVE_USO+' - <b>'+obj.DES_USO+'</b></li>';
                                        $('#usos_list').append(html);
                                    });
                                   
                            });

                            //se obtienen los propietarios
                            $("#tblpropietarios").DataTable().destroy();
                            var url_cons = "<?php echo base_url("verificacion/consulta_propietarios")?>/"+cvecat;
                            $("#tblpropietarios").DataTable({
                                //ajax: url_cons,
                                data : data.propietarios,
                                "language": {
                                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                                },
                                responsive: true,
                                "bPaginate": false,
                                "bFilter": false,
                                "bInfo": false,
                                "ordering": false,
                                //dom: 'Bfrtip',
                                //buttons:["excel","pdf", "print", "colvis"],
                                columns : [              
                                    { "data" : "CVE_PROP" },
                                    { "data" : "NOM_PROP" },
                                    { "data" : "APE_PAT" },
                                    { "data" : "APE_MAT" },
                                    { "data" : "REG_FED" },                                    
                                ],
                            });
                            
                            //Se obtiene el array del poligono 
                            jsonobj = jQuery.parseJSON(data.unidad[0].geometrico);

                            //si encuentra el poligono en el vectorial
                            if (typeof _geojson_vectorSource != "undefined") {
                                _geojson_vectorSource.clear();
                            }
                            if (jsonobj.features != null) {
                                //Se visualiza el poligono en el mapa
     
                                visualizar_capa(jsonobj);

                                map.updateSize(); 
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Vectorial no encontrado',
                                    text: 'La clave catastral no se encontró en el vectorial!'
                                    
                                })
                            }
                        }
                            
                       }
                       else {
  
                           //verificar si se encuentra de primer registro
                           Swal.fire({
                            title: 'Clave Catastral no encontrada',
                            icon: 'error',
                            }).then((result) => {
                               
                            })

                           
                       }          
                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });
         }
        

       </script>