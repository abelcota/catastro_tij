<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">ENVIOS A GRAFICACIÓN - TRÁMITES DE VENTANILLA</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">ADMINISTRATIVO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="btn-toolbar p-3" role="toolbar">
                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_nuevo"><i class="fa fa-plus"></i> Nuevo Envío a Graficación</button>
                                        </div>

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button disabled type="button" class="btn btn-primary waves-light waves-effect dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                Más <i class="mdi mdi-dots-vertical ml-2"></i>
                                            </button>
                                            <div class="dropdown-menu" style="">
                                                <a class="dropdown-item" href="#"><i class="fa fa-edit"></i> Editar</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Borrar Registro</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-print"></i> Imprimir</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end toolbar -->
                     <div class="col-lg-12">
                     
                            <div class="card">
                                <div class="card-body">
                                <h4 class="card-title">Bitácoras de Envíos a Graficación - Trámites de Ventanilla</h4>

                                    <!-- start form -->
                                        <div class="row">
                                            <div class="col-lg-12" style="margin-top:10px;">
                                            <table class="table display table-hover table-sm" id="tblbitacora">
                                            <thead class="text-white" style="background-color: #480912;">
                                            <tr>
                                                <th>Envio</th>
                                                <th>Fecha</th>
                                                <th>Año</th>
                                                <th>Folio</th>
                                                <th>Pobl</th>
                                                <th>Ctel</th>
                                                <th>Manz</th>
                                                <th>Pred</th>
                                                <th>Unid</th>
                                                <th>Fecha Graficación</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                            </div>
                                        </div>
                                        
                                                                               
                                    
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->


                     <!--modal para ver el listado de solicitudes -->
                     <div class="modal fade modal-nuevo" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                        <div class="modal-dialog modal-lg modal-dialog-top">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title mt-0">Registrar Bitácora de Envío a Graficación</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">  
                                                                <form id="form-envio">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group position-relative">
                                                                            <label for="bitacora">No. Bitácora:</label>
                                                                            <input type="number" class="form-control" placeholder="00000000" id="bitacora" name="bitacora" required readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                    <div class="form-group position-relative">
                                                                            <label for="origen">Origen:</label>
                                                                            <input type="text" class="form-control" id="origen" name="origen" value="A" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                    <div class="form-group position-relative">
                                                                            <label for="bitacora">Fecha de Envío:</label>
                                                                            <input type="date" class="form-control" id="fechaenvio" name="fechaenvio" required readonly>
                                                                        </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="col-md-6">
                                                                        <label for="bitacora">Envía:</label>
                                                                            <select class="form-control" id="envia" name="envia">
                                                                            <option>Seleccionar Empleado</option>
                                                                            </select>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label for="bitacora">Recibe:</label>
                                                                            <select class="form-control" id="recibe" name="recibe">
                                                                            <option>Seleccionar Empleado</option>
                                                                            </select>
                                                                    </div>
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row">
                                                                    <div class="col-lg-12">
                                                                    <table class="table display table-hover table-sm" id="tblnotingraficacion" width="100%">
                                                                        <thead class="text-white" style="background-color: #480912;">
                                                                        <tr>
                                                                            <th></th>
                                                                            <th>Año</th>
                                                                            <th>Folio</th>
                                                                            <th>Pobl</th>
                                                                            <th>Ctel</th>
                                                                            <th>Manz</th>
                                                                            <th>Pred</th>
                                                                            <th>Unid</th>
                                                                            <th>Clave</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                    </div>
                                              
                                                                </div>
                                                                </form>
                                                                </div>
                                                                <div class="modal-footer">              
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_tcancelar">Cerrar</button>
                                                                    <button type="button" class="btn btn-success"  id="btn_guardar">Guardar</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->

                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->
