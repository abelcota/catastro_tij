
<style>
.card-box {
    padding: 20px;
    border-radius: 3px;
    margin-bottom: 30px;
    background-color: #fff;
}

.file-man-box {
    padding: 20px;
    border: 1px solid #e3eaef;
    border-radius: 5px;
    position: relative;
    margin-bottom: 20px
}

.file-man-box .file-close {
    color: #98a6ad;
    position: absolute;
    line-height: 24px;
    font-size: 24px;
    right: 10px;
    top: 10px;
    visibility: hidden
}

.file-man-box .file-img-box {
    line-height: 120px;
    text-align: center
}

.file-man-box .file-img-box img {
    height: 64px
}

.file-man-box .file-download {
    font-size: 24px;
    color: #98a6ad;
    position: absolute;
    right: 10px
}

.file-man-box .file-download:hover {
    color: #313a46
}

.file-man-box .file-man-title {
    padding-right: 25px
}

.file-man-box:hover {
    -webkit-box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02);
    box-shadow: 0 0 24px 0 rgba(0, 0, 0, .06), 0 1px 0 0 rgba(0, 0, 0, .02)
}

.file-man-box:hover .file-close {
    visibility: visible
}
.text-overflow {
    text-overflow: ellipsis;
    white-space: nowrap;
    font-size:12px;
    display: block;
    width: 100%;
    overflow: hidden;
}
</style>
<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">CONFIGURACION DEL SELLO DIGITAL</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">ADMINISTRATIVO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">

                        </div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                <form id="form_archivos">
                                    <label for="validationTooltip02">Adjuntar Archivos de la firma electrónica:</label>
                                    <br><p>Para poder firmar los avaluos, se debe de adjuntar los archivos certificado.cer y clave.key, además de proporcionar la contraseña para abrir la clave privada.</p>
                                        <div id="alert_firma" class="alert alert-info alert-dismissible fade show" role="alert">
                                                <p id="info_folio"></p>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="custom-file mb-2 mt-2">
                                                <input type="file" class="custom-file-input" id="archivos[]" name="archivos[]" multiple accept="pdf">
                                                <label class="custom-file-label text-truncate" for="customFile" data-browse="Buscar...">Buscar Archivos .cer y .key</label>
                                            </div>
                                            <div class="custom-file mb-2 mt-2">
                                               <button type="button" class="btn btn-success" id="btn_subir_archivo"><i class="mdi mdi-upload-outline"></i>Adjuntar Archivos y Actualizar Informacion</button>
                                            </div>
                                            <div class="form-group position-relative">
                                            <label for="validationTooltip02">Contraseña para abrir la llave privada:</label>
                                            <input type="password" class="form-control card-title" id="txt_password" name="txt_password" required>
                                        </div>
                                        <div class="form-group position-relative">
                                            <label for="validationTooltip02">Director:</label>
                                            <input type="text" class="form-control card-title" id="txt_director" name="txt_director" required>
                                        </div>
                                    <div class="row border p-3" id="files">
                                    </div>
                                    
                                </form>
                                </div>
                            </div>
                        </div>

                     </div>
                     <!-- end row -->

                     <div class="modal fade" id="modal-doc">
                        <div class="modal-dialog modal-xl" role="document">
                            <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                                <object type="application/pdf" data="path/to/pdf" width="100%" height="800" style="height: 85vh;" id="objdata">No Support</object>
                            </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
