<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>


    <!-- Required datatable js -->
    <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
     <!-- Buttons examples -->
     <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
       
   
       <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
       <!-- webcam -->   
       <!-- App js -->
       <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
       <script src="<?php echo base_url("/assets/js/jquery.validate.js");?>"></script>
       <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>

       <script>
            $(document).ready(function () {
                get_documentos();
                verificar_firma();
            });

            function verificar_firma(){
                $('#info_folio').text('');
                var url = "<?php echo base_url("firma.php"); ?>";
                $.getJSON(url, function(json){
                    console.log(json['firma']);
                   if(json['status'] == "0"){
                        $('#alert_firma').removeClass('alert-info');
                        $('#alert_firma').addClass('alert-danger');
                        $('#info_folio').text('La firma no esta configurada correctamente, por favor asegurese de que los archivos .cer y .key se encuentren adjuntos ademas de que la contraseña sea la correcta');
                   }
                   else{
                        $('#alert_firma').addClass('alert-info');
                        $('#alert_firma').removeClass('alert-danger');
                        $('#info_folio').text("Firma Configurada correctamente: \nRFC: " + json['firma']['RFC'] + " - \nNOMBRE:" + json['firma']['NOMBRE']);
                        
                   }
                });
            }
       
            function get_documentos(){
            $('#files').empty();
            var url = "<?php echo base_url("administrativo/obtener_efirma"); ?>";
            $.getJSON(url, function(json){
                $.each(json['data'], function(i, obj){
                    $('#txt_password').val(obj.passphrase);
                    $('#txt_director').val(obj.director);
                });
                    var img = "";
                    $.each(json['result'], function(i, obj){

                            if(obj.ext == 'pdf')
                                img = "<?php echo base_url("assets/images/file_pdf.svg") ?>";
                            else if(obj.ext == 'doc' || obj.text == 'docx')
                                img = "<?php echo base_url("assets/images/file_word.svg") ?>";
                            else if(obj.ext == 'xlsx' || obj.text == 'xls')
                                img = "<?php echo base_url("assets/images/file_excel.svg") ?>";
                            else if (obj.ext == 'jpg' || obj.ext == 'png' || obj.ext == 'gif' || obj.ext == 'jpeg')
                                img = "<?php echo base_url("assets/images/file_img.svg") ?>";
                            else
                                img = "<?php echo base_url("assets/images/file_empty.svg") ?>";

                            var html = ' <div class="col-lg-4 col-xl-3"><div class="file-man-box"><a href="javascript:;" class="del_doc" nombre="'+obj.nombre+'"><i class="bx bx-x-circle text-danger"></i></a><div class="file-img-box"><img src="'+img+'" alt="icon"></div><div class="file-man-title"><h5 class="mb-0 small">'+obj.nombre+'</h5><p class="mb-0"><small>'+parseFloat(obj.size)/1000+' KB</small></p></div> </div></div>';
                            $('#files').append(html);
                    });
            }).fail(function(jqXHR){
        if(jqXHR.status==500 || jqXHR.status==0){
            // internal server error or internet connection broke 
            var html = ' <div class="col-lg-4 col-xl-3"> <div class="file-man-box"><div class="file-img-box"><img src="https://icons.iconarchive.com/icons/custom-icon-design/pretty-office-9/256/file-warning-icon.png" alt="icon"><div class="file-man-title"><h5 class="mb-0 text-overflow">Sin Documentos Cargados</h5></div></div> </div></div>';
                            $('#files').append(html);
        }
        
    });
    verificar_firma();
        }

        $( document ).on('click', '.del_doc', function(e) {
            e.preventDefault();
            var a = $("#year").val();
            var fol = $("#folio").val();
            var name = $(this).attr('nombre');
            //alert(name);
            //eliminar_documento
            //preguntar si en realidad se desea cancelar el tramite
            Swal.fire({
                text: '¿Seguro de eliminar el documento '+name+'?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si Eliminar'
                }).then((result) => {
                    if (result.isConfirmed) {
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url("administrativo/eliminar_documento_cer"); ?>/"+a+"/"+fol+"/"+name,
                                type: "POST",
                                success: function(data){
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'info',
                                                            title: 'Mensaje del Sistema',
                                                            text: data
                                                            
                                                })
                                        
                                        get_documentos();
                                   
                                }
                            });
                    }
                })

                verificar_firma();

        });


        $( document ).on('click', '#btn_subir_archivo', function(e) {
            e.preventDefault();
            // Create an FormData object 
            var form = $("#form_archivos")[0];
            var data = new FormData(form);
            $.ajax({
            url: "<?php echo base_url("administrativo/subir_archivos_firma"); ?>",
            enctype: 'multipart/form-data',
            type: "POST",
            data: data,//form.serialize(),
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);          
                },
                success: function (data) {
                    if(data == 'ok'){
                         //mandamos mensaje de 
                         Swal.fire({
                                icon: 'info',
                                title: 'Mensaje del Sistema',
                                text: 'Los documentos fueron cargados correctamente'
                        })

                        //actalizamos los archivos
                        get_documentos();
                        $(this).next('.custom-file-label').html("");
                    }
                    else{
                        Swal.fire({
                                icon: 'error',
                                title: 'Mensaje del Sistema',
                                text: 'No se adjunto ningun documento'
                        })
                    }
                }
            });

            verificar_firma();

        });  

         
         // Add the following code if you want the name of the file appear on select
         $(".custom-file-input").on("change", function() {
            var files = [];
            for (var i = 0; i < $(this)[0].files.length; i++) {
                files.push($(this)[0].files[i].name);
            }
            $(this).next('.custom-file-label').html(files.join(', '));
        });

       </script>