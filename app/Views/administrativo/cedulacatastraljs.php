<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>
<!-- Required datatable js -->
    <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
     <!-- Buttons examples -->
     <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
       
   
       <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
       <!-- webcam -->
       <script src="<?php echo base_url("assets/js/webcam.min.js");?>"></script>
   
       <!-- App js -->
       <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
       <script src="<?php echo base_url("/assets/js/jquery.validate.js");?>"></script>
       <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>

       <script>
           $(document).ready(function () {
                cargar_cuarteles();

                $('#CTEL').blur(function() {
                    var ctel = $('#CTEL').val();
                    cargar_manzanas(ctel);
                });

                $('#CTEL').change(function() {
                    $('#MANZ').val("");
                });

                $('#MANZ').blur(function() {
                    var ctel = $('#CTEL').val();
                    var manz = $('#MANZ').val();
                    
                    cargar_predios(ctel, manz);
                });

                $('#MANZ').change(function() {
                    $('#PRED').val("");
                });

                $('#PRED').blur(function() {
                    var ctel = $('#RCTEL').val();
                    var manz = $('#RMANZ').val();
                    var pred = $('#RPRED').val();
                });  

            });
               
            function formatFriendCode(friendCode, numDigits, delimiter) {
                return Array.from(friendCode).reduce((accum, cur, idx) => {
                    return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
                }, '')
            }

            $(document).on( 'click', '#btn_consultar', function () {
                validar();
            });

            $(document).on( 'click', '#btn_imprimir_avaluo', function () {
                //var cve = $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
                var cve = $('#CTEL').val()+$('#MANZ').val()+$('#PRED').val();
                console.log(cve);
                //Crear la captura del mapa
                var img = "";
                map.once('rendercomplete', function () {
                    var mapCanvas = document.createElement('canvas');
                    var size = map.getSize();
                    mapCanvas.width = size[0];
                    mapCanvas.height = size[1];
                    var mapContext = mapCanvas.getContext('2d');
                    Array.prototype.forEach.call(
                    document.querySelectorAll('.ol-layer canvas'),
                    function (canvas) {
                        if (canvas.width > 0) {
                        var opacity = canvas.parentNode.style.opacity;
                        mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
                        var transform = canvas.style.transform;
                        // Get the transform parameters from the style's transform matrix
                        var matrix = transform
                            .match(/^matrix\(([^\(]*)\)$/)[1]
                            .split(',')
                            .map(Number);
                        // Apply the transform to the export map context
                        CanvasRenderingContext2D.prototype.setTransform.apply(
                            mapContext,
                            matrix
                        );
                        mapContext.drawImage(canvas, 0, 0);
                        }
                    }
                    );
                    if (navigator.msSaveBlob) {
                    // link download attribuute does not work on MS browsers
                    navigator.msSaveBlob(mapCanvas.msToBlob(), 'map.png');
                    } else {
                    img = mapCanvas.toDataURL('image/png');
                    var url = "/reportes/consultacedulacatastral.php";
                    var form = document.createElement("form");
                    form.setAttribute("method", "post");
                    form.setAttribute("action", url);
                    form.setAttribute("target", "popupwindow");
                    exportwindow = window.open("", "popupwindow", "width=800,height=600,resizable=yes");
                    var params = {'ccat': cve, 'img' : img};
                    for(var key in params) {
                        if(params.hasOwnProperty(key)) {
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", key);
                            hiddenField.setAttribute("value", params[key]);

                            form.appendChild(hiddenField);
                        }
                    }
                    document.body.appendChild(form);
                    form.submit();

                    }
                });
                map.renderSync();
                //console.log("ad");

            });

            $(document).on( 'click', '#ubicacion', function () {
                map.updateSize(); 
            });

            setTimeout(function() {
                map.updateSize();
            }, 100);

            

            function zeroPad(num, places) {
                var zero = places - num.toString().length + 1;
                return Array(+(zero > 0 && zero)).join("0") + num;
            }
         

        function validar() {

            // Create our number formatter.
            var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            });

             //obtener los valores de los input de la clave
           
             var ctel = $('#CTEL').val();
             var manz = $('#MANZ').val();
             var pred = $('#PRED').val();

             var cvecat = ctel + manz + pred;
             var endpoint = "<?php echo base_url("verificacion/consulta_cedulacatastral");?>/"+cvecat;
             console.log(endpoint);
             console.log("validando clave: " + cvecat)
             $.ajax({
                url: endpoint,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Avaluo...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                       console.log(data.unidad);
                    if(data.unidad.length > 0){
                        $('#propietario').val(data.unidad[0].Paterno+ " " + data.unidad[0].Materno+ " " + data.unidad[0].PNombre + " " + data.unidad[0].SNombre);
                        
                        $('#impuestotrans').val(data.unidad[0].ImpuestoPredial);

                        $('#domicilio').val(data.unidad[0].Calle + " " + data.unidad[0].CaNumeroOficiallle + " " + data.unidad[0].Colonia);

                        $('#colonia').val(data.unidad[0].Colonia);

                        $('#calle').val(data.unidad[0].Calle);

                        $('#numerooficial').val(data.unidad[0].NumeroOficial);

                        $("#uso").val(data.unidad[0].Uso_Predial);

                        $("#superficiecons").val(data.unidad[0].SuperficieFisica);
                        $("#superficieterr").val(data.unidad[0].SuperficieDocumental);

                        $('#colonia_u').val(data.unidad[0].Colonia);

                        $('#regimen').val(data.unidad[0].TipoPredio);

                        $('#supterr').val(data.unidad[0].SuperficieFisica);
                        $('#valorterr').val(data.unidad[0].ValorUnitario_Terreno);
                        $('#supcons').val(data.unidad[0].SuperficieDocumental);
                        $('#valorconst').val("0.00");
                        $('#cvezona').val(data.unidad[0].Zona);

                        $("#valorcat").val(Number(parseFloat(data.unidad[0].ValorFiscal)));

                         //llenar la tabla de terreno

                         $("#tblterreno").DataTable().destroy();                            
                            $("#tblterreno").DataTable({
                                data : data.terreno,
                                //ajax: url_terr,
                                "language": {
                                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                                },
                                responsive: true,
                                "bPaginate": false,
                                "bFilter": false,
                                "bInfo": false,
                                "ordering": false,
                                //dom: 'Bfrtip',
                                //buttons:["excel","pdf", "print", "colvis"],
                                columns : [              
                                    { "data" : "NUM_TERR" },
                                    { "data" : "SUP_TERR" },    
                                ],
                                rowCallback: function(row, data, index) {
                            
                                },
                                
                            });

                            //se obtienen los propietarios
                            $("#tblpropietarios").DataTable().destroy();
                            $("#tblpropietarios").DataTable({
                                //ajax: url_cons,
                                data : data.propietarios,
                                "language": {
                                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                                },
                                responsive: true,
                                "bPaginate": false,
                                "bFilter": false,
                                "bInfo": false,
                                "ordering": false,
                                //dom: 'Bfrtip',
                                //buttons:["excel","pdf", "print", "colvis"],
                                columns : [              
                                    { "data" : "NOM_PROP" },
                                    { "data" : "APE_PAT" },
                                    { "data" : "APE_MAT" },
                                    { "data" : "REG_FED" },                                    
                                ],
                            });
                        

                         //Se obtiene el array del poligono 
                         jsonobj = jQuery.parseJSON(data.unidad[0].jsonb_build_object);
                            //si encuentra el poligono en el vectorial
                            console.log(jsonobj);
                            if (jsonobj.features != null) {
                                //Se visualiza el poligono en el mapa
                                visualizar_capa(jsonobj);                                
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Vectorial no encontrado',
                                    text: 'La clave catastral no se encontró en el vectorial!'
                                    
                                })
                            }

                    }
                    /*
                       

                            //se obtienen los servicios
                            var url = "<?php echo base_url("verificacion/consulta_servicios/");?>/"+cvecat;
                            var elements = "";
                            $.getJSON(url, function(json){
                                    $('#servicios_list').empty();
                                    $.each(json.data, function(i, obj){
                                            
                                            elements = obj.descripcionx;
                                    });
                                    var res = elements.split(",");
                                    for (var serv in res) {
                                        var html = '<li class="list-group-item">'+res[serv]+'</li>';
                                            $('#servicios_list').append(html);
                                    }
                            });

                            var url = "<?php echo base_url("verificacion/consulta_usos");?>/"+cvecat;
                            //se obtienen los usos
                            $.getJSON(url, function(json){
                                    $('#usos_list').empty();
                                    $.each(json.data, function(i, obj){
                                        var html = '<li class="list-group-item">'+obj.CVE_USO+' - <b>'+obj.DES_USO+'</b></li>';
                                        $('#usos_list').append(html);
                                    });
                                   
                            });

                            
                            
                            //Se obtiene el array del poligono 
                            jsonobj = jQuery.parseJSON(data.unidad[0].jsonb_build_object);

                            //si encuentra el poligono en el vectorial
                            if (typeof _geojson_vectorSource != "undefined") {
                                _geojson_vectorSource.clear();
                            }
                            if (jsonobj.features != null) {
                                //Se visualiza el poligono en el mapa
     
                                visualizar_capa(jsonobj);

                                map.updateSize(); 
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Vectorial no encontrado',
                                    text: 'La clave catastral no se encontró en el vectorial!'
                                    
                                })
                            }
                        }
                            
                       }*/
                       else {
  
                           //verificar si se encuentra de primer registro
                           Swal.fire({
                            title: 'Clave Catastral no encontrada',
                            icon: 'error',
                            }).then((result) => {
                               
                            })

                           
                       }          
                    },
                   complete : function (){
                        $.unblockUI();
                    }
               });
        }


       

        function cargar_cuarteles(){
            $('#list_cuarteles').empty();
            var url = "<?php echo base_url("padron/list_cuarteles");?>";
            //console.log(url);
            $.getJSON(url, function(json){
                    //console.log(json);
                    $.each(json.data, function(i, obj){
                            $('#list_cuarteles').append($('<option>').val(obj.cve_ctel));
                           
                    });    
            });                       
         }

         function cargar_manzanas(ctel){
            $('#list_manzanas').empty();
            var url = "<?php echo base_url("padron/list_manzanas");?>/"+ctel;
            //console.log(url);
            $.getJSON(url, function(json){
                    //console.log(json);
                    $.each(json.data, function(i, obj){
                            let manz = zeroPad(obj.NUM_MANZ, 3);
                            $('#list_manzanas').append($('<option>').val(manz));      
                    });    
            });                       
         }

         function cargar_predios(ctel, manz){
            $('#list_predios').empty();
            var url = "<?php echo base_url("padron/list_predios");?>/"+ctel+"/"+manz;
            //console.log(url);
            $.getJSON(url, function(json){
                    //console.log(json);
                    $.each(json.data, function(i, obj){
                            let pred = zeroPad(obj.Predio, 3);
                            $('#list_predios').append($('<option>').val(pred));      
                    });    
            });                       
         }


       </script>