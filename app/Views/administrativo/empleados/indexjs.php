<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    <!-- Datatable init js -->
    <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>

    

    <script>

        $(document).ready(function () {
            $("#btn_actualizar").hide();
            
            var table = $("#tbl_usuarios").DataTable({
                ajax: "<?php echo base_url("administrativo/get_empleados2")?>",
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                responsive: false,
                columns : [
                    { "data" : "IdEmpleado" },
                    { "data" : "Nombre" },
                    { "data" : "ApellidoPaterno" },
                    { "data" : "ApellidoMaterno" },
                    { "data" : "IdPuesto" },
                    {
                        targets: -1,
                        data: null,
                        defaultContent: '<a class="editar text-primary" style="cursor:pointer !important; margin-left:5px;"> <i class="bx bx-edit-alt bx-xs bx-burst-hover"></i></a><a class="eliminar text-danger" style="cursor:pointer !important; margin-left:5px;"><i class="bx bx-trash-alt bx-xs bx-burst-hover"></i> </a>'
                    } 
                ],
                rowCallback: function(row, data, index) {
                    $("td:eq(-1)", row).addClass("text-center");
                }
            });

            $('#tbl_usuarios tbody').on( 'click', '.editar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //llenar los campos del modal con los de la tabla
                $("#idemp2").val(data["IdEmpleado"]);
                $("#idemp").val(data["IdEmpleado"]);
                $("#nombre").val(data["Nombre"]);
                $("#apellidopat").val(data["ApellidoPaterno"]);
                $("#apellidomat").val(data["ApellidoMaterno"]);
                $("#puesto").val(data["IdPuesto"]);
                //mostrar el modal y mostrar el boton de actualizar
                $(".modal-registro").modal("show");
                $(".modal-title").text("Actualizar Empleado");
                $("#btn_guardar").hide();
                $("#btn_actualizar").show();
                
            });

            $('#tbl_usuarios tbody').on( 'click', '.eliminar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //alert( "Eliminar: " + data[0] );
                var id = data["IdEmpleado"];
                var nombre = data["Nombre"];
                
                Swal.fire({
                title: '¿Seguro de eliminar el Empleado: '+nombre+'?',
                type: 'question',
                //imageUrl:"<?php echo base_url("assets/images/logo_c.png") ?>",
                imageHeight:80,
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Si, Eliminar!'
                }).then((result) => {
                    if (result.value) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url("administrativo/eliminar_empleado/"); ?>",
                        data: {'id': id},
                        success: function(data){
                            $('#tbl_usuarios').DataTable().ajax.reload(null, false);
                            if(data == 'ok'){
                                Swal.fire({
                                type: 'success',
                                title: "Empleado eliminado correctamente",
                                showConfirmButton: false,
                                timer: 1500
                            })
                            }
                            else{
                                Swal.fire({
                                type: 'error',
                                title: "Ocurrio un error al eliminar el empleado",
                                showConfirmButton: false,
                                timer: 1500
                            })
                            }
                            
                           
                        }
                    });
                    }
                })
            });

            
        });
        
        $(document).on('click', '#btn_nuevo',  function(e){
            e.preventDefault();
            $("#form_empleados")[0].reset();
            $(".modal-title").text("Registrar Nuevo Empleado");
            $("#btn_guardar").show();
            $(".modal-registro").modal("show");
        });

        $(document).on('click', '#btn_guardar',  function(e){
            e.preventDefault();
          var form =  $("#form_empleados");
          form.validate({
            messages: {
            idemp:  "* Id de EmpleadoRequerido",
            nombre:  "* Nombre Requerido",
            apellidopat: "* Apellido Paterno requerido",
            apellidomat: "* Apellido Materno requerido",
            puesto: "* Puesto requerido"
        }
          });
          console.log(form);
          if(form.valid()){
          $.ajax({
            url: "<?php echo base_url("administrativo/registrar_empleado"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //reset el form
                    document.getElementById('form_empleados').reset();
                    //actualizar tabla y cerrar modal
                    $('#tbl_usuarios').DataTable().ajax.reload(null, false);
                    $(".modal-registro").modal("hide");
                    //desplegar mensaje del controlador
                    //alert(data);
                    if(data == 'ok'){
                        Swal.fire({
                        //title:"Good job!",
                        text: "Empleado Guardado correctamente",
                        type: "success",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                    }else{
                        Swal.fire({
                        //title:"Good job!",
                        text: "Ocurrio un error al guardar el empleado",
                        type: "error",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                    }
                    
                }
            });
          }

          
        });

        
        $(document).on('click', '#btn_actualizar',  function(e){
            e.preventDefault();
          var form =  $("#form_empleados");
          form.validate({
            messages: {
            idemp:  "* Id de EmpleadoRequerido",
            nombre:  "* Nombre Requerido",
            apellidopat: "* Apellido Paterno requerido",
            apellidomat: "* Apellido Materno requerido",
            puesto: "* Puesto requerido"
        }
          });
          console.log(form);
          if(form.valid()){
          $.ajax({
            url: "<?php echo base_url("administrativo/actualizar_empleado"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //reset el form
                    document.getElementById('form_empleados').reset();
                    //actualizar tabla y cerrar modal
                    $('#tbl_usuarios').DataTable().ajax.reload(null, false);
                    $(".modal-registro").modal("hide");
                    $("#btn_guardar").show();
                    $("#btn_actualizar").hide();
                    //desplegar mensaje del controlador
                    //alert(data);
                    if(data == 'ok'){
                        Swal.fire({
                        //title:"Good job!",
                        text: "Empleado Actualizado correctamente",
                        type: "success",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                       

                    }else{
                        Swal.fire({
                        //title:"Good job!",
                        text: "Ocurrio un error al actualizar el empleado",
                        type: "error",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                    }
                    
                }
            });
          }

          
        });
         
    </script>