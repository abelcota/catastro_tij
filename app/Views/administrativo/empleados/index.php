 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">ADMINISTRACIÓN DE EMPLEADOS</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">ADMINISTRATIVO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="btn-toolbar p-3" role="toolbar">

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_nuevo">
                                            <i class="dripicons-user-group"></i> Registrar Empleado
                                            </button>
                                            
                                        </div>
                                        <?= view('Myth\Auth\Views\_message_block') ?>
                                    </div>
                                    <!--end toolbar -->

                      <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">       
                                <div class="float-right">
                                  
                                    <i class="bx bx-edit-alt bx-xs text-info"></i><small> Editar</small>
                                    <i class="bx bx-trash-alt bx-xs text-danger"></i><small> Eliminar</small>
                                </div>
                                
                <table class="table table-hover table-sm" id="tbl_usuarios">
                <thead>
                    <tr>
                        <th>ID</th> 
                        <th>Nombre</th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Puesto</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                </table>

                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            <!-- modal para registrar un nuevo rol -->
<div class="modal fade modal-registro" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                    <form name="form_empleados" id="form_empleados" method="post" accept-charset="utf-8">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Registrar Nuevo Empleado</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                        
                                                        
                                                            <div class="form-group row mb-4">
                                                            
                                                                <label for="nombre" class="col-form-label col-lg-3">IdEmpleado: </label>
                                                                <div class="col-lg-9">
                                                    
                                                                <input type="text" class="form-control" name="idemp" id="idemp" placeholder="" required maxlength="4">
                                                                </div>
                                                                <input type="hidden" class="form-control" name="idemp2" id="idemp2" placeholder="" maxlength="4">
                                                                
                                                            </div>
                                                            <div class="form-group row mb-4">
                                                                <label class="col-form-label col-lg-3">Nombre</label>
                                                                <div class="col-lg-9">
                                                                <input type="text" class="form-control" name="nombre" id="nombre" aria-describedby="emailHelp" placeholder="" required>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-4">
                                                            
                                                                <label for="nombre" class="col-form-label col-lg-3">Apellido Paterno</label>
                                                                <div class="col-lg-9">
                                                    
                                                                <input type="text" name="apellidopat" id="apellidopat" class="form-control " placeholder="" autocomplete="off" required>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-4">
                                                            
                                                                <label for="nombre" class="col-form-label col-lg-3">Apellido Materno</label>
                                                                <div class="col-lg-9">
                                                    
                                                                <input type="text" name="apellidomat" id="apellidomat" class="form-control" placeholder="" autocomplete="off" required>
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-4">
                                                            
                                                                <label for="nombre" class="col-form-label col-lg-3">Puesto</label>
                                                                <div class="col-lg-9">
                                                    
                                                                <input type="text" name="puesto" id="puesto" class="form-control" placeholder="" autocomplete="off" required maxlength="2">
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="modal-footer">
                                                        
                                                            <button type="button" class="btn btn-success" id="btn_guardar">Registrar</button>
                                                            <button type="button" class="btn btn-primary" id="btn_actualizar">Actualizar</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar">Cancelar</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->