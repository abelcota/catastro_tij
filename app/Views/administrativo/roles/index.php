 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">ADMINISTRACIÓN DE ROLES</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">ADMINISTRATIVO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">

                     <div class="btn-toolbar p-3" role="toolbar">

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_nuevo">
                                            <i class="dripicons-user-group"></i> Registrar Nuevo Rol
                                            </button>
                                            
                                        </div>
                                    </div>
                                    <!--end toolbar -->

                      <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">       

                                <div class="float-right">
               
                                    <i class="bx bx-edit-alt bx-xs text-info"></i><small> Editar</small>
                                    <i class="bx bx-trash-alt bx-xs text-danger"></i><small> Eliminar</small>
                                </div>
                <table class="table table-striped dt-responsive nowrap dataTable no-footer dtr-inline collapsed" id="tblroles" width="100%">
                <thead>
                    <tr>
                        <th>ID</th> 
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    
                    </tbody>
                </table>

                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->


<!-- modal para registrar un nuevo rol -->
<div class="modal fade modal-registro" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                    <form action="<?php echo base_url('administrativo/roles');?>" name="roles_create" id="roles_create" method="post" accept-charset="utf-8">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Registrar Nuevo Rol</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                                     
                                                            <div class="form-group row mb-4">
                                                                <label for="nombre" class="col-form-label col-lg-3">Nombre:</label>
                                                                <div class="col-lg-9">
                                                    
                                                                <input id="id" name="id" type="hidden" class="form-control">

                                                                    <input id="nombre" name="nombre" type="text" class="form-control" placeholder="" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-4">
                                                                <label class="col-form-label col-lg-3">Descripción:</label>
                                                                <div class="col-lg-9">
                                                                <textarea rows="2" name="descripcion" id="descripcion" class="form-control" required></textarea >
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" id="btn_guardar">Guardar</button>
                                                            <button type="button" class="btn btn-primary" id="btn_actualizar">Actualizar</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar">Cancelar</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->