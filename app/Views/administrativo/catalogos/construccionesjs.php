<!-- Required datatable js -->
<script src="<?php echo base_url("assets/js/jquery.dataTables1_10_24.min.js");?>"></script>
<script src="<?php echo base_url("assets/js/dataTables.bootstrap4.min.js");?>"></script>
    
    <script>
         $(document).ready(function () {
             
             $("#btn_actualizar").hide();

            var table = $("#tblconstrucciones").DataTable({
                processing: true,
                serverSide: true,
                ajax: "<?php echo base_url("padron/construcciones2")?>",
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                responsive: true,
                deferRender: true,
                pageLength: 10,
                lengthMenu: [[ 10, 25, 50, 100], [ 10, 25, 50, 100]],
                columns : [
                    { "data" : "clave" },
                    { "data" : "NUM_CONST" },
                    { "data" : "SUP_CONST" },
                    { "data" : "CVE_CAT_CO" },
                    { "data" : "CVE_EDO_CO" },
                    { "data" : "EDD_CONST" },
                    { "data" : "FEC_CONST" },
                    
                ],
                rowCallback: function(row, data, index) {
                    
                    $("td:eq(0)", row).addClass("font-weight-bold"); 
                }
            });

            $('#tblconstrucciones tbody').on( 'click', '.eliminar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //alert( "Eliminar: " + data[0] );
                var id = data["NUM_CONST"];
                var nombre = data["clave"];
                
                Swal.fire({
                title: '¿Seguro de eliminar la Construccion: '+nombre+'?',
                type: 'question',
                //imageUrl:"<?php echo base_url("assets/images/logo_c.png") ?>",
                imageHeight:80,
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Si, Eliminar!'
                }).then((result) => {
                    if (result.value) {
                    /*$.ajax({
                        type: 'POST',
                        url: "<?php echo base_url("construcciones/eliminar/"); ?>",
                        data: {'id': id},
                        success: function(data){
                            //console.log(data);
                            $('#tblmanzanas').DataTable().ajax.reload(null, false);
                            Swal.fire({
                                type: 'success',
                                title: data,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    });*/
                    }
                })
            });

            $('#tblmanzanas tbody').on( 'click', '.editar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //llenar los campos del modal con los de la tabla
                /*$("#id").val(data["id"]);
                $("#nombre").val(data["name"]);
                $("#descripcion").val(data["description"]);
                //mostrar el modal y mostrar el boton de actualizar
                $(".modal-registro").modal("show");
                $(".modal-title").text("Actualizar Rol");
                $("#btn_guardar").hide();
                $("#btn_actualizar").show();*/
                
            });


         });

         $(document).on('click', '#btn_guardar',  function(e){
            e.preventDefault();
           
            var form = $("#roles_create");
            console.log(form.serialize());

            /*$.ajax({
            url: "<?php //echo base_url("roles/registro"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //reset el form
                    document.getElementById('roles_create').reset();
                    //actualizar tabla y cerrar modal
                    $('#tblmanzanas').DataTable().ajax.reload(null, false);
                    $(".modal-registro").modal("hide");
                    //desplegar mensaje del controlador
                    //alert(data);
                    Swal.fire({
                        //title:"Good job!",
                        text: data,
                        type:"success",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                }
            });*/

        });

        $(document).on('click', '#btn_nuevo',  function(e){
            e.preventDefault();
            $("#roles_create")[0].reset();
            $(".modal-title").text("Registrar Nueva Manzana");
            $("#btn_guardar").show();
            $("#btn_actualizar").hide();
            $(".modal-registro").modal("show");
        });
        
        $(document).on('click', '#btn_actualizar',  function(e){
            e.preventDefault();
            var form = $("#roles_create");
            console.log(form.serialize());

            $.ajax({
            url: "<?php echo base_url("colonias/editar"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //reset el form
                    document.getElementById('roles_create').reset();
                    //actualizar tabla y cerrar modal
                    $('#tblmanzanas').DataTable().ajax.reload(null, false);
                    $(".modal-registro").modal("hide");
                    //desplegar mensaje del controlador
                    $(".modal-title").text("Registrar Nueva Manzana");
                    $("#btn_guardar").show();
                    $("#btn_actualizar").hide();
                    Swal.fire({
                        //title:"Good job!",
                        text: data,
                        type:"success",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                }
            });

        });

         
    </script>