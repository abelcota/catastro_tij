<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">CATÁLOGO DE MANZANAS</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">ADMINISTRATIVO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">

                     <div class="btn-toolbar p-3" role="toolbar">

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_nuevo">
                                            <i class="mdi mdi-plus"></i> Registrar Manzanas
                                            </button>
                                            
                                        </div>
                                    </div>
                                    <!--end toolbar -->

                      <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">       

    
                <table class="table table-hover" id="tblmanzanas" width="100%">
                <thead class="text-white" style="background-color: #480912;">
                    <tr>
                        <th>Población</th>
                        <th>Zona</th>
                        <th>Manzana</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    
                    </tbody>
                </table>

                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->


<!-- modal para registrar un nuevo rol -->
<div class="modal fade modal-registro" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                    <form action="<?php echo base_url('administrativo/colonias');?>" name="roles_create" id="roles_create" method="post" accept-charset="utf-8">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Registrar Nueva Manzana</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                                     
                                                            <div class="form-group row mb-4">
                                                                <label for="nombre" class="col-form-label col-lg-3">Clave Poblacion:</label>
                                                                <div class="col-lg-9">
                                                    
                                                                <input id="cve_poblh" name="cve_poblh" type="hidden" class="form-control">

                                                                <input id="cve_pobl" name="cve_pobl" type="text" class="form-control" placeholder="">
                                                                
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-4">
                                                                <label for="nombre" class="col-form-label col-lg-3">Número de Cuartél:</label>
                                                                <div class="col-lg-9">
                                                                <input id="num_ctel" name="num_ctel" type="text" class="form-control" placeholder="">
                                                                
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-4">
                                                                <label for="nombre" class="col-form-label col-lg-3">Número de Manzana:</label>
                                                                <div class="col-lg-9">
                                                                <input id="num_manz" name="num_manz" type="text" class="form-control" placeholder="">
                                                                
                                                                </div>
                                                            </div>

                                                            <div class="form-group row mb-4">
                                                                <label for="nombre" class="col-form-label col-lg-3">Clave de Zona:</label>
                                                                <div class="col-lg-9">
                                                                <input id="cve_zona" name="cve_zona" type="text" class="form-control" placeholder="">
                                                                
                                                                </div>
                                                            </div>
                                                           
                                                            
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" id="btn_guardar">Guardar</button>
                                                            <button type="button" class="btn btn-primary" id="btn_actualizar">Actualizar</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar">Cancelar</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->