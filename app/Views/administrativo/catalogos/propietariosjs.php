
    <script src="<?php echo base_url("assets/js/jquery.dataTables1_10_24.min.js");?>"></script>
    <script src="<?php echo base_url("assets/js/dataTables.bootstrap4.min.js");?>"></script>
    <!-- Magnific Popup-->
    <script src="<?php echo base_url("assets/libs/magnific-popup/jquery.magnific-popup.min.js");?>"></script>

    <script>
         $(document).ready(function () {
             
             $("#btn_actualizar").hide();

            var table = $("#tblpropietarios").DataTable({
               
                ajax: "<?php echo base_url("padron/get_propietarios")?>",
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
            
                columns : [
                    { "data" : "CLAVE" },
                    { "data" : "NOM_PROP" },
                    { "data" : "APE_PAT" },
                    { "data" : "APE_MAT" },
                    { "data" : "REG_FED" },
                    
                ],
                rowCallback: function(row, data, index) {
                    
                    
                    $("td:eq(0)", row).addClass("text-primary"); 
                    $("td:eq(1)", row).addClass("font-weight-bold"); 
                }
            });

            $('#tblpropietarios tbody').on( 'click', '.eliminar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //alert( "Eliminar: " + data[0] );
                var id = data["CVE_PROP"];
                var nombre = data["CVE_PROP"];
                
                Swal.fire({
                title: '¿Seguro de eliminar el Propietario: '+nombre+'?',
                type: 'question',
                //imageUrl:"<?php echo base_url("assets/images/logo_c.png") ?>",
                imageHeight:80,
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Si, Eliminar!'
                }).then((result) => {
                    if (result.value) {
                    /*$.ajax({
                        type: 'POST',
                        url: "<?php echo base_url("propietarios/eliminar/"); ?>",
                        data: {'id': id},
                        success: function(data){
                            //console.log(data);
                            $('#tblpropietarios').DataTable().ajax.reload(null, false);
                            Swal.fire({
                                type: 'success',
                                title: data,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    });*/
                    }
                })
            });

            $('#tblpropietarios tbody').on( 'click', '.editar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //llenar los campos del modal con los de la tabla
                /*$("#id").val(data["id"]);
                $("#nombre").val(data["name"]);
                $("#descripcion").val(data["description"]);
                //mostrar el modal y mostrar el boton de actualizar
                $(".modal-registro").modal("show");
                $(".modal-title").text("Actualizar Rol");
                $("#btn_guardar").hide();
                $("#btn_actualizar").show();*/
                
            });


         });

         $(document).on('click', '#btn_guardar',  function(e){
            e.preventDefault();
           
            var form = $("#roles_create");
            console.log(form.serialize());

            /*$.ajax({
            url: "<?php //echo base_url("roles/registro"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //reset el form
                    document.getElementById('roles_create').reset();
                    //actualizar tabla y cerrar modal
                    $('#tblpropietarios').DataTable().ajax.reload(null, false);
                    $(".modal-registro").modal("hide");
                    //desplegar mensaje del controlador
                    //alert(data);
                    Swal.fire({
                        //title:"Good job!",
                        text: data,
                        type:"success",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                }
            });*/

        });

        $(document).on('click', '#btn_nuevo',  function(e){
            e.preventDefault();
            $("#roles_create")[0].reset();
            $(".modal-title").text("Registrar Nuevo Propietario");
            $("#btn_guardar").show();
            $("#btn_actualizar").hide();
            $(".modal-registro").modal("show");
        });
        
        $(document).on('click', '#btn_actualizar',  function(e){
            e.preventDefault();
            var form = $("#roles_create");
            console.log(form.serialize());

            $.ajax({
            url: "<?php echo base_url("propietarios/editar"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //reset el form
                    document.getElementById('roles_create').reset();
                    //actualizar tabla y cerrar modal
                    $('#tblpropietarios').DataTable().ajax.reload(null, false);
                    $(".modal-registro").modal("hide");
                    //desplegar mensaje del controlador
                    $(".modal-title").text("Registrar Nuevo Propietario");
                    $("#btn_guardar").show();
                    $("#btn_actualizar").hide();
                    Swal.fire({
                        //title:"Good job!",
                        text: data,
                        type:"success",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                }
            });

        });

         
    </script>