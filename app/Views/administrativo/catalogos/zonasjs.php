<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    <!-- Datatable init js -->

    <script>
         $(document).ready(function () {
             
             $("#btn_actualizar").hide();

            var table = $("#tblzonas").DataTable({
                ajax: "<?php echo base_url("padron/zonas")?>",
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                responsive: true,
                processing: true,
                deferRender:    true,
                pageLength: 10,
                columns : [
                    { "data" : "id" },
                    { "data" : "CVE_ZONA" },
                    { "data" : "VAL_UNIT_T" },
                    { "data" : "FAC_DEM_VA" },
                    { "data" : "NOM_POBL" },
                    {
                        targets: -1,
                        data: null,
                        defaultContent: '<a class="editar text-primary" style="cursor:pointer !important; margin-left:5px;"> <i class="bx bx-edit-alt bx-xs"></i></a> <a class="eliminar text-danger" style="cursor:pointer !important; margin-left:5px;"><i class="bx bx-trash-alt bx-xs"></i> </a>'
                    } 
                    
                ],
                rowCallback: function(row, data, index) {
                    
                    $("td:eq(1)", row).addClass("font-weight-bold"); 
                    $("td:eq(2)", row).text(formatter.format(data['VAL_UNIT_T'])); 
                    $("td:eq(3)", row).addClass("text-center"); 
                }
            });

            var formatter = new Intl.NumberFormat('en-US', {
                            style: 'currency',
                            currency: 'USD',
                            });

            $('#tblzonas tbody').on( 'click', '.eliminar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //alert( "Eliminar: " + data[0] );
                var id = data["CVE_ZONA"];
                var nombre = data["DES_ZONA"];
                
                Swal.fire({
                text: '¿Seguro de eliminar la Zona: '+nombre+'?',
                type: 'question',
                imageHeight:80,
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Si, Eliminar!'
                }).then((result) => {
                    if (result.value) {
                    /*$.ajax({
                        type: 'POST',
                        url: "<?php //echo base_url("padron/eliminar_zonas/"); ?>",
                        data: {'id': id},
                        success: function(data){
                            //console.log(data);
                            $('#tblzonas').DataTable().ajax.reload(null, false);
                            Swal.fire({
                                type: 'success',
                                title: data,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    });*/
                    }
                })
            });

            $('#tblzonas tbody').on( 'click', '.editar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //llenar los campos del modal con los de la tabla
                $("#cve_pobl").val(data["CVE_POBL"]);
                $("#cve_zona").val(data["CVE_ZONA"]);
                $("#des_zona").val(data["DES_ZONA"]);
                $("#val_unit_t").val(data["VAL_UNIT_T"]);
                $("#fac_dem_va").val(data["FAC_DEM_VA"]);
                //mostrar el modal y mostrar el boton de actualizar
                $(".modal-registro").modal("show");
                $(".modal-title").text("Actualizar Zona");
                $("#btn_guardar").hide();
                $("#btn_actualizar").show();
                
            });


         });

         $(document).on('click', '#btn_guardar',  function(e){
            e.preventDefault();
           
            var form = $("#roles_create");
            console.log(form.serialize());

            /*$.ajax({
            url: "<?php //echo base_url("padron/registrar_zona"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //reset el form
                    document.getElementById('roles_create').reset();
                    //actualizar tabla y cerrar modal
                    $('#tblzonas').DataTable().ajax.reload(null, false);
                    $(".modal-registro").modal("hide");
                    //desplegar mensaje del controlador
                    //alert(data);
                    Swal.fire({
                        //title:"Good job!",
                        text: data,
                        type:"success",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                }
            });*/

        });

        $(document).on('click', '#btn_nuevo',  function(e){
            e.preventDefault();
            $("#roles_create")[0].reset();
            $(".modal-title").text("Registrar Nueva Zona");
            $("#btn_guardar").show();
            $("#btn_actualizar").hide();
            $(".modal-registro").modal("show");
        });
        
        $(document).on('click', '#btn_actualizar',  function(e){
            e.preventDefault();
            var form = $("#roles_create");
            console.log(form.serialize());

            /*$.ajax({
            url: "<?php //echo base_url("padron/actualizar_zona"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //reset el form
                    document.getElementById('roles_create').reset();
                    //actualizar tabla y cerrar modal
                    $('#tblzonas').DataTable().ajax.reload(null, false);
                    $(".modal-registro").modal("hide");
                    //desplegar mensaje del controlador
                    $(".modal-title").text("Registrar Nueva Zona");
                    $("#btn_guardar").show();
                    $("#btn_actualizar").hide();
                    Swal.fire({
                        //title:"Good job!",
                        text: data,
                        type:"success",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                }
            });*/

        });

         
    </script>