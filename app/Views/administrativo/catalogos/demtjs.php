<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    <!-- Datatable init js -->

    <script>
         $(document).ready(function () {
             
            var table = $("#tbldemt").DataTable({
                ajax: "<?php echo base_url("padron/demt")?>",
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                responsive: true,
                processing : true,
                deferRender:    true,
                pageLength: 10,
                columns : [
                    { "data" : "CVE_DEM_TE" },
                    { "data" : "DES_DEM_TE" },
                    { "data" : "FAC_DEM_TE" },
                    
                ],
                rowCallback: function(row, data, index) {
                    
                    $("td:eq(1)", row).addClass("font-weight-bold"); 
                }
            });


         });
</script>
