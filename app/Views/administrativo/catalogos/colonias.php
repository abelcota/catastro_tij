 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">CATÁLOGO DE COLONIAS</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">ADMINISTRATIVO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">

                     <div class="btn-toolbar p-3" role="toolbar">

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_nuevo">
                                            <i class="mdi mdi-sync"></i> Actualizar Colonias
                                            </button>
                                            
                                        </div>
                                    </div>
                                    <!--end toolbar -->

                      <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">       

    
                <table class="table table-hover" id="tblcolonias" width="100%">
                <thead class="text-white" style="background-color: #480912;">
                    <tr>
                        <th>Clave</th>
                        <th>Colonia</th>
                        <th>Delegación</th>
                        <th>Población</th> 
                        
                    </tr>
                </thead>
                <tbody>
                    
                    </tbody>
                </table>

                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->


<!-- modal para registrar un nuevo rol -->
<div class="modal fade modal-registro" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                    <form action="<?php echo base_url('administrativo/colonias');?>" name="roles_create" id="roles_create" method="post" accept-charset="utf-8">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Registrar Nueva Colonia</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                                     
                                                            <div class="form-group row mb-4">
                                                                <label for="nombre" class="col-form-label col-lg-3">Clave Poblacion:</label>
                                                                <div class="col-lg-9">
                                                    
                                                                <input id="cve_pobl" name="cve_pobl" type="hidden" class="form-control">

                                                                    <input id="nombre" name="nombre" type="text" class="form-control" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-4">
                                                                <label for="nombre" class="col-form-label col-lg-3">Clave Colonia:</label>
                                                                <div class="col-lg-9">
                                                    
                                                                <input id="cve_calle" name="cve_calle" type="hidden" class="form-control">

                                                                    <input id="nombre" name="nombre" type="text" class="form-control" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-4">
                                                                <label class="col-form-label col-lg-3">Nombre de la Colonia:</label>
                                                                <div class="col-lg-9">
                                                                <textarea rows="2" name="nom_calle" id="nom_calle" class="form-control"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row mb-4">
                                                                <label class="col-form-label col-lg-3">Código Postál:</label>
                                                                <div class="col-lg-9">
                                                                <textarea rows="2" name="nom_calle" id="nom_calle" class="form-control"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" id="btn_guardar">Guardar</button>
                                                            <button type="button" class="btn btn-primary" id="btn_actualizar">Actualizar</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar">Cancelar</button>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->