<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">CATÁLOGO DE TERRENOS</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">ADMINISTRATIVO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">

                     <div class="btn-toolbar p-3" role="toolbar">

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_nuevo">
                                            <i class="mdi mdi-sync"></i> Actualizar Terrenos
                                            </button>
                                            
                                        </div>
                                    </div>
                                    <!--end toolbar -->

                      <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">       

                <div class="table-responsive">
                <table class="table table-hover nowrap" id="tblterrenos" width="100%">
                <thead class="text-white" style="background-color: #480912;">
                    <tr>
                        <th>Clave Cat.</th> 
                        <th>Poblacion</th> 
                        <th>Terreno</th>
                        <th>Tipo</th>
                        <th>Sup M2</th>
                        <th>Calle</th>
                        <th>Tramo</th>
                        <th>Sup E.</th>
                        <th>Calle E.</th>
                        <th>Tramo E.</th>
                        <th>Sup Dem</th>
                        <th>Factor Dem</th>
                        
                    </tr>
                </thead>
                <tbody>
                    
                    </tbody>
                </table>
</div>

                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

