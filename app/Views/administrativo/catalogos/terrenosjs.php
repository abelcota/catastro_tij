
    <script src="<?php echo base_url("assets/js/jquery.dataTables1_10_24.min.js");?>"></script>
    <script src="<?php echo base_url("assets/js/dataTables.bootstrap4.min.js");?>"></script>

    <script>
         $(document).ready(function () {
             
            $("#btn_actualizar").hide();

            $('#tblterrenos').DataTable({
                processing: true,
                serverSide: true,
                ajax: '<?php echo site_url('padron/terrenos'); ?>',
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                deferRender: true,
                pageLength: 10,
                lengthMenu: [[ 10, 25, 50, 100], [ 10, 25, 50, 100]],
                columns : [
                    { "data" : "clave" },
                    { "data" : "CVE_POBL" },
                    { "data" : "NUM_TERR" },
                    { "data" : "TIP_TERR" },
                    { "data" : "SUP_TERR" },
                    { "data" : "CVE_CALLE" },
                    { "data" : "CVE_TRAMO" },
                    { "data" : "SUP_TERR_E" },
                    { "data" : "CVE_CALLE_" },
                    { "data" : "CVE_TRAMO_" },
                    { "data" : "SUP_TERR_D" },
                    { "data" : "CVE_DEM_TE" },                  
                ],
                rowCallback: function(row, data, index) {
                    
                    $("td:eq(0)", row).addClass("font-weight-bold"); 
                }
            });
         });
        

         
    </script>