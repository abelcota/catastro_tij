
 <!-- Required datatable js -->
 <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>


    <!-- Datatable init js -->

    <script>

        function cargar_poblaciones(){
                $('#list_poblaciones').empty();
                $.getJSON("<?php echo base_url("padron/poblaciones");?>", function(json){
                        //console.log(json);
                        $.each(json.data, function(i, obj){
                                $('#list_poblaciones').append($('<option>').val(obj.CVE_POBL + " - " + obj.NOM_POBL));
                        });    
                });                       
            }

         $(document).ready(function () {

            cargar_poblaciones();
             
             $("#btn_actualizar").hide();

            var table = $("#tblcalles").DataTable({
                ajax: "<?php echo base_url("padron/calles")?>",
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                responsive: true,
                processing: true,
                deferRender: true,
                pagination: true,
                pageLength: 10,
                columns : [              
                    { "data" : "id" },
                    { "data" : "NOM_CALLE" },
                    { "data" : "COLONIA" },
                    { "data" : "NOM_POBL" },
                    {
                        targets: -1,
                        data: null,
                        defaultContent: '<a class="editar text-primary" style="cursor:pointer !important; margin-left:5px;"> <i class="bx bx-edit-alt bx-xs"></i></a> <a class="eliminar text-danger" style="cursor:pointer !important; margin-left:5px;"><i class="bx bx-trash-alt bx-xs"></i> </a>'
                    } 
                    
                ],
                rowCallback: function(row, data, index) {
                    
                    $("td:eq(1)", row).addClass("font-weight-bold"); 
                }
            });

            $('#tblcalles tbody').on( 'click', '.eliminar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //alert( "Eliminar: " + data[0] );
                var id = data["CVE_CALLE"];
                var nombre = data["NOM_CALLE"];
                
                Swal.fire({
                title: '¿Seguro de eliminar la Calle: '+nombre+'?',
                type: 'question',
                imageHeight:80,
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Si, Eliminar!'
                }).then((result) => {
                    if (result.value) {
                    /*$.ajax({
                        type: 'POST',
                        url: "<?php //echo base_url("padron/eliminar_calle/"); ?>",
                        data: {'id': id},
                        success: function(data){
                            //console.log(data);
                            $('#tblroles').DataTable().ajax.reload(null, false);
                            Swal.fire({
                                type: 'success',
                                title: data,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    });*/
                    }
                })
            });

            $('#tblcalles tbody').on( 'click', '.editar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                //llenar los campos del modal con los de la tabla
                $("#cve_pobl").val(data["CVE_POBL"]);
                $("#cve_calle").val(data["CVE_CALLE"]);
                $("#nom_calle").val(data["NOM_CALLE"]);
                //mostrar el modal y mostrar el boton de actualizar
                $(".modal-registro").modal("show");
                $(".modal-title").text("Actualizar Calle");
                $("#btn_guardar").hide();
                $("#btn_actualizar").show();
                
            });


         });

         $(document).on('click', '#btn_guardar',  function(e){
            e.preventDefault();
           
            var form = $("#roles_create");
            console.log(form.serialize());

            /*$.ajax({
            url: "<?php //echo base_url("roles/registro"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //reset el form
                    document.getElementById('roles_create').reset();
                    //actualizar tabla y cerrar modal
                    $('#tblroles').DataTable().ajax.reload(null, false);
                    $(".modal-registro").modal("hide");
                    //desplegar mensaje del controlador
                    //alert(data);
                    Swal.fire({
                        //title:"Good job!",
                        text: data,
                        type:"success",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                }
            });*/

        });

        $(document).on('click', '#btn_nuevo',  function(e){
            e.preventDefault();
            $("#roles_create")[0].reset();
            $(".modal-title").text("Registrar Nueva Calle");
            $("#btn_guardar").show();
            $("#btn_actualizar").hide();
            
            $(".modal-registro").modal("show");
        });
        
        $(document).on('click', '#btn_actualizar',  function(e){
            e.preventDefault();
            var form = $("#roles_create");
            console.log(form.serialize());

            /*$.ajax({
            url: "<?php //echo base_url("padron/editar_calle"); ?>",
            type: "POST",
            data: form.serialize(),
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function (data) {
                    //reset el form
                    document.getElementById('roles_create').reset();
                    //actualizar tabla y cerrar modal
                    $('#tblroles').DataTable().ajax.reload(null, false);
                    $(".modal-registro").modal("hide");
                    //desplegar mensaje del controlador
                    $(".modal-title").text("Registrar Nuevo Rol");
                    $("#btn_guardar").show();
                    $("#btn_actualizar").hide();
                    Swal.fire({
                        //title:"Good job!",
                        text: data,
                        type:"success",
                        showCancelButton:!0,
                        confirmButtonColor:"#3b5de7",
                        cancelButtonColor:"#f46a6a"
                    })
                }
            });*/

        });

         
    </script>