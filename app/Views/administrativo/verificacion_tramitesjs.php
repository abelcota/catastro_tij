<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>


    <!-- Required datatable js -->
    <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
     <!-- Buttons examples -->
     <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
       
   
       <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
       <!-- webcam -->
       <script src="<?php echo base_url("assets/js/webcam.min.js");?>"></script>
   
       <!-- App js -->
       <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
       <script src="<?php echo base_url("/assets/js/jquery.validate.js");?>"></script>
       <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>

       <script>
            $(document).ready(function () {
                get_anio();
                
                $('#POBL').blur(function () {  this.value = zeroPad(this.value, 3); });
                $('#CTEL').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#PRED').blur(function () { this.value = zeroPad(this.value, 3);  });
                $('#UNID').blur(function () { this.value = zeroPad(this.value, 3);  });

            });

            $(document).on( 'click', '#btn_consultar', function () {
                var anio = $("#year").val();
                var folio = $("#folio").val();
                consulta_folio(anio, folio);
            });

            function zeroPad(num, places) {
                var zero = places - num.toString().length + 1;
                return Array(+(zero > 0 && zero)).join("0") + num;
            }

            function consulta_folio(a, folio)
            {
            if (folio == ""){
                    Swal.fire({
                        icon: 'error',
                        text: 'Ingrese un folio válido para continuar',
                        })
                 }
                else{

                get_documentos(a+folio);
                    
                $.ajax({
                url: "<?php echo base_url("ventanilla/consultafolio/");?>/"+a+"/"+folio+"",
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Folio...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                       console.log(data.data[0]);
                       var data = data.data;
                       if(data.length > 0){
                           
                            //CARGAR CALLES Y COLONIAS DEPENDIENDO LA POBLACION
                            
                            //agregar los valores a los controles
                                $("#POBL").val(data[0].Clave.substr(0, 3));
                                $("#CTEL").val(data[0].Clave.substr(3, 3));
                                $("#MANZ").val(data[0].Clave.substr(6, 3));
                                $("#PRED").val(data[0].Clave.substr(9, 3));
                                $("#UNID").val(data[0].Clave.substr(12, 3));
                                
                                //fecha de captura
                                //$("#dtfecha").val(moment(data[0].FechaCapturaInconformidad).format('YYYY-MM-DD'));
                                //Calle y colonia
                                $('#nombres').val(data[0].NombrePropietario);

                                $('#idcolonia').val(data[0].NOM_COL);
                                //$('#idcolonia').val(data[0].IdColonia).trigger('change.select2')
                                $('#idcalle').val(data[0].NOM_CALLE);
                                //Numero interior y uso
                                $("#numeroof").val(data[0].NumeroOficial);
                                $("#uso").val(data[0].DES_USO);
                                //superficies y valor
                                $("#superficiecons").val(data[0].TotalSuperficieConstruccion);
                                $("#superficieterr").val(data[0].TotalSuperficieTerreno);
                                $("#valorcat").val(data[0].ValorCatastral);
                                //nombre del solicitante, domicilio y telefono
                                $("#nom_sol").val(data[0].NombreSolicitante);
                                $("#dom_sol").val(data[0].DomicilioSolicitante);
                                $("#tel_sol").val(data[0].TelefonoSolicitante);
                                //tramites y motivo
                                $('#tramites').val(data[0].DescripcionTramite);
                                cargar_requisitos(data[0].IdTramite);
                                $('#empleados').val(data[0].Nombre);
                                $("#tramitenot").val(data[0].TramiteNotario);
                                $("#motivo").text(data[0].ObservacionesBarra);
                                //validar el vectorial
                                validar();
                                
                                
                       }
                       else {
                           Swal.fire({
                               icon: 'error',
                               title: 'Folio no encontrado',
                               text: 'El folio no se encuentra registrado!'
                               
                           })
                       }
                       
                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });
                    
                    

            }

        }

        function get_anio(){
            var anio = new Date().getFullYear().toString(); 
            var a = anio.substr(2,4);
            $('#year').val(a); 
            return a;
        }

        function get_documentos(cve){
            $('#files').empty();
            var url = "<?php echo base_url("administrativo/cargar_documentacion/"); ?>/"+cve;
            $.getJSON(url, function(json){
                    console.log(json);
                    $.each(json, function(i, obj){
                            var html = ' <div class="col-lg-4 col-xl-3"> <div class="file-man-box"><div class="file-img-box"><img src="https://coderthemes.com/highdmin/layouts/assets/images/file_icons/pdf.svg" alt="icon"></div><a href="javascript:;" class="file-download btnShow" url="<?php echo base_url("documentos"); ?>/'+cve+'/'+obj.nombre+'"><i class="bx bx-cloud-download"></i></a><div class="file-man-title"><h5 class="mb-0 text-overflow">'+obj.nombre+'</h5><p class="mb-0"><small>'+parseFloat(obj.size)/1000+' KB</small></p></div> </div></div>';
                            $('#files').append(html);
                    });
            });
        }

        function cargar_requisitos(id){
            var url = "<?php echo base_url("requisitos/get/");?>/"+id;
            $.getJSON(url, function(json){
                    $('#requisitos_list').empty();
                    //console.log(json);
                    $.each(json.result, function(i, obj){
                            var html = '<div class="form-check mb-2"><input class="form-check-input" type="checkbox" checked id="'+obj.IdRequisito+'" name="doc'+(i+1)+'"><label class="form-check-label" for="defaultCheck1">'+obj.DescripcionRequisito+'</label></div>';
                            $('#requisitos_list').append(html);
                    });
            });
         }

         

        function validar() {
             //limpiamos el consultado
            if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }
            //_geojson_vectorSource.clear();
             //obtener los valores de los input de la clave
             var pobl = $('#POBL').val();
             var ctel = $('#CTEL').val();
             var manz = $('#MANZ').val();
             var pred = $('#PRED').val();
             var unid = $('#UNID').val();

             var cvecat = pobl + ctel + manz + pred + unid;

             console.log("validando clave: " + cvecat)
             $.ajax({
                url: "<?php echo base_url("padron/validarcve");?>/"+cvecat,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Validando Clave Catastral...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                       console.log(data.result);
                    if(data.result.length > 0){

                        if(data.result[0].ID_REGTO == "B"){
                            Swal.fire({
                               icon: 'error',
                               title: 'Clave dada de Baja...',                               
                           })
                           $("#btn_limpiar").click();
                        }
                        else if(data.result[0].DOM_NOT == ""){
                            Swal.fire({
                               icon: 'error',
                               title: 'Este trámite no puede seguir, requiere Domicilio de Notificación!!',                               
                           })
                           $("#btn_limpiar").click();
                        }
                        else{
                            //NOMBRE COMPLETO    
                            //se obtiene la foto y se decodifica
                            var foto = data.result[0].foto;
                            //console.log(foto);
                            if(foto != null){
                                $(".image-tag").val(foto);
                                $("#foto").attr('src', "data:image/jpeg;base64," + foto );
                            }
                            //Se obtiene el array del poligono 
                            jsonobj = jQuery.parseJSON(data.result[0].jsonb_build_object);
                            //si encuentra el poligono en el vectorial
                            console.log(jsonobj);
                            if (jsonobj.features != null) {
                                //Se visualiza el poligono en el mapa
                                visualizar_capa(jsonobj);
                                //llenar datos del geometrico
                                var nombre = jsonobj.features[0].properties.f3 != null ? jsonobj.features[0].properties.f3 : "";
                               
                                var calle = jsonobj.features[0].properties.f4 != null ? jsonobj.features[0].properties.f4 : "";

                                var colonia = jsonobj.features[0].properties.f5 != null ? jsonobj.features[0].properties.f5 : "";

                                var numoficial = jsonobj.features[0].properties.f6 != null ? jsonobj.features[0].properties.f6 : "";

                                var supterr = jsonobj.features[0].properties.f7 != null ? jsonobj.features[0].properties.f7 : "";

                                var supcons = jsonobj.features[0].properties.f8 != null ? jsonobj.features[0].properties.f8 : "";

                                var valcat = jsonobj.features[0].properties.f9 != null ? jsonobj.features[0].properties.f9 : "";

                                $('#nombres').val($('#nombres').val() + " / " + nombre);
                                
                                $('#idcalle').val($('#idcalle').val() + " / " + calle);

                                $('#idcolonia').val($('#idcolonia').val() + " / " + colonia);

                                $('#numeroof').val($('#numeroof').val() + " / " + numoficial);

                                $('#superficieterr').val($('#superficieterr').val() + " / " + supterr );

                                $('#superficiecons').val($('#superficiecons').val() + " / " + supcons );

                                $('#valorcat').val($('#valorcat').val() + " / " + valcat );
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Vectorial no encontrado',
                                    text: 'La clave catastral no se encontró en el vectorial!'
                                    
                                })
                            }
                        }
                            
                       }
                       else {
  
                           //verificar si se encuentra de primer registro
                           Swal.fire({
                            title: 'Clave Catastral no encontrada',
                            icon: 'error',
                            showCancelButton: true,
                            cancelButtonText: 'Notificar',
                            confirmButtonColor: '#00b300',
                            cancelButtonColor: '#0000FF',
                            confirmButtonText: 'Primer Registro'
                            }).then((result) => {
                                if (result.value) {
                                    $('#tramites').val("20.0").trigger('change');
                                    $('#tramites').focus();
                                    $("#Clave").val(cvecat);
                                }
                                else{
                                    $("#btn_limpiar").click();
                                }
                            })

                           
                       }          
                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });
         }
        

        $(document).on('click', '.btnShow', function(e){
            e.preventDefault();
            var url = $(this).attr('url');
            $('#modal-doc').modal('show');
            $('#objdata').attr('data', url);

        });

       </script>