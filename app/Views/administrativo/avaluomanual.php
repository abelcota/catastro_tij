<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">AVALUOS</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">ADMINISTRATIVO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="col-lg-12">
                     <div class="card">
                                <div class="card-body">  
                                        <div class="float-right">
                                            
                                            <i class="mdi mdi-file-export-outline text-success mdi-18px"></i> Consultar | 

                                            <i class="mdi mdi-printer text-primary mdi-18px"></i>Imprimir
                                        </div>
                                        <h4 class="card-title">Consultar Avaluos Registrados</h4>
                                        <hr>
                                        <div class="table-responsive">   
                                        <table class="table table-hover nowrap" id="tblavaluos" width="100%">
                                            <thead class="text-white" style="background-color: #480912;">
                                            <tr>
                                                <th>Acciones</th>
                                                <th>Folio</th>
                                                <th>Clave</th>
                                                <th>Trámite</th>
                                                <th>Valor Anterior</th>
                                                <th>Valor Actual</th>
                                                <th>Fecha</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        </div>
                                        </div>
                                        </div>
                     </div>
                     <div class="col-lg-12" id="vertoolbar">
                        <div class="btn-toolbar card d-print-none" role="toolbar">
                            <div class="card-body">
                                <div class="row">

                                <div class="col-lg-3">
                                    <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Folio:</label>
                                                    <div class="input-group">
                                                                <input type="text"  class="form-control" placeholder="00" id="year" name="year" required maxlength="3" required value="<?php echo $year; ?>">
                                                                <input type="text"  class="form-control" name="folio" placeholder="00000" id="folio" required maxlength="5" required value="<?php echo $folio; ?>">
                                                    </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral:</label>
                                                    
                                                            <div class="input-group">
                                                                <input type="text"  class="form-control" placeholder="POBL" id="POBL" name="POBL" required maxlength="3" required>
                                                                <input type="text"  class="form-control" name="CTEL" placeholder="CTEL" id="CTEL" required maxlength="3" required>
                                                                <input type="text"  class="form-control" placeholder="MANZ" id="MANZ" name="MANZ" required maxlength="3" required>
                                                                <input type="text"  class="form-control" name="PRED" placeholder="PRED" id="PRED" required maxlength="3" required>
                                                                <input type="text"  class="form-control" placeholder="UNID" id="UNID" name="UNID" required maxlength="3" required>
                                                                
                                                            </div>
                                                   
                                                </div>
                                </div>
                                   
                                    <div class="col-lg-3">
                                            Acciones<br>
                                               <!--<a href="javascript:;" class="btn btn-primary waves-effect waves-light mt-2" id="btn_consultar"><i class="fa fa-search"></i> Consultar</a>-->
                                                <a href="javascript:;)" class="btn btn-success waves-effect mt-2 waves-light" id="btn_imprimir_avaluo"><i class="fa fa-print"></i>Imprimir Avaluo </a>
                                               
                                    </div>
                                </div>            
                            </div>
                        </div>
                        <!--end toolbar -->             
                    </div>                


                    <!-- begin tabs -->
                    <div class="col-lg-12" id="vertabs">
                            <div class="card">
                                <div class="card-body">
                                    <h3 id="des_tramite"></h3>
                                    <h4 class="card-title">Consulta de Datos Activos del Predio</h4>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-pills nav-fill pt-3" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#generales" role="tab">
                                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                                <span class="d-none d-sm-block ">Generales</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#nota" role="tab">
                                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                                <span class="d-none d-sm-block ">Nota</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#terreno" role="tab">
                                                <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                                <span class="d-none d-sm-block ">Terreno</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#construcciones" role="tab">
                                                <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                <span class="d-none d-sm-block ">Construcción</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#servicios" role="tab">
                                                <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                <span class="d-none d-sm-block ">Servicios</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#usos" role="tab">
                                                <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                <span class="d-none d-sm-block ">Usos</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#propietarios" role="tab">
                                                <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                <span class="d-none d-sm-block ">Propietarios</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#antecedente" role="tab">
                                                <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                <span class="d-none d-sm-block ">Antecendete Legal</span>
                                            </a>
                                        </li>
                                       
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content p-3 text-muted">
                                    
                                        <div class="tab-pane active" id="generales" role="tabpanel">
                                           <!-- form de generales -->
                                           <h5>DATOS GENERARLES </h5>
                                           <div class="row border p-3">
                                           <div class="col-lg-12">
                                                <div class="form-group row">
                                                        <label for="example-text-input" class="col-md-2 col-form-label text-primary">Propietario:</label>
                                                        <div class="col-md-10">
                                                            <input class="form-control" type="text" id="propietario">
                                                        </div>
                                                        <div class="col-md-5">
                                                        </div>
                                                        <label for="example-text-input mt-2" style="text-align:right" class="col-md-4 col-form-label">Impuesto Art. Transitorio:</label>
                                                        <div class="col-md-3">
                                                            <input class="form-control float-right mt-2" type="text" id="impuestotrans">
                                                        </div>
                                                </div>
                                                

                                                <div class="form-group row">
                                                        <label for="example-text-input" class="col-md-2 col-form-label text-primary">Notificación:</label>
                                                        <div class="col-md-10">
                                                            Domicilio: <input class="form-control" type="text" id="domicilio">
                                                        </div>
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-6 mt-2">
                                                            Colonia: <input class="form-control" type="text" id="colonia">
                                                        </div>
                                                        <div class="col-md-4 mt-2">
                                                            Código Postal: <input class="form-control" type="text" id="cp">
                                                        </div>
                                                    </div>
                                                

                                                <div class="form-group row">
                                                        <label for="example-text-input" class="col-md-2 col-form-label text-primary">Geometría:</label>
                                                        
                                                        <div class="col-md-3">
                                                            No. Frentes: <input class="form-control" type="text" id="frentes">
                                                        </div>
                                                        <div class="col-md-3">
                                                            Mts. Frente: <input class="form-control" type="text" id="mtsfrente">
                                                        </div>
                                                        <div class="col-md-4">
                                                            Mts. Fondo: <input class="form-control" type="text" id="mtsfondo">
                                                        </div>
                                                </div>

                                                <div class="form-group row">
                                                        <label for="example-text-input" class="col-md-2 col-form-label text-primary">Efectos:</label>
                                                        
                                                        <div class="col-md-3">
                                                            Tipo: <input class="form-control" type="text" id="tipo_efec">
                                                        </div>
                                                        <div class="col-md-3">
                                                            No. Trimestre: <input class="form-control" type="text" id="tri_efec">
                                                        </div>
                                                        <div class="col-md-4">
                                                            Año: <input class="form-control" type="text" id="anio_efec">
                                                        </div>
                                                    
                                                </div>

                                                <div class="form-group row">
                                                        <label for="example-text-input" class="col-md-2 col-form-label text-primary">Ubicación:</label>
                                                        
                                                        <div class="col-md-6">
                                                            Calle: <input class="form-control" type="text" id="calle">
                                                        </div>
                                                        <div class="col-md-4">
                                                            Numero Oficial: <input class="form-control" type="text" id="numerooficial">
                                                        </div>
                                                        <div class="col-md-2">
                                                            
                                                        </div>
                                                        <div class="col-md-10">
                                                            Domicilio: <input class="form-control" type="text" id="domiclio">
                                                        </div>

                                                        <div class="col-md-2">
                                                            
                                                        </div>
                                                        <div class="col-md-10">
                                                            Colonia: <input class="form-control" type="text" id="colonia_u">
                                                        </div>

                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-2 col-form-label text-primary">Registro Público de la Propiedad:</label>
                                                        
                                                        <div class="col-md-2">
                                                            Libro: <input class="form-control" type="text" id="libro">
                                                        </div>

                                                        <div class="col-md-2">
                                                            Escritura: <input class="form-control" type="text" id="escritura">
                                                        </div>

                                                        <div class="col-md-2">
                                                            Sección: <input class="form-control" type="text" id="seccion">
                                                        </div>

                                                        <div class="col-md-4">
                                                            Inscripción: <input class="form-control" type="text" id="inscripcion">
                                                        </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-2 col-form-label text-primary">Otros Datos:</label>
                                                    <div class="col-md-4">
                                                            Régimen: <input class="form-control" type="text" id="regimen">
                                                        </div>

                                                        <div class="col-md-2">
                                                            Restringido: <input class="form-control" type="text" id="restringido">
                                                        </div>

                                                        <div class="col-md-2">
                                                            Fecha Alta: <input class="form-control" type="text" id="fechalta">
                                                        </div>

                                                        <div class="col-md-2">
                                                            Fecha Mvto.: <input class="form-control" type="text" id="fechmov">
                                                        </div>

                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-2 col-form-label text-primary">Terreno:</label>
                                                    <div class="col-md-5">
                                                            Superficie: <input class="form-control" type="text" id="supterr">
                                                    </div>
                                                    <div class="col-md-5">
                                                            Valor: <input class="form-control" type="text" id="valorterr">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-2 col-form-label text-primary">Valor de Zona:</label>
                                                    <div class="col-md-4">
                                                            Clave: <input class="form-control" type="text" id="cvezona">
                                                    </div>
                                                    <div class="col-md-4">
                                                            $ M2: <input class="form-control" type="text" id="valorzona">
                                                    </div>
                                                    <div class="col-md-2">
                                                            Demérito: <input class="form-control" type="text" id="demzona">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-2 col-form-label text-primary">Constucción:</label>
                                                    <div class="col-md-5">
                                                            Superficie: <input class="form-control" type="text" id="supcons">
                                                    </div>
                                                    <div class="col-md-5">
                                                            Valor: <input class="form-control" type="text" id="valorconst">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-md-2 col-form-label text-primary">Valor Catastral:</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" type="text" id="valorcat">
                                                    </div>
                                                    
                                                </div>


                                            </div>

                                           </div>
                                        </div>
                                        <div class="tab-pane" id="nota" role="tabpanel">
                                        <h5>NOTA </h5>
                                            <div class="row border p-3">
                                                <div class="col-lg-12">
                                                        <div class="form-group row">
                                                                <label for="example-text-input" class="col-md-2 col-form-label text-primary">No. Control:</label>
                                                                <div class="col-md-4">
                                                                    <input class="form-control" type="text" id="nocontrol">
                                                                </div>
                                                                <label for="example-text-input" class="col-md-2 col-form-label text-primary">Folio de Pago:</label>
                                                                <div class="col-md-4">
                                                                    <input class="form-control" type="text" id="folpago">
                                                                </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            Nota:
                                                            <textarea class="form-control" rows="10"></textarea>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-md-6 col-form-label text-primary">Unidad Catastral de Referencia:</label>
                                                                <div class="col-md-6">
                                                                <div class="input-group">
                                                                <input type="text" readonly  class="form-control" placeholder="POBL" id="POBLR" name="POBLR" required maxlength="3" required>
                                                                <input type="text" readonly  class="form-control" name="CTELR" placeholder="CTEL" id="CTELR" required maxlength="3" required>
                                                                <input type="text" readonly  class="form-control" placeholder="MANZ" id="MANZR" name="MANZR" required maxlength="3" required>
                                                                <input type="text" readonly  class="form-control" name="PREDR" placeholder="PRED" id="PREDR" required maxlength="3" required>
                                                                <input type="text" readonly  class="form-control" placeholder="UNID" id="UNIDR" name="UNIDR" required maxlength="3" required>
                                                                
                                                            </div>
                                                                </div>
                                                        </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="terreno" role="tabpanel">
                                        <h5>TERRENO </h5>
                                           <table class="table table-hover" id="tblterreno" width="100%">
                                                <thead class="text-primary">
                                                    <tr>
                                                        <th colspan="3" class="text-center">GENERAL</th>
                                                        <th colspan="2" class="text-center">ESQUINA</th>
                                                        <th colspan="2" class="text-center">GENERAL</th>
                                                        <th colspan="1" class="text-center">VALOR</th>
                                                    </tr>
                                                    <tr>
                                                        <th>T/Ren</th>
                                                        <th>Superficie</th>
                                                        <th>Valor Unit.</th>
                                                        <th>Superficie</th>
                                                        <th>Valor Unit.</th>
                                                        <th>Superficie</th>
                                                        <th>% Dem.</th>
                                                        <th>Valor</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                        <th class="font-medium">Sup Total:</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th>Valor Total:</th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                           </table>
                                        </div>
                                        <div class="tab-pane" id="construcciones" role="tabpanel">
                                        <h5>CONSTRUCCIONES </h5>
                                        <table class="table table-hover" id="tblconstrucciones" width="100%">
                                                <thead class="text-primary">
                                                    <tr>
                                                        <th colspan="3" class="text-center">CATEGORIA</th>
                                                        <th colspan="7" class="text-center">OTROS DATOS</th>
                                                       
                                                    </tr>
                                                    <tr>
                                                        <th>Cve</th>
                                                        <th>Descripcion</th>
                                                        <th>Valor M2.</th>
                                                        <th>Superficie</th>
                                                        <th>EDO</th>
                                                        <th>Fecha</th>
                                                        <th>Edad</th>
                                                        <th>F. Dem</th>
                                                        <th>F.C.</th>
                                                        <th>Valor</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                        <th colspan="3" class="text-right">Superficie Total de Construcción:</th>
                                                        
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th colspan="3" class="text-right">Valor Total de Construccion:</th>
                                                       
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                           </table>
                                        </div>

                                        <div class="tab-pane" id="servicios" role="tabpanel">
                                            <h5>SERVICIOS </h5>
                                            <ul class="list-group text-primary" id="servicios_list">
                                                
                                            </ul>
                                        </div>

                                        <div class="tab-pane" id="usos" role="tabpanel">
                                            <h5>USOS </h5>
                                            <ul class="list-group text-primary" id="usos_list">
                                                
                                            </ul>
                                        </div>

                                        <div class="tab-pane" id="propietarios" role="tabpanel">
                                            <h5>PROPIETARIOS </h5>
                                            <table class="table table-hover" id="tblpropietarios" width="100%">
                                                <thead class="text-primary"> 
                                                    <tr>
                                                        <th>Cve</th>
                                                        <th>Nombre (Razón Social)</th>
                                                        <th>Apellido Paterno</th>
                                                        <th>Apellido Materno</th>
                                                        <th>Reg.Fed.Cont.</th>
                                                       
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                
                                           </table>
                                        </div>

                                        <div class="tab-pane" id="antecedente" role="tabpanel">
                                            <h5>ANTECEDENTE LEGAL </h5>
                                            <textarea class="form-control" rows="10">* Sin Antecedentes *</textarea>
                                        </div>

                                        

                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12" id="vermapa">
                        <div class="card">
                            <div class="card-body">
                            <h5>UBICACIÓN GEOGRÁFICA </h5>
                            <hr>
                                <div id="map" class="map" style="width: 1000px; height: 500px;"></div>
                            </div>
                        </div>
                    </div>

                    </div>
                    <!-- end tabs -->

                   
                     
                     
                     </div>
                     <!-- end row -->


                   