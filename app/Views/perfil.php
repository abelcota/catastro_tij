<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">PERFIL DE USUARIO</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">CONFIGURACIÓN</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <!-- start row -->
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    
                                    <h5 class="card-title">Configuracion de la Información de Usuario</h5>
                                    <hr>
                                    <form id="form-info">
                                    <div class="form-group row">
                                        <label for="txtnombre" class="col-md-3 col-form-label">Nombre:</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="hidden" value="<?php echo $usuario->id; ?>" id="txtusuario" name="txtusuario" >
                                            <input class="form-control" type="text" value="" id="txtnombre" name="txtnombre">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="txtrfc" class="col-md-3 col-form-label">RFC:</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" value="" id="txtrfc" name="txtrfc">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="txtdireccion" class="col-md-3 col-form-label">Dirección:</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" value="" id="txtdireccion" name="txtdireccion">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="txttelefono" class="col-md-3 col-form-label">Teléfono:</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" value="" id="txttelefono" name="txttelefono">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="txtcelular" class="col-md-3 col-form-label">Celular:</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" value="" id="txtcelular" name="txtcelular">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="txtrfc" class="col-md-3 col-form-label">Correo:</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="email" value="" id="txtcorreo" name="txtcorreo">
                                        </div>
                                    </div>
                                    </form>
                                    <hr>
                                    <div class="form-group row">
                                        <label for="txtrfc" class="col-md-3 col-form-label">Actualizar Contraseña:</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <input class="input-group-text" type="password" id="txtpassword" name="txtpassword">
                                                </div>
                                                <span class="input-group-btn input-group-append"><button class="btn btn-info" type="button" id="btn_actualizar_password"><i class="mdi mdi-sync"></i> Actualizar</button></span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                          
                                                        <button type="button" class="btn btn-success " id="btn-guardar"> <i class="mdi mdi-content-save-edit-outline"></i>Guardar Perfil</button>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>