</div>
        <!-- END layout-wrapper -->
        <div id="wait" style="display:none;width:69px;height:89px;border:1px solid black;position:absolute;top:50%;left:50%;padding:2px;"><img src='<?php echo base_url("assets/images/Loader.gif");?>' width="64" height="64" /><br>Procesando..</div>

        <!-- modal 
        <div class="modal fade modal-autorizar" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg modal-dialog-top">
                                                    <div class="modal-content">
                                                    
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Firma y Autorización</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                                     
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label for="txtRFC">RFC</label>
                                                                            <input id="datofirmar" type="hidden"/>
                                                                            <input id="txtRFC" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important;" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label for="fileCerPer">Certificado (cer)*</label><label style="color: red;">*</label>
                                                                            <input type="file" id="fileCerPer" lang="es" class="form-control" required="required" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label for="fileKeyPer">Clave privada (key)*</label><label style="color: red;">*</label>
                                                                            <input type="file" id="fileKeyPer" lang="es" class="form-control" required="required" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group col-md-12">
                                                                            <label for="txtPAssKeyPer">Contraseña de clave privada*</label><label style="color: red;">*</label>
                                                                            <input id="txtPAssKeyPer" type="password" class="form-control" placeholder="" required="required" />
                                                                        </div>
                                                                    </div>
                                                                    <button id="btnFirmarPer" class="btn btn-primary btn-block btnFirmar"><i class="mdi mdi-file-certificate-outline"></i> Firmar</button>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <label for="txtNombre">Nombre:</label>
                                                                    <input id="txtNombre" class="form-control" disabled="disabled" style="background-color:  #F0F0F0!important;" />
                                                                    <label for="txtSerie">Serie del Certificado</label>
                                                                    <input id="txtSerie" class="form-control" disabled="disabled" style="background-color:  #F0F0F0!important;" />
                                                                    <label for="txtSello">Sello Digital</label>
                                                                    <textarea id="txtSello" class="form-control" rows="11" disabled="disabled" style="background-color:  #F0F0F0!important;"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-success" id="btn_autorizar"><i class="mdi mdi-check"></i> Autorizar</button>
                                                          
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar_autorizar">Cancelar</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            modal -->
    </div>
    <!-- end container-fluid -->

    <!-- JAVASCRIPT -->
    <script src="<?php echo base_url("assets/libs/jquery/jquery.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/bootstrap/js/bootstrap.bundle.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/metismenu/metisMenu.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/simplebar/simplebar.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/node-waves/waves.min.js");?>"></script>
    <script src="<?php echo base_url("assets/js/moment.min.js");?>"></script>


     
    <!-- Sweet Alerts js -->
    <script src="<?php echo base_url("assets/libs/sweetalert2/sweetalert2.min.js");?>"></script>

    <!-- Select 2 js -->
    <script src="<?php echo base_url("assets/libs/select2/js/select2.min.js");?>"></script>

    <!-- App js -->
    <script src="<?php echo base_url("assets/js/app.js");?>"></script>

    <script>
        $(document).on('click', '#btnAutorizar', function(e) {
                var datofirmar = document.getElementById('datofirmar').value;
                var certfile = document.getElementById('fileCerPer').files[0];
                var certkey = document.getElementById('fileKeyPer').files[0];
                var passkey = document.getElementById('txtPAssKeyPer').value;
                var mensaje = datofirmar;

                var formData = new FormData();
                formData.append('certfile', certfile);
                formData.append('certkey', certkey);
                formData.append('passkey', passkey);
                formData.append('mensaje', mensaje);

                $.ajax({
                        url: "<?php echo base_url("padron/firmar"); ?>",
                        type: "POST",
                        data: formData,
                        processData: false,  
                        contentType: false,
                            success: function (data) {
                                console.log(data);
                                if(data.certificado.length > 0){
                                    $('#txtRFC').val(data.certificado[0].rfc);
                                    $('#txtSerie').val(data.certificado[0].serial);
                                    $('#txtSello').val(data.certificado[0].sello);
                                    $('#txtNombre').val(data.certificado[0].nombre);
                                }
                            
                            },
                            error: function (jqXHR, status) {
                                
                                $('#txtRFC').val("");
                                $('#txtSerie').val("");
                                $('#txtSello').val("");
                                $('#txtNombre').val("");
                                Swal.fire({
                                    title: "Error al Firmar",
                                    text: 'Por favor verifique los archivos y contraseña de su firma electrónica',
                                    icon: 'error'
                                })
                            }
                    });


            });
        </script>
    

</body>

</html>