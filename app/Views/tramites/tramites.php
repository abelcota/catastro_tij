<!doctype html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Catalogo de Tramites Sistema de Gestion Catastral</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body>

<div class="container mt-5">
    <a href="<?php echo base_url('public/index.php/tramites/crear/') ?>" class="btn btn-success mb-2">Crear</a>
    <?php
     if(isset($_SESSION['msg'])){
        echo $_SESSION['msg'];
      }
     ?>
  <div class="row mt-3">
     <table class="table table-bordered" id="tramites">
       <thead>
          <tr>
             <th>COD</th>
             <th>Descripcion</th>
             <th>Acciones</th>
          </tr>
       </thead>
       <tbody>
          <?php if($tramites): ?>
          <?php foreach($tramites as $t): ?>
          <tr>
             <td><?php echo $t['cod_tramite']; ?></td>
             <td><?php echo $t['descripcion']; ?></td>
             <td>
              <a href="<?php echo base_url('public/index.php/tramites/editar/'.$t['cod_tramite']);?>" class="btn btn-success">Editar</a>
              <a href="<?php echo base_url('public/index.php/tramites/eliminar/'.$t['cod_tramite']);?>" class="btn btn-danger">Eliminar</a>
              </td>
          </tr>
         <?php endforeach; ?>
         <?php endif; ?>
       </tbody>
     </table>
  </div>
</div>
 
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
 
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
 
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
 
<script>
    $(document).ready( function () {
      $('#tramites').DataTable();
  } );
</script>
</body>
</html>