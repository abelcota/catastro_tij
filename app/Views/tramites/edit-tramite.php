<!DOCTYPE html>
<html>
<head>
  <title>Editar Tramite Sistema de Gestion Catastral</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
 
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
 
</head>
<body>
 <div class="container">
    <br>
    <?= \Config\Services::validation()->listErrors(); ?>
 
    <span class="d-none alert alert-success mb-3" id="res_message"></span>
 
    <div class="row">
      <div class="col-md-9">
        <form action="<?php echo base_url('public/index.php/tramites/actualizar');?>" name="edit-tramite" id="edit-tramite" method="post" accept-charset="utf-8">
            <div class="form-group">
                <input type="hidden" name="cod_tramite" class="form-control" id="cod_tramite" value="<?php echo $tramites['cod_tramite'] ?>">
            </div> 
           <div class="form-group">
            <label for="descripcion">Descripcion</label>
            <textarea class="form-control" id="descripcion" name="descripcion" rows="2"><?php echo $tramites['descripcion'] ?></textarea>             
          </div>   
 
          <div class="form-group">
           <button type="submit" id="send_form" class="btn btn-success">Actualizar</button>
          </div>
          
        </form>
      </div>
 
    </div>
  
</div>
 <script>
   if ($("#edit-tramite").length > 0) {
      $("#edit-tramite").validate({
      
    rules: {
        descripcion: {
        required: true,
      },

    },
    messages: {
        
        descripcion: {
        required: "Por favor ingrese una descripcion",
      },
    },
  })
}
</script>
</body>
</html>