<!DOCTYPE html>
<html>
<head>
  <title>Registro Nuevos Tramites Sistema de Gestion Catastral</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
 
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>  
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
 
</head>
<body>
 <div class="container">
 <h2>Registrar nuevo Tramite </h2>
    <br>
    <?= \Config\Services::validation()->listErrors(); ?>
 
    <span class="d-none alert alert-success mb-3" id="res_message"></span>
  
    <div class="row">
      <div class="col-md-9">
        <form action="<?php echo base_url('public/index.php/tramites/guardar');?>" name="tramites_create" id="tramites_create" method="post" accept-charset="utf-8">
 
          <div class="form-group">
            <label for="cod_tramite">Codigo de Tramite</label>
            <input type="number" name="cod_tramite" class="form-control" id="cod_tramite" placeholder="0.0" step="any">
          </div> 
 
          <div class="form-group">
            <label for="descripcion">Descripcion</label>
            <textarea class="form-control" id="descripcion" name="descripcion" rows="2"></textarea>             
          </div>   
 
          <div class="form-group">
           <button type="submit" id="send_form" class="btn btn-success">Guardar</button>
          </div>        
        </form>
      </div>
 
    </div>
  
</div>
 <script>
   if ($("#tramites_create").length > 0) {
      $("#tramites_create").validate({
      
    rules: {
    cod_tramite: {
        required: true,
      },
  
      descripcion: {
        required: true,
      },   
    },
    messages: {
        
      cod_tramite: {
        required: "Por favor ingrese un codigo para el tramite",
      },
      descripcion: {
        required: "Por favor ingrese una descripcion del nuevo tramite",
        }, 
    },
  })
}
</script>
</body>
</html>