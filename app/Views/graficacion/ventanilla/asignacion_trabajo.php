<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">ASIGNACIÓN DE TRABAJO A GRAFICADORES PARA TRAMITES DE VENTANILLA</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">GRAFICACIÓN</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <!-- start form -->
                                        <div class="row">
                                            <div class="col-lg-12" style="margin-top:10px;">
                                                <table class="table display table-hover" id="tbl-folios">
                                                    <thead class="text-white" style="background-color: #480912;">
                                                        <th class="text-center">Año</th>
                                                        <th class="text-center">Folio</th>
                                                        <th class="text-center">Pobl</th>
                                                        <th class="text-center">Ctel</th>
                                                        <th class="text-center">Manz</th>
                                                        <th class="text-center">Pred</th>
                                                        <th class="text-center">Unid</th>
                                                        <th></th>
                                                        </thead>
                                                    </table>
                                            </div>
                                        </div>
                                        
                                                                               
                                    
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                     
                     <!--modal para asignacion de graficadores -->
                     <div class="modal fade modal-asignar" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                        <div class="modal-dialog modal-lg modal-dialog-top">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title mt-0">Asignación de Trabajo a Graficador</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body"> 
                                            <!-- start form -->
                                            <form class="needs-validation" novalidate>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                        <div class="form-group position-relative">
                                                            <label for="validationTooltip02">Año:</label>
                                                            <input type="text" id="anio" name="anio" class="form-control" readonly>
                                                        </div>
                                                </div>
                                                <div class="col-lg-4">
                                                        <div class="form-group position-relative">
                                                            <label for="validationTooltip02">Folio:</label>
                                                            <input type="text" id="folio" name="folio" class="form-control" readonly>
                                                        </div>
                                                </div>
                                                <div class="col-lg-4">
                                                        <div class="form-group position-relative">
                                                            <label for="validationTooltip02">Clave Catastral:</label>
                                                            <input type="text" id="clave" name="clave" class="form-control" readonly>
                                                        </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group position-relative">
                                                        <label for="validationTooltip02">Seleccionar Graficador:</label>
                                                        <select class="form-control" id="graficador" name="graficador">
                                                        </select>
                                                        
                                                    </div>
                                                </div>
                                        </div>                                       
                                                                               
                                    </form>
                                                                </div>
                                                                <div class="modal-footer">              
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_tcancelar">Cancelar</button>
                                                                    <button type="button" class="btn btn-success"  id="btn_guardar">Asignar</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->


                </div>
                <!-- End Page-content -->
                
            </div>
            <!-- end main content-->

            
