<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
 <!-- Buttons examples -->
 <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
   
   <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
   

   <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


   <!-- App js -->
   <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
   <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
   <script src="<?php echo base_url("/assets/js/dataTables.fixedHeader.min.js");?>"></script>
   
<script>
$(document).ready(function () {

// Setup - add a text input to each footer cell
/*$('#tbl-folios thead tr').clone(true).appendTo( '#tbl-folios thead' );
$('#tbl-folios thead tr:eq(1) th').each( function (i) {
    var title = $(this).text();
    $(this).html( '<input type="text" class="form-control" placeholder="Filtrar '+title+'" />' );

    $( 'input', this ).on( 'keyup change', function () {
        if ( table.column(i).search() !== this.value ) {
            table
                .column(i)
                .search( this.value )
                .draw();
        }
    } );
} );*/

var table = $('#tbl-folios').DataTable({
    "orderCellsTop": true,
    "language": {
        "url": "<?php echo base_url("assets/Spanish.json")?>"
    },
    "bProcessing": true,
    "sAjaxSource": "<?php echo base_url("graficacion/get_folios_sin_graficador")?>",
    "bPaginate":true,
    "sPaginationType":"full_numbers",
    "iDisplayLength": 25,
    "order": [[ 0, "desc" ], [1, "desc"]],
    "aoColumns": [
        { mData: 'Id_Anio' } ,
        { mData: 'Id_Folio' },
        { mData: 'Pobl' },
        { mData: 'Ctel' },
        { mData: 'Manz' },
        { mData: 'Pred' },
        { mData: 'Unid' },
        {
            data: null,
            defaultContent: '<button type="button" class="asignar btn btn-outline-success waves-effect waves-light btn-sm"><i class="dripicons-flag"></i>  <small>Asignar</small></button>'
        },
    ],
    rowCallback: function(row, data, index) {
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        $("td:eq(0)", row).addClass('text-primary text-center font-weight-bold');
        $("td:eq(1)", row).addClass('text-primary text-center');
        $("td:eq(2)", row).addClass('text-center');
        $("td:eq(3)", row).addClass('text-center');
        $("td:eq(4)", row).addClass('text-center');
        $("td:eq(5)", row).addClass('text-center');
        $("td:eq(6)", row).addClass('text-center');
        $("td:eq(7)", row).addClass('text-center');
       
    }
});

$('#tbl-folios tbody').on( 'click', '.asignar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                var a = data["Id_Anio"];
                var fol = data["Id_Folio"];
                var clave = data["Pobl"] +"-"+ data["Ctel"] +"-"+ data["Manz"] +"-"+ data["Pred"] +"-"+ data["Unid"];

                Swal.fire({
                title: 'Asignar Graficador para el folio: '+fol+' del año '+a+'?',
                type: 'info',
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Seleccionar Graficador!'
                }).then((result) => {
                    if (result.value) {
                        cargar_graficadores();
                        $(".modal-asignar").modal("show");
                        $("#folio").val(fol);
                        $("#anio").val(a);
                        $("#clave").val(clave);
                    }
                })  
            });

});

function cargar_graficadores(){
        $('#graficador').empty();
        var url = "<?php echo base_url("graficacion/get_graficadores");?>";
        $.getJSON(url, function(json){
                $('#graficador').append($('<option>').text("Seleccionar Graficador").attr({'value': 0}));
                $.each(json.results, function(i, obj){
                        $('#graficador').append($('<option>').text(obj.Nombre + " " +obj.ApellidoPaterno).attr({'value': obj.IdEmpleado}));
                });     
        });
     }

$( document ).on('click', '#btn_guardar', function(e) {
    //obtener los elementos que se van a enviar por post
    var graficador = $("#graficador").val();
    var anio = $("#anio").val();
    var folio = $("#folio").val();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url("graficacion/asignar_graficador"); ?>",
        type: "POST",
        data: {"graficador": graficador, "anio": anio, "folio":folio },
        cache: false,
        success: function(data){
            //console.log(data);
            if (data == 'ok')
            {
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'success',
                                    title: 'Mensaje del Sistema',
                                    text: 'El Graficador se asignó Correctamente'
                                    
                                })
                //cerramos el modal 
                $('.modal-asignar').modal('hide');
                $('#tbl-folios').DataTable().ajax.reload(null, false);
            }
            else{
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'Ocurrió un error al asignar el graficador'
                                    
                                })
            }
        }
    });
    
});   

</script>