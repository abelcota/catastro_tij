    <!-- JAVASCRIPT -->
  <script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
 <!-- Buttons examples -->
 <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
   
   <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
   

   <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


   <!-- App js -->
   <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
   <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
   <script src="<?php echo base_url("/assets/js/dataTables.fixedHeader.min.js");?>"></script>
   <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
   
<script>
$(document).ready(function () {

/*// Setup - add a text input to each footer cell
$('#tbl-graficaciones thead tr').clone(true).appendTo( '#tbl-graficaciones thead' );
//$('#tbl-graficaciones thead tr:eq(1) th').hide();
$('#tbl-graficaciones thead tr:eq(1) th').each( function (i) {
    var title = $(this).text();
    $(this).html( '<input type="text" class="form-control" placeholder="Filtrar '+title+'" />' );

    $( 'input', this ).on( 'keyup change', function () {
        if ( table.column(i).search() !== this.value ) {
            table
                .column(i)
                .search( this.value )
                .draw();
        }
    } );
} );*/

var table = $('#tbl-graficaciones').DataTable({
    "orderCellsTop": true,
    "language": {
        "url": "<?php echo base_url("assets/Spanish.json")?>"
    },
    "bProcessing": true,
    "sAjaxSource": "<?php echo base_url("graficacion/get_ventanilla_graficacion")?>",
    "bPaginate":true,
    "sPaginationType":"full_numbers",
    "iDisplayLength": 25,
    "order": [[ 0, "desc" ], [1, "desc"]],
    "aoColumns": [
        { mData: 'Id_PaqueteEnvioGraficacion' } ,
        { mData: 'F_EnvioGraficacion' },
        { mData: 'Id_Anio' },
        { mData: 'Id_Folio' },
        { mData: 'Descripcion' },
        { mData: 'Pobl' },
        { mData: 'Ctel' },
        { mData: 'Manz' },
        { mData: 'Pred' },
        { mData: 'Unid' },
        { mData: 'F_AtendidoGraficacion' },
    ],
    rowCallback: function(row, data, index) {
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        $("td:eq(0)", row).addClass('text-primary font-weight-bold');
        $("td:eq(3)", row).addClass('text-primary');
        $("td:eq(1)", row).text(moment(data['F_EnvioGraficacion']).format('DD/MM/YYYY'));
        $("td:eq(1)", row).addClass('text-center');
        $("td:eq(2)", row).addClass('text-center');
        $("td:eq(3)", row).addClass('text-center');
        $("td:eq(5)", row).addClass('text-center');
        $("td:eq(6)", row).addClass('text-center');
        $("td:eq(7)", row).addClass('text-center');
        $("td:eq(8)", row).addClass('text-center');
        $("td:eq(9)", row).addClass('text-center');
        $("td:eq(10)", row).addClass('text-center');
        if(data['F_AtendidoGraficacion'] != null)
            $("td:eq(10)", row).text(moment(data['F_AtendidoGraficacion']).format('DD/MM/YYYY'));
       
    }
});

});

$( document ).on('click', '#btn_captura', function(e) {
e.preventDefault();
get_anio();
cargar_graficadores();
cargar_movimientos();
cargar_resultados();
$("#form-captura")[0].reset();
$(".modal-captura").modal("show");

});

$( document ).on('click', '#btn_guardar', function(e) {
    e.preventDefault();
    var form = $("#form-captura");
    data = form.serialize();
    console.log(data);
    $.ajax({
        type: "POST",
        url: "<?php echo base_url("graficacion/capturar_graficacion_ventanilla"); ?>",
        type: "POST",
        data: data,
        cache: false,
        success: function(data){
            //console.log(data);
            if (data == 'ok')
            {
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'success',
                                    title: 'Mensaje del Sistema',
                                    text: 'La captura se Registro Correctamente'
                                    
                                })
                //cerramos el modal 
                $('.modal-captura').modal('hide');
                $('#tbl-graficaciones').DataTable().ajax.reload(null, false);
            }
            else{
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'Ocurrió un error al guardar la Captura'
                                    
                                })
            }
        }
    });
});

$( document ).on('click', '#btn_buscar', function(e) {
    e.preventDefault();
    consulta_folio();
});


function consulta_folio(){
    var anio = $('#year').val();
        var folio = $('#folio').val();
        //consultar el folio
        var url = "<?php echo base_url("graficacion/consultar_folio");?>/"+anio+"/"+folio;
        $.getJSON(url, function(json){
            var data = json.results;
            //console.log(data);
            if(data.length > 0)
            {
                //llenamos los datos en los destos
                $('#POBL').val(data[0].Pobl);
                $('#CTEL').val(data[0].Ctel);
                $('#MANZ').val(data[0].Manz);
                $('#PRED').val(data[0].Pred);
                $('#UNID').val(data[0].Unid);
                //si hay datos pues se llenan los demas controles
                if(data[0].F_AtendidoGraficacion != null)
                    $('#dtfecha').val(moment(data[0].F_AtendidoGraficacion).format('YYYY-MM-DD'));

                $('#graficador').val(data[0].Id_Graficador);
                $('#movimiento').val(data[0].Id_MovimientoGraficacion1);
                $('#resultado').val(data[0].Id_ResultadoGraficacion);
                $('#dictamen').text(data[0].Dictamen);
                $('#observaciones').text(data[0].Observaciones);
                
            }    
            else{
                 //mandamos mensaje de 
                 Swal.fire({
                                    icon: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'Folio no encontrado'
                                    
                                })
            }
        });
}

$(document).on('keypress','#folio',function(e) {
    if(e.which == 13) {
        consulta_folio();
    }
});

function get_anio(){
        var anio = new Date().getFullYear().toString(); 
        var a = anio.substr(2,4);
        $('#year').val(a); 
        return a;
    }
    
    function cargar_graficadores(){
        $('#graficador').empty();
        var url = "<?php echo base_url("graficacion/get_graficadores");?>";
        $.getJSON(url, function(json){
                $('#graficador').append($('<option>').text("Seleccionar Graficador").attr({'value': 0}));
                $.each(json.results, function(i, obj){
                        $('#graficador').append($('<option>').text(obj.Nombre + " " +obj.ApellidoPaterno).attr({'value': obj.IdEmpleado}));
                });     
        });
     }   

     function cargar_resultados(){
        $('#resultado').empty();
        var url = "<?php echo base_url("graficacion/get_resultados_graficacion");?>";
        $.getJSON(url, function(json){
                $('#resultado').append($('<option>').text("Ninguno").attr({'value': 0}));
                $.each(json.results, function(i, obj){
                        $('#resultado').append($('<option>').text(obj.Id_ResultadoGraficacion + " " +obj.Descripcion).attr({'value': obj.Id_ResultadoGraficacion}));
                });     
        });
     }   

     function cargar_movimientos(){
        $('#movimiento').empty();
        var url = "<?php echo base_url("graficacion/get_movimientos_graficacion");?>";
        $.getJSON(url, function(json){
                $('#movimiento').append($('<option>').text("Ninguno").attr({'value': 0}));
                $.each(json.results, function(i, obj){
                        $('#movimiento').append($('<option>').text(obj.Id_MovimientoGraficacion + " - " +obj.Descripcion).attr({'value': obj.Id_MovimientoGraficacion, 'desc': obj.Descripcion}));
                });     
        });
     }        



</script>