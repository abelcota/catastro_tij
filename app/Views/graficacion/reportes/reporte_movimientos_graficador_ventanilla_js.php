<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
 <!-- Buttons examples -->
 <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
   
   <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
   

   <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


   <!-- App js -->
   <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
   <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
   <script src="<?php echo base_url("/assets/js/dataTables.fixedHeader.min.js");?>"></script>
   <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
   
<script>

var table;

$(document).on('click', '#btn_consultar', function (e) {
    e.preventDefault();
    var graf = "";
    var f1 = $("#start").val();
    var f2 = $("#end").val();

    //obtener el radio seleccionado 
    var url = "<?php echo base_url("graficacion/get_reporte_movimientos_graficador_ventanilla")?>/"+f1+"/"+f2;
    console.log(url);
    //Destroy the old Datatable
    $('#tbl-rpt').DataTable().destroy();
    $('#tbl-rpt').DataTable({
    "language": {
        "url": "<?php echo base_url("assets/Spanish.json")?>"
    },
    "sAjaxSource": url,
    "bPaginate": false,
    "bProcesing": true,
    "bFilter": false,
    "bInfo": false,
    //"order": [[ 0, "asc" ]],
    "aoColumns": [
        { mData: 'Id_Graficador' } ,
        { mData: 'nom' } ,
        { mData: 'id' },
        { mData: 'Descripcion' },
        { mData: 'total' },
    ],
    rowCallback: function(row, data, index) {
        if(data['Id_Graficador'] == graf){
            $("td:eq(0)", row).text(''); 
            $("td:eq(1)", row).text(''); 
        }
        else{
            graf = data['Id_Graficador'];
        }
            
        $("td:eq(0)", row).addClass('small'); 
        $("td:eq(1)", row).addClass('small'); 
        $("td:eq(2)", row).addClass('small'); 
        $("td:eq(3)", row).addClass('small'); 
        $("td:eq(4)", row).addClass('small'); 
    },
   

    });

});

$(document).ready(function () {
    cargar_brigadas()
});

function formatFriendCode(friendCode, numDigits, delimiter) {
  return Array.from(friendCode).reduce((accum, cur, idx) => {
    return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
  }, '')
}

function cargar_brigadas(){
        $('#brigadas').empty();
        var url = "<?php echo base_url("inspeccion/get_brigadas_inspeccion");?>";
        $.getJSON(url, function(json){
            //console.log(json);  
                $.each(json.results, function(i, obj){
                        $('#brigadas').append($('<option>').text(obj.Id_Brigada + " - " +obj.brigada).attr({'value': obj.Id_Brigada, 'i1': obj.i1, 'i2': obj.i2}));
                        $('#brigadas2').append($('<option>').text(obj.Id_Brigada + " - " +obj.brigada).attr({'value': obj.Id_Brigada, 'i1': obj.i1, 'i2': obj.i2}));
                });     
        });
     }   

</script>