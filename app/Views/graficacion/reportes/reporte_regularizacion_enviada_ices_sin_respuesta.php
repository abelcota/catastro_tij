<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">REPORTE DE REGULARIZACIONES ENVIADAS A ICES SIN RESPUESTA</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">GRAFICACIÓN</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <!-- start row -->
                    <div class="row ">
                    <div class="col-lg-12">
                        <div class="btn-toolbar card d-print-none" role="toolbar">
                            <div class="card-body">
                                <div class="row">
                                <div class="col-lg-9">
                                    <label>Rango de Fechas:</label>
                                                <div>
                                                    <div class="input-daterange input-group" data-provide="datepicker">
                                                        <input type="date" class="form-control" name="start" id="start" />
                                                        <input type="date" class="form-control" name="end" id="end" />
                                                    </div>
                                                </div>
            
                                    </div>

                                    <div class="col-lg-3">
                                            
                                                <a href="javascript:;" class="btn btn-primary waves-effect waves-light btn-block mt-2" id="btn_consultar"><i class="fa fa-search"></i> Consultar</a>
                                                <a href="javascript:window.print()" class="btn btn-success waves-effect btn-block mt-2 waves-light"><i class="fa fa-print"></i> Imprimir Reporte</a>
                                               
                                    </div>
                                </div>            
                            </div>
                        </div>
                        <!--end toolbar -->             
                    </div>                
                    <!--inicia el reporte -->
                    <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="invoice-title">
                                    <div class="row">
                                        <div class="col-md-4 text-center">
                                            <img src="<?php echo base_url("assets/images/logo_catastro1.jpg")?>" alt="logo" height="100" />
                                            <img src="<?php echo base_url("assets/images/logo_h.png")?>" alt="logo" height="100" />
                                        </div>
                                        <div class="col-md-4 text-center">
                                        <span class="font-size-14"><b>H. AYUNTAMIENTO DE TIJUANA<br>DIRECCIÓN DE CATASTRO MUNICIPAL<br>TESORERIA - UNIDAD DE CATASTRO<br>DEPARTAMENTO DE GRAFICACIÓN</b><br>Programa Regularización Enviados al ICES Sin Respuesta</span>
                                        </div>
                                        <div class="col-md-4 ">
                                            <br><span class="float-right font-size-14 text-right ">Fecha de Impresión: <br><?php echo date("d/m/Y"); ?></span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="table-responsive">
                                    <table class="table table-hover table-sm" id="tbl-rpt" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead class="text-white" style="background-color: #480912;">
                                                <tr>
                                                    <th>Numero Oficio</th>
                                                    <th>Fecha Oficio</th>
                                                    <th>Cve. Trámite</th>
                                                    <th>Trámite</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    <!-- termina el reporte -->

                    </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->