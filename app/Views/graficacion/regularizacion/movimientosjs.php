<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
 <!-- Buttons examples -->
 <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
   
   <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
   

   <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


   <!-- App js -->
   <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
   <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
   <script src="<?php echo base_url("/assets/js/dataTables.fixedHeader.min.js");?>"></script>
   <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
   
<script>
$(document).ready(function () {

var table = $('#tbl-rpt').DataTable({
    "orderCellsTop": true,
    "language": {
        "url": "<?php echo base_url("assets/Spanish.json")?>"
    },
    "bProcessing": true,
    "sAjaxSource": "<?php echo base_url("graficacion/consultar_movimientos_reg_ventanilla")?>",
    "bPaginate": false,
    "bFilter": false,
    "bInfo": true,
    "order": [[ 0, "desc" ], [2, "desc"]],
    "aoColumns": [
        { mData: 'Clave' } ,
        { mData: 'DescripcionTramite' },
        { mData: 'FechaCapturaInconformidad' },
        { mData: 'F_InspeccionCampo' },
        { mData: 'Descripcion' },
    ],
    rowCallback: function(row, data, index) {
            $("td:eq(0)", row).addClass('small');  
            $("td:eq(1)", row).addClass('small');          
            $("td:eq(2)", row).text(moment(data['FechaCapturaInconformidad']).locale('es').format('L'));
            $("td:eq(2)", row).addClass('small');
            $("td:eq(3)", row).text(moment(data['F_InspeccionCampo']).locale('es').format('L'));
            $("td:eq(3)", row).addClass('small');
            $("td:eq(4)", row).addClass('small');
        }
});

});

</script>