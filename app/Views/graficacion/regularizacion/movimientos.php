<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">REPORTE DE MOVIMIENTOS DE REGULARIZACIÓN DE TRÁMITES DE VENTANILLA</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">GRAFICACIÓN</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <!-- start row -->
                    <div class="row">

                    <div class="col-lg-12">
                    <div class="d-print-none">
                                        <div class="float-right">
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light"><i class="fa fa-print"></i> Imprimir Reporte</a>
                                           
                                        </div>
                                    </div>
                    </div>

                    <!--inicia el reporte -->
                    <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="invoice-title">
                                    <div class="row">
                                        <div class="col-md-4 text-center">
                                            <img src="<?php echo base_url("assets/images/logo_catastro1.jpg")?>" alt="logo" height="100" />
                                            <img src="<?php echo base_url("assets/images/logo_h.png")?>" alt="logo" height="100" />
                                        </div>
                                        <div class="col-md-4 text-center">
                                        <span class="font-size-14"><b>H. AYUNTAMIENTO DE TIJUANA<br>DIRECCIÓN DE CATASTRO MUNICIPAL<br>TESORERIA - UNIDAD DE CATASTRO</b></span><br><span class="font-size-13">Departamento de Graficación <br>Predios Inspeccionados por Regularización con Inconformidad emitida anteriormente</span>
                                        </div>
                                        <div class="col-md-4 ">
                                            <br><span class="float-right font-size-14 text-right ">Fecha de Impresión: <br><?php echo date("d/m/Y"); ?></span>
                                        </div>
                                    </div>
                                
                                    <div class="table-responsive">
                                        <table class="table table-sm" id="tbl-rpt">
                                            <thead class="text-white" style="background-color: #480912;">
                                                <tr>
                                                    <th class="small">Clave Cat</th>
                                                    <th class="small">Trámite</th>
                                                    <th class="small">F. Ventanilla</th>
                                                    <th class="small">F. Inspección</th>
                                                    <th class="small">Movimiento</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    <!-- termina el reporte -->

                    </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->