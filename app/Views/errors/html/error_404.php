<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <title>404 | Pagina no Encontrada</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url("assets/images/favicon.ico"); ?>">

    <!-- Bootstrap Css -->
    <link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?php echo base_url("assets/css/icons.min.css"); ?>" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?php echo base_url("assets/css/app.min.css"); ?>" id="app-style" rel="stylesheet" type="text/css" />
	<style>
        body {
            background: linear-gradient(rgba(255, 255, 255, 0.95),
            rgba(255, 255, 255, 0.9)),url(<?php echo base_url("assets/images/fondo2.png");?>);
            background-attachment: fixed;
            background-size: cover;
            padding: 1em 0;
      
        }
    </style>
</head>
<body>
    <div class="home-btn d-none d-sm-block">
        <a href="<?php echo base_url("/"); ?>" class="text-dark"><i class="fas fa-home h2"></i></a>
    </div>
    <div class="account-pages my-5 pt-sm-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card overflow-hidden">

                        <div class="card-body">
							<div class="text-center">
							   <img src="<?php echo base_url("assets/images/logo_catastro1.png");?>" alt="" height="80">
							   <img src="<?php echo base_url("assets/images/logo_h.png");?>" alt="" height="100">
                            </div>

                            <div class="text-center p-3">

                                <div class="img">
                                    <img src="<?php echo base_url("assets/images/404.png"); ?>" class="img-fluid" alt="">
                                </div>
                                <h4 class="mb-4">Página no encontrada</h4>
                                <a class="btn btn-secondary mb-4 waves-effect waves-light" style="background-color: #480912!important;" href="<?php echo base_url("/"); ?>"><i class="mdi mdi-home" ></i> Regresar</a>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- JAVASCRIPT -->
    <script src="<?php echo base_url("assets/libs/jquery/jquery.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/libs/bootstrap/js/bootstrap.bundle.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/libs/metismenu/metisMenu.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/libs/simplebar/simplebar.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/libs/node-waves/waves.min.js"); ?>"></script>

    <script src="<?php echo base_url("assets/js/app.js"); ?>"></script>

</body>

</html>