<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">NOTAS ISAI</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">NOTARIO</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <!-- start row -->
                    <div class="row">
                     
                    <div class="col-lg-12">
                        <div class="btn-toolbar card d-print-none" role="toolbar">
                            <div class="card-body">
                                <div class="row">
                                
                                    <div class="col-lg-12">                                                
                                        <button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_nuevo"><i class="mdi mdi-receipt"></i> Registrar Nueva Nota ISAI</button>

                                    </div>

                                   
                                </div>            
                            </div>
                        </div>
                        <!--end toolbar -->             
                    </div> 

                    <div class="col-lg-12" id="tblbusqueda">
                    
                    </div>
                          <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    
                                <h5 class="card-title">Bitácora de Notas ISAI Registradas</h5>
                                <hr>
                                     <!-- start form -->

                                        <div class="row">
                                            <div class="col-lg-12 table-responsive" style="margin-top:10px;">
                                                <table class="table display table-hover nowrap" id="tbl-bitacoras_notas">
                                                    <thead class="text-white" style="background-color: #480912;">
                                                        <th class="text-center"></th>
                                                        <th class="text-center">No. Nota</th> 
                                                        <th class="text-center">Folio Escritura</th>
                                                        <th class="text-center">Clave Catastral</th>
                                                        <th class="text-center">Enajenante</th>
                                                        <th class="text-center">Adquiriente</th>
                                                        <th class="text-center">Fecha</th>
                                                        
                                                    </thead>
                                                    
                                                    </table>
                                            </div>
                                        </div> 
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->

                     
                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->


            
        <div class="modal fade modal-nueva-nota" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
                                                <div class="modal-dialog modal-xl modal-dialog-top">
                                                    <div class="modal-content">
                                                    
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Datos para registro de la Nota de ISAI</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                                     
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <ul class="nav nav-pills nav-fill" role="tablist">
                                                                        <li class="nav-item p-1 ">
                                                                            <a class="nav-link border active" data-toggle="tab" href="#li1" role="tab">
                                                                                <span class="d-block d-sm-none"><i class="mdi mdi-receipt"></i></span>
                                                                                <span class="d-none d-sm-block "><i class="mdi mdi-receipt"></i> Datos de la Nota</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="nav-item p-1 ">
                                                                            <a class="nav-link border" data-toggle="tab" href="#li2" role="tab">
                                                                                <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                                                                                <span class="d-none d-sm-block "><i class="mdi mdi-account"></i> Datos del Notario</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="nav-item p-1">
                                                                            <a class="nav-link border" data-toggle="tab" href="#li3" role="tab">
                                                                                <span class="d-block d-sm-none"><i class="mdi mdi-cash"></i></span>
                                                                                <span class="d-none d-sm-block "><i class="mdi mdi-cash"></i> Valores</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="nav-item p-1 ">
                                                                            <a class="nav-link border" data-toggle="tab" href="#li4" role="tab">
                                                                                <span class="d-block d-sm-none"><i class="mdi mdi-currency-usd"></i></span>
                                                                                <span class="d-none d-sm-block "><i class="mdi mdi-currency-usd"></i> Liquidación</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="nav-item p-1 ">
                                                                            <a class="nav-link border" data-toggle="tab" href="#li5" role="tab">
                                                                                <span class="d-block d-sm-none"><i class="mdi mdi-sale"></i></span>
                                                                                <span class="d-none d-sm-block "><i class="mdi mdi-sale"></i> Descuento</span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="nav-item p-1">
                                                                            <a class="nav-link border" data-toggle="tab" href="#li6" role="tab">
                                                                                <span class="d-block d-sm-none"><i class="mdi mdi mdi-currency-usd"></i></span>
                                                                                <span class="d-none d-sm-block "><i class="mdi mdi mdi-currency-usd"></i> Liquidación</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>

                                                                </div>
                                                                <div class="col-12 mt-2">
                                                                    <div class="tab-content">
                                                                        <div class="tab-pane fade in show active" id="li1">
                                                                            <div class="row p-2">
                                                                                
                                                                                <div class="form-group col-4">
                                                                                    <label for="txtFecha">Fecha</label>
                                                                                    <input id="txtFecha" name="txtFecha" type="date" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important;" value="" data-date-format="DD/MM/YYYY" />
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtTipoClave">Tipo de Clave</label>
                                                                                   <select name="txtTipoClave" id="txtTipoClave" class="form-control">
                                                                                       <option value="Normal">Normal</option>
                                                                                       <option value="Rustica">Rustica</option>
                                                                                   </select>
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtClaveCat">Clave Catastral</label>
                                                                                    <input type="text" class="form-control" id="txtClaveCat" name="txtClaveCat">
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtNota">Número de Nota</label>
                                                                                    <input type="text" class="form-control" id="txtNota" name="txtNota">
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtFolioEscritura">Folio de Escritura</label>
                                                                                    <input type="text" class="form-control" id="txtFolioEscritura" name="txtFolioEscritura">
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtTipoNota">Tipo de Nota</label>
                                                                                   <select name="txtTipoNota" id="txtTipoNota" class="form-control">
                                                                                       <option value="Normal">Normal</option>
                                                                                   </select>
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtNotaNormal">Número de nota normal</label>
                                                                                    <input id="txtNotaNormal" name="txtNotaNormal" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important;" value="" />
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtFechaFirma">Fecha de firma de escritura</label>
                                                                                    <input id="txtFechaFirma" name="txtFechaFirma" type="date" class="form-control" placeholder="" value="" />
                                                                                </div>

                                                                                <div class="form-group col-8">
                                                                                    <label for="txtEnajenante">Enajenante</label>
                                                                                    <input type="text" class="form-control" id="txtEnajenante" name="txtEnajenante">
                                                                                </div>
                                                                              
                                                                                <div class="form-group col-8">
                                                                                    <label for="txtAdquiriente">Nombre del adquiriente</label>
                                                                                    <input type="text" class="form-control" id="txtAdquiriente" name="txtAdquiriente">
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane fade in" id="li2">
                                                                            <div class="row p-2">
                                                                                
                                                                                <div class="form-group col-8">
                                                                                    <label for="txtTramitador">Tramitador</label>
                                                                                    <select name="txtTramitador" id="txtTramitador" class="form-control">
                                                                                       <option value="-">Seleccione un Notario</option>
                                                                                   </select>
                                                                                </div>

                                                                                <div class="form-group col-8">
                                                                                    <label for="txtNotarioSelected">Notario</label>
                                                                                    <input type="text" class="form-control" id="txtNotarioSelected" name="txtNotarioSelected" disabled="disabled" style="background-color:  #F0F0F0!important;" >
                                                                                </div>

                                                                                <div class="form-group col-8">
                                                                                    <label for="txtNotariaSelected">Notaría</label>
                                                                                    <input type="text" class="form-control" id="txtNotariaSelected" name="txtNotariaSelected" disabled="disabled" style="background-color:  #F0F0F0!important;" >
                                                                                </div>

                                                                                <div class="form-group col-8">
                                                                                    <label for="txtProcedenciaSelected">Procedencia</label>
                                                                                    <input type="text" class="form-control" id="txtProcedenciaSelected" name="txtProcedenciaSelected" disabled="disabled" style="background-color:  #F0F0F0!important;" >
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane fade in" id="li3">
                                                                            <div class="row p-2">

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtTipoAvaluo">Tipo de Avaluo</label>
                                                                                    <select name="txtTramitador" id="txtTipoAvaluo" class="form-control">
                                                                                       <option value="-">Seleccione</option>
                                                                                   </select>
                                                                                </div>

                                                                                <div class="form-group col-8">
                                                                                    <label for="txtPerito">Perito Valuador</label>
                                                                                    <select name="txtPerito" id="txtPerito" class="form-control">
                                                                                       <option value="-">Seleccione</option>
                                                                                   </select>
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtFechaAvaluo">Fecha del Avaluo</label>
                                                                                    <input id="txtFechaAvaluo" name="txtFechaAvaluo" type="date" class="form-control" placeholder="" value="" />
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtValorTerreno">Valor del terreno</label>
                                                                                    <input id="txtValorTerreno" name="txtValorTerreno" type="number" class="form-control" placeholder="" value="" />
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtValorConstruccion">Valor de la construcción</label>
                                                                                    <input id="txtValorConstruccion" name="txtValorConstruccion" type="number" class="form-control" placeholder="" value="" />
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtValorTotal">Valor total</label>
                                                                                    <input id="txtValorTotal" name="txtValorTotal" type="number" class="form-control" placeholder="" value="" />
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtValorOperacion">Valor de operación</label>
                                                                                    <input id="txtValorOperacion" name="txtValorOperacion" type="number" class="form-control" placeholder="" value="" />
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane fade in" id="li4">
                                                                            <div class="row p-2">

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtFechaVencimiento">Fecha de vencimiento</label>
                                                                                    <input id="txtFechaVencimiento" name="txtFechaVencimiento" type="date" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important;" value="" data-date-format="DD/MM/YYYY" />
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtBaseInpuesto">Base del impuesto</label>
                                                                                    <input id="txtBaseInpuesto" name="txtBaseInpuesto" type="number" class="form-control" placeholder="" value="" />
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <div class="custom-control custom-switch mt-3">
                                                                                        <input type="checkbox" class="custom-control-input" id="chkNuevaConstruccion">
                                                                                        <label class="custom-control-label" for="chkNuevaConstruccion" style="cursor:pointer; ">Nueva construcción de casa habitación de interés social</label>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group col-8">
                                                                                    <label for="txtConcepto">Concepto</label>
                                                                                    <select name="txtConcepto" id="txtConcepto" class="form-control">
                                                                                       <option value="Adquisicion de inmueble">Adquisición de inmueble</option>
                                                                                   </select>
                                                                                </div>

                                                                                <div class="form-group col-8">
                                                                                    <label for="txtObservaciones">Observaciones de la nota</label>
                                                                                    <input id="txtObservaciones" name="txtObservaciones" type="text" class="form-control" placeholder="" value="" />
                                                                                </div>


                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane fade in" id="li5">
                                                                           
                                                                            <div class="row p-2">

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtPorcentajeDescuento">Porcentaje de descuento</label>
                                                                                    <input id="txtPorcentajeDescuento" name="txtPorcentajeDescuento" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important;" value="" />
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtFechaInicialDescuento">Fecha inicial</label>
                                                                                    <input id="txtFechaInicialDescuento" name="txtFechaInicialDescuento" type="date" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important;" value="" data-date-format="DD/MM/YYYY" />
                                                                                </div>

                                                                                <div class="form-group col-4">
                                                                                    <label for="txtFechaFinalDescuento">Fecha final</label>
                                                                                    <input id="txtFechaFinalDescuento" name="txtFechaFinalDescuento" type="date" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important;" value="" data-date-format="DD/MM/YYYY" />
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane fade in" id="li6">
                                                                            <div class="row p-2">
                                                                                
                                                                                <div class="form-group col-4">
                                                                                    <label for="txtEstatus">Estatus</label>
                                                                                    <select name="txtEstatus" id="txtEstatus" class="form-control">
                                                                                       <option value="capturada">Capturada</option>
                                                                                       <option value="finalizada">Finalizada</option>
                                                                                   </select>
                                                                                </div>

                                                                                <div class="form-group col-8">
                                                                                    <label for="txtObservacionesEstatus">Observaciones del estatus</label>
                                                                                    <input id="txtObservacionesEstatus" name="txtObservacionesEstatus" type="text" class="form-control" placeholder="" value="" />
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                              
                                                        </div>
                                                        <div class="modal-footer">
                                                           
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar_autorizar">Cancelar</button>

                                                            <button type="button" class="btn btn-success" id="btn_autorizar"><i class="mdi mdi-check"></i> Guardar</button>
                                                          
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                             
            