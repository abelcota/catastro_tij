<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">AVALUO CATASTRAL</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">PERITOS</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <!-- start row -->
                    <div class="row">
                     
                    <div class="col-lg-12">
                        <div class="btn-toolbar card d-print-none" role="toolbar">
                            <div class="card-body">
                                <div class="row">
                                
                                    <div class="col-lg-12">
                                        <label>Consulta y Registro de Avaluos</label>
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected mt-1">
                                                        <div class="input-group-prepend">
                                                            <span style="cursor:pointer" class="input-group-text waves-effect" data-container="body" data-toggle="popover" data-placement="bottom" data-content="<image src='<?php echo base_url("assets/images/icat.png"); ?>' />" data-original-title="" title="" data-html="true" >
                                                           
                                                                <i class="mdi mdi-information-outline"></i> Inf. Catastral
                                                            </span>
                                                        </div>    
                                                        <input type="text" class="form-control" name="info_cat" id="info_cat" placeholder="0000000000"/>
                                                        <span class="input-group-btn input-group-append"><button class="btn btn-info bootstrap-touchspin-up" type="button" id="btn_validar"><i class="mdi mdi-check"></i> Validar</button></span>
                                                        </div>
                                                        <div class="input-group mt-1 ml-1">
                                                            <input type="text" class="form-control" name="cvecat" id="cvecat" placeholder="Clave Catastral" readonly style="width:292px;"/>

                                                            <span class="input-group-btn input-group-append">
                                                            
                                                                <button class="btn btn-success bootstrap-touchspin-up" type="button" id="btn_nuevo" disabled><i class="mdi mdi-plus"></i> Nuevo</button>
                                                                <!--<button class="btn btn-success bootstrap-touchspin-up" type="button" id="btn_nuevo_pdf" disabled><i class="mdi mdi-plus"></i> Nuevo PDF <i class="mdi mdi-file-pdf"></i> </button>-->
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
            
                                    </div>

                                   
                                </div>            
                            </div>
                        </div>
                        <!--end toolbar -->             
                    </div> 

                    <div class="col-lg-12" id="tblbusqueda">
                    
                    </div>
                          <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    
                                <h5 class="card-title">Bitácora de Avaluos Registrados</h5>
                                <hr>
                                     <!-- start form -->

                                        <div class="row">
                                            <div class="col-lg-12 table-responsive" style="margin-top:10px;">
                                                <table class="table display table-hover nowrap" id="tbl-bitacoras">
                                                    <thead class="text-white" style="background-color: #480912;">
                                                        <th class="text-center"></th>
                                                        <th class="text-center">Inf. Catastral</th>
                                                        <th class="text-center">Clave Catastral</th>
                                                        <th class="text-center">Estatus</th>
                                                        <th class="text-center">Graficación</th>
                                                        <th class="text-center">Fecha Creación</th>
                                                        <th class="text-center">Última Modificacion</th>
                                                        <th class="text-center">Fecha Autorización</th>
                                                        
                                                    </thead>
                                                    
                                                    </table>
                                            </div>
                                        </div> 
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->

                     
                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            