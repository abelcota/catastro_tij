<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
 <!-- Buttons examples -->
 <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
   
   <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
   

   <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


   <!-- App js -->
   <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/peritos/utils.js")."?v".rand(); ?>" ></script>

   
<script>

function formatFriendCode(friendCode, numDigits, delimiter) {
        return Array.from(friendCode).reduce((accum, cur, idx) => {
            return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
        }, '')
    }

$(document).on('keydown', '#info_cat', function(e) {
    $('#cvecat').val("");
    $("#btn_nuevo").attr("disabled", "disabled");
    $("#btn_nuevo_pdf").attr("disabled", "disabled");
    $("#btn_cargar").attr("disabled","disabled");
});

$(document).on('click', '#btn_cargar', function(e) {
    //document.location = "AvaluoWeb.aspx?inf=" + $("#textInfo").val().trim() + "&mpio=" + $("#selectMpio").val();
});

function consultar(folio){
    document.location = "solicitudavaluo/" + folio;
}

$(document).on('click', '#btn_nuevo', function(e) {
    //preguntar si en realidad se desea cancelar el tramite
    Swal.fire({
                title: 'Confirmar el Nuevo Avaluo',
                text: "Una vez confirmado se asignará su usuario de perito como editor de este folio, por seguridad escriba el numero de solicitud para continuar:",
                icon: 'success',
                input: 'text',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Confirmar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        if (result.value) {
                            var json = new Object();
                            json.infcat = $('#info_cat').val();
                            json.numsol = result.value;
                            json.perito = "<?php echo $usuario->id; ?>";
                            json.cvecat = $('#cvecat').val().replaceAll("-","");
                            //console.log("Result: " + result.value);
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url("peritos/nuevo_avaluo"); ?>",
                                type: "POST",
                                data: json,
                                cache: false,
                                success: function(data){
                                    console.log(data);
                                    if (data == 'ok')
                                    {
                                        $('#tbl-bitacoras').DataTable().ajax.reload(null, false);
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                    icon: 'info',
                                                    title: 'Avaluo creado correctamente',
                                                    text: '¿Desea cargar el avaluo nuevo?',
                                                    showCancelButton: true,
                                                    confirmButtonColor: '#3085d6',
                                                    cancelButtonColor: '#d33',
                                                    cancelButtonText: 'No',
                                                    confirmButtonText: 'Si '              
                                        }).then((result) => {
                                            if (result.isConfirmed) {
                                                consultar($('#info_cat').val());
                                            }
                                        })
                                        
                                       
                                    }
                                    else if(data == 'error'){
                                        //mandamos mensaje de 
                                        Swal.fire({
                                                            icon: 'error',
                                                            title: 'Mensaje del Sistema',
                                                            text: 'Ocurrió un error al registrar el avaluo'
                                                            
                                                        })
                                    }
                                    else if(data == 'validacion'){
                                         //mandamos mensaje de 
                                         Swal.fire({
                                                            icon: 'warning',
                                                            title: 'Verificacion de Información',
                                                            text: 'El numero de solicitud no coincide con la informacion catastral, por favor verifique e intente de nuevo'
                                                            
                                                        })
                                    }
                                    //document.location = "AvaluoWeb.aspx?inf=" + $("#textInfo").val().trim() + "&mpio=" + $("#selectMpio").val();
                                }
                            });

                        }
                        
                            
                        
                    }
                })
    
});

$(document).on('click', '#btn_validar', function(e) {
    e.preventDefault();
    var cve_info = $('#info_cat').val();
    if(cve_info === '' || cve_info === null){
        Swal.fire({
                        icon: 'error',
                        text: 'Ingrese un folio válido para continuar',
                        })
    }else{
        //alert(cve_info); call ajax request
        $.ajax({
                url: "<?php echo base_url("peritos/validar_infocat");?>/"+cve_info,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Folio...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                       if(data.result.length > 0){
                            if(data.result[0].estatus === 'S'){
                                
                                    Swal.fire({
                                    icon: 'warning',
                                    text: 'El folio ingresado ya se encuentra asignado a un avaluo',
                                    })
                                $('#cvecat').val('');
                            }
                            else{
                                $('#btn_nuevo').prop( "disabled", false );
                                $("#btn_nuevo_pdf").attr("disabled", false);
                                $('#cvecat').val(formatFriendCode(data.result[0].clave_catastral, 3, '-').slice(0, -1));
                            }
 

                       }
                            
                        else{
                            Swal.fire({
                            icon: 'error',
                            text: 'El folio ingresado no se encuentra registrado',
                            })
                        }
                            
                    },
                   complete : function (){
                        $.unblockUI();
                    }
               });
    }
    
});

$(document).ready(function () {

// Setup - add a text input to each footer cell
$('#tbl-bitacoras tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input class="form-control small" type="text" placeholder="Buscar '+title+'" />' );
    } );

var table = $('#tbl-bitacoras').DataTable({
    "orderCellsTop": true,
    "language": {
        "url": "<?php echo base_url("assets/Spanish.json")?>"
    },
    "bProcessing": true,
    "sAjaxSource": "<?php echo base_url("peritos/get_bitacora_avaluos")."/".$usuario->id;?>",
    "bPaginate":true,
    "sPaginationType":"full_numbers",
    "iDisplayLength": 25,
    "lengthMenu": [ [10, 25, 50, 100, 200, -1], [10, 25, 50,100,200, "Todos"] ],
    "order": [[ 0, "desc" ], [1, "desc"]],
    "aoColumns": [
        {
                data: null,
                defaultContent: '<a class="consultar text-primary" style="cursor:pointer !important;"> <i class="dripicons-information"></i></a> <a class="ml-3 eliminar text-danger" style="cursor:pointer !important;"> <i class="dripicons-trash"></i></a>'
        },
        { mData: 'informacion_id' } ,
        { mData: 'clave_cat' },
        { mData: 'estatus' },
        { mData: 'graficado' },
        { mData: 'fecha_creacion' },
        { mData: 'ultima_modificacion' },
        { mData: 'fecha_autorizacion' }
        
    ],
    rowCallback: function(row, data, index) {
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        $("td:eq(1)", row).addClass('text-primary font-weight-bold ');
        $("td:eq(2)", row).text(formatFriendCode(data['clave_cat'], 3, '-').slice(0, -1));
 
        $("td:eq(2)", row).addClass('font-weight-bold');

        if(data['estatus'] == 'Edicion')
            $("td:eq(3)", row).html('<span class="badge badge-primary font-size-12">Edición</span>'); 
        else if(data['estatus'] == 'Enviado'){
            $("td:eq(3)", row).html('<span class="badge badge-success font-size-12">Enviado</span>'); 
        }else if(data['estatus'] == 'Cancelado'){
            $("td:eq(3)", row).html('<span class="badge badge-danger font-size-12">Cancelado</span>'); 
        }else if(data['estatus'] == 'Regresado'){
            $("td:eq(3)", row).html('<span class="badge badge-warning font-size-12">Regresado</span>'); 
        }   

        if(data['graficado'] == '1')
            $("td:eq(4)", row).html('<span class="badge badge-success font-size-12">Requiere graficación</span>'); 
        else{
            $("td:eq(4)", row).html('<span class="badge badge-secondary font-size-12">No requiere graficación</span>'); 
        }  
        if(data['fecha_creacion'] != null)
            $("td:eq(5)", row).text(moment(data['fecha_creacion']).locale('es').format('DD/MM/YYYY'));
        if(data['ultima_modificacion'] != null)
            $("td:eq(6)", row).text(moment(data['ultima_modificacion']).locale('es').format('DD/MM/YYYY'));
        if(data['fecha_autorizacion'] != null)
            $("td:eq(7)", row).text(moment(data['fecha_autorizacion']).locale('es').format('DD/MM/YYYY'));
    }
}); 


    //click al boton consultar
    $('#tbl-bitacoras tbody').on( 'click', '.consultar', function () {
                if($('#tbl-bitacoras').DataTable().row(this).child.isShown()){
                    var data = $('#tbl-bitacoras').DataTable().row(this).data();
                }else{
                    var data = $('#tbl-bitacoras').DataTable().row($(this).parents("tr")).data();
                }
                var fol = data["informacion_id"];

                Swal.fire({
                title: '¿Cargar el folio: '+fol+'?',
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Consultar!'
                }).then((result) => {
                    if (result.value) {
                        consultar(fol);
                    }
                })
        });


});

    //funcion para formatear los campos de la clave catastral
    function zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }

    function formatFriendCode(friendCode, numDigits, delimiter) {
        return Array.from(friendCode).reduce((accum, cur, idx) => {
            return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
        }, '')
    }

</script>