<script src="<?php echo base_url("assets/js/ol.js");?>"></script>
<script src="<?php echo base_url("assets/js/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList");?>"></script>
<script src="<?php echo base_url("assets/js/ol-layerswitcher.js");?>"></script>
<script src="<?php echo base_url("assets/js/ol-contextmenu.js");?>"></script>

<script>
var key = "pk.eyJ1IjoiYWJlbGRleCIsImEiOiJjajNkamdoNm0wMDA3MzJubjZ5MXdzNGUxIn0.e3K1JXrhWSXvZ13TwvLdSw";
var url = "https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/256/{z}/{x}/{y}?access_token=" + key;
        //estilos para las capas
        var image = new ol.style.Circle({
             radius: 5,
             fill: null,
             stroke: new ol.style.Stroke({ color: 'red', width: 1 })
         });

         var callesStyle = new ol.style.Style({
                    text: new ol.style.Text({
                        font: 'bold 9px "Open Sans", "Arial Unicode MS", "sans-serif"',
                        placement: 'line',
                        fill: new ol.style.Fill({
                        color: 'black',
                        }),
                        stroke: new ol.style.Stroke({
                            color: 'white',
                            width: 2
                        }),
                    }),
            });

            function manzanasStyle (feature){
             var texto = "";
             if (feature.get('Manzana') != null)
             {
                texto = feature.get('Manzana').toString();
             }
            return new ol.style.Style({
                    stroke: new ol.style.Stroke({
                            color: 'brown',
                            width: 1
                        }),
                        fill: new ol.style.Fill({
                            color: 'rgba(255,255,255,0.1)'
                        }),
                        text: new ol.style.Text({
                            font: '9px Arial,sans-serif',
                            fill: new ol.style.Fill({ color: 'brown' }),
                            stroke: new ol.style.Stroke({
                            color: '#fff', width: 2
                            }),
                            // get the text from the feature - `this` is ol.Feature
                            // and show only under certain resolution
                            text: texto
                        })
        });
        }

        function makePattern() {
            var cnv = document.createElement('canvas');
            var ctx = cnv.getContext('2d');
            cnv.width = 8;
            cnv.height = 8;
            ctx.fillStyle = 'rgb(171, 171, 171)';
            
            for(var i = 0; i < 8; ++i) {
                ctx.fillRect(i, i, 1, 1);
            }
            
            return ctx.createPattern(cnv, 'repeat');
        }


         function pStyle (feature){
             var texto = "";
             if (feature.get('Predio') != null)
             {
                texto = feature.get('Predio').toString();
             }
            return new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: 'rgb(160, 82, 45)',
                            width: 1
                        }),
                        fill: new ol.style.Fill({
                            color: 'rgba(255, 255, 255, 0.1)'//makePattern()//'rgba(255, 255, 255, 0.1)'
                        }),
                        text: new ol.style.Text({
                            font: '9px Arial,sans-serif',
                            fill: new ol.style.Fill({ color: 'rgb(0, 0, 0)' }),
                            stroke: new ol.style.Stroke({
                            color: '#fff', width: 1
                            }),
                            // get the text from the feature - `this` is ol.Feature
                            // and show only under certain resolution
                            text: texto.replaceAll('0','')
                        }),
                        
        });
        }

        function constStyle (feature){
            return new ol.style.Style({
                    stroke: new ol.style.Stroke({
                            color: 'rgb(184, 134, 11)',//'magenta',
                            width: 0.5
                        }),
                        fill: new ol.style.Fill({
                            color: makePattern()//'rgba(255, 255, 255, 0.1)'
                        }),
                        /*text: new ol.style.Text({
                            font: '5px Calibri,sans-serif',
                            fill: new ol.style.Fill({ color: '#000' }),
                            stroke: new ol.style.Stroke({
                            color: '#fff', width: 1
                            }),
                            // get the text from the feature - `this` is ol.Feature
                            // and show only under certain resolution
                            text: feature.get('Clasificacion')
                        })*/
        });
        }

        function predioStyle (feature){
            var texto = "";
             if (feature.get('f2') != null)
             {
                texto = feature.get('f2').toString();
             }
            return new ol.style.Style({
                    stroke: new ol.style.Stroke({
                            color: 'blue',
                            width: 2
                        }),
                        fill: new ol.style.Fill({
                            color: 'rgba(220, 20, 60, 0.5)'
                        }),text: new ol.style.Text({
                            font: '12px Arial,sans-serif',
                            
                            fill: new ol.style.Fill({ color: 'rgb(255, 255, 255)' }),
                            stroke: new ol.style.Stroke({
                            color: '#fff', width: 1
                            }),
                            // get the text from the feature - `this` is ol.Feature
                            // and show only under certain resolution
                            text: texto.substr(12,3)
                        }),
                        
        });
        }

//sources layers
var callesSource = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: function (extent) {
                return (
                'http://geoservervte.sinaloa.gob.mx/geoserver/ices/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ices%3ARedVialCuliacan&outputFormat=application%2Fjson&srsname=EPSG:3857&' +
                'bbox=' +
                extent.join(',') +
                ',EPSG:3857'
                );
            },
            crossOrigin: 'Anonymous',
            strategy: ol.loadingstrategy.bbox,
        });

         var manzanasSource = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: function (extent) {
                return (
                '<?php echo base_url(""); ?>:8082/geoserver/catastro/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=catastro%3Amanzanas&outputFormat=application%2Fjson&srsname=EPSG:3857&' +
                'bbox=' + extent.join(',') + ',EPSG:3857');
            },
            crossOrigin: 'Anonymous',
            strategy: ol.loadingstrategy.bbox,
        });

        var prediosSource = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: function (extent) {
                return (
                '<?php echo base_url(""); ?>:8082/geoserver/catastro/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=catastro%3Apredio&outputFormat=application%2Fjson&srsname=EPSG:3857&' +
                'bbox=' +
                extent.join(',') +
                ',EPSG:3857'
                );
            },
            crossOrigin: 'Anonymous',
            strategy: ol.loadingstrategy.bbox,
        });

        var construccionesSource = new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: function (extent) {
                return (
                '<?php echo base_url(""); ?>:8082/geoserver/catastro/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=catastro%3Aconstruccion&outputFormat=application%2Fjson&srsname=EPSG:3857&' +
                'bbox=' +
                extent.join(',') +
                ',EPSG:3857'
                );
            },
            crossOrigin: 'Anonymous',
            strategy: ol.loadingstrategy.bbox,
        });


//inicializacion del mapa map
            var view = new ol.View({
						center: ol.proj.fromLonLat([-107.397653, 24.8009272]),
                 zoom: 19
			  });

            var map = new ol.Map({
                 controls: ol.control.defaults().extend([
                    new ol.control.FullScreen(), 
                    /*new ol.control.OverviewMap({
                        layers: [
                        new ol.layer.Tile({
                            source: new ol.source.OSM()
                        }) ]
                    }),*/
                ]),
  				layers: 
					[new ol.layer.Group({
						'title': 'Mapas Base',
						layers: [
                            new ol.layer.Tile({
                                title: 'MapBox',
                                visible: true,
                                type: 'base',
                                source: new ol.source.XYZ({
                                    url,
                                    crossOrigin: 'Anonymous',
                                })
                            }),
							new ol.layer.Tile({
								title: 'OSM',
								type: 'base',
								visible: false,
								source: new ol.source.OSM()
							})
                            
                           							
						]
					}),
					new ol.layer.Group({
						title: 'Capas',
						layers: [
                            new ol.layer.Tile({
                                title: 'Culiacan 2020',
                                visible: false,
                                source: new ol.source.TileWMS({
                                    url: '<?php echo base_url().":8081";?>/cgi-bin/mapserv.exe?map=C:/visualizador/servers/mapserver/ortofotos.map&REQUEST=GetMap&SERVICE=WMS&VERSION=1.1.1&LAYERS=ortofotos&FORMAT=image/png&STYLES=&SRS=EPSG:6368&BBOX=245089,2734170,264848,2754943&WIDTH=768&HEIGHT=768',
                                crossOrigin: 'Anonymous',
                                })
                            }),
                            new ol.layer.Vector({
                                title: 'Calles',
                                declutter: true,
                                visible: false,
                                preload: Infinity,
                                source: callesSource,
                                style: function (feature) {
                                    callesStyle.getText().setText(feature.get('Nombre'));
                                    return callesStyle;
                                },
                            }),
							new ol.layer.Vector({
                                title: 'Manzanas',
                                visible: true,
                                preload: Infinity,
                                source: manzanasSource,
                                style: manzanasStyle,
                            }),
                            new ol.layer.Vector({
                                title: 'Predios',
                                visible: true,
                                preload: Infinity,
                                source: prediosSource,
                                style: pStyle,
                            }),
                            new ol.layer.Vector({
                                title: 'Construcciones',
                                visible: true,
                                preload: Infinity,
                                source: construccionesSource,
                                style: constStyle,
                            })
						]
					})
					],	
					target: 'map',
					view: view
			});
			
			var layerSwitcher = new ol.control.LayerSwitcher({
				tipLabel: 'Leyenda'
			});
			//map.addControl(layerSwitcher);
			//layerSwitcher.showPanel();

            map.getInteractions().forEach(function(interaction) {
            if (interaction instanceof ol.interaction.MouseWheelZoom) {
                interaction.setActive(false);
            }
            }, this);


//funciones del mapa
        function streetview(evt){
            var coords = ol.proj.toLonLat(evt.coordinate);
            var lat = coords[1];
            var lon = coords[0];
            var locTxt = "Latitude: " + lat + " Longitude: " + lon;
            var url = "http://maps.google.com/?cbll="+lat+","+lon+"&cbp=12,90,0,0,5&layer=c";
            // coords is a div in HTML below the map to display
            //document.getElementById('coords').innerHTML = locTxt;
            //console.log(locTxt);
            //console.log(url);
            window.open(url,'popup','width=800,height=600,scrollbars=no,resizable=no');
        }

        var contextmenu = new ContextMenu({
            width: 200,
            defaultItems: false, // defaultItems are (for now) Zoom In/Zoom Out
            items: [
                {
                text: 'Abrir Street View',
                classname: 'text-primary',
                icon: '/assets/images/street-view.png', // add some CSS rules
                callback: streetview // `center` is your callback function
                },                
            ]
            });
            map.addControl(contextmenu);

        
         function visualizar_capa(_geojson_object) {
             _geojson_vectorSource = new ol.source.Vector({
                 features: (new ol.format.GeoJSON()).readFeatures(_geojson_object, { featureProjection: 'EPSG:3857' 
                 }),crossOrigin: 'Anonymous',
             });

             _geojson_vectorLayer = new ol.layer.Vector({
                 visible: true,
                 source: _geojson_vectorSource,
                 style: predioStyle  
             });

             map.addLayer(_geojson_vectorLayer);
             //layers[-1].setVisible();
             var extent = _geojson_vectorLayer.getSource().getExtent();
             map.getView().fit(extent, { size: map.getSize(), maxZoom: 20 })
         }


</script>