<style type="text/css">
/* Define a custom page size */
@page {
    size: 21.59cm 27.94cm;
}

/* Use letter paper */
@page {
  size: letter;
}

/* Use letter paper in landscape orientation */
@page {
  size: letter portrait;
}
              
</style> 
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class=""style="font-size: 14px; !important; font-family: Arial, Helvetica, sans-serif !important; color:black; ">

    <div class="page-content">
        <!-- start row -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">

                        <!--inicia el contenido -->
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="text-center d-print-none">
                                    <button id="botonImprimir" class="btn btn-outline-secondary">Imprimir</button>
                                </p>
                                <img src="<?php echo base_url("assets/images/cabecera_avaluo.png"); ?>" class="img-fluid" />
                                <p class="text-center">
                                    <strong style="font-size: 25px;">AVALUO DE INMUEBLE</strong>
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div id="divIMGPortada" class="row">
                                </div>
                                <div class='caption text-center'>
                                    <h4><span id="labelInmueblePortada"></span></h4>
                                </div>
                            </div>
                        </div>

                        <div class="card mt-3">
                           
                            <div class="card-body">
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Nombre del perito</strong>
                                </div>
                                <div class="col-9 ">
                                   
                                    <span id="labelNombrePerito"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Dirección</strong>
                                </div>
                                <div class="col-9 ">
                                   
                                    <span id="labelDireccionPerito"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Teléfono</strong>
                                </div>
                                <div class="col-9 ">
                                   
                                    <span id="labelTelefono"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Celular</strong>
                                </div>
                                <div class="col-9 ">
                                    
                                    <span id="labelCelular"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Correo</strong>
                                </div>
                                <div class="col-9 ">
                                   
                                    <span id="labelCorreo"></span>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Inmueble</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelInmueble"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Clave Catastral</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelClaveCatastral"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Ubicación</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelUbicacion"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Propietario</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelPropietario"></span>
                                </div>
                            </div>
                        </div>
                        </div>

                        <p style="page-break-before: always"></p>

                        <div class="row">
                            <div class="col-12">
                               
                                <h4 class="text-center" style="color: #480912">
                                    <strong>I.- ANTECEDENTES</strong>
                                </h4>
                                
                            </div>
                        </div>

                       
                        <div class="card border mt-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Solicitante del Avaluo</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelSolicitante"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Perito Valuador</strong>
                                </div>
                                <div class="col-9 ">
                                  
                                    <span id="labelPeritoValuador"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Numero de Registro</strong>
                                </div>
                                <div class="col-9 ">
                                    
                                    <span id="labelNumRegistro"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Especialidad</strong>
                                </div>
                                <div class="col-9 ">
                                    
                                    <span id="labelEspecialidad"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Fecha de inspección</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelFechaInspeccion"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Inmueble que se valua</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelInmuebleValua"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Regimen de propiedad</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelRegimen"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Propietario del inmueble</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelPropietarioInm"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Objeto del avaluo</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelObjeto"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Ubicación</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelUbi"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Reg. Prublico de la Propiedad</strong>
                                </div>
                                <div class="col-9 ">
                                    <span id="labelRegPubProp">Folio:, Inscripcion:, Libro:, Sección:</span>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <h4 class="text-center" style="color: #480912">
                                    <strong>II.-CARACTERÍSTICAS URBANAS</strong>
                                </h4> 
                            </div>
                        </div>

                        <div class="card border mt-3">
                            <div class="card-body">
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Clasificacion de la zona</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelClasificacionZona"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Tipo de construcción dominante en la calle</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelTipoConstruccionDominante"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Índice de saturación en la zona</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelIndiceSarturacion"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Población</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelPoblacion"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Contaminación ambiental</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelContaminacion"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Uso del suelo permitido</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelUsoSuelo"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Vías de acceso e importancias de las mismas</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelViasAcceso"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <hr style= "margin-top: 8px !important; margin-bottom: 8px !important; border: 0;border-top: 1px solid #eee;"  />
                                    <strong>Servicios públicos y equipamientos inmobiliario</strong>
                                    <hr style= "margin-top: 8px !important; margin-bottom: 8px !important; border: 0;border-top: 1px solid #eee;"  />
                                </div>
                                <div class="col-3 text-right">
                                    <strong>Agua potable</strong>
                                </div>
                                <div class="col-3">
                                    <span id="labelAgua"></span>
                                </div>
                                <div class="col-3 text-right">
                                    <strong>Teléfono</strong>
                                </div>
                                <div class="col-3">
                                    <span id="labelTel"></span>
                                </div>
                                <div class="col-3 text-right">
                                    <strong>Drenaje</strong>
                                </div>
                                <div class="col-3">
                                    <span id="labelDrenaje"></span>
                                </div>
                                <div class="col-3 text-right">
                                    <strong>Banquetas</strong>
                                </div>
                                <div class="col-3">
                                    <span id="labelBanquetas"></span>
                                </div>
                                <div class="col-3 text-right">
                                    <strong>Energía Eléctrica</strong>
                                </div>
                                <div class="col-3">
                                    <span id="labelEnergia"></span>
                                </div>
                                <div class="col-3 text-right">
                                    <strong>Guarniciones</strong>
                                </div>
                                <div class="col-3">
                                    <span id="labelGuarniciones"></span>
                                </div>
                                <div class="col-3 text-right">
                                    <strong>Alumbrado Público</strong>
                                </div>
                                <div class="col-3">
                                    <span id="labelAlumbrado"></span>
                                </div>
                                <div class="col-3 text-right">
                                    <strong>Pavimento</strong>
                                </div>
                                <div class="col-3">
                                    <span id="labelPavimento"></span>
                                </div>
                                <div class="col-3 text-right">
                                    <strong>Recolección de basura</strong>
                                </div>
                                <div class="col-3">
                                    <span id="labelBasura"></span>
                                </div>
                                <div class="col-3 text-right">
                                    <strong>Vigilancia policiaca</strong>
                                </div>
                                <div class="col-3">
                                    <span id="labelVigilancia"></span>
                                </div>
                                <div class="col-3 text-right">
                                    <strong>Transporte público</strong>
                                </div>
                                <div class="col-3">
                                    <span id="labelTransporte"></span>
                                </div>
                            </div>
                            </div>
                        </div>

                       
                        
                        <div class="row">
                            <div class="col-12">
                                <h5 class="text-center" style="color: #480912">
                                    <strong>CROQUIS</strong>
                                </h5> 
                            </div>
                        </div>

                        <div id="map" class="map" style="height: 400px;"></div>

                        <p style="page-break-before: always" />

                        <!--<div id="divCroquis" class="row" style="height: 400px">
                            <iframe src="https://vcc.sinaloa.gob.mx/visor/?predio=007212001024010001&zoom=19&fixed" id="frameCro" style="height: 400px; border: 0px; width: 100%"></iframe>
                        </div>-->

                        <div class="row">
                            <div class="col-12">
                                <h4 class="text-center" style="color: #480912">
                                    <strong>III.- CARACTERÍSTICAS DEL TERRENO</strong>
                                </h4> 
                               
                            </div>
                        </div>

                        <div class="card border mt-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <strong>TRAMOS DE CALLE, CALLES TRANSVERSALES LIMÍTROFES Y ORIENTACIÓN</strong>
                                    <hr style= "margin-top: 8px !important; margin-bottom: 8px !important; border: 0;border-top: 1px solid #eee;"  />
                                </div>
                                <div class="col-12">
                                    <strong>Medidas y colindancias segun escrituras</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="tablaOrientacion" class="table table-condensed table-hover table-bordered" style="color:black !important;">
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Superficie del terreno según escrituras</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelSuperficieEcrituras"></span>
                                    <strong>m2</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Superficie del terreno según catastro</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelSuperficieCatastro"></span>
                                    <strong>m2</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Superficie del terreno segun levantamiento topográfico</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelSuperficieTopografico"></span>
                                    <strong>m2</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Topografía y configuración</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelTopografiayconf"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Número de frentes</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelNumeroFrentes"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Características panorámicas</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelCaracteristicasPanoramicas"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Densidad habitacional</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelDensidadHabita"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Intencidad de construcción</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelIntencidadConstruccion"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Servidumbres y/o restricciones</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelServidumbresRestricciones"></span>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <h4 class="text-center" style="color: #480912">
                                    <strong>IV.- DESCRIPCIÓN GENERAL DEL INMUEBLE</strong>
                                </h4> 
                            </div>
                        </div>

                        <div class="card border mt-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Descripción General</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelDescripcionGeneralInmueble"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Uso actual</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelUsoActual"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Número de niveles</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelNumeroNiveles"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Edad aproximada</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelEdadAprox"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Estado de conservación</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelEstadoConservacion"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Calidad de proyecto</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelCalidadProyecto"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Unidades rentables</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelUnidadesRentables"></span>
                                </div>
                            </div>
                        </div>
                        </div>

                        <p style="page-break-before: always"></p>
                        <div class="row">
                            <div class="col-12">
                                <h4 class="text-center" style="color: #480912">
                                    <strong>V.- ELEMENTOS DE CONSTRUCCIÓN</strong>
                                </h4>
                               
                            </div>
                        </div>

                        <div class="card border mt-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <strong>a) Obra negra o gruesa</strong>
                                    <hr style= "margin-top: 8px !important; margin-bottom: 8px !important; border: 0;border-top: 1px solid #eee;"  />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Cimientos</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelCimientos"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Estructura</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelEstructura"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Muros</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelMuros"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Entrepisos</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelEntrepisos"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Techos</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelTechos"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Azoteas</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelAzoteas"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Bardas</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelBardas"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <hr style= "margin-top: 8px !important; margin-bottom: 8px !important; border: 0;border-top: 1px solid #eee;"  />
                                    <strong>b) Revestimientos y acabados interiores</strong>
                                    <hr style= "margin-top: 8px !important; margin-bottom: 8px !important; border: 0;border-top: 1px solid #eee;"  />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Aplanados interiores</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelAplanadosInteriores"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Aplanados exteriores</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelAplanadosExteriores"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Plafones</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelPlafones"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Lambrines</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelLambrines"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Pisos</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelPisos"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Zoclos</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelZoclos"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Escaleras</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelEscaleras"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Pintura</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelPintura"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Recubrimientos especiales</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelRecubrimientosEspeciales"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <hr style= "margin-top: 8px !important; margin-bottom: 8px !important; border: 0;border-top: 1px solid #eee;"  />
                                    <strong>c) Carpinteria</strong>
                                    <hr style= "margin-top: 8px !important; margin-bottom: 8px !important; border: 0;border-top: 1px solid #eee;"  />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Puertas</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelPuertas"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Closets</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelClosets"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <hr style= "margin-top: 8px !important; margin-bottom: 8px !important; border: 0;border-top: 1px solid #eee;"  />
                                    <strong>d) Instalaciones hidráulicas y sanitarias</strong>
                                    <hr style= "margin-top: 8px !important; margin-bottom: 8px !important; border: 0;border-top: 1px solid #eee;"  />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Muebles de baño</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelMueblesBaño"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Equipo de cocina</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelEquipoCocina"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>e) Instalaciones eléctricas</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelInstacionesElectricas"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>f) Herrería</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelHerreria"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>g) Vidriería</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelVidrieria"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>g) Cerrajería</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelCerrajeria"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>g) Fachadas</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelFachadas"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>g) Instalaciones Especiales, elementros de construcción y obras complementarias</strong>
                                </div>
                                <div class="col-9">
                                    <span id="labelInstalacionesEspecialesEC"></span>
                                </div>
                            </div>
                        </div>
                        </div>
                        <p style="page-break-before: always"></p>
                        <div class="row">
                            <div class="col-12">
                                <h4 class="text-center" style="color: #480912">
                                    <strong>VI.- APLICACIÓN DEL ENFOQUE DE COSTOS (AVALÚO FÍSICO O DIRECTO)</strong>
                                </h4> 
                                
                            </div>
                        </div>

                        <div class="card border mt-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    
                                <table id="tablaFisicoTerreno" class="table table-condensed table-hover table-bordered text-dark">
                                                    <thead >
                                                        <tr>
                                                            <th colspan="5">a) TERRENO</th>
                                                        </tr>
                                                        <tr>
                                                            <th>Superficie Mt2</th>
                                                            <th>Valor unitario Mt2</th>
                                                            <th>Factor</th>
                                                            <th>Motivo</th>
                                                            <th>Valor terreno</th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>

                                                            <th class="font-medium" style="text-align:left;">Superficie Total: <span id="txtsuptotalterreno"></span></th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th colspan="2" class="font-medium text-right">Valor Total: <span id="txtvalortotalterreno">$0.0</span></th>
                                                            
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                </div>
                            </div>
                
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="tablaFisicoConstrucciones" class="table table-condensed table-hover table-bordered  text-dark">
                                    <thead >
                                                        <tr>
                                                            <th colspan="9">b) CONSTRUCCIONES
                                                                
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th>Cat</th>
                                                            <th>Superficie M2</th>
                                                            <th>Valor Unitario</th>
                                                            <th>Estado</th>
                                                            <th>Edad</th>
                                                            <th>F.Dem.</th>
                                                            <th>F.Com.</th>
                                                            <th>Valor Unitario</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th colspan="2">Superficie Total: <span id="txtsuptotalconst"></span></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th colspan="4" class="text-right">Valor Total: <span id="txtvalortotalconst">$0.0</span></th>
                                                            
                                                        </tr>
                                                    </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="tablaEspeciales" class="table table-condensed table-hover table-bordered  text-dark">
                                    <thead>
                                                        <tr>
                                                            <th colspan="8">c) INSTALACIONES ESPECIALES, ELEMENTOS ACCESORIOS Y OBRAS COMPLEMENTARIAS
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th>Concepto</th>
                                                            <th>Unidad</th>
                                                            <th>Cantidad</th>
                                                            <th>V.N.R. Unit.</th>
                                                            <th>% Demerito</th>
                                                            <th>V.R.N. Unitario</th>
                                                            <th>Valor Parcial</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th colspan="4"></th>
                                                            <th colspan="3" class="font-medium text-right">Valor Total: <span id="txtvalortotalespeciales">$0.0</span></th>
                                                            
                                                        </tr>
                                                    </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                </div>
                                <div class="col-lg-4">
                                    <!-- TODO: -->
                                    <strong>Gran Total:</strong><label id="labelValorFisico"></label>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="row hidden">
                            <div class="col-12">
                                <h4 class="text-center" style="color: #480912">
                                    <strong>VII.- RESUMEN DE VALORES OBTENIDOS</strong>
                                </h4> 
                            </div>
                        </div>

                        <div class="card hidden border mt-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Valor físico o directo</strong>
                                </div>
                                <div class="col-9">
                                    <!-- TODO: -->
                                    <span id="labelValorFisico_2"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3 text-right">
                                    <strong>Valor de mercado</strong>
                                </div>
                                <div class="col-9">
                                    <!-- TODO: -->
                                    <span id="labelValorMercado"></span>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="col-12">
                                    <h4 class="text-center" style="color: #480912">
                                        <strong>VII.- CONSIDERACIONES PREVIAS A LA CONCLUSIÓN</strong>
                                    </h4> 
                                </div>
                                
                            </div>
                        </div>

                        <div class="panel panel-default border mt-3">
                            <div class="row">
                                <div class="col-12">
                                    <p id="labelConsideraciones"></p>
                                </div>
                            </div>
                        </div>

                        <p style="page-break-before: always" />

                        <div class="row">
                            <div class="col-12">
                                <h4 class="text-center" style="color: #480912">
                                        <strong>REPORTE FOTOGRÁFICO</strong>
                                </h4> 
                            </div>
                        </div>
                        
                        <div id="divRepfoto" class="row mt-3">
                        </div>

                        <p style="page-break-before: always" />

                        <div class="row">
                            <div class="col-12">
                                <h4 class="text-center" style="color: #480912">
                                        <strong>VIII.- CONCLUSIÓN</strong>
                                </h4> 
                                
                            </div>
                        </div>

                        <div class="card border mt-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <p class="text-center">
                                        <strong>EL VALOR AUTORIZADO DEL INMUEBLE MOTIVO DEL PRESENTE AVALÚO ASCIENDE A:&nbsp;<span id="labelValorConclusion" style="font-size:14px;"></span>
            <br />
            <span>ESTE AVALÚO SOLO SERÁ VÁLIDO SI PRESENTA CÓDIGO QR, TIENE UNA VIGENCIA DE 6 (SEIS) MESES Y SU OBJETO ES UNICAMENTE PARA TRASLADO DE DOMINIO. </span>
            </strong>
                                    </p>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="row">
                            <div class="col-2">
                            </div>
                            <div class="col-4" style="font-size:11px">
                                <strong>Perito</strong>
                                <br />
                                <strong>Serie del certificado</strong>
                                <br />
                                <span id="labelSerie"></span
                                <br />
                                <strong>Sello Digital</strong>
                                <br />
                                <textarea name="labelSello1" id="labelSello1" style="border: 0; width: 100%; height: 110px; overflow: hidden; resize: none;"></textarea>
                            </div>
                            <div class="col-6">
                                <p class="text-center">
                                    <span id="labelNombreC"></span>
                                </p>
                                <p class="text-center">
                                    <label>Clave:</label>
                                    <span id="labelCedula"></span>
                                </p>
                                <p class="text-center">
                                    <label>Especialidad:</label>
                                    <span id="labelEspecialidadP"></span>
                                </p>
                                <p class="text-center">
                                    <label>Fecha Envío:</label>
                                    <!-- TODO -->
                                    <span id="labelFechaEnvio"></span>
                                </p>
                            </div>
                        </div>

                        <div id="divSelloAux" class="row" style="display: none">
                            <div class="col-2">
                            </div>
                            <div class="col-4" style="font-size: 11px">
                                <div>
                                    <strong><span id="labeleAUX">text</span></strong>
                                    <br />
                                    <strong>Serie del certificado</strong>
                                    <br />
                                    <span id="labelSerieAux">text</span>
                                    <br />
                                    <strong>Sello Digital</strong>
                                    <br />
                                    <textarea name="labelSelloAux" id="labelSelloAux" style="border: 0; width: 100%; height: 110px; overflow: hidden; resize: none;"></textarea>
                                </div>
                            </div>
                            <div class="col-6">
                                <p class="text-center">
                                    <span id="labelNombreAux"></span>
                                </p>
                                <p class="text-center">
                                    <label>Usuario:</label>
                                    <span id="labelAuxRFC"></span>
                                </p>
                                <p class="text-center">
                                    <label>Fecha Sello:</label>
                                    <span id="labelFechaSelloAux"></span>
                                </p>
                            </div>
                        </div>
                        <div id="divSelloJefeVal" class="row" style="display: none">
                            <div class="col-2">
                            </div>
                            <div class="col-4" style="font-size: 11px">
                                <div>
                                    <strong>Delegado</strong>
                                    <br />
                                    <strong>Serie del certificado</strong>
                                    <br />
                                    <span id="labelSerieJefeVal">text</span>
                                    <br />
                                    <strong>Sello Digital</strong>
                                    <br />
                                    <textarea name="labelSelloJefeVal" id="labelSelloJefeVal" style="border: 0; width: 100%; height: 110px; overflow: hidden; resize: none;"></textarea>
                                </div>
                            </div>
                            <div class="col-6">
                                <p class="text-center">
                                    <span id="labelNombreJef"></span>
                                </p>
                                <p class="text-center">
                                    <label>Usuario:</label>
                                    <span id="labelRFCJef"></span>
                                </p>
                                <p class="text-center">
                                    <label>Fecha Sello:</label>
                                    <span id="labelFechaSelloJef"></span>
                                </p>
                            </div>
                        </div>
                        <div id="divSelloDirector" class="row" style="display: none">
                            <div class="col-2">
                            </div>
                            <div class="col-4" style="font-size: 11px">
                                <div>
                                    <strong>Director de Enlace</strong>
                                    <br />
                                    <strong>Serie del certificado</strong>
                                    <br />
                                    <span id="labelSerieDirector">text</span>
                                    <br />
                                    <strong>Sello Digital</strong>
                                    <br />
                                    <textarea name="labelSelloDirector" id="labelSelloDirector" style="border: 0; width: 100%; height: 110px; overflow: hidden; resize: none;"></textarea>
                                </div>
                            </div>
                            <div class="col-6">
                                <p class="text-center">
                                    <span id="labelNombreDir"></span>
                                </p>
                                <p class="text-center">
                                    <label>Usuario:</label>
                                    <span id="labelRFCDir"></span>
                                </p>
                                <p class="text-center">
                                    <label>Fecha Sello:</label>
                                    <span id="labelSelloDir"></span>
                                </p>
                            </div>
                        </div>
                        <div id="divRepfotoAnexo" class="row">
                        </div>

                        <span id="labelError" style="display:none"></span>
                        <span id="labelAvaluo" style="display:none"></span>
                        <span id="labelMpio" style="display:none"></span>

                    <!-- termina el contenido -->

                    </div>
            </div>
        </div>
    </div>
</div>