<script src="<?php echo base_url("assets/js/sweetalert2@10.js"); ?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"); ?>"></script>
<!-- Buttons examples -->

<!-- Datatable init js -->
<script src="<?php echo base_url("assets/js/jquery.blockUI.js"); ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.validate.js"); ?>"></script>
<script src="<?php echo base_url("/assets/js/messages_es.js"); ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js"); ?>"></script>
<!-- form mask -->
<script src="<?php echo base_url("assets/libs/inputmask/min/jquery.inputmask.bundle.min.js"); ?>"></script>

<!-- form mask init -->
<script src="<?php echo base_url("assets/js/pages/form-mask.init.js"); ?>"></script>

<script src="<?php echo base_url("assets/js/jquery-ui-1.12.1.js"); ?>"></script>

<script src="<?php echo base_url("assets/js/peritos/utils.js")."?v".rand(); ?>" ></script>


<script>
  //variables glonales
  var editindx = 0;
  var editrowindx = 0;
  var valor = 0.00;
  var editando = 0;
  var cat_const_list;

  var valTotT_ws = 0.0;
  
  $.getJSON("<?php echo base_url("peritos/consulta_cat_const"); ?>", function(json){ cat_const_list=json.data; });

  var estatusEdoConservCalidadProy = ['BUENO', 'REGULAR', 'MALO'];
  var orientaciones = ['Norte', 'Sur', 'Este', 'Oeste', 'Noroeste', 'Suroeste', 'Noreste', 'Sureste', 'Oriente', 'Poniente', 'Arriba', 'Abajo'];
  var avaluo_id = 0;
  $(document).ready(function() {


    if ($("#liPortada > a").hasClass("active")) {
       //todo: validar
        //guardarPortada();  
    }

    $("#imprimir_ok").hide();
    $('#av_fisico_ok').hide();
    
    var info = $('#txtInfCat').val();

    $("#txtEstadoConservacion").focus(function() { $("#txtEstadoConservacion").keydown(); });
    $("#txtEstadoConservacion").autocomplete({ source: estatusEdoConservCalidadProy, minLength: 0 });
    $("#txtCalidadProyecto").focus(function() { $("#txtCalidadProyecto").keydown(); });
    $("#txtCalidadProyecto").autocomplete({ source: estatusEdoConservCalidadProy, minLength: 0 });
    $("#txtSupEscrituras").change(function() { if ($("#txtSupEscrituras").val() != $("#txtSupConst").val()) { $("#txtSupEscrituras")[0].style.backgroundColor = "#e6b8b8"; } else { $("#txtSupEscrituras")[0].style.backgroundColor = "white" } });

    //cargar la informacion de ese folio
    cargar_info(info);
    cargar_portada(info);
    cargar_antecedentes(info);
    cargar_car_urb(info);
    cargar_car_terreno(info);
    cargar_descripcion_general(info);
    cargar_elementos_construccion(info);
    cargar_sello(info);

    $("#selectOrientacion").focus(function() { $("#selectOrientacion").keydown(); });
    $("#selectOrientacion").autocomplete({ source: orientaciones, minLength: 0 });
    cargar_combo("clasificacion_zonas", "get_clasificacion_zona");
    cargar_combo("construccion_dominante", "get_construccion_dominante");
    cargar_combo("saturacion_zona", "get_saturacion_zona");
    cargar_combo("list_poblacion", "get_poblacion");
    cargar_combo("suelo_permitido", "get_suelo_permitido");

    cargar_combo_usos();
    cargar_combo("list_especiales", "get_list_especiales");
    cargar_combo("list_fachadas", "get_list_fachadas");
    cargar_combo("list_cerrajeria", "get_list_cerrajeria");
    cargar_combo("list_vidrieria", "get_list_vidrieria");
    cargar_combo("list_herreria", "get_list_herreria");
    cargar_combo("list_instalacionese", "get_list_instalacionese");
    cargar_combo("list_mueblesb", "get_list_mueblesb");
    cargar_combo("list_puertas", "get_list_puertas");
    cargar_combo("list_pintura", "get_list_pintura");
    cargar_combo("list_pisos", "get_list_pisos");
    cargar_combo("list_aplanados", "get_list_aplanados");
    cargar_combo("list_techos", "get_list_techos");
    cargar_combo("list_muros", "get_list_muros");
    cargar_combo("list_lambrines", "get_list_lambrines");
    cargar_combo("list_cimientos", "get_list_cimientos");

  });

  var formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });

  //funciones

  function cargar_construcciones_fisico(data){
    var suptotalconst = 0.0;
    var valortotalconst = 0.0;
    $.each(data, function(i, obj) {
        let sup = parseFloat(obj.superficie);
        let valneto = parseFloat(obj.valor_construccion.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        $('#tablaFisicoConstrucciones > tbody').append('<tr><td>'+obj.categoria+'</td><td>'+sup+'</td><td>'+obj.valor_unitario+'</td><td>'+obj.estado+'</td><td>'+obj.edad+'</td><td>'+obj.fact_dem+'</td><td>'+obj.fact_com+'</td><td>'+obj.valor_construccion+'</td><td><button type="button" class="deleterow btn btn-outline-danger btn-sm editaTerr"><i class="mdi mdi-delete"></i></button></td></tr>');
        suptotalconst += sup;
        valortotalconst += valneto;
      });
      $('#txtsuptotalconst').text(suptotalconst.toFixed(2));
      $('#txtvalortotalconst').text(aPesos(valortotalconst));

      $("#tablaFisicoConstrucciones")[0].addEventListener("dblclick", function(e) { if (e.target && e.target.nodeName == "TD" && e.target.cellIndex < 7) { activaEditarCONST(e.target.cellIndex, e.target); } });
  }

  function cargar_especiales_fisico(data){
    var valortotal = 0.0;
    $.each(data, function(i, obj) {
        let valparcial = parseFloat(obj.valor_parcial.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        $('#tablaEspeciales > tbody').append('<tr><td>'+obj.concepto+'</td><td>'+obj.unidad+'</td><td>'+obj.cantidad+'</td><td>'+obj.valor_unit+'</td><td>'+obj.fact_dem+'</td><td>'+obj.vrn_unitario+'</td><td>'+obj.valor_parcial+'</td><td><button type="button" class="deleterow btn btn-outline-danger btn-sm editaTerr"><i class="mdi mdi-delete"></i></button></td></tr>');
        valortotal += valparcial;
      });
      $('#txtvalortotalespeciales').text(aPesos(valortotal));

      $("#tablaEspeciales")[0].addEventListener("dblclick", function(e) { if (e.target && e.target.nodeName == "TD" && e.target.cellIndex < 5) { activaEditarEspeciales(e.target.cellIndex, e.target); } });
  }

  function cargar_construcciones(cvecat){
    var suptotalconst = 0.0;
    var valortotalconst = 0.0;
    $.ajax({
      url: "<?php echo base_url("peritos/consulta_construcciones"); ?>/"+ cvecat,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        for(var i in response.data){
          var obj = response.data[i];
          let sup = parseFloat(obj.SUP_CONST);
          let val = parseFloat(obj.valorunitario);
          let valneto = parseFloat(obj.valorneto);
          $('#tablaFisicoConstrucciones > tbody').append('<tr><td>'+obj.CVE_CAT_CO+'</td><td>'+sup+'</td><td>'+aPesos(val)+'</td><td>'+obj.CVE_EDO_CO+'</td><td>'+obj.EDD_CONST+'</td><td>'+obj.factordemerito+'</td><td>'+obj.factorcomercializacion+'</td><td>'+aPesos(valneto)+'</td><td><button type="button" class="deleterow btn btn-outline-danger btn-sm editaTerr"><i class="mdi mdi-delete"></i></button></td></tr>');
          suptotalconst += sup;
          valortotalconst += valneto;
        }

        $('#txtsuptotalconst').text(suptotalconst.toFixed(2));
        $('#txtvalortotalconst').text(aPesos(valortotalconst));
        calculaTotal();
        
      }
    });
    $("#tablaFisicoConstrucciones")[0].addEventListener("dblclick", function(e) { if (e.target && e.target.nodeName == "TD" && e.target.cellIndex < 7) { activaEditarCONST(e.target.cellIndex, e.target); } });
    
  }

  function cargar_avaluo_fisico(){
    //TODO:
    $('#av_fisico_ok').show();
  }

  function cargar_terreno_fisico(data){
    console.log(data);
    suptotalterreno = 0.0;
    valortotalterreno = 0.0;
    $.each(data, function(i, obj) {
        suptotalterreno += parseFloat(obj.superficie);
        valortotalterreno += parseFloat(obj.valor_terreno.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        $('#tablaFisicoTerreno > tbody').append('<tr><td>'+obj.superficie+'</td><td>'+obj.valor_unitario+'</td><td>'+obj.factor_demerito+'</td><td>'+obj.motivo+'</td><td>'+obj.valor_terreno+'</td></tr>');
      });
      
    $('#txtsuptotalterreno').text(suptotalterreno.toFixed(2));
    $('#txtvalortotalterreno').text(aPesos(valortotalterreno));
    for (var i = 1; i < $("#tablaFisicoTerreno")[0].rows.length; i++) {}
         $("#tablaFisicoTerreno")[0].addEventListener("dblclick", function(e) { if (e.target && e.target.nodeName == "TD" && e.target.cellIndex < 4) { activaEditar(e.target.cellIndex, e.target); } });
  }

  

  function cargar_terreno(cvecat){
    var suptotalterreno = 0.0;
    var valortotalterreno = 0.0;
    $.ajax({
      url: "<?php echo base_url("peritos/consulta_terreno"); ?>/"+ cvecat,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        for(var i in response.data){
          var obj = response.data[i];
          let sup = parseFloat(obj.SUP_TERR);
          let val = parseFloat(obj.valorunitario1);
          let valneto = parseFloat(obj.valorneto);
          $('#tablaFisicoTerreno > tbody').append('<tr><td>'+sup+'</td><td>'+aPesos(val)+'</td><td>'+obj.FAC_DEM_TE+'</td><td></td><td>'+aPesos(valneto)+'</td></tr>');
          suptotalterreno += sup;
          valortotalterreno += valneto;
        }
        $('#txtsuptotalterreno').text(suptotalterreno.toFixed(2));
        $('#txtvalortotalterreno').text(aPesos(valortotalterreno));
        calculaTotal();
      }
    });
    for (var i = 1; i < $("#tablaFisicoTerreno")[0].rows.length; i++) {
      $("#tablaFisicoTerreno")[0].addEventListener("dblclick", function(e) { if (e.target && e.target.nodeName == "TD" && e.target.cellIndex < 4) { activaEditar(e.target.cellIndex, e.target); } });
    }
              
    
  }


  function cargar_portada(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_portada"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.portada.length > 0) {
          $("#portada_ok").show();
          $("#divPortada-tab").addClass('border border-success');
          var json = response.portada[0];
          $("#txtInmueble").val(json.inmueble);
          $("#txtCP").val(json.cp);
          $("#txtCalle").val(json.calle);
          $("#txtNumero").val(json.numero);
          $("#txtColonia").val(json.colonia);
          $("#txtMunicipio").val(json.municipio);
          $("#txtNotaInterna").val(json.nota_interna);
          //$("#txtCalleCVE").val();
          //$("#txtColoniaCVE").val();
        }else {
          $("#portada_ok").hide();
        }
      }
    });
  }

  function cargar_antecedentes(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_antecedentes"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.antecedentes.length > 0) {
          $("#antecedentes_ok").show();
          $("#divAntecedentes-tab").addClass('border border-success');
          var json = response.antecedentes[0];
          $("#txtSolicitante").val(json.solicitante);
          $("#txtFechaInspeccion").val(moment(json.fecha_inspeccion).format('YYYY-MM-DD'));
          $("#ContentPlaceHolder1_txtRegimen").val(json.regimen);
          $("#selObjectoAvaluo").val(json.objeto);
          $("#txtRegPubPro").val(json.rpp);
          $("#txtInscripcion").val(json.inscripcion);
          $("#txtLibro").val(json.libro);
          $("#txtSeccion").val(json.seccion);
        }else {
          $("#antecedentes_ok").hide();
        }
      }
    });
  }

  function cargar_car_urb(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_car_urb"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.caracteristicas_urbanas.length > 0) {
          $("#car_urb_ok").show();
          $("#divCarUrb-tab").addClass('border border-success');
          var json = response.caracteristicas_urbanas[0];

          $("#clasificacion_zonas").val(json.clasificacionzona);
          $("#construccion_dominante").val(json.tipoconstdominante);
          $("#saturacion_zona").val(json.indicesatzona);
          $("#list_poblacion").val(json.poblacion);
          $("#txtContaminacionAmbiental").val(json.contaminacionambiental);
          $("#suelo_permitido").val(json.usosuelopermitido);
          $("#txtViaAccesoImportancia").val(json.viaaccesoimportancia);

          $("#chkAgua")[0].checked = stringToBoolean(json.agua);
          $("#chkDrenaje")[0].checked = stringToBoolean(json.drenaje);
          $("#chkElectricidad")[0].checked = stringToBoolean(json.energia);
          $("#chkAlumbrado")[0].checked = stringToBoolean(json.alumbrado);
          $("#chkBasura")[0].checked = stringToBoolean(json.basura);
          $("#chkTransporte")[0].checked = stringToBoolean(json.transporte);
          $("#chkTelefono")[0].checked = stringToBoolean(json.telefono);
          $("#chkBanqueta")[0].checked = stringToBoolean(json.banqueta);
          $("#chkGuarniciones")[0].checked = stringToBoolean(json.guarniciones);
          $("#chkPavimento")[0].checked = stringToBoolean(json.pavimento);
          $("#chkVigilancia")[0].checked = stringToBoolean(json.vigilancia);

        }else {
          $("#car_urb_ok").hide();
        }
      }
    });
  }

  function cargar_car_terreno(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_car_terreno"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.caracteristicas_terreno.length > 0) {
          $("#car_terr_ok").show();
          $("#divCarTerr-tab").addClass('border border-success');
          var json = response.caracteristicas_terreno[0];

          $("#txtTopografiaConst").val(json.topografia_const);
          $("#txtNumeroFrentes").val(json.numero_frentes);
          $("#txtCaracteristicasPan").val(json.car_panoramicas);
          $("#txtDensidadHabitacional").val(json.densidad_habitacional);
          $("#txtIntensidadConstruccion").val(json.intensidad_const);
          $("#txtServidumbresConstrucciones").val(json.servidumbre_restricciones);
          $("#txtSupEscrituras").val(json.sup_escrituras);
          $("#txtSupLevantamiento").val(json.sup_topografico);

        }else {
          $("#car_terr_ok").hide();
        }
      }
    });
  }

  function cargar_sello(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_autorizacion_perito"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log("SELLO" + response);
        if (response.autorizacion.length > 0) {
          var json = response.autorizacion[0];
          $("#txtRFCPer").val(json.rfc_certificado);
          $("#txtSerie").val(json.serie_certificado);
          $("#txtSello").val(json.sello_digital);
          if(json.sello_digital != null)
            $("#diva-tab").addClass('border border-success');
        }else {
        
        }
      }
    });
  }

  function cargar_valor_mercado(info){
    //TODO
  }

  function cargar_descripcion_general(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_descripcion_general"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.descripcion_general.length > 0) {
          $("#desc_gen_ok").show();
          $("#divDescGen-tab").addClass('border border-success');
          var json = response.descripcion_general[0];

          $("#selUso").val(json.cve_uso);
          $("#txtEdadAprox").val(json.edad_aprox);
          $("#txtNumeroNiveles").val(json.num_niveles);
          $("#txtEstadoConservacion").val(json.estado_conservacion);
          $("#txtCalidadProyecto").val(json.calidad_proyecto);
          $("#txtUnidadesRentables").val(json.unidades_rentables);
          $("#txtDescGeneralInmueble").val(json.descripcion_general);
        }else {
          $("#desc_gen_ok").hide();
        }
      }
    });
  }

  function cargar_elementos_construccion(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_elementos_construccion"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log(response);
        if (response.elementos_construccion.length > 0) {
          $("#elem_const_ok").show();
          $("#divElemConst-tab").addClass('border border-success');
          var json = response.elementos_construccion[0];

          $("#list_cimientos").val(json.a_cimientos);
          $("#txtEstructura").val(json.a_estructura);
          $("#list_muros").val(json.a_muros);
          $("#txtEntrepisos").val(json.a_entrepisos);
          $("#list_techos").val(json.a_techos);
          $("#txtAzoteas").val(json.a_azoteas);
          $("#txtBardas").val(json.a_bardas); 
          $("#list_aplanados").val(json.b_aplanadosinteriores);
          $("#txtAplanadosExteriores").val(json.b_aplanadosexteriores);
          $("#txtPlafones").val(json.b_plafones);
          $("#list_lambrines").val(json.b_lambrines);
          $("#list_pisos").val(json.b_pisos);
          $("#txtZoclos").val(json.b_zoclos);
          $("#txtEscaleras").val(json.b_escaleras);
          $("#list_pintura").val(json.b_pintura);
          $("#txtRecubrimientosEspeciales").val(json.b_recubrimientosespeciales);
          $("#list_puertas").val(json.c_puertas);
          $("#txtClosets").val(json.c_closets);
          $("#list_mueblesb").val(json.d_mueblesbano);
          $("#txtEquipoCocina").val(json.d_equipodecocina);
          $("#list_instalacionese").val(json.e_instalacioneselectricas);
          $("#list_herreria").val(json.f_herreria);
          $("#list_vidrieria").val(json.g_vidrieria);
          $("#list_cerrajeria").val(json.h_cerrajeria);
          $("#list_fachadas").val(json.i_fachadas);
          $("#list_especiales").val(json.j_instalacionesespeciales);

        }else {
          $("#elem_const_ok").hide();
        }
      }
    });
  }
  

  function cargar_info(info) {
    var cve_cat = "";
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_info"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      beforeSend : function (){
        $.blockUI({ 
          fadeIn : 0,
          fadeOut : 0,
          showOverlay : false,
          message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Cargando Información...</h3><br><h5>Espere un momento por favor</h5>', 
          css: {
              backgroundColor: 'white',
              border: '1', 
          }, 
        });
      },
      success: function(response) {
        console.log(response);
        if (response.info.length > 0) {
          avaluo_id = response.info[0].id;
          //Clave catastral compuesta
          $('#MUNI').val(zeroPad(response.info[0].cve_mpio, 3));
          $('#POBL').val(zeroPad(response.info[0].cve_pobl, 3));
          $('#CTEL').val(zeroPad(response.info[0].num_ctel, 3));
          $('#MANZ').val(zeroPad(response.info[0].num_manz, 3));
          $('#PRED').val(zeroPad(response.info[0].num_pred, 3));
          $('#UNID').val(zeroPad(response.info[0].num_unid, 3));
          //fechas
          if (response.info[0].ultima_modificacion != null)
            $("#txtFecha").val(moment(response.info[0].ultima_modificacion).locale('es').format('L LTS'));
          if (response.info[0].fecha_autorizacion != null)
            $("#txtFechaModificacion").val(moment(response.info[0].fecha_autorizacion).locale('es').format('L LTS'));

          var pobl = $('#POBL').val();
          var ctel = $('#CTEL').val();
          var manz = $('#MANZ').val();
          var pred = $('#PRED').val();
          var unid = $('#UNID').val();

          var cvecat = pobl + ctel + manz + pred + unid;
          //cargamos informacion de la clave catastral 
          $.ajax({
            url: "<?php echo base_url("peritos/validarcve"); ?>/" + cvecat,
            dataType: "json",
            success: function(data) {
              console.log(data.result);
              if (data.result.length > 0) {
                if (data.result[0].ID_REGTO == "B") {
                  Swal.fire({
                    icon: 'error',
                    title: 'Clave dada de Baja...'
                  })
                  //$("#btn_limpiar").click();
                } else {
                  //llenamos los datos correspondientes 
                  var prop = "";
                  if(data.result[0].NOM_PROP != null) { prop = prop + data.result[0].NOM_PROP; }
                  if(data.result[0].APE_PAT != null) { prop = prop + " " +data.result[0].APE_PAT; }
                  if(data.result[0].APE_MAT != null) { prop = prop + " " +data.result[0].APE_MAT; }
                  $("#txtPropietario").val(prop);

                  if (data.result[0].TIP_PERS === 'F')
                    $("#txtTipoPersona").val("Física");
                  else if(data.result[0].TIP_PERS === 'F')
                    $("#txtTipoPersona").val("Moral");
                  else
                    $("#txtTipoPersona").val("");

                  $("#txtCalleCVE").val(data.result[0].CVE_CALLE);
                  $("#txtCalle").val(data.result[0].CALLE);
                  $("#txtColoniaCVE").val(data.result[0].CVE_COL);
                  $("#txtColonia").val(data.result[0].COLONIA);
                  $("#txtSupConst").val(data.result[0].SUP_TERR);

                  valTotT_ws = parseFloat(data.result[0].VAL_TERR);

                  cargar_tabla_orientaciones();

                  if (response.Terr.length > 0) {
                    cargar_terreno_fisico(response.Terr);
                    $("#divAvaluoFisico-tab").addClass('border border-success');
                    $('#av_fisico_ok').show();
                  } else { cargar_terreno(cvecat); }
                  
                  if (response.Const.length > 0) {
                    cargar_construcciones_fisico(response.Const);
                    $('#av_fisico_ok').show();
                  } else { cargar_construcciones(cvecat); }

                  if (response.Esp.length > 0) {
                    cargar_especiales_fisico(response.Esp);
                    $('#av_fisico_ok').show();
                  }
                  else{
                    $("#tablaEspeciales")[0].addEventListener("dblclick", function(e) { if (e.target && e.target.nodeName == "TD" && e.target.cellIndex < 5) { activaEditarEspeciales(e.target.cellIndex, e.target); } });
                  }
                  //cargar_terreno(cvecat);
                  //cargar_construcciones(cvecat);

                  cargar_categorias_construccion(cvecat);

                  if (response.detalle.length > 0) {
                    if(response.detalle[0].valor_mercado != null)
                    { 
                      $("#txtValorMercado").val(aPesos(parseFloat(response.detalle[0].valor_mercado))); 
                      $("#txtValorFisico").val(aPesos(parseFloat(response.detalle[0].valor_mercado))); 
                    }
                    if(response.detalle[0].consideraciones_previas != null)
                    { 
                      $("#txtConsideraciones").val(response.detalle[0].consideraciones_previas); 
                      $("#divConclusion-tab").addClass('border border-success');
                      $("#conclusion_ok").show();
                      
                      $('#imprimir_ok').show();
                    }
                    else{
                      $("#conclusion_ok").hide();
                    }
                  }
 
                  cargarImagen();
                  calculaTotal();
                  //Se obtiene el array del poligono 
                  jsonobj = jQuery.parseJSON(data.result[0].jsonb_build_object);
                  //si encuentra el poligono en el vectorial
                  if (jsonobj.features != null) {
                    //Se visualiza el poligono en el mapa
                    visualizar_capa(jsonobj);
                  } else {
                    Swal.fire({
                      icon: 'error',
                      title: 'Vectorial no encontrado',
                      text: 'La clave catastral no se encontró en el vectorial!'

                    })
                  }
                  
                }

              } else {

                //verificar si se encuentra de primer registro
                Swal.fire({
                  title: 'Clave Catastral no encontrada',
                  icon: 'error',
                  showCancelButton: true,
                  cancelButtonText: 'Notificar',
                  confirmButtonColor: '#00b300',
                  cancelButtonColor: '#0000FF',
                  confirmButtonText: 'Primer Registro'
                }).then((result) => {
                  if (result.value) {

                  } else {

                  }
                })


              }
            }
          });
        }
      },
      complete : function (){
        $.unblockUI();
      }
    });

  }

  function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
  }

  function cargar_combo(control, url) {
      $("#" + control).focus(function() { $("#" + control).keydown(); });
      $("#" + control).autocomplete({ source: function(request, response) { $.ajax({ type: "POST", contentType: "application/json; charset=utf-8", url: "<?php echo base_url("peritos/"); ?>/" + url, dataType: "json", success: function(data) { response($.map( data.result, function( item ) {
                return {
                    label: item.desc,
                    value: item.desc
                }
            }));
       }, error: function(result) {} }); }, minLength: 0 });
  }

  function cargar_combo_usos() {
    $('#selUso').empty();
    $.getJSON("<?php echo base_url("peritos/grupo_usos"); ?>", function(json) {
      $.each(json.results, function(i, obj) {
        $('#selUso').append($('<option>').text(obj.DES_GPO_US).attr({
          'value': obj.CVE_GPO_US
        }));
      });
    });
  }

  function cargar_categorias_construccion(cvecat) { //mostramos la tabla con las categorias de construccion
    var urlconst = "<?php echo base_url("peritos/consulta_construcciones/"); ?>/" + cvecat;
    var html = "";
    $.getJSON(urlconst, function(json) {
      $('#tablaConstrucciones').empty();
      html += '<thead class="text-white" style="background-color: #480912;"><th>Categoria</th><th>Superficie M2</th><th>Valor Unitario</th></thead>';
      $.each(json.data, function(i, obj) {
        html += '<tr><td>' + obj.CVE_CAT_CO + '</td><td>' + obj.SUP_CONST + '</td><td>' + formatter.format(obj.valorunitario) + '</td></tr>';
      });

      $('#tablaConstrucciones').append(html);
    });
  }

  function cargar_tabla_orientaciones() {
    var url = "<?php echo base_url("peritos/obtener_orientaciones/"); ?>/<?php echo $info; ?>";
    var html = "";
    $.getJSON(url, function(json) {
      $('#tablaOrientacion').empty();
      html += '<thead class="text-white" style="background-color: #480912;"><th>Orientación</th><th>Descripción</th><th></th></thead>';
      $.each(json.orientaciones, function(i, obj) {
        html += '<tr><td>' + obj.orientacion + '</td><td>' + obj.descripcion + '</td><td class="text-right"><button id="' + obj.id + '" class="eliminar_orientacion btn btn-danger btn-sm"><i class="mdi mdi-window-close""></i></button></td></tr>';
      });

      $('#tablaOrientacion').append(html);
    });
  }

  $(document).on('click', '#btnRedondear', function() {
    //redondea el valor fisico del avalo
    var valorfisico = parseFloat($("#txtValorFisico").val().replace("$", "").replace(",", "").replace(",", ""));
    $("#txtValorFisico").val(Math.round(valorfisico));
  });

  $(document).on('click', '.eliminar_orientacion', function() {
    var id = $(this).attr('id');

    Swal.fire({
      text: '¿Seguro de eliminar la orientacion?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: "<?php echo base_url("peritos/eliminar_avaluo_orientacion"); ?>/" + id,
          success: function(data) {
            //mandamos mensaje de 
            if (data === 'ok') {
              cargar_tabla_orientaciones();
            } else {
              Swal.fire({
                icon: 'error',
                title: 'Mensaje del Sistema',
                text: 'Ocurrio un error al intentar eliminar la orientacion'

              })
            }
          }
        })
      }
    })

  });


  function guardarPortada(){
    var info = $('#txtInfCat').val();
    //obtenemos el id del avaluo para guardarlo con los datos de la portada
    var json = new Object();
    json.info = info;
    json.inmueble = $("#txtInmueble").val();
    json.cp = $("#txtCP").val();
    json.calle = $("#txtCalle").val();
    json.numero = $("#txtNumero").val();
    json.colonia = $("#txtColonia").val();
    json.municipio = $("#txtMunicipio").val();
    json.notaInterna = $("#txtNotaInterna").val();
    json.avaluo = avaluo_id;
    json.cve_calle = $("#txtCalleCVE").val();
    json.cve_col = $("#txtColoniaCVE").val();
   
    $.ajax({
        url: "<?php echo base_url("peritos/registrar_portada"); ?>",
        type: "POST",
        data: json,
        success: function(data) {
          console.log(data);
          if (data === 'ok') {
            Swal.fire({
              text: 'Portada Guardada Correctamente',
              icon: "success",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
            cargar_portada(info);
            $("#liAntecedentes a:first-child").click();

          } else {
            Swal.fire({
              text: data,
              icon: "warning",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
          }

        }
      });
    
  }

  function guardarAntecedentes() {
    var info = $('#txtInfCat').val();
    //obtenemos el id del avaluo para guardarlo con los datos de la portada
    var json = new Object();
    json.info = info;
    json.avaluo = avaluo_id;
    json.solicitante = $("#txtSolicitante").val();
    json.fechaInspeccion = $("#txtFechaInspeccion").val();
    json.regimen = $("#ContentPlaceHolder1_txtRegimen").val();
    json.objeto =  $("#selObjectoAvaluo").val();
    json.folioRegPubProp = $("#txtRegPubPro").val();
    json.inscripcion = $("#txtInscripcion").val();
    json.libro = $("#txtLibro").val();
    json.seccion = $("#txtSeccion").val();

    $.ajax({
        url: "<?php echo base_url("peritos/registrar_antecedentes"); ?>",
        type: "POST",
        data: json,
        success: function(data) {
          console.log(data);
          if (data === 'ok') {
            Swal.fire({
              text: 'Antecedentes Guardados Correctamente',
              icon: "success",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
            cargar_antecedentes(info);
            $("#liCaracteristicasUrbanas a:first-child").click();

          } else {
            Swal.fire({
              text: data,
              icon: "warning",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
          }

        }
      });
  } 

  function guardarCaracteristicasUrbanas() {
    var info = $('#txtInfCat').val();
    var json = new Object();
    json.info = info;
    json.avaluo = avaluo_id;
    json.clasificacionZona = $("#clasificacion_zonas").val();
    json.tipoConstDominante = $("#construccion_dominante").val();
    json.indiceSatZona = $("#saturacion_zona").val();
    json.poblacion = $("#list_poblacion").val();
    json.contaminacionAmbiental = $("#txtContaminacionAmbiental").val();
    json.usoSueloPermitido = $("#suelo_permitido").val();
    json.viaAccesoImportancia = $("#txtViaAccesoImportancia").val() ;
    json.agua = $("#chkAgua")[0].checked;
    json.drenaje = $("#chkDrenaje")[0].checked;
    json.energia = $("#chkElectricidad")[0].checked;
    json.alumbrado = $("#chkAlumbrado")[0].checked;
    json.basura =  $("#chkBasura")[0].checked;
    json.transporte = $("#chkTransporte")[0].checked;
    json.telefono = $("#chkTelefono")[0].checked;
    json.banqueta =  $("#chkBanqueta")[0].checked;
    json.guarniciones = $("#chkGuarniciones")[0].checked;
    json.pavimento = $("#chkPavimento")[0].checked;
    json.vigilancia =  $("#chkVigilancia")[0].checked;

    $.ajax({
        url: "<?php echo base_url("peritos/registrar_car_urb"); ?>",
        type: "POST",
        data: json,
        success: function(data) {
          console.log(data);
          if (data === 'ok') {
            Swal.fire({
              text: 'Caracteristicas Urbanas Guardados Correctamente',
              icon: "success",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
            cargar_car_urb(info);
            $("#liCaracteristicasTerreno a:first-child").click();

          } else {
            Swal.fire({
              text: data,
              icon: "warning",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
          }

        }
      });
  }

  function guardarCaracteristicasTerreno() {
    var info = $('#txtInfCat').val();
    var json = new Object();
    json.info = info;
    json.avaluo = avaluo_id;
    json.TopografiaConstruccion = $("#txtTopografiaConst").val();
    json.NumeroFrentes = $("#txtNumeroFrentes").val();
    json.CaracteristicasPanoramicas = $("#txtCaracteristicasPan").val();
    json.DensidadHabitacional = $("#txtDensidadHabitacional").val();
    json.IntensidadConstruccion =  $("#txtIntensidadConstruccion").val();
    json.ServidumbresRestricciones = $("#txtServidumbresConstrucciones").val();
    json.SuperficieEscrituras = $("#txtSupEscrituras").val();
    json.SuperficieTopografico = ($("#txtSupLevantamiento").val().trim() == "" ? "0" : $("#txtSupLevantamiento").val());

    $.ajax({
        url: "<?php echo base_url("peritos/registrar_car_terreno"); ?>",
        type: "POST",
        data: json,
        success: function(data) {
          console.log(data);
          if (data === 'ok') {
            Swal.fire({
              text: 'Caracteristicas del Terreno Guardados Correctamente',
              icon: "success",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
            cargar_car_terreno(info);
            $("#liDescripcionGeneral a:first-child").click();

          } else {
            Swal.fire({
              text: data,
              icon: "warning",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
          }

        }
      });
}

function guardarSello(){
    var rfc = $("#txtRFCPer").val();
    var nombre = $("#txtNombrePer").val();
    var serie = $("#txtSerie").val();
    var sello = $("#txtSello").val();
    var info = $('#txtInfCat').val();
  
    var json = new Object();
    json.info = info;
    json.avaluo = avaluo_id;
    json.rfc = rfc;
    json.nombre = nombre;
    json.serie = serie;
    json.sello = sello;

    if(json.sello != "" || sello == null){
      console.log(json);
      $.ajax({
        url: "<?php echo base_url("peritos/registrar_sello"); ?>",
        type: "POST",
        data: json,
        success: function(data) {
          console.log(data);
          if (data === 'ok') {
            Swal.fire({
              text: 'Autorización Guardada Correctamente',
              icon: "success",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
            cargar_sello(info);

          } else {
            Swal.fire({
              text: "No se pudo guardar la Autorización",
              icon: "warning",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
          }

        }
      });
    }
    else{
      alert("Debe firmar primero para sellar el tramite");
    }

   
}

function guardarValorMercado() {
    var valorMercado = $("#txtValorMercado").val().replace("$", "").replace(",", "").replace(",", "");
    var consideraciones = $("#txtConsideraciones").val();
    var info = $('#txtInfCat').val();
    var json = new Object();
    json.info = info;
    json.avaluo = avaluo_id;
    json.valorMercado = valorMercado;
    json.consideraciones = consideraciones;

    console.log(json);

    $.ajax({
        url: "<?php echo base_url("peritos/registrar_valor_mercado"); ?>",
        type: "POST",
        data: json,
        success: function(data) {
          console.log(data);
          if (data === 'ok') {
            Swal.fire({
              text: 'Conclusión Guardada Correctamente',
              icon: "success",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
            cargar_valor_mercado(info);
            $("#liFoto a:first-child").click();

          } else {
            Swal.fire({
              text: data,
              icon: "warning",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
          }

        }
      });
}

  function guardarDescripcionGeneral() {
    var info = $('#txtInfCat').val();
    var json = new Object();
    json.info = info;
    json.avaluo = avaluo_id;
    json.CVE_GPO_USO = $("#selUso").val();
    json.EdadAproximada = $("#txtEdadAprox").val();
    json.NumeroNiveles = $("#txtNumeroNiveles").val();
    json.EstadoConservacion = $("#txtEstadoConservacion").val();
    json.CalidadProyecto = $("#txtCalidadProyecto").val();
    json.UnidadesRentables = $("#txtUnidadesRentables").val();
    json.DescripcionGeneral = $("#txtDescGeneralInmueble").val();

    $.ajax({
        url: "<?php echo base_url("peritos/registrar_descripcion_general"); ?>",
        type: "POST",
        data: json,
        success: function(data) {
          console.log(data);
          if (data === 'ok') {
            Swal.fire({
              text: 'Descripción General Guardada Correctamente',
              icon: "success",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
            cargar_descripcion_general(info);
            $("#liElementosConstruccion a:first-child").click();

          } else {
            Swal.fire({
              text: data,
              icon: "warning",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
          }

        }
      });
  }

  function guardarElementosConstruccion() {
    var info = $('#txtInfCat').val();
    var json = new Object();
    json.info = info;
    json.avaluo = avaluo_id;
    json.A_Cimientos = $("#list_cimientos").val();
    json.A_Estructura = $("#txtEstructura").val();
    json.A_Muros = $("#list_muros").val();
    json.A_EntrePisos = $("#txtEntrepisos").val();
    json.A_Techos = $("#list_techos").val();
    json.A_Azoteas = $("#txtAzoteas").val();
    json.A_Bardas = $("#txtBardas").val();
    json.B_AplanadosInteriores = $("#list_aplanados").val();
    json.B_AplanadosExteriores = $("#txtAplanadosExteriores").val();
    json.B_Plafones = $("#txtPlafones").val();
    json.B_Lambrines = $("#list_lambrines").val();
    json.B_Pisos = $("#list_pisos").val();
    json.B_Zoclos = $("#txtZoclos").val();
    json.B_Escaleras = $("#txtEscaleras").val();
    json.B_Pintura = $("#list_pintura").val();
    json.B_RecubrimientosEspeciales = $("#txtRecubrimientosEspeciales").val();
    json.C_Puertas = $("#list_puertas").val();
    json.C_Closets = $("#txtClosets").val();
    json.D_MueblesDeBano = $("#list_mueblesb").val();
    json.D_EquipoDeConcina = $("#txtEquipoCocina").val();
    json.E_InstalacionesElectricas = $("#list_instalacionese").val();
    json.F_Herreria = $("#list_herreria").val();
    json.G_Vidrieria = $("#list_vidrieria").val();
    json.H_Cerrajeria = $("#list_cerrajeria").val();
    json.I_Fachadas = $("#list_fachadas").val();
    json.J_InstalacionesEspeciales = $("#list_especiales").val();
    
    $.ajax({
              url: "<?php echo base_url("peritos/registrar_elementos_construccion"); ?>",
              type: "POST",
              data: json,
              success: function(data) {
                console.log(data);
                if (data === 'ok') {
                  Swal.fire({
                    text: 'Elementos de Construcción Guardados Correctamente',
                    icon: "success",
                    showCancelButton: !0,
                    confirmButtonColor: "#3b5de7",
                  })
                  cargar_elementos_construccion(info);
                  $("#liAvaluoFisico a:first-child").click();

                } else {
                  Swal.fire({
                    text: data,
                    icon: "warning",
                    showCancelButton: !0,
                    confirmButtonColor: "#3b5de7",
                  })
                }

              }
            });
    
}

function guardarFisico() {

if (!$("#botonSubirPlano")[0].hasAttribute("disabled")) { alert("Para guardar debe subir el plano..."); return false; }

var fisicoTerreno = [];
var valorMercado = $("#txtValorMercado").val().replace("$", "").replace(",", "").replace(",", "");
$('#tablaFisicoTerreno tbody tr').each(function(i, el) {
    var val1 = $.trim($(this).find('td:eq(0)').text());
    var val2 = $.trim($(this).find('td:eq(1)').text());
    var val3 = $.trim($(this).find('td:eq(2)').text()); 
    var val4 = $.trim($(this).find('td:eq(3)').text()); 
    var val5 = $.trim($(this).find('td:eq(4)').text()); 
    var obj = {};
    obj["SupMt2"] = val1;
    obj["ValorUnitario"] = val2;
    obj["FactorDemerito"] = val3;
    obj["Motivo"] = val4;
    obj["ValorTerreno"] = val5;
    fisicoTerreno.push(obj);
});

var fisicoConstruccion = [];
$('#tablaFisicoConstrucciones tbody tr').each(function(i, el) {
    var cat = $.trim($(this).find('td:eq(0)').text());
    var supmt2 = $.trim($(this).find('td:eq(1)').text());
    var edad = $.trim($(this).find('td:eq(4)').text()); 
    var fcom = $.trim($(this).find('td:eq(6)').text());
    var edo = $.trim($(this).find('td:eq(3)').text());
    var valunit = $.trim($(this).find('td:eq(2)').text()); 
    var valneto = $.trim($(this).find('td:eq(7)').text());
    var fdem = $.trim($(this).find('td:eq(5)').text());
    var obj = {};

    if (supmt2 <= 0) { alert("No es posible guardar una construccion con superficie menor o igual a cero..."); return false; }
    if (valunit <= 0) { alert("No es posible guardar una construccion con valor unitario menor o igual a cero..."); return false; }
    obj["Categoria"] = cat;
    obj["SupMt2"] = supmt2;
    obj["Edad"] = edad;
    obj["FCom"] = fcom;
    obj["EstadoConservacion"] = edo;
    obj["ValorUnitario"] = valunit;
    obj["ValorNeto"] = valneto;
    obj["FDem"] = fdem;
    fisicoConstruccion.push(obj);
});

var fisicoEspeciales = [];
$('#tablaEspeciales tbody tr').each(function(i, el) {
    var val1 = $.trim($(this).find('td:eq(0)').text());
    var val2 = $.trim($(this).find('td:eq(1)').text());
    var val3 = $.trim($(this).find('td:eq(2)').text());
    var val4 = $.trim($(this).find('td:eq(3)').text());
    var val5 = $.trim($(this).find('td:eq(4)').text());
    var vrnu = $.trim($(this).find('td:eq(5)').text());
    var valp = $.trim($(this).find('td:eq(6)').text());
    var obj = {};
    if (val3 <= 0) { alert("No es posible guardar con superficie menor o igual a cero..."); return false; }
    if (val4 <= 0) { alert("No es posible guardar con valor unitario menor o igual a cero..."); return false; }
    obj["Concepto"] = val1;
    obj["Unidad"] = val2;
    obj["Cantidad"] = val3;
    obj["ValorUnitario"] = val4;
    obj["FDemerito"] = val5;
    obj["VRNUnitario"] = vrnu;
    obj["ValorParcial"] = valp;
    fisicoEspeciales.push(obj);
});
var jFt = JSON.stringify(fisicoTerreno);
var jFc = JSON.stringify(fisicoConstruccion);
var jFe = JSON.stringify(fisicoEspeciales);

/*console.log(jFt);
console.log(jFc);
console.log(jFe);*/

var info = $('#txtInfCat').val();
var json = new Object();
json.info = info;
json.avaluo = avaluo_id;
json.fisico = jFt;
json.construcciones = jFc;
json.especiales = jFe;
json.valorMercado = valorMercado;

$.ajax({
  url: "<?php echo base_url("peritos/registrar_avaluo_fisico"); ?>",
  type: "POST",
              data: json,
              success: function(data) {
                //console.log(data);
                if (data === 'ok') {
                  Swal.fire({
                    text: 'Avaluo Físico Guardado Correctamente',
                    icon: "success",
                    showCancelButton: !0,
                    confirmButtonColor: "#3b5de7",
                  })
                  cargar_avaluo_fisico();
                } else {
                  Swal.fire({
                    text: 'Ocurrio un error al guardar el avaluo fisico',
                    icon: "warning",
                    showCancelButton: !0,
                    confirmButtonColor: "#3b5de7",
                  })
                }

              }
  });
}

$(document).on('click', '#btnFirmarPer', function(e) {
      var avaluo = avaluo_id;
      var certfile = document.getElementById('fileCerPer').files[0];
      var certkey = document.getElementById('fileKeyPer').files[0];
      var passkey = document.getElementById('txtPAssKeyPer').value;
      
      var mensaje = "|AUTORIZACIÓN DE AVALUO COMERCIAL|<?php echo $info; ?>||";
      //var mensaje = "Autorizar " + inf;

      var formData = new FormData();
      formData.append('certfile', certfile);
      formData.append('certkey', certkey);
      formData.append('passkey', passkey);
      formData.append('mensaje', mensaje);

      $.ajax({
            url: "<?php echo base_url("peritos/firmar"); ?>",
            type: "POST",
            data: formData,
            processData: false,  
            contentType: false,
                success: function (data) {
                  console.log(data);
                  if(data.certificado.length > 0){
                    $('#txtRFCPer').val(data.certificado[0].rfc);
                    $('#txtSerie').val(data.certificado[0].serial);
                    $('#txtSello').val(data.certificado[0].sello);
                    $('#txtNombrePer').val(data.certificado[0].nombre);
                  }
                  
                },
                error: function (jqXHR, status) {
                                Swal.fire({
                                    title: "Error al Firmar",
                                    text: 'Por favor verifique los archivos y contraseña de su firma electrónica',
                                    icon: 'error'
                                })
                            }
        });


});

  //botones
  $(document).on('change', '#fileUpload', function(e) {
      var inf = "<?php echo $info; ?>";
      var avaluo = avaluo_id;
      var file = e.target.files[0];
      var formData = new FormData();
      formData.append('file', file);
      formData.append('inf', inf);
      formData.append('avaluo', avaluo);
      $.ajax({
            url: "<?php echo base_url("peritos/upload_imagen"); ?>",
            type: "POST",
            data: formData,
            processData: false,  
            contentType: false,
                success: function (data) {
                  console.log(data);
                  if(data == "ok"){
                    Swal.fire({
                      text: "Fotografia agregada correctamente",
                      icon: "success",
                      showCancelButton: !0,
                      confirmButtonColor: "#3b5de7",
                    })  
                    cargarImagen();
                  }
                  else{
                    Swal.fire({
                      text: "No se pudo guardar la imagen, favor de contactar con sistemas.",
                      icon: "error",
                      showCancelButton: !0,
                      confirmButtonColor: "#3b5de7",
                    })  
                  }
                  
                }
        });
      
    });

  $(document).on('change', '#fileUploadPlano', function(e) {
        var inf = "<?php echo $info; ?>";
        var avaluo = avaluo_id;
        var file = e.target.files[0];
        var formData = new FormData();
        formData.append('file', file);
        formData.append('inf', inf);
        formData.append('avaluo', avaluo);

        $.ajax({
            url: "<?php echo base_url("peritos/upload_plano"); ?>",
            type: "POST",
            data: formData,
            processData: false,  
            contentType: false,
                success: function (data) {
                  console.log(data);
                  if(data != "error"){
                    Swal.fire({
                      text: "Se guardo con exito",
                      icon: "success",
                      showCancelButton: !0,
                      confirmButtonColor: "#3b5de7",
                    })  
                    $("#botonSubirPlano").attr("disabled", 'disabled');
                    $("#aVerPlano").attr("href", "<?php echo base_url("documentos_peritos/planos");?>/"+inf+"/"+data);
                    $("#aVerPlano").attr("style", "color:blue");
                  }
                  else{
                    Swal.fire({
                      text: "No se pudo guardar el archivo, favor de contactar con sistemas.",
                      icon: "error",
                      showCancelButton: !0,
                      confirmButtonColor: "#3b5de7",
                    })  
                  }
                  
                }
        });    
  });

  $(document).on('click', '#botonAgregarOrientacion', function() {
    var desc = $("#txtOrientacion").val();
    var ori = $("#selectOrientacion").val();
    if (desc != "") {
      //guardar orientacion
      var json = new Object();
      json.inf_cat = "<?php echo $info; ?>";
      json.orientacion = ori;
      json.descripcion = desc;

      $.ajax({
        url: "<?php echo base_url("peritos/registrar_orientacion"); ?>",
        type: "POST",
        data: json,
        success: function(data) {
          console.log(data);
          if (data === 'ok') {
            cargar_tabla_orientaciones();
            $("#txtOrientacion").val("");
            $("#selectOrientacion").val("");
          } else {
            Swal.fire({
              text: data,
              icon: "warning",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
          }

        }
      });
    }
  });

  $(document).on('click', '#botonImprimirAvaluo', function(e) {
        var inf = "<?php echo $info; ?>";
        var url = "<?php echo base_url("peritos/imprimiravaluo"); ?>/" + inf;
        window.open(url,'popup','width=1200,height=800,scrollbars=yes,resizable=yes');
        return false;
  });

  $(document).on('click', '#botonGuardar', function(e) {
    e.preventDefault();
    if ($("#liPortada > a").hasClass("active")) {
       //todo: validar
        guardarPortada();
        
    } else if ($("#liAntecedentes > a").hasClass("active")) {
       
      guardarAntecedentes();
        
    } else if ($("#liCaracteristicasUrbanas > a").hasClass("active")) {
       
      guardarCaracteristicasUrbanas();
        
    } else if ($("#liCaracteristicasTerreno > a").hasClass("active")) {
       
      guardarCaracteristicasTerreno();
        
    } else if ($("#liDescripcionGeneral > a").hasClass("active")) {
       
      guardarDescripcionGeneral();
        
    } else if ($("#liElementosConstruccion > a").hasClass("active")) {
       
      guardarElementosConstruccion();

    } else if ($("#liAvaluoFisico > a").hasClass("active")) {

        guardarFisico();
    
    } else if ($("#liConclusion > a").hasClass("active")) {
        
        guardarValorMercado();
        
    } else if($("#liAutorizar > a").hasClass("active")){
        guardarSello();
    }
    return false;
  });

  $("#botonSubirPlano").click(function() { $('#fileUploadPlano').click(); return false; });


  $("input[data-type='currency']").on({ keyup: function() { formatCurrency($(this)); }, blur: function() { formatCurrency($(this), "blur"); } });

  $(document).on('change', '#checkBaldio', function() {
    if ($(this)[0].checked) { 
      $("#divElementosConstruccion .form-control").val("NO APLICA"); 
    } else { 
      $("#divElementosConstruccion .form-control").val(""); 
    }
  });

  //utilerias
  function stringToBoolean(string){
    switch (string.toLowerCase().trim()) {
      case "t":
        //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor1
        return true;
        break;
      case "f":
        //Declaraciones ejecutadas cuando el resultado de expresión coincide con el valor2
        return false;
        break;
      default:
        return false;
    }
  }


  function activaEditar(ind, ridx) {
    if (editando == 0) {
        editindx = ind;
        editrowindx = ridx.parentElement.rowIndex;
        valor = $("#tablaFisicoTerreno")[0].rows[editrowindx].cells[editindx].innerText;
        $("#tablaFisicoTerreno")[0].rows[editrowindx].cells[editindx].innerHTML = '<div class="input-group bor"><input class="form-control" type="text" ><div class="input-group-btn"> <button type="button" onclick="return actualizarCol(0)" class="btn btn-success" aria-label="Help"><span class="mdi mdi-check"></span></button></div></div>';
        $("#tablaFisicoTerreno")[0].rows[editrowindx].cells[editindx].firstChild.firstChild.value = valor.replace("$", '').replace(" ", "").replace(",", "").replace(",", "");
        $("#tablaFisicoTerreno")[0].rows[editrowindx].cells[editindx].firstChild.firstChild.focus();
        editando = 1;
    }
}

function activaEditarCONST(ind, ridx) {
  if (editando == 0) {
        editindx = ind;
        editrowindx = ridx.parentElement.rowIndex;
        if (ind == 0) {
            valor = $("#tablaFisicoConstrucciones")[0].rows[editrowindx].cells[editindx].innerText;
            $("#tablaFisicoConstrucciones")[0].rows[editrowindx].cells[editindx].innerHTML = crearComboCategoriaMpio(valor);
            $(".selCatMpio").change(function() { actualizaSelConsMpio(this); });
            editando = 1;
        } else if (ind == 1 || 2 || ind == 3 || ind == 4 || ind == 6) {
            valor = $("#tablaFisicoConstrucciones")[0].rows[editrowindx].cells[editindx].innerText;
            $("#tablaFisicoConstrucciones")[0].rows[editrowindx].cells[editindx].innerHTML = '<div class="input-group bor"><input class="form-control" type="text" ><div class="input-group-btn"> <button type="button" onclick="return actualizarCol(1)" class="btn btn-success" aria-label="Help"><span class="mdi mdi-check"></span></button></div></div>';
            $("#tablaFisicoConstrucciones")[0].rows[editrowindx].cells[editindx].firstChild.firstChild.value = valor.replace("$", '').replace(" ", "").replace(",", "").replace(",", "");
            $("#tablaFisicoConstrucciones")[0].rows[editrowindx].cells[editindx].firstChild.firstChild.focus();
            editando = 1;
        }
  }
}

function activaEditarEspeciales(ind, ridx) {
    if (editando == 0) {
        editindx = ind;
        editrowindx = ridx.parentElement.rowIndex;
        valor = $("#tablaEspeciales")[0].rows[editrowindx].cells[editindx].innerText;
        $("#tablaEspeciales")[0].rows[editrowindx].cells[editindx].innerHTML = '<div class="input-group bor"><input class="form-control" type="text"><div class="input-group-btn"> <button type="button" onclick="return actualizarCol(2)" class="btn btn-success" aria-label="Help"><span class="mdi mdi-check"></span></button></div></div>';
        $("#tablaEspeciales")[0].rows[editrowindx].cells[editindx].firstChild.firstChild.value = valor.replace("$", '').replace(" ", "").replace(",", "").replace(",", "");
        $("#tablaEspeciales")[0].rows[editrowindx].cells[editindx].firstChild.firstChild.focus();
        editando = 1;
    }
}

function crearComboCategoriaMpio(va) {
    var ht = '<select class="form-control selCatMpio">';
    for (var i = 0; i < cat_const_list.length; i++) { if (va != cat_const_list[0].CVE_CAT_CONST) { ht = ht + '<option value="' + cat_const_list[i].CVE_CAT_CONST + '" >' + cat_const_list[i].CVE_CAT_CONST + '</option>'; } else { ht = ht + '<option value="' + cat_const_list[i].CVE_CAT_CONST + '" selected="selected">' + cat_const_list[i].CVE_CAT_CONST + '</option>'; } }
    ht = ht + '<select class="form-control">';
    return ht;
}

function actualizarCol(tabl) {
    var tabla = '';
    if (tabl == 0) { tabla = "tablaFisicoTerreno"; } else if (tabl == 1) { tabla = 'tablaFisicoConstrucciones'; } else { tabla = 'tablaEspeciales'; }
    valor = $("#" + tabla)[0].rows[editrowindx].cells[editindx].firstChild.firstChild.value;
    $(".bor").remove();
    if (editindx == 1 && tabl == 0) { $("#" + tabla)[0].rows[editrowindx].cells[editindx].innerText = aPesos(parseFloat(valor)); } else { $("#" + tabla)[0].rows[editrowindx].cells[editindx].innerText = valor; }
    if (tabl == 1) {
        var json = new Object();
        json.CVE_CAT_CONST = $("#" + tabla)[0].rows[editrowindx].cells[0].innerText;
        json.CVE_EDO_CONS = $("#" + tabla)[0].rows[editrowindx].cells[3].innerText;
        json.EDD_CONST = $("#" + tabla)[0].rows[editrowindx].cells[4].innerText;
        //console.log(json);
        $.ajax({
        url: "<?php echo base_url("peritos/consulta_factor_demerito_construccion"); ?>",
        type: "POST",
        data: json,
        success: function(response) {
          //console.log(response);
          var info1 = eval(response.data);
                if (info1.length > 0) {
                    $("#" + tabla)[0].rows[editrowindx].cells[5].innerText = info1[0].FAC_DEM_CO;
                    $("#" + tabla)[0].rows[editrowindx].cells[7].innerText = aPesos(parseFloat(info1[0].FAC_DEM_CO) * parseFloat($("#" + tabla)[0].rows[editrowindx].cells[1].innerText) * parseFloat($("#" + tabla)[0].rows[editrowindx].cells[2].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", "")) * parseFloat($("#" + tabla)[0].rows[editrowindx].cells[6].innerText));
                    calculaTotal();
                } else { $("#" + tabla)[0].rows[editrowindx].cells[5].innerText = '0'; }
        }
      });

    } else if (tabl == 2) {
        $("#" + tabla)[0].rows[editrowindx].cells[5].innerText = parseFloat($("#" + tabla)[0].rows[editrowindx].cells[3].innerText) * (1 - parseFloat($("#" + tabla)[0].rows[editrowindx].cells[4].innerText) / 100);
        $("#" + tabla)[0].rows[editrowindx].cells[6].innerText = aPesos(parseFloat($("#" + tabla)[0].rows[editrowindx].cells[2].innerText) * parseFloat($("#" + tabla)[0].rows[editrowindx].cells[5].innerText));
        $("#" + tabla)[0].rows[editrowindx].cells[5].innerText = aPesos(parseFloat($("#" + tabla)[0].rows[editrowindx].cells[5].innerText));
        calculaTotal();
    } else {
        var sup = parseFloat($("#" + tabla)[0].rows[editrowindx].cells[0].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        var valunit = parseFloat($("#" + tabla)[0].rows[editrowindx].cells[1].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        var factDM = parseFloat($("#" + tabla)[0].rows[editrowindx].cells[2].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        $("#" + tabla)[0].rows[editrowindx].cells[4].innerText = aPesos(sup * valunit * factDM);
        calculaTotal();
    }
    editando = 0;
    return false;
}


function calculaTotal() {
    //acumuladores
    var sum = 0.0;
    var sumTerr = 0.0;
    var valTot = 0.00;
    var sumConst = 0.0;
    var sumFisico = 0.0;
    
    //se recorren cada una de las filas del terreno
    for (var i = 2; i < $("#tablaFisicoTerreno")[0].rows.length -1; i++) {
        //obtenemos el valor del terreno
        sum += parseFloat($("#tablaFisicoTerreno")[0].rows[i].cells[4].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        sumTerr += parseFloat($("#tablaFisicoTerreno")[0].rows[i].cells[0].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));;
        valTot += parseFloat($("#tablaFisicoTerreno")[0].rows[i].cells[4].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
    }
    //si el valor del terreno es menor que el valor comercial segun catastro mandamos una alerta
    if (valTot < valTotT_ws) { //Swal.fire({ icon: 'info', text: 'El valor comercial del terreno no puede ser menor que el valor catastral'}) 
    }
    //se actualizan los controles
    $("#txtvalortotalterreno").text(aPesos(parseFloat(sum)));
    $("#txtsuptotalterreno").text(parseFloat(sumTerr).toFixed(2));
    $("#supTerrRev2").text(parseFloat(sumTerr).toFixed(2));
    $("#valTerrRev2").text($("#txtvalortotalterreno").text());
    
    sumFisico = sumFisico + sum;
    
    //se recorren cada una de las filas de
    sum = 0.0;
    for (var i = 2; i < $("#tablaFisicoConstrucciones")[0].rows.length -1; i++) {
        sum = sum + parseFloat($("#tablaFisicoConstrucciones")[0].rows[i].cells[7].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        sumConst = sumConst + parseFloat($("#tablaFisicoConstrucciones")[0].rows[i].cells[1].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
    }
    $("#txtvalortotalconst").text(aPesos(parseFloat(sum)));
    $("#txtsuptotalconst").text(parseFloat(sumConst).toFixed(2));
    $("#supConstRev2").text(parseFloat(sumConst).toFixed(2));
    $("#valConstRev2").text($("#txtvalortotalconst").text());
    sumFisico = sumFisico + sum;
    
    //se recorren cada una de las filas del terreno
    sum = 0.0;
    for (var i = 2; i < $("#tablaEspeciales")[0].rows.length -1; i++) { 
      sum = sum + parseFloat($("#tablaEspeciales")[0].rows[i].cells[6].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", "")); 
    }
    $("#txtvalortotalespeciales").text(aPesos(parseFloat(sum)));

    sumFisico = sumFisico + sum;

    console.log("Sum fisico: " + sumFisico);

    $("#txtValorFisico").val(aPesos(parseFloat(sumFisico)));
    $("#txtValorFisico2").val(aPesos(parseFloat(sumFisico)));
    $("#txtValorMercado").val(aPesos(parseFloat(sumFisico)));
    $("#valTOTALRev2").text(aPesos(parseFloat(sumFisico)));
}

function sumatotal(){
    var sumFisico = 0.0;
    sumFisico += parseFloat($("#txtvalortotalterreno").text().replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
    sumFisico += parseFloat($("#txtvalortotalconst").text().replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
    sumFisico += parseFloat($("#txtvalortotalespeciales").text().replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));

    $("#txtValorFisico").val(aPesos(parseFloat(sumFisico)));
    $("#txtValorFisico2").val(aPesos(parseFloat(sumFisico)));
    $("#txtValorMercado").val(aPesos(parseFloat(sumFisico)));
    $("#valTOTALRev2").text(aPesos(parseFloat(sumFisico)));
}

function agregarConstruccion() {
    $("#botonSubirPlano").removeAttr("disabled");
    var ro;
    if ($("#tablaFisicoConstrucciones")[0].rows.length == 0) {
        var header = $("#tablaFisicoConstrucciones")[0].createTHead();
        ro = header.insertRow(-1);
        ro.insertCell(-1).outerHTML = "<th>Categoria</th>";
        ro.insertCell(-1).outerHTML = "<th>Superficie M2</th>";
        ro.insertCell(-1).outerHTML = "<th>Valor Unitario</th>";
        ro.insertCell(-1).outerHTML = "<th>Edo.</th>";
        ro.insertCell(-1).outerHTML = "<th>Edad</th>";
        ro.insertCell(-1).outerHTML = "<th>F.Dem.</th>";
        ro.insertCell(-1).outerHTML = "<th>F.Com</th>";
        ro.insertCell(-1).outerHTML = "<th>Valor Unitario</th>";
        ro.insertCell(-1).outerHTML = "<th></th>";
        $("#tablaFisicoConstrucciones")[0].appendChild(header);
        var tbody = document.createElement("tbody");
        ro = $("#tablaFisicoConstrucciones")[0].appendChild(tbody);
    }
    /*if (parseFloat($("#tablaFisicoConstrucciones")[0].rows[$("#tablaFisicoConstrucciones")[0].rows.length - 1].cells[7].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", "")) != 0) {*/
        ro = $("#tablaFisicoConstrucciones tbody")[0].insertRow(-1);
        ro.insertCell(0).innerText = "0";
        ro.insertCell(0).innerText = "0";
        ro.insertCell(0).innerText = "0";
        ro.insertCell(0).innerText = "0";
        ro.insertCell(0).innerText = "0";
        ro.insertCell(0).innerText = "0";
        ro.insertCell(0).innerText = "0";
        ro.insertCell(0).innerText = "M2";
        ro.insertCell(-1).innerHTML = '<button class="deleterow editaTerr btn btn-outline-danger pull-right btn-sm"><span class="mdi mdi-delete"></span></button>';
    //}
    return false;
}

function agregarEspeciales() {
    var ro;
    if ($("#tablaEspeciales")[0].rows.length == 0) {
        var header = $("#tablaEspeciales")[0].createTHead();
        ro = header.insertRow(-1);
        ro.insertCell(-1).outerHTML = "<th>Concepto</th>";
        ro.insertCell(-1).outerHTML = "<th>Unidad</th>";
        ro.insertCell(-1).outerHTML = "<th>Cantidad</th>";
        ro.insertCell(-1).outerHTML = "<th>V.N.R. Unit.</th>";
        ro.insertCell(-1).outerHTML = "<th>% Demerito</th>";
        ro.insertCell(-1).outerHTML = "<th>V.R.N. Unitario</th>";
        ro.insertCell(-1).outerHTML = "<th>Valor Parcial</th>";
        ro.insertCell(-1).outerHTML = "<th></th>";
        $("#tablaEspeciales")[0].appendChild(header);
        var tbody = document.createElement("tbody");
        ro = $("#tablaEspeciales")[0].appendChild(tbody);
    }
    ro = $("#tablaEspeciales")[0].getElementsByTagName('tbody')[0].insertRow(-1);
    ro.insertCell(0).innerText = "0";
    ro.insertCell(0).innerText = "0";
    ro.insertCell(0).innerText = "0";
    ro.insertCell(0).innerText = "0";
    ro.insertCell(0).innerText = "0";
    ro.insertCell(0).innerText = "";
    ro.insertCell(0).innerText = "";
    ro.insertCell(-1).innerHTML = '<button class="deleterowe editaTerr btn btn-outline-danger btn-sm pull-right"><span class="mdi mdi-delete"></span></button>';
    return false;
}

function actualizaSelConsMpio(cmbo) {
    var tabla = "tablaFisicoConstrucciones";
    valor = $("#tablaFisicoConstrucciones")[0].rows[editrowindx].cells[editindx].firstChild.firstChild.value;
    $(cmbo).remove();
    if (editindx == 0) {
        $("#tablaFisicoConstrucciones")[0].rows[editrowindx].cells[editindx].innerText = $(cmbo).val();
        $("#tablaFisicoConstrucciones")[0].rows[editrowindx].cells[2].innerText = regresaValorConstMpio($(cmbo).val());
        var json = new Object();
        json.CVE_CAT_CONST = $("#" + tabla)[0].rows[editrowindx].cells[0].innerText;
        json.CVE_EDO_CONS = $("#" + tabla)[0].rows[editrowindx].cells[3].innerText;
        json.EDD_CONST = $("#" + tabla)[0].rows[editrowindx].cells[4].innerText;
        $.ajax({
        url: "<?php echo base_url("peritos/consulta_factor_demerito_construccion"); ?>",
        type: "POST",
        data: json,
        success: function(response) {
          //console.log(response);
          var info1 = eval(response.data);
                if (info1.length > 0) {
                    $("#" + tabla)[0].rows[editrowindx].cells[5].innerText = info1[0].FAC_DEM_CO;
                    $("#" + tabla)[0].rows[editrowindx].cells[7].innerText = aPesos(parseFloat(info1[0].FAC_DEM_CO) * parseFloat($("#" + tabla)[0].rows[editrowindx].cells[1].innerText) * parseFloat($("#" + tabla)[0].rows[editrowindx].cells[2].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", "")) * parseFloat($("#" + tabla)[0].rows[editrowindx].cells[6].innerText));
                    calculaTotal();
                } else { $("#" + tabla)[0].rows[editrowindx].cells[5].innerText = 'Error'; }
        }
      });
       
    }
    editando = 0;
    return false;
}

function regresaValorConstMpio(categoria) { for (var i = 0; i < cat_const_list.length; i++) { if (cat_const_list[i].CVE_CAT_CONST == categoria) { return aPesos(parseFloat(cat_const_list[i].VAL_UNIT_CONST)); } } }

function editarTerreno() {
    if ($("#tablaFisicoTerreno")[0].rows.length > 2) {
        $("#tablaFisicoTerreno")[0].deleteRow($("#tablaFisicoTerreno")[0].rows.length - 1);
        calculaTotal();
        return false;
    }
    return false;
}

$("#tablaFisicoConstrucciones").on('click', '.deleterow', function () {
    $(this).closest('tr').remove();
    calculaTotal();
});

$("#tablaEspeciales").on('click', '.deleterowe', function () {
    $(this).closest('tr').remove();
    calculaTotal();
});

function agregarImagen() {
    $("#fileUpload").click();
    return false;
}

function cargarImagen() {
    var inf = "<?php echo $info; ?>";
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_imagenes"); ?>/" + inf,
      dataType: "json",
      success: function(data) {
        console.log(data.result);
        if (data.result.length > 0) {
          var info1 = eval(data.result);
            var imHtml = "";
            var imHtmlRev = "";
            $("#divImages").html("");
            $("#divImagesRevision").html("");
            var img = true;
            for (var i = 0; i < info1.length; i++) {
                imHtml = imHtml + "<div class='col-md-6 '><div class=''><img class='imgAv img-thumbnail' src='<?php echo base_url("documentos_peritos/img_reporte_fotografico");?>/"+inf+ "/"+ info1[i].url + "' alt='...'><div class='caption'><div class='input-group '><input id='img_" + info1[i].id_imagen + "' type='text' class='form-control' placeholder='...' value='" + info1[i].descripcion + "'> <span class='input-group-btn'><button onclick='actualizaImg(" + info1[i].id_imagen + ")' class='btn btn-outline-success' type='button'><span class='mdi mdi-refresh' aria-hidden='true'></span></button><button class='btn btn-outline-danger' onclick='return eliminarImg(" + info1[i].id_imagen + " ,\""+info1[i].url+"\")' type='button'><span class='mdi mdi-delete' aria-hidden='true'></span></button></span></div></div></div></div>";
                imHtmlRev = imHtmlRev + "<div class='col-md-4 '><img class='imgAv img-thumbnail' src='<?php echo base_url("documentos_peritos/img_reporte_fotografico");?>/"+inf+"/"+info1[i].url + "' alt='...'></div>";
            }
            if (img) {
                $("#reporte_foto_ok").show();
                $("#divReporteFoto-tab").addClass('border border-success');
                $("#divImprimir-tab").addClass('border border-success');
                $("#imprimir_ok").show();
                $("#divAvaluoFisico-tab").addClass('border border-success');
                $('#av_fisico_ok').show();
                /*activarTab($("#liFoto"), true);
                activarTab($("#liReporte"), true);
                activarTab($("#liAutorizar"), true);*/
            } else { 
              $("#reporte_foto_ok").hide();
            }
            $("#divImages").html(imHtml);
            $("#divImagesRevision").html(imHtmlRev);
        }
        else{
          $("#reporte_foto_ok").hide();
        }
      }
    });

}

function actualizaImg(imgID) {
    var json = new Object();
    json.imgID = imgID;
    json.descripcion = $("#img_" + imgID).val();
    //if (estatus > 2) { return false; }
    $.ajax({
        url: "<?php echo base_url("peritos/actualizar_imagen"); ?>",
        type: "POST",
        data: json,
        success: function(data) {
          if (data === 'ok') {
            Swal.fire({
              text: 'Se actualizo la descripcion con exito',
              icon: "success",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
            cargarImagen();
          } else {
            Swal.fire({
              text: 'Ocurrio un error al actualizar la descripcion',
              icon: "warning",
              showCancelButton: !0,
              confirmButtonColor: "#3b5de7",
            })
          }

        }
      });
}

function eliminarImg(imgID, nombre) {
    var inf = "<?php echo $info; ?>";
    var json = new Object();
    json.imgID = imgID;
    json.inf = inf;
    json.nombre = nombre;
    //if (estatus > 2) { return false; }
    Swal.fire({
                text: "¿Esta seguro de eliminar la imagen?",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Si, Eliminar'
                }).then((result) => {
                    if (result.isConfirmed) {
                      $.ajax({
                        url: "<?php echo base_url("peritos/eliminar_imagen"); ?>",
                        type: "POST",
                        data: json,
                        success: function(data) {
                          if (data === 'ok') {
                            Swal.fire({
                              text: 'Se elimino la magen con exito',
                              icon: "success",
                              showCancelButton: !0,
                              confirmButtonColor: "#3b5de7",
                            })
                            cargarImagen();
                          } else {
                            Swal.fire({
                              text: 'Ocurrio un error al eliminar la descripcion',
                              icon: "warning",
                              showCancelButton: !0,
                              confirmButtonColor: "#3b5de7",
                            })
                          }

                        }
                      });
                    }
                })
    
    
    return false;
}


</script>