<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h4 class="page-title mb-0 font-size-18">SOLICITUD DE AVALUO CATASTRAL</h4>

                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                            <li class="breadcrumb-item active">PERITOS</li>
                        </ol>
                    </div>



                </div>
            </div>
        </div>
        <!-- end page title -->
        <!-- start row -->
        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Asistente de Captura del Avaluo Comercial</h4>
                        <p class="card-title-desc">Por favor valla completando cada de uno de los pasos a continuación para poder enviar el avaluo a revision</p>

                        <ul class="nav nav-pills nav-fill" role="tablist">
                            <li class="nav-item p-1 col-3" id="liPortada">
                                <a class="nav-link border active" data-toggle="tab" href="#divPortada" role="tab" id="divPortada-tab" aria-controls="divPortada" aria-selected="true">
                                    <span class="d-block d-sm-none"><i class="mdi mdi-book-open-page-variant"></i></span>
                                    <span class="d-none d-sm-block"><i class="mdi mdi-book-open-page-variant"></i> Portada
                                        <i class="mdi mdi-check-circle text-success" id="portada_ok"></i>
                                    </span>

                                </a>
                            </li>
                            <li class="nav-item p-1 col-3" id="liAntecedentes">
                                <a class="nav-link border" data-toggle="tab" href="#divAntecedentes" role="tab" id="divAntecedentes-tab">
                                    <span class="d-block d-sm-none"><i class="mdi mdi-archive-outline"></i></span>
                                    <span class="d-none d-sm-block "><i class="mdi mdi-archive-outline"></i> Antecedentes
                                    <i class="mdi mdi-check-circle text-success" id="antecedentes_ok"></i>
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item p-1 col-3 overflow-hidden" id="liCaracteristicasUrbanas">
                                <a class="nav-link border" data-toggle="tab" href="#divCaracteristicasUrbanas" role="tab" id="divCarUrb-tab">
                                    <span class="d-block d-sm-none"><i class="mdi mdi-city-variant-outline"></i></span>
                                    <span class="d-none d-sm-block "><i class="mdi mdi-city-variant-outline"></i> Características Urbanas <i class="mdi mdi-check-circle text-success" id="car_urb_ok"></i></span>
                                </a>
                            </li>
                            <li class="nav-item p-1 col-3" id="liCaracteristicasTerreno">
                                <a class="nav-link border" data-toggle="tab" href="#divCaracteristicasTerreno" role="tab" id="divCarTerr-tab">
                                    <span class="d-block d-sm-none"><i class="mdi mdi-vector-rectangle"></i></span>
                                    <span class="d-none d-sm-block "><i class="mdi mdi-vector-rectangle"></i> Características de Terreno <i class="mdi mdi-check-circle text-success" id="car_terr_ok"></i></span>
                                </a>
                            </li>
                            <li class="nav-item p-1 col-3" id="liDescripcionGeneral">
                                <a class="nav-link border" data-toggle="tab" href="#divDescripcionGeneral" role="tab" id="divDescGen-tab">
                                    <span class="d-block d-sm-none"><i class="mdi mdi-home-outline"></i></span>
                                    <span class="d-none d-sm-block "><i class="mdi mdi-home-outline"></i> Desc. Gen. del Inmueble <i class="mdi mdi-check-circle text-success" id="desc_gen_ok"></i></span>
                                </a>
                            </li>
                            <li class="nav-item p-1 col-3" id="liElementosConstruccion">
                                <a class="nav-link border" data-toggle="tab" href="#divElementosConstruccion" role="tab" id="divElemConst-tab">
                                    <span class="d-block d-sm-none"><i class="mdi mdi-wall"></i></span>
                                    <span class="d-none d-sm-block "><i class="mdi mdi-wall"></i> Elementos Construcción <i class="mdi mdi-check-circle text-success" id="elem_const_ok"></i></span>
                                </a>
                            </li>
                            <li class="nav-item p-1 col-3" id="liAvaluoFisico">
                                <a class="nav-link border" data-toggle="tab" href="#divAvaluoFisico" role="tab" id="divAvaluoFisico-tab" onclick="">
                                    <span class="d-block d-sm-none"><i class="mdi mdi-square-inc-cash"></i></span>
                                    <span class="d-none d-sm-block "><i class="mdi mdi-square-inc-cash"></i> Avalúo Físico <i class="mdi mdi-check-circle text-success" id="av_fisico_ok"></i></span>
                                </a>
                            </li>
                            <li class="nav-item p-1 col-3" id="liConclusion">
                                <a class="nav-link border" data-toggle="tab" href="#divConclusion" role="tab" id="divConclusion-tab">
                                    <span class="d-block d-sm-none"><i class="mdi mdi-post-outline"></i></span>
                                    <span class="d-none d-sm-block "><i class="mdi mdi-post-outline"></i> Conclusión <i class="mdi mdi-check-circle text-success" id="conclusion_ok"></i></span>
                                </a>
                            </li>
                            <li class="nav-item p-1 col-3" id="liFoto">
                                <a class="nav-link border" data-toggle="tab" href="#divFoto" role="tab" id="divReporteFoto-tab">
                                    <span class="d-block d-sm-none"><i class="mdi mdi-image-multiple"></i></span>
                                    <span class="d-none d-sm-block "><i class="mdi mdi-image-multiple"></i> Reporte Fotográfico <i class="mdi mdi-check-circle text-success" id="reporte_foto_ok"></i></span>
                                </a>
                            </li>
                            <li class="nav-item p-1 col-3" id="liReporte">
                                <a class="nav-link border" data-toggle="tab" href="#divReporte" role="tab" id="divImprimir-tab">
                                    <span class="d-block d-sm-none"><i class="mdi mdi-printer-check"></i></span>
                                    <span class="d-none d-sm-block "><i class="mdi mdi-printer-check"></i> Imprimir <i class="mdi mdi-check-circle text-success hide" id="imprimir_ok" style="display:hidden;"></i></span>
                                </a>
                            </li>
                            <li class="nav-item p-1 col-3" id="liAutorizar">
                                <a class="nav-link border" data-toggle="tab" href="#divAutorizar" role="tab" id="diva-tab">
                                    <span class="d-block d-sm-none"><i class="mdi mdi-account-multiple-check-outline"></i></span>
                                    <span class="d-none d-sm-block "><i class="mdi mdi-account-multiple-check-outline"></i> Autorizar</span>
                                </a>
                            </li>
                            <?php if ($tipo == 'revision') : ?>
                                <li class="nav-item p-1 col-3" id="liRevision">
                                    <a class="nav-link border" data-toggle="tab" href="#divRevision" role="tab">
                                        <span class="d-block d-sm-none"><i class="mdi mdi-format-list-checks"></i></span>
                                        <span class="d-none d-sm-block "><i class="mdi mdi-format-list-checks"></i> Revisión</span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="divPortada" aria-labelledby="home-tab">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group col-lg-1">
                                                <label for="txtMpio">Municipio</label>
                                                <input id="MUNI" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important; color:#480912; font-weight:550;" />
                                            </div>
                                            <div class="form-group col-lg-1">
                                                <label for="txtPobl">Poblacíon</label>
                                                <input id="POBL" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important; color:#480912; font-weight:550;" />
                                            </div>
                                            <div class="form-group col-lg-1">
                                                <label for="txtCtel">Cuartel</label>
                                                <input id="CTEL" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important; color:#480912; font-weight:550;" />
                                            </div>
                                            <div class="form-group col-lg-1">
                                                <label for="txtManz">Manzana</label>
                                                <input id="MANZ" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important; color:#480912; font-weight:550;" />
                                            </div>
                                            <div class="form-group col-lg-1">
                                                <label for="txtPred">Predio</label>
                                                <input id="PRED" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important; color:#480912; font-weight:550;" />
                                            </div>
                                            <div class="form-group col-lg-1">
                                                <label for="txtUnid">Unidad</label>
                                                <input id="UNID" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important; color:#480912; font-weight:550;" />
                                            </div>
                                            <div class="form-group col-lg-2">
                                                <label for="txtInfCat">Inf. Catastral</label>
                                                <input id="txtInfCat" type="text" class="form-control text-primary" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important;  font-weight:500;" value="<?php echo $info; ?>" />
                                            </div>
                                            <div class="form-group col-lg-2">
                                                <label for="txtFecha">Última modificación</label>
                                                <input id="txtFecha" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important; font-size:smaller !important; ">
                                            </div>
                                            <div class="form-group col-lg-2">
                                                <label for="txtFechaModificacion">Fecha autorización</label>
                                                <input id="txtFechaModificacion" type="text" class="form-control small" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important; font-size:smaller !important;" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-lg-8">
                                                <label for="txtPropietario">Propietario</label>
                                                <input id="txtPropietario" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important; font-weight:500" />
                                            </div>
                                            <div class="form-group col-lg-2">
                                                <label for="txtTipoPersona">Régimen Fiscal</label>
                                                <input id="txtTipoPersona" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important; font-weight:500" />
                                            </div>
                                            <div class="form-group col-lg-2">
                                                <label for="txtTipoPred">Tipo Predio</label>
                                                <input id="txtTipoPred" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important; font-weight:500;" value="URBANO" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtInmueble">Inmueble</label><label style="color: red;">*</label>
                                            <input id="txtInmueble" type="text" class="form-control" placeholder="" required="required" />
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-lg-8">
                                                <label for="txtCalle">Calle (Escriba el nombre de la calle de nuevo)</label><label style="color: red;">*</label>
                                                <div class="input-group">
                                                    <input id="txtCalle" type="text" class="form-control" placeholder="" required="required" />
                                                    <div class="input-group-btn" style="width: 13%;">
                                                        <input id="txtCalleCVE" type="text" class="form-control" placeholder="" required="required" readonly="readonly" style="background-color:  #F0F0F0!important;" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="txtNumero">Numero</label><label style="color: red;">*</label>
                                                <input id="txtNumero" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-lg-5">
                                                <label for="txtColonia">Colonia (Escriba el nombre de la colonia de nuevo)</label><label style="color: red;">*</label>
                                                <div class="input-group">
                                                    <input id="txtColonia" type="text" class="form-control" placeholder="" required="required" />
                                                    <div class="input-group-btn" style="width: 17%;">
                                                        <input id="txtColoniaCVE" type="text" class="form-control" placeholder="" required="required" readonly="readonly" style="background-color:  #F0F0F0!important;" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-5">
                                                <label for="txtMunicipio">Municipio</label><label style="color: red;">*</label>
                                                <input id="txtMunicipio" type="text" class="form-control" placeholder="" required="required" value="CULIACAN" />
                                            </div>
                                            <div class="form-group col-lg-2">
                                                <label for="txtCP">Codigo Postal</label><label style="color: red;">*</label>
                                                <input id="txtCP" type="number" step="1" max="99999" class="form-control" placeholder="" required="required" maxlength="5" />
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="form-group col-lg-12">
                                                <label for="txtNotaInterna">Nota Interna</label>
                                                <textarea id="txtNotaInterna" class="form-control" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--entab-->
                            <div class="tab-pane fade in" id="divAntecedentes">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group col-lg-12">
                                                <label for="txtSolicitante">Solicitante</label><label style="color: red;">*</label>
                                                <input id="txtSolicitante" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-lg-2">
                                                <label for="txtFechaInspeccion">Fecha Inspección</label><label style="color: red;">*</label>
                                                <input id="txtFechaInspeccion" type="date" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <label for="selObjectoAvaluo">Objeto</label>
                                                <select id="selObjectoAvaluo" class="form-control" disabled="disabled" style="background-color:  #F0F0F0!important;">
                                                    <option value="TRASLADO DE DOMINIO" selected="selected">TRASLADO DE DOMINIO</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <label for="txtRegimen">Regimen</label><label style="color: red;">*</label>
                                                <select name="ctl00$ContentPlaceHolder1$txtRegimen" id="ContentPlaceHolder1_txtRegimen" class="form-control" required="required">
                                                    <option value="EJIDAL">EJIDAL</option>
                                                    <option value="PARTICULAR">PARTICULAR</option>
                                                    <option value="PARTICULAR (POSESION)">PARTICULAR (POSESION)</option>
                                                    <option value="PARTICULAR (PROPIEDAD)">PARTICULAR (PROPIEDAD)</option>
                                                    <option value="REGIMEN CONDOMINAL VERTICAL">REGIMEN CONDOMINAL VERTICAL</option>
                                                    <option value="REGIMEN CONDOMINAL HORIZONTAL">REGIMEN CONDOMINAL HORIZONTAL</option>
                                                    <option value="REGIMEN MUNICIPAL">REGIMEN MUNICIPAL</option>
                                                    <option value="FEDERAL">FEDERAL</option>
                                                    <option value="FEDERAL(VIAS COM. Y CANALES)">FEDERAL(VIAS COM. Y CANALES)</option>
                                                    <option value="FEDERAL(TEMPLOS)">FEDERAL(TEMPLOS)</option>
                                                    <option value="FEDERAL(BENEF. PUBLICA)">FEDERAL(BENEF. PUBLICA)</option>
                                                    <option value="FEDERAL(ESCUELAS)">FEDERAL(ESCUELAS)</option>
                                                    <option value="FEDERAL(VIV. POPULARES)">FEDERAL(VIV. POPULARES)</option>
                                                    <option value="MUNICIPAL">MUNICIPAL</option>
                                                    <option value="MUNICIPAL(VIAS COM. Y CANALES)">MUNICIPAL(VIAS COM. Y CANALES)</option>
                                                    <option value="MUNICIPAL(ESCUELAS)">MUNICIPAL(ESCUELAS)</option>
                                                    <option value="MUNICIPAL(VIV. POPULARES)">MUNICIPAL(VIV. POPULARES)</option>
                                                    <option value="ESTATAL">ESTATAL</option>
                                                    <option value="ESTATAL(VIAS COM. Y CANALES)">ESTATAL(VIAS COM. Y CANALES)</option>
                                                    <option value="ESTATAL(BENEF. PUBLICA)">ESTATAL(BENEF. PUBLICA)</option>
                                                    <option value="ESTATAL(ESCUELAS)">ESTATAL(ESCUELAS)</option>
                                                    <option value="ESTATAL(VIV. POPULARES)">ESTATAL(VIV. POPULARES)</option>
                                                    <option value="FEDERAL(USO PRIVADO)">FEDERAL(USO PRIVADO)</option>
                                                    <option value="MUNICIPAL(USO PRIVADO)">MUNICIPAL(USO PRIVADO)</option>
                                                    <option value="ESTATAL(USO PRIVADO)">ESTATAL(USO PRIVADO)</option>
                                                    <option value="OTROS">OTROS</option>
                                                    <option value="MUNICIPAL/ESTATAL/FEDERAL">MUNICIPAL/ESTATAL/FEDERAL</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="txtRegPubPro">Registro Público de Propiedad</label>
                                                <input id="txtRegPubPro" type="text" class="form-control" placeholder="" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-lg-4">
                                                <label for="txtInscripcion">Inscripcion</label>
                                                <input id="txtInscripcion" type="text" class="form-control" placeholder="" />
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="txtLibro">Libro</label>
                                                <input id="txtLibro" type="text" class="form-control" placeholder="" />
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="txtSeccion">Seccion</label>
                                                <input id="txtSeccion" type="text" class="form-control" placeholder="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--endtab-->
                            <div class="tab-pane fade in" id="divCaracteristicasUrbanas">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group col-lg-4">
                                                <label for="txtClasificacionZona">Clasificacion de la Zona<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label>

                                                <input type="text" class="form-control" id="clasificacion_zonas" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="txtTipoConstruccionDominante">Tipo de Construccion Dominante<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label>

                                                <input type="text" class="form-control" id="construccion_dominante" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="txtIndiceSaturacionZona">Indice Saturacion Zona<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label>

                                                <input type="text" class="form-control" id="saturacion_zona" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="txtPoblacion">Poblacion<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label>

                                                <input type="text" class="form-control" id="list_poblacion" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="txtContaminacionAmbiental">Contaminación Ambiental</label>
                                                <input id="txtContaminacionAmbiental" type="text" class="form-control" placeholder="Escribir Contaminación" />

                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="txtUsoSueloPermitido">Uso Suelo Permitido<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label>
                                                <input type="text" class="form-control" id="suelo_permitido" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-12">
                                                <label for="txtViaAccesoImportancia">Vías de acceso e importancia</label>
                                                <input id="txtViaAccesoImportancia" type="text" class="form-control" placeholder="Escribir Vías de acceso" />
                                            </div>
                                        </div>
                                        <h5 style="color:#480912; text-transform:uppercase; font-weight:600; text-align:center;">Servicios públicos y equipamiento inmobiliario</h5>
                                        <hr />
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="custom-control custom-switch mb-2">

                                                    <input type="checkbox" class="custom-control-input" id="chkAgua">
                                                    <label class="custom-control-label" for="chkAgua" style="cursor:pointer; ">Agua Potable</label>
                                                </div>

                                            </div>
                                            <div class="col-lg-3">
                                                <div class="custom-control custom-switch mb-2">
                                                    <input type="checkbox" class="custom-control-input" id="chkDrenaje">
                                                    <label class="custom-control-label" for="chkDrenaje" style="cursor:pointer; ">Drenaje</label>
                                                </div>

                                            </div>
                                            <div class="col-lg-3">
                                                <div class="custom-control custom-switch mb-2">
                                                    <input type="checkbox" class="custom-control-input" id="chkElectricidad">
                                                    <label class="custom-control-label" for="chkElectricidad" style="cursor:pointer; ">Energia Electrica</label>
                                                </div>

                                            </div>
                                            <div class="col-lg-3">
                                                <div class="custom-control custom-switch mb-2">
                                                    <input type="checkbox" class="custom-control-input" id="chkAlumbrado">
                                                    <label class="custom-control-label" for="chkAlumbrado" style="cursor:pointer; ">Alumbrado Público</label>
                                                </div>

                                            </div>
                                            <div class="col-lg-3">

                                                <div class="custom-control custom-switch mb-2">
                                                    <input type="checkbox" class="custom-control-input" id="chkBasura">
                                                    <label class="custom-control-label" for="chkBasura" style="cursor:pointer; ">Recoleccion de Basura</label>
                                                </div>

                                            </div>
                                            <div class="col-lg-3">

                                                <div class="custom-control custom-switch mb-2">
                                                    <input type="checkbox" class="custom-control-input" id="chkTransporte">
                                                    <label class="custom-control-label" for="chkTransporte" style="cursor:pointer; ">Transporte Público</label>
                                                </div>

                                            </div>
                                            <div class="col-lg-3">

                                                <div class="custom-control custom-switch mb-2">
                                                    <input type="checkbox" class="custom-control-input" id="chkTelefono">
                                                    <label class="custom-control-label" for="chkTelefono" style="cursor:pointer; ">Telefono</label>
                                                </div>

                                            </div>
                                            <div class="col-lg-3">

                                                <div class="custom-control custom-switch mb-2">
                                                    <input type="checkbox" class="custom-control-input" id="chkBanqueta">
                                                    <label class="custom-control-label" for="chkBanqueta" style="cursor:pointer; ">Banqueta</label>
                                                </div>

                                            </div>
                                            <div class="col-lg-3">

                                                <div class="custom-control custom-switch mb-2">
                                                    <input type="checkbox" class="custom-control-input" id="chkGuarniciones">
                                                    <label class="custom-control-label" for="chkGuarniciones" style="cursor:pointer; ">Guarniciones</label>
                                                </div>

                                            </div>
                                            <div class="col-lg-3">

                                                <div class="custom-control custom-switch mb-2">
                                                    <input type="checkbox" class="custom-control-input" id="chkPavimento">
                                                    <label class="custom-control-label" for="chkPavimento" style="cursor:pointer; ">Pavimento</label>
                                                </div>

                                            </div>
                                            <div class="col-lg-3">

                                                <div class="custom-control custom-switch mb-2">
                                                    <input type="checkbox" class="custom-control-input" id="chkVigilancia">
                                                    <label class="custom-control-label" for="chkVigilancia" style="cursor:pointer; ">Vigilancia Policiaca</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--endtab-->
                            <div class="tab-pane fade in" id="divCaracteristicasTerreno">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row ">
                                            <div class="form-group col-lg-4">
                                                <label for="txtSupEscrituras">Superficie del terreno segun escrituras</label><label style="color: red;">*</label>
                                                <input id="txtSupEscrituras" type="number" step="0.01" class="form-control" placeholder="" required="required" />
                                            </div>

                                            <div class="form-group col-lg-4">
                                                <label for="txtSupConst">Superficie del terreno segun catastro</label>
                                                <input id="txtSupConst" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important;" />
                                            </div>

                                            <div class="form-group col-lg-4">
                                                <label for="txtSupLevantamiento" style="font-size:small">Superficie del terreno segun levantamiento topográfico</label>
                                                <input id="txtSupLevantamiento" type="text" class="form-control" placeholder="" />
                                            </div>
                                        </div>
                                        <hr />
                                        <h5 style="color:#480912; text-transform:uppercase; font-weight:600; text-align:center;">Medidas y colindancias según escrituras</h5>
                                        <hr />
                                        <div class="row ">
                                            <div class="col-lg-6">
                                                <div class="form-group ">
                                                    <label for="selectOrientacion">Orientación</label>
                                                    <input type="text" class="form-control" id="selectOrientacion" style="cursor:pointer;" placeholder="Seleccionar una Orientación">
                                                    
                                                    <label for="txtOrientacion" class="mt-2">Descripción</label><label style="color: red;">*</label>
                                                    <div class="input-group">
                                                        <input id="txtOrientacion" type="text" class="form-control" placeholder="" required="required" />
                                                        <div class="input-group-append">
                                                            <button id="botonAgregarOrientacion" class="btn btn-success"><span class="mdi mdi-plus"></span></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <table id="tablaOrientacion" class="table table-condensed">
                                                </table>
                                            </div>
                                            <div class="col-lg-6">
                                                <h6 style="color:#480912; text-transform:uppercase; font-weight:600">Clasificación de las construcciones</h6>
                                                <table id="tablaConstrucciones" class="table">
                                                </table>
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="row ">
                                            <div class="form-group col-lg-6">
                                                <label for="txtTopografiaConst">Topografía y configuración</label>
                                                <input id="txtTopografiaConst" type="text" class="form-control" placeholder="" />
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <label for="txtNumeroFrentes">Número de frentes</label><label style="color: red;">*</label>
                                                <input id="txtNumeroFrentes" type="number" class="form-control" placeholder="" required="required" />
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <label for="txtCaracteristicasPan">Características panorámicas</label>
                                                <input id="txtCaracteristicasPan" type="text" class="form-control" placeholder="" />
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <label for="txtDensidadHabitacional">Densidad habitacional</label>
                                                <input id="txtDensidadHabitacional" type="text" class="form-control" placeholder="" />
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <label for="txtIntensidadConstruccion">Intensidad de construcción</label>
                                                <input id="txtIntensidadConstruccion" type="text" class="form-control" placeholder="" />
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <label for="txtServidumbresConstrucciones">Servidumbres y/o restricciones</label>
                                                <input id="txtServidumbresConstrucciones" type="text" class="form-control" placeholder="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- endtab -->
                            <div class="tab-pane fade in" id="divDescripcionGeneral">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row ">
                                            <div class="form-group col-lg-12">
                                                <label for="txtDescGeneralInmueble">Descripción General del Inmueble</label><label style="color: red;">*</label>
                                                <textarea id="txtDescGeneralInmueble" class="form-control" placeholder="" required="required" rows="3"></textarea>
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <label for="selUso">Uso actual</label>
                                                <select id="selUso" class="form-control">
                                                </select>
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <label for="txtNumeroNiveles">Numero de niveles</label><label style="color: red;">*</label>
                                                <input id="txtNumeroNiveles" type="number" class="form-control" placeholder="" required="required" />
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <label for="txtEdadAprox">Edad aproximada</label><label style="color: red;">*</label>
                                                <input id="txtEdadAprox" type="number" class="form-control" placeholder="" required="required" />
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <label for="txtEstadoConservacion">Estado de conservacion</label><label style="color: red;">*</label>
                                                <input id="txtEstadoConservacion" type="text" class="form-control" placeholder="" required="required" />
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <label for="txtCalidadProyecto">Calidad del proyecto</label><label style="color: red;">*</label>
                                                <input id="txtCalidadProyecto" type="text" class="form-control" placeholder="" required="required" />
                                            </div>

                                            <div class="form-group col-lg-6">
                                                <label for="txtUnidadesRentables">Unidades Rentables</label><label style="color: red;">*</label>
                                                <input id="txtUnidadesRentables" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--endtab -->
                            <div class="tab-pane fade in" id="divElementosConstruccion">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label for="checkBaldio">CHECK AQUI SI ES BALDÍO</label><br>
                                                <input type="checkbox" id="checkBaldio" switch="none" />
                                                <label for="checkBaldio" data-on-label="SI" data-off-label="NO"></label>
                                                <hr />
                                            </div>
                                            <div class="col-lg-12">
                                                <h5 style="color:#480912; text-transform:uppercase; font-weight:600; text-align:center;">Obra negra o gruesa</h5>
                                                <hr />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtCimientos">Cimientos<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_cimientos" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtEstructura">Estructura</label><label style="color: red;">*</label>
                                                <input id="txtEstructura" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtMuros">Muros<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_muros" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtEntrepisos">Entrepisos</label><label style="color: red;">*</label>
                                                <input id="txtEntrepisos" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtTechos">Techos<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_techos" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtAzoteas">Azoteas</label><label style="color: red;">*</label>
                                                <input id="txtAzoteas" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtBardas">Bardas</label><label style="color: red;">*</label>
                                                <input id="txtBardas" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="col-lg-12">
                                                <hr />
                                                <h5 style="color:#480912; text-transform:uppercase; font-weight:600; text-align:center;">Revestimientos y acabados interiores</h5>
                                                <hr />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtAplanadosInteriores">Aplanados interiores<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_aplanados" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtAplanadosExteriores">Aplanados exteriores</label><label style="color: red;">*</label>
                                                <input id="txtAplanadosExteriores" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtPlafones">Plafones</label><label style="color: red;">*</label>
                                                <input id="txtPlafones" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtLambrines">Lambrines<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_lambrines" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtPisos">Pisos<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_pisos" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtZoclos">Zoclos</label><label style="color: red;">*</label>
                                                <input id="txtZoclos" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtEscaleras">Escaleras</label><label style="color: red;">*</label>
                                                <input id="txtEscaleras" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtPintura">Pintura<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_pintura" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtRecubrimientosEspeciales">Recubrimientos Especiales</label><label style="color: red;">*</label>
                                                <input id="txtRecubrimientosEspeciales" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="col-lg-12">
                                                <hr />
                                                <h5 style="color:#480912; text-transform:uppercase; font-weight:600; text-align:center;">Carpinteria</h5>
                                                <hr />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtcPuertas">Puertas<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_puertas" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtClosets">Closets</label><label style="color: red;">*</label>
                                                <input id="txtClosets" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="col-lg-12">
                                                <hr />
                                                <h5 style="color:#480912; text-transform:uppercase; font-weight:600; text-align:center;">Instalaciones hidráulicas y sanitarias</h5>
                                                <hr />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtMueblesBaño">Muebles de baño<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_mueblesb" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtEquipoCocina">Equipo de cocina</label><label style="color: red;">*</label>
                                                <input id="txtEquipoCocina" type="text" class="form-control" placeholder="" required="required" />
                                            </div>
                                            <div class="col-lg-12">
                                                <hr />
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtInstalacionesElectricas">Instalaciones electricas<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_instalacionese" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtHerreria">Herreria<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_herreria" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtVidrieria">Vidrieria<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_vidrieria" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtCerrajeria">Cerrajeria<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_cerrajeria" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtFachadas">Fachadas<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_fachadas" style="cursor:pointer;">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="txtInstalacionesEspeciales">Inst. especiales, elementos accesorios y obras complementarias<span class="mdi mdi-chevron-down" aria-hidden="true"></span></label><label style="color: red;">*</label>

                                                <input type="text" class="form-control" id="list_especiales" style="cursor:pointer;">
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--endtab -->
                            <div class="tab-pane fade in" id="divAvaluoFisico">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-lg-12">

                                                <table id="tablaFisicoTerreno" class="table table-hover" width="100%">
                                                    <thead style="color: #480912;">
                                                        <tr>
                                                            <th colspan="5">a) TERRENO</th>
                                                        </tr>
                                                        <tr class="text-white" style="background-color: #480912;">
                                                            <th>Superficie Mt2</th>
                                                            <th>Valor unitario Mt2</th>
                                                            <th>Factor</th>
                                                            <th>Motivo</th>
                                                            <th>Valor terreno</th>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr style="color: #480912;">

                                                            <th class="font-medium" style="text-align:left;">Superficie Total: <span id="txtsuptotalterreno"></span></th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th colspan="2" class="font-medium text-right">Valor Total: <span id="txtvalortotalterreno">$0.0</span></th>
                                                            
                                                        </tr>
                                                    </tfoot>
                                                </table>

                                        </div>
                                        <div class="col-lg-12">
                                                <table class="table table-hover mt-5" id="tablaFisicoConstrucciones" class="table table-hover" width="100%">
                                                    <thead style="color: #480912;">
                                                        <tr>
                                                            <th colspan="9">b) CONSTRUCCIONES
                                                                <span class="float-right">
                                                                    <a href="images/DEMERITOS_CONST_CATALOGO2.xlsx" id="verTablaDemerito" target="_blank" class="pull-right ml-1" style="color: green; font-style: italic; margin-right: 30px;">Ver tabla de equivalencias del F.Dem.</a> <a href="#" id="aVerPlano" target="_blank" class="pull-right">Ver Plano</a>
                                                                    <button id="botonSubirPlano" disabled="disabled" class="editaTerr btn btn-primary pull-right btn-sm ml-1"><span class="mdi mdi-cloud-upload"></span>&nbsp; Subir Plano de Construcción</button>
                                                                    <input type="file" id="fileUploadPlano" lang="es" style="display: none" />
                                                                    <button onclick="return agregarConstruccion()" class="editaTerr btn btn-success pull-right btn-sm ml-1"><span class="mdi mdi-plus"></span></button>
                                                                    
                                                                </span>
                                                            </th>
                                                        </tr>
                                                        <tr class="text-white" style="background-color: #480912;">
                                                            <th>Cat</th>
                                                            <th>Superficie M2</th>
                                                            <th>Valor Unitario</th>
                                                            <th>Estado</th>
                                                            <th>Edad</th>
                                                            <th>F.Dem.</th>
                                                            <th>F.Com.</th>
                                                            <th>Valor Unitario</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr style="color: #480912;">
                                                            <th colspan="2">Superficie Total: <span id="txtsuptotalconst"></span></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th colspan="5" class="text-right">Valor Total: <span id="txtvalortotalconst">$0.0</span></th>
                                                            
                                                        </tr>
                                                    </tfoot>
                                                </table>

                                        </div>
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <table id="tablaEspeciales" class="table table-hover" width="100%">
                                                    <thead style="color: #480912;">
                                                        <tr>
                                                            <th colspan="8">c) INSTALACIONES ESPECIALES, ELEMENTOS ACCESORIOS Y OBRAS COMPLEMENTARIAS

                                                            <span class="float-right">
                                                                  
                                                                    <button onclick="return agregarEspeciales()" class="editaTerr btn btn-success pull-right btn-sm ml-1"><span class="mdi mdi-plus"></span></button>
                                                                    
                                                                </span>
                                                            </th>
                                                        </tr>
                                                        <tr class="text-white" style="background-color: #480912;">
                                                            <th>Concepto</th>
                                                            <th>Unidad</th>
                                                            <th>Cantidad</th>
                                                            <th>V.N.R. Unit.</th>
                                                            <th>% Demerito</th>
                                                            <th>V.R.N. Unitario</th>
                                                            <th>Valor Parcial</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr style="color: #480912;">
                                                            <th colspan="5"></th>
                                                            <th colspan="3" class="font-medium text-right">Valor Total: <span id="txtvalortotalespeciales">$0.0</span></th>
                                                            
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                               
                                            </div>
                                            <div class="row right">
                                                <div class="form-group col-lg-8">
                                                </div>
                                                <div class="form-group col-lg-4">
                                                    <label for="txtValorFisico2">Valor físico o directo</label>
                                                    <input id="txtValorFisico2" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important;text-align: right;font-size:20px;" value="$0.0"/>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--endtab-->
                            <div class="tab-pane fade in" id="divConclusion">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-lg-12">
                                            <h5 style="color:#480912; text-transform:uppercase; font-weight:600">Resumen de los valores obtenidos</h5>
                                            <hr />
                                        </div>
                                        <div class="row ">
                                            <div class="form-group col-lg-6">
                                                <label for="txtValorFisico">Valor físico o directo</label>
                                                <div class="input-group">
                                                <input id="txtValorFisico" type="text" class="form-control" placeholder="" disabled="disabled" style="background-color:  #F0F0F0!important;text-align: right;" />
                                                <span class="input-group-btn input-group-append">
                                                <button id="btnRedondear" class="btn btn-primary">Redondear</button></span></div>
                                                <button id="btnVerBitacora" class="btn btn-warning" style="display:none;">Bitácora</button>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;">
                                            <div class="form-group col-lg-3">
                                                <label for="txtValorMercado">Valor de mercado</label><label style="color: red;">*</label>
                                                <input id="txtValorMercado" type="text" class="form-control" placeholder="" required="required" style="text-align: right" />
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <hr />
                                            <label for="txtConsideraciones">Consideraciones previas a la conclusion</label><label style="color: red;">*</label>
                                            <hr />
                                        </div>
                                        <div class="row ">
                                            <div class="form-group col-lg-12">
                                                <textarea id="txtConsideraciones" class="form-control" rows="5" required="required"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- endtab -->
                            <div class="tab-pane fade in" id="divFoto">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-lg-12">
                                            <h5 style="color:#480912; text-transform:uppercase; font-weight:600">Reporte fotográfico - agregue tambien el plano de construción en formato JPG</h5>
                                            <hr />
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button class="editaTerr btn btn-success" onclick="return agregarImagen()"><span class="mdi mdi-cloud-upload"></span>  Agregar imagen</button>
                                                <hr />
                                                <input type="file" id="fileUpload" lang="es" accept="image/*" style="display: none" />
                                            </div>
                                        </div>
                                        <div class="row" id="divImages">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--endtab-->
                            <div class="tab-pane fade in" id="divReporte">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-lg-12">
                                            <h5 style="color:#480912; text-transform:uppercase; font-weight:600">Imprimir Avalúo</h5>
                                            <hr />
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button id="botonImprimirAvaluo" class="btn btn-success"><span class="mdi mdi-printer"></span>Imprimir</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--endtab-->
                            <div class="tab-pane fade in" id="divAutorizar">
                                <div id="divFirmaPer" class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" style="color: black" href="#collapse1">Firma Electrónica Perito</a>
                                    </div>
                                    <div id="collapse1" class="panel-body panel-collapse">
                                        <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                <label for="txtSerie">Nombre</label>
                                                <input id="txtNombrePer" class="form-control" disabled="disabled" style="background-color:  #F0F0F0!important;" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label for="fileCerPer">Certificado (cer)*</label><label style="color: red;">*</label>
                                                    <input type="file" id="fileCerPer" lang="es" class="form-control" required="required" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label for="fileKeyPer">Clave privada (key)*</label><label style="color: red;">*</label>
                                                    <input type="file" id="fileKeyPer" lang="es" class="form-control" required="required" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <label for="txtPAssKeyPer">Contraseña de clave privada*</label><label style="color: red;">*</label>
                                                    <input id="txtPAssKeyPer" type="password" class="form-control" placeholder="" required="required" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                <button id="btnFirmarPer" class="btn btn-primary btn-block btnFirmar">Firmar <i class="mdi mdi-certificate"></i></button>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                        <div class="col-md-6">
                                            <label for="txtRFCPer">RFC</label>
                                            <input id="txtRFCPer" class="form-control" disabled="disabled" style="background-color:  #F0F0F0!important;" />
                                           
                                            <label for="txtSerie">Serie del Certificado</label>
                                            <input id="txtSerie" class="form-control" disabled="disabled" style="background-color:  #F0F0F0!important;" />
                                            <label for="txtSello">Sello Digital</label>
                                            <textarea id="txtSello" class="form-control" rows="11" disabled="disabled" style="background-color:  #F0F0F0!important;"></textarea>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!--endtab -->
                            <div class="tab-pane fade in" id="divRevision">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <label>
                            Perito:
                            </label>
                                            <label id="labelNomPerito"></label>
                                            <hr>
                                            <label>
                            Revision:
                            </label>
                                            <label id="labelRevision">0</label>
                                            <hr>
                                            <div id="divReingreso" class="hide">
                                                <label>
                            Re-Ingreso:
                            </label>
                                                <label id="labelReIngreso">0</label>
                                                <hr>
                                                <label>
                            Fecha Avaluo Anterior:
                            </label>
                                                <label id="labelFechaAnterior">
                            </label>
                                                <hr>
                                                <label>
                            Perito:
                            </label>
                                                <label id="labelPeritoAnt">
                            </label>
                                                <hr>
                                                <label>
                            ValorAutorizado:
                            </label>
                                                <label id="labelValorAutorizadoAnt">
                            </label>
                                            </div>
                                            <hr>
                                        </div>
                                        <div class="row">
                                            <table class="table table-responsive table-hover">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Actual
                                                        </th>
                                                        <th>Nuevo
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <label>Superficie Terreno</label>
                                                        </td>
                                                        <td>
                                                            <label id="supTerrRev1">0.00</label>
                                                        </td>
                                                        <td>
                                                            <label id="supTerrRev2">0.00</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Superficie Construcción</label>
                                                        </td>
                                                        <td>
                                                            <label id="supConstRev1">0.00</label>
                                                        </td>
                                                        <td>
                                                            <label id="supConstRev2">0.00</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Valor Terreno</label>
                                                        </td>
                                                        <td>
                                                            <label id="valTerrRev1">$0.00</label>
                                                        </td>
                                                        <td>
                                                            <label id="valTerrRev2">$0.00</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Valor Construcción</label>
                                                        </td>
                                                        <td>
                                                            <label id="valConstRev1">$0.00</label>
                                                        </td>
                                                        <td>
                                                            <label id="valConstRev2">$0.00</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>TOTAL</label>
                                                        </td>
                                                        <td>
                                                            <label id="valTOTALRev1">$0.00</label>
                                                        </td>
                                                        <td>
                                                            <label id="valTOTALRev2">$0.00</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Clasificacion de la construcción</label>
                                                        </td>
                                                        <td>
                                                            <table id="tablaConstrucciones2" class="table table-condensed table-responsive">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Categoria</th>
                                                                        <th>Superficie M2</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table id="tablaConstrucciones3" class="table table-condensed table-responsive">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Categoria</th>
                                                                        <th>Superficie M2</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Afecta</label>
                                                        </td>
                                                        <td>
                                                            <label id="aAfecta">-</label>
                                                        </td>
                                                        <td>
                                                            <a href="#" id="aPlano" target="_blank" style="color:gray">Descargar Plano de construcción</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Nota Interna</label>
                                                        </td>
                                                        <td colspan="2">
                                                            <label id="labelNotaInterna"></label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Consideraciones previas a la conclusión</label>
                                                        </td>
                                                        <td colspan="2">
                                                            <label id="labelConsideraciones">-</label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-lg-12">
                                            <label>Reporte fotográfico</label>
                                            <hr>
                                        </div>
                                        <div id="divImagesRevision">
                                           
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <hr>
                        <button id="botonGuardar" class="btn btn-md btn-success float-right waves-effect waves-light"><i class="mdi mdi-content-save-edit-outline"></i> GUARDAR Y CONTINUAR </button>
                    </div>

                </div>

            </div>

            <div class="col-md-12">
                <h4 class="card-title">Ubicación Geográfica del Predio</h4>
                <hr>
                <div id="map" class="map" style="height: 400px;"></div>
            </div>

        </div>

    </div>
    <!-- end row -->





</div>
<!-- End Page-content -->
</div>
<!-- end main content-->