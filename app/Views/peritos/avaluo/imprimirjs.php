<script src="<?php echo base_url("assets/js/jquery-ui-1.12.1.js"); ?>"></script>
<script src="<?php echo base_url("assets/js/peritos/utils.js")."?v".rand(); ?>" ></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js"); ?>"></script>

<script type="text/javascript">
var total_fisico = 0.0;
var catMpio;
$(document).ready(function () {
    var info = "<?php echo $info; ?>";
    //obtener portada
    cargar_portada(info);

    cargar_imagenes(info);

    cargar_info(info);

    cargar_antecedentes(info);

    cargar_car_urb(info);

    cargar_tabla_orientaciones(info);

    cargar_car_terreno(info);

    cargar_descripcion_general(info);

    cargar_elementos_construccion(info);

    cargar_sello(info);

    calculaTotal();

    $("#botonImprimir").click(function () {
        //$("#botonImprimir").hide();
        window.print();
        //$("#botonImprimir").show();
        return false;
    });
});

function cargar_info_perito(id) {
  $.ajax({
      url: "<?php echo base_url("peritos/obtener_perito_perfil"); ?>/"+id,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        //console.log(response);
        if (response.perfil.length > 0) {
          var json = response.perfil[0];
            console.log(json);
          $("#labelNombrePerito").text(json.nombre);
          $("#labelDireccionPerito").text(json.direccion);
          $("#labelTelefono").text(json.telefono);
          $("#labelCelular").text(json.celular);
          $("#labelCorreo").text(json.correo);

          $("#labelNombreC").text(json.nombre);
          $("#labelCedula").text(json.clave_registro);
          $("#labelEspecialidadP").text(json.especialidad);

          $("#labelPeritoValuador").text(json.nombre);
          $("#labelNumRegistro").text(json.clave_registro);
          $("#labelEspecialidad").text(json.especialidad);
        }

      }
    });
}

function cargar_sello(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_autorizacion_perito"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        console.log("SELLO" + response);
        if (response.autorizacion.length > 0) {
          var json = response.autorizacion[0];
          $("#labelSerie").text(json.serie_certificado);
          $("#labelSello1").text(json.sello_digital);
        }else {
        
        }
      }
    });
  }

function cargar_info(info) {
    var cve_cat = "";
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_info"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        //console.log(response);
        if (response.info.length > 0) {
          /*avaluo_id = response.info[0].id;
          //Clave catastral compuesta
          $('#MUNI').val(zeroPad(response.info[0].cve_mpio, 3));
          $('#POBL').val(zeroPad(response.info[0].cve_pobl, 3));
          $('#CTEL').val(zeroPad(response.info[0].num_ctel, 3));
          $('#MANZ').val(zeroPad(response.info[0].num_manz, 3));
          $('#PRED').val(zeroPad(response.info[0].num_pred, 3));
          $('#UNID').val(zeroPad(response.info[0].num_unid, 3));
          //fechas
          if (response.info[0].ultima_modificacion != null)
            $("#txtFecha").val(moment(response.info[0].ultima_modificacion).locale('es').format('L LTS'));
          if (response.info[0].fecha_autorizacion != null)
            $("#txtFechaModificacion").val(moment(response.info[0].fecha_autorizacion).locale('es').format('L LTS'));
*/
          var pobl = zeroPad(response.info[0].cve_pobl, 3);
          var ctel = zeroPad(response.info[0].num_ctel, 3);
          var manz = zeroPad(response.info[0].num_manz, 3);
          var pred = zeroPad(response.info[0].num_pred, 3);
          var unid = zeroPad(response.info[0].num_unid, 3);
        
          var cvecat = pobl + ctel + manz + pred + unid;
          var cvecatcomp = zeroPad(response.info[0].cve_mpio, 3) + cvecat;

          $("#labelClaveCatastral").text(cvecatcomp);

          $("#labelPeritoValuador").text(response.info[0].id_perito.toUpperCase());

          if(response.detalle.length > 0){
            $("#labelValorMercado").text(aPesos(parseFloat(response.detalle[0].valor_mercado)));
          
            $("#labelConsideraciones").text(response.detalle[0].consideraciones_previas);
          }

          //mostrar la info del perito
          cargar_info_perito(response.info[0].id_perito);
         


          
          //cargamos informacion de la clave catastral 
          $.ajax({
            url: "<?php echo base_url("peritos/validarcve"); ?>/" + cvecat,
            dataType: "json",
            success: function(data) {
              ////console.log(data.result);
              if (data.result.length > 0) {
                
                  //llenamos los datos correspondientes 
                  var prop = "";
                  if(data.result[0].NOM_PROP != null) { prop = prop + data.result[0].NOM_PROP; }
                  if(data.result[0].APE_PAT != null) { prop = prop + " " +data.result[0].APE_PAT; }
                  if(data.result[0].APE_MAT != null) { prop = prop + " " +data.result[0].APE_MAT; }
                  $("#labelPropietario").text(prop.toUpperCase());
                  $("#labelPropietarioInm").text(prop.toUpperCase());
                  $("#labelSuperficieCatastro").text(data.result[0].SUP_TERR);

                  if (response.Terr.length > 0) {
                    cargar_terreno_fisico(response.Terr);
                  } else { cargar_terreno(cvecat); }
                  
                  if (response.Const.length > 0) {
                    cargar_construcciones_fisico(response.Const);
                  } else { cargar_construcciones(cvecat); }

                  if (response.Esp.length > 0) {
                    cargar_especiales_fisico(response.Esp);
                  }
                  
                  $('#labelValorFisico').text(aPesos(total_fisico));
                  $('#labelValorFisico_2').text(aPesos(total_fisico));
                  
 
                  //Se obtiene el array del poligono 
                  jsonobj = jQuery.parseJSON(data.result[0].jsonb_build_object);
                  //si encuentra el poligono en el vectorial
                  if (jsonobj.features != null) {
                    //Se visualiza el poligono en el mapa
                    visualizar_capa(jsonobj);
                  } else {
                    
                  }


              } else {

              }
            }
          });
        }

        
      }

      
    });

  }

  function calcula_total_fisico(){
      var valterr = parseFloat($('#txtvalortotalterreno').text().replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
                  var valconst = parseFloat($('#txtvalortotalconst').text().replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
                  var valesp = parseFloat($('#txtvalortotalespeciales').text().replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
                  var val_fisico = valterr + valconst + valesp;
                  $("#labelValorFisico").text(aPesos(val_fisico));
                  $("#labelValorFisico_2").text(aPesos(val_fisico));
  }

  function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
  }

function cargar_imagenes(inf) {

            if (inf == undefined || inf == null) {
                inf = $("#labelAvaluo").text();
            }

            $.ajax({
                url: "<?php echo base_url("peritos/obtener_imagenes"); ?>/" + inf,
                dataType: "json",
                success: function(data) {
                    //console.log(data.result);
                    if (data.result.length > 0) {
                        var info = eval(data.result);
                        var imHtml = "";
                        var imHtmlAnexo = "";

                        $("#divRepfoto").html("");
                        $("#divRepfotoAnexo").html("");

                        var img = true;

                        $("#divIMGPortada").html("<div class='col-12 '><img class='' src='<?php echo base_url("documentos_peritos/img_reporte_fotografico");?>/"+inf+ "/" + info[0].url + "' style='width: 100%;'><div class='caption text-center'></div></div>");

                        for (var i = 0; i < info.length; i++) {
                            if (info[i].descripcion.trim() == "ANEXO") {
                                imHtmlAnexo = imHtmlAnexo + "<div class='col-12 '><img class='' src='<?php echo base_url("documentos_peritos/img_reporte_fotografico");?>/"+inf+ "/"+ info[i].url + "' style='width: 100%; height: 300px;'><div class='caption text-center'></div></div>";
                            } else {
                                imHtml = imHtml + "<div class='col-6 '><img class='imgAV' src='<?php echo base_url("documentos_peritos/img_reporte_fotografico");?>/"+inf+ "/" + info[i].url + "' style='width: 100%; height: 300px;' ><div class='caption text-center mt-2'><h5>" + info[i].descripcion.trim() + "</h5></div></div>";
                            }
                        }

                        $("#divRepfoto").html(imHtml);
                        $("#divRepfotoAnexo").html(imHtmlAnexo);

                        }
                    }
                });
        }

function cargar_portada(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_portada"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
          var d = response.portada[0];
          //console.log(d);
          $('#labelInmueble').text(d.inmueble.toUpperCase());
          $('#labelUbicacion').text(d.calle.toUpperCase()+" "+d.numero.toUpperCase()+" "+d.colonia.toUpperCase()+" "+d.cp+" "+d.municipio.toUpperCase());
          $('#labelUbi').text(d.calle.toUpperCase()+" "+d.numero.toUpperCase()+" "+d.colonia.toUpperCase()+" "+d.cp+" "+d.municipio.toUpperCase());
          $("#labelInmuebleValua").text(d.inmueble.toUpperCase()); 

      }
    });
  }

  function cargar_antecedentes(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_antecedentes"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        //console.log(response);
        if (response.antecedentes.length > 0) {
            var json = response.antecedentes[0];
            $("#labelSolicitante").text(json.solicitante.toUpperCase()); 
            $("#labelFechaInspeccion").text(moment(json.fecha_inspeccion).format('DD/MM/YYYY'));
            $("#labelRegimen").text(json.regimen.toUpperCase()); 
            $("#labelObjeto").text(json.objeto.toUpperCase()); 
            $("#labelRegPubProp").text("Folio:"+json.rpp+", Inscripcion:"+json.inscripcion+", Libro:"+json.libro+", Sección:"+json.seccion); 
          /*$("#antecedentes_ok").show();
          $("#divAntecedentes-tab").addClass('border border-success');
          var json = response.antecedentes[0];
          $("#txtSolicitante").val(json.solicitante);
          $("#txtFechaInspeccion").val(moment(json.fecha_inspeccion).format('YYYY-MM-DD'));
          $("#ContentPlaceHolder1_txtRegimen").val(json.regimen);
          $("#selObjectoAvaluo").val(json.objeto);
          $("#txtRegPubPro").val(json.rpp);
          $("#txtInscripcion").val(json.inscripcion);
          $("#txtLibro").val(json.libro);
          $("#txtSeccion").val(json.seccion);*/
        }
      }
    });
  }

  function checks_servicios(value){
      if(value === 't')
        return 'Sí'; 
      else if (value === 'f')
        return 'No';
      else
        return 'sabe';
  }

  function cargar_car_urb(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_car_urb"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        //console.log(response);
        if (response.caracteristicas_urbanas.length > 0) {
          var json = response.caracteristicas_urbanas[0];

          $("#labelClasificacionZona").text(json.clasificacionzona.toUpperCase());
          $("#labelTipoConstruccionDominante").text(json.tipoconstdominante.toUpperCase());
          $("#labelIndiceSarturacion").text(json.indicesatzona.toUpperCase());
          $("#labelPoblacion").text(json.poblacion.toUpperCase());
          $("#labelContaminacion").text(json.contaminacionambiental.toUpperCase());
          $("#labelUsoSuelo").text(json.usosuelopermitido.toUpperCase());
          $("#labelViasAcceso").text(json.viaaccesoimportancia.toUpperCase());

          $("#labelAgua").text(checks_servicios(json.agua));
          $("#labelTel").text(checks_servicios(json.telefono));
          $("#labelDrenaje").text(checks_servicios(json.drenaje));
          $("#labelBanquetas").text(checks_servicios(json.banqueta));
          $("#labelEnergia").text(checks_servicios(json.energia));
          $("#labelGuarniciones").text(checks_servicios(json.guarniciones));
          $("#labelAlumbrado").text(checks_servicios(json.alumbrado));
          $("#labelPavimento").text(checks_servicios(json.pavimento));
          $("#labelBasura").text(checks_servicios(json.basura));
          $("#labelVigilancia").text(checks_servicios(json.vigilancia));
          $("#labelTransporte").text(checks_servicios(json.transporte));

        }
      }
    });
  }

  function cargar_tabla_orientaciones(info) {
    var url = "<?php echo base_url("peritos/obtener_orientaciones/"); ?>/"+info;
    var html = "";
    $.getJSON(url, function(json) {
      $('#tablaOrientacion').empty();
      html += '<thead><th>Orientación</th><th>Descripción</th></thead>';
      $.each(json.orientaciones, function(i, obj) {
        html += '<tr><td>' + obj.orientacion + '</td><td>' + obj.descripcion + '</td></tr>';
      });

      $('#tablaOrientacion').append(html);
    });
  }

  function cargar_car_terreno(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_car_terreno"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        //console.log(response);
        if (response.caracteristicas_terreno.length > 0) {
          
          var json = response.caracteristicas_terreno[0];

          $("#labelTopografiayconf").text(json.topografia_const.toUpperCase());
          $("#labelNumeroFrentes").text(json.numero_frentes);
          $("#labelCaracteristicasPanoramicas").val(json.car_panoramicas.toUpperCase());
          $("#labelDensidadHabita").text(json.densidad_habitacional.toUpperCase());
          $("#labelIntencidadConstruccion").text(json.intensidad_const.toUpperCase());
          $("#labelServidumbresRestricciones").text(json.servidumbre_restricciones.toUpperCase());
          $("#labelSuperficieEcrituras").text(json.sup_escrituras);
          $("#labelSuperficieTopografico").text(json.sup_topografico);

        }
      }
    });
  }

  function cargar_valor_mercado(info){
    //TODO
  }

  function cargar_descripcion_general(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_descripcion_general"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        //console.log(response);
        if (response.descripcion_general.length > 0) {
          var json = response.descripcion_general[0];
          $("#labelUsoActual").text(json.cve_uso);
            $.getJSON("<?php echo base_url("peritos/grupo_usos"); ?>", function(json) {
                $.each(json.results, function(i, obj) {
                    if(obj.CVE_GPO_US == $("#labelUsoActual").text()){
                        $("#labelUsoActual").text(obj.DES_GPO_US);
                    }
                });
            });
          
          $("#labelEdadAprox").text(json.edad_aprox);
          $("#labelNumeroNiveles").text(json.num_niveles);
          $("#labelEstadoConservacion").text(json.estado_conservacion.toUpperCase());
          $("#labelCalidadProyecto").text(json.calidad_proyecto.toUpperCase());
          $("#labelUnidadesRentables").text(json.unidades_rentables.toUpperCase());
          $("#labelDescripcionGeneralInmueble").text(json.descripcion_general.toUpperCase());
        }
      }
    });
  }

  function cargar_elementos_construccion(info){
    $.ajax({
      url: "<?php echo base_url("peritos/obtener_elementos_construccion"); ?>/" + info,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        //console.log(response);
        if (response.elementos_construccion.length > 0) {
          
          var json = response.elementos_construccion[0];

          $("#labelCimientos").text(json.a_cimientos);
          $("#labelEstructura").text(json.a_estructura);
          $("#labelMuros").text(json.a_muros);
          $("#labelEntrepisos").text(json.a_entrepisos);
          $("#labelTechos").text(json.a_techos);
          $("#labelAzoteas").text(json.a_azoteas);
          $("#labelBardas").text(json.a_bardas); 
          $("#labelAplanadosInteriores").text(json.b_aplanadosinteriores);
          $("#labelAplanadosExteriores").text(json.b_aplanadosexteriores);
          $("#labelPlafones").text(json.b_plafones);
          $("#labelLambrines").text(json.b_lambrines);
          $("#labelPisos").text(json.b_pisos);
          $("#labelZoclos").text(json.b_zoclos);
          $("#labelEscaleras").text(json.b_escaleras);
          $("#labelPintura").text(json.b_pintura);
          $("#labelRecubrimientosEspeciales").text(json.b_recubrimientosespeciales);
          $("#labelPuertas").text(json.c_puertas);
          $("#labelClosets").text(json.c_closets);
          $("#labelMueblesBaño").text(json.d_mueblesbano);
          $("#labelEquipoCocina").text(json.d_equipodecocina);
          $("#labelInstacionesElectricas").text(json.e_instalacioneselectricas);
          $("#labelHerreria").text(json.f_herreria);
          $("#labelVidrieria").text(json.g_vidrieria);
          $("#labelCerrajeria").text(json.h_cerrajeria);
          $("#labelFachadas").text(json.i_fachadas);
          $("#labelInstalacionesEspecialesEC").text(json.j_instalacionesespeciales);

        }
      }
    });
  }

  function cargar_terreno_fisico(data){
    //console.log(data);
    suptotalterreno = 0.0;
    valortotalterreno = 0.0;
    $.each(data, function(i, obj) {
        suptotalterreno += parseFloat(obj.superficie);
        valortotalterreno += parseFloat(obj.valor_terreno.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        $('#tablaFisicoTerreno > tbody').append('<tr><td>'+obj.superficie+'</td><td>'+obj.valor_unitario+'</td><td>'+obj.factor_demerito+'</td><td>'+obj.motivo+'</td><td>'+obj.valor_terreno+'</td></tr>');
      });
    $('#txtsuptotalterreno').text(suptotalterreno.toFixed(2));
    $('#txtvalortotalterreno').text(aPesos(valortotalterreno));
    total_fisico += valortotalterreno;
    calculaTotal();
  }

  function cargar_terreno(cvecat){
    var suptotalterreno = 0.0;
    var valortotalterreno = 0.0;
    $.ajax({
      url: "<?php echo base_url("peritos/consulta_terreno"); ?>/"+ cvecat,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        for(var i in response.data){
            var obj = response.data[i];
            let sup = parseFloat(obj.SUP_TERR);
            let val = parseFloat(obj.valorunitario1);
            let valneto = parseFloat(obj.valorneto);
            $('#tablaFisicoTerreno > tbody').append('<tr><td>'+sup+'</td><td>'+aPesos(val)+'</td><td>'+obj.FAC_DEM_TE+'</td><td></td><td>'+aPesos(valneto)+'</td></tr>');
            suptotalterreno += sup;
            valortotalterreno += valneto;
        }
        $('#txtsuptotalterreno').text(suptotalterreno.toFixed(2));
        $('#txtvalortotalterreno').text(aPesos(valortotalterreno));
        calculaTotal();
      }
    });
    
  }


  function cargar_construcciones_fisico(data){
    var suptotalconst = 0.0;
    var valortotalconst = 0.0;
    $.each(data, function(i, obj) {
        let sup = parseFloat(obj.superficie);
        let valneto = parseFloat(obj.valor_construccion.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        $('#tablaFisicoConstrucciones > tbody').append('<tr><td>'+obj.categoria+'</td><td>'+sup+'</td><td>'+obj.valor_unitario+'</td><td>'+obj.estado+'</td><td>'+obj.edad+'</td><td>'+obj.fact_dem+'</td><td>'+obj.fact_com+'</td><td>'+obj.valor_construccion+'</td></tr>');
        suptotalconst += sup;
        valortotalconst += valneto;
      });
      $('#txtsuptotalconst').text(suptotalconst.toFixed(2));
      $('#txtvalortotalconst').text(aPesos(valortotalconst));
      total_fisico += valortotalconst;
      calculaTotal();

  }

  function cargar_especiales_fisico(data){
    var valortotal = 0.0;
    $.each(data, function(i, obj) {
        let valparcial = parseFloat(obj.valor_parcial.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        $('#tablaEspeciales > tbody').append('<tr><td>'+obj.concepto+'</td><td>'+obj.unidad+'</td><td>'+obj.cantidad+'</td><td>'+obj.valor_unit+'</td><td>'+obj.fact_dem+'</td><td>'+obj.vrn_unitario+'</td><td>'+obj.valor_parcial+'</td></tr>');
        valortotal += valparcial;
      });
      $('#txtvalortotalespeciales').text(aPesos(valortotal));
      total_fisico += valortotal;
      calculaTotal();
  }

  function cargar_construcciones(cvecat){
    var suptotalconst = 0.0;
    var valortotalconst = 0.0;
    $.ajax({
      url: "<?php echo base_url("peritos/consulta_construcciones"); ?>/"+ cvecat,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response) {
        for(var i in response.data){
            let sup = parseFloat(response.data[i].SUP_CONST);
            let val = parseFloat(response.data[i].valorunitario);
            let valneto = parseFloat(response.data[i].valorneto);
            $('#tablaFisicoConstrucciones > tbody').append('<tr><td>'+response.data[i].CVE_CAT_CO+'</td><td>'+sup+'</td><td>'+aPesos(val)+'</td><td>'+response.data[i].CVE_EDO_CO+'</td><td>'+response.data[i].EDD_CONST+'</td><td>'+response.data[i].factordemerito+'</td><td>'+response.data[i].factorcomercializacion+'</td><td>'+aPesos(valneto)+'</td>></tr>');
            suptotalconst += sup;
            valortotalconst += valneto;
        }
        
        $('#txtsuptotalconst').text(suptotalconst.toFixed(2));
        $('#txtvalortotalconst').text(aPesos(valortotalconst));
        calculaTotal();
      }
    });
  }

  function calculaTotal() {
    //acumuladores
    var sum = 0.0;
    var sumTerr = 0.0;
    var valTot = 0.00;
    var sumConst = 0.0;
    var sumFisico = 0.0;
    
    //se recorren cada una de las filas del terreno
    for (var i = 2; i < $("#tablaFisicoTerreno")[0].rows.length -1; i++) {
        //obtenemos el valor del terreno
        sum += parseFloat($("#tablaFisicoTerreno")[0].rows[i].cells[4].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        sumTerr += parseFloat($("#tablaFisicoTerreno")[0].rows[i].cells[0].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));;
        valTot += parseFloat($("#tablaFisicoTerreno")[0].rows[i].cells[4].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
    }
    //si el valor del terreno es menor que el valor comercial segun catastro mandamos una alerta
    
    //se actualizan los controles
    $("#txtvalortotalterreno").text(aPesos(parseFloat(sum)));
    $("#txtsuptotalterreno").text(parseFloat(sumTerr).toFixed(2));
    $("#supTerrRev2").text(parseFloat(sumTerr).toFixed(2));
    $("#valTerrRev2").text($("#txtvalortotalterreno").text());
    
    sumFisico = sumFisico + sum;
    
    //se recorren cada una de las filas de
    sum = 0.0;
    for (var i = 2; i < $("#tablaFisicoConstrucciones")[0].rows.length -1; i++) {
        sum = sum + parseFloat($("#tablaFisicoConstrucciones")[0].rows[i].cells[7].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
        sumConst = sumConst + parseFloat($("#tablaFisicoConstrucciones")[0].rows[i].cells[1].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", ""));
    }
    $("#txtvalortotalconst").text(aPesos(parseFloat(sum)));
    $("#txtsuptotalconst").text(parseFloat(sumConst).toFixed(2));
    $("#supConstRev2").text(parseFloat(sumConst).toFixed(2));
    $("#valConstRev2").text($("#txtvalortotalconst").text());
    sumFisico = sumFisico + sum;
    
    //se recorren cada una de las filas del terreno
    sum = 0.0;
    for (var i = 2; i < $("#tablaEspeciales")[0].rows.length -1; i++) { 
      sum = sum + parseFloat($("#tablaEspeciales")[0].rows[i].cells[6].innerText.replace("$", '').replace(" ", "").replace(",", "").replace(",", "")); 
    }
    $("#txtvalortotalespeciales").text(aPesos(parseFloat(sum)));

    sumFisico = sumFisico + sum;

    $("#labelValorFisico").text(aPesos(parseFloat(sumFisico)));
    $("#labelValorFisico_2").text(aPesos(parseFloat(sumFisico)));
    var conclusion = aPesos(parseFloat(sumFisico)) +" ";
    conclusion += numeroALetras(parseFloat(sumFisico));
    $("#labelValorConclusion").text(conclusion);

}


  
  

var numeroALetras = (function() {
    // Código basado en el comentario de @sapienman
    // Código basado en https://gist.github.com/alfchee/e563340276f89b22042a
    function Unidades(num) {

        switch (num) {
            case 1:
                return 'UN';
            case 2:
                return 'DOS';
            case 3:
                return 'TRES';
            case 4:
                return 'CUATRO';
            case 5:
                return 'CINCO';
            case 6:
                return 'SEIS';
            case 7:
                return 'SIETE';
            case 8:
                return 'OCHO';
            case 9:
                return 'NUEVE';
        }

        return '';
    } //Unidades()

    function Decenas(num) {

        let decena = Math.floor(num / 10);
        let unidad = num - (decena * 10);

        switch (decena) {
            case 1:
                switch (unidad) {
                    case 0:
                        return 'DIEZ';
                    case 1:
                        return 'ONCE';
                    case 2:
                        return 'DOCE';
                    case 3:
                        return 'TRECE';
                    case 4:
                        return 'CATORCE';
                    case 5:
                        return 'QUINCE';
                    default:
                        return 'DIECI' + Unidades(unidad);
                }
            case 2:
                switch (unidad) {
                    case 0:
                        return 'VEINTE';
                    default:
                        return 'VEINTI' + Unidades(unidad);
                }
            case 3:
                return DecenasY('TREINTA', unidad);
            case 4:
                return DecenasY('CUARENTA', unidad);
            case 5:
                return DecenasY('CINCUENTA', unidad);
            case 6:
                return DecenasY('SESENTA', unidad);
            case 7:
                return DecenasY('SETENTA', unidad);
            case 8:
                return DecenasY('OCHENTA', unidad);
            case 9:
                return DecenasY('NOVENTA', unidad);
            case 0:
                return Unidades(unidad);
        }
    } //Unidades()

    function DecenasY(strSin, numUnidades) {
        if (numUnidades > 0)
            return strSin + ' Y ' + Unidades(numUnidades)

        return strSin;
    } //DecenasY()

    function Centenas(num) {
        let centenas = Math.floor(num / 100);
        let decenas = num - (centenas * 100);

        switch (centenas) {
            case 1:
                if (decenas > 0)
                    return 'CIENTO ' + Decenas(decenas);
                return 'CIEN';
            case 2:
                return 'DOSCIENTOS ' + Decenas(decenas);
            case 3:
                return 'TRESCIENTOS ' + Decenas(decenas);
            case 4:
                return 'CUATROCIENTOS ' + Decenas(decenas);
            case 5:
                return 'QUINIENTOS ' + Decenas(decenas);
            case 6:
                return 'SEISCIENTOS ' + Decenas(decenas);
            case 7:
                return 'SETECIENTOS ' + Decenas(decenas);
            case 8:
                return 'OCHOCIENTOS ' + Decenas(decenas);
            case 9:
                return 'NOVECIENTOS ' + Decenas(decenas);
        }

        return Decenas(decenas);
    } //Centenas()

    function Seccion(num, divisor, strSingular, strPlural) {
        let cientos = Math.floor(num / divisor)
        let resto = num - (cientos * divisor)

        let letras = '';

        if (cientos > 0)
            if (cientos > 1)
                letras = Centenas(cientos) + ' ' + strPlural;
            else
                letras = strSingular;

        if (resto > 0)
            letras += '';

        return letras;
    } //Seccion()

    function Miles(num) {
        let divisor = 1000;
        let cientos = Math.floor(num / divisor)
        let resto = num - (cientos * divisor)

        let strMiles = Seccion(num, divisor, 'UN MIL', 'MIL');
        let strCentenas = Centenas(resto);

        if (strMiles == '')
            return strCentenas;

        return strMiles + ' ' + strCentenas;
    } //Miles()

    function Millones(num) {
        let divisor = 1000000;
        let cientos = Math.floor(num / divisor)
        let resto = num - (cientos * divisor)

        let strMillones = Seccion(num, divisor, 'UN MILLON DE', 'MILLONES DE');
        let strMiles = Miles(resto);

        if (strMillones == '')
            return strMiles;

        return strMillones + ' ' + strMiles;
    } //Millones()

    return function NumeroALetras(num, currency) {
        currency = currency || {};
        let data = {
            numero: num,
            enteros: Math.floor(num),
            centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
            letrasCentavos: '',
            letrasMonedaPlural: currency.plural || 'PESOS', //'PESOS', 'Dólares', 'Bolívares', 'etcs'
            letrasMonedaSingular: currency.singular || 'PESO', //'PESO', 'Dólar', 'Bolivar', 'etc'
            letrasMonedaCentavoPlural: currency.centPlural || 'CENTAVOS',
            letrasMonedaCentavoSingular: currency.centSingular || 'CENTAVO'
        };

        if (data.centavos > 0) {
            data.letrasCentavos = 'CON ' + (function() {
                if (data.centavos == 1)
                    return Millones(data.centavos) + ' ' + data.letrasMonedaCentavoSingular;
                else
                    return Millones(data.centavos) + ' ' + data.letrasMonedaCentavoPlural;
            })();
        };

        if (data.enteros == 0)
            return 'CERO ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
        if (data.enteros == 1)
            return Millones(data.enteros) + ' ' + data.letrasMonedaSingular + ' ' + data.letrasCentavos;
        else
            return Millones(data.enteros) + ' ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
    };

})();
</script>