<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
 <!-- Buttons examples -->
 <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
   
   <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
   

   <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


   <!-- App js -->
   <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
   <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
   <script src="<?php echo base_url("/assets/js/dataTables.fixedHeader.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jquery.repeater/jquery.repeater.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/pages/form-repeater.init.js");?>"></script>
   <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>

   
<script>
$(document).ready(function () {

/*Setup - add a text input to each footer cell
$('#tbl-dictamenes thead tr').clone(true).appendTo( '#tbl-dictamenes thead' );
$('#tbl-dictamenes thead tr:eq(1) th').each( function (i) {
    var title = $(this).text();
    $(this).html( '<input type="text" class="form-control small" placeholder="Filtrar '+title+'" />' );

    $( 'input', this ).on( 'keyup change', function () {
        if ( table.column(i).search() !== this.value ) {
            table
                .column(i)
                .search( this.value )
                .draw();
        }
    } );
} );*/

var table = $('#tbl-dictamenes').DataTable({
    "orderCellsTop": true,
    "language": {
        "url": "<?php echo base_url("assets/Spanish.json")?>"
    },
    "bProcessing": true,
    "sAjaxSource": "<?php echo base_url("inspeccion/get_dictamenes_regularizacion")?>",
    "bPaginate":true,
    "sPaginationType":"full_numbers",
    "iDisplayLength": 25,
    "order": [[ 0, "desc" ], [1, "desc"]],
    "aoColumns": [
        { mData: 'Id_PaqueteEnvioInspeccion' } ,
        { mData: 'F_EnvioInspeccion' },
        { mData: 'Id_Anio' },
        { mData: 'Id_Folio' },
        { mData: 'Pobl' },
        { mData: 'Ctel' },
        { mData: 'Manz' },
        { mData: 'Pred' },
        { mData: 'Unid' },
        { mData: 'F_InspeccionCampo' },
    ],
    rowCallback: function(row, data, index) {
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        $("td:eq(0)", row).addClass('text-primary font-weight-bold');
        $("td:eq(3)", row).addClass('text-primary');
        $("td:eq(1)", row).text(moment(data['F_EnvioInspeccion']).format('L'));
        
        if(data['F_InspeccionCampo'] != null)
            $("td:eq(9)", row).text(moment(data['F_InspeccionCampo']).format('L'));
        
        $("td:eq(0)", row).addClass('text-center');
        $("td:eq(2)", row).addClass('text-center');
        $("td:eq(3)", row).addClass('text-center');
        $("td:eq(4)", row).addClass('text-center');
        $("td:eq(5)", row).addClass('text-center');
        $("td:eq(6)", row).addClass('text-center');
        $("td:eq(7)", row).addClass('text-center');
        $("td:eq(8)", row).addClass('text-center');
        $("td:eq(9)", row).addClass('text-center');
    }
});

});

$( document ).on('click', '#btn_gen_fol', function(e) {
    e.preventDefault();
    genera_bitacora_regularizacion();
});


$( document ).on('click', '#btn_captura', function(e) {
    e.preventDefault();
    $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD"));  
    folios = [];
    cargar_brigadas();
    cargar_movimientos();
    //genera_bitacora_regularizacion();
    $(".modal-captura").modal("show");

    $('#tblfoliospaq').DataTable().destroy();
        //load table
        var tablef = $('#tblfoliospaq').DataTable({
            "language": {
                "url": "<?php echo base_url("assets/Spanish.json")?>"
            },
            "ordering": true,
            "sAjaxSource": "<?php echo base_url("inspeccion/get_regularizacion_inspeccion_sinpaquete")?>",
            "bPaginate":true,
            "sPaginationType":"full_numbers",
            "iDisplayLength": 10,
            "aoColumns": [
                {
                    data: null,
                    defaultContent: '<input type="checkbox" class="fol" name="folios[]">'
                },
                { mData: 'Id_Anio' } ,
                { mData: 'Id_Folio' },
                { mData: 'Clave' },
                { mData: 'F_InspeccionCampo' },
                { mData: 'Id_Brigada' },
                { mData: 'Id_MovimientoInspeccion1' },
                { mData: 'Id_MovimientoInspeccion2' },
                { mData: 'AreaMedida' },
                { mData: 'Dictamen' },
                { mData: 'Observaciones' }
            ],
            rowCallback: function(row, data, index) {
                var options = { year: 'numeric', month: 'long', day: 'numeric' };
                $("td:eq(2)", row).addClass('text-primary font-weight-bold');
                $("td:eq(1)", row).addClass('text-primary');                
                if(data['F_InspeccionCampo'] != null)
                    $("td:eq(4)", row).text(moment(data['F_InspeccionCampo']).format('L'));
                
              
            }
        });

        $('#tblfoliospaq tbody').on( 'click', '.fol', function () {
                    if(tablef.row(this).child.isShown()){
                        var data = tablef.row(this).data();
                    }else{
                        var data = tablef.row($(this).parents("tr")).data();
                    }
                    if ( folios.includes(data) ){
                        folios = folios.filter(value => value !== data);
                    }
                    else{
                        folios.push(data);
                    }

                    console.log(folios);
                    
            });

}); 

$( document ).on('click', '#btn_guardar', function(e) {
    e.preventDefault();
    var bitacora = $("#bitacora").val();
    if (!$('#bitacora').val() || folios.length === 0) {
        Swal.fire({
                                    icon: 'warning',
                                    title: 'Mensaje del Sistema',
                                    text: 'Por favor Genere un Número de Bitácora y seleccione al menos un folio para realizar el Envio'
                                    
                                })
    }
    else{
        var fecha = $("#dtfecha").val();
    //los folios seleccionados
    var selectfolios = folios;
    //alert(bitacora + " " + fecha + " " + envia + " " + recibe + " " + selectfolios);
    $.ajax({
        type: "POST",
        url: "<?php echo base_url("inspeccion/registrar_bitacora_regularizacion_inspeccion"); ?>",
        type: "POST",
        data: {"Bitacora": bitacora, "FechaEnv": fecha, "Folios": selectfolios },
        cache: false,
        success: function(data){
            console.log(data);
            if (data == 'ok')
            {
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'info',
                                    title: 'Mensaje del Sistema',
                                    text: 'El Paquete de Inspección de Regularización se registro correctamente!'
                                    
                                })
                //cerramos el modal 
                $('#tblfoliospaq').DataTable().ajax.reload(null, false);
                //genera una nueva folio para bitacora
                genera_bitacora_regularizacion();
            }
            else{
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'Ocurrió un error al registrar el Paquete de Inspección de Regularización'
                                    
                                })
            }
        }
    });

    }
   
});   

$( document ).on('click', '#btn_guardar_regu', function(e) {
    e.preventDefault();
    var form = $("#form-regu");
    var i1 = $("#brigadas option:selected").attr('i1');
    var i2 = $("#brigadas option:selected").attr('i2');
    data = form.serialize();
    data += "&i1="+i1+"&i2="+i2;
    //console.log(data);
    $.ajax({
        type: "POST",
        url: "<?php echo base_url("inspeccion/registrar_regularizacion_inspeccion"); ?>",
        type: "POST",
        data: data,
        cache: false,
        success: function(data){
            console.log(data);
            if (data == 'ok')
            {
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'success',
                                    title: 'Mensaje del Sistema',
                                    text: 'La captura se Registro Correctamente'
                                    
                                })
                //cerramos el modal 
                //$('.modal-').modal('hide');
                $('#tblfoliospaq').DataTable().ajax.reload(null, false);
                //reset el form
                $("#form-regu")[0].reset();
            }
            else{
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'Ocurrió un error al guardar la Captura'
                                    
                                })
            }
        }
    });
});   

$(document).on('keypress','#year',function(e) {
    if(e.which == 13) {
        var anio = $('#year').val();
        foliar(anio);
    }
});

$( document ).on('click', '#btn_buscar', function(e) {
    e.preventDefault();
    var anio = $('#year').val();
    if(anio != ""){
        foliar(anio);
    }
     else{
        Swal.fire({
                                    icon: 'warning',
                                    title: 'Mensaje del Sistema',
                                    text: 'Por favor ingrese un año para poder generar un folio'
                                    
                                })
     }  
});
    
    function cargar_brigadas(){
        $('#brigadas').empty();
        var url = "<?php echo base_url("inspeccion/get_brigadas_inspeccion");?>";
        $.getJSON(url, function(json){
            //console.log(json);
                $('#brigadas').append($('<option>').text("Seleccionar Brigada").attr({'value': 0}));
                $.each(json.results, function(i, obj){
                        $('#brigadas').append($('<option>').text(obj.Id_Brigada + " - " +obj.brigada).attr({'value': obj.Id_Brigada, 'i1': obj.i1, 'i2': obj.i2}));
                });     
        });
     }   

     function cargar_movimientos(){
        $('#movimiento1').empty();
        $('#movimiento2').empty();
        var url = "<?php echo base_url("inspeccion/get_movimientos_inspeccion");?>";
        $.getJSON(url, function(json){
            //console.log(json);
                $('#movimiento1').append($('<option>').text("Ninguno").attr({'value': 0}));
                $('#movimiento2').append($('<option>').text("Ninguno").attr({'value': 0}));
                $.each(json.results, function(i, obj){
                        $('#movimiento1').append($('<option>').text(obj.Id_MovimientoInspeccion + " - " +obj.Descripcion).attr({'value': obj.Id_MovimientoInspeccion, 'desc': obj.Descripcion}));
                        $('#movimiento2').append($('<option>').text(obj.Id_MovimientoInspeccion + " - " +obj.Descripcion).attr({'value': obj.Id_MovimientoInspeccion, 'desc': obj.Descripcion}));
                });     
        });
     }      


    function genera_bitacora_regularizacion()
    {
        $.getJSON("<?php echo base_url("inspeccion/genera_bitacora_regularizacion");?>", function(json){ 
                    //console.log(json);
                    $.each(json.result, function(i, obj){
                        var max = parseInt(obj.maximo);
                        var max = max + 1; 
                        $('#bitacora').val(zeroPad(max, 8));
                    });
        }); 
    }  

    function foliar(anio)
    {
        $.getJSON("<?php echo base_url("inspeccion/genera_folio_paquete_regularizacion");?>/"+anio, function(json){ 
                    //console.log(json);
                    $.each(json.result, function(i, obj){
                        var max = parseInt(obj.maximo);
                        var max = max + 1; 
                        $('#folio').val(zeroPad(max, 8));
                    });
        }); 
    }  

    //funcion para formatear los campos de la clave catastral
    function zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }





</script>