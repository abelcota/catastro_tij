<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    <!-- Datatable init js -->
    <script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>




<script>
//funcion para formatear los campos de la clave catastral
function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        }

$( document ).ready(function() {

    // Setup - add a text input to each footer cell
    $('#fechaenvio').val(moment(new Date()).format("YYYY-MM-DD"));  


    /* Setup - add a text input to each footer cell
    $('#tblbitacora thead tr').clone(true).appendTo( '#tblbitacora thead' );
    $('#tblbitacora thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" class="form-control small" placeholder="Filtrar '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );*/
     
    var table = $('#tblbitacora').DataTable({
        "language": {
            "url": "<?php echo base_url("assets/Spanish.json")?>"
        },
        "bProcessing": true,
        "ordering": true,
        "order": [[ 0, "DESC" ], [2, "DESC"]],
        "sAjaxSource": "<?php echo base_url("inspeccion/get_envios_regularizacion_graficacion")?>",
        "bPaginate":true,
        "sPaginationType":"full_numbers",
        "iDisplayLength": 25,
        "aoColumns": [
            { mData: 'Id_PaqueteEnvioGraficacion' } ,
            { mData: 'F_EnvioGraficacion' },
            { mData: 'Id_Anio' },
            { mData: 'Id_Folio' },
            { mData: 'Pobl' },
            { mData: 'Ctel' },
            { mData: 'Manz' },
            { mData: 'Pred' },
            { mData: 'Unid' },
            { mData: 'F_AtendidoGraficacion' },
        ],
        rowCallback: function(row, data, index) {
            var options = { year: 'numeric', month: 'long', day: 'numeric' };
            $("td:eq(0)", row).addClass('text-primary');
            $("td:eq(2)", row).addClass('text-primary');
            $("td:eq(1)", row).addClass('text-center');
            $("td:eq(2)", row).addClass('text-center');
            $("td:eq(3)", row).addClass('text-center');
            $("td:eq(4)", row).addClass('text-center');
            $("td:eq(5)", row).addClass('text-center');
            $("td:eq(6)", row).addClass('text-center');
            $("td:eq(7)", row).addClass('text-center');
            $("td:eq(8)", row).addClass('text-center');
            if(data['F_EnvioGraficacion'] != null)
                $("td:eq(1)", row).text(moment(data['F_EnvioGraficacion']).format('DD/MM/YYYY'));
            $("td:eq(3)", row).addClass('font-weight-bold');

            if(data['F_AtendidoGraficacion'] != null)
                $("td:eq(9)", row).text(moment(data['F_AtendidoGraficacion']).format('DD/MM/YYYY'));
            
        }
    });

    
});

$( document ).on('click', '#btn_nuevo', function(e) {
    e.preventDefault();
    cargar_empleados();
    folios = [];
    genera_bitacora_graficacion_reg();
    $(".modal-nuevo").modal("show");

    $('#tblnotingraficacion').DataTable().destroy();
    //load table
    var table = $('#tblnotingraficacion').DataTable({
        "language": {
            "url": "<?php echo base_url("assets/Spanish.json")?>"
        },
        "bProcessing": true,
        "ordering": true,
        "order": [[ 0, "DESC" ], [1, "DESC"]],
        "sAjaxSource": "<?php echo base_url("inspeccion/get_notin_graficacion_reg")?>",
        "bPaginate":true,
        "sPaginationType":"full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            {
                data: null,
                defaultContent: '<input type="checkbox" class="fol" name="folios[]">'
            },
            { mData: 'Id_Anio' } ,
            { mData: 'Id_Folio' },
            { mData: 'Pobl' },
            { mData: 'Ctel' },
            { mData: 'Manz' },
            { mData: 'Pred' },
            { mData: 'Unid' },
            { mData: 'Clave' },
        ],
        rowCallback: function(row, data, index) {
            var options = { year: 'numeric', month: 'long', day: 'numeric' };
            $("td:eq(2)", row).addClass('text-primary');
            $("td:eq(3)", row).text(zeroPad(data['Pobl'], 3));
            $("td:eq(4)", row).text(zeroPad(data['Ctel'], 3));
            $("td:eq(5)", row).text(zeroPad(data['Manz'], 3));
            $("td:eq(6)", row).text(zeroPad(data['Pred'], 3));
            $("td:eq(7)", row).text(zeroPad(data['Unid'], 3));
        }
    });

    $('#tblnotingraficacion tbody').on( 'click', '.fol', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                if ( folios.includes(data) ){
                    folios = folios.filter(value => value !== data);
                }
                else{
                    folios.push(data);
                }

                console.log(folios);
                
         });
});

$( document ).on('click', '#btn_guardar', function(e) {
    //obtener los elementos que se van a enviar por post
    var bitacora = $("#bitacora").val();
    var fecha = $("#fechaenvio").val();
    var envia = $("#envia").val();
    var recibe = $("#recibe").val();
    //los folios seleccionados
    var selectfolios = folios;
    //alert(bitacora + " " + fecha + " " + envia + " " + recibe + " " + selectfolios);
    $.ajax({
        type: "POST",
        url: "<?php echo base_url("inspeccion/registrar_bitacora_regularizacion_graficacion"); ?>",
        type: "POST",
        data: {"Bitacora": bitacora, "FechaEnv": fecha, "Envia": envia, "Recibe":recibe, "Folios": selectfolios },
        cache: false,
        success: function(data){
            //console.log(data);
            if (data == 'ok')
            {
                //mandamos mensaje de 
                Swal.fire({
                                    type: 'info',
                                    title: 'Mensaje del Sistema',
                                    text: 'El paquete se envió a Graficación Correctamente'
                                    
                                })
                //cerramos el modal 
                $('.modal-nuevo').modal('hide');
                $('#tblbitacora').DataTable().ajax.reload(null, false);
            }
            else{
                //mandamos mensaje de 
                Swal.fire({
                                    type: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'Ocurrió un error al enviar el paquete a Graficación'
                                    
                                })
            }
        }
    });
    
});

function genera_bitacora_graficacion_reg()
{
    $.getJSON("<?php echo base_url("inspeccion/genera_bitacora_graficacion_reg");?>", function(json){ 
                $.each(json.result, function(i, obj){
                    var max = parseInt(obj.maximo);
                    var max = max + 1; 
                    $('#bitacora').val(zeroPad(max, 8));
                });
    }); 
}

function cargar_empleados(){
            $.getJSON("<?php echo base_url("inspeccion/get_empleados");?>", function(json){
                    $('#envia').empty();
                    $('#recibe').empty();
                    $('#envia').append($('<option>').text("Seleccionar Empleado").attr({'value':""}));
                    $('#recibe').append($('<option>').text("Seleccionar Empleado").attr({'value':""}));
                    $.each(json.result, function(i, obj){
                            $('#envia').append($('<option>').text(obj.Nombre + " " + obj.ApellidoPaterno + " " + obj.ApellidoMaterno).attr({ 'value' : obj.IdEmpleado}));
                            $('#recibe').append($('<option>').text(obj.Nombre + " " + obj.ApellidoPaterno + " " + obj.ApellidoMaterno).attr({ 'value' : obj.IdEmpleado}));
                    });
                    //$('#empleados').select2();
            });
         }

</script>
            