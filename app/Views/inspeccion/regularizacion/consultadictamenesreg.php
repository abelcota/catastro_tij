<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">CONSULTA DE DICTÁMENES DE INSPECCIÓN DE REGULARIZACIÓN</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">INSPECCIÓN</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="btn-toolbar p-3" role="toolbar">
                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_captura"><i class="fa fa-plus"></i> Captura de Inspección de Regularización</button>
                                        </div>

                                       
                                    </div>
                                    <!--end toolbar -->
                          <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    
                                <h5 class="card-title">Bitácoras de Dictámenes de Regularización</h5>
                                <hr>
                                     <!-- start form -->

                                        <div class="row">
                                            <div class="col-lg-12" style="margin-top:10px;">
                                                <table class="table display table-hover" id="tbl-dictamenes">
                                                    <thead class="text-white" style="background-color: #480912;">
                                                        <th class="text-center">Envio</th>
                                                        <th class="text-center">Fecha</th>
                                                        <th class="text-center">Año</th>
                                                        <th class="text-center">Folio</th>
                                                        <th class="text-center">Pobl</th>
                                                        <th class="text-center">Ctel</th>
                                                        <th class="text-center">Manz</th>
                                                        <th class="text-center">Pred</th>
                                                        <th class="text-center">Unid</th>
                                                        <th class="text-center">Inspeccionado</th>
                                                        </thead>
                                                    </table>
                                            </div>
                                        </div>
                                        
                                                                               
                                    
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->

                     <!--modal para ver el listado de solicitudes -->
                     <div class="modal fade modal-captura" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                        <div class="modal-dialog modal-lg modal-dialog-top">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title mt-0">CREACIÓN DE PAQUETE DE INSPECCIÓN DE REGULARIZACIONES</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body"> 
                                            <!-- start form -->
                                            <form class="repeater" id="form-regu">
                                           
                                               <h5 class="card-title">Registrar Regularizaciones de Inspección</h5>
                                               <div class="row">        
                                            <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="year">Año:</label>
                                                    <input type="text" id="year" name="year" class="form-control" placeholder="Año">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="folio">Folio:</label>
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                    <input type="text" class="form-control" placeholder="00000000" id="folio" name="folio" maxlength="8" readonly>
                                                    <span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_buscar">+</button></span></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral:</label>
                                                    
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="POBL" id="POBL" name="POBL" maxlength="3"> 
                                                                <input type="text" class="form-control" name="CTEL" placeholder="CTEL" id="CTEL" maxlength="3" >
                                                                <input type="text" class="form-control" placeholder="MANZ" id="MANZ" name="MANZ" maxlength="3">
                                                                <input type="text" class="form-control" name="PRED" placeholder="PRED" id="PRED" maxlength="3">
                                                                <input type="text" class="form-control" placeholder="UNID" id="UNID" name="UNID" maxlength="3">
                                                               
                                                            </div>
                                                
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Fecha de Inspección:</label>
                                                    <input type="date" class="form-control" id="dtfechain"  name="dtfechain" data-date-format="DD/MM/YYYY">
                                                
                                                </div>
                                            </div> <!-- end col 12 -->
                                            <div class="col-lg-8">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Brigada:</label>
                                                    <select class="form-control" id="brigadas" name="brigadas">
                                                    </select>
                                                
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Movimiento 1:</label>
                                                    <select class="form-control" id="movimiento1" name="movimiento1">
                                                    </select>
                                                
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Movimiento 2:</label>
                                                    <select class="form-control" id="movimiento2" name="movimiento2">
                                                    </select>
                                                
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Área Cal.:</label>
                                                    <input type="number" class="form-control" value="0.0" name="area" id="area" min="0" value="0" step="any">    
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Dictámen:</label>
                                                    <textarea class="form-control" id="dictamen" name="dictamen" cols="1" rows="3"></textarea>
                                                
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Observaciones</label>
                                                    <textarea class="form-control" id="observaciones" name="observaciones" cols="1" rows="3"></textarea>
                                                
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                            <button type="button" class="btn btn-success float-right"  id="btn_guardar_regu">Registrar</button>
                                            </div>
                                            

                                    </div>       
                                               <!-- end form -->
                                             
                                    </form>
                                    <hr>
                                    <div class="row">
                                            
                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="bitacora">No. Bitacora:</label>
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected"><input data-toggle="touchspin" type="text" id="bitacora" name="bitacora" class="form-control" readonly><span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_gen_fol">+</button></span></div>

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                               
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="fecha">Fecha:</label>
                                                    <input type="date" class="form-control" id="dtfecha" name="dtfecha">
                                                </div>
                                            </div>
                                        </div>
                                           <!--formulario para agregar-->
                                           <hr>
                                                                <!--tabla de los folios para el paquete -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                        <div class="table-responsive">
                                        <table class="table table-hover nowrap table-sm" id="tblfoliospaq" width="100%">
                                                                        <thead class="text-white" style="background-color: #480912;">
                                                                        <tr>
                                                                            <th></th>
                                                                            <th>Año</th>
                                                                            <th>Folio</th>
                                                                            <th>Clave</th>
                                                                            <th>Fecha Insp</th>
                                                                            <th>Brigada</th>
                                                                            <th>Mov 1</th>
                                                                            <th>Mov 2</th>
                                                                            <th>Area Cal.</th>
                                                                            <th>Dictamen</th>
                                                                            <th>Observaciones</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                    </div>
                                        </div>
                                    </div>
                                                                </div>
                                                                <div class="modal-footer">              
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_tcancelar">Cancelar</button>
                                                                    <button type="button" class="btn btn-success"  id="btn_guardar">Enviar Paquete</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            