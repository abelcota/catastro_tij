  <!-- JAVASCRIPT -->
  <script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

    <!-- Required datatable js -->
    <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
     <!-- Buttons examples -->
     <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
       
       <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
       
   
       <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
   
   
       <!-- App js -->
       <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
       <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
<script>
$(document).ready(function () {
    get_anio();
    cargar_brigadas();
    cargar_movimientos();
});

function get_anio(){
            var anio = new Date().getFullYear().toString(); 
            var a = anio.substr(2,4);
            $('#year').val(a); 
            return a;
        }
        
        function cargar_brigadas(){
            $('#brigadas').empty();
            var url = "<?php echo base_url("inspeccion/get_brigadas_inspeccion");?>";
            $.getJSON(url, function(json){
                console.log(json);
                    $('#brigadas').append($('<option>').text("Seleccionar Brigada").attr({'value': 0}));
                    $.each(json.results, function(i, obj){
                            $('#brigadas').append($('<option>').text(obj.Id_Brigada + " - " +obj.brigada).attr({'value': obj.Id_Brigada, 'i1': obj.i1, 'i2': obj.i2}));
                    });     
            });
         }   

         function cargar_movimientos(){
            $('#movimiento1').empty();
            $('#movimiento2').empty();
            var url = "<?php echo base_url("inspeccion/get_movimientos_inspeccion");?>";
            $.getJSON(url, function(json){
                console.log(json);
                    $('#movimiento1').append($('<option>').text("Ninguno").attr({'value': 0}));
                    $('#movimiento2').append($('<option>').text("Ninguno").attr({'value': 0}));
                    $.each(json.results, function(i, obj){
                            $('#movimiento1').append($('<option>').text(obj.Id_MovimientoInspeccion + " - " +obj.Descripcion).attr({'value': obj.Id_MovimientoInspeccion, 'desc': obj.Descripcion}));
                            $('#movimiento2').append($('<option>').text(obj.Id_MovimientoInspeccion + " - " +obj.Descripcion).attr({'value': obj.Id_MovimientoInspeccion, 'desc': obj.Descripcion}));
                    });     
            });
         }        

          
</script>