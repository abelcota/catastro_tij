<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">CONSULTA DE DICTÁMENES VENTANILLA-INSPECCIÓN</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">INSPECCION</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="btn-toolbar p-3" role="toolbar">
                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_captura"><i class="fa fa-plus"></i> Capturar Dictámen de Inspección</button>
                                        </div>

                                        
                                    </div>
                                    <!--end toolbar -->
                          <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                     <!-- start form -->
                                     <h4 class="card-title">Bitácoras de Dictámenes Ventanilla-Inspección</h4>
                                        <hr>
                                        <div class="row">
                                            <div class="col-lg-12" style="margin-top:10px;">
                                                <table class="table display table-hover" id="tbl-dictamenes">
                                                    <thead class="text-white" style="background-color: #480912;">
                                                        <th>P. Envio</th>
                                                        <th>Fecha</th>
                                                        <th class="text-center">Año</th>
                                                        <th class="text-center">Folio</th>
                                                        <th class="text-center">Pobl</th>
                                                        <th class="text-center">Ctel</th>
                                                        <th class="text-center">Manz</th>
                                                        <th class="text-center">Pred</th>
                                                        <th class="text-center">Unid</th>
                                                        <th class="text-center">Antendido</th>
                                                        <th class="text-center">Verificar</th>
                                                        </thead>
                                                    </table>
                                            </div>
                                        </div>
                                        
                                                                               
                                    
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->

                     <!--modal para ver el listado de solicitudes -->
                     <div class="modal fade modal-captura" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                        <div class="modal-dialog modal-lg modal-dialog-top">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title mt-0">Captura de Dictámenes de Inspección de Trámites de Ventanilla</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body"> 
                                            <!-- start form -->
                                            <form class="needs-validation" novalidate id="form-captura">
                                            <div class="row">
                                            
                                                <div class="col-md-2">
                                                    <div class="form-group position-relative">
                                                        <label for="year">Año:</label>
                                                        <input type="text" id="year" name="year" class="form-control" placeholder="Año">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group position-relative">
                                                        <label for="folio">Folio:</label>
                                                        <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                        <input type="text" class="form-control"  placeholder="00000000" id="folio" name="folio" maxlength="8">
                                                        <span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_buscar"><i class="bx bx-search-alt"></i></button></span></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group position-relative">
                                                        <label for="validationTooltipUsername">Clave Catastral:</label>
                                                        
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" placeholder="POBL" id="POBL" name="POBL" maxlength="3" readonly style="background-color:  #dddddd!important;"> 
                                                                    <input type="text" class="form-control" name="CTEL" placeholder="CTEL" id="CTEL" readonly maxlength="3" style="background-color:  #dddddd!important;">
                                                                    <input type="text" class="form-control" placeholder="MANZ" id="MANZ" name="MANZ" maxlength="3" readonly style="background-color:  #dddddd!important;">
                                                                    <input type="text" class="form-control" name="PRED" placeholder="PRED" id="PRED" maxlength="3" readonly style="background-color:  #dddddd!important;">
                                                                    <input type="text" class="form-control" placeholder="UNID" id="UNID" name="UNID" maxlength="3" readonly style="background-color:  #dddddd!important;">
                                                                   
                                                                </div>
                                                    
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group position-relative">
                                                        <label for="validationTooltip02">Fecha de Inspección:</label>
                                                        <input type="date" class="form-control" id="dtfecha"  name="dtfecha" data-date-format="DD/MM/YYYY">
                                                    
                                                    </div>
                                                </div> <!-- end col 12 -->
                                                <div class="col-lg-8">
                                                    <div class="form-group position-relative">
                                                        <label for="validationTooltip02">Brigada:</label>
                                                        <select class="form-control" id="brigadas" name="brigadas">
                                                        </select>
                                                    
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group position-relative">
                                                        <label for="validationTooltip02">Movimiento 1:</label>
                                                        <select class="form-control" id="movimiento1" name="movimiento1">
                                                        </select>
                                                    
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group position-relative">
                                                        <label for="validationTooltip02">Movimiento 2:</label>
                                                        <select class="form-control" id="movimiento2" name="movimiento2">
                                                        </select>
                                                    
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group position-relative">
                                                        <label for="validationTooltip02">Dictámen:</label>
                                                        <textarea class="form-control" id="dictamen" name="dictamen" cols="1" rows="3"></textarea>
                                                    
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group position-relative">
                                                        <label for="validationTooltip02">Observaciones</label>
                                                        <textarea class="form-control" id="observaciones" name="observaciones" cols="1" rows="3"></textarea>
                                                    
                                                    </div>
                                                </div>
                                                

                                        </div>                                       
                                                                               
                                    </form>
                                                                </div>
                                                                <div class="modal-footer">              
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_tcancelar">Cerrar</button>
                                                                    <button type="button" class="btn btn-success"  id="btn_guardar">Guardar</button>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                     
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            