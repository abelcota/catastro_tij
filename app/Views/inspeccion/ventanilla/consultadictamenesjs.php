  <!-- JAVASCRIPT -->
  <script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

    <!-- Required datatable js -->
    <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
     <!-- Buttons examples -->
     <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
       
       <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
       
   
       <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
   
   
       <!-- App js -->
       <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
       <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
       <script src="<?php echo base_url("/assets/js/dataTables.fixedHeader.min.js");?>"></script>
       <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
       
<script>
$(document).ready(function () {

    /* Setup - add a text input to each footer cell
    $('#tbl-dictamenes thead tr').clone(true).appendTo( '#tbl-dictamenes thead' );
    $('#tbl-dictamenes thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" class="form-control small" placeholder="Filtrar '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );*/
    
    var table = $('#tbl-dictamenes').DataTable({
        "orderCellsTop": true,
        "language": {
            "url": "<?php echo base_url("assets/Spanish.json")?>"
        },
        "bProcessing": true,
        "sAjaxSource": "<?php echo base_url("inspeccion/get_dictamenes_ventanilla")?>",
        "bPaginate":true,
        "sPaginationType":"full_numbers",
        "iDisplayLength": 25,
        "order": [[ 0, "desc" ], [1, "desc"]],
        "aoColumns": [
            { mData: 'Id_PaqueteEnvioInspeccion' } ,
            { mData: 'F_EnvioInspeccion' },
            { mData: 'Id_Anio' },
            { mData: 'Id_Folio' },
            { mData: 'Pobl' },
            { mData: 'Ctel' },
            { mData: 'Manz' },
            { mData: 'Pred' },
            { mData: 'Unid' },
            { mData: 'F_InspeccionCampo' },
            {
                data: null,
                defaultContent: '<a class="verificar text-success ml-2" style="cursor:pointer !important;"> <i class="mdi mdi-file-document-box-check-outline"></i></a>'
            },
        ],
        rowCallback: function(row, data, index) {
            var options = { year: 'numeric', month: 'long', day: 'numeric' };
            $("td:eq(0)", row).addClass('text-primary font-weight-bold');
            $("td:eq(2)", row).addClass('text-center');
            $("td:eq(3)", row).addClass('text-primary');
            $("td:eq(4)", row).addClass('text-center');
            $("td:eq(5)", row).addClass('text-center');
            $("td:eq(6)", row).addClass('text-center');
            $("td:eq(7)", row).addClass('text-center');
            $("td:eq(8)", row).addClass('text-center');
            $("td:eq(1)", row).text(moment(data['F_EnvioInspeccion']).format('DD/MM/YYYY'));
            
            if(data['F_InspeccionCampo'] != null)
                $("td:eq(9)", row).text(moment(data['F_InspeccionCampo']).format('DD/MM/YYYY'));
           
            $("td:eq(10)", row).addClass('text-center');
        }
    });

    //click al boton consultar
    $('#tbl-dictamenes tbody').on( 'click', '.verificar', function () {
                if($('#tbl-dictamenes').DataTable().row(this).child.isShown()){
                    var data = $('#tbl-dictamenes').DataTable().row(this).data();
                }else{
                    var data = $('#tbl-dictamenes').DataTable().row($(this).parents("tr")).data();
                }
                var a = data["Id_Anio"];
                var fol = data["Id_Folio"].slice(3);;

                var url = "<?php echo base_url("verificacion/verificacion"); ?>/"+a+"/"+fol;
                window.open(url, '_blank');
                
    });

});

$( document ).on('click', '#btn_captura', function(e) {
    e.preventDefault();
    get_anio();
    cargar_brigadas();
    cargar_movimientos();
    $("#form-captura")[0].reset();
    $(".modal-captura").modal("show");

});

$( document ).on('click', '#btn_buscar', function(e) {
    e.preventDefault();
    consulta_folio();
});


$(document).on('keypress','#folio',function(e) {
    if(e.which == 13) {
        consulta_folio();
    }
});

function consulta_folio(){
    var anio = $('#year').val();
        var folio = $('#folio').val();
        //consultar el folio
        var url = "<?php echo base_url("inspeccion/consultar_folio");?>/"+anio+"/"+folio;
        $.getJSON(url, function(json){
            var data = json.results;
            console.log(data);
            if(data.length > 0)
            {
                //llenamos los datos en los destos
                $('#POBL').val(data[0].Pobl);
                $('#CTEL').val(data[0].Ctel);
                $('#MANZ').val(data[0].Manz);
                $('#PRED').val(data[0].Pred);
                $('#UNID').val(data[0].Unid);
                //si hay datos pues se llenan los demas controles
                if(data[0].F_InspeccionCampo != null)
                    $('#dtfecha').val(moment(data[0].F_InspeccionCampo).format('YYYY-MM-DD'));

                if(data[0].Id_Brigada != null)
                    $('#brigadas').val(data[0].Id_Brigada);
                if(data[0].Id_MovimientoInspeccion1 != null)
                    $('#movimiento1').val(data[0].Id_MovimientoInspeccion1);
                if(data[0].Id_MovimientoInspeccion2 != null)
                    $('#movimiento2').val(data[0].Id_MovimientoInspeccion2);
                $('#dictamen').text(data[0].Dictamen);
                $('#observaciones').text(data[0].Observaciones);
                
            }    
            else{
                 //mandamos mensaje de 
                 Swal.fire({
                                    icon: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'Folio no encontrado'
                                    
                                })
            }
        });
}

$( document ).on('click', '#btn_guardar', function(e) {
    e.preventDefault();
    var form = $("#form-captura");
    var i1 = $("#brigadas option:selected").attr('i1');
    var i2 = $("#brigadas option:selected").attr('i2');
    data = form.serialize();
    data += "&i1="+i1+"&i2="+i2;
    console.log(data);
    $.ajax({
        type: "POST",
        url: "<?php echo base_url("inspeccion/capturar_inspeccion_ventanilla"); ?>",
        type: "POST",
        data: data,
        cache: false,
        success: function(data){
            //console.log(data);
            if (data == 'ok')
            {
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'success',
                                    title: 'Mensaje del Sistema',
                                    text: 'La captura se Registro Correctamente'
                                    
                                })
                //cerramos el modal 
                $('.modal-captura').modal('hide');
                $('#tbl-dictamenes').DataTable().ajax.reload(null, false);
            }
            else{
                //mandamos mensaje de 
                Swal.fire({
                                    icon: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'Ocurrió un error al guardar la Captura'
                                    
                                })
            }
        }
    });
});

function get_anio(){
            var anio = new Date().getFullYear().toString(); 
            var a = anio.substr(2,4);
            $('#year').val(a); 
            return a;
        }
        
        function cargar_brigadas(){
            $('#brigadas').empty();
            var url = "<?php echo base_url("inspeccion/get_brigadas_inspeccion");?>";
            $.getJSON(url, function(json){
                console.log(json);
                    $('#brigadas').append($('<option>').text("Seleccionar Brigada").attr({'value': 0}));
                    $.each(json.results, function(i, obj){
                            $('#brigadas').append($('<option>').text(obj.Id_Brigada + " - " +obj.brigada).attr({'value': obj.Id_Brigada, 'i1': obj.i1, 'i2': obj.i2}));
                    });     
            });
         }   

         function cargar_movimientos(){
            $('#movimiento1').empty();
            $('#movimiento2').empty();
            var url = "<?php echo base_url("inspeccion/get_movimientos_inspeccion");?>";
            $.getJSON(url, function(json){
                    $('#movimiento1').append($('<option>').text("Seleccionar Movimiento 1").attr({'value': '00'}));
                    $('#movimiento2').append($('<option>').text("Seleccionar Movimiento 2").attr({'value': '00'}));
                    $.each(json.results, function(i, obj){
                            $('#movimiento1').append($('<option>').text(obj.Id_MovimientoInspeccion + " - " +obj.Descripcion).attr({'value': obj.Id_MovimientoInspeccion, 'desc': obj.Descripcion}));
                            $('#movimiento2').append($('<option>').text(obj.Id_MovimientoInspeccion + " - " +obj.Descripcion).attr({'value': obj.Id_MovimientoInspeccion, 'desc': obj.Descripcion}));
                    });     
            });
         }        



</script>