<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
 <!-- Buttons examples -->
 <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
   
   <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
   

   <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


   <!-- App js -->
   <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
   <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
   <script src="<?php echo base_url("/assets/js/dataTables.fixedHeader.min.js");?>"></script>
   <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
   
<script>

var table;

$(document).on('click', '#btn_consultar', function (e) {
    e.preventDefault();
    var tra1 = $("#tramites").val();
    var tra2 = $("#tramites2").val();

    var f1 = $("#start").val();
    var f2 = $("#end").val();

    //obtener el radio seleccionado 
    var url = "<?php echo base_url("inspeccion/get_reporte_movimientos_regularizacion")?>/"+tra1+"/"+tra2+"/"+f1+"/"+f2;
    console.log(url);
    //Destroy the old Datatable
    $('#tbl-rpt').DataTable().destroy();
    $('#tbl-rpt').DataTable({
    "language": {
        "url": "<?php echo base_url("assets/Spanish.json")?>"
    },
    "sAjaxSource": url,
    "bPaginate": false,
    "bFilter": false,
    "bInfo": false,
    //"order": [[ 0, "asc" ]],
    "aoColumns": [
        { mData: 'id' } ,
        { mData: 'Descripcion' },
        { mData: 'total' },
    ],
    rowCallback: function(row, data, index) {
       
    },
    footerCallback: function ( row, data, start, end, display ) {
        var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            
         // Total over all pages
         total = api
                .column( 2 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

         // Update footer
         $( api.column( 2 ).footer() ).html(
                'TOTAL: <label class="text-right">' +total +'</label>'
            );
    }

    });

});

$(document).ready(function () {
 cargar_movimientos();

});

function formatFriendCode(friendCode, numDigits, delimiter) {
  return Array.from(friendCode).reduce((accum, cur, idx) => {
    return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
  }, '')
}


function cargar_movimientos(){
            $('#tramites').empty();
            $('#tramites2').empty();
            var url = "<?php echo base_url("inspeccion/get_movimientos_inspeccion");?>";
            $.getJSON(url, function(json){
                console.log(json);
                    $.each(json.results, function(i, obj){
                            $('#tramites').append($('<option>').text(obj.Id_MovimientoInspeccion + " - " +obj.Descripcion).attr({'value': obj.Id_MovimientoInspeccion}));
                            $('#tramites2').append($('<option>').text(obj.Id_MovimientoInspeccion + " - " +obj.Descripcion).attr({'value': obj.Id_MovimientoInspeccion}));
                    });     
            });
         }   
</script>