<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div class="h-100">

        <div class="user-wid text-center py-4">
            <div class="">
                <img src="<?php echo base_url("assets/images/aytoColor.png");?>" alt="" class="" width="80%">
            </div>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Módulos</li>

                <li class="">
                    <a href="<?php echo base_url("/");?>" class="waves-effect" style="color: #480912; ">
                        <i class="mdi mdi-home-circle" style="color: #480912"></i>
                        <span>Inicio</span>
                    </a>
                </li>

                <?php 
                //Validamos si esta en el grupo de Graficacion o administracion
                if( in_groups('Administracion', user_id()) ) : 
                ?>

                <li class="menu-title" >Dirección de Catastro</li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; ">
                        <i class="bx bx-group" style="color: #480912"></i>
                        <span>Administrativo</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                    
                    <li><a href="#" class="has-arrow waves-effect"><i class="bx bx-folder-open bx-xs" style="color: #480912"></i> Catalogos</a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li><a href="<?php echo base_url("administrativo/poblaciones/");?>"><i class="bx bx-data bx-xs"></i>Poblaciones</a></li>
                            <li><a href="<?php echo base_url("administrativo/zonas/");?>"><i class="bx bx-data bx-xs"></i>Zonas</a></li>
                            <li><a href="<?php echo base_url("administrativo/colonias/");?>"><i class="bx bx-data bx-xs"></i>Colonias</a></li>
                            <li><a href="<?php echo base_url("administrativo/manzanas/");?>"><i class="bx bx-data bx-xs"></i>Manzanas</a></li>
                            <li><a href="<?php echo base_url("administrativo/calles/");?>"><i class="bx bx-data bx-xs"></i>Calles</a></li>
                            <li><a href="<?php echo base_url("administrativo/tramos/");?>"><i class="bx bx-data bx-xs"></i>Tramos</a></li>
                            <li><a href="<?php echo base_url("administrativo/categorias_const/");?>"><i class="bx bx-data bx-xs"></i>Categorías Construcción</a></li>
                            <li><a href="<?php echo base_url("administrativo/terrenos/");?>"><i class="bx bx-data bx-xs"></i>Terrenos</a></li>
                            <li><a href="<?php echo base_url("administrativo/construcciones/");?>"><i class="bx bx-data bx-xs"></i>Construcciones</a></li>
                            <li><a href="<?php echo base_url("administrativo/propietarios/");?>"><i class="bx bx-data bx-xs"></i>Propietarios</a></li>
                            <li><a href="<?php echo base_url("administrativo/demt/");?>"><i class="bx bx-data bx-xs"></i>Deméritos Terreno</a></li>
                            <li><a href="<?php echo base_url("administrativo/demc/");?>"><i class="bx bx-data bx-xs"></i>Deméritos Construcción</a></li>
                            
                           
                            
                    </ul></li>
                    <li><a href="<?php echo base_url("administrativo/roles/");?>"><i class="bx bx-check-shield bx-xs" style="color: #480912"></i> Roles</a></li>
                        <li><a href="<?php echo base_url("administrativo/usuarios/");?>"><i class="mdi mdi-account-key-outline" style="color: #480912"></i> Usuarios</a></li> 
                        <li><a href="<?php echo base_url("administrativo/empleados/");?>"><i class="mdi mdi-account-tie-outline" style="color: #480912"></i> Empleados</a></li> 
                        <li><a href="<?php echo base_url("administrativo/efirma/");?>"><i class="mdi mdi-certificate" style="color: #480912"></i> Firma Digital</a></li>
                        <li><a href="<?php echo base_url("administrativo/envinspeccion/");?>"><i class="mdi mdi-cube-send" style="color: #480912"></i> Envios a Inspección</a></li>     
                        <li><a href="<?php echo base_url("administrativo/envgraficacion/");?>"><i class="mdi mdi-cube-send" style="color: #480912"></i>Envios a Graficación</a></li>                    
                    </ul> 
                    
                </li>

                <li><a href="<?php echo base_url("administrativo/cedulacatastral");?>"><small><i class="mdi mdi-folder-home-outline" style="color: #480912"></i>Consultar Ficha Catastral</a></small></li>

                <?php endif; ?>

               

                <li class="menu-title" >Trámites</li>
                <?php 
                //Validamos si esta en el grupo de ventanilla o administracion
                if( in_groups(['Ventanilla','Administracion'], user_id()) ) : 
                ?>
                <li class="">
                <a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; ">
                    <i class="mdi mdi-barcode-scan" style="color: #480912"></i>
                        <span>Ventanilla</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?php echo base_url("ventanilla/");?>"><i class="mdi mdi-comment-text-outline" style="color: #480912"></i> <small>Solicitud de Tramites</small></a></li>
                        <li><a href="<?php echo base_url("ventanilla/avisocdi/");?>"><i class="mdi mdi-file-document-box-outline" style="color: #480912"></i><small> Cambios de Inscripción</small></a></li>
                        <!--<li><a href="<?php echo base_url("ventanilla/avisocdimanzana/");?>"><i class="mdi mdi-file-document-box-multiple-outline" style="color: #480912"></i> 
                        <small>Cambios al Padrón por Manzana</small></a></li>

                        <li><a href="<?php echo base_url("ventanilla/oficiosbonificacion/");?>"><i class="mdi mdi-account-cash-outline" style="color: #480912"></i> 
                        <small>Oficios de Bonificación</small></a></li>-->

                        <li><a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; "><i class="mdi mdi-printer" style="color: #480912"></i> Reportes</a>
                        <ul class="sub-menu" aria-expanded="false">             
                                <li><a href="<?php echo base_url("ventanilla/reporte_listado_declaraciones/");?>"><small>Listado de Declaraciones Recibidas</small></a></li>
                                <li><a href="<?php echo base_url("ventanilla/reporte_numero_declaraciones/");?>"><small>Listado de Número Trámites</small></a></li>
                                <li><a href="<?php echo base_url("ventanilla/reporte_primer_registro/");?>"><small>Listado Primer Registro</small></a></li>
                                <li><a href="<?php echo base_url("ventanilla/reporte_inco_inspeccionadas/");?>"><small>Listado de Análsis Inconformidades Inspeccionadas</small></a></li>
                                <li><a href="<?php echo base_url("ventanilla/reporte_inco_por_clavecat/");?>"><small>No. Inconformidades Recibidas por Clave Catastral</small></a></li>
                                <li><a href="<?php echo base_url("ventanilla/reporte_cuartel/");?>"><small>Por Zonas</small></a></li>
                                <li><a href="<?php echo base_url("ventanilla/reporte_cdi/");?>"><small>Listado C.D.I.</small></a></li>
                                <li><a href="<?php echo base_url("ventanilla/reporte_oficiosbonificacion/");?>"><small>Listado Oficios Bonificación</small></a></li>
                            </ul>
                        </li>
                        
                    </ul>
                </li>
                <?php endif; ?>
                <?php 
                //Validamos si esta en el grupo de ventanilla o administracion
                if( in_groups(['Ventanilla','Administracion', 'Seguimiento'], user_id()) ) : 
                ?>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; ">
                        <i class="mdi mdi-calendar-multiselect" style="color: #480912"></i>
                        <span>Seguimiento Trámites</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?php echo base_url("seguimiento/ventanilla/");?>"><i class="mdi mdi-barcode-scan" style="color: #480912"></i><small>Trámites de Ventanilla</small></a></li>
                        
                        <li><a href="<?php echo base_url("seguimiento/cdi/");?>"><i class="mdi mdi-home-edit-outline" style="color: #480912"></i> <small>Cambios de Inscripción</small></a></li>
                        <li><a href="<?php echo base_url("seguimiento/bandeja/");?>"><i class="mdi mdi-archive-outline" style="color: #480912"></i><small>Bandeja de Trámites-Rezago</small></a></li>

                        <li><a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; "><i class="mdi mdi-printer" style="color: #480912"></i> Reportes</a>
                        <ul class="sub-menu" aria-expanded="false">             
                                <li><a href="<?php echo base_url("seguimiento/reporte_movimiento_graficados/");?>"><small>Movimientos Graficados</small></a></li>
                                <li><a href="<?php echo base_url("seguimiento/reporte_movimientos_graficados_enviados/");?>"><small>Movimientos Graficados Enviados a Captura</small></a></li>
                                <li><a href="<?php echo base_url("seguimiento/reporte_tramites_detenidos/");?>"><small>Relación de Trámites Detenidos </small></a></li>
                                <li><a href="<?php echo base_url("seguimiento/reporte_inco_con_respuesta_Captura/");?>"><small>Tramites de Ventanilla Enviados a Captura (CON RESPUESTA)</small></a></li>
                                <li><a href="<?php echo base_url("seguimiento/reporte_inco_sin_respuesta_Captura/");?>"><small>Tramites de Ventanilla Enviados a Captura (SIN RESPUESTA)</small></a></li>
                                <li><a href="<?php echo base_url("seguimiento/reporte_tramites_sin_enviar_Captura/");?>"><small>Tramites de Ventanilla sin enviar a Captura</small></a></li>
                                <li><a href="<?php echo base_url("seguimiento/reporte_seguimiento_ventanilla/");?>"><small>Seguimiento Administrativo de Trámites de Ventanilla</small></a></li>
                                <li><a href="<?php echo base_url("seguimiento/reporte_regresados_Captura/");?>"><small>Trámites regresados por Captura</small></a></li>
                                <li><a href="<?php echo base_url("seguimiento/reporte_tramites_notarios/");?>"><small>Trámites por Notarios</small></a></li>
                                
                            </ul>
                        </li>

                       
                    </ul>
                    
                </li>
                <?php endif; ?>

                <li class="menu-title" >Áreas Operativas</li>

                <?php 
                //Validamos si esta en el grupo de Inspeccion o administracion
                if( in_groups(['Inspeccion','Administracion'], user_id()) ) : 
                ?>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; ">
                        <i class="mdi mdi-image-search-outline" style="color: #480912"></i>
                        <span>Inspección</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; "><i class="mdi mdi-barcode-scan mdi-18px" style="color: #480912"></i> Ventanilla</a>
                        <ul class="sub-menu" aria-expanded="false">      
                                <li><a href="<?php echo base_url("inspeccion/consulta_ventanilla/");?>"><small> Dictámenes de Ventanilla a Ispección</small></a></li>
                                
                                <li><a href="<?php echo base_url("inspeccion/consulta_graficacion/");?>"><small> Envíos de Ventanilla a Graficación</small></a></li>
                            </ul>
                        </li>
                        <li><a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; "><i class="mdi mdi-incognito mdi-18px" style="color: #480912"></i> Regularización</a>
                        <ul class="sub-menu" aria-expanded="false">
                                           
                                <li><a href="<?php echo base_url("inspeccion/consulta_regularizacion/");?>"><small>Dictámenes de Inspección de Regularización</small></a></li>
                                
                                <li><a href="<?php echo base_url("inspeccion/consulta_graficacionr/");?>"><small>Envíos Regularización-Graficación</small></a></li>
                            </ul>
                        </li>
                        <li><a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; "><i class="mdi mdi-printer" style="color: #480912"></i> Reportes</a>
                        <ul class="sub-menu" aria-expanded="false">   
                                <li><a href="<?php echo base_url("inspeccion/reporte_movimientos_ventanilla/");?>"><small>Resumen de Inspección (Ventanilla)</small></a></li>
                                <li><a href="<?php echo base_url("inspeccion/reporte_inspeccion_movimiento_ventanilla/");?>"><small>Predios Inspeccionados por Movimiento (Ventanilla)</small></a></li>
                                <li><a href="<?php echo base_url("inspeccion/reporte_inspeccion_movimiento_brigada_ventanilla/");?>"><small>Resumen de Movimientos de Ventanilla por Brigada</small></a></li>
                                <li class="border-bottom"></li>        
                                <li><a href="<?php echo base_url("inspeccion/reporte_movimientos_regularizacion/");?>"><small>Resumen de Inspección (Regularización)</small></a></li>
                                <li><a href="<?php echo base_url("inspeccion/reporte_inspeccion_brigada_regularizacion/");?>"><small>Predios Inspeccionados por Brigada (Regularización)</small></a></li>
                                <li><a href="<?php echo base_url("inspeccion/reporte_inspeccion_movimiento_regularizacion/");?>"><small>Predios Inspeccionados por Movimiento (Regularización)</small></a></li>
                                <li><a href="<?php echo base_url("inspeccion/reporte_inspeccion_movimiento_brigada_regularizacion/");?>"><small>Resumen de Movimientos de Regularización por Brigada</small></a></li>
                               
                            </ul>
                        </li>
                        
                    </ul>
                </li>
                <?php endif; ?>

                <?php 
                //Validamos si esta en el grupo de Graficacion o administracion
                if( in_groups(['Graficacion','Administracion'], user_id()) ) : 
                ?>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; ">
                        <i class="mdi mdi-vector-polyline-edit" style="color: #480912"></i>
                        <span>Graficación</span>
                    </a>

                        <ul class="sub-menu" aria-expanded="true">
                        <li><a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; "><i class="mdi mdi-barcode-scan mdi-18px" style="color: #480912"></i> Ventanilla</a>
                        <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?php echo base_url("graficacion/consulta_ventanilla/");?>"><small>Consultar Graficaciones de ventanilla</small></a></li>
                        <li><a href="<?php echo base_url("graficacion/asignacion_trabajo/");?>"><small>Asignación de Trabajo a los Graficadores</small></a></li>                
                         
                               
                            </ul>
                        </li>
                        <li><a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; "><i class="mdi mdi-incognito mdi-18px" style="color: #480912"></i> Regularización</a>
                        <ul class="sub-menu" aria-expanded="false">                      
                                <li><a href="<?php echo base_url("graficacion/dictamenes_regularizacion/");?>"><small>Dictámenes de Graficaciónes de Regularización</small></a></li>
                                <li><a href="<?php echo base_url("graficacion/movimientos_regularizacion_ventanilla/");?>"><small>Movimientos Regularizacion - Ventanilla</small></a></li>
                            </ul>
                        </li>

                          <li><a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; "><i class="mdi mdi-printer" style="color: #480912"></i> Reportes</a>
                        <ul class="sub-menu" aria-expanded="false">   
                                <li><a href="<?php echo base_url("graficacion/reporte_movimientos_ventanilla/");?>"><small>Resumen de Graficaciones (Ventanilla)</small></a></li>
                                <li><a href="<?php echo base_url("graficacion/reporte_movimientos_graficador_ventanilla/");?>"><small>Resumen de Ventanilla por Graficador</small></a></li>
                                <li><a href="<?php echo base_url("graficacion/reporte_tramites_detenidos/");?>"><small>Listado de Trámites Detenidos</small></a></li>
                                <li><a href="<?php echo base_url("graficacion/reporte_tramites_enviados_Captura/");?>"><small>Listado de Trámites Enviados a Captura</small></a></li>
                                <li class="border-bottom"></li>        
                                <li><a href="<?php echo base_url("graficacion/reporte_oficios_sin_folio/");?>"><small>Oficios Pendientes de Capturar Folio</small></a></li>
                                <li class="border-bottom"></li>        
                                <li><a href="<?php echo base_url("graficacion/reporte_movimientos_regularizacion/");?>"><small>Resumen de Graficaciones (Regularización)</small></a></li>
                                <li><a href="<?php echo base_url("graficacion/reporte_movimientos_graficador_regularizacion/");?>"><small>Resumen de Regularizacion por Graficador</small></a></li>
                                <li><a href="<?php echo base_url("graficacion/reporte_resumen_tipo_resultado_regularizacion/");?>"><small>Resumen por Tipo de Resultado (Regularizacion)</small></a></li>
                                <li><a href="<?php echo base_url("graficacion/reporte_regularizacion_enviada_Captura_con_respuesta/");?>"><small>Regularizacion Enviada al Captura con Respuesta</small></a></li>
                                <li><a href="<?php echo base_url("graficacion/reporte_regularizacion_enviada_Captura_sin_respuesta/");?>"><small>Regularizacion Enviada al Captura sin Respuesta</small></a></li>
                            </ul>
                        </li>    
                        
                    </ul>     
                       
                </li>
                <?php endif; ?>

               

                <?php 
                //Validamos si esta en el grupo de Graficacion o administracion
                if( in_groups(['Valores', 'Verificacion','Administracion'], user_id()) ) : 
                ?>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; ">
                        <i class="mdi mdi-alert-circle-check-outline" style="color: #480912"></i>
                        <span>Verificación</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false" style="margin-left:-5px;">
                        
                        <li><a href="<?php echo base_url("verificacion/verificacion/");?>"><i class="mdi mdi-file-document-box-check-outline" style="color: #480912"></i><small>Tramites Ventanilla</small></a></li> 

                        <li><a href="<?php echo base_url("verificacion/verificacioncdi/");?>"><i class="mdi mdi-smart-card-outline" style="color: #480912"></i><small>Cambios al Padron</small></a></li> 

                        <li><a href="<?php echo base_url("verificacion/avaluoscomerciales/");?>"><i class="mdi mdi-home-currency-usd" style="color: #480912"></i><small>Avaluos Comerciales</small></a></li>
                    </ul>
                </li>

                <?php endif; ?>

                
                <?php 
                //Validamos si esta en el grupo de Seguimiento Ventanilla o administracion
                if( in_groups(['Seguimiento','Administracion'], user_id()) ) : 
                ?>
               <!--<li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; ">
                        <i class="mdi mdi-bank-outline" style="color: #480912"></i>
                        <span>Juridico</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false" style="margin-left:-5px;">
                        
                        <li><a href="javascript: void(0);" class="waves-effe" style="color: #480912; "><i class="mdi mdi-check-circle" style="color: #480912"></i> <small>Revisión de Requisitos Juridicos</small></a>
                            
                        </li>
                    </ul>
                </li>-->
                <?php endif; ?>

                <?php 
                //Validamos si esta en el grupo de Graficacion o administracion
                if( in_groups(['Captura','Administracion'], user_id()) ) : 
                ?>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; ">
                        <i class="mdi mdi-database-edit" style="color: #480912"></i>
                        <span>Captura</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?php echo base_url("captura/");?>"><small><i class="mdi mdi-file-document-edit-outline mdi-18px" style="color: #480912"></i> Captura de Trámites</small></a></li>
                        <li><a href="<?php echo base_url("captura/cdi");?>"><small> <i class="mdi mdi-file-document-edit-outline mdi-18px" style="color: #480912"></i> Captura de C.D.I.</small></a></li>
                    </ul>
                </li>
                

                <li>
                    
                    <a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; ">
                        <i class="mdi mdi-certificate-outline" style="color: #480912"></i>
                        <span>Avaluos</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?php echo base_url("administrativo/avaluos");?>"><small><i class="mdi mdi-certificate-outline" style="color: #480912"></i>Generar Avaluos</a></small></li>
                        <li><a href="<?php echo base_url("administrativo/cedulacatastral");?>"><small><i class="mdi mdi-folder-home-outline" style="color: #480912"></i>Consultar Ficha</a></small></li>
                    </ul>
                   
                </li>
                <?php endif; ?>

                <?php 
                //Validamos si esta en el grupo de Inspeccion o administracion
                if( in_groups(['Perito','Administracion'], user_id()) ) : 
                ?>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; ">
                        <i class="mdi mdi-account-group" style="color: #480912"></i>
                        <span>Peritos</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">   
                        <li><a href="<?php echo base_url("pagos/");?>" target="_new"><i class="mdi mdi-cash-marker" style="color: #480912"></i> <small>Informacion Catastral</small></a></li>
                        <li><a href="<?php echo base_url("peritos/avaluos");?>"><i class="mdi mdi-home-lightbulb-outline" style="color: #480912"></i> <small>Avaluo Catastral</small></a></li>
                        <li><a href="<?php echo base_url("peritos/configuracion");?>"><i class="mdi mdi-settings-outline" style="color: #480912"></i> <small>Configuracion</small></a></li>
                    </ul>
                </li>
                <?php endif; ?>

               

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect" style="color: #480912; ">
                        <i class="bx bx-map-pin" style="color: #480912"></i>
                        <span>Visor</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="<?php echo substr(site_url(), 0, -1).':1234/'; ?>" target="_new"> <i class="mdi mdi-map-search-outline" style="color: #480912"></i><small>Consultar Visor</small></a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->