<header id="page-topbar">
    <div class="navbar-header">
        <div class="container-fluid">
            <div class="float-right">

                <div class="dropdown d-inline-block d-lg-none ml-2">
                    <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="mdi mdi-magnify"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0" aria-labelledby="page-header-search-dropdown">

                        <form class="p-3">
                            <div class="form-group m-0">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Buscar ..." aria-label="Recipient's username">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="dropdown d-none d-lg-inline-block ml-1">
                    <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                        <i class="mdi mdi-fullscreen"></i>
                    </button>
                </div>

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-notifications-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  style="width:80px">
                        <i class="mdi mdi-bell-outline"></i>
                        <span class="badge badge-danger badge-pill">0</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0" aria-labelledby="page-header-notifications-dropdown">
                        <div class="p-3">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h6 class="m-0"> Notificaciones </h6>
                                </div>
                                <div class="col-auto">
                                    <a href="#!" class="small"> Ver todas</a>
                                </div>
                            </div>
                        </div>
                        <div data-simplebar style="max-height: 230px;">
                            <!--<a href="" class="text-reset notification-item">
                                    <div class="media">
                                        <div class="avatar-xs mr-3">
                                            <span class="avatar-title bg-primary rounded-circle font-size-16">
                                                <i class="bx bx-cart"></i>
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="mt-0 mb-1">Your order is placed</h6>
                                            <div class="font-size-12 text-muted">
                                                <p class="mb-1">If several languages coalesce the grammar</p>
                                                <p class="mb-0"><i class="mdi mdi-clock-outline"></i> 3 min ago</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>-->
                        </div>
            <div class="p-2 border-top">
                <a class="btn btn-sm btn-link font-size-14 btn-block text-center" href="javascript:void(0)">
                    <i class="mdi mdi-arrow-right-circle mr-1"></i> Ver más..
                </a>
            </div>
        </div>
    </div>

    <div class="dropdown d-inline-block">
        <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="rounded-circle header-profile-user" src="<?php echo base_url("assets/images/users/user.jpg");?>" alt="Header Avatar">
            <span class="d-none d-xl-inline-block ml-1"> <?php echo $usuario->username; ?></span>
            <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right">
            <!-- item-->
                            <a class="dropdown-item" href="<?php echo base_url("home/perfil"); ?>"><i class="bx bx-user font-size-16 align-middle mr-1"></i> Perfil</a>
                           
                            <div class="dropdown-divider"></div>
                            
                            <a class="dropdown-item text-danger" href="<?php echo base_url("auth/logout");?>"><i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> Salir</a>
                        </div>
                </div>
            </div>
            <div>
                <!-- LOGO -->
                <div class="navbar-brand-box">
                    <a href="/sistema/" class="logo logo-dark">
                        <span class="logo-sm">
                            
                        </span>
                        <span class="logo-lg">
                            <h5 class="text-white mt-4">Sistema de Administracion Catastral</h5>
                        </span>
                    </a>

                    <a href="<?php echo base_url("/");?>" class="logo logo-light">
                        <span class="logo-sm">
                        </span>
                        <span class="logo-lg">
                            <!--<img src="<?php //echo base_url("assets/images/logo_letras.svg");?>" alt="" height="15">-->
                            <h5 class="text-white mt-4">Sistema de Administracion Catastral</h5>
                        </span>
                    </a>
                </div>

                <button type="button" class="btn btn-sm px-3 font-size-16 header-item toggle-btn waves-effect" id="vertical-menu-btn">
                    <i class="fa fa-fw fa-bars"></i>
                </button>

                <!-- App Search
                <form class="app-search d-none d-lg-inline-block">
                    <div class="position-relative">
                        <input type="text" class="form-control" placeholder="Buscar...">
                        <span class="bx bx-search-alt"></span>
                    </div>
                </form>-->


            </div>

        </div>
    </div>
</header>