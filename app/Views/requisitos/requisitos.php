<div class="container mt-5">
    <a href="<?php echo base_url('public/index.php/requisitos/crear/') ?>" class="btn btn-success mb-2">Crear</a>
    <?php
     if(isset($_SESSION['msg'])){
        echo $_SESSION['msg'];
      }
     ?>
  <div class="row mt-3">
     <table class="table table-bordered" id="tramites">
       <thead>
          <tr>
             <th>COD</th>
             <th>Descripcion</th>
             <th>Acciones</th>
          </tr>
       </thead>
       <tbody>
          <?php if($requisitos): ?>
          <?php foreach($requisitos as $t): ?>
          <tr>
             <td><?php echo $t['cod_requisito']; ?></td>
             <td><?php echo $t['descripcion']; ?></td>
             <td>
              <a href="<?php echo base_url('public/index.php/tramites/editar/'.$t['cod_requisito']);?>" class="btn btn-success">Editar</a>
              <a href="<?php echo base_url('public/index.php/tramites/eliminar/'.$t['cod_requisito']);?>" class="btn btn-danger">Eliminar</a>
              </td>
          </tr>
         <?php endforeach; ?>
         <?php endif; ?>
       </tbody>
     </table>
  </div>
</div>
 