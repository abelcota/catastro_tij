<?php header('Access-Control-Allow-Origin: *'); ?>
<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">RECEPCION DE SOLICITUDES</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">VENTANILLA</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="btn-toolbar p-3" role="toolbar">
                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_nuevo"><i class="fa fa-plus"></i> Nuevo</button>
                                            <button type="button" id="btn_listado" class="btn btn-primary waves-light waves-effect"><i class="fa fa-list"></i> Ver Listado</button>
                                            <button type="button" class="btn btn-secondary waves-light waves-effect" id="btn_limpiar"><i class="fa fa-ban"></i> Limpiar Pantalla</button>                                        </div>

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button disabled type="button" class="btn btn-primary waves-light waves-effect dropdown-toggle" data-toggle="dropdown" aria-expanded="false" id="btn_mas">
                                                Más <i class="mdi mdi-dots-vertical ml-2"></i>
                                            </button>
                                            <div class="dropdown-menu" style="">
                                                <a class="dropdown-item" href="#" id="btn_editar"><i class="fa fa-edit"></i> Editar</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Borrar Registro</a>
                                                <a class="dropdown-item" href=":;" id="btn_imprimir"><i class="fa fa-print"></i> Imprimir</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end toolbar -->

                          <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                     <h4 class="card-title">Atención a Declaraciones de Contribuyentes</h4>
                                     <p class="card-title-desc"></p>
                                     <!-- start form -->
                                     <form class="needs-validation" id="form-ventanilla" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="year">Año:</label>
                                                    <input type="text" id="year" name="year" readonly class="form-control" placeholder="Año" required>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="folio">Folio:</label>
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                    <input type="text" class="form-control"  placeholder="Folio" id="folio" name="IdInconformidad" required>
                                                    <span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_gen_fol">+</button></span></div>
                                                    <!--hidden inputs -->
                                                    <input type="hidden" id="IdPropietario" name="IdPropietario" value="0">
                                                    <input type="hidden" id="IdUso" name="IdUso">
                                                    <input type="hidden" id="Clave" name="Clave">
                                                    <input type="hidden" id="NombrePropietario" name="NombrePropietario">
                                                    <input type="hidden" id="IdCalleSolicitante" name="IdCalleSolicitante">
                                                    <input type="hidden" id="NumeroOficialSolicitante" name="NumeroOficialSolicitante">
                                                    <input type="hidden" id="OrientacionSolicitante" name="OrientacionSolicitante">
                                                    <input type="hidden" id="IdColoniaSolicitante" name="IdColoniaSolicitante">
                                                    <input type="hidden" id="Codig  oPostalSolicitante" name="CodigoPostalSolicitante">
                                                    <input type="hidden" id="OtrosDocumentos" name="OtrosDocumentos">
                                                    <input type="hidden" id="Fol_Mov" name="Fol_Mov">
                                                    <input type="hidden" id="Fec_Ava" name="Fec_Ava">
                                                    <input type="hidden" id="cve_concep" name="cve_concep">
                                                    <input type="hidden" id="observa01" name="observa01">
                                                    <input type="hidden" id="fec_Avaluo" name="fec_Avaluo">
                                                    <input type="hidden" id="TramiteICES" name="TramiteICES">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral:</label>
                                                    
                                                            <div class="input-group">
                                                               
                                                                <input type="text" class="form-control number-references" name="CTEL" placeholder="ZONA" id="CTEL" required maxlength="3" required list="list_cuarteles"><datalist id="list_cuarteles"></datalist>
                                                                
                                                                <input type="text" class="form-control number-references" placeholder="MANZ" id="MANZ" name="MANZ" required maxlength="3" required list="list_manzanas">
                                                                <datalist id="list_manzanas"></datalist>

                                                                <input type="text" class="form-control number-references" name="PRED" placeholder="PRED" id="PRED" required maxlength="3" required list="list_predios">
                                                                <datalist id="list_predios"></datalist>

                                                                <button class="btn btn-primary input-group-append" type="button" id="btn_validar"><i class="mdi mdi-check"></i> Validar</button>

                                                            </div>
                                                   
                                                </div>
                                            </div>
                                             <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Fecha:</label>
                                                    <input type="date" class="form-control" id="dtfecha"  name="dtfecha" data-date-format="DD/MM/YYYY" required style="font-size:11px;">
                                                 
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                       
                                            <div class="col-lg-9">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label for="validationTooltip03">Apellido Paterno</label>
                                                            <input type="text" class="form-control number-references" readonly id="appat" name="appat" value="">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="validationTooltip04">Nombre(s)</label>
                                                            <input type="text" class="form-control number-references" readonly id="nombres" name="nombres" value="">
                                                        </div>
                                                        
                                                </div>
                                                <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label for="validationTooltip04">Apellido Materno</label>
                                                            <input type="text" class="form-control number-references" readonly id="apmat" name="apmat" value="">
                                                        </div>
                                                        

                                                        <div class="form-group">
                                                            <label for="validationTooltip03">Razon Social</label>
                                                            <input type="text" class="form-control " id="curp" name="curp" value="">
                                                        </div>
                                                </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 text-center">
                                                
                                                <label for="validationTooltip04">Fotografía</label><br>
                                                <img id="foto" src="assets/images/users/user.jpg" alt="" width="130"  class="img-thumbnail circle" style="cursor: pointer;">
                                                
                                                <input type="hidden" class="form-control image-tag" name="image">
                                        </div>
                                            
                                                
                                        </div>
                                          <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="idcalle">Calle:</label>
                                                    <div class="row">
                                                        <div class="col-10">
                                                            <input type="text" class="form-control number-references" id="nombrecalle" name="nombrecalle" readonly>
                                                        </div>
                                                       
                                                    </div>
                                                    <!--<select class="form-control select2" id="idcalle" name="idcalle"></select>-->
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="idcolonia">Colonia:</label>
                                                    <div class="row">
                                                       
                                                        <div class="col-10">
                                                            <input type="text" class="form-control number-references" id="nombrecolonia" name="nombrecolonia" readonly>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip04">Numero Oficial:</label>
                                                    <input type="text" class="form-control number-references" readonly id="numeroof" name="numeroof" value="">
                                                </div>
                                            </div>
                                              <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip04">Uso</label>
                                                    <input type="text" class="form-control number-references" readonly id="uso" value="">
                                                </div>
                                            </div>
                                        </div>
                                          <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Superficie Fisica:</label>
                                                    <input type="text" class="form-control number-references" id="superficieterr" name="superficieterr" placeholder="Superficie" readonly value="0">
                                                </div>
                                            </div>
                                              <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                   <label for="validationTooltip02">Superficie Documental:</label>
                                                    <input type="text" class="form-control number-references"  id="superficiecons" name="superficiecons" placeholder="Construccion" readonly value="0">
                                                </div>
                                            </div>
                                              <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                   <label for="validationTooltip02">Valor Fiscal:</label>
                                                    <input type="text" class="form-control number-references" placeholder="Valor" id="valorcat" name="valorcat" readonly value="0">
                                                </div>
                                            </div>
                                        </div>
                                         <h3 class="card-title">Solicitante</h3>
                                         <div class="custom-control custom-switch mb-2" dir="ltr">
                                        <input type="checkbox" class="custom-control-input" id="chkpasardatos">
                                        <label class="custom-control-label" for="chkpasardatos">Pasar Datos</label>
                                    </div>
                                          <hr>
                                        
                                           <div class="row">
                                                <div class="col-lg-9">
                                                   <div class="row">
                                                   
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Nombre:</label>
                                                    <input type="text" class="form-control" placeholder="Nombre del Solicitante" id="nom_sol" name="nom_sol" required>
                                                </div>
                                            </div>
                                              <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                   <label for="validationTooltip02">Domicilio:</label>
                                                    <input type="text" class="form-control" placeholder="Domicilio del Solicitante" id="dom_sol" name="dom_sol" required>
                                                </div>
                                            </div>
                                              <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                   <label for="validationTooltip02">Telefono:</label>
                                                    <input type="text" class="form-control" placeholder="Telefono del Solicitante" id="tel_sol" name="tel_sol" required>
                                                </div>
                                            </div>
                                               <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="tramites">Tramite:</label>
                                                      <select class="form-control required" id="tramites" name="tramites" required>
                                                        <option value="">Seleccionar Tramite</option>    
                                                      </select>
                                                      <input type="hidden" id="IdDeclaracion" name="IdDeclaracion">
                                                </div>
                                            </div>
                                            
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="tramitenot">Tramite por Notario:</label>
                                                    <input type="text" class="form-control" placeholder="Notario" id="tramitenot" name="tramitenot" maxlength="3" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="IdEpleadoBarra">Atendió:</label>
                                                   
                                                   <select class="form-control select2" id="empleados" name="IdEpleadoBarra" required>
                                                      <option>Seleccionar Empleado</option>
                                                      </select>
                                                </div>
                                            </div>
                                            
                                              <div class="col-md-12">
                                                <div class="form-group position-relative">
                                                   <label for="motivo">Motivo de la Declaración:</label>
                                                     <textarea class="form-control" rows="3" id="motivo" name="motivo" required></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                            <div class="form-group position-relative">
                                            <label for="validationTooltip02">Adjuntar Documentos de Requisitos:</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="archivos[]" name="archivos[]" multiple accept="pdf">
                                                <label class="custom-file-label text-truncate" for="customFile" data-browse="Buscar...">Seleccionar Archivos</label>
                                            </div>
                                                
                                                                                                
                                            </div>
                                        </div>
                                                </div>
                                            </div>
                                                <div class="col-lg-3">
                                                    <h4 class="card-title">Requisitos</h4>
                                                    <hr>
                                                    <div id="requisitos_list" name="requisitos_list">
                                                     
                                                    </div>
                                                    
                                                </div>
                                        </div>
                                        
                                        <div class="col-md-12">
                                                <div class="form-group position-relative">    
                                                   <div id="map" class="map" style="height: 400px; max-width: 900px;"></div>
                                                   
                                        </div>

                                        <div class="errorTxt invalid-feedback" style="margin-bottom: 15px;"></div>

                                        <button class="btn btn-success" type="button" id="btn_guardar">Guardar</button>
                                         <button class="btn btn-primary" type="button" id="btn_actualizar">Actualizar</button>
                                    </form>
                                    <!-- end form -->
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            <!--modal para ver el listado de solicitudes -->
            <div class="modal fade modal-listado" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                <div class="modal-dialog modal-xl modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Buscar todas las Declaraciones</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                               <table class="table table-hover no-footer" id="tabla_declaraciones" width="100%">
                                                                <thead class="text-white" style="background-color: #480912;">
                                                                <tr>
                                                                    <th></th>
                                                                    <th>Año</th>
                                                                    <th>Folio</th>
                                                                    <th>Clave</th>
                                                                    <th>Trámite</th>
                                                                    <th>Nombre Solicitante</th>
                                                                    
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                               </table>      
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar">Cerrar</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
            <!--en modal -->
            

                        <!--modal para ver el listado de solicitudes -->
                        <div class="modal fade modal-foto" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                <div class="modal-dialog modal-lg modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Capturar Fotografía</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                        <center>  
                                                            <div id="my_camera"></div><br>
                                                                <input type="button" class="btn btn-primary" value="Capturar" onClick="take_snapshot()">
                                                            </div>
                                                            </center>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
            <!--en modal -->
            