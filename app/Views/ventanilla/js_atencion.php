  <!-- JAVASCRIPT -->
  <script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>
    

 <!-- Required datatable js -->
 <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    

    <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
    <!-- webcam -->
    <script src="<?php echo base_url("assets/js/webcam.min.js");?>"></script>

    <!-- App js -->
    <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
    <script src="<?php echo base_url("/assets/js/jquery.validate.js");?>"></script>
    <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>

     <script>          
        function take_snapshot() {
            var shutter = new Audio();
            shutter.autoplay = true;
            shutter.src = '<?php echo base_url("assets/preview.mp3");?>';

            // play sound effect
            shutter.play();

            Webcam.snap( function(data_uri) {
                //console.log(data_uri);
                var data = data_uri.split(",");
                $(".image-tag").val(data[1]);
                //document.getElementById('results').innerHTML = '';

                $("#foto").attr('src', data_uri );
                $("#foto").attr('width', '200' );
                $("#foto").attr('height', '150' );
                $(".modal-foto").modal("hide");
                Webcam.reset();
            } );
        }

         // Create our number formatter.
         var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',

            // These options are needed to round to whole numbers if that's what you want.
            //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
            //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
            });

         $(document).ready(function () {
            
            $("#btn_actualizar").hide();
             
             get_anio();
             get_fecha();
             cargar_tramites();
             cargar_empleados();
             cargar_cuarteles();
             /*$('#POBL').blur(function () {  this.value = zeroPad(this.value, 3); });
             $('#CTEL').blur(function () { this.value = zeroPad(this.value, 3);  });
             $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
             $('#PRED').blur(function () { this.value = zeroPad(this.value, 3);  });
             $('#UNID').blur(function () { this.value = zeroPad(this.value, 3);  });*/

            var table = $("#tabla_declaraciones").DataTable({
                ajax: "<?php echo base_url("ventanilla/listadodeclaraciones")?>/"+get_anio(),
                Processing : true,
                ServerSide : true,
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                columns : [
                    {
                        data: null,
                        defaultContent: '<a class="consultar text-primary" style="cursor:pointer !important;"> <i class="dripicons-information"></i></a>'
                    },
                    { "data" : "AnioDeclaracion" },
                    { "data" : "IdInconformidad" },
                    { "data" : "clavef" },
                    { "data" : "DescripcionTramite" },
                    { "data" : "NombreSolicitante" },     
                ],
                rowCallback: function(row, data, index) {
                    $("td:eq(1)", row).addClass("font-weight-bold"); 
                    $("td:eq(2)", row).addClass("font-weight-bold text-primary"); 
                    $("td:eq(3)", row).addClass("text-nowrap"); 
                    $("td:eq(4)", row).addClass("text-wrap"); 

                }
            });

            //table.ajax.reload("null", false); 
            $('#tabla_declaraciones tbody').on( 'click', '.consultar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                var a = data["AnioDeclaracion"];
                var fol = data["IdInconformidad"];

                Swal.fire({
                title: '¿Consultar el folio: '+fol+' del año '+a+'?',
                type: 'info',
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Consultar!'
                }).then((result) => {
                    if (result.value) {
                    consulta_folio(a, fol);
                    $("#folio").val(fol);
                    $("#year").val(a);
                    $(".modal-listado").modal("hide");
                    $("#btn_guardar").prop("disabled", true);
                    $("#btn_mas").prop("disabled", false);

                    //deshabilitar los controles
                    $("#form-ventanilla :input").prop("disabled", true);
                    $("#btn_validar").prop("disabled", false);
                    }
                })
                
            
            });
         });

         $(document).on('click','#btn_editar',function(e) {
            e.preventDefault();
            //habilitar los controles
            $("#form-ventanilla :input").prop("disabled", false);
            //aparecer el boton de actualizar
            $("#btn_actualizar").show();
            $("#btn_guardar").hide();
         });

         function cargar_tramites(){
            $.getJSON("<?php echo base_url("padron/tramites/");?>", function(json){
                    $('#tramites').empty();
                    $('#tramites').append($('<option>').text("Seleccionar Tramite").attr({ 'value' : ""}));
                    $.each(json.result, function(i, obj){
                            $('#tramites').append($('<option>').text(obj.DescripcionTramite).attr({ 'value' : obj.IdTramite, 'declaracion':obj.idDeclaracion}));
                    });
                    //$('#tramites').select2();
            });
         }

         function cargar_empleados(){
            $.getJSON("<?php echo base_url("padron/empleados/05");?>", function(json){
                    $('#empleados').empty();
                    $('#empleados').append($('<option>').text("Seleccionar Empleado").attr({'value':""}));
                    $.each(json.result, function(i, obj){
                            $('#empleados').append($('<option>').text(obj.Nombre).attr({ 'value' : obj.IdEmpleado}));
                    });
                    //$('#empleados').select2();
            });
         }

         function cargar_calles(pobl){
            $('#idcalle').empty();
            $.getJSON("<?php echo base_url("padron/calles2");?>/"+pobl, function(json){
                    $('#idcalle').append($('<option>').text("Seleccionar Calle").attr({'value': 0}));
                    $.each(json.results, function(i, obj){
                            $('#idcalle').append($('<option>').text(obj.id + " - " + obj.text).attr({'value': obj.id, 'calle': obj.text}));
                    });    
            });
            $('#idcalle').select2();
            
            /*$('#idcalle').select2({
               //ajax call
               ajax: {
                    url: "",
                    type: "post",
                    dataType: 'json',
                    delay: 200
               }
            });*/
                       
         }

         function cargar_colonias(pobl){
            $('#idcolonia').empty();
            $.getJSON("<?php echo base_url("padron/colonias");?>/"+pobl, function(json){
                    
                    $('#idcolonia').append($('<option>').text("Seleccionar Colonia").attr({'value': 0}));
                    $.each(json.result, function(i, obj){
                            var idcol = pobl + obj.CVE_COL;
                            $('#idcolonia').append($('<option>').text(obj.CVE_COL + " - " +obj.COLONIA).attr({'value': idcol, 'col': obj.COLONIA}));
                    });     
            });
            $('#idcolonia').select2();
         }

        $("#tramites").change(function() {
            if($(this).val()){
                var id = $(this).val();
                var dec = $(this).children('option:selected').attr('declaracion');
                $("#IdDeclaracion").val(dec);
                cargar_requisitos(id);
            }
        });


         function cargar_requisitos(id){
            var url = "<?php echo base_url("requisitos/get/");?>/"+id;
            $.getJSON(url, function(json){
                    $('#requisitos_list').empty();
                    //console.log(json);
                    $.each(json.result, function(i, obj){
                            var html = '<div class="form-check mb-2"><input class="form-check-input" type="checkbox" checked id="'+obj.IdRequisito+'" name="doc'+(i+1)+'"><label class="form-check-label" for="defaultCheck1">'+obj.DescripcionRequisito+'</label></div>';
                            $('#requisitos_list').append(html);
                    });
            });
         }

         
         
         
         $(document).on('click', '#btn_listado',  function(e){
            e.preventDefault();
            $('#tabla_declaraciones').DataTable().ajax.reload(null, false);
            $(".modal-listado").modal("show"); 

         });

         $(document).on('click', '#foto',  function(e){
            e.preventDefault();
            Webcam.set({
                //width: 640,
                //height: 480,
                width: 400,
                height: 300,
                image_format: 'jpeg',
                jpeg_quality: 90
            });
        
            Webcam.attach( '#my_camera' );
            $(".modal-foto").modal("show"); 

         });

         $(document).on('click', '#btn_gen_fol',  function(e){
            e.preventDefault();
            genera_folio(); 
             //calculamos el nuevo folio
             $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Generando Folio Nuevo...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
            
         });
          

        function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        }


        function consulta_folio(a, folio)
        {
            if (folio == ""){
                    Swal.fire({
                        icon: 'error',
                        text: 'Ingrese un folio válido para continuar',
                        })
                 }
                else{
                    
                $.ajax({
                url: "<?php echo base_url("ventanilla/consultafolio/");?>/"+a+"/"+folio+"",
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Folio...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                       console.log(data.data[0]);
                       var data = data.data;
                       if(data.length > 0){
                           
                            //CARGAR CALLES Y COLONIAS DEPENDIENDO LA POBLACION
                            //cargar_calles(parseInt(data[0].Poblacion));
                            //cargar_colonias(parseInt(data[0].Poblacion));
                            //agregar los valores a los controles
                                $("#POBL").val(data[0].Clave.substr(0, 3));
                                $("#CTEL").val(data[0].Clave.substr(3, 3));
                                $("#MANZ").val(data[0].Clave.substr(6, 3));
                                $("#PRED").val(data[0].Clave.substr(9, 3));
                                $("#UNID").val(data[0].Clave.substr(12, 3));
                                
                                //fecha de captura
                                $("#dtfecha").val(moment(data[0].FechaCapturaInconformidad).format('YYYY-MM-DD'));
                                $("#idcolonia").val(data[0].IdColonia);
                                $("#nombrecolonia").val(data[0].NOM_COL);
                                $('#idcalle').val(data[0].IdCalle);
                                $('#nombrecalle').val(data[0].NOM_CALLE);
                                //Calle y colonia
                                //Calle y colonia
                                //$('#idcolonia').append($('<option>').text(data[0].IdColonia + " - " + data[0].NOM_COL ).attr({ 'value':data[0].IdColonia, 'selected': true}) );
                                //$('#idcolonia').val(data[0].IdColonia).trigger('change.select2')
                                //$('#idcalle').append($('<option>').text(data[0].IdCalle + " - " + data[0].NOM_CALLE).attr({ 'value':data[0].IdCalle, 'selected':true }) );

                                

                                //$('#idcalle').val(data[0].IdCalle).trigger('change.select2')
                                //Numero interior y uso
                                $("#numeroof").val(data[0].NumeroOficial);
                                $("#uso").val(data[0].IdUso);
                                //superficies y valor
                                $("#superficiecons").val(data[0].TotalSuperficieConstruccion);
                                $("#superficieterr").val(data[0].TotalSuperficieTerreno);
                                $("#valorcat").val(data[0].ValorCatastral);
                                //nombre del solicitante, domicilio y telefono
                                $("#nom_sol").val(data[0].NombreSolicitante);
                                $("#dom_sol").val(data[0].DomicilioSolicitante);
                                $("#tel_sol").val(data[0].TelefonoSolicitante);
                                //tramites y motivo
                                $('#tramites').val(data[0].IdTramite).trigger('change');
                                $('#empleados').val(data[0].IdEmpleadoBarra).trigger('change');
                                $("#tramitenot").val(data[0].TramiteNotario);
                                $("#motivo").text(data[0].ObservacionesBarra);
                                //validar el vectorial
                                $('#btn_validar').click();
                                
                       }
                       else {
                           Swal.fire({
                               icon: 'error',
                               title: 'Folio no encontrado',
                               text: 'El folio no se encuentra registrado!'
                               
                           })
                       }
                       
                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });
                    
                    

            }

        }

        //Funcionalidad del folio al presionar enter
        $(document).on('keypress','#folio',function(e) {
            if(e.which == 13) {
                var folio = $(this).val();
                var a = $('#year').val();
                consulta_folio(a, folio);
            }
        });

         //Funcionalidad del folio al presionar enter
         $(document).on('dblclick','#folio',function(e) {
            genera_folio();
        });

        $(document).on('click', '#btn_validar', function () {
             //limpiamos el consultado
            if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }
            //_geojson_vectorSource.clear();
             //obtener los valores de los input de la clave
             var ctel = $('#CTEL').val();
             var manz = $('#MANZ').val();
             var pred = $('#PRED').val();

             //CARGAR CALLES Y COLONIAS DEPENDIENDO LA POBLACION
             //cargar_calles(parseInt(pobl));
             //cargar_colonias(parseInt(pobl));

             var cvecat = ctel + manz + pred;

             console.log("validando clave: " + cvecat)
             $.ajax({
                url: "<?php echo base_url("padron/validarcve");?>/"+cvecat,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Validando Clave Catastral...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                    console.warn("Validacion" , data.result);   
                    if(data.result.length > 0){
                        $("#appat").val(data.result[0].Paterno);
                        $("#apmat").val(data.result[0].Materno);
                        $("#nombres").val(data.result[0].PNombre + " " + data.result[0].SNombre);
                        $("#curp").val(data.result[0].RazonSocial);

                        $('#nombrecalle').val(data.result[0].Calle);

                        $('#nombrecolonia').val(data.result[0].Colonia);

                        $("#numeroof").val(data.result[0].NumeroOficial);
                        $("#uso").val(data.result[0].Uso_Predial);

                        $("#superficiecons").val(data.result[0].SuperficieFisica);
                        $("#superficieterr").val(data.result[0].SuperficieDocumental);

                        $("#valorcat").val(Number(parseFloat(data.result[0].ValorFiscal)));

                         //Se obtiene el array del poligono 
                         jsonobj = jQuery.parseJSON(data.result[0].jsonb_build_object);
                            //si encuentra el poligono en el vectorial
                            console.log(jsonobj);
                            if (jsonobj.features != null) {
                                //Se visualiza el poligono en el mapa
                                visualizar_capa(jsonobj);                                
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Vectorial no encontrado',
                                    text: 'La clave catastral no se encontró en el vectorial!'
                                    
                                })
                            }

                    }  
                    },
                   complete : function (){
                        $.unblockUI();
                    }
               });
         });

        $(document).on('click','#btn_limpiar',function(e) {
            if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }
            $("#form-ventanilla")[0].reset();
            $(".form-control").removeClass("is-valid");
            $(".form-control").removeClass("is-invalid");
            $('#motivo').text('');
            $('#requisitos_list').empty();
            $('#idcolonia').empty();
            $('#idcalle').empty();
            $('#tramites').val(null).trigger('change');
            $('#empleados').val(null).trigger('change');
            $("#foto").attr('src', 'assets/images/users/user.jpg' );
            get_anio();
            $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD"));
            $('#archivos').empty();
            $('.custom-file-label').text('Seleccionar Archivos...');
            $('.errorTxt').empty();
             //aparecer el boton de actualizar
             $("#btn_actualizar").hide();
            $("#btn_guardar").show();
             //deshabilitar los controles
             $("#form-ventanilla :input").prop("disabled", false);
        });

        $(document).on('click','#btn_nuevo',function(e) {
            $('#btn_limpiar').click();
            $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD"));  
             genera_folio();
             //calculamos el nuevo folio
             $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Generando Folio Nuevo...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
            
             
        });
        
        $(document).on('click','#btn_imprimir',function(e) {
            e.preventDefault();

            //Crear la captura del mapa
            var a = $("#year").val();
            var f = $("#folio").val();
            var img = "";
            map.once('rendercomplete', function () {
                var mapCanvas = document.createElement('canvas');
                var size = map.getSize();
                mapCanvas.width = size[0];
                mapCanvas.height = size[1];
                var mapContext = mapCanvas.getContext('2d');
                Array.prototype.forEach.call(
                document.querySelectorAll('.ol-layer canvas'),
                function (canvas) {
                    if (canvas.width > 0) {
                    var opacity = canvas.parentNode.style.opacity;
                    mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
                    var transform = canvas.style.transform;
                    // Get the transform parameters from the style's transform matrix
                    var matrix = transform
                        .match(/^matrix\(([^\(]*)\)$/)[1]
                        .split(',')
                        .map(Number);
                    // Apply the transform to the export map context
                    CanvasRenderingContext2D.prototype.setTransform.apply(
                        mapContext,
                        matrix
                    );
                    mapContext.drawImage(canvas, 0, 0);
                    }
                }
                );
                if (navigator.msSaveBlob) {
                // link download attribuute does not work on MS browsers
                navigator.msSaveBlob(mapCanvas.msToBlob(), 'map.png');
                } else {
                img = mapCanvas.toDataURL('image/png');
                var url = "/reportes/pdficonformidad.php";
                var tram = $('#tramites').val();
                if(tram == '20.0'){
                    url = "/reportes/pdficonformidadp.php";
                }
                //console.log(img);
                //var url = "/reportes/pdficonformidad.php?a="+a+"&fol="+f+"&img="+img;
                //window.open(url,'_new');
                var form = document.createElement("form");
                form.setAttribute("method", "post");
                //form.setAttribute("target", "_blank");
                form.setAttribute("action", url);
                form.setAttribute("target", "popupwindow");
                exportwindow = window.open("", "popupwindow", "width=800,height=600,resizable=yes");
                var params = { 'a': a, 'fol':f, 'img':img};
                for(var key in params) {
                    if(params.hasOwnProperty(key)) {
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", key);
                        hiddenField.setAttribute("value", params[key]);

                        form.appendChild(hiddenField);
                    }
                }
                document.body.appendChild(form);
                form.submit();

                }
            });
            map.renderSync();

        });

        function target_popup(form) {
            window.open('', 'formpopup', 'width=600,height=800,resizeable,scrollbars');
            form.target = 'formpopup';
        }

        $(document).on('click','#btn_guardar',function(e) {
            e.preventDefault();
            // Get form
            var form = $("#form-ventanilla")[0];
            var form2 = $("#form-ventanilla");
            // Create an FormData object 
            var data = new FormData(form);
            
            var a = $("#year").val();
            var f = $("#folio").val();
            var tram = $('#tramites').val();
            
            // Display the key/value pairs
            /*for (var pair of data.entries())
            {
            console.log(pair[0]+ ', '+ pair[1]); 
            }*/
            form2.validate({ 
                messages: {
                    year:  "* Año Requerido",
                    IdInconformidad:  "* Folio Requerido",
                    CTEL: "* Cuartel requerido",
                    MANZ: "* Manzana requerido",
                    PRED: "* Predio requerido",
                    nom_sol: "* Nombre del Solicitante requerido",
                    dom_sol: "* Domicilio del Solicitante requerido",
                    tel_sol: "* Telefono del Solicitante requerido",
                    tramites: "* Tramite requerido",
                    tramitenot: "* Notario requerido",
                    IdEpleadoBarra: "* Atendio requerido",
                    motivo: "* Motivo de la declaración requerido"
                },
                errorClass: "is-invalid",
                validClass: "is-valid",
                errorElement : 'div',
                errorLabelContainer: '.errorTxt'
            });
            if(form2.valid()){
                // disabled the submit button
            $("#btn_guardar").prop("disabled", true);
           $.ajax({
            url: "<?php echo base_url("ventanilla/guardar_declaracion"); ?>",
            enctype: 'multipart/form-data',
            type: "POST",
            data: data,//form.serialize(),
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    $("#btn_guardar").prop("disabled", false);
                },
                success: function (data) {
                        console.log("respuesta del controlador" + data);
                        if(data == 'folio_existente')
                        {
                            $("#btn_guardar").prop("disabled", false);

                            Swal.fire({
                            title: 'Folio Existente',
                            text: 'El folio ya fue registrado por alguien más, por favor generar un nuevo para realizar el registro',
                            icon: 'warning',
                            showCancelButton: true,
                            cancelButtonText: 'Cancelar',
                            confirmButtonColor: '#00b300',
                            cancelButtonColor: '#0000FF',
                            confirmButtonText: 'Generar Nuevo Folio y Enviar'
                            }).then((result) => {
                                if (result.value) {
                                    $("#btn_gen_fol").click();
                                }
                                else{
                                    $("#btn_limpiar").click();
                                }
                            })

                        }else{
                            var img = "";
                            map.once('rendercomplete', function () {
                                var mapCanvas = document.createElement('canvas');
                                var size = map.getSize();
                                mapCanvas.width = size[0];
                                mapCanvas.height = size[1];
                                var mapContext = mapCanvas.getContext('2d');
                                Array.prototype.forEach.call(
                                document.querySelectorAll('.ol-layer canvas'),
                                function (canvas) {
                                    if (canvas.width > 0) {
                                    var opacity = canvas.parentNode.style.opacity;
                                    mapContext.globalAlpha = opacity === '' ? 1 : Number(opacity);
                                    var transform = canvas.style.transform;
                                    // Get the transform parameters from the style's transform matrix
                                    var matrix = transform
                                        .match(/^matrix\(([^\(]*)\)$/)[1]
                                        .split(',')
                                        .map(Number);
                                    // Apply the transform to the export map context
                                    CanvasRenderingContext2D.prototype.setTransform.apply(
                                        mapContext,
                                        matrix
                                    );
                                    mapContext.drawImage(canvas, 0, 0);
                                    }
                                }
                                );
                                if (navigator.msSaveBlob) {
                                // link download attribuute does not work on MS browsers
                                navigator.msSaveBlob(mapCanvas.msToBlob(), 'map.png');
                                } else {
                                
                                    img = mapCanvas.toDataURL('image/png');

                                    var url = "/reportes/pdficonformidad.php";
                                   
                                    if(tram == '20.0'){
                                        url = "/reportes/pdficonformidadp.php";
                                    }

                                    //console.log(img);
                                    //var url = "/reportes/pdficonformidad.php?a="+a+"&fol="+f+"&img="+img;
                                    //window.open(url,'_new');
                                    var form = document.createElement("form");
                                    form.setAttribute("method", "post");
                                    //form.setAttribute("target", "_new");
                                    form.setAttribute("action", url);
                                    form.setAttribute("target", "popupwindow");
                                    exportwindow = window.open("", "popupwindow", "width=800,height=600,resizable=yes");
                                    var params = { 'a': a, 'fol':f, 'img':img};
                                    for(var key in params) {
                                        if(params.hasOwnProperty(key)) {
                                            var hiddenField = document.createElement("input");
                                            hiddenField.setAttribute("type", "hidden");
                                            hiddenField.setAttribute("name", key);
                                            hiddenField.setAttribute("value", params[key]);

                                            form.appendChild(hiddenField);
                                        }
                                    }
                                    document.body.appendChild(form);
                                    form.submit();

                                }
                            });
                            map.renderSync();
                            //abrir el pdf 
                            //Crear la captura del mapa
                            
                            //var url = "/reportes/pdficonformidad.php?a="+a+"&fol="+f;
                            //window.open(url,'_new');
                            //$("#btn_limpiar").click();
                            Swal.fire({
                                icon: 'info',
                                title: 'Mensaje del Sistema',
                                text: data
                                
                            }).then(function() {
                                    $("#btn_guardar").prop("disabled", false);
                                    $("#btn_limpiar").click();
                                });

                        }   //end else     
                }
            });
            }
           
        });

        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var files = [];
            for (var i = 0; i < $(this)[0].files.length; i++) {
                files.push($(this)[0].files[i].name);
            }
            $(this).next('.custom-file-label').html(files.join(', '));
        });

        function get_anio(){
            var anio = new Date().getFullYear().toString(); 
            var a = anio.substr(2,4);
            $('#year').val(a); 
            return a;
        }

        $("#chkpasardatos").on("change", function() {
            var nombre = $("#appat").val() + " " + $("#apmat").val() + " " + $("#nombres").val();
            $("#nom_sol").val(nombre);
            var calle = $("#nombrecalle").val();
            var num = $('#numeroof').text();
            var colonia = $("#nombrecolonia").val();
            var dom = calle + " " + num + " " + colonia;
            $("#dom_sol").val(dom); 

        });

              

        function get_fecha()
        {
            $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD"));  
        }
        
         function genera_folio()
         {
            $.getJSON("<?php echo base_url("ventanilla/generafolio/");?>/"+get_anio(), function(json){ 
                $.each(json.data, function(i, obj){
                    var max = parseInt(obj.maximo);
                    var max = max + 1; 
                    $('#folio').val(zeroPad(max, 5));
                });
                
                $.unblockUI();
             });
         }

       
        $('#CTEL').blur(function() {
            var ctel = $('#CTEL').val();
            cargar_manzanas(ctel);
        });

        $('#CTEL').change(function() {
            $('#MANZ').val("");
        });

        $('#MANZ').blur(function() {
            var ctel = $('#CTEL').val();
            var manz = $('#MANZ').val();
            
            cargar_predios(ctel, manz);
        });

        $('#MANZ').change(function() {
            $('#PRED').val("");
        });

        $('#PRED').blur(function() {
            var ctel = $('#RCTEL').val();
            var manz = $('#RMANZ').val();
            var pred = $('#RPRED').val();
         });

        $('#PRED').change(function() {
            $('#UNID').val("");
        });

       

         function cargar_cuarteles(){
            $('#list_cuarteles').empty();
            var url = "<?php echo base_url("padron/list_cuarteles");?>";
            //console.log(url);
            $.getJSON(url, function(json){
                    //console.log(json);
                    $.each(json.data, function(i, obj){
                            $('#list_cuarteles').append($('<option>').val(obj.cve_ctel));
                           
                    });    
            });                       
         }

         function cargar_manzanas(ctel){
            $('#list_manzanas').empty();
            var url = "<?php echo base_url("padron/list_manzanas");?>/"+ctel;
            //console.log(url);
            $.getJSON(url, function(json){
                    //console.log(json);
                    $.each(json.data, function(i, obj){
                            let manz = zeroPad(obj.NUM_MANZ, 3);
                            $('#list_manzanas').append($('<option>').val(manz));      
                    });    
            });                       
         }

         function cargar_predios(ctel, manz){
            $('#list_predios').empty();
            var url = "<?php echo base_url("padron/list_predios");?>/"+ctel+"/"+manz;
            //console.log(url);
            $.getJSON(url, function(json){
                    //console.log(json);
                    $.each(json.data, function(i, obj){
                            let pred = zeroPad(obj.Predio, 3);
                            $('#list_predios').append($('<option>').val(pred));      
                    });    
            });                       
         }

         


         function cargar_usos(cve_grupo){
            $('#usos_datalist').empty();
            $.getJSON("<?php echo base_url("padron/usos");?>/"+cve_grupo, function(json){
                    console.log(json);
                    $.each(json.results, function(i, obj){
                            $('#usos_datalist').append($('<option>').val(obj.CVE_USO.substring(1) + " - " + obj.DES_USO));
                           
                    });    
            });                       
         }
     </script>