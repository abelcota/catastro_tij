  <!-- JAVASCRIPT -->
    <script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>
    <script src="<?php echo base_url("assets/js/ol.js");?>"></script>
    <script src="<?php echo base_url("assets/js/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList");?>"></script>
    <script src="<?php echo base_url("assets/js/ol-layerswitcher.js");?>"></script>

    
    
 <!-- Required datatable js -->
 <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    

    <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
    
    <!-- context menu para openlayers -->

    <script src="<?php echo base_url("assets/js/ol-contextmenu.js");?>"></script>

    <script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script>

    <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>


    <!-- App js -->
    <script src="<?php echo base_url("assets/js/app.js");?>"></script>

    <script> 
        function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        }

        function get_anio(){
            var anio = new Date().getFullYear().toString(); 
            var a = anio.substr(2,4);
            $('#year').val(a); 
            return a;
        }

        function get_fecha()
        {
            $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD")); 
        }

        function genera_folio_cdi()
         {
            $.getJSON("<?php echo base_url("ventanilla/generafolio/");?>/"+get_anio(), function(json){ 
                $.each(json.data, function(i, obj){
                    var max = parseInt(obj.maximo);
                    var max = max + 1; 
                    $('#folio').val(zeroPad(max, 5));
                });
                
                $.unblockUI();
             });
         }

         function cargar_empleados(){
            $.getJSON("<?php echo base_url("padron/empleados/05");?>", function(json){
                    $('#atendio').empty();
                    $('#atendio').append($('<option>').text("Seleccionar Empleado").attr({'value':""}));
                    $.each(json.result, function(i, obj){
                            $('#atendio').append($('<option>').text(obj.Nombre).attr({ 'value' : obj.IdEmpleado}));
                    });
                    //$('#empleados').select2();
            });
         }

         $(document).ready(function () {
             cargar_empleados();
             get_anio();
             get_fecha();
             listar_notarios();

             $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
             $('#PRED').blur(function () { this.value = zeroPad(this.value, 3);  });

             var table = $("#tabla_cambios").DataTable({
                ajax: "<?php echo base_url("ventanilla/listadocdi")?>/"+get_anio(),
                Processing : true,
                ServerSide : true,
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                columns : [
                    {
                        data: null,
                        defaultContent: '<a class="consultar text-primary" style="cursor:pointer !important;"> <i class="dripicons-information"></i></a>'
                    },
                    { "data" : "AnioAviso" },
                    { "data" : "Folio" },
                    { "data" : "clavef" },
                    { "data" : "PropietarioAnterior" },
                    { "data" : "PropietarioActual" },     
                ],
                rowCallback: function(row, data, index) {
                    $("td:eq(1)", row).addClass("font-weight-bold"); 
                    $("td:eq(2)", row).addClass("font-weight-bold text-primary"); 
                    $("td:eq(3)", row).addClass("text-nowrap"); 
                    $("td:eq(4)", row).addClass("text-wrap"); 

                }
            });


            $('#tabla_cambios tbody').on( 'click', '.consultar', function () {
                if(table.row(this).child.isShown()){
                    var data = table.row(this).data();
                }else{
                    var data = table.row($(this).parents("tr")).data();
                }
                var a = data["AnioAviso"];
                var fol = data["Folio"];

                Swal.fire({
                title: '¿Consultar el folio: '+fol+' del año '+a+'?',
                type: 'info',
                icon: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Consultar!'
                }).then((result) => {
                    if (result.value) {
                    consulta_folio(a, fol);
                    $("#folio").val(fol);
                    $("#year").val(a);
                    $(".modal-listado").modal("hide");
                    $("#btn_mas").prop("disabled", false);
                   // $("#btn_guardar").prop("disabled", true);
                    }
                })
                
            
            });

         });

         $(document).on('click','#btn_nuevo',function(e) {
             e.preventDefault();
            //agregamos el año 
            $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD"));  
            genera_folio_cdi();
             //calculamos el nuevo folio
             $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Generando Folio Nuevo...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
            

         });

         
        $(document).on('click','#btn_buscar_prop',function(e) {
             e.preventDefault();
            var txt_prop = $('#bprop').val();
            listar_propietarios(txt_prop, 'prop_ant', 0);
            $('#bprop').val("");
        });

        $(document).on('click','#btn_buscar_prop_act',function(e) {
             e.preventDefault();
            var txt_prop = $('#bprop_act').val();
            listar_propietarios(txt_prop, 'prop_act', 0);
            $('#bprop_act').val("");
        });

        $(document).on("click", "#btn_agregar_propietario", function(e) {
           e.preventDefault();
           $('.modal-propietarios').modal("show");
           var cvecat =  $('#POBL').val()+$('#CTEL').val()+$('#MANZ').val()+$('#PRED').val()+$('#UNID').val();
           $('#prop_cvecat').val(cvecat);

           $.getJSON("<?php echo base_url("padron/genera_clave_propietario");?>", function(json){
                   console.log(json);
                   $('#prop_clave').val(json.result[0].cve_prop);
            });  
           
        });

        $('#num_notario').change(function() {
            $('#notario_nom').val($( this ).find("option:selected").text().slice(4));
        });


        function listar_notarios(){
            $.getJSON("<?php echo base_url("padron/listar_notarios/");?>", function(json){
                    $('#num_notario').empty();
                    $.each(json.data, function(i, obj){
                            
                            $('#num_notario').append($('<option>').text(obj.cve_notario + " " + obj.nombre).attr({ 'value' : obj.cve_notario}));
                    });
                    $('#num_notario').select2();
            });
        }

       function listar_propietarios(text, control, id){
            $.getJSON("<?php echo base_url("padron/propietarios/");?>/"+text+"/"+id, function(json){
                    $('#'+control).empty();
                    $.each(json.result, function(i, obj){
                            var pat = "";
                            var mat = "";
                            if(obj.APE_PAT == null)
                                pat = "";
                            else
                                pat = obj.APE_PAT;
                            
                            if(obj.APE_MAT == null)
                                mat = "";
                            else
                                mat = obj.APE_MAT;

                            $('#'+control).append($('<option>').text(pat+" "+mat+ " "+obj.NOM_PROP).attr({ 'value' : obj.CVE_PROP}));
                    });
                    $('#'+control).select2();
            });
       }
       
            
        
        
         // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
            var files = [];
            for (var i = 0; i < $(this)[0].files.length; i++) {
                files.push($(this)[0].files[i].name);
            }
            $(this).next('.custom-file-label').html(files.join(', '));
        });

         $(document).on('click', '#btn_gen_fol',  function(e){
            e.preventDefault();
            genera_folio_cdi(); 
             //calculamos el nuevo folio
             $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Generando Folio Nuevo...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
            
         });

         $(document).on('click', '#btn_validar', function () {
             //limpiamos el consultado
            if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }
            //_geojson_vectorSource.clear();
             //obtener los valores de los input de la clave
             var ctel = $('#CTEL').val();
             var manz = $('#MANZ').val();
             var pred = $('#PRED').val();

             //CARGAR CALLES Y COLONIAS DEPENDIENDO LA POBLACION
             //cargar_calles(parseInt(pobl));
             //cargar_colonias(parseInt(pobl));

             var cvecat = ctel + manz + pred;

             console.log("validando clave: " + cvecat)
             $.ajax({
                url: "<?php echo base_url("padron/validarcve");?>/"+cvecat,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Validando Clave Catastral...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                    console.warn("Validacion" , data.result);   
                    if(data.result.length > 0){
                        $("#bprop").val(data.result[0].Paterno + " " + data.result[0].Materno + " " + data.result[0].PNombre + " " + data.result[0].SNombre);
                       
                        $("#ubi_pred").val(data.result[0].Calle + " " + data.result[0].NumeroOficial + " " + data.result[0].Colonia + " " + data.result[0].Delegacion);
                        $("#num_of").val(data.result[0].NumeroOficial);
                        $("#dom_not").val(data.result[0].Calle + " " + data.result[0].NumeroOficial + " " + data.result[0].Colonia + " " + data.result[0].Delegacion);

                         //Se obtiene el array del poligono 
                         jsonobj = jQuery.parseJSON(data.result[0].jsonb_build_object);
                            //si encuentra el poligono en el vectorial
                            console.log(jsonobj);
                            if (jsonobj.features != null) {
                                //Se visualiza el poligono en el mapa
                                visualizar_capa(jsonobj);                                
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Vectorial no encontrado',
                                    text: 'La clave catastral no se encontró en el vectorial!'
                                    
                                })
                            }

                    }  
                    },
                   complete : function (){
                        $.unblockUI();
                    }
               });
         });

         $(document).on('click', '#btn_listado',  function(e){
            e.preventDefault();
            $(".modal-listado").modal("show"); 

         });

         $(document).on('click', '#btn_guardar', function (e) {
            e.preventDefault();
            console.log($("#prop_act").val());
            if($("#prop_act").val() != null){
                var propact = $("#prop_act").val().trim();
                $("#prop_act").val(propact);
            }
            else{
                var propact = $("#bprop_act").val().trim();
                $("#prop_act").val(propact);
            }
            
            
            var form = $("#form-cdi");
            var form2 = $("#form-cdi")[0];
            var data = new FormData(form2);
            
            form.validate({ 
                messages: {
                    year:  "* Año Requerido",
                    folio:  "* Folio Requerido",
                    POBL: "* Poblacion requerido",
                    CTEL: "* Cuartel requerido",
                    MANZ: "* Manzana requerido",
                    PRED: "* Predio requerido",
                    UNID: "* Unidad requerido",
                    solicitante: "* Solicitante requerido",
                    prop_ant: "* Propietario Anterior requerido",
                    prop_act: "* Propietario Actual requerido",
                    ubi_pred: "* Ubicacion del predio requerido",
                    num_of: "* Numero oficial requerido",
                    telefono: "* Telefono requerido",
                    dom_not: "* Domicilio de notificación requerido",
                    tipo_mov: "* Tipo de movimiento requerido",
                    observaciones: "* Observaciones requeridas",
                    atendio: "* Ususario que atiende el cambio de inscripcion requerido"
                },
                errorClass: "is-invalid",
                validClass: "is-valid",
                errorElement : 'div',
                errorLabelContainer: '.errorTxt'
            });
            if(form.valid()){
                console.log("formulario valido");
                console.log(form.serialize());
                $("#btn_guardar").prop("disabled", true);
                $.ajax({
                    url: "<?php echo base_url("ventanilla/guardar_cdi"); ?>",
                    type: "POST",
                    data: data,
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,   // tell jQuery not to set contentType
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert("Status: " + textStatus);
                            alert("Error: " + errorThrown);
                            $("#btn_guardar").prop("disabled", false);
                        },
                        success: function (data) {
                            console.log("respuesta del controlador" + data);
                            if(data == 'folio_existente')
                            {
                                $("#btn_guardar").prop("disabled", false);
                                Swal.fire({
                                title: 'Folio Existente',
                                text: 'El folio ya fue registrado por alguien más, por favor generar un nuevo para realizar el registro',
                                icon: 'warning',
                                showCancelButton: true,
                                cancelButtonText: 'Cancelar',
                                confirmButtonColor: '#00b300',
                                cancelButtonColor: '#0000FF',
                                confirmButtonText: 'Generar Nuevo Folio y Enviar'
                                }).then((result) => {
                                    if (result.value) {
                                        $("#btn_gen_fol").click();
                                    }
                                    else{
                                        $("#btn_limpiar").click();
                                        
                                    }
                                })
                            }
                            else
                            {   
                            if(data == 'ok'){
                                //mandamos mensaje de 
                                Swal.fire({
                                    icon: 'info',
                                    title: 'Mensaje del Sistema',
                                    text: 'Cambio de Inscripción Guardado correctamente'
                                    
                                })

                                //imprimimos
                                var a = $("#year").val();
                                var f = $("#folio").val();
                                var url = "/reportes/pdfavisocdi.php?a="+a+"&fol="+f;
                                
                                window.open(url,'popup', "width=800,height=600,resizable=yes");
                                $("#btn_limpiar").click();
                                $("#btn_guardar").prop("disabled", false);
                            }
                            else{
                                //mandamos mensaje de 
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'No se pudo guardar el cambio de inscripcion'
                                    
                                })
                                
                                $("#btn_guardar").prop("disabled", false);
                            } 
                        }                              
                        }
                    });
            }
            
        });
         
        function consulta_folio(a, folio)
        {
            if (folio == ""){
                    Swal.fire({
                        icon: 'error',
                        text: 'Ingrese un folio válido para continuar',
                        })
                 }
                else{
                    
                $.ajax({
                url: "<?php echo base_url("ventanilla/consultafoliocdi/");?>/"+a+"/"+folio+"",
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Consultando Folio...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                       console.log(data.data[0]);
                       var data = data.data;
                       if(data.length > 0){
                            //CARGAR CALLES Y COLONIAS DEPENDIENDO LA POBLACION
                            //cargar_calles(parseInt(data[0].Poblacion));
                            //cargar_colonias(parseInt(data[0].Poblacion));
                            //agregar los valores a los controles
                                $("#POBL").val(data[0].Clave.substr(0, 3));
                                $("#CTEL").val(data[0].Clave.substr(3, 3));
                                $("#MANZ").val(data[0].Clave.substr(6, 3));
                                $("#PRED").val(data[0].Clave.substr(9, 3));
                                $("#UNID").val(data[0].Clave.substr(12, 3));
                                //fecha de captura
                                var fecha = data[0].FechaCaptura.substring(0, 10);
                                $("#dtfecha").val(fecha);
                                //Calle y colonia
                                $("#solicitante").val(data[0].NombreSolicitante);
                                $("#prop_ant").val(data[0].PropietarioAnterior);
                                $("#prop_act").val(data[0].PropietarioActual);
                                $("#ubi_pred").val(data[0].DomicilioUbicacion); 
                                $("#num_of").val(data[0].NumeroOficial); 
                                $("#telefono").val(data[0].TelefonoSolicitante); 
                                $("#dom_not").val(data[0].DomicilioNotificacion); 
                                $("#concepto").val(data[0].Concepto); 
                                $("#observaciones").val(data[0].Observaciones); 
                                $("#atendio").val(data[0].IdEmpleadoBarra); 
                                $("#noticia").val(data[0].Noticia); 
                                $("#control").val(data[0].Control); 
                                $("#notario").val(data[0].TramiteNotario); 
                                $("#titulo").val(data[0].TituloTraslativo); 
                                $("#cancelacioncdi").text(data[0].Motivo); 
                                
                                $('#btn_validar').click();


                       }
                       else {
                           Swal.fire({
                               icon: 'error',
                               title: 'Folio no encontrado',
                               text: 'El folio no se encuentra registrado!'
                               
                           })
                       }
                       
                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });  
            }
        }

        $(document).on('click','#btn_imprimir',function(e) {
            //imprimimos
            var a = $("#year").val();
            var f = $("#folio").val();
            var url = "/reportes/pdfavisocdi.php?a="+a+"&fol="+f;
            window.open(url,'popup', "width=800,height=600,resizable=yes");
        });
		
		$(document).on("click", "#btn_guardar_propietario", function(e) {
            e.preventDefault();
            var form =  $("#form_propietarios"); 
            if(form.valid()){
                console.log(form.serialize());
                $.ajax({
                url: "<?php echo base_url("captura/registrar_propietario_cdi"); ?>",
                type: "POST",
                data: form.serialize(),
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                    },
                    success: function (data) {
                        console.log(data);
                        if(data == 'ok'){
                            //reset el form
                            document.getElementById('form_propietarios').reset();
                            //actualizar tabla y cerrar modal
                            
                            //$('#tblpropietarios').DataTable().ajax.reload(null, false);
                            $(".modal-propietarios").modal("hide");
                           
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Propietario Registrado Correctamente',
                                icon:"success",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                        else{
                            Swal.fire({
                                //title:"Good job!",
                                text: 'Ocurrio un error al registrar el Propietario',
                                icon:"error",
                                showCancelButton:!0,
                                confirmButtonColor:"#3b5de7",
                                cancelButtonColor:"#f46a6a"
                            })
                        }
                    
                    }
                }); 
            }
        });    
        

        $(document).on('click','#btn_limpiar',function(e) {
            if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }
            $("#form-cdi")[0].reset();
            $(".form-control").removeClass("is-valid");
            $(".form-control").removeClass("is-invalid");
           
            get_anio();
            $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD"));  
            $('.errorTxt').empty();
             //aparecer el boton de actualizar
            //$("#btn_actualizar").hide();
            $("#btn_guardar").show();
        });
         

    </script>