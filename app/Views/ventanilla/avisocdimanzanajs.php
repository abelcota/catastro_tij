  <!-- JAVASCRIPT -->
  <script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>
    <script src="<?php echo base_url("assets/js/ol.js");?>"></script>
    <script src="<?php echo base_url("assets/js/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList");?>"></script>
    <script src="<?php echo base_url("assets/js/ol-layerswitcher.js");?>"></script>

    
    
 <!-- Required datatable js -->
 <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
  <!-- Buttons examples -->
  <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
    <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
    

    <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


    
    <!-- context menu para openlayers -->

    <script src="<?php echo base_url("assets/js/ol-contextmenu.js");?>"></script>

    <script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script>




    <!-- App js -->
    <script src="<?php echo base_url("assets/js/app.js");?>"></script>

    <script> 
        function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        }

        function get_anio(){
            var anio = new Date().getFullYear().toString(); 
            var a = anio.substr(2,4);
            $('#year').val(a); 
            return a;
        }

        function get_fecha()
        {
            $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD")); 
        }

        $(document).ready(function () {
             cargar_empleados();
             get_anio();
             get_fecha();

             $('#POBL').blur(function () {  this.value = zeroPad(this.value, 3); });
             $('#CTEL').blur(function () { this.value = zeroPad(this.value, 3);  });
             $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
             $('#PRED1').blur(function () { this.value = zeroPad(this.value, 3);  });
             $('#PRED2').blur(function () { this.value = zeroPad(this.value, 3);  });

             var table = $("#tabla_cambios").DataTable({
                ajax: "<?php echo base_url("ventanilla/listadocdimanzana")?>/"+get_anio(),
                Processing : true,
                ServerSide : true,
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                columns : [
                    {
                        data: null,
                        defaultContent: '<a class="consultar text-primary" style="cursor:pointer !important;"> <i class="dripicons-information"></i></a>'
                    },
                    { "data" : "AnioAviso" },
                    { "data" : "Folio" },
                    { "data" : "clavef" },
                    { "data" : "PredioInicio" },
                    { "data" : "PredioFin" },
                    { "data" : "PropietarioAnterior" },
                    { "data" : "PropietarioActual" },     
                ],
                rowCallback: function(row, data, index) {
                    $("td:eq(1)", row).addClass("font-weight-bold"); 
                    $("td:eq(2)", row).addClass("font-weight-bold text-primary"); 
                    $("td:eq(3)", row).addClass("text-nowrap"); 
                    $("td:eq(4)", row).addClass("text-wrap"); 

                }
            });


        });

        function cargar_empleados(){
            $.getJSON("<?php echo base_url("padron/empleados/05");?>", function(json){
                    $('#atendio').empty();
                    $('#atendio').append($('<option>').text("Seleccionar Empleado").attr({'value':""}));
                    $.each(json.result, function(i, obj){
                            $('#atendio').append($('<option>').text(obj.Nombre).attr({ 'value' : obj.IdEmpleado}));
                    });
                    //$('#empleados').select2();
            });
         }

         $(document).on('click', '#btn_listado',  function(e){
            e.preventDefault();
            $(".modal-listado").modal("show"); 

         });

         $(document).on('click','#btn_nuevo',function(e) {
             e.preventDefault();
            //agregamos el año 
            let today = new Date().toISOString().substr(0, 10);
             $('#dtfecha').val(today);  
             genera_folio_cdi_manzana();
             //calculamos el nuevo folio
             $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Generando Folio Nuevo...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
            

         });

         function genera_folio_cdi_manzana(){
            $.getJSON("<?php echo base_url("ventanilla/generafoliocdimanz");?>/"+get_anio(), function(json){ 
                $.each(json.data, function(i, obj){
                    var max = parseInt(obj.maximo);
                    var max = max + 1;
                    var f = zeroPad(max, 5);
                    $('#folio').val(f);
                });
                
                $.unblockUI();
             });
         }

         $(document).on('click', '#btn_validar', function (e) {
            e.preventDefault();
             //limpiamos el consultado
             if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }
             //obtener los valores de los input de la clave
             var pobl = $('#POBL').val();
             var ctel = $('#CTEL').val();
             var manz = $('#MANZ').val();
            //construir la clave catastral
             var cvecat = pobl + ctel + manz;
             console.log("validando clave: " + cvecat)
             $.ajax({
                url: "<?php echo base_url("padron/validarcvecdimanz");?>/"+cvecat,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Validando Clave Catastral...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                    success: function (data) {
                    
                    if(data.result.length > 0){
                        console.log(data.result);
                        $("#NPoblacion").val(data.result[0].nompobl);
                         //Se obtiene el array del poligono 
                         jsonobj = jQuery.parseJSON(data.result[0].fc);
                            //si encuentra el poligono en el vectorial
                            console.log(jsonobj);
                            if (jsonobj.features != null) {
                                //Se visualiza el poligono en el mapa
                                visualizar_capa(jsonobj);                                
                            }
                            else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Vectorial no encontrado',
                                    text: 'La clave catastral no se encontró en el vectorial!'
                                    
                                })
                            }
                        }
                        else{
                            Swal.fire({
                               icon: 'error',
                               title: 'Clave no encontrada',
                               text: 'La clave catastral no fue encontrada!'
                               
                           })
                        }

                    },
                    complete : function (){
                        $.unblockUI();
                    }
               });
         });

         $(document).on('click', '#btn_emision', function (e) {
            e.preventDefault();
            var form = $("#form-cdi");
            var form2 = $("#form-cdi")[0];
            var data = new FormData(form2);

            form.validate();
            if(form.valid()){
                console.log("formulario valido");
                console.log(form.serialize());
                $("#btn_emision").prop("disabled", true);
                $.ajax({
                    url: "<?php echo base_url("ventanilla/guardar_cdi_manz"); ?>",
                    type: "POST",
                    data: data,
                    processData: false,  // tell jQuery not to process the data
                    contentType: false,   // tell jQuery not to set contentType
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert("Status: " + textStatus);
                            alert("Error: " + errorThrown);
                            $("#btn_emision").prop("disabled", false);
                        },
                        success: function (data) {
                            console.log("respuesta del controlador" + data);
                            if(data == 'ok'){
                                //mandamos mensaje de 
                                Swal.fire({
                                    icon: 'info',
                                    title: 'Mensaje del Sistema',
                                    text: 'Cambio de Inscripción Guardado correctamente'
                                    
                                })
                                //imprimimos
                                var a = $("#year").val();
                                var f = $("#folio").val();
                                var url = "/reportes/pdfcdimanzana.php?a="+a+"&fol="+f;
                                window.open(url,'_new');
                                $("#btn_limpiar").click();
                                $("#btn_emision").prop("disabled", false);
                            }
                            else{
                                //mandamos mensaje de 
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Mensaje del Sistema',
                                    text: 'No se pudo guardar el cambio de inscripcion'
                                    
                                })
                                
                                $("#btn_emision").prop("disabled", false);
                            }                               
                        }
                    });
            }
            
        });

        $(document).on('click','#btn_limpiar',function(e) {
            if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }
            $("#form-cdi")[0].reset();
           
            get_anio();
            $('#dtfecha').val(new Date().toISOString().substr(0, 10));  
             //aparecer el boton de actualizar
            //$("#btn_actualizar").hide();
            $("#btn_guardar").show();
        });

         
         

    </script> 