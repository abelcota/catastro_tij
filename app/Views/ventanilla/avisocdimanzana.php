<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                    
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">CAMBIOS AL PADRÓN POR MANZANA</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">VENTANILLA</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="btn-toolbar p-3" role="toolbar">
                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_nuevo"><i class="fa fa-plus"></i> Nuevo</button>
                                            <button type="button" id="btn_listado" class="btn btn-primary waves-light waves-effect"><i class="fa fa-list"></i> Ver Listado</button>
                                            <button type="button" class="btn btn-secondary waves-light waves-effect" id="btn_limpiar"><i class="fa fa-ban"></i> Limpiar Pantalla</button>                                        </div>

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button disabled type="button" class="btn btn-primary waves-light waves-effect dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                Más <i class="mdi mdi-dots-vertical ml-2"></i>
                                            </button>
                                            <div class="dropdown-menu" style="">
                                                <a class="dropdown-item" href="#"><i class="fa fa-edit"></i> Editar</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Borrar Registro</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-print"></i> Imprimir</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end toolbar -->
                          <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                     <h4 class="card-title">Aviso C.D.I Por Manzana</h4>
                                     <p class="card-title-desc"></p>
                                     <!-- start form -->
                                     <form class="needs-validation" novalidate id="form-cdi">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip01">Año:</label>
                                                    <input type="text" class="form-control" placeholder="Año" name="year" id="year">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Folio:</label>
                                                    <input type="text" class="form-control" placeholder="Folio" name="folio" id="folio">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral:</label>
                                                    
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" placeholder="POBL" id="POBL" name="POBL" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="CTEL" id="CTEL" name="CTEL" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="MANZ" id="MANZ" name="MANZ" required maxlength="3" minlength="3">
                                                                <button class="btn btn-primary" type="button" style="margin-left:5px;" id="btn_validar">Validar</button>
                                                            </div>   
                                                   
                                                </div>
                                            </div>
                                             <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Fecha:</label>
                                                    <input type="date" class="form-control" name="dtfecha" id="dtfecha">
                                                 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip01">Del Predio:</label>
                                                    <input type="text" class="form-control" placeholder="" id="PRED1" name="PRED1">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip01">al Predio:</label>
                                                    <input type="text" class="form-control" placeholder="" id="PRED2" name="PRED2">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip01">Población:</label>
                                                    <input type="text" class="form-control" id="NPoblacion" name="NPoblacion" readonly>
                                                </div>
                                            </div>
                                            
                                           
                                        </div>
                                       
                                          <div class="row">
                                            <div class="col-md-6 border-right">
                                                <h4 class="card-title">RECEPCION EN VENTANILLA</h4>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip03">Propietario Anterior:</label>
                                                    <input type="text" class="form-control" id="propant" name="propant">
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip03">Propietario Actual:</label>
                                                    <input type="text" class="form-control" id="propact" name="propact">
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip03">Ubicación del Predio:</label>
                                                    <input type="text" class="form-control" id="ubipred" name="ubipred">
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip03">Domicilio de Notificacion:</label>
                                                    <input type="text" class="form-control" id="domnot" name="domnot">
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip03">Concepto:</label>
                                                    <input type="text" class="form-control" id="concepto" name="concepto">
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip03">Tipo de Movimiento:</label>
                                                    <select class="form-control" id="tipomov" name="tipomov">
                                                        <option selected value="Baja">Baja</option>
                                                        <option value="Cambio">Cambio</option>
                                                    </select>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip03">Observaciones:</label>
                                                    <input type="text" class="form-control" id="observaciones" name="observaciones">
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip03">Atendió:</label>
                                                    <select class="form-control select2" id="atendio" name="atendio" required>
                                                      <option>Seleccionar Empleado</option>
                                                      </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <h4 class="card-title">C.D.I.</h4>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip04">Noticia:</label>
                                                    <input type="number" class="form-control" id="noticia" name="noticia">
                                                </div>
                                               
                                              
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip04">Titulo Traslativo:</label>
                                                    <textarea rows="5" class="form-control" id="titulo" name="titulo">
                                                    </textarea>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip04">Cancelacion o Cambio de Inscripcion:</label>
                                                    <textarea rows="5" class="form-control" id="cancocdi" name="cancocdi">
                                                    </textarea>
                                                </div>

                                                <div class="form-group position-relative">
                                                    <button type="button" class="btn btn-success btn-block" id="btn_emision">Emision C.D.I.</button>
                                                </div>
                                            </div>
                                           
                                        </div>
                                         

                                       
                                    </form>
                                    <!-- end form -->
                                    <div class="col-md-12">
                                                <div class="form-group position-relative">    
                                                   <div id="map" class="map" style="height: 400px; max-width: 900px;"></div>
                                                   
                                        </div>
                                </div>
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            <!--modal para ver el listado de solicitudes -->
            <div class="modal fade modal-listado" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                <div class="modal-dialog modal-xl modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Buscar todos los Cambios</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                               <table class="table table-hover no-footer" id="tabla_cambios" width="100%">
                                                                <thead class="text-white" style="background-color: #480912;">
                                                                <tr>
                                                                    <th></th>
                                                                    <th>Año Aviso</th>
                                                                    <th>Folio</th>
                                                                    <th>Clave</th>
                                                                    <th>Predio Inicio</th>
                                                                    <th>Predio Fin</th>
                                                                    <th>Propietario Anterior</th>
                                                                    <th>Propietario Actual</th>
                                                                    
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                               </table>      
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar">Cerrar</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
            <!--en modal -->