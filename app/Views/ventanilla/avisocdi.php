<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">CAMBIOS AL PADRÓN</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">VENTANILLA</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->
                     <div class="row">
                     <div class="btn-toolbar p-3" role="toolbar">
                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_nuevo"><i class="fa fa-plus"></i> Nuevo</button>
                                            <button type="button" id="btn_listado" class="btn btn-primary waves-light waves-effect"><i class="fa fa-list"></i> Ver Listado</button>
                                            <button type="button" class="btn btn-secondary waves-light waves-effect" id="btn_limpiar"><i class="fa fa-ban"></i> Limpiar Pantalla</button>                                        </div>

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button id="btn_mas" disabled type="button" class="btn btn-primary waves-light waves-effect dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                Más <i class="mdi mdi-dots-vertical ml-2"></i>
                                            </button>
                                            <div class="dropdown-menu" style="">
                                                <a class="dropdown-item" href="#"><i class="fa fa-edit"></i> Editar</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Borrar Registro</a>
                                                <a id="btn_imprimir" class="dropdown-item" href="#"><i class="fa fa-print"></i> Imprimir</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end toolbar -->
                          <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                     <h4 class="card-title">Aviso C.D.I</h4>
                                     <p class="card-title-desc"></p>
                                     <!-- start form -->
                                     <form class="needs-validation" novalidate id="form-cdi">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="year">Año:</label>
                                                    <input type="text" class="form-control" placeholder="Año" id="year" name="year" required>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="Folio">Folio:</label>
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                    <input type="text" class="form-control" placeholder="Folio" id="folio" name="folio" required><span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_gen_fol">+</button></span></div>
                                                    <input type="hidden" class="form-control" placeholder="Folio" id="NPoblacion" name="NPoblacion" >
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral:</label>
                                                    
                                                            <div class="input-group" >
                                                                <input type="text" class="form-control" placeholder="CTEL" id="CTEL" name="CTEL" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="MANZ" id="MANZ" name="MANZ" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="PRED" id="PRED" name="PRED" required maxlength="3" minlength="3">
                                                                <button class="btn btn-primary" type="button" style="margin-left:5px;" id="btn_validar">Validar</button>
                                                                <input type="hidden" class="form-control" placeholder="UNID" id="clavecat" name="clavecat" required maxlength="3" minlength="3">
                                                            </div>
                                                   
                                                </div>
                                            </div>
                                             <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="dtfecha">Fecha:</label>
                                                    <input type="date" class="form-control" id="dtfecha"  name="dtfecha" required style="font-size:11px;">
                                                 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group position-relative">
                                                    <label for="solicitante">Solicitante:</label>
                                                    <input type="text" class="form-control" id="solicitante" name="solicitante" required>
                                                </div>
                                            </div>
                                        </div>
                                          <div class="row">
                                            <div class="col-md-6 border-right">
                                                <h4 class="card-title">RECEPCION EN VENTANILLA</h4>
                                                <div class="form-group position-relative">
                                                    <label for="prop_ant">Propietario Anterior:</label>
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected mb-2">
                                                    <input type="text" class="form-control" placeholder="Buscar Propietario Anterior por Nombre" id="bprop" name="bprop"><span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_buscar_prop">Buscar</button></span></div>
                                                    <select class="form-control select2" id="prop_ant" name="prop_ant"></select>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="prop_act">Propietario Actual:</label>
                                                    <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected mb-2">
                                                    <input type="text" class="form-control" placeholder="Buscar Propietario Actual por Nombre" id="bprop_act" name="bprop_act"><span class="input-group-btn input-group-append"><button class="btn btn-primary bootstrap-touchspin-up" type="button" id="btn_buscar_prop_act">Buscar</button></span><span class="input-group-btn input-group-append"><!--<button class="btn btn-success bootstrap-touchspin-up" type="button" id="btn_agregar_propietario"><i class="mdi mdi-account-multiple-plus-outline
"></i> registrar</button>--></span></div>
                                                    <select class="form-control select2" id="prop_act" name="prop_act"></select>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="ubi_pred">Ubicación del Predio:</label>
                                                    <input type="text" class="form-control" id="ubi_pred"  name="ubi_pred" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="num_of">Numero Oficial:</label>
                                                    <input type="text" class="form-control" id="num_of" name="num_of" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="telefono">Telefono:</label>
                                                    <input type="text" class="form-control" id="telefono"  name="telefono" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="dom_not">Domicilio de Notificacion:</label>
                                                    <input type="text" class="form-control" id="dom_not" name="dom_not" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="concepto">Concepto:</label>
                                                    <input type="text" class="form-control" id="concepto" name="concepto" required>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="tipo_mov">Tipo de Movimiento:</label>
                                                    <select class="form-control" id="tipo_mov" name="tipo_mov" required>
                                                        <option selected>Baja</option>
                                                        <option>Cambio</option>
                                                    </select>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="observaciones">Observaciones:</label>
                                                    <textarea rows="4" class="form-control" id="observaciones" name="observaciones" required></textarea>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip03">Atendió:</label>
                                                    <select class="form-control select2" id="atendio" name="atendio" required>
                                                      <option>Seleccionar Empleado</option>
                                                      </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <h4 class="card-title">C.D.I.</h4>
                                                <div class="form-group position-relative">
                                                    <label for="notario_num">Número de Notario:</label>
                                                    <select class="form-control select2" id="num_notario" name="num_notario"></select>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="notario_nom">Nombre de Notario:</label>
                                                    <input type="text" class="form-control" id="notario_nom" name="notario_nom">
                                                </div>
                                                
                                                <div class="form-group position-relative">
                                                    <label for="titulo">Titulo Traslativo:</label>
                                                    <textarea rows="5" class="form-control" id="titulo" name="titulo"></textarea>
                                                </div>
                                                <div class="form-group position-relative">
                                                    <label for="cancelacioncdi">Cancelacion o Cambio de Inscripcion:</label>
                                                    <textarea rows="5" class="form-control" id="cancelacioncdi" name="cancelacioncdi"></textarea>
                                                </div>

                                                <div class="form-group position-relative">
                                                    <button class="btn btn-success btn-block" id="btn_guardar">Guardar Emision C.D.I.</button>
                                                </div>

                                                <div class="errorTxt invalid-feedback" style="margin-bottom: 15px;"></div>

                                            </div>
                                           
                                        </div>
                                         
                                        <label for="validationTooltip02">Adjuntar Documentos:</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="archivos[]" name="archivos[]" multiple accept="pdf">
                                                <label class="custom-file-label text-truncate" for="customFile" data-browse="Buscar...">Seleccionar Archivos</label>
                                            </div>
<br><br>
<div class="form-group position-relative">    
                                                   <div id="map" class="map" style="height: 400px; max-width: 100%;">
                                                </div>   
                                       
                                    </form>
                                    <!-- end form -->
                                   
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            <!--modal para ver el listado de solicitudes -->
            <div class="modal fade modal-listado" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                <div class="modal-dialog modal-xl modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Buscar todos los Cambios</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                               <table class="table table-hover no-footer" id="tabla_cambios" width="100%">
                                                                <thead class="text-white" style="background-color: #480912;">
                                                                <tr>
                                                                    <th></th>
                                                                    <th>Año Aviso</th>
                                                                    <th>Folio</th>
                                                                    <th>Clave</th>
                                                                    <th>Propietario Anterior</th>
                                                                    <th>Propietario Actual</th>
                                                                    
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                               </table>      
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar">Cerrar</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
            <!--en modal -->

          <div class="modal fade modal-propietarios" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Agregar Nuevo Propietario</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                        </div>
                        <div class="modal-body">
                        <form action="" name="form_propietarios" id="form_propietarios" method="post" accept-charset="utf-8">
                            <div class="row">
                           
                            
                                <div class="col-lg-12">
                                    
                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Clave de Propietario:</label>                                  
                                        <input type="number" name="prop_clave" id="prop_clave" class="form-control " placeholder="0" required readonly>
                                        <input type="hidden" name="prop_cvecat" id="prop_cvecat" class="form-control">
                                                                               
                                    </div>

                                    <div class="form-group position-relative">
                                                            
                                        <label class="form-label">Tipo de Persona: </label>
                                                           
                                        <input type="text" class="form-control" name="prop_tip_persona" id="prop_tip_persona"  placeholder="(F / M)" required list="list_tip_per">
                                        <datalist id="list_tip_per">
                                            <option value="F - FISICA">
                                             <option value="M - MORAL">                            
                                        </datalist>
                                                              
                                     </div>
                                    

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Nombre (Razon Social):</label>
                                        
                                        <input type="text" name="prop_nombre" id="prop_nombre" class="form-control " required placeholder="NOMBRE(S)">
                                    
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Apellido Paterno:</label>
                                        
                                        <input type="text" name="prop_apellido_paterno" id="prop_apellido_paterno" class="form-control " required placeholder="APELLIDO PATERNO">
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04">Apellido Materno:</label>
                                        
                                        <input type="text" name="prop_apellido_materno" id="prop_apellido_materno" class="form-control " required placeholder="APELLIDO MATERNO">
                                                                            
                                    </div>

                                    <div class="form-group position-relative">
                                        <label for="validationTooltip04"> Reg.Fed.Cont.:</label>
                                        
                                        <input type="text" name="prop_rfc" id="prop_rfc" class="form-control " required placeholder="RFC">
                                                                            
                                    </div>

                                   

                                </div>

                               
                            </div>
                            

                        </div>
                        <div class="modal-footer">
                                                        
                                                        <button type="button" class="btn btn-success" id="btn_guardar_propietario">Registrar</button>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar_propietario">Cancelar</button>
                        </div>
                    </div>
                    </form>
                    
                </div>
            </div>
            <!-- end modal -->

            