<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">OFICIOS DE BONIFICACIÓN</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MÓDULO</a></li>
                                        <li class="breadcrumb-item active">VENTANILLA</li>
                                    </ol>
                                </div>



                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                     <!-- start row -->

                     <div class="row">
                     <div class="btn-toolbar p-3" role="toolbar">
                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button type="button" class="btn btn-success waves-light waves-effect" id="btn_nuevo"><i class="fa fa-plus"></i> Nuevo</button>
                                            <button type="button" id="btn_listado" class="btn btn-primary waves-light waves-effect"><i class="fa fa-list"></i> Ver Listado</button>
                                            <button type="button" class="btn btn-secondary waves-light waves-effect" id="btn_limpiar"><i class="fa fa-ban"></i> Limpiar Pantalla</button>                                        </div>

                                        <div class="btn-group mr-2 mb-2 mb-sm-0">
                                            <button disabled type="button" class="btn btn-primary waves-light waves-effect dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                Más <i class="mdi mdi-dots-vertical ml-2"></i>
                                            </button>
                                            <div class="dropdown-menu" style="">
                                                <a class="dropdown-item" href="#"><i class="fa fa-edit"></i> Editar</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-trash"></i> Borrar Registro</a>
                                                <a class="dropdown-item" href="#"><i class="fa fa-print"></i> Imprimir</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end toolbar -->
                          <!-- start col 6 -->
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                     <h4 class="card-title">Aetnción a Oficios de Bonificacion</h4>
                                     <p class="card-title-desc"></p>
                                     <!-- start form -->
                                     <form class="needs-validation" novalidate id="form-incorrecto">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="year">Año:</label>
                                                    <input type="text" class="form-control" placeholder="Año" id="year" name="year" required>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="Folio">Folio:</label>
                                                    <input type="text" class="form-control" placeholder="Folio" id="folio" name="folio" required>
                                                    <input type="hidden" class="form-control" placeholder="Folio" id="NPoblacion" name="NPoblacion" >
                                                    
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral:</label>
                                                    
                                                            <div class="input-group" >
                                                                <input type="text" class="form-control" placeholder="POBL" id="POBL" name="POBL" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="CTEL" id="CTEL" name="CTEL" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="MANZ" id="MANZ" name="MANZ" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="PRED" id="PRED" name="PRED" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="UNID" id="UNID" name="UNID" required maxlength="3" minlength="3">
                                                                <button class="btn btn-primary" type="button" style="margin-left:5px;" id="btn_validar">Validar</button>
                                                            </div>
                                                   
                                                </div>
                                            </div>
                                             <div class="col-md-2">
                                                <div class="form-group position-relative">
                                                    <label for="dtfecha">Fecha:</label>
                                                    <input type="date" class="form-control" id="dtfecha"  name="dtfecha" required style="font-size:11px;">
                                                 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="solicitante">Nombre:</label>
                                                    <input type="text" class="form-control" id="nombre" name="nombre" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="solicitante">Domicilio:</label>
                                                    <input type="text" class="form-control" id="domicilio" name="domicilio" required>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Superficie Terreno:</label>
                                                    <input type="text" class="form-control" id="superficieterr" name="superficieterr" placeholder="Superficie" readonly value="0">
                                                </div>
                                            </div>
                                              <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                   <label for="validationTooltip02">Superficie Construcción:</label>
                                                    <input type="text" class="form-control"  id="superficiecons" name="superficiecons" placeholder="Construccion" readonly value="0">
                                                </div>
                                            </div>
                                              <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                   <label for="validationTooltip02">Valor Catastral:</label>
                                                    <input type="text" class="form-control" placeholder="Valor" id="valorcat" name="valorcat" readonly value="0">
                                                </div>
                                            </div>
                                        </div>
                                           
                                    </form>
                                    <!-- end form -->
                                    <h5 class="card-title">Le Corresponde a:</h5>
                                    <!-- le corresponde a -->
                                    <!-- start form -->
                                    <form class="needs-validation border" style="padding:15px;" novalidate id="form-correcto">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltipUsername">Clave Catastral:</label>
                                                    
                                                            <div class="input-group" >
                                                                <input type="text" class="form-control" placeholder="POBL" id="POBL2" name="POBL2" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="CTEL" id="CTEL2" name="CTEL2" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="MANZ" id="MANZ2" name="MANZ2" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="PRED" id="PRED2" name="PRED2" required maxlength="3" minlength="3">
                                                                <input type="text" class="form-control" placeholder="UNID" id="UNID2" name="UNID2" required maxlength="3" minlength="3">
                                                                <button class="btn btn-primary" type="button" style="margin-left:5px;" id="btn_validar2">Validar</button>
                                                            </div>
                                                   
                                                </div>
                                            </div>
                                                                                        
                                            <div class="col-md-6">
                                                
                                            </div>

                                            
                                        </div>
                                        <div class="row">
                                            
                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="solicitante">Nombre:</label>
                                                    <input type="text" class="form-control" id="nombre2" name="nombre2" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                    <label for="solicitante">Domicilio:</label>
                                                    <input type="text" class="form-control" id="domicilio2" name="domicilio2" required>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                    <label for="validationTooltip02">Superficie Terreno:</label>
                                                    <input type="text" class="form-control" id="superficieterr2" name="superficieterr2" placeholder="Superficie" readonly value="0">
                                                </div>
                                            </div>
                                              <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                   <label for="validationTooltip02">Superficie Construcción:</label>
                                                    <input type="text" class="form-control"  id="superficiecons2" name="superficiecons2" placeholder="Construccion" readonly value="0">
                                                </div>
                                            </div>
                                              <div class="col-md-4">
                                                <div class="form-group position-relative">
                                                   <label for="validationTooltip02">Valor Catastral:</label>
                                                    <input type="text" class="form-control" placeholder="Valor" id="valorcat2" name="valorcat2" readonly value="0">
                                                </div>
                                            </div>
                                        </div>
                                           
                                    </form>
                                    <!-- end form -->
                                    <!-- end le corresponde a -->
                                    <div class="row" style="margin-top:15px;">
                                    <div class="col-md-6">
                                                <div class="form-group position-relative">
                                                   <label for="motivo">Observaciones:</label>
                                                     <textarea class="form-control" rows="4" id="observaciones" name="observaciones" required></textarea>
                                                </div>
                                            </div>

                                        <div class="col-md-6">
                                                <div class="form-group">
                                                   <label for="IdEpleadoBarra">Atendió:</label>
                                                   
                                                   <select class="form-control select2" id="empleados" name="IdEpleadoBarra" required>
                                                      <option>Seleccionar Empleado</option>
                                                      </select>
                                                </div>
                                                <div class="errorTxt invalid-feedback" style="margin-bottom: 15px;"></div>

                                                <button class="btn btn-success float-right" type="button" id="btn_guardar">Guardar</button>
                                               
                                            </div>
                                            
                                     </div>         
                                    <hr>
                                    <div class="col-md-12">
                                                <div class="form-group position-relative">    
                                                   <div id="map" class="map" style="height: 400px; max-width: 900px;"></div>
                                                   
                                        </div>
                                </div>
                             </div>
                         </div>
                     </div>
                     <!-- end row -->
                </div>
                <!-- End Page-content -->
            </div>
            <!-- end main content-->

            <!--modal para ver el listado de solicitudes -->
            <div class="modal fade modal-listado" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
                                                <div class="modal-dialog modal-xl modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title mt-0">Buscar todos los Cambios</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">  
                                                               <table class="table table-hover no-footer" id="tabla_oficios" width="100%">
                                                                <thead class="text-white" style="background-color: #480912;">
                                                                <tr>
                                                                    <th>Año</th>
                                                                    <th>Folio</th>
                                                                    <th>Clave Incorrecta</th>
                                                                    <th>Nombre Incorrecto</th>
                                                                    <th>Clave Correcta</th>
                                                                    <th>Nombre Correcto</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                               </table>      
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btn_cancelar">Cerrar</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
            <!--en modal -->