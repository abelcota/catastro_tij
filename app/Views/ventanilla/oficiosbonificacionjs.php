  <!-- JAVASCRIPT -->
  <script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>
    
    <!-- Required datatable js -->
    <script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
     <!-- Buttons examples -->
     <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
       <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
       
   
       <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>
       <!-- webcam -->
       <script src="<?php echo base_url("assets/js/webcam.min.js");?>"></script>
   
   
       <!-- App js -->
       <script src="<?php echo base_url("assets/js/app.js");?>"></script>
       <script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script>
       <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
<script>

    $(document).ready(function () {
            get_anio();
            get_fecha();
            cargar_empleados();
            //Dar formato de 3 espacios a los elementos de la clave catasttrales
            $('#POBL').blur(function () {  this.value = zeroPad(this.value, 3); });
            $('#CTEL').blur(function () { this.value = zeroPad(this.value, 3);  });
            $('#MANZ').blur(function () { this.value = zeroPad(this.value, 3);  });
            $('#PRED').blur(function () { this.value = zeroPad(this.value, 3);  });
            $('#UNID').blur(function () { this.value = zeroPad(this.value, 3);  });
            $('#POBL2').blur(function () {  this.value = zeroPad(this.value, 3); });
            $('#CTEL2').blur(function () { this.value = zeroPad(this.value, 3);  });
            $('#MANZ2').blur(function () { this.value = zeroPad(this.value, 3);  });
            $('#PRED2').blur(function () { this.value = zeroPad(this.value, 3);  });
            $('#UNID2').blur(function () { this.value = zeroPad(this.value, 3);  });

    });

    $(document).on('click','#btn_nuevo',function(e) {
            $('#btn_limpiar').click();
            $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD"));  
             genera_folio();
             //calculamos el nuevo folio
             $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Generando Folio Nuevo...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
            
             
        });

    //Funcionalidad del folio al presionar enter
    $(document).on('dblclick','#folio',function(e) {    
            genera_folio();
    });

    //funcionalidad del boton validar 1
    $(document).on('click', '#btn_validar', function () {
             //limpiamos el consultado
            if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }

            //obtener los valores de los input de la clave
            var pobl = $('#POBL').val();
             var ctel = $('#CTEL').val();
             var manz = $('#MANZ').val();
             var pred = $('#PRED').val();
             var unid = $('#UNID').val();

             var cvecat = pobl + ctel + manz + pred + unid;

             console.log("validando clave: " + cvecat)

             $.ajax({
                url: "<?php echo base_url("padron/validarcve");?>/"+cvecat,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Validando Clave Catastral...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                        console.log(data.result);
                        if(data.result.length > 0){
                            var nombrecompleto = data.result[0].NOM_PROP + " " + data.result[0].APE_PAT + " " + data.result[0].APE_MAT;
                            $("#nombre").val(nombrecompleto);
                            var domicilio = data.result[0].UBI_PRED + " COL. " + data.result[0].NOM_COL;
                            $("#domicilio").val(domicilio);
                            $("#superficiecons").val(data.result[0].SUP_CONST);
                            $("#superficieterr").val(data.result[0].SUP_TERR);
                            $("#valorcat").val(Number(parseFloat(data.result[0].VAL_TERR) + parseFloat(data.result[0].VAL_CONST)));
                            //mostramos en el mapa 
                            jsonobj = jQuery.parseJSON(data.result[0].jsonb_build_object);
                            if (jsonobj.features != null) {
                                //Se visualiza el poligono en el mapa
                                visualizar_capa(jsonobj);                                
                            }else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Vectorial no encontrado',
                                    text: 'La clave catastral no se encontró en el vectorial!'     
                                })
                            }
                        }else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Alfanumerico no encontrado',
                                    text: 'La clave catastral no se encontró en el alfanumerico!'     
                                })
                        }

                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });

   });

   $(document).on('click', '#btn_listado', function () {
        //limpiamos el consultado
        $(".modal-listado").modal('show');

        $("#tabla_oficios").DataTable().destroy();
        var table = $("#tabla_oficios").DataTable({
                ajax: "<?php echo base_url("ventanilla/get_oficiosbonificacion")?>/"+get_anio(),
                Processing : true,
                ServerSide : true,
                "language": {
                    "url": "<?php echo base_url("assets/Spanish.json")?>"
                },
                columns : [
                    { "data" : "IdAnio" },
                    { "data" : "IdFolio" },
                    { "data" : "ClaveIncorrecta" },
                    { "data" : "NombreIncorrecto" },
                    { "data" : "ClaveCorrecta" },  
                    { "data" : "NombreCorrecto" },        
                ],
                rowCallback: function(row, data, index) {
                    $("td:eq(1)", row).addClass("font-weight-bold"); 
                    $("td:eq(2)", row).addClass("font-weight-bold text-primary"); 
                    $("td:eq(3)", row).addClass("text-nowrap"); 
                    $("td:eq(4)", row).addClass("text-wrap"); 

                }
            });

    });

   //funcionalidad del boton validar 1
   $(document).on('click', '#btn_validar2', function () {
             //limpiamos el consultado

            //obtener los valores de los input de la clave
            var pobl = $('#POBL2').val();
             var ctel = $('#CTEL2').val();
             var manz = $('#MANZ2').val();
             var pred = $('#PRED2').val();
             var unid = $('#UNID2').val();

             var cvecat = pobl + ctel + manz + pred + unid;

             console.log("validando clave 2: " + cvecat)

             $.ajax({
                url: "<?php echo base_url("padron/validarcve");?>/"+cvecat,
                dataType: "json",
                   beforeSend : function (){
                        $.blockUI({ 
                            fadeIn : 0,
                            fadeOut : 0,
                            showOverlay : false,
                            message: '<img src="/assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Validando Clave Catastral...</h3><br><h5>Espere un momento por favor</h5>', 
                            css: {
                            backgroundColor: 'white',
                            border: '0', 
                            }, });
                    },
                   success: function (data) {
                        console.log(data.result);
                        if(data.result.length > 0){
                            var nombrecompleto = data.result[0].NOM_PROP + " " + data.result[0].APE_PAT + " " + data.result[0].APE_MAT;
                            $("#nombre2").val(nombrecompleto);
                            var domicilio = data.result[0].UBI_PRED + " COL. " + data.result[0].NOM_COL;
                            $("#domicilio2").val(domicilio);
                            $("#superficiecons2").val(data.result[0].SUP_CONST);
                            $("#superficieterr2").val(data.result[0].SUP_TERR);
                            $("#valorcat2").val(Number(parseFloat(data.result[0].VAL_TERR) + parseFloat(data.result[0].VAL_CONST)));
                            //mostramos en el mapa 
                            jsonobj = jQuery.parseJSON(data.result[0].jsonb_build_object);
                            if (jsonobj.features != null) {
                                //Se visualiza el poligono en el mapa
                                visualizar_capa(jsonobj);                                
                            }else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Vectorial no encontrado',
                                    text: 'La clave catastral no se encontró en el vectorial!'     
                                })
                            }
                        }else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Alfanumerico no encontrado',
                                    text: 'La clave catastral no se encontró en el alfanumerico!'     
                                })
                        }

                   },
                   complete : function (){
                        $.unblockUI();
                    }
               });

   });

   //funcionalidad del boton guardar

   $(document).on('click','#btn_guardar',function(e) {
        e.preventDefault();
        var a = $("#year").val();
        var f = $("#folio").val();
        var form = $("#form-incorrecto");
        var form2 = $("#form-correcto");
        var obs = $("#observaciones").val();
        var atendio = $("#empleados").val();
        var data = form.serialize() + "&" + form2.serialize() + "&observaciones=" + obs + "&IdEpleadoBarra=" + atendio;
        
        console.log(data);

        $.ajax({
            url: "<?php echo base_url("ventanilla/guardar_oficio_bonificacion"); ?>",
            type: "POST",
            data: data,//form.serialize(),
                success: function (data) {
                    //console.log(data);
                    Swal.fire({
                               icon: 'success',
                               title: 'Mensaje del Sistema',
                               text: data
                               
                           }).then(function() {
                                //$("#btn_guardar").prop("disabled", false);
                                $("#btn_limpiar").click();
                            });

                           //mandar imprimir
                            var url = "/reportes/pdfoficiobonificacion.php";
                            var form = document.createElement("form");
                                    form.setAttribute("method", "post");
                                    form.setAttribute("target", "_new");
                                    form.setAttribute("action", url);
                                    var params = { 'a': a, 'fol':f};
                                    for(var key in params) {
                                        if(params.hasOwnProperty(key)) {
                                            var hiddenField = document.createElement("input");
                                            hiddenField.setAttribute("type", "hidden");
                                            hiddenField.setAttribute("name", key);
                                            hiddenField.setAttribute("value", params[key]);

                                            form.appendChild(hiddenField);
                                        }
                                    }
                                    document.body.appendChild(form);
                                    form.submit();
                }
        });  
   });      

   $(document).on('click','#btn_limpiar',function(e) {
            if (typeof _geojson_vectorSource != "undefined") {
                _geojson_vectorSource.clear();
            }

            $("#form-incorrecto")[0].reset();
            $("#form-correcto")[0].reset();
            $("#observaciones").val("");
            $('#empleados').val(null).trigger('change');
            get_anio();
            $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD")); 
        });

    function get_anio(){
        var anio = new Date().getFullYear().toString(); 
        var a = anio.substr(2,4);
        $('#year').val(a); 
        return a;
    }

    function get_fecha()
    {
       $('#dtfecha').val(moment(new Date()).format("YYYY-MM-DD"));  
    }

    function genera_folio()
         {
            $.getJSON("<?php echo base_url("ventanilla/genera_folio_bonificacion/");?>/"+get_anio(), function(json){ 
                $.each(json.data, function(i, obj){
                    var max = parseInt(obj.maximo);
                    var max = max + 1; 
                    $('#folio').val(zeroPad(max, 4));
                });       
                $.unblockUI();
             });
         }

    function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
    }

    function cargar_empleados(){
            $.getJSON("<?php echo base_url("padron/empleados/05");?>", function(json){
                    $('#empleados').empty();
                    $('#empleados').append($('<option>').text("Seleccionar Empleado").attr({'value':""}));
                    $.each(json.result, function(i, obj){
                            $('#empleados').append($('<option>').text(obj.Nombre).attr({ 'value' : obj.IdEmpleado}));
                    });
                    //$('#empleados').select2();
            });
    }

</script>