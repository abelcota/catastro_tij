<!-- JAVASCRIPT -->
<script src="<?php echo base_url("assets/js/sweetalert2@10.js");?>"></script>

<!-- Required datatable js -->
<script src="<?php echo base_url("assets/libs/datatables.net/js/jquery.dataTables.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js");?>"></script>
 <!-- Buttons examples -->
 <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/jszip/jszip.min.js");?>"></script>
   
   <script src="<?php echo base_url("assets/libs/pdfmake/build/vfs_fonts.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.html5.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
   <script src="<?php echo base_url("assets/libs/datatables.net-buttons/js/buttons.colVis.min.js");?>"></script>
   

   <script src="<?php echo base_url("assets/js/dataTables.scroller.min.js");?>"></script>
   <script src="<?php echo base_url("assets/js/jquery.blockUI.js");?>"></script>


   <!-- App js -->
   <script src="<?php echo base_url("/assets/js/app.js");?>"></script>
   <script src="<?php echo base_url("https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js");?>"></script>
   <script src="<?php echo base_url("/assets/js/dataTables.fixedHeader.min.js");?>"></script>
   <script type="text/javascript" src="<?php echo base_url("assets/js/moment-with-locales.min.js");?>"></script>
   
<script>

$(document).on('click', '#btn_consultar', function (e) {
    e.preventDefault();;
    //obtener el radio seleccionado 
    var rad = $('input[name=radio_tipo]:checked').val();
    if(rad === '03')
      $("#titulo").text('I N C O N F O R M I D A D E S');
    else
      $("#titulo").text('O T R O S  T R A M I T E S');
    var f1 = $("#start").val();
    var f2 = $("#end").val();

    //obtener el radio seleccionado 

    var url = "<?php echo base_url("ventanilla/get_reporte_inco_x_clavecat")?>/"+rad;
    console.log(url);
    //Destroy the old Datatable
    $('#tbl-rpt').DataTable().destroy();
    $('#tbl-rpt').DataTable({
    "orderCellsTop": true,
    "language": {
        "url": "<?php echo base_url("assets/Spanish.json")?>"
    },
    "bProcessing": false,
    "sAjaxSource": url,
    "bPaginate": false,
    "bFilter": false,
    "bInfo": false,
    "order": [[ 0, "asc" ]],
    "aoColumns": [
        { mData: 'Clave' },
        { mData: 'AnioDeclaracion' },
        { mData: 'DescripcionTramite' },
        { mData: 'total' },
    ],
    rowCallback: function(row, data, index) {
            $("td:eq(0)", row).text(formatFriendCode( data['Clave'], 3, '-').slice(0, -1));
            $("td:eq(0)", row).addClass('small');  
            $("td:eq(1)", row).addClass('small');  
            $("td:eq(2)", row).addClass('small');  
            $("td:eq(3  )", row).addClass('small');         
        }
});

});

$(document).ready(function () {

});

function formatFriendCode(friendCode, numDigits, delimiter) {
  return Array.from(friendCode).reduce((accum, cur, idx) => {
    return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
  }, '')
}

</script>