-- Table: public.Inco_Barra_temp

-- DROP TABLE public."Inco_Barra_temp";

CREATE TABLE IF NOT EXISTS public."Inco_Barra_temp"
(
    "IdInconformidad" character varying(5) COLLATE pg_catalog."default",
    "IdTramite" character varying(4) COLLATE pg_catalog."default",
    "Poblacion" integer,
    "Cuartel" integer,
    "Manzana" integer,
    "Predio" integer,
    "Unidad" integer,
    "Clave" character varying(15) COLLATE pg_catalog."default",
    "NombrePropietario" character varying(255) COLLATE pg_catalog."default",
    "NombreSolicitante" character varying(255) COLLATE pg_catalog."default",
    "DomicilioSolicitante" character varying(255) COLLATE pg_catalog."default",
    "IdCalleSolicitante" real,
    "NumeroOficialSolicitante" character varying(15) COLLATE pg_catalog."default",
    "OrientacionSolicitante" character varying(15) COLLATE pg_catalog."default",
    "IdColoniaSolicitante" real,
    "CodigoPostalSolicitante" character varying(15) COLLATE pg_catalog."default",
    "TelefonoSolicitante" character varying(30) COLLATE pg_catalog."default",
    "IdPropietario" double precision,
    "IdColonia" double precision,
    "IdCalle" double precision,
    "NumeroOficial" character varying(15) COLLATE pg_catalog."default",
    "ValorCatastral" real,
    "IdUso" character varying(4) COLLATE pg_catalog."default",
    "ObservacionesBarra" character varying(200) COLLATE pg_catalog."default",
    "IdDeclaracion" character varying(2) COLLATE pg_catalog."default",
    "IdEmpleadoBarra" character varying(4) COLLATE pg_catalog."default",
    "FechaCapturaInconformidad" timestamp(6) without time zone,
    "OtrosDocumentos" character varying(100) COLLATE pg_catalog."default",
    "TotalSuperficieTerreno" double precision,
    "TotalSuperficieConstruccion" double precision,
    "IdDocumento1" integer,
    "IdDocumento2" integer,
    "IdDocumento3" integer,
    "IdDocumento4" integer,
    "IdDocumento5" integer,
    "IdDocumento6" integer,
    "Fol_Mov" double precision,
    "Fec_Ava" timestamp(6) without time zone,
    cve_concep double precision,
    observa01 character varying(255) COLLATE pg_catalog."default",
    "fec_Avaluo" double precision,
    "AnioDeclaracion" character varying(2) COLLATE pg_catalog."default",
    "TramiteNotario" character varying(3) COLLATE pg_catalog."default",
    "FechaEntregaTramite" timestamp(6) without time zone,
    "TramiteICES" character varying(5) COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public."Inco_Barra_temp"
    OWNER to postgres;