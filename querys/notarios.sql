/*
 Navicat Premium Data Transfer

 Source Server         : catastrocln
 Source Server Type    : PostgreSQL
 Source Server Version : 100015
 Source Host           : localhost:5432
 Source Catalog        : catastrocln
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100015
 File Encoding         : 65001

 Date: 24/05/2021 17:26:45
*/


-- ----------------------------
-- Table structure for notarios
-- ----------------------------
DROP TABLE IF EXISTS "public"."notarios";
CREATE TABLE "public"."notarios" (
  "cve_notario" varchar(3) COLLATE "pg_catalog"."default" NOT NULL,
  "nombre" varchar(100) COLLATE "pg_catalog"."default" NOT NULL
)
;
ALTER TABLE "public"."notarios" OWNER TO "postgres";

-- ----------------------------
-- Records of notarios
-- ----------------------------
BEGIN;
INSERT INTO "public"."notarios" VALUES ('002', 'CÉSAR VALADES SOTO');
INSERT INTO "public"."notarios" VALUES ('003', 'DONACIANO GARZÓN LÓPEZ');
INSERT INTO "public"."notarios" VALUES ('004', 'EDUARDO NIEBLA ÁLVAREZ');
INSERT INTO "public"."notarios" VALUES ('005', 'ELEUTERIO RÍOS ESPINOZA');
INSERT INTO "public"."notarios" VALUES ('006', 'FELIPE GÓMEZ FLORES');
INSERT INTO "public"."notarios" VALUES ('007', 'FRANCISCO EDUARDO URREA SALAZAR');
INSERT INTO "public"."notarios" VALUES ('008', 'FRANCISCO JAVIER GAXIOLA BELTRÁN');
INSERT INTO "public"."notarios" VALUES ('009', 'FRANCISCO XAVIER GARCÍA FÉLIX');
INSERT INTO "public"."notarios" VALUES ('010', 'GERARDO GAXIOLA DÍAZ');
INSERT INTO "public"."notarios" VALUES ('011', 'GILDARDO AMARILLAS LÓPEZ');
INSERT INTO "public"."notarios" VALUES ('012', 'GONZALO ARMIENTA HERNÁNDEZ');
INSERT INTO "public"."notarios" VALUES ('013', 'GUADALUPE NOHEMÍ PACHECO IBARRA');
INSERT INTO "public"."notarios" VALUES ('014', 'JESÚS ALFREDO ESQUER RUIZ');
INSERT INTO "public"."notarios" VALUES ('015', 'JESÚS MANUEL ORTIZ ANDRADE');
INSERT INTO "public"."notarios" VALUES ('016', 'JOSÉ LUIS MONARREZ PALAZUELOS');
INSERT INTO "public"."notarios" VALUES ('017', 'JOSÉ RAFAEL CEBREROS BRINGAS');
INSERT INTO "public"."notarios" VALUES ('018', 'JUAN JOSÉ RUIZ OROZCO');
INSERT INTO "public"."notarios" VALUES ('019', 'BLANCA HAYDEE CASTRO LÓPEZ');
INSERT INTO "public"."notarios" VALUES ('020', 'MANUEL ANTONIO CHÁVEZ LÓPEZ');
INSERT INTO "public"."notarios" VALUES ('021', 'LUIS GUILLERMO MONTAÑO VILLALOBOS');
INSERT INTO "public"."notarios" VALUES ('022', 'MANUEL DÍAZ SALAZAR');
INSERT INTO "public"."notarios" VALUES ('023', 'MARIO ALBERTO NIEBLA PARRA');
INSERT INTO "public"."notarios" VALUES ('024', 'MARISELA MONJARAZ ARTEAGA');
INSERT INTO "public"."notarios" VALUES ('025', 'OSCAR ARIEL CARRILLO ECHEAGARAY');
INSERT INTO "public"."notarios" VALUES ('026', 'OSCAR GUILLERMO CORRALES LÓPEZ');
INSERT INTO "public"."notarios" VALUES ('027', 'OSCAR LÓPEZ CASTRO');
INSERT INTO "public"."notarios" VALUES ('028', 'PABLO GASTELUM CASTRO');
INSERT INTO "public"."notarios" VALUES ('029', 'RAMÓN ENRIQUE BASTIDAS ANGULO');
INSERT INTO "public"."notarios" VALUES ('030', 'RAÚL JESÚS IZABAL MONTOYA');
INSERT INTO "public"."notarios" VALUES ('031', 'RENÉ GONZÁLEZ OBESO');
INSERT INTO "public"."notarios" VALUES ('032', 'SAMUEL LARA LÓPEZ');
INSERT INTO "public"."notarios" VALUES ('001', 'CÉSAR MANUEL GUERRA SAINZ');
INSERT INTO "public"."notarios" VALUES ('000', 'S/N');
COMMIT;

-- ----------------------------
-- Primary Key structure for table notarios
-- ----------------------------
ALTER TABLE "public"."notarios" ADD CONSTRAINT "notarios_pkey" PRIMARY KEY ("cve_notario");
