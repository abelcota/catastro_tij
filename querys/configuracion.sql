/*
 Navicat Premium Data Transfer

 Source Server         : catastrocln
 Source Server Type    : PostgreSQL
 Source Server Version : 100015
 Source Host           : localhost:5432
 Source Catalog        : catastrocln
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100015
 File Encoding         : 65001

 Date: 24/05/2021 17:26:18
*/


-- ----------------------------
-- Table structure for configuracion
-- ----------------------------
DROP TABLE IF EXISTS "public"."configuracion";
CREATE TABLE "public"."configuracion" (
  "cerfile" varchar(500) COLLATE "pg_catalog"."default",
  "pemkeyfile" varchar(500) COLLATE "pg_catalog"."default",
  "passphrase" varchar(500) COLLATE "pg_catalog"."default",
  "director" varchar(200) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."configuracion" OWNER TO "postgres";

-- ----------------------------
-- Records of configuracion
-- ----------------------------
BEGIN;
INSERT INTO "public"."configuracion" VALUES ('CSD_KARLA_FUENTE_NOLASCO_FUNK671228PH6_20190528_174243s.cer', 'CSD_KARLA_FUENTE_NOLASCO_FUNK671228PH6_20190528_174243.key', 'MTIzNDU2Nzhh', 'Pedro Rios Morgan');
COMMIT;
