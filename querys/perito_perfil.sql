/*
 Navicat Premium Data Transfer

 Source Server         : catastrocln
 Source Server Type    : PostgreSQL
 Source Server Version : 100015
 Source Host           : localhost:5432
 Source Catalog        : catastrocln
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100015
 File Encoding         : 65001

 Date: 07/09/2021 17:27:10
*/


-- ----------------------------
-- Table structure for perito_perfil
-- ----------------------------
DROP TABLE IF EXISTS "public"."perito_perfil";
CREATE TABLE "public"."perito_perfil" (
  "id_perfil" SERIAL,
  "id_usuario" int4,
  "rfc" varchar(20) COLLATE "pg_catalog"."default",
  "nombre" varchar(100) COLLATE "pg_catalog"."default",
  "clave_registro" varchar(50) COLLATE "pg_catalog"."default",
  "especialidad" varchar(255) COLLATE "pg_catalog"."default",
  "direccion" varchar(255) COLLATE "pg_catalog"."default",
  "telefono" varchar(20) COLLATE "pg_catalog"."default",
  "celular" varchar(20) COLLATE "pg_catalog"."default",
  "correo" varchar(50) COLLATE "pg_catalog"."default",
  "estatus" varchar(10) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."perito_perfil" OWNER TO "postgres";

-- ----------------------------
-- Uniques structure for table perito_perfil
-- ----------------------------
ALTER TABLE "public"."perito_perfil" ADD CONSTRAINT "unique_user" UNIQUE ("id_usuario");

-- ----------------------------
-- Primary Key structure for table perito_perfil
-- ----------------------------
ALTER TABLE "public"."perito_perfil" ADD CONSTRAINT "perito_pefil_pkey" PRIMARY KEY ("id_perfil");

-- ----------------------------
-- Foreign Keys structure for table perito_perfil
-- ----------------------------
ALTER TABLE "public"."perito_perfil" ADD CONSTRAINT "fk_perfil_users" FOREIGN KEY ("id_usuario") REFERENCES "public"."users" ("id") ON DELETE CASCADE ON UPDATE CASCADE;
