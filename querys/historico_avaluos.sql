
CREATE TABLE "public"."h_const" (
  "folio" text COLLATE "pg_catalog"."default",
  "NUM_CONST" float8,
  "CVE_CAT_CO" varchar(5) COLLATE "pg_catalog"."default",
  "DES_CAT_CO" varchar(30) COLLATE "pg_catalog"."default",
  "valorunitario" float8,
  "SUP_CONST" float8,
  "CVE_EDO_CO" float8,
  "EDO_CO" varchar(30) COLLATE "pg_catalog"."default",
  "FEC_CONST" varchar(10) COLLATE "pg_catalog"."default",
  "EDD_CONST" float8,
  "factordemerito" float8,
  "factorcomercializacion" float8,
  "valorneto" float8
)
;
ALTER TABLE "public"."h_const" OWNER TO "postgres";

CREATE TABLE "public"."h_prop_u" (
  "folio" text COLLATE "pg_catalog"."default",
  "NUM_PROP" float8,
  "CVE_PROP" float8,
  "TIP_PERS" varchar(1) COLLATE "pg_catalog"."default",
  "APE_PAT" varchar(80) COLLATE "pg_catalog"."default",
  "APE_MAT" varchar(80) COLLATE "pg_catalog"."default",
  "NOM_PROP" varchar(80) COLLATE "pg_catalog"."default",
  "REG_FED" varchar(13) COLLATE "pg_catalog"."default",
  "STT_AFE_SU" varchar(1) COLLATE "pg_catalog"."default",
  "STT_AFE_IN" varchar(1) COLLATE "pg_catalog"."default",
  "FOTO" bytea
)
;
ALTER TABLE "public"."h_prop_u" OWNER TO "postgres";


CREATE TABLE "public"."h_terr" (
  "folio" text COLLATE "pg_catalog"."default",
  "CVE_POBL" float8,
  "NUM_TERR" float8,
  "TIP_TERR" varchar(1) COLLATE "pg_catalog"."default",
  "CVE_CALLE" float8,
  "CVE_TRAMO" float8,
  "SUP_TERR" float8,
  "valorunitario1" float8,
  "demeritotramo1" int4,
  "CVE_CALLE_" float8,
  "CVE_TRAMO_" float8,
  "SUP_TERR_E" float8,
  "valorunitario2" float8,
  "demeritotramo2" int4,
  "SUP_TERR_D" float8,
  "demeritos" text COLLATE "pg_catalog"."default",
  "FAC_DEM_TE" float8,
  "valorneto" float8
)
;
ALTER TABLE "public"."h_terr" OWNER TO "postgres";


CREATE TABLE "public"."h_unidad" (
  "folio" text COLLATE "pg_catalog"."default",
  "CVE_POBL" float8,
  "NUM_CTEL" float8,
  "NUM_MANZ" float8,
  "NUM_PRED" float8,
  "NUM_UNID" float8,
  "CVE_CALLE_" float8,
  "NUM_OFIC_U" varchar(8) COLLATE "pg_catalog"."default",
  "UBI_PRED" varchar(36) COLLATE "pg_catalog"."default",
  "CVE_COL_UB" float8,
  "DOM_NOT" varchar(36) COLLATE "pg_catalog"."default",
  "NOM_COL_NO" varchar(30) COLLATE "pg_catalog"."default",
  "COD_POST_N" float8,
  "NUM_FTES" float8,
  "LON_FTE" float8,
  "LON_FONDO" float8,
  "CVE_REGIM" float8,
  "TIP_EFEC" varchar(1) COLLATE "pg_catalog"."default",
  "TRI_EFEC" float8,
  "ATO_EFEC" float8,
  "NUM_LIB" float8,
  "NUM_SECC" float8,
  "NUM_INSC" float8,
  "NUM_ESC" float8,
  "FEC_ALTA" varchar(10) COLLATE "pg_catalog"."default",
  "FEC_MOV" varchar(10) COLLATE "pg_catalog"."default",
  "STT_COND" varchar(1) COLLATE "pg_catalog"."default",
  "SUP_TERR" float8,
  "SUP_CONST" float8,
  "VAL_TERR" float8,
  "VAL_CONST" float8,
  "SUP_TERR_A" float8,
  "SUP_CONST_" float8,
  "VAL_TERR_A" float8,
  "VAL_CONST_" float8,
  "TIP_IMPTO" float8,
  "NUM_RANGO" varchar(3) COLLATE "pg_catalog"."default",
  "CLAVE" varchar(15) COLLATE "pg_catalog"."default",
  "STT_REST" varchar(1) COLLATE "pg_catalog"."default",
  "IMPTO_ANIO" float8,
  "TPO_DESCTO" varchar(1) COLLATE "pg_catalog"."default",
  "STS_SERVIC" varchar(11) COLLATE "pg_catalog"."default",
  "CVE_USO" varchar(4) COLLATE "pg_catalog"."default",
  "ID_REGTO" varchar(1) COLLATE "pg_catalog"."default",
  "STS_MOVTO" varchar(1) COLLATE "pg_catalog"."default",
  "CVE_PROP" float8,
  "CVE_CALLE" numeric,
  "NOM_CALLE" varchar(45) COLLATE "pg_catalog"."default",
  "TIP_CALLE" varchar(15) COLLATE "pg_catalog"."default",
  "CALLE" varchar(45) COLLATE "pg_catalog"."default",
  "CVE_COL" numeric,
  "TIPO_COL" varchar(25) COLLATE "pg_catalog"."default",
  "NOM_COL" varchar(40) COLLATE "pg_catalog"."default",
  "COLONIA" varchar(40) COLLATE "pg_catalog"."default",
  "COD_POS" varchar(5) COLLATE "pg_catalog"."default",
  "TIP_PERS" varchar(1) COLLATE "pg_catalog"."default",
  "APE_PAT" varchar(80) COLLATE "pg_catalog"."default",
  "APE_MAT" varchar(80) COLLATE "pg_catalog"."default",
  "NOM_PROP" varchar(80) COLLATE "pg_catalog"."default",
  "REG_FED" varchar(13) COLLATE "pg_catalog"."default",
  "FOTO" bytea,
  "DES_USO" varchar(30) COLLATE "pg_catalog"."default",
  "DES_REGIM" varchar(30) COLLATE "pg_catalog"."default",
  "TIP_PROP" varchar(1) COLLATE "pg_catalog"."default",
  "geometrico" json
)
;
ALTER TABLE "public"."h_unidad" OWNER TO "postgres";

CREATE TABLE "public"."h_uso_u" (
  "folio" text COLLATE "pg_catalog"."default",
  "CVE_POBL" float8,
  "NUM_CTEL" float8,
  "NUM_MANZ" float8,
  "NUM_PRED" float8,
  "NUM_UNID" float8,
  "NUM_USO" float8,
  "CVE_GPO_USO" varchar(1) COLLATE "pg_catalog"."default",
  "CVE_USO" float8,
  "CLAVE" varchar(15) COLLATE "pg_catalog"."default",
  "DES_USO" varchar(30) COLLATE "pg_catalog"."default",
  "cve_comp" text COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."h_uso_u" OWNER TO "postgres";

CREATE TABLE "public"."h_zona" (
  "folio" text COLLATE "pg_catalog"."default",
  "CVE_POBL" float8,
  "CVE_ZONA" float8,
  "DES_ZONA" varchar(30) COLLATE "pg_catalog"."default",
  "VAL_UNIT_T" float8,
  "FAC_DEM_VA" float8,
  "STT_AFE_SU" varchar(1) COLLATE "pg_catalog"."default",
  "STT_AFE_IN" varchar(1) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."h_zona" OWNER TO "postgres";

-- ----------------------------
-- Uniques structure for table h_unidad
-- ----------------------------
ALTER TABLE "public"."h_unidad" ADD CONSTRAINT "unique_h_unidad" UNIQUE ("folio");
