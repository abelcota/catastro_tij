-- Table: public.h_prop_u

-- DROP TABLE public.h_prop_u;

CREATE TABLE IF NOT EXISTS public.h_prop_u
(
    folio text COLLATE pg_catalog."default",
    "NUM_PROP" double precision,
    "CVE_PROP" double precision,
    "TIP_PERS" character varying(1) COLLATE pg_catalog."default",
    "APE_PAT" character varying(80) COLLATE pg_catalog."default",
    "APE_MAT" character varying(80) COLLATE pg_catalog."default",
    "NOM_PROP" character varying(80) COLLATE pg_catalog."default",
    "REG_FED" character varying(13) COLLATE pg_catalog."default",
    "STT_AFE_SU" character varying(1) COLLATE pg_catalog."default",
    "STT_AFE_IN" character varying(1) COLLATE pg_catalog."default",
    "FOTO" bytea,
    clave character varying(255) COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.h_prop_u
    OWNER to postgres;