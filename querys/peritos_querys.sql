-- FUNCTION: public.generate_inf_id()

-- DROP FUNCTION public.generate_inf_id();

CREATE OR REPLACE FUNCTION public.generate_inf_id(
	)
    RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
begin
    return (SELECT COALESCE(MAX(uuid)::int + 1, CONCAT(EXTRACT(YEAR FROM now())::text, '000001')::int) as maximo FROM perito_infcat_detalle);
end;
$BODY$;

ALTER FUNCTION public.generate_inf_id()
    OWNER TO postgres;


-- Table: public.perito_avaluo

-- DROP TABLE public.perito_avaluo;

CREATE TABLE IF NOT EXISTS public.perito_avaluo
(
    id SERIAL,
    id_perito integer NOT NULL,
    cve_mpio integer NOT NULL,
    cve_pobl integer NOT NULL,
    num_ctel integer NOT NULL,
    num_manz integer NOT NULL,
    num_pred integer NOT NULL,
    num_unid integer NOT NULL,
    clave_cat character varying(18) COLLATE pg_catalog."default" NOT NULL,
    fecha_creacion timestamp without time zone DEFAULT now(),
    ultima_modificacion timestamp without time zone,
    fecha_autorizacion timestamp without time zone,
    informacion_id character varying(10) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT perito_avaluo_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo
    OWNER to postgres;


-- Table: public.perito_avaluo_antecedentes

-- DROP TABLE public.perito_avaluo_antecedentes;

CREATE TABLE IF NOT EXISTS public.perito_avaluo_antecedentes
(
    id_antecedente SERIAL,
    id_avaluo integer,
    solicitante character varying(80) COLLATE pg_catalog."default",
    fecha_inspeccion timestamp(6) without time zone,
    regimen character varying(50) COLLATE pg_catalog."default",
    objeto character varying(50) COLLATE pg_catalog."default",
    rpp character varying(10) COLLATE pg_catalog."default",
    inscripcion character varying(10) COLLATE pg_catalog."default",
    libro character varying(10) COLLATE pg_catalog."default",
    seccion character varying(10) COLLATE pg_catalog."default",
    informacion_id character varying(10) COLLATE pg_catalog."default",
    CONSTRAINT perito_avaluo_antecedentes_pkey PRIMARY KEY (id_antecedente),
    CONSTRAINT unique_antecedentes UNIQUE (id_avaluo),
    CONSTRAINT fk_perito_avaluo FOREIGN KEY (id_avaluo)
        REFERENCES public.perito_avaluo (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo_antecedentes
    OWNER to postgres;

    -- Table: public.perito_avaluo_car_terreno

-- DROP TABLE public.perito_avaluo_car_terreno;

CREATE TABLE IF NOT EXISTS public.perito_avaluo_car_terreno
(
    id_car_terreno serial,
    id_avaluo integer,
    informacion_id character varying(10) COLLATE pg_catalog."default",
    topografia_const character varying(100) COLLATE pg_catalog."default",
    numero_frentes character varying(50) COLLATE pg_catalog."default",
    car_panoramicas character varying(255) COLLATE pg_catalog."default",
    densidad_habitacional character varying(100) COLLATE pg_catalog."default",
    intensidad_const character varying(100) COLLATE pg_catalog."default",
    servidumbre_restricciones character varying(255) COLLATE pg_catalog."default",
    sup_escrituras real,
    sup_topografico real,
    CONSTRAINT perito_avaluo_car_terreno_pkey PRIMARY KEY (id_car_terreno),
    CONSTRAINT unique_car_terre_avaluo UNIQUE (id_avaluo),
    CONSTRAINT fk_avaluo_car_terr FOREIGN KEY (id_avaluo)
        REFERENCES public.perito_avaluo (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo_car_terreno
    OWNER to postgres;

-- Table: public.perito_avaluo_car_urb

-- DROP TABLE public.perito_avaluo_car_urb;

CREATE TABLE IF NOT EXISTS public.perito_avaluo_car_urb
(
    id_car_urb serial,
    id_avaluo integer,
    informacion_id character varying(10) COLLATE pg_catalog."default",
    clasificacionzona character varying(100) COLLATE pg_catalog."default",
    tipoconstdominante character varying(100) COLLATE pg_catalog."default",
    indicesatzona character varying(50) COLLATE pg_catalog."default",
    poblacion character varying(50) COLLATE pg_catalog."default",
    contaminacionambiental character varying(100) COLLATE pg_catalog."default",
    usosuelopermitido character varying(150) COLLATE pg_catalog."default",
    viaaccesoimportancia character varying(255) COLLATE pg_catalog."default",
    agua boolean,
    drenaje boolean,
    energia boolean,
    alumbrado boolean,
    basura boolean,
    transporte boolean,
    telefono boolean,
    banqueta boolean,
    guarniciones boolean,
    pavimento boolean,
    vigilancia boolean,
    CONSTRAINT perito_avaluo_car_urb_pkey PRIMARY KEY (id_car_urb),
    CONSTRAINT unique_carurb_avaluo UNIQUE (id_avaluo),
    CONSTRAINT fk_carurb_avaluo FOREIGN KEY (id_avaluo)
        REFERENCES public.perito_avaluo (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo_car_urb
    OWNER to postgres;

-- Table: public.perito_avaluo_desc_general

-- DROP TABLE public.perito_avaluo_desc_general;

CREATE TABLE IF NOT EXISTS public.perito_avaluo_desc_general
(
    id_desc_general serial,
    id_avaluo integer,
    informacion_id character varying(10) COLLATE pg_catalog."default",
    cve_uso character varying(5) COLLATE pg_catalog."default",
    edad_aprox integer,
    num_niveles integer,
    estado_conservacion character varying(50) COLLATE pg_catalog."default",
    calidad_proyecto character varying(50) COLLATE pg_catalog."default",
    unidades_rentables character varying(255) COLLATE pg_catalog."default",
    descripcion_general character varying(500) COLLATE pg_catalog."default",
    CONSTRAINT perito_avaluo_desc_general_pkey PRIMARY KEY (id_desc_general),
    CONSTRAINT unique_desc_gen UNIQUE (id_avaluo),
    CONSTRAINT fk_avaluo_desc_gen FOREIGN KEY (id_avaluo)
        REFERENCES public.perito_avaluo (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo_desc_general
    OWNER to postgres;

-- Table: public.perito_avaluo_detalle

-- DROP TABLE public.perito_avaluo_detalle;

CREATE TABLE IF NOT EXISTS public.perito_avaluo_detalle
(
    id_detalle serial,
    id_avaluo integer NOT NULL,
    en_movimiento character varying(2) COLLATE pg_catalog."default",
    en_revision character varying(2) COLLATE pg_catalog."default",
    reingreso character varying(2) COLLATE pg_catalog."default",
    valor_mercado double precision,
    consideraciones_previas character varying(500) COLLATE pg_catalog."default",
    tipo character varying(10) COLLATE pg_catalog."default",
    motivo character varying(500) COLLATE pg_catalog."default",
    numero_firmas smallint,
    rfc_certificado character varying(13) COLLATE pg_catalog."default",
    serie_certificado character varying(50) COLLATE pg_catalog."default",
    sello_digital character varying(500) COLLATE pg_catalog."default",
    rfc_certificado_aux character varying(13) COLLATE pg_catalog."default",
    serie_certificado_aux character varying(50) COLLATE pg_catalog."default",
    sello_digital_aux character varying(500) COLLATE pg_catalog."default",
    rfc_certificado_aux_valores character varying(13) COLLATE pg_catalog."default",
    serie_certificado_aux_valores character varying(50) COLLATE pg_catalog."default",
    sello_digital_aux_valores character varying(500) COLLATE pg_catalog."default",
    rfc_certificado_director character varying(13) COLLATE pg_catalog."default",
    serie_certificado_director character varying(50) COLLATE pg_catalog."default",
    sello_digital_director character varying(500) COLLATE pg_catalog."default",
    rfc_certificado_delegado character varying(13) COLLATE pg_catalog."default",
    serie_certificado_delegado character varying(50) COLLATE pg_catalog."default",
    sello_digital_delegado character varying(500) COLLATE pg_catalog."default",
    informacion_id character varying(10) COLLATE pg_catalog."default",
    graficado character varying(30) COLLATE pg_catalog."default",
    estatus character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT perito_avaluo_detalle_pkey PRIMARY KEY (id_detalle),
    CONSTRAINT unique_avaluo_detalle UNIQUE (id_avaluo)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo_detalle
    OWNER to postgres;

-- Table: public.perito_avaluo_elementos_construccion

-- DROP TABLE public.perito_avaluo_elementos_construccion;

CREATE TABLE IF NOT EXISTS public.perito_avaluo_elementos_construccion
(
    id_elementos_construccion serial,
    id_avaluo integer,
    informacion_id character varying(10) COLLATE pg_catalog."default",
    a_cimientos character varying(255) COLLATE pg_catalog."default",
    a_estructura character varying(255) COLLATE pg_catalog."default",
    a_muros character varying(255) COLLATE pg_catalog."default",
    a_entrepisos character varying(255) COLLATE pg_catalog."default",
    a_techos character varying(255) COLLATE pg_catalog."default",
    a_azoteas character varying(255) COLLATE pg_catalog."default",
    a_bardas character varying(255) COLLATE pg_catalog."default",
    b_aplanadosinteriores character varying(255) COLLATE pg_catalog."default",
    b_aplanadosexteriores character varying(255) COLLATE pg_catalog."default",
    b_plafones character varying(255) COLLATE pg_catalog."default",
    b_lambrines character varying(255) COLLATE pg_catalog."default",
    b_pisos character varying(255) COLLATE pg_catalog."default",
    b_zoclos character varying(255) COLLATE pg_catalog."default",
    b_escaleras character varying(255) COLLATE pg_catalog."default",
    b_pintura character varying(255) COLLATE pg_catalog."default",
    b_recubrimientosespeciales character varying(255) COLLATE pg_catalog."default",
    c_puertas character varying(255) COLLATE pg_catalog."default",
    c_closets character varying(255) COLLATE pg_catalog."default",
    d_mueblesbano character varying(255) COLLATE pg_catalog."default",
    d_equipodecocina character varying(255) COLLATE pg_catalog."default",
    e_instalacioneselectricas character varying(255) COLLATE pg_catalog."default",
    f_herreria character varying(255) COLLATE pg_catalog."default",
    g_vidrieria character varying(255) COLLATE pg_catalog."default",
    h_cerrajeria character varying(255) COLLATE pg_catalog."default",
    i_fachadas character varying(255) COLLATE pg_catalog."default",
    j_instalacionesespeciales character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT perito_avaluo_elementos_construccion_pkey PRIMARY KEY (id_elementos_construccion),
    CONSTRAINT unique_avaluo_el_const UNIQUE (id_avaluo),
    CONSTRAINT fk_avaluo_elem_const FOREIGN KEY (id_avaluo)
        REFERENCES public.perito_avaluo (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo_elementos_construccion
    OWNER to postgres;

-- Table: public.perito_avaluo_fisico_terreno

-- DROP TABLE public.perito_avaluo_fisico_terreno;

CREATE TABLE IF NOT EXISTS public.perito_avaluo_fisico_terreno
(
    id_avaluofisico serial,
    id_avaluo integer,
    informacion_id character varying(10) COLLATE pg_catalog."default",
    sup_terr real,
    "VAL_UNIT_TERR" real,
    "FAC_DEM_TERR" real,
    "MOTIVO" character varying(100) COLLATE pg_catalog."default",
    "VAL_TERR" real,
    CONSTRAINT perito_avaluo_fisico_terreno_pkey PRIMARY KEY (id_avaluofisico)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo_fisico_terreno
    OWNER to postgres;

-- Table: public.perito_avaluo_imagenes

-- DROP TABLE public.perito_avaluo_imagenes;

CREATE TABLE IF NOT EXISTS public.perito_avaluo_imagenes
(
    id_imagen serial,
    id_avaluo integer,
    informacion_id character varying(10) COLLATE pg_catalog."default",
    url character varying(255) COLLATE pg_catalog."default",
    descripcion character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT perito_avaluo_imagenes_pkey PRIMARY KEY (id_imagen),
    CONSTRAINT fk_avaluo_imagenes FOREIGN KEY (id_avaluo)
        REFERENCES public.perito_avaluo (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo_imagenes
    OWNER to postgres;

-- Table: public.perito_avaluo_orientaciones

-- DROP TABLE public.perito_avaluo_orientaciones;

CREATE TABLE IF NOT EXISTS public.perito_avaluo_orientaciones
(
    id serial,
    informacion_id character varying(10) COLLATE pg_catalog."default" NOT NULL,
    orientacion character varying(20) COLLATE pg_catalog."default" NOT NULL,
    descripcion character varying(100) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT perito_avaluo_orientaciones_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo_orientaciones
    OWNER to postgres;

-- Table: public.perito_avaluo_planos

-- DROP TABLE public.perito_avaluo_planos;

CREATE TABLE IF NOT EXISTS public.perito_avaluo_planos
(
    id_plano serial,
    id_avaluo integer,
    informacion_id character varying(10) COLLATE pg_catalog."default",
    url character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT perito_avaluo_planos_pkey PRIMARY KEY (id_plano),
    CONSTRAINT fk_avaluo_planos FOREIGN KEY (id_avaluo)
        REFERENCES public.perito_avaluo (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo_planos
    OWNER to postgres;

-- Table: public.perito_avaluo_portada

-- DROP TABLE public.perito_avaluo_portada;

CREATE TABLE IF NOT EXISTS public.perito_avaluo_portada
(
    id_portada serial,
    id_avaluo integer,
    informacion_id character varying(10) COLLATE pg_catalog."default",
    inmueble character varying(500) COLLATE pg_catalog."default",
    calle character varying(100) COLLATE pg_catalog."default",
    cve_calle character varying(6) COLLATE pg_catalog."default",
    numero character varying(50) COLLATE pg_catalog."default",
    colonia character varying(100) COLLATE pg_catalog."default",
    cve_colonia character varying(6) COLLATE pg_catalog."default",
    municipio character varying(20) COLLATE pg_catalog."default",
    cp character varying(10) COLLATE pg_catalog."default",
    nota_interna character varying(600) COLLATE pg_catalog."default",
    CONSTRAINT perito_avaluo_portada_pkey PRIMARY KEY (id_portada),
    CONSTRAINT unique_avaluo UNIQUE (id_avaluo),
    CONSTRAINT fk_avaluo_id FOREIGN KEY (id_avaluo)
        REFERENCES public.perito_avaluo (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_avaluo_portada
    OWNER to postgres;

-- Table: public.perito_infcat

-- DROP TABLE public.perito_infcat;

CREATE TABLE IF NOT EXISTS public.perito_infcat
(
    informacion_id uuid NOT NULL,
    nombre character varying(50) COLLATE pg_catalog."default" NOT NULL,
    rfc character varying(20) COLLATE pg_catalog."default" NOT NULL,
    cargo character varying(50) COLLATE pg_catalog."default" NOT NULL,
    estatus character varying(20) COLLATE pg_catalog."default",
    correo character varying(50) COLLATE pg_catalog."default",
    telefono character varying(15) COLLATE pg_catalog."default",
    fecha timestamp(6) without time zone DEFAULT now(),
    CONSTRAINT perito_infcat_pkey PRIMARY KEY (informacion_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_infcat
    OWNER to postgres;

-- Table: public.perito_infcat_detalle

-- DROP TABLE public.perito_infcat_detalle;

CREATE TABLE IF NOT EXISTS public.perito_infcat_detalle
(
    informacion_id uuid NOT NULL,
    concepto character varying(50) COLLATE pg_catalog."default" NOT NULL,
    clave_catastral character varying(20) COLLATE pg_catalog."default" NOT NULL,
    importe real NOT NULL,
    estatus character varying(20) COLLATE pg_catalog."default",
    uuid character varying(10) COLLATE pg_catalog."default" NOT NULL DEFAULT generate_inf_id(),
    fecha timestamp(6) without time zone DEFAULT now(),
    user_id integer,
    CONSTRAINT perito_infcat_detalle_pkey PRIMARY KEY (uuid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_infcat_detalle
    OWNER to postgres;

-- Table: public.perito_perfil

-- DROP TABLE public.perito_perfil;

CREATE TABLE IF NOT EXISTS public.perito_perfil
(
    id_perfil serial,
    id_usuario integer,
    rfc character varying(20) COLLATE pg_catalog."default",
    nombre character varying(100) COLLATE pg_catalog."default",
    clave_registro character varying(50) COLLATE pg_catalog."default",
    especialidad character varying(255) COLLATE pg_catalog."default",
    direccion character varying(255) COLLATE pg_catalog."default",
    telefono character varying(20) COLLATE pg_catalog."default",
    celular character varying(20) COLLATE pg_catalog."default",
    correo character varying(50) COLLATE pg_catalog."default",
    estatus character varying(10) COLLATE pg_catalog."default",
    CONSTRAINT perito_pefil_pkey PRIMARY KEY (id_perfil),
    CONSTRAINT unique_user UNIQUE (id_usuario),
    CONSTRAINT fk_perfil_users FOREIGN KEY (id_usuario)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.perito_perfil
    OWNER to postgres;

-- Table: public.peritos_avaluo_fisico_construcciones

-- DROP TABLE public.peritos_avaluo_fisico_construcciones;

CREATE TABLE IF NOT EXISTS public.peritos_avaluo_fisico_construcciones
(
    id_fisico_construccion serial,
    id_avaluo integer,
    informacion_id character varying(10) COLLATE pg_catalog."default",
    categoria character varying(5) COLLATE pg_catalog."default",
    superficie double precision,
    edad double precision,
    fact_com double precision,
    estado double precision,
    valor_construccion character varying(50) COLLATE pg_catalog."default",
    valor_unitario character varying(50) COLLATE pg_catalog."default",
    fact_dem double precision,
    CONSTRAINT peritos_avaluo_fisico_construcciones_pkey PRIMARY KEY (id_fisico_construccion)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.peritos_avaluo_fisico_construcciones
    OWNER to postgres;

-- Table: public.peritos_avaluo_fisico_especiales

-- DROP TABLE public.peritos_avaluo_fisico_especiales;

CREATE TABLE IF NOT EXISTS public.peritos_avaluo_fisico_especiales
(
    id_fisico_especial serial,
    id_avaluo integer,
    informacion_id character varying(10) COLLATE pg_catalog."default",
    concepto character varying(50) COLLATE pg_catalog."default",
    unidad character varying(20) COLLATE pg_catalog."default",
    cantidad double precision,
    fact_dem double precision,
    valor_unit double precision,
    vrn_unitario character varying(50) COLLATE pg_catalog."default",
    valor_parcial character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT peritos_avaluo_fisico_especiales_pkey PRIMARY KEY (id_fisico_especial)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.peritos_avaluo_fisico_especiales
    OWNER to postgres;

-- Table: public.peritos_avaluo_fisico_terreno

-- DROP TABLE public.peritos_avaluo_fisico_terreno;

CREATE TABLE IF NOT EXISTS public.peritos_avaluo_fisico_terreno
(
    id_fisico_terreno serial,
    id_avaluo integer,
    informacion_id character varying(10) COLLATE pg_catalog."default",
    superficie double precision,
    valor_unitario character varying(20) COLLATE pg_catalog."default",
    factor_demerito double precision,
    motivo character varying(100) COLLATE pg_catalog."default",
    valor_terreno character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT peritos_avaluo_fisico_terreno_pkey PRIMARY KEY (id_fisico_terreno)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.peritos_avaluo_fisico_terreno
    OWNER to postgres;

-- Table: public.usuario_perfil

-- DROP TABLE public.usuario_perfil;

CREATE TABLE IF NOT EXISTS public.usuario_perfil
(
    id_perfil serial,
    id_usuario integer,
    rfc character varying(20) COLLATE pg_catalog."default",
    nombre character varying(100) COLLATE pg_catalog."default",
    direccion character varying(255) COLLATE pg_catalog."default",
    telefono character varying(20) COLLATE pg_catalog."default",
    celular character varying(20) COLLATE pg_catalog."default",
    correo character varying(50) COLLATE pg_catalog."default",
    estatus character varying(10) COLLATE pg_catalog."default",
    cert bytea,
    key bytea,
    keypass character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT perito_perfil_copy1_pkey PRIMARY KEY (id_perfil),
    CONSTRAINT perito_perfil_copy1_id_usuario_key UNIQUE (id_usuario),
    CONSTRAINT user_perfil FOREIGN KEY (id_usuario)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.usuario_perfil
    OWNER to postgres;
