-- Table: public.a_unid_temp

-- DROP TABLE public.a_unid_temp;

CREATE TABLE IF NOT EXISTS public.a_unid_temp
(
    "CVE_POBL" double precision,
    "NUM_CTEL" double precision,
    "NUM_MANZ" double precision,
    "NUM_PRED" double precision,
    "NUM_UNID" double precision,
    "CVE_CALLE_" double precision,
    "NUM_OFIC_U" character varying(8) COLLATE pg_catalog."default",
    "UBI_PRED" character varying(36) COLLATE pg_catalog."default",
    "CVE_COL_UB" double precision,
    "DOM_NOT" character varying(36) COLLATE pg_catalog."default",
    "NOM_COL_NO" character varying(30) COLLATE pg_catalog."default",
    "COD_POST_N" double precision,
    "NUM_FTES" double precision,
    "LON_FTE" double precision,
    "LON_FONDO" double precision,
    "CVE_REGIM" double precision,
    "TIP_EFEC" character varying(1) COLLATE pg_catalog."default",
    "TRI_EFEC" double precision,
    "ATO_EFEC" double precision,
    "NUM_LIB" double precision,
    "NUM_SECC" double precision,
    "NUM_INSC" double precision,
    "NUM_ESC" double precision,
    "FEC_ALTA" character varying(10) COLLATE pg_catalog."default",
    "FEC_MOV" character varying(10) COLLATE pg_catalog."default",
    "STT_COND" character varying(1) COLLATE pg_catalog."default",
    "SUP_TERR" double precision,
    "SUP_CONST" double precision,
    "VAL_TERR" double precision,
    "VAL_CONST" double precision,
    "SUP_TERR_A" double precision,
    "SUP_CONST_" double precision,
    "VAL_TERR_A" double precision,
    "VAL_CONST_" double precision,
    "TIP_IMPTO" double precision,
    "NUM_RANGO" character varying(3) COLLATE pg_catalog."default",
    "CLAVE" character varying(15) COLLATE pg_catalog."default",
    "STT_REST" character varying(1) COLLATE pg_catalog."default",
    "IMPTO_ANIO" double precision,
    "TPO_DESCTO" character varying(1) COLLATE pg_catalog."default",
    "STS_SERVIC" character varying(11) COLLATE pg_catalog."default",
    "CVE_USO" character varying(4) COLLATE pg_catalog."default",
    "ID_REGTO" character varying(1) COLLATE pg_catalog."default",
    "STS_MOVTO" character varying(100) COLLATE pg_catalog."default",
    "CVE_PROP" double precision
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.a_unid_temp
    OWNER to postgres;