-- Table: public.h_zona

-- DROP TABLE public.h_zona;

CREATE TABLE IF NOT EXISTS public.h_zona
(
    folio text COLLATE pg_catalog."default",
    "CVE_POBL" double precision,
    "CVE_ZONA" double precision,
    "DES_ZONA" character varying(30) COLLATE pg_catalog."default",
    "VAL_UNIT_T" double precision,
    "FAC_DEM_VA" double precision,
    "STT_AFE_SU" character varying(1) COLLATE pg_catalog."default",
    "STT_AFE_IN" character varying(1) COLLATE pg_catalog."default",
    clave character varying(255) COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.h_zona
    OWNER to postgres;