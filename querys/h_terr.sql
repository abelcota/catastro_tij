-- Table: public.h_terr

-- DROP TABLE public.h_terr;

CREATE TABLE IF NOT EXISTS public.h_terr
(
    folio text COLLATE pg_catalog."default",
    "CVE_POBL" double precision,
    "NUM_TERR" double precision,
    "TIP_TERR" character varying(1) COLLATE pg_catalog."default",
    "CVE_CALLE" double precision,
    "CVE_TRAMO" double precision,
    "SUP_TERR" double precision,
    valorunitario1 double precision,
    demeritotramo1 integer,
    "CVE_CALLE_" double precision,
    "CVE_TRAMO_" double precision,
    "SUP_TERR_E" double precision,
    valorunitario2 double precision,
    demeritotramo2 integer,
    "SUP_TERR_D" double precision,
    demeritos text COLLATE pg_catalog."default",
    "FAC_DEM_TE" double precision,
    valorneto double precision,
    clave character varying(255) COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.h_terr
    OWNER to postgres;