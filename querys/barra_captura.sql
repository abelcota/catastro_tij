/*
 Navicat Premium Data Transfer

 Source Server         : catastrocln
 Source Server Type    : PostgreSQL
 Source Server Version : 100015
 Source Host           : localhost:5432
 Source Catalog        : catastrocln
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100015
 File Encoding         : 65001

 Date: 24/05/2021 17:25:52
*/


-- ----------------------------
-- Table structure for barra_captura
-- ----------------------------
DROP TABLE IF EXISTS "public"."barra_captura";
CREATE TABLE "public"."barra_captura" (
  "id_captura" int4 NOT NULL DEFAULT nextval('barra_captura_id_captura_seq'::regclass),
  "id_folio" varchar(10) COLLATE "pg_catalog"."default" NOT NULL,
  "id_anio" varchar(3) COLLATE "pg_catalog"."default" NOT NULL,
  "id_tramite" varchar(4) COLLATE "pg_catalog"."default" NOT NULL,
  "fecha_envio" timestamp(6),
  "estatus_verificacion" varchar(2) COLLATE "pg_catalog"."default",
  "verificador" varchar(50) COLLATE "pg_catalog"."default",
  "fecha_captura" timestamp(6),
  "capturista" varchar(50) COLLATE "pg_catalog"."default",
  "clave" varchar(15) COLLATE "pg_catalog"."default",
  "capturista_geom" varchar(50) COLLATE "pg_catalog"."default",
  "fecha_captura_geom" timestamp(6),
  "observaciones_geom" varchar(500) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."barra_captura" OWNER TO "postgres";

-- ----------------------------
-- Records of barra_captura
-- ----------------------------
BEGIN;
INSERT INTO "public"."barra_captura" VALUES (7, '00000004', '21', '20.0', '2021-03-28 15:08:00.781763', 'V', 'Administrador', NULL, '', '999999999999999', NULL, NULL, NULL);
INSERT INTO "public"."barra_captura" VALUES (11, '00000038', '21', '4.0', '2021-04-23 12:52:40.50227', 'GC', 'Administrador', NULL, '', '000028364012001', NULL, NULL, NULL);
INSERT INTO "public"."barra_captura" VALUES (10, '00000030', '21', '6.0', '2021-04-23 11:38:31.699384', 'C', 'Administrador', '2021-04-26 21:46:57.763614', 'Administrador', '060001061017001', NULL, NULL, NULL);
INSERT INTO "public"."barra_captura" VALUES (5, '00000046', '21', '2.4', '2021-03-19 14:19:46.051664', 'C', 'Administrador', '2021-04-26 23:39:51.895282', 'Administrador', '000001010001001', NULL, NULL, NULL);
INSERT INTO "public"."barra_captura" VALUES (8, '00000048', '21', '20.0', '2021-04-04 00:45:12.586263', 'GC', 'Administrador', NULL, '', '000001001001004', 'Administrador', '2021-05-24 01:15:04.932498', NULL);
INSERT INTO "public"."barra_captura" VALUES (16, '00000054', '21', '2.3', '2021-05-24 01:17:23.871161', 'V', 'Administrador', NULL, NULL, '000001001001001', NULL, NULL, NULL);
INSERT INTO "public"."barra_captura" VALUES (6, '00000036', '21', '2.3', '2021-03-24 20:40:22.920002', 'C', 'Administrador', '2021-05-24 13:07:19.334419', 'Administrador', '000025096007001', 'Administrador', '2021-05-24 00:42:05.991198', NULL);
INSERT INTO "public"."barra_captura" VALUES (12, '00000013', '21', '18.0', '2021-05-04 10:55:17.929085', 'C', 'Administrador', '2021-05-24 13:13:29.177901', 'Administrador', '000047196016001', 'Administrador', '2021-05-24 00:43:42.757509', NULL);
COMMIT;

-- ----------------------------
-- Primary Key structure for table barra_captura
-- ----------------------------
ALTER TABLE "public"."barra_captura" ADD CONSTRAINT "barra_captura_pkey" PRIMARY KEY ("id_captura");
