-- Table: public.h_const

-- DROP TABLE public.h_const;

CREATE TABLE IF NOT EXISTS public.h_const
(
    folio text COLLATE pg_catalog."default",
    "NUM_CONST" double precision,
    "CVE_CAT_CO" character varying(5) COLLATE pg_catalog."default",
    "DES_CAT_CO" character varying(30) COLLATE pg_catalog."default",
    valorunitario double precision,
    "SUP_CONST" double precision,
    "CVE_EDO_CO" double precision,
    "EDO_CO" character varying(30) COLLATE pg_catalog."default",
    "FEC_CONST" character varying(10) COLLATE pg_catalog."default",
    "EDD_CONST" double precision,
    factordemerito double precision,
    factorcomercializacion double precision,
    valorneto double precision,
    clave character varying(255) COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.h_const
    OWNER to postgres;