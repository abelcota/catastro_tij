/*
 Navicat Premium Data Transfer

 Source Server         : catastrocln
 Source Server Type    : PostgreSQL
 Source Server Version : 100015
 Source Host           : localhost:5432
 Source Catalog        : catastrocln
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100015
 File Encoding         : 65001

 Date: 24/05/2021 17:25:40
*/


-- ----------------------------
-- Table structure for avaluos
-- ----------------------------
DROP TABLE IF EXISTS "public"."avaluos";
CREATE TABLE "public"."avaluos" (
  "id_avaluo" int4 NOT NULL DEFAULT nextval('avaluos_id_avaluo_seq'::regclass),
  "id_folio" varchar(8) COLLATE "pg_catalog"."default" NOT NULL,
  "id_tramite" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "clavecat" varchar(18) COLLATE "pg_catalog"."default" NOT NULL,
  "valor_anterior" varchar(53) COLLATE "pg_catalog"."default",
  "valor_catastral" varchar(53) COLLATE "pg_catalog"."default",
  "fecha" timestamp(6)
)
;
ALTER TABLE "public"."avaluos" OWNER TO "postgres";

-- ----------------------------
-- Records of avaluos
-- ----------------------------
BEGIN;
INSERT INTO "public"."avaluos" VALUES (2, '2100038', '4.0 - DESMANCOMUNACIONES CON ESCRITURAS EXTEMPORANEAS', '000028364012001', '1169122.99', '1169677.49', '2021-04-26 15:19:25.985072');
INSERT INTO "public"."avaluos" VALUES (3, '2100030', '6.0 - FUSIONES CON ESCRITURAS EXTEMPORANEAS', '060001061017001', '222992.42', '275129.45050000004', '2021-04-26 21:46:57.763614');
INSERT INTO "public"."avaluos" VALUES (4, '2100046', '2.4 - POR CORRECCION DE USOS', '000001010001001', '2850467.4699999997', '2742796.273', '2021-04-26 23:39:51.895282');
INSERT INTO "public"."avaluos" VALUES (5, '2100048', 'C.D.I', '060001001001001', '$744,560.93', '$744,560.93', '2021-05-24 13:03:37.602327');
INSERT INTO "public"."avaluos" VALUES (6, '2100036', '2.3 - POR CORRECCION DE SUPERFICIE CONSTRUIDA', '000025096007001', '$1,411,780.79', '1450151.6600000001', '2021-05-24 13:07:19.334419');
INSERT INTO "public"."avaluos" VALUES (7, '2100013', '18.0 - INCLUIR CONSTRUCCION', '000047196016001', '$129,600.00', '$228,960.00', '2021-05-24 13:13:29.177901');
COMMIT;

-- ----------------------------
-- Primary Key structure for table avaluos
-- ----------------------------
ALTER TABLE "public"."avaluos" ADD CONSTRAINT "avaluos_pkey" PRIMARY KEY ("id_avaluo");
