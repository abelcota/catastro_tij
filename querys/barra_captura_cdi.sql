/*
 Navicat Premium Data Transfer

 Source Server         : catastrocln
 Source Server Type    : PostgreSQL
 Source Server Version : 100015
 Source Host           : localhost:5432
 Source Catalog        : catastrocln
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100015
 File Encoding         : 65001

 Date: 24/05/2021 17:26:03
*/


-- ----------------------------
-- Table structure for barra_captura_cdi
-- ----------------------------
DROP TABLE IF EXISTS "public"."barra_captura_cdi";
CREATE TABLE "public"."barra_captura_cdi" (
  "id_captura" int4 NOT NULL DEFAULT nextval('barra_captura_id_captura_seq'::regclass),
  "id_folio" varchar(10) COLLATE "pg_catalog"."default" NOT NULL,
  "id_anio" varchar(3) COLLATE "pg_catalog"."default" NOT NULL,
  "fecha_envio" timestamp(6),
  "estatus_verificacion" varchar(2) COLLATE "pg_catalog"."default",
  "verificador" varchar(50) COLLATE "pg_catalog"."default",
  "fecha_captura" timestamp(6),
  "capturista" varchar(50) COLLATE "pg_catalog"."default",
  "clave" varchar(15) COLLATE "pg_catalog"."default",
  "obs" varchar(500) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."barra_captura_cdi" OWNER TO "postgres";

-- ----------------------------
-- Records of barra_captura_cdi
-- ----------------------------
BEGIN;
INSERT INTO "public"."barra_captura_cdi" VALUES (15, '00000046', '21', '2021-05-23 15:15:23.145125', 'NP', 'Administrador', NULL, NULL, '000009010012001', 'Falta documentacion para el cambio de inscripcion');
INSERT INTO "public"."barra_captura_cdi" VALUES (13, '00000048', '21', '2021-05-23 14:25:07.262764', 'C', 'Administrador', '2021-05-24 13:03:37.602327', 'Administrador', '060001001001001', NULL);
COMMIT;

-- ----------------------------
-- Primary Key structure for table barra_captura_cdi
-- ----------------------------
ALTER TABLE "public"."barra_captura_cdi" ADD CONSTRAINT "barra_captura_copy1_pkey" PRIMARY KEY ("id_captura");
