<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('memory_limit', '-1');
setlocale(LC_TIME, "spanish");
date_default_timezone_set('America/Mazatlan');
require('../conexion.php');

$fmt = new NumberFormatter( 'es_MX', NumberFormatter::CURRENCY );


//variables que se obtendran por post
//variables que se obtendran por post
if (isset($_POST['a']) && isset($_POST['fol'])) {

  $a = $_POST['a'];
  $fol = $_POST['fol'];


$queryprimer = 'SELECT a.*, c."DescripcionTramite", c."DiasMaximoRespuesta", d."Nombre", i."Declaracion" , f."NOM_POBL"
FROM "Inco_Barra" AS a, "Inco_Tramites" AS c, "Gen_Empleados" AS d, "Inco_Declaracion" As i , a_pobl AS f
WHERE a."IdTramite"=c."IdTramite" 
AND a."IdEmpleadoBarra" = d."IdEmpleado" AND a."IdDeclaracion" = i."IdDeclaracion"
AND a."Poblacion"=f."CVE_POBL"
AND a."IdInconformidad" = \''.$fol.'\' AND a."AnioDeclaracion" = \''.$a.'\'';

// Include the main TCPDF library (search for installation path).
require_once('extensions/tcpdf/tcpdf.php');

$estado= $pdo->prepare($queryprimer);
$estado->execute();
$data = $estado->fetch(\PDO::FETCH_ASSOC);

$tipodec = 'DECLARACIÓN DE PREDIOS URBANOS';

/*obtener los servicios y regimen
$sqlserv = '
-- busca servicios y regimen
SELECT CONCAT(a."CVE_REGIM" ,  \' - \' , reg."DES_REGIM") AS Regimen, a."NUM_FTES" AS Num_Frentes, a."LON_FTE" AS Long_Frente, a."LON_FONDO" AS Long_Fondo, ser.descripcionx AS Servicios FROM a_unid AS a, a_regim AS reg, "a_ServiciosInternet" AS ser WHERE (a."CVE_REGIM"=reg."CVE_REGIM") AND (a."STS_SERVIC"=ser.campox) AND a."CLAVE"= \''.$data['Clave'].'\'';

$estadoserv = $pdo->prepare($sqlserv);
$estadoserv->execute();
$dataserv = $estadoserv->fetch(\PDO::FETCH_ASSOC);*/

//obtener los documentos
$sqldocs = 'SELECT * FROM "Inco_TramitesRequisitos" WHERE "IdTramite"=\''.$data['IdTramite'].'\'';
$estadocs = $pdo->prepare($sqldocs);
$estadocs->execute();
$datadoc = $estadocs->fetchAll(PDO::FETCH_ASSOC);

$documentos = "";
foreach($datadoc as $row){
  $documentos = $documentos." -".$row['DescripcionRequisito']." ";
}

if($data['IdDeclaracion'] == '03'){
  $nota = '<tr>
  <td align="center" width="100%" style="display:flex; items-align:center; font-size:12px;">
    <b><u>**** EL DICTAMEN QUE SE REALICE APARTIR DE ESTA INCONFORMIDAD, INCLUIRA TODAS AQUELLAS MODIFICACIONES OBSERVADAS POR EL INSPECTOR ****</u></b>
  </td>
</tr>';
}else{
  $nota = "";
}




// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    protected $info;
    
    public function setInfo($info){
      $this->info = $info;
    }

    //Page header
    public function Header() {
      
      $this->Image('../assets/images/logo_c.png', 160, 13, 35, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
      // Logo
      $this->Image('../assets/images/Culiacan_Escudo.png', 13, 6, 20, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
      $this->Image('../assets/images/logo_catastro1.jpg', 35, 8, 20, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);


      $this->SetFont('helvetica', 'N', 11);
      // Set font
      $this->Ln(4);
      $this->writeHTML('
      <p style="text-align:center"><strong>GOBIERNO DE CULIACÁN 2018-2021<BR>AYUNTAMIENTO DE CULIACÁN<BR>SECRETARÍA DE ADMINISTRACIÓN Y FINANZAS<br>DIRECCIÓN DE CATASTRO MUNICIPAL</strong><br><u>DECLARACIÓN DE PREDIOS URBANOS</u></p>
      ', true, false, false, false, '');
    }

    // Page footer
    public function Footer() {
      // Position at 30 mm from bottom
       $this->SetY(-28);
       $this->writeHTML($this->info, true, false, false, false, '');
      }
}

// create new PDF document
$pdf = new MYPDF('P', 'mm', 'LETTER', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Direccion de Catastro Culiacan');
$pdf->SetTitle('Solicitud de Inconformidad');
$pdf->SetSubject('Solicitud de Inconformidad');
$pdf->SetKeywords('solicitud, inconformidad, catastro, ayuntamiento, culiacan');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// define barcode style
$style = array(
  'position' => 'absolute',
  'align' => 'R',
  'stretch' => true,
  'fitwidth' => true,
  'cellfitalign' => '',
  'border' => false,
  'hpadding' => 0,
  'vpadding' => 0,
  'fgcolor' => array(0,0,0),
  'bgcolor' => false, //array(255,255,255),
  'text' => true,
  'font' => 'helvetica',
  'fontsize' => 8,
  'stretchtext' => 4
);


// add a page
$pdf->AddPage('P', 'LETTER', TRUE);
$pdf->SetFont('helvetica', 'N', 11);
// Set font
//$pdf->Ln(2);
//pdf->writeHTML('<p style="text-align:center; text-decoration: underline; ">'.$tipodec.'</p>', true, false, false, false, '');
$style['position'] = 'absolute';
$style['stretch'] = true; // disable stretch
$style['fitwidth'] = true; // disable fitwidth
$style['position'] = 'C';

$pdf->write1DBarcode($data['AnioDeclaracion'].$data['IdInconformidad'], 'C128A', 0, 37, '', 8, 0.4, $style, 'N');
$pdf->SetFont('helvetica', 'N', 8);  
// Right alignment
$html2 = '

<table border="0" cellpadding="2" cellspacing="0" nobr="true">
  <tr>
    <td align="left" width="65%" style="display:flex; items-align:center;"><br><br>
    <span style="font-size:11; font-weight:bold;">TELÉFONO DE CATASTRO:     218-32-00 EXT. 1203</span>
    </td>
    <td width="35%" style="font-size:12;">
      <b align="left">Folio:</b> <span align="right">'.$data['IdInconformidad'].'</span><br>
      <b align="left">Fecha:</b> <span align="right">'.strftime("%d/%B/%Y",strtotime($data['FechaCapturaInconformidad'])).'</span>
    </td>
  </tr>
</table>
<hr>
';
$pdf->writeHTML($html2, true, false, false, false, '');

$html3 = '
<table border="0" cellpadding="3" cellspacing="0" nobr="true">
  <tr>
    <td align="left" width="35%" style="display:flex; items-align:center;">
    <b>OBJETO DE LA DECLARACION: </b>
    </td>
    <td align="left" width="65%">
    <b>'.$data['IdDeclaracion'].'</b>  -  '.$data['Declaracion'].'</td>
  </tr>
  <tr>
  <td></td>
</tr>
  <tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>DECLARACION CONTRIBUYENTE: </b>
  </td>
  <td align="left" width="65%">
   '.$data['ObservacionesBarra'].'</td>
</tr>
<tr>
  <td></td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>TIPO DE TRAMITE: </b>
  </td>
  <td align="left" width="65%">
  <b>'.$data['IdTramite'].'  -  '.$data['DescripcionTramite'].'</b></td>
</tr>
<tr>
  <td></td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>CLAVE CATASTRAL: </b>
  </td>
  <td align="left" width="65%">
  '.$data['Clave'].'
  </td>
</tr>
<tr>
  <td></td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>NOMBRE SOLICITANTE : </b>
  </td>
  <td align="left" width="65%">
  '.$data['NombreSolicitante'].'</td>
</tr>
<tr>
  <td></td>
</tr>

<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>DOMICILIO NOTIFICACIÓN : </b>
  </td>
  <td align="left" width="65%">
  '.$data['DomicilioSolicitante'].'</td>
</tr>
<tr>
  <td align="left" width="50%" style="display:flex; items-align:center;">
  </td>
  <td align="left" width="50%">
  </td>
</tr>

<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>TELEFONO SOLICITANTE : </b>
  </td>
  <td align="left" width="65%" style="display:flex; items-align:center;">
  '.$data['TelefonoSolicitante'].'
  </td>
</tr>
<tr>
  <td></td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>POBLACIÓN : </b>'.$data['NOM_POBL'].'
  </td>
  <td align="left" width="65%">
  <b>MUNICIPIO : </b> CULIACÁN
  </td>
</tr>
<tr>
  <td></td>
</tr>

<tr>
  <td align="left" width="100%" style="display:flex; items-align:center;">
  <b>DOCUMENTOS QUE SE ANEXAN : </b>
  </td>
</tr>
<tr>
  <td></td>
</tr>
<tr>
  <td align="left" width="100%" style="display:flex; items-align:center;">
  '.$documentos.'
  </td>
</tr>
<tr><td></td></tr>
</table>
';
$pdf->writeHTML($html3, true, false, false, false, '');


$tablefooter = '
<table border="0" cellpadding="3" cellspacing="0" nobr="false">
<tr>
  <td align="left" style="font-size:8px;">TODA FALSEDAD EN LAS DECLARACIONES, SE CASTIGARA CONFORME A LOS ARTICULOS 41, 44, 47, 52, 54, 55, 56, 57, 58, 60, 61 Y 62 DE LA LEY DE CATASTRO.</td>
  <td></td>
</tr>
<tr>
  <td align="left" style="font-size:12px;"><br><br>ATENDIDO POR: '.$data["Nombre"].'</td>
  <td align="center" style="font-size:12px;"> 
  ______________________________________________<br>
  FIRMA Y NOMBRE DEL SOLICITANTE
  </td>
</tr>
</table>
';

$pdf->setInfo($tablefooter);

//Close and output PDF document
$pdf->Output('PDFInconformidad.pdf', 'I');


//============================================================+
// END OF FILE
//============================================================+
}
else
    echo "No se enviaron parametros";
    
?>