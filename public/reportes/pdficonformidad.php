<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('memory_limit', '-1');
setlocale(LC_TIME, "spanish");
date_default_timezone_set('America/Mazatlan');
require('../conexion.php');

$fmt = new NumberFormatter( 'es_MX', NumberFormatter::CURRENCY );

//variables que se obtendran por post

//variables que se obtendran por post
if (isset($_POST['a']) && isset($_POST['fol']) && $_POST['img']) {

  $a = $_POST['a'];
  $fol = $_POST['fol'];
//$a = isset($_POST['a']) ? $_POST['a'] : '20';
//$fol = isset($_POST['fol']) ? $_POST['fol'] : '00001';


$queryprimer = 'SELECT a.*, c."DescripcionTramite", c."DiasMaximoRespuesta", d."Nombre", i."Declaracion" 
FROM "Inco_Barra" AS a, "Inco_Tramites" AS c, "Gen_Empleados" AS d, "Inco_Declaracion" As i 
WHERE a."IdTramite"=c."IdTramite" 
AND a."IdEmpleadoBarra" = d."IdEmpleado" AND a."IdDeclaracion" = i."IdDeclaracion"
AND a."IdInconformidad" = \''.$fol.'\' AND a."AnioDeclaracion" = \''.$a.'\'';

$querytodos = 'SELECT A.*,A."NombrePropietario" AS wpropietario,C."DescripcionTramite",C."DiasMaximoRespuesta",d."Nombre",i."Declaracion", j.* FROM "Inco_Barra" AS A,"Inco_Tramites" AS C,"Gen_Empleados" AS d,"Inco_Declaracion" AS i,a_unid AS j WHERE A."IdTramite"=C."IdTramite" AND A."IdEmpleadoBarra"=d."IdEmpleado" AND A."IdDeclaracion"=i."IdDeclaracion" AND A."Clave"=j."ClaveCatastral" AND a."IdInconformidad" = \''.$fol.'\' AND a."AnioDeclaracion" = \''.$a.'\'';

// Include the main TCPDF library (search for installation path).
require_once('extensions/tcpdf/tcpdf.php');
$estado= $pdo->prepare($querytodos);
$estado->execute();
$data = $estado->fetch(\PDO::FETCH_ASSOC);
$tipodec = 'DECLARACIÓN DE PREDIOS URBANOS';

//obtener los documentos
$sqldocs = 'SELECT * FROM "Inco_TramitesRequisitos" WHERE "IdTramite"=\''.$data['IdTramite'].'\'';
$estadocs = $pdo->prepare($sqldocs);
$estadocs->execute();
$datadoc = $estadocs->fetchAll(PDO::FETCH_ASSOC);

$documentos = "";
foreach($datadoc as $row){
  $documentos = $documentos." -".$row['DescripcionRequisito']." ";
}

if($data['IdDeclaracion'] == '03'){
  $nota = '<tr>
  <td align="center" width="100%" style="display:flex; items-align:center; font-size:12px;">
    <b><u>**** EL DICTAMEN QUE SE REALICE APARTIR DE ESTA INCONFORMIDAD, INCLUIRA TODAS AQUELLAS MODIFICACIONES OBSERVADAS POR EL INSPECTOR ****</u></b>
  </td>
</tr>';
}else{
  $nota = "";
}

//obtenemos los valores del terreno
$sqlterr = '
SELECT * FROM
	a_terr AS trr 
WHERE
	trr.clave = \''.$data['Clave'].'\' 
ORDER BY
	"NUM_TERR"
';
$estaterr = $pdo->prepare($sqlterr);
$estaterr->execute();
$dataterr = $estaterr->fetchAll(PDO::FETCH_ASSOC);
$terrenos = "";
$sup_total = 0.0;
$valor_total = 0.0;

//empiezo a calcular renglones de construccion
$SuperficieConstruccionFormateado = 0;
$ValorConstruccionFormateado = 0;
$ValorCatastralFormateado = 0;

$sqlconst = 'SELECT cons."CVE_CAT_CO", cons."SUP_CONST", 
                0.00 as ValorUnitario, cons."CVE_EDO_CO", 
                cons."EDD_CONST", 0.00 as FactorDemerito, 
                1.00 as FactorComercializacion, 0.00 as ValorNeto 
                FROM a_const AS cons 
                WHERE cons.clave=\''.$data['Clave'].'\' ORDER BY "NUM_CONST"';

$estaconst = $pdo->prepare($sqlconst);
$estaconst->execute();
$dataconst = $estaconst->fetchAll(PDO::FETCH_ASSOC);

$sqlterr = 'SELECT * FROM a_terr WHERE clave=\''.$data['Clave'].'\'';

$estaterr = $pdo->prepare($sqlterr);
$estaterr->execute();
$dataterr = $estaterr->fetchAll(PDO::FETCH_ASSOC);

$terrenos .= '<tr style="border-style: dotted;">
    <td align="center" style="font-size:8px; border-left-style: solid;"></td>
    <td align="center" style="font-size:8px; "></td>
    <td align="center" style="font-size:8px;  ">'.$dataterr[0]["SUP_TERR"].'</td>
    <td align="center" style="font-size:8px;  ">'.$fmt->formatCurrency($data["ValorUnitario_Terreno"],"MXN").'</td>
    <td align="center" style="font-size:8px;  "></td>
    <td align="center" style="font-size:8px;  "></td>
    <td align="center" style="font-size:8px;  "> </td>
    <td align="center" style="font-size:8px;  "> </td>
    <td align="center" style="font-size:8px;  "> </td>
    <td align="center" style="font-size:8px;  "> </td>
    <td align="center" style="font-size:8px;  ">'.$dataterr[0]["SUP_TERR_E"].' </td>
    <td align="center" style="font-size:8px;  "></td>
    <td align="center" style="font-size:8px;  "></td>
    <td align="center" style="font-size:8px; border-right-style: solid;">'.$fmt->formatCurrency($data["ValorUnitario_Terreno"],"MXN").' </td>
    </tr>';

$construcciones = "";
$Categoria = "";
$Edad = 0;
$estado = 0;
foreach ($dataconst as $const){
  //'busquedas de categorias
  $Categoria = $const["CVE_CAT_CO"];
  $Superficie = $const["SUP_CONST"];
  $Edad = $const["EDD_CONST"];
  $estado = $const["CVE_EDO_CO"];
  $valorunitario = $const["valorunitario"];
  $factdem = $const["factordemerito"];
  $factcom = $const["factorcomercializacion"];
  $valorneto = $const["valorneto"];
  //buscamos el valor del factor de demerito de la construccion
  $sqldemc = 'SELECT * FROM a_demc WHERE "CVE_CAT_CO" =\''.$Categoria.'\' AND "CVE_EDO_CO"= '.$estado.' AND "EDD_CONST"= '.$Edad;
  $estadodemc = $pdo->prepare($sqldemc);
  $estadodemc->execute();
  $datademc = $estadodemc->fetch(PDO::FETCH_ASSOC);
  if ($datademc != null)
  {
    $factdem = $datademc["FAC_DEM_CO"];
  }
  //Buscamos el valor unitario de la construccion
  $sqlcatco = 'SELECT * FROM a_cat_mp WHERE "CVE_CAT_CO"=\''.$Categoria.'\'';
  $estadocatco = $pdo->prepare($sqlcatco);
  $estadocatco->execute();
  $datacatco = $estadocatco->fetch(PDO::FETCH_ASSOC);
  if ($datacatco != null)
  {
    $valorunitario = $datacatco["val_unit_c"];
  }
  //buscamos el valor del factor de comercializacion
  $sqlfaccom = 'SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL" ='.$data['Poblacion'];
  $estadofaccom = $pdo->prepare($sqlfaccom);
  $estadofaccom->execute();
  $datafaccom = $estadofaccom->fetch(PDO::FETCH_ASSOC);
  if ($datafaccom != null)
  {
    $factcom = $datafaccom["FAC_COM"];
  } 

  //calcular el valorneto por cobnstruccion
  $valorneto = $Superficie * $valorunitario * $factdem * $factcom;
  $construcciones .= '<tr style="border-style: dotted;">
  <td align="center" style="font-size:8px; border-left-style: solid;">'.$Categoria.' </td>
  <td align="center" style="font-size:8px; ">'.$Superficie.' </td>
  <td align="center" style="font-size:8px;  ">'.$fmt->formatCurrency($valorunitario,"MXN").' </td>
  <td align="center" style="font-size:8px;  ">'.$estado.' </td>
  <td align="center" style="font-size:8px;  ">'.$Edad.' </td>
  <td align="center" style="font-size:8px;  ">'.$factdem.' </td>
  <td align="center" style="font-size:8px;  ">'.$factcom.' </td>
  <td align="center" style="font-size:8px; border-right-style: solid;">'.$fmt->formatCurrency($valorneto,"MXN").' </td>
  </tr>';

  $SuperficieConstruccionFormateado += $Superficie;
  $ValorConstruccionFormateado += $valorneto;
}

//valor catastral del predio total de construccion mas total del terreno
$ValorCatastralFormateado = $ValorConstruccionFormateado + $valor_total;


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    protected $info;
    
    public function setInfo($info){
      $this->info = $info;
    }

    //Page header
    public function Header() {
      
         // Logo
         $this->Image('../assets/images/aytoColor.png', 13, 15, 50, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
        // Logo
    
        $this->SetFont('helvetica', 'N', 11);
        // Set font
        $this->Ln(4);
        $this->writeHTML('
        <p style="text-align:center"><strong>GOBIERNO DE TIJUANA 2021-2024<BR>AYUNTAMIENTO DE TIJUANA<BR>SECRETARÍA DE ADMINISTRACIÓN Y FINANZAS<br>DIRECCIÓN DE CATASTRO MUNICIPAL</strong><br><u>DECLARACIÓN DE PREDIOS URBANOS</u></p>
        ', true, false, false, false, '');
    }

    // Page footer
    public function Footer() {
      // Position at 30 mm from bottom
       $this->SetY(-28);
       $this->writeHTML($this->info, true, false, false, false, '');
      }
}

// create new PDF document
$pdf = new MYPDF('P', 'mm', 'LETTER', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Direccion de Catastro Tijuana');
$pdf->SetTitle('Solicitud de Inconformidad '.$a.$fol);
$pdf->SetSubject('Solicitud de Inconformidad');
$pdf->SetKeywords('solicitud, inconformidad, catastro, ayuntamiento, tijuana');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// define barcode style
$style = array(
  'position' => 'absolute',
  'align' => 'R',
  'cellfitalign' => '',
  'border' => false,
  'hpadding' => 0,
  'vpadding' => 0,
  'fgcolor' => array(0,0,0),
  'bgcolor' => false, //array(255,255,255),
  'text' => true,
  'font' => 'helvetica',
  'fontsize' => 6,
  'stretchtext' => 4
);


// add a page
$pdf->AddPage('P', 'LETTER', TRUE);
$pdf->SetFont('helvetica', 'N', 11);
// Set font
//$pdf->Ln(2);
//pdf->writeHTML('<p style="text-align:center; text-decoration: underline; ">'.$tipodec.'</p>', true, false, false, false, '');
$style['position'] = 'absolute';
$style['stretch'] = true; // disable stretch
$style['fitwidth'] = true; // disable fitwidth
$style['position'] = 'R';

$pdf->write1DBarcode($data['AnioDeclaracion'].$data['IdInconformidad'], 'C128A', 0, 37, '', 8, 0.4, $style, 'N');
$pdf->SetFont('helvetica', 'N', 8);  
// Right alignment
$html2 = '

<table border="0" cellpadding="2" cellspacing="0" nobr="true">
  <tr>
    <td align="left" width="65%" style="display:flex; items-align:center;"><br><br>
    <span style="font-size:11; font-weight:bold;">TELÉFONO DE CATASTRO:     218-32-00 EXT. 1203</span>
    </td>
    <td width="35%" style="font-size:12;">
      <b align="left">Folio:</b> <span stye="text-align:right">'.$data['IdInconformidad'].'</span><br>
      <b align="left">Fecha:</b> <span align="right">'.strftime("%d/%B/%Y",strtotime($data['FechaCapturaInconformidad'])).'</span>
    </td>
  </tr>
</table>
<hr>
';
$pdf->writeHTML($html2, true, false, false, false, '');

$html3 = '
<table border="0" cellpadding="3" cellspacing="0" nobr="true">
  <tr>
    <td align="left" width="35%" style="display:flex; items-align:center;">
    <b>OBJETO DE LA DECLARACION: </b>
    </td>
    <td align="left" width="65%">
    <b>'.$data['IdDeclaracion'].'</b>  -  '.$data['Declaracion'].'</td>
  </tr>
  <tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>CONTRIBUYENTE: </b>
  </td>
  <td align="left" width="65%">
   '.$data['ObservacionesBarra'].'</td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>TIPO DE TRAMITE: </b>
  </td>
  <td align="left" width="65%">
  <b>'.$data['IdTramite'].'  -  '.$data['DescripcionTramite'].'</b></td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>CLAVE : </b>'.$data['Clave'].'
  </td>
  <td align="left" width="65%">
  <b>USO : </b>'.$data['Uso_Predial'].'</td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>NOMBRE PROPIETARIO : </b>
  </td>
  <td align="left" width="65%">
  '.$data['NombrePropietario'].'</td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>DOMICILIO NOTIFICACIÓN : </b>
  </td>
  <td align="left" width="65%">
  '.$data['DomicilioSolicitante'].'</td>
</tr>
<tr>
  <td align="left" width="50%" style="display:flex; items-align:center;">
  <b>UBICACIÓN PREDIO : </b>'.$data['Calle'].' '.$data['NumeroOficial'].' '.$data['Colonia'].' '.$data['Delegacion'].'
  </td>
  <td align="left" width="50%">
   <B>COLONIA:</B> '.$data['Colonia'].'
  </td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>POBLACIÓN : </b>'.$data['Poblacion'].'
  </td>
  <td align="left" width="65%">
  <b>MUNICIPIO : </b> CULIACÁN
  </td>
</tr>
<tr>
  <td align="left" width="100%" style="display:flex; items-align:center;">
  <b>SERVICIOS : </b> ---
  </td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>TELEFONO PROPIETARIO : </b>
  </td>
  <td align="left" width="65%" style="display:flex; items-align:center;">
  '.$data['TelefonoSolicitante'].'
  </td>
</tr>
<tr>
  <td align="left" width="100%" style="display:flex; items-align:center;">
  <b>DOCUMENTOS QUE SE ANEXAN : </b> '.$documentos.'
  </td>
</tr>
<tr><td></td></tr>
'.$nota.'
<tr>
  <td align="left" width="60%" style="display:flex; items-align:center;"><span style="font-size:8px;">NOTA: SE DEBERÁ ADJUNTAR PLANOS DEL PREDIO (TERRENO Y CONSTRUCCIÓN)</span><br><b style="font-size:12px;">'. $data["Zona"].'</b>
  </td>
  <td align="right" width="40%" style="display:flex; items-align:center; font-size:12px;"><br><br>
  <b >VALOR DE ZONA ($):</b>    '. $fmt->formatCurrency($data["ValorUnitario_Terreno"], "MXN").'
  </td>
</tr>
</table>
';
$pdf->writeHTML($html3, true, false, false, false, '');

$tblterreno = '
<table border="0" cellpadding="3" cellspacing="0" nobr="false">
<tr>
  <td colspan="14" align="center" style="font-size:12px; font-weight:bold; border-style: solid; border-left-style: solid; border-right-style: solid;">DATOS DEL TERRENO</td>
</tr>
<tr>
  <td colspan="4" align="center" style="font-size:8px;  border-style: solid; border-style: solid; border-left-style: solid;">G E N E R A L</td>
  <td colspan="6" align="center" style="font-size:8px;  border-style: solid;">INCREMENTO POR ESQUINA (25%)</td>
  <td colspan="4" align="center" style="font-size:8px;  border-style: solid; border-style: solid; border-right-style: solid;">D E M E R I T O</td>
</tr>
<tr style="border-style: dotted;">
  <td align="center" style="font-size:6px; border-style: solid; border-left-style: solid;  border-bottom-style: solid;">CALLE</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">TRAMO</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">SUPERFICIE M2</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">VALOR UNITARIO</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">DEM</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">CALLE</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">TRAMO</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">SUPERFICIE M2</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">VALOR UNITARIO</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">DEM</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">SUPERFICIE M2</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">CODIGOS</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">% DEM</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid; border-right-style: solid;">VALOR NETO ($)</td>
</tr>
'.$terrenos.'
<tr>
  <td colspan="14" style="font-size:8px;  border-left-style: solid; border-right-style: solid; border-bottom-style: solid;"></td>
</tr>
<tr>
  <td colspan="2" style="font-size:10px;">TOTAL :</td>
  <td style="font-size:10px;">'.$sup_total.'</td>
  <td colspan="4" style="font-size:10px;"> </td>
  <td colspan="5" style="font-size:10px; text-align:right;" >VALOR CATASTRAL DEL TERRENO :</td>
  <td colspan="2" style="font-size:10px; text-align:right;" >'.$fmt->formatCurrency($data["ValorUnitario_Terreno"],"MXN").'</td>
</tr>

</table>
';
$pdf->writeHTML($tblterreno, true, false, false, false, '');

$tblconst = '
<table border="0" cellpadding="3" cellspacing="0" nobr="false">
<tr>
  <td colspan="8" align="center" style="font-size:12px; font-weight:bold; border-style: solid; border-left-style: solid; border-right-style: solid;">DATOS DE LA CONSTRUCCIÓN</td>
</tr>
<tr style="border-style: dotted;">
  <td align="center" style="font-size:6px; border-style: solid; border-left-style: solid;  border-bottom-style: solid;">CATEGORÍA</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">SUPERFICIE M2</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">VALOR UNITARIO</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">ESTADO</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">EDAD</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">FACT. DEM.</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">FACT. COM.</td>
  <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid; border-right-style: solid;">VALOR NETO ($)</td>
</tr>
'.$construcciones.'
<tr>
  <td colspan="8" style="font-size:8px;  border-left-style: solid; border-right-style: solid; border-bottom-style: solid;"></td>
</tr>
<tr>
  <td colspan="1" style="font-size:10px;">TOTAL :</td>
  <td style="font-size:10px;" align="center">'.$SuperficieConstruccionFormateado.'</td>
  <td colspan="1" style="font-size:10px;"> </td>
  <td colspan="4" style="font-size:10px; text-align:right;" >VALOR CATASTRAL DE LA CONSTRUCCIÓN :</td>
  <td colspan="2" style="font-size:10px; text-align:right;" >'.$fmt->formatCurrency($ValorConstruccionFormateado,"MXN").'</td>
</tr>
<tr>
  <td colspan="7" style="font-size:10px; text-align:right;" >VALOR CATASTRAL DEL PREDIO :</td>
  <td colspan="1" style="font-size:10px; text-align:right;" >'.$fmt->formatCurrency($data["ValorFiscal"],"MXN").'</td>
</tr>

</table>
';
$pdf->writeHTML($tblconst, true, false, false, false, '');

$tablefooter = '
<table border="0" cellpadding="3" cellspacing="0" nobr="false">
<tr>
  <td align="left" style="font-size:8px;">TODA FALSEDAD EN LAS DECLARACIONES, SE CASTIGARA CONFORME A LOS ARTICULOS 41, 44, 47, 52, 54, 55, 56, 57, 58, 60, 61 Y 62 DE LA LEY DE CATASTRO.</td>
  <td></td>
</tr>
<tr>
  <td align="left" style="font-size:12px;"><br><br>ATENDIDO POR: '.$data["Nombre"].'</td>
  <td align="center" style="font-size:12px;"> 
  ______________________________________________<br>
  FIRMA Y NOMBRE DEL SOLICITANTE
  </td>
</tr>
</table>
';

$pdf->setInfo($tablefooter);

//$pdf->writeHTML($tablefooter, true, false, false, false, '');

$pdf->AddPage('L', 'LETTER', TRUE);
$pdf->setPrintFooter(true);


$img_base64_encoded = $_POST['img'];
$img = base64_decode(preg_replace('#^data:image/[^;]+;base64,#', '', $img_base64_encoded));
$pdf->writeHTML('<br><br><p style="text-align:center; font-size:12px;">CROQUIS DEL PREDIO</p><br><p><b>CLAVE CATASTRAL:</b> '.$data["Clave"].' || <b>PROPIETARIO:</b> '.$data['NombrePropietario'].'</p><p><b>UBICACIÓN DEL PREDIO:</b> '.$data["Calle"].' '.$data["NumeroOficial"].' || <b>COLONIA:</b> '.$data['Colonia'].' || <b>DELEGACIÓN:</b> '.$data['Delegacion'].'</p>', true, false, false, false, '');
$pdf->Ln(2);
$pdf->Image("@".$img);

//Close and output PDF document
$pdf->Output('PDFInconformidad_'.$a.$fol.'.pdf', 'I');

}
else
    echo "No se enviaron parametros";

//============================================================+
// END OF FILE
//============================================================+

?>