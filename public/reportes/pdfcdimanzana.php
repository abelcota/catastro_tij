<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('memory_limit', '-1');
setlocale(LC_TIME, "spanish");
date_default_timezone_set('America/Mazatlan');
require('../conexion.php');

$fmt = new NumberFormatter( 'es_MX', NumberFormatter::CURRENCY );
//variables que se obtendran por post
$a = isset($_GET['a']) ? $_GET['a'] : '15';
$fol = isset($_GET['fol']) ? $_GET['fol'] : '00002';

// Include the main TCPDF library (search for installation path).
require_once('extensions/tcpdf/tcpdf.php');


$query = 'SELECT * FROM "Inco_AvisoCambioManzana" ia 
inner join "Gen_Empleados" AS e on ia."IdEmpleadoBarra" = e."IdEmpleado"
WHERE "Folio" = \''.$fol.'\' AND "AnioAviso" = \''.$a.'\' ORDER BY "Clave"';
$estado= $pdo->prepare($query);
$estado->execute();
$data = $estado->fetch(\PDO::FETCH_ASSOC);

$movimiento = "";

if($data["TipoB"] == 'X'){
  $movimiento = "BAJA";
}
else if($data["TipoC"] == 'X'){
  $movimiento = "CAMBIO";
}
$clave = "";
if($a != '00')
  $clave = "007".$data["Poblacion"]."-".$data["Cuartel"]."-".$data["Manzana"];

$expedicion = "";
if($a != '00')
{
    $expedicion = $data["Poblacion"]."-".$data["Cuartel"]."-".$data["Manzana"];
}  
// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    protected $info;
    
    public function setInfo($info){
      $this->info = $info;
    }

    //Page header
    public function Header() {
      
      $this->Image('../assets/images/logo_c.png', 160, 13, 35, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
      // Logo
      $this->Image('../assets/images/Culiacan_Escudo.png', 13, 6, 20, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
      $this->Image('../assets/images/logo_catastro1.jpg', 35, 8, 20, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);


        $this->SetFont('helvetica', 'N', 11);
        // Set font
        $this->Ln(4);
        $this->writeHTML('
        <p style="text-align:center"><strong>GOBIERNO DE CULIACÁN 2018-2021<BR>AYUNTAMIENTO DE CULIACÁN<BR>SECRETARÍA DE ADMINISTRACIÓN Y FINANZAS<br>DIRECCIÓN DE CATASTRO MUNICIPAL</strong><br><u>PROYECTO DE CAMBIO AL PADRÓN</u></p>
        ', true, false, false, false, '');
    }

    // Page footer
    public function Footer() {
      // Position at 30 mm from bottom
       $this->SetY(-28);
       $this->writeHTML($this->info, true, false, false, false, '');
      }
}

// create new PDF document
$pdf = new MYPDF('P', 'mm', 'LETTER', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Direccion de Catastro Culiacan');
$pdf->SetTitle('Proyecto de Cambio al Padron');
$pdf->SetSubject('Proyecto de Cambio al Padron');
$pdf->SetKeywords('cambio, padron, proyecto, catastro, ayuntamiento, culiacan');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// define barcode style
$style = array(
  'position' => 'absolute',
  'align' => 'R',
  'stretch' => true,
  'fitwidth' => true,
  'cellfitalign' => '',
  'border' => false,
  'hpadding' => 0,
  'vpadding' => 0,
  'fgcolor' => array(0,0,0),
  'bgcolor' => false, //array(255,255,255),
  'text' => true,
  'font' => 'helvetica',
  'fontsize' => 8,
  'stretchtext' => 4
);


// add a page
$pdf->AddPage('P', 'LETTER', TRUE);
$pdf->SetFont('helvetica', 'N', 11);
// Set font
//$pdf->Ln(2);
//pdf->writeHTML('<p style="text-align:center; text-decoration: underline; ">'.$tipodec.'</p>', true, false, false, false, '');
$style['position'] = 'absolute';
$style['stretch'] = true; // disable stretch
$style['fitwidth'] = true; // disable fitwidth
$style['position'] = 'R';

$pdf->write1DBarcode($data['AnioDeclaracion'].$data['IdInconformidad'], 'C128A', 0, 37, '', 8, 0.4, $style, 'N');
$pdf->SetFont('helvetica', 'N', 10);  
// Right alignment
$html2 = '

<table border="0" cellpadding="2" cellspacing="0" nobr="true">
  <tr>
    <td align="left" width="65%" style="display:flex; items-align:center;"><br><br>
    <span style="font-size:11; font-weight:bold;">TELÉFONO DE CATASTRO:     218-32-00 EXT. 1203</span>
    </td>
    <td width="35%" style="font-size:12;">
      <b align="left">Folio:</b> <span align="right">'.$data["Folio"].'</span><br>
      <b align="left">Fecha:</b> <span align="right">'.strftime("%d/%B/%Y",strtotime($data['FechaCaptura'])).'</span><br>
    </td>
  </tr>
</table>
<hr>
';
$pdf->writeHTML($html2, true, false, false, false, '');

$html3 = '
<table border="0" cellpadding="7" cellspacing="0" nobr="true">
  <tr>
    <td align="right" width="100%"> 
    <b align="left" style="font-size:11; font-weight:bold;">Tipo de Movimiento:</b> <span align="right">'.$movimiento.'</span>
    </td>
  </tr>
  <tr>
    <td align="left" width="35%" style="display:flex; items-align:center;">
    <b>CLAVE: </b></td>
    <td align="left" width="65%">'.$clave.'</td>
  </tr>
  <tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>NOMBRE ANTERIOR: </b>
  </td>
  <td align="left" width="65%">'.$data['PropietarioAnterior'].' </td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>NOMBRE ACTUAL: </b>
  </td>
  <td align="left" width="65%">'.$data['PropietarioActual'].'</td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>UBICACIÓN DEL PREDIO : </b> </td>
  <td align="left" width="65%">'.$data['DomicilioUbicacion'].'  </td>
</tr>

<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>DOMICILIO NOTIFICACIÓN : </b>
  </td>
  <td align="left" width="65%">'.$data['DomicilioNotificacion'].'</td>
</tr>
<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>CONCEPTO : </b>  </td>
  <td align="left" width="65%">'.$data['Concepto'].'</td>
</tr>

<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;">
  <b>OBSERVACIONES : </b>
  </td>
  <td align="left" width="65%" style="display:flex; items-align:center;">'.$data['Observaciones'].'</td>
</tr>
</table>
';
$pdf->writeHTML($html3, true, false, false, false, '');


$tablefooter = '
<table border="0" cellpadding="3" cellspacing="0" nobr="false">
<tr>
  <td align="left" style="font-size:12px;"><br><br>ATENDIDO POR: '.$data["Nombre"].'</td>
  <td align="center" style="font-size:12px;"> 
  ______________________________________________<br>
  NOMBRE Y FIRMA DEL CONTRIBUYENTE
  </td>
</tr>
</table>
';

$pdf->setInfo($tablefooter);

//Close and output PDF document
$pdf->Output('PDFAvisoCDIManzana.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

?>