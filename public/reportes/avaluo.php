<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
setlocale(LC_TIME, "spanish");
date_default_timezone_set('America/Mazatlan');

//para firmar (revision)
require_once('../src/Internal/BaseConverter.php');
require_once('../src/Internal/BaseConverterSequence.php');
require_once('../src/Internal/DataArrayTrait.php');
require_once('../src/Internal/Key.php');
require_once('../src/Internal/LocalFileOpenTrait.php');
require_once('../src/Internal/Rfc4514.php');

require_once('../src/Certificate.php');
require_once('../src/Credential.php');
require_once('../src/PemExtractor.php');
require_once('../src/Certificate.php');
require_once('../src/PrivateKey.php');
require_once('../src/PublicKey.php');
require_once('../src/SerialNumber.php');

require('../conexion.php');


$fmt = new NumberFormatter( 'es_MX', NumberFormatter::CURRENCY );

$query_firma = 'SELECT * FROM configuracion';

$estadof= $pdo->prepare($query_firma);
$estadof->execute();
$data_firma = $estadof->fetch(\PDO::FETCH_ASSOC);

$cerFile = '../assets/firma/'.$data_firma['cerfile'];
$pemKeyFile = '../assets/firma/'.$data_firma['pemkeyfile'];
$passPhrase = base64_decode($data_firma['passphrase']); // contraseña para abrir la llave privada

$fiel = PhpCfdi\Credentials\Credential::openFiles($cerFile, $pemKeyFile, $passPhrase);


//variables que se obtendran por post
if ( (isset($_POST['ccat']) && isset($_POST['folio']) && isset($_POST['img'])) || (isset($_GET['folio']) || isset($_GET['ccat']))) {
    if( isset($_GET['folio']) ){
        $folio = $_GET['folio'];
    }
    else{
        $folio = $_POST['folio'];
    }

    if( isset($_GET['ccat']) ){
        $ccat = $_GET['ccat'];
    }
    else{
        $ccat = $_POST['ccat'];
    }

    $a = substr($folio, 0,2);
    $fol = substr($folio, -5);
    //se hace la consulta a la base de datos para obtener la informacion del h_unidad
    $query_clave = "SELECT * FROM h_unidad WHERE folio = '$folio' and \"CLAVE\"= '$ccat'; ";

    $estado= $pdo->prepare($query_clave);
    $estado->execute();
    $data_cvecat = $estado->fetch(\PDO::FETCH_ASSOC);

    $tramite = "";
    $fecha_tramite = "";
    $descripcion_tramite = "";

    //obtener informacion del folio
        $sql_fol = "SELECT * from \"Inco_Barra\" ib 
        inner join \"Inco_Tramites\" t on ib.\"IdTramite\" = t.\"IdTramite\"
        where \"IdInconformidad\" =  '$fol' AND \"AnioDeclaracion\" = '$a' AND \"Clave\" = '$ccat' ;";
        $estado_fol = $pdo->prepare($sql_fol);
        $estado_fol->execute();
        $data_folio = $estado_fol->fetch(\PDO::FETCH_ASSOC);

        if (!$data_folio) { // again, no rowCount() is needed!
            $sql_fol = 'SELECT ia.*, e.*, (SELECT CONCAT("NOM_PROP", \' \', "APE_PAT", \' \', "APE_MAT") FROM a_prop where "CVE_PROP"::text = ia."PropietarioAnterior") as prop_ant, (SELECT CONCAT("NOM_PROP", \' \', "APE_PAT", \' \', "APE_MAT") FROM a_prop where "CVE_PROP"::text = ia."PropietarioActual") as prop_act, n.* FROM "Inco_AvisoCambio" ia 
            inner join "Gen_Empleados" AS e on ia."IdEmpleadoBarra" = e."IdEmpleado"
            left join notarios n on n.cve_notario = ia."TramiteNotario"
            WHERE "Folio" = \''.$fol.'\' AND "AnioAviso"=\''.$a.'\';';
            $estado_fol = $pdo->prepare($sql_fol);
            $estado_fol->execute();
            $data_folio = $estado_fol->fetch(\PDO::FETCH_ASSOC);

            $tramite = 'Cambio de Inscripción';
            $fecha_tramite = $data_folio['FechaCaptura'];
            $descripcion_tramite = $data_folio['Observaciones'];
        }
        else{
            $tramite = $data_folio['DescripcionTramite'];
            $fecha_tramite = $data_folio['FechaCapturaInconformidad'];
            $descripcion_tramite = $data_folio['ObservacionesBarra'];
        }

        


    //OBTENER EL NOMBRE DE LA POBLACION

    $sql_pobl = 'SELECT * FROM a_pobl WHERE "CVE_POBL" = '.$data_cvecat['CVE_POBL'];
    $estadopobl = $pdo->prepare($sql_pobl);
    $estadopobl->execute();
    $datapobl = $estadopobl->fetch(PDO::FETCH_ASSOC);


    //OBNTENEMS LOS DATOS DE LA ZONA
    $sqlzona = "SELECT * FROM h_zona WHERE folio = '$folio' AND clave = '$ccat';";
    $estadozona = $pdo->prepare($sqlzona);
    $estadozona->execute();
    $datazona = $estadozona->fetch(PDO::FETCH_ASSOC);

    //obtener los servicios y regimen
    $sqlserv = '
    -- busca servicios y regimen
    SELECT CONCAT(a."CVE_REGIM" ,  \' - \' , reg."DES_REGIM") AS Regimen, a."NUM_FTES" AS Num_Frentes, a."LON_FTE" AS Long_Frente, a."LON_FONDO" AS Long_Fondo, ser.descripcionx AS Servicios FROM h_unidad AS a, a_regim AS reg, "a_ServiciosInternet" AS ser WHERE (a."CVE_REGIM"=reg."CVE_REGIM") AND (a."STS_SERVIC"=ser.campox) AND a."CLAVE"= \''. $data_cvecat['CLAVE'].'\'';
    //echo $sqlserv;
    $estadoserv = $pdo->prepare($sqlserv);
    $estadoserv->execute();
    $dataserv = $estadoserv->fetch(\PDO::FETCH_ASSOC);


    // Include the main TCPDF library (search for installation path).
    require_once('extensions/tcpdf/tcpdf.php');

    // Extend the TCPDF class to create custom Header and Footer
    class MYPDF extends TCPDF {

        protected $info;
        protected $folio;
        protected $cve;
        protected $pobl;

        public function set_data($folio = "", $cve = "", $datapobl) {
            $this->folio = $folio;
            $this->cve = $cve;
            $this->pobl = $datapobl;
        }

        public function setInfo($info){
            $this->info = $info;

        }

        //Page header
        public function Header() {
        
            /*$this->Image('../assets/images/logo_v.png', 180, 5, 25, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
            */
            // Logo
            $this->Image('../assets/images/Culiacan_Escudo.png', 13, 6, 20, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
            $this->Image('../assets/images/logo_catastro1.png', 35, 8, 20, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);

            $this->SetFont('helvetica', 'N', 10);
            // Set font
            $this->Ln(1);

            $tabla_header = '<table border="0" cellpadding="2" cellspacing="0" nobr="false">
            <tr>
                <td align="left" width="20%"></td>
                <td align="center" width="50%" style="display:flex; items-align:justify;"><strong>GOBIERNO DE CULIACÁN 2018-2021<BR>AYUNTAMIENTO DE CULIACÁN<BR>SECRETARÍA DE ADMINISTRACIÓN Y FINANZAS<br>DIRECCIÓN DE CATASTRO MUNICIPAL</strong><br></td>
                <td align="left" width="10%">Folio: <br>Fecha de Emisión: <br>Municipio: <br>Poblacion: </td>
                <td align="right" width="20%" style="font-weight:bold; font-size:9"><strong style="font-size:16">'.$this->folio.'</strong> <strong style="font-size:12"><br>'.date("m/d/Y", time()).'</strong><br><strong style="font-size:10">007 - CULIACÁN</strong> <br><strong style="font-size:10">'.str_pad($this->pobl['CVE_POBL'], 2, "0").' - '.$this->pobl['NOM_POBL'].'</strong></td>
            </tr>
            <tr>
                <td width="100%" aligh="center" style="font-size: 16; font-weight:bold; text-align:center;">CÉDULA CATASTRAL</td>
            </tr>
            </table>';
            $this->writeHTML($tabla_header , true, false, false, false, '');
            /*$this->writeHTML('
            <p style="text-align:center"><strong>GOBIERNO DEL ESTADO DE SINALOA<BR>H. AYUNTAMIENTO DE TIJUANA<BR>DIRECCIÓN DE CATASTRO MUNICIPAL</strong><br></p>
            ', true, false, false, false, '');*/
            /*$this->SetFont('helvetica', 'N', 16);
            $this->writeHTML('
            <p style="text-align:center"><b>CÉDULA CATASTRAL</b></p>
            ', true, false, false, false, '');
           */
        }

        // Page footer
        public function Footer() {
            // Position at 30 mm from bottom
            $this->SetY(-45);
            $this->writeHTML($this->info, true, false, false, false, '');
        }
    }

    // create new PDF document
    $pdf = new MYPDF('P', 'mm', PDF_PAGE_FORMAT , true, 'UTF-8', false);

    $pdf->set_data($folio, $data_cvecat['CLAVE'], $datapobl);

    $url_qr = 'http://'.$_SERVER['SERVER_NAME'].'/reportes/avaluo.php?folio='.$folio.'&ccat='.$ccat;

    $signature = $fiel->sign($url_qr);//$folio."-".$data_cvecat['CLAVE']);


    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Direccion de Catastro Culiacan');
    $pdf->SetTitle('Cedula Catastral '.$folio);
    $pdf->SetSubject('Cedula Catastral');
    $pdf->SetKeywords('cedula, catastral, catastro, ayuntamiento, culiacan');

    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,);

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // add a page
    $pdf->AddPage('P', [216, 279], TRUE);
    $pdf->SetFont('helvetica', 'N', 10);
    $pdf->Ln(14);

    $clave_split = str_split($data_cvecat['CLAVE'], 3);
    $propietario_nombre = $data_cvecat['NOM_PROP']." ".$data_cvecat['APE_PAT']." ".$data_cvecat['APE_MAT'];
    $tip_pers = "";
    if($data_cvecat['TIP_PERS'] == 'F')
        $tip_pers = 'Persona física';
    else
        $tip_pers = 'Persona moral';
    
    $cod_pos = "";
    if($data_cvecat['COD_POS'] == null)
        $cod_pos = "S/N";
    else
        $cod_pos = $data_cvecat['COD_POS'];

    $servicios_explode = explode(",", $dataserv['servicios']);

   if(count($servicios_explode) < 9)
   {
       $faltantes = 9 - count($servicios_explode);
       for($i = 0; $i < $faltantes; $i++ ){
            array_push($servicios_explode, "");
       }
       
   }

    if(isset($_POST['img'])){
        $img_base64_encoded = $_POST['img'];
        $img = base64_decode(preg_replace('#^data:image/[^;]+;base64,#', '', $img_base64_encoded));
        $pdf->Image("@".$img, 150, 132, 50);
    }
    else{
        $pdf->Image('http://'.$_SERVER['SERVER_NAME'].'/assets/images/blank_map.png', 150, 132, 50);
        //$pdf->Image("@".$img, 145, 133, 60)
    }
    

    $html1 = '<table border="0" cellpadding="2" cellspacing="0" nobr="true">
    <tr>
        <td align="left" width="40%" style="display:flex; items-align:center;"><span style="font-size:8;">Clave catastral:</span></td>
        <td align="left" width="60%" style="display:flex; items-align:center;"><span style="font-size:8;">Titular: <strong>'.$propietario_nombre.'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="40%" style="display:flex; items-align:left;"><span style="font-size:14; font-weight:bold;">007&nbsp;&nbsp;&nbsp;'.$clave_split[0].'&nbsp;&nbsp;'.$clave_split[1].'&nbsp;&nbsp;'.$clave_split[2].'&nbsp;&nbsp;'.$clave_split[3].'&nbsp;&nbsp;'.$clave_split[4].'</span></td>
        <td align="left" width="60%" style="display:flex; items-align:left;"><span style="font-size:8;">Tipo de Persona: <strong>'.$tip_pers.'</strong></span>  <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Uso: <strong>'.$data_cvecat['DES_USO'].'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="40%" style="display:flex; items-align:left;"><span style="font-size:6;">Municipio &nbsp;&nbsp;&nbsp; Población &nbsp;&nbsp; Cuartel &nbsp;&nbsp;&nbsp;&nbsp;  Manzana  &nbsp;&nbsp;&nbsp;&nbsp; Predio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Unidad</span></td>
        <td align="left" width="60%" style="display:flex; items-align:left;"><span style="font-size:8;">Régimen: <strong>'.$data_cvecat['DES_REGIM'].'</strong></span></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Ubicación / Zona</span></td>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Para Notificación</span></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Asentamiento: <strong>'.$data_cvecat['NOM_COL'].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Asentamiento: <strong>'.$data_cvecat['NOM_COL_NO'].'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Calle: <strong>'.$data_cvecat['NOM_CALLE'].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Calle: <strong>'.$data_cvecat['UBI_PRED'].'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Código Postal: <strong>'.$cod_pos.'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Código zona: <strong> '.$datazona["CVE_ZONA"].' - '.$datazona["DES_ZONA"].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Código Postal: <strong>'.$data_cvecat['COD_POST_N'].'</strong> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Entrecalles: <strong> S/N </strong></span></span></td>
    </tr>
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Núm de Fte: <strong>'.$data_cvecat['NUM_FTES'].'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;  Metros de Fte: <strong> '.$data_cvecat["LON_FTE"].'</strong></span><span style="font-size:8;">&nbsp;&nbsp;&nbsp;  Valor de zona: <strong> '.$datazona["VAL_UNIT_T"].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Referencia: <strong> S/N </strong></span></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Registro Público de la Propiedad</span></td>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Libro: <strong>'.$data_cvecat['NUM_LIB'].'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Inscripción: <strong> '.$data_cvecat["NUM_INSC"].'</strong></span><span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Sección: <strong> '.$data_cvecat["NUM_SECC"].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Escritura: <strong>'.str_pad($data_cvecat['NUM_ESC'], 5, "0", STR_PAD_LEFT).'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Folio Real: <strong> - </strong></span></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Servicios</span></td>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"></td>
    </tr>

    <tr>
        <td align="left" width="40%" style="display:flex; items-align:left;"><span style="font-size:8;"> <strong> '.$servicios_explode[0].'    </strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <strong> '.$servicios_explode[6].'   </strong></span></td>
        <td align="left" width="60%" style="display:flex; items-align:left;"><span style="font-size:8;"> <strong> '.$servicios_explode[1].'        </strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <strong> '.$servicios_explode[2].'      </strong></span></td>
    </tr>
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;"><strong>  '.$servicios_explode[5].'   </strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <strong>  '.$servicios_explode[7].'  </strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;"> <strong>  '.$servicios_explode[3].'       </strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <strong>  '.$servicios_explode[8].'    </strong></span></td>
    </tr>
    
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Último movimiento</span></td>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"></td>
    </tr>

    <tr>
        <td align="left" width="100%" style="display:flex; items-align:left;"><span style="font-size:8;">Folio de trámite: <strong>  '.$folio.'   </strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fecha trámite: <strong>  '.strftime("%d/%B/%Y",strtotime($fecha_tramite)).'   </strong></span><span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Trámite: <strong>  '.$tramite.'    </strong></span></td>
        
    </tr>
    <tr>
        <td align="left" width="70%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Descripción:</span></td>
        <td align="center" width="30%" style="display:flex; items-align:center;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Croquis de ubicación:</span></td>
    </tr>
    <tr>
        <td align="left" width="65%" style="display:flex; items-align:left;"><span style="font-size:8;">'.$descripcion_tramite.'</span></td>
        <td align="left" width="35%" style="display:flex; items-align:left;"></td>
    </tr>
    <tr>
        <td></td><td></td>
    </tr>
    </table>
    ';
    $pdf->writeHTML($html1, true, false, false, false, '');
    $pdf->writeHTML('<p style="text-align:center; font-size:16px; "><b>Avalúo</b></p>
    ', true, false, false, false, '');

    //obtenemos los valores del terreno
    $sqlterr = '
    SELECT * from h_terr WHERE folio = \''.$folio.'\' and clave= \''.$ccat.'\' ORDER BY "NUM_TERR";
    ';
    $estaterr = $pdo->prepare($sqlterr);
    $estaterr->execute();
    $dataterr = $estaterr->fetchAll(PDO::FETCH_ASSOC);
    $terrenos = "";
    $sup_total = 0.0;
    $valor_total = 0.0;
    foreach ($dataterr as $terr){

        $terrenos .= '<tr style="border-style: dotted;">
        <td align="center" style="font-size:8px; border-left-style: solid;">'.$terr["CVE_CALLE"].' </td>
        <td align="center" style="font-size:8px; ">'.$terr["CVE_TRAMO"].' </td>
        <td align="center" style="font-size:8px;  ">'.$terr["SUP_TERR"].'</td>
        <td align="center" style="font-size:8px;  ">'.$fmt->formatCurrency($terr["valorunitario1"],"MXN").' </td>
        <td align="center" style="font-size:8px;  ">'.$terr["CVE_CALLE_"].' </td>
        <td align="center" style="font-size:8px;  ">'.$terr["CVE_TRAMO_"].' </td>
        <td align="center" style="font-size:8px;  ">'.$terr["SUP_TERR_E"].' </td>
        <td align="center" style="font-size:8px;  ">'.$fmt->formatCurrency($terr["valorunitario2"],"MXN").' </td>

        <td align="center" style="font-size:8px;  ">'.$terr["SUP_TERR_D"].' </td>
        <td align="center" style="font-size:8px;  ">'.$terr["demeritos"].' </td>
        <td align="center" style="font-size:8px;  ">'.$terr["FAC_DEM_TE"].' </td>
        <td align="center" style="font-size:8px; border-right-style: solid;">'.$fmt->formatCurrency($terr["valorneto"],"MXN").' </td>
        </tr>';
        $sup_total +=  $terr["SUP_TERR"];
        $valor_total += $terr["valorneto"];
    }  

    $tblterreno = '<span style="font-size:9; color:#632B2B; font-weight:bold;">Datos de Terreno:</span><br><table border="0" cellpadding="2" cellspacing="0" nobr="false">
    <tr style="background-color:#e3e3e3;">
    <td colspan="4" align="center" style="font-size:8px;  border-style: solid; border-style: solid; border-left-style: solid;">G E N E R A L</td>
    <td colspan="4" align="center" style="font-size:8px;  border-style: solid; border-left-style: solid; border-right-style: solid;">INCREMENTO POR ESQUINA (25%)</td>
    <td colspan="4" align="center" style="font-size:8px;  border-style: solid; border-style: solid; border-right-style: solid; border-left-style: solid;">D E M E R I T O</td>
    </tr>
    <tr style="border-style: solid; background-color:#e3e3e3;">
    <td align="center" style="font-size:6px; border-style: solid; border-left-style: solid;  border-bottom-style: solid;">Calle</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Tramo</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Sup M2</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Valor Unitario</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Calle</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Tramo</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Superficie M2</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Valor Unitario</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Superficie M2</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">CV1-CV2-CV3</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">% dem</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid; border-right-style: solid;">Valor Neto</td>
    </tr>
    '.$terrenos.'
    <tr>
    <td colspan="14" style="font-size:8px;  border-left-style: solid; border-right-style: solid; border-bottom-style: solid;"></td>
    </tr>
    <tr>
    <td colspan="2" style="font-size:8; color:#632B2B; font-weight:bold;">Total :</td>
    <td style="font-size:9px; font-weight:bold;">'.$sup_total.'</td>
    <td colspan="3" style="font-size:8px;"> </td>
    <td colspan="4" style="font-size:8; color:#632B2B; font-weight:bold;  text-align:right;" >Valor Catastral del Terreno :</td>
    <td colspan="2" style="font-size:9px; text-align:right; font-weight:bold;" >'.$fmt->formatCurrency($valor_total,"MXN").'</td>
    </tr>

    </table>
    ';
    $pdf->writeHTML($tblterreno, true, false, false, false, '');


    //empiezo a calcular renglones de construccion
    $SuperficieConstruccionFormateado = 0;
    $ValorConstruccionFormateado = 0;
    $ValorCatastralFormateado = 0;

    $sqlconst = 'SELECT * FROM h_const WHERE folio = \''.$folio.'\' and clave= \''.$ccat.'\' ORDER BY "NUM_CONST"';

    $estaconst = $pdo->prepare($sqlconst);
    $estaconst->execute();
    $dataconst = $estaconst->fetchAll(PDO::FETCH_ASSOC);

    $construcciones = "";
    $Categoria = "";
    $Edad = 0;
    $estado = 0;
    foreach ($dataconst as $const){
    //'busquedas de categorias
    $Categoria = $const["CVE_CAT_CO"];
    $Superficie = $const["SUP_CONST"];
    $Edad = $const["EDD_CONST"];
    $estado = $const["CVE_EDO_CO"];
    $valorunitario = $const["valorunitario"];
    $factdem = $const["factordemerito"];
    $factcom = $const["factorcomercializacion"];
    $valorneto = $const["valorneto"];
    
    $construcciones .= '<tr style="border-style: dotted;">
    <td align="center" style="font-size:8px; border-left-style: solid;">'.$Categoria.' </td>
    <td align="center" style="font-size:8px; ">'.$Superficie.' </td>
    <td align="center" style="font-size:8px;  ">'.$fmt->formatCurrency($valorunitario,"MXN").' </td>
    <td align="center" style="font-size:8px;  ">'.$estado.' </td>
    <td align="center" style="font-size:8px;  ">'.$Edad.' </td>
    <td align="center" style="font-size:8px;  ">'.$factdem.' </td>
    <td align="center" style="font-size:8px;  ">'.$factcom.' </td>
    <td align="center" style="font-size:8px; border-right-style: solid;">'.$fmt->formatCurrency($valorneto,"MXN").' </td>
    </tr>';

    $SuperficieConstruccionFormateado += $Superficie;
    $ValorConstruccionFormateado += $valorneto;
    }

    //valor catastral del predio total de construccion mas total del terreno
    $ValorCatastralFormateado = $ValorConstruccionFormateado + $valor_total;

    $tblconst = '<span style="font-size:9; color:#632B2B; font-weight:bold;">Datos de Construcción:</span><br><table border="0" cellpadding="1" cellspacing="0" nobr="false">
    <tr style="border-style: solid; background-color:#e3e3e3;">
    <td align="center" style="font-size:7px; border-style: solid; border-left-style: solid;  border-bottom-style: solid;">Categoría</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Superficie M2</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Valor Unitario</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Estado</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Edad</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Fact. Dem.</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Fact. Com.</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid; border-right-style: solid;">Valor Neto ($)</td>
    </tr>
    '.$construcciones.'
    <tr>
    <td colspan="8" style="font-size:8px;  border-left-style: solid; border-right-style: solid; border-bottom-style: solid;"></td>
    </tr>
    <tr>
    <td colspan="1" style="font-size:8; color:#632B2B; font-weight:bold;">Total :</td>
    <td style="font-size:9px; font-weight:bold;" align="center">'.$SuperficieConstruccionFormateado.'</td>
    <td colspan="1" style="font-size:10px;"> </td>
    <td colspan="4" style="font-size:8; color:#632B2B; text-align:right; font-weight:bold;" >Valor Catastral de la Construcción :</td>
    <td colspan="2" style="font-size:9px; font-weight:bold; text-align:right;" >'.$fmt->formatCurrency($ValorConstruccionFormateado,"MXN").'</td>
    </tr>

    </table>
    ';
    $pdf->writeHTML($tblconst, true, false, false, false, '');

    $ValorAnterior = 0.0;
    $ValorAnterior = $data_cvecat['VAL_TERR_A'] + $data_cvecat['VAL_CONST_'];
    $pdf->SetAutoPageBreak(TRUE, 0);

    $tablefooter = '
    <table border="0" cellpadding="2" cellspacing="0" nobr="false">
        <tr>
            <td align="center" width="15%" rowspan="5"></td>
            <td align="left" width="85%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:7; text-align:justify;"><b>Sello digital del trámite :</b> '.base64_encode($signature).'</span></td>
        </tr>

        <tr>
            <td align="left" width="30%" style="display:flex; items-align:center;"><span style="font-size:8;"><b>Autorizado por: </b></span></td>
            <td align="left" width="50%" style="display:flex; items-align:center;"><span style="font-size:8;">Director:  <strong>'.$data_firma['director'].'</strong></span></td>
        </tr>

        <tr>
            <td align="left" width="85%" style="display:flex; items-align:center;"><span style="font-size:8; font-weight:bold;">Valor catastral del predio:</span></td>
        </tr>

        <tr>
            <td align="left" width="30%" style="display:flex; items-align:center;"><span style="font-size:8;">Valor Anterior ($): <strong>'.$fmt->formatCurrency($ValorAnterior,"MXN").'</strong></span></td>
            <td align="center" width="25%" style="display:flex; items-align:center;"><span style="font-size:8;">Efectos : <strong>TRIMESTRE '.$data_cvecat['TRI_EFEC'].'/'.$data_cvecat['ATO_EFEC'].'</strong> </span></td>
            <td align="right" width="30%" style="display:flex; items-align:center;"><span style="font-size:8;">Valor actual : <strong style="font-size:10;">'.$fmt->formatCurrency($ValorCatastralFormateado,"MXN").'</strong> </span></td>
        </tr>
        
    </table>
    <pre>Fecha de Impresión: '.date("m/d/Y h:i:s a", time()).'</pre>
    ';

    $style = array(
        'border' => 1,
        'padding' => 1,
        'fgcolor' => array(0,0,0),
        'bgcolor' => false
    );

    $pdf->write2DBarcode($url_qr, 'QRCODE,H',14,235, 28, 28, $style, 'N');

    $pdf->setInfo($tablefooter);

    //el QR en la esquina
    //pie de la pagina
    // new style
    
    //Close and output PDF document
    $pdf->Output('cedula_catastral_'.$folio.'.pdf', 'I');

}
else
    echo "No se enviaron parametros";

?>