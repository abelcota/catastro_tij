<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('memory_limit', '-1');
setlocale(LC_TIME, "spanish");
date_default_timezone_set('America/Mazatlan');
require('../conexion.php');

$fmt = new NumberFormatter( 'es_MX', NumberFormatter::CURRENCY );


//variables que se obtendran por post
$a = isset($_GET['a']) ? $_GET['a'] : '00';
$fol = isset($_GET['fol']) ? $_GET['fol'] : '00000';
$hoy = date('Y-m-d H:i:s');

// Include the main TCPDF library (search for installation path).
require_once('extensions/tcpdf/tcpdf.php');

$query = 'SELECT * FROM "Inco_AvisoCambio" ia 
inner join "Gen_Empleados" AS e on ia."IdEmpleadoBarra" = e."IdEmpleado"
WHERE "Folio" = \''.$fol.'\' AND "AnioAviso" = \''.$a.'\' ORDER BY "Clave"';
$estado= $pdo->prepare($query);
$estado->execute();
$data = $estado->fetch(\PDO::FETCH_ASSOC);

$movimiento = "";

if($data["TipoB"] == 'X'){
  $movimiento = "BAJA";
}
else if($data["TipoC"] == 'X'){
  $movimiento = "CAMBIO";
}
$expedicion = "";
if($a != '00')
{
    $expedicion = $data["Poblacion"]."-".$data["Cuartel"]."-".$data["Manzana"];
}
//$clave = "007".$data["Poblacion"]."-".$data["Cuartel"]."-".$data["Manzana"]."-".$data["Predio"]."-".$data["Unidad"];


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
      
       
    }

    // Page footer
    public function Footer() {
     
      }
}

// create new PDF document
$pdf = new MYPDF('P', 'mm', 'LETTER', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Direccion de Catastro Culiacan');
$pdf->SetTitle('Proyecto de Cambio al Padron');
$pdf->SetSubject('Proyecto de Cambio al Padron');
$pdf->SetKeywords('cambio, padron, proyecto, catastro, ayuntamiento, culiacan');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// define barcode style


// add a page
$pdf->AddPage('P', 'LETTER', TRUE);
$pdf->SetFont('helvetica', 'N', 9);
$pdf->setXY(10,10);
// Right alignment
$html2 = '
<table border="0" cellpadding="3" cellspacing="0" nobr="false">
<tr>
<td align="left" width="10%"><img src="../assets/images/ices.jpg" width="70"></td>
  <td align="center" style="font-size:12px;" width="50%"><b>GOBIERNO DE CULIACAN 2018-2021<br>DIRECCION DE CATASTRO MUNICIPAL<br>AVISO DE CAMBIO AL PADRÓN<br>DIRECTOR DE CATASTRO<br>PRESENTE.</b></td>
  <td align="left" width="10%"><img src="../assets/images/logo_catastro1.jpg" width="70"></td>
  <td align="right" style="font-size:11px;" width="18%"> RELACION NÚMERO: <br><br>EXPEDICIÓN: <br><br>NÚMERO DE OFICIO:</td>
  <td align="left" style="font-size:11px;" width="12%">  <br><br><br>'.$expedicion.' <br><br>
  </td>
</tr>
<tr>
<td width="50%"><br><br><b>SIRVASE EFECTUAR LOS SIGUIENTES MOVIMIENTOS:<br>
EN SU PADRON DE PROPIEDAD RAIZ</b></td>
<td width="20%" align:"left"><br><br>BAJA: '.$data["TipoB"].'<br>CAMBIO: '.$data["TipoC"].'</td>
<td width="30%">
<table border="1" cellpadding="3" cellspacing="0" nobr="false" align="center">
<tr><td style="background-color:#C9C9C1">POBLACIÓN</td></tr>
<tr><td>'.$data["NPoblacion"].'</td></tr>
<tr><td style="background-color:#C9C9C1">MUNICIPIO</td></tr>
<tr><td>CULIACÁN</td></tr>
</table>
</td>
</tr>
<tr>
<td>ANTERIOR</td>
</tr>
<tr>
<td width="100%">
<table border="1" cellpadding="3" cellspacing="0" nobr="false" align="center" style="font-size:10px">
<tr>
<td style="background-color:#C9C9C1" width="6%">OFIC.</td>
<td style="background-color:#C9C9C1" width="6%">CTEL.</td>
<td style="background-color:#C9C9C1" width="6%">MANZ.</td>
<td style="background-color:#C9C9C1" width="6%">PRED.</td>
<td style="background-color:#C9C9C1" width="3%">C</td>
<td style="background-color:#C9C9C1" width="6%">MPIO.</td>
<td style="background-color:#C9C9C1" width="6%">FOTO</td>
<td style="background-color:#C9C9C1" width="6%">LOTE</td>
<td style="background-color:#C9C9C1" width="6%">PRED</td>
<td style="background-color:#C9C9C1" width="9%">RUSTICA</td>
<td style="background-color:#C9C9C1" width="40%">NOMBRE</td>
</tr>
<tr>
<td width="6%">007'.$data["Poblacion"].'</td>
<td width="6%">'.$data["Cuartel"].'</td>
<td width="6%">'.$data["Manzana"].'</td>
<td width="6%">'.$data["Predio"].'</td>
<td width="3%">'.intval($data["Unidad"]).'</td>
<td width="6%"> </td>
<td width="6%"> </td>
<td width="6%"> </td>
<td width="6%"> </td>
<td width="9%"> </td>
<td width="40%">'.$data["PropietarioAnterior"].'</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>ACTUAL</td>
</tr>
<tr>
<td width="100%">
<table border="1" cellpadding="3" cellspacing="0" nobr="false" align="center" style="font-size:10px">
<tr>
<td style="background-color:#C9C9C1" width="6%">OFIC.</td>
<td style="background-color:#C9C9C1" width="6%">CTEL.</td>
<td style="background-color:#C9C9C1" width="6%">MANZ.</td>
<td style="background-color:#C9C9C1" width="6%">PRED.</td>
<td style="background-color:#C9C9C1" width="3%">C</td>
<td style="background-color:#C9C9C1" width="6%">MPIO.</td>
<td style="background-color:#C9C9C1" width="6%">FOTO</td>
<td style="background-color:#C9C9C1" width="6%">LOTE</td>
<td style="background-color:#C9C9C1" width="6%">PRED</td>
<td style="background-color:#C9C9C1" width="9%">RUSTICA</td>
<td style="background-color:#C9C9C1" width="40%">NOMBRE</td>
</tr>
<tr>
<td width="6%">007'.$data["Poblacion"].'</td>
<td width="6%">'.$data["Cuartel"].'</td>
<td width="6%">'.$data["Manzana"].'</td>
<td width="6%">'.$data["Predio"].'</td>
<td width="3%">'.intval($data["Unidad"]).'</td>
<td width="6%"> </td>
<td width="6%"> </td>
<td width="6%"> </td>
<td width="6%"> </td>
<td width="9%"> </td>
<td width="40%">'.$data["PropietarioActual"].'</td>
</tr>
<tr>
<td width="50%" align="center" style="background-color:#C9C9C1">DOMICILIO PARA NOTIFICACIONES</td>
<td width="50%" align="center" style="background-color:#C9C9C1">UBICACIÓN DEL PREDIO</td>
</tr>
<tr>
<td width="50%" align="left" >'.$data["DomicilioNotificacion"].'<br><br></td>
<td width="50%" align="left" >'.$data["DomicilioUbicacion"].'<br><br></td>
</tr>
<tr>
<td width="60%"> </td>
<td width="20%">TOTAL O PARCIAL</td>
<td width="20%">TOTAL O PARCIAL</td>
</tr>
<tr>
<td width="10%" align="center" style="background-color:#C9C9C1">NOTICIA No.</td>
<td width="15%" align="center" style="background-color:#C9C9C1">CONTROL No.</td>
<td width="15%" align="center" style="background-color:#C9C9C1">CONCEPTO</td>
<td width="20%" align="center" style="background-color:#C9C9C1">VALOR OPERACION</td>
<td width="20%" align="center" style="background-color:#C9C9C1">VALOR CATASTRAL</td>
<td width="20%" align="center" style="background-color:#C9C9C1">VALOR FISCAL</td>
</tr>
<tr>
<td width="10%" align="center" >'.$data["Noticia"].'</td>
<td width="15%" align="center" >'.$data["Control"].'</td>
<td width="15%" align="center" >'.$data["Concepto"].'</td>
<td width="20%" align="center" > </td>
<td width="20%" align="center" > </td>
<td width="20%" align="center" > </td>
</tr>
<tr>
<td width="100%" align="left">TITULO TRASLATIVO</td>
</tr>
<tr>
<td width="100%" align="left">'.$data["TituloTraslativo"].'<br><br></td>
</tr>
<tr>
<td width="100%" align="left" style="background-color:#C9C9C1"></td>
</tr>
<tr>
<td width="100%" align="left">MOTIVOS</td>
</tr>
<tr>
<td width="100%" align="left">'.$data["Motivo"].'<br><br></td>
</tr>
<tr>
<td width="23%" align="center" >FORMULÓ</td>
<td width="23%" align="center" >REVISÓ</td>
<td width="23%" align="center" >AUTORIZÓ</td>
<td width="10%" align="center" >DIA</td>
<td width="11%" align="center" >MES</td>
<td width="10%" align="center" >AÑO</td>
</tr>
<tr>
<td width="23%" align="center"><br>'.$data["Nombre"].' '.$data["ApellidoPaterno"].'<br></td>
<td width="23%" align="center"><br>MARTÍN ALONSO MILLÁN MARTÍNEZ<br></td>
<td width="23%" align="center"><br>PEDRO RIOS MORGAN<br></td>
<td width="10%" align="center"><br>'.strftime("%d",strtotime($hoy)).'<br></td>
<td width="11%" align="center"><br>'.strtoupper(strftime("%B",strtotime($hoy))).'<br></td>
<td width="10%" align="center"><br>'.strftime("%Y",strtotime($hoy)).'<br></td>
</tr>
</table>
</td>
</tr>
</table>';
$pdf->writeHTML($html2, true, false, false, false, '');

$pdf->IncludeJS("print(true);");
//Close and output PDF document
$pdf->Output('PDFCDI.pdf', 'I');


//============================================================+
// END OF FILE
//============================================================+

?>