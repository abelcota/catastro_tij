<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('memory_limit', '-1');
setlocale(LC_TIME, "spanish");
date_default_timezone_set('America/Mazatlan');

require('../conexion.php');

$fmt = new NumberFormatter( 'es_MX', NumberFormatter::CURRENCY );

//variables que se obtendran por post
if (isset($_POST['a']) && isset($_POST['fol'])) {

$a = $_POST['a'];
$fol = $_POST['fol'];

// Include the main TCPDF library (search for installation path).
require_once('extensions/tcpdf/tcpdf.php');


$query = 'SELECT * FROM "Barra_OficioBonificacion" ob 
inner join "Gen_Empleados" AS e on ob."IdEmpleadoBarra" = e."IdEmpleado"
WHERE "IdFolio" = \''.$fol.'\' AND "IdAnio" = \''.$a.'\'';
$estado= $pdo->prepare($query);
$estado->execute();
$data = $estado->fetch(\PDO::FETCH_ASSOC);

if ($estado->rowCount() > 0){

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    protected $info;
    
    public function setInfo($info){
      $this->info = $info;
    }

    //Page header
    public function Header() {
      
        $this->Image('../assets/images/logo_v.png', 180, 5, 25, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->Image('../assets/images/logo_c.png', 150, 13, 30, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
        // Logo
        $this->Image('../assets/images/escudo_sinaloa.png', 15, 8, 15, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->Image('../assets/images/logo_catastro1.jpg', 35, 8, 20, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);

        $this->SetFont('helvetica', 'N', 11);
        // Set font
        $this->Ln(4);
        $this->writeHTML('
        <p style="text-align:center"><strong>GOBIERNO DEL ESTADO DE SINALOA<BR>H. AYUNTAMIENTO DE TIJUANA<BR>DIRECCIÓN DE CATASTRO</strong><br><u>AVISO PARA OFICIO DE BONIFICACIÓN</u></p>
        ', true, false, false, false, '');
    }

    // Page footer
    public function Footer() {
      // Position at 30 mm from bottom
       $this->SetY(-28);
       $this->writeHTML($this->info, true, false, false, false, '');
      }
}

// create new PDF document
$pdf = new MYPDF('P', 'mm', 'LETTER', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Direccion de Catastro Culiacan');
$pdf->SetTitle('Oficio de Bonificación');
$pdf->SetSubject('Aviso para Oficio de Bonificación');
$pdf->SetKeywords('aviso, oficio, bonificacion, catastro, ayuntamiento, culiacan');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// define barcode style
$style = array(
  'position' => 'absolute',
  'align' => 'R',
  'stretch' => true,
  'fitwidth' => true,
  'cellfitalign' => '',
  'border' => false,
  'hpadding' => 0,
  'vpadding' => 0,
  'fgcolor' => array(0,0,0),
  'bgcolor' => false, //array(255,255,255),
  'text' => true,
  'font' => 'helvetica',
  'fontsize' => 8,
  'stretchtext' => 4
);


// add a page
$pdf->AddPage('P', 'LETTER', TRUE);
$pdf->SetFont('helvetica', 'N', 11);
// Set font
//$pdf->Ln(2);
//pdf->writeHTML('<p style="text-align:center; text-decoration: underline; ">'.$tipodec.'</p>', true, false, false, false, '');
$style['position'] = 'absolute';
$style['stretch'] = true; // disable stretch
$style['fitwidth'] = true; // disable fitwidth
$style['position'] = 'C';

$pdf->write1DBarcode($a.$fol, 'C128A', 0, 33, '', 10, 0.5, $style, 'N');
$pdf->SetFont('helvetica', 'N', 10);  
// Right alignment
$html2 = '

<table border="0" cellpadding="2" cellspacing="0" nobr="true">
  <tr>
    <td align="left" width="65%" style="display:flex; items-align:center;"><br><br>
    <span style="font-size:11; font-weight:bold;">TELÉFONO DE CATASTRO:     218-32-00 EXT. 1203</span>
    </td>
    <td width="35%" style="font-size:12;">
      <b align="left">Folio:</b> <span align="right">'.$data["IdFolio"].'</span><br>
      <b align="left">Fecha:</b> <span align="right">'.strftime("%d/%B/%Y",strtotime($data['FechaCaptura'])).'</span><br>
    </td>
  </tr>
</table>
<hr>
';
$pdf->writeHTML($html2, true, false, false, false, '');

$html3 = '
<table cellpadding="10" cellspacing="0" nobr="true">
  <tr>
    <td align="left" width="50%" style="display:flex; items-align:center;"><b>CLAVE DONDE PAGO SU IMPUESTO PREDIAL: </b></td>
    <td align="left" width="50%">007-'.rtrim(chunk_split($data['ClaveIncorrecta'], 3, '-'),"-").'</td>
  </tr>
  <tr>
    <td align="left" width="50%" style="display:flex; items-align:center;"><b>A NOMBRE DE: </b></td>
    <td align="left" width="50%">'.trim($data['NombreIncorrecto']).'</td>
  </tr>
  <tr>
  <td align="left" width="50%" style="display:flex; items-align:center;"><b>LE CORRESPONDE LA CLAVE: </b>
  </td>
  <td align="left" width="50%">007-'.rtrim(chunk_split($data['ClaveCorrecta'], 3, '-'),"-").' </td>
</tr>
<tr>
  <td align="left" width="50%" style="display:flex; items-align:center;"><b>A NOMBRE DE: </b>
  </td>
  <td align="left" width="50%">'.trim($data['NombreCorrecto']).'</td>
</tr>

<tr>
  <td align="left" width="35%" style="display:flex; items-align:center;"><b>OBSERVACIONES : </b></td>
  
</tr>
<tr>
    <td align="left" width="100%" style="display:flex; text-align:justify;">'.$data['Observaciones'].'</td>
</tr>
</table>
';
$pdf->writeHTML($html3, true, false, false, false, '');


$tablefooter = '
<table border="0" cellpadding="3" cellspacing="0" nobr="false">
<tr>
  <td align="left" style="font-size:12px;"><br><br>ATENDIDO POR: '.$data["Nombre"].'</td>
  <td align="center" style="font-size:12px;"> 
  ______________________________________________<br>
  NOMBRE Y FIRMA DEL SOLICITANTE
  </td>
</tr>
</table>
';

$pdf->setInfo($tablefooter);

$nombrepdf = 'OficioBonificacion_'.$a.$fol.'.pdf';

//Close and output PDF document
$pdf->Output($nombrepdf, 'I');

//============================================================+
// END OF FILE
//============================================================+
}
else{
    echo "No se ha encontrado el Folio proporcionado";
}
}
else
    echo "No se enviaron parametros";

?>