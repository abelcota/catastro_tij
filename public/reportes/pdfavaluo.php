<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
setlocale(LC_TIME, "spanish");
date_default_timezone_set('America/Mazatlan');

//para firmar (revision)
require_once('../src/Internal/BaseConverter.php');
require_once('../src/Internal/BaseConverterSequence.php');
require_once('../src/Internal/DataArrayTrait.php');
require_once('../src/Internal/Key.php');
require_once('../src/Internal/LocalFileOpenTrait.php');
require_once('../src/Internal/Rfc4514.php');

require_once('../src/Certificate.php');
require_once('../src/Credential.php');
require_once('../src/PemExtractor.php');
require_once('../src/Certificate.php');
require_once('../src/PrivateKey.php');
require_once('../src/PublicKey.php');
require_once('../src/SerialNumber.php');

require('../conexion.php');


$fmt = new NumberFormatter( 'es_MX', NumberFormatter::CURRENCY );

$query_firma = 'SELECT * FROM configuracion';

$estadof= $pdo->prepare($query_firma);
$estadof->execute();
$data_firma = $estadof->fetch(\PDO::FETCH_ASSOC);

$cerFile = '../assets/firma/'.$data_firma['cerfile'];
$pemKeyFile = '../assets/firma/'.$data_firma['pemkeyfile'];
$passPhrase = base64_decode($data_firma['passphrase']); // contraseña para abrir la llave privada

$fiel = PhpCfdi\Credentials\Credential::openFiles($cerFile, $pemKeyFile, $passPhrase);


//variables que se obtendran por post
if (isset($_POST['clave']) && isset($_POST['img']) || isset($_GET['id']) ) {

    if( isset($_GET['id']) ){
        $cvecat = $_GET['id'];
    }
    else{
        $cvecat = $_POST['clave'];
    }
    //se hace la consulta a la base de datos
   
    $query_clave = 'SELECT*,encode("FOTO",\'base64\') AS "foto",(
        SELECT row_to_json (fc) AS jsonb_build_object FROM (
        SELECT \'FeatureCollection\' AS TYPE,array_to_json (ARRAY_AGG (f)) AS features FROM (
        SELECT \'Feature\' AS TYPE,ST_AsGeoJSON (ST_Transform (lg.geom,4326),15,0) :: json AS geometry,row_to_json ((ID,dg_ccat,nom_comp_t,nom_calle,nom_col,num_ofic_u,sup_terr,sup_const,val_catast)) AS properties FROM (
        SELECT*FROM "Culiacan_Join" WHERE dg_ccat=\'007'.$cvecat.'\') AS lg) AS f) AS fc) jsonb_build_object FROM a_unid A LEFT JOIN a_calle b ON A."CVE_POBL"=b."CVE_POBL" AND A."CVE_CALLE_"=b."CVE_CALLE" LEFT JOIN a_col C ON A."CVE_POBL"=C."CVE_POBL" AND A."CVE_COL_UB"=C."CVE_COL" LEFT JOIN a_prop d ON A."CVE_PROP"=d."CVE_PROP" LEFT JOIN a_usosayto e ON A."CVE_USO"=e."CVE_USO" LEFT JOIN a_regim ar ON A."CVE_REGIM"=ar."CVE_REGIM" WHERE A."CLAVE"=\''.$cvecat.'\'';

    $estado= $pdo->prepare($query_clave);
    $estado->execute();
    $data_cvecat = $estado->fetch(\PDO::FETCH_ASSOC);

    $folio = "";
    $tramite = "";
    $fecha_tramite = "";
    $descripcion_tramite = "";

    //obtener informacion del folio
    if(isset($_POST['a']) && isset($_POST['fol'])){
        $a = $_POST['a'];
        $fol = $_POST['fol'];

        $sql_fol = "SELECT * from \"Inco_Barra\" ib 
        inner join \"Inco_Tramites\" t on ib.\"IdTramite\" = t.\"IdTramite\"
        where \"IdInconformidad\" =  '$fol' AND \"AnioDeclaracion\" = '$a' ;";

        $estado_fol = $pdo->prepare($sql_fol);
        $estado_fol->execute();
        $data_folio = $estado_fol->fetch(\PDO::FETCH_ASSOC);

        $folio = $a.$fol;
        $tramite = $data_folio['DescripcionTramite'];
        $fecha_tramite = $data_folio['FechaCapturaInconformidad'];
        $descripcion_tramite = $data_folio['ObservacionesBarra'];
    }

    //OBTENER EL NOMBRE DE LA POBLACION
    $clave_pobl = str_split($cvecat, 3);
    $clave_pobl = intval($clave_pobl[0]); 
    $sql_pobl = 'SELECT * FROM a_pobl WHERE "CVE_POBL" = '.$clave_pobl;
    $estadopobl = $pdo->prepare($sql_pobl);
    $estadopobl->execute();
    $datapobl = $estadopobl->fetch(PDO::FETCH_ASSOC);


    //OBNTENEMS LOS DATOS DE LA ZONA
    $sqlzona = '
    SELECT * FROM a_zona, a_manz 
    WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA")
    AND a_manz."CVE_POBL"='.$data_cvecat["CVE_POBL"].'
    AND a_manz."NUM_CTEL"= '.$data_cvecat["NUM_CTEL"].'
    AND a_manz."NUM_MANZ"= '.$data_cvecat["NUM_MANZ"].'
    ';
    $estadozona = $pdo->prepare($sqlzona);
    $estadozona->execute();
    $datazona = $estadozona->fetch(PDO::FETCH_ASSOC);

    //obtener los servicios y regimen
    $sqlserv = '
    -- busca servicios y regimen
    SELECT CONCAT(a."CVE_REGIM" ,  \' - \' , reg."DES_REGIM") AS Regimen, a."NUM_FTES" AS Num_Frentes, a."LON_FTE" AS Long_Frente, a."LON_FONDO" AS Long_Fondo, ser.descripcionx AS Servicios FROM a_unid AS a, a_regim AS reg, "a_ServiciosInternet" AS ser WHERE (a."CVE_REGIM"=reg."CVE_REGIM") AND (a."STS_SERVIC"=ser.campox) AND a."CLAVE"= \''. $cvecat.'\'';

    $estadoserv = $pdo->prepare($sqlserv);
    $estadoserv->execute();
    $dataserv = $estadoserv->fetch(\PDO::FETCH_ASSOC);


    // Include the main TCPDF library (search for installation path).
    require_once('extensions/tcpdf/tcpdf.php');

    // Extend the TCPDF class to create custom Header and Footer
    class MYPDF extends TCPDF {

        protected $info;
        protected $folio;
        protected $cve;
        protected $pobl;

        public function set_data($folio = "", $cve = "", $datapobl) {
            $this->folio = $folio;
            $this->cve = $cve;
            $this->pobl = $datapobl;
        }

        public function setInfo($info){
            $this->info = $info;

        }

        //Page header
        public function Header() {
        
            /*$this->Image('../assets/images/logo_v.png', 180, 5, 25, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
            */
            // Logo
            $this->Image('../assets/images/Culiacan_Escudo.png', 13, 6, 20, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
            $this->Image('../assets/images/logo_catastro1.jpg', 35, 8, 20, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);

            $this->SetFont('helvetica', 'N', 10);
            // Set font
            $this->Ln(1);

            $tabla_header = '<table border="0" cellpadding="2" cellspacing="0" nobr="false">
            <tr>
                <td align="left" width="20%"></td>
                <td align="center" width="50%" style="display:flex; items-align:justify;"><strong>GOBIERNO DE CULIACÁN 2018-2021<BR>AYUNTAMIENTO DE CULIACÁN<BR>SECRETARÍA DE ADMINISTRACIÓN Y FINANZAS<br>DIRECCIÓN DE CATASTRO MUNICIPAL</strong><br></td>
                <td align="left" width="10%">Folio: <br>Fecha de Emisión: <br>Municipio: <br>Poblacion: </td>
                <td align="right" width="20%" style="font-weight:bold; font-size:9"><strong style="font-size:16">'.$this->folio.'</strong> <strong style="font-size:12"><br>'.date("m/d/Y", time()).'</strong><br><strong style="font-size:10">007 - CULIACÁN</strong> <br><strong style="font-size:10">'.str_pad($this->pobl['CVE_POBL'], 2, "0").' - '.$this->pobl['NOM_POBL'].'</strong></td>
            </tr>
            <tr>
                <td width="100%" aligh="center" style="font-size: 16; font-weight:bold; text-align:center;">CÉDULA CATASTRAL</td>
            </tr>
            </table>';
            $this->writeHTML($tabla_header , true, false, false, false, '');
            /*$this->writeHTML('
            <p style="text-align:center"><strong>GOBIERNO DEL ESTADO DE SINALOA<BR>H. AYUNTAMIENTO DE TIJUANA<BR>DIRECCIÓN DE CATASTRO MUNICIPAL</strong><br></p>
            ', true, false, false, false, '');*/
            /*$this->SetFont('helvetica', 'N', 16);
            $this->writeHTML('
            <p style="text-align:center"><b>CÉDULA CATASTRAL</b></p>
            ', true, false, false, false, '');
           */
        }

        // Page footer
        public function Footer() {
            // Position at 30 mm from bottom
            $this->SetY(-45);
            $this->writeHTML($this->info, true, false, false, false, '');
        }
    }

    // create new PDF document
    $pdf = new MYPDF('P', 'mm', PDF_PAGE_FORMAT , true, 'UTF-8', false);

    $pdf->set_data($folio, $cvecat, $datapobl);

    $signature = $fiel->sign($folio."-".$cvecat);


    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Direccion de Catastro Culiacan');
    $pdf->SetTitle('Cedula Catastral '.$folio);
    $pdf->SetSubject('Cedula Catastral');
    $pdf->SetKeywords('cedula, catastral, catastro, ayuntamiento, culiacan');

    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,);

    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // add a page
    $pdf->AddPage('P', [216, 279], TRUE);
    $pdf->SetFont('helvetica', 'N', 10);
    $pdf->Ln(14);

    $clave_split = str_split($cvecat, 3);
    $propietario_nombre = $data_cvecat['NOM_PROP']." ".$data_cvecat['APE_PAT']." ".$data_cvecat['APE_MAT'];
    $tip_pers = "";
    if($data_cvecat['TIP_PERS'] == 'F')
        $tip_pers = 'Persona física';
    else
        $tip_pers = 'Persona moral';
    
    $cod_pos = "";
    if($data_cvecat['COD_POS'] == null)
        $cod_pos = "S/N";
    else
        $cod_pos = $data_cvecat['COD_POS'];

    $servicios_explode = explode(",", $dataserv['servicios']);

   if(count($servicios_explode) < 9)
   {
       $faltantes = 9 - count($servicios_explode);
       for($i = 0; $i < $faltantes; $i++ ){
            array_push($servicios_explode, "");
       }
       
   }

    if(isset($_POST['img'])){
        $img_base64_encoded = $_POST['img'];
        $img = base64_decode(preg_replace('#^data:image/[^;]+;base64,#', '', $img_base64_encoded));
        $pdf->Image("@".$img, 150, 132, 50);
    }
    else{
        $pdf->Image('http://'.$_SERVER['SERVER_NAME'].'/assets/images/blank_map.png', 150, 132, 50);
        //$pdf->Image("@".$img, 145, 133, 60)
    }
    

    $html1 = '<table border="0" cellpadding="2" cellspacing="0" nobr="true">
    <tr>
        <td align="left" width="40%" style="display:flex; items-align:center;"><span style="font-size:8;">Clave catastral:</span></td>
        <td align="left" width="60%" style="display:flex; items-align:center;"><span style="font-size:8;">Titular: <strong>'.$propietario_nombre.'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="40%" style="display:flex; items-align:left;"><span style="font-size:14; font-weight:bold;">007&nbsp;&nbsp;&nbsp;'.$clave_split[0].'&nbsp;&nbsp;'.$clave_split[1].'&nbsp;&nbsp;'.$clave_split[2].'&nbsp;&nbsp;'.$clave_split[3].'&nbsp;&nbsp;'.$clave_split[4].'</span></td>
        <td align="left" width="60%" style="display:flex; items-align:left;"><span style="font-size:8;">Tipo de Persona: <strong>'.$tip_pers.'</strong></span>  <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Uso: <strong>'.$data_cvecat['DES_USO'].'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="40%" style="display:flex; items-align:left;"><span style="font-size:6;">Municipio &nbsp;&nbsp;&nbsp; Población &nbsp;&nbsp; Cuartel &nbsp;&nbsp;&nbsp;&nbsp;  Manzana  &nbsp;&nbsp;&nbsp;&nbsp; Predio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Unidad</span></td>
        <td align="left" width="60%" style="display:flex; items-align:left;"><span style="font-size:8;">Régimen: <strong>'.$data_cvecat['DES_REGIM'].'</strong></span></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Ubicación / Zona</span></td>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Para Notificación</span></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Asentamiento: <strong>'.$data_cvecat['NOM_COL'].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Asentamiento: <strong>'.$data_cvecat['NOM_COL_NO'].'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Calle: <strong>'.$data_cvecat['NOM_CALLE'].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Calle: <strong>'.$data_cvecat['UBI_PRED'].'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Código Postal: <strong>'.$cod_pos.'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Código zona: <strong> '.$datazona["CVE_ZONA"].' - '.$datazona["DES_ZONA"].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Código Postal: <strong>'.$data_cvecat['COD_POST_N'].'</strong> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Entrecalles: <strong> S/N </strong></span></span></td>
    </tr>
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Núm de Fte: <strong>'.$data_cvecat['NUM_FTES'].'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;  Metros de Fte: <strong> '.$data_cvecat["LON_FTE"].'</strong></span><span style="font-size:8;">&nbsp;&nbsp;&nbsp;  Valor de zona: <strong> '.$datazona["VAL_UNIT_T"].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Referencia: <strong> S/N </strong></span></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Registro Público de la Propiedad</span></td>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Libro: <strong>'.$data_cvecat['NUM_LIB'].'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Inscripción: <strong> '.$data_cvecat["NUM_INSC"].'</strong></span><span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Sección: <strong> '.$data_cvecat["NUM_SECC"].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Escritura: <strong>'.str_pad($data_cvecat['NUM_ESC'], 5, "0", STR_PAD_LEFT).'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Folio Real: <strong> - </strong></span></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Servicios</span></td>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"></td>
    </tr>

    <tr>
        <td align="left" width="40%" style="display:flex; items-align:left;"><span style="font-size:8;"> <strong> '.$servicios_explode[0].'    </strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <strong> '.$servicios_explode[6].'   </strong></span></td>
        <td align="left" width="60%" style="display:flex; items-align:left;"><span style="font-size:8;"> <strong> '.$servicios_explode[1].'        </strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <strong> '.$servicios_explode[2].'      </strong></span></td>
    </tr>
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;"><strong>  '.$servicios_explode[5].'   </strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <strong>  '.$servicios_explode[7].'  </strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;"> <strong>  '.$servicios_explode[3].'       </strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <strong>  '.$servicios_explode[8].'    </strong></span></td>
    </tr>
    
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Último movimiento</span></td>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"></td>
    </tr>

    <tr>
        <td align="left" width="100%" style="display:flex; items-align:left;"><span style="font-size:8;">Folio de trámite: <strong>  '.$folio.'   </strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fecha trámite: <strong>  '.strftime("%d/%B/%Y",strtotime($fecha_tramite)).'   </strong></span><span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Trámite: <strong>  '.$tramite.'    </strong></span></td>
        
    </tr>
    <tr>
        <td align="left" width="70%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Descripción:</span></td>
        <td align="center" width="30%" style="display:flex; items-align:center;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Croquis de ubicación:</span></td>
    </tr>
    <tr>
        <td align="left" width="65%" style="display:flex; items-align:left;"><span style="font-size:8;">'.$descripcion_tramite.'</span></td>
        <td align="left" width="35%" style="display:flex; items-align:left;"></td>
    </tr>
    <tr>
        <td></td><td></td>
    </tr>
    </table>
    ';
    $pdf->writeHTML($html1, true, false, false, false, '');
    $pdf->writeHTML('<p style="text-align:center; font-size:16px; "><b>Avalúo</b></p>
    ', true, false, false, false, '');

    //obtenemos los valores del terreno
    $sqlterr = '
    SELECT
        trr."CVE_CALLE",
        trr."CVE_TRAMO",
        trr."SUP_TERR",
        0.00 AS ValorUnitario1,
        0.00 AS DemeritoTramo1,
        trr."CVE_CALLE_",
        trr."CVE_TRAMO_",
        trr."SUP_TERR_E",
        0.00 AS ValorUnitario2,
        0.00 AS DemeritoTramo2,
        "SUP_TERR_D",
        CONCAT (
            "CVE_DEM_TE"::varchar(2),
            \'-\',
            "CVE_DEM_T2"::varchar(2),
            \'-\',
        "CVE_DEM_T3" :: VARCHAR ( 2 )) AS demeritos,
        "FAC_DEM_TE",
        0.00 AS ValorNeto 
    FROM
        a_terr AS trr 
    WHERE
        trr.clave = \''.$cvecat.'\' 
    ORDER BY
        "NUM_TERR"
    ';
    $estaterr = $pdo->prepare($sqlterr);
    $estaterr->execute();
    $dataterr = $estaterr->fetchAll(PDO::FETCH_ASSOC);
    $terrenos = "";
    $sup_total = 0.0;
    $valor_total = 0.0;
    foreach ($dataterr as $terr){
    //busqueda de tramos
    $ValorTramoNormal = 0;
    $Dem1 = 0.0;
    $ValorTramoEsquina = 0;
    $Dem2 = 0.0;
    //Valores del terreno 
    $num_zona = $datazona["CVE_ZONA"];
    $valorzona = $datazona["VAL_UNIT_T"];

    //calculamos el valor del unitario general
    $sqltramo1 = 'SELECT * FROM a_tramo WHERE "CVE_POBL"=\''.$data_cvecat["CVE_POBL"].'\' AND "CVE_CALLE"=\''.$terr['CVE_CALLE'].'\' AND "CVE_TRAMO"=\''.$terr['CVE_TRAMO'].'\'';
    $estadotramo1 = $pdo->prepare($sqltramo1);
    $estadotramo1->execute();
    $datatramo1 = $estadotramo1->fetch(PDO::FETCH_ASSOC);
    if ($datatramo1 != null)
    {
        $ValorTramoNormal = $datatramo1["VAL_UNIT_T"];
        $Dem1 = 100 * (1 - $datatramo1["FAC_DEM_VA"]);
    }
    else{
        $ValorTramoNormal = $valorzona;
    }

    //calculamos el valor del unitario de la esquina
    $sqltramo2 = 'SELECT * FROM a_tramo WHERE "CVE_POBL"=\''.$data_cvecat["CVE_POBL"].'\' AND "CVE_CALLE"=\''.$terr['CVE_CALLE_'].'\' AND "CVE_TRAMO"=\''.$terr['CVE_TRAMO_'].'\'';
    $estadotramo2 = $pdo->prepare($sqltramo2);
    $estadotramo2->execute();
    $datatramo2 = $estadotramo2->fetch(PDO::FETCH_ASSOC);
    if ($datatramo2 != null)
    {
        $ValorTramoEsquina = $datatramo2["VAL_UNIT_T"];
        $Dem2 = 100 * (1 - $datatramo2["FAC_DEM_VA"]);
    }
    else{
        $ValorTramoEsquina = $valorzona;
    }


    $valorneto = ($terr["SUP_TERR"] * $ValorTramoNormal * ((100 - $Dem1) / 100) ) + ($terr["SUP_TERR_E"] * 0.25 * $ValorTramoEsquina * ((100 - $Dem2) / 100) ) - ($terr["SUP_TERR_D"]  * $ValorTramoNormal * $terr["FAC_DEM_TE"]);

    $sup_total += $terr["SUP_TERR"];
    $valor_total += $valorneto;

    $terrenos .= '<tr style="border-style: dotted;">
    <td align="center" style="font-size:8px; border-left-style: solid;">'.$terr["CVE_CALLE"].' </td>
    <td align="center" style="font-size:8px; ">'.$terr["CVE_TRAMO"].' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["SUP_TERR"].'</td>
    <td align="center" style="font-size:8px;  ">'.$fmt->formatCurrency($ValorTramoNormal,"MXN").' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["CVE_CALLE_"].' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["CVE_TRAMO_"].' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["SUP_TERR_E"].' </td>
    <td align="center" style="font-size:8px;  ">'.$fmt->formatCurrency($ValorTramoEsquina,"MXN").' </td>

    <td align="center" style="font-size:8px;  ">'.$terr["SUP_TERR_D"].' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["demeritos"].' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["FAC_DEM_TE"].' </td>
    <td align="center" style="font-size:8px; border-right-style: solid;">'.$fmt->formatCurrency($valorneto,"MXN").' </td>
    </tr>';
    }  

    $tblterreno = '<span style="font-size:9; color:#632B2B; font-weight:bold;">Datos de Terreno:</span><br><table border="0" cellpadding="2" cellspacing="0" nobr="false">
    <tr style="background-color:#e3e3e3;">
    <td colspan="4" align="center" style="font-size:8px;  border-style: solid; border-style: solid; border-left-style: solid;">G E N E R A L</td>
    <td colspan="4" align="center" style="font-size:8px;  border-style: solid; border-left-style: solid; border-right-style: solid;">INCREMENTO POR ESQUINA (25%)</td>
    <td colspan="4" align="center" style="font-size:8px;  border-style: solid; border-style: solid; border-right-style: solid; border-left-style: solid;">D E M E R I T O</td>
    </tr>
    <tr style="border-style: solid; background-color:#e3e3e3;">
    <td align="center" style="font-size:6px; border-style: solid; border-left-style: solid;  border-bottom-style: solid;">Calle</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Tramo</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Sup M2</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Valor Unitario</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Calle</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Tramo</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Superficie M2</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Valor Unitario</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Superficie M2</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">CV1-CV2-CV3</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">% dem</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid; border-right-style: solid;">Valor Neto</td>
    </tr>
    '.$terrenos.'
    <tr>
    <td colspan="14" style="font-size:8px;  border-left-style: solid; border-right-style: solid; border-bottom-style: solid;"></td>
    </tr>
    <tr>
    <td colspan="2" style="font-size:8; color:#632B2B; font-weight:bold;">Total :</td>
    <td style="font-size:9px; font-weight:bold;">'.$sup_total.'</td>
    <td colspan="3" style="font-size:8px;"> </td>
    <td colspan="4" style="font-size:8; color:#632B2B; font-weight:bold;  text-align:right;" >Valor Catastral del Terreno :</td>
    <td colspan="2" style="font-size:9px; text-align:right; font-weight:bold;" >'.$fmt->formatCurrency($valor_total,"MXN").'</td>
    </tr>

    </table>
    ';
    $pdf->writeHTML($tblterreno, true, false, false, false, '');


    //empiezo a calcular renglones de construccion
    $SuperficieConstruccionFormateado = 0;
    $ValorConstruccionFormateado = 0;
    $ValorCatastralFormateado = 0;

    $sqlconst = 'SELECT cons."CVE_CAT_CO", cons."SUP_CONST", 
                    0.00 as ValorUnitario, cons."CVE_EDO_CO", 
                    cons."EDD_CONST", 0.00 as FactorDemerito, 
                    1.00 as FactorComercializacion, 0.00 as ValorNeto 
                    FROM a_const AS cons 
                    WHERE cons.clave=\''.$cvecat.'\' ORDER BY "NUM_CONST"';

    $estaconst = $pdo->prepare($sqlconst);
    $estaconst->execute();
    $dataconst = $estaconst->fetchAll(PDO::FETCH_ASSOC);

    $construcciones = "";
    $Categoria = "";
    $Edad = 0;
    $estado = 0;
    foreach ($dataconst as $const){
    //'busquedas de categorias
    $Categoria = $const["CVE_CAT_CO"];
    $Superficie = $const["SUP_CONST"];
    $Edad = $const["EDD_CONST"];
    $estado = $const["CVE_EDO_CO"];
    $valorunitario = $const["valorunitario"];
    $factdem = $const["factordemerito"];
    $factcom = $const["factorcomercializacion"];
    $valorneto = $const["valorneto"];
    //buscamos el valor del factor de demerito de la construccion
    $sqldemc = 'SELECT * FROM a_demc WHERE "CVE_CAT_CO" =\''.$Categoria.'\' AND "CVE_EDO_CO"= '.$estado.' AND "EDD_CONST"= '.$Edad;
    $estadodemc = $pdo->prepare($sqldemc);
    $estadodemc->execute();
    $datademc = $estadodemc->fetch(PDO::FETCH_ASSOC);
    if ($datademc != null)
    {
        $factdem = $datademc["FAC_DEM_CO"];
    }
    //Buscamos el valor unitario de la construccion
    $sqlcatco = 'SELECT * FROM a_cat_mp WHERE "CVE_CAT_CO"=\''.$Categoria.'\'';
    $estadocatco = $pdo->prepare($sqlcatco);
    $estadocatco->execute();
    $datacatco = $estadocatco->fetch(PDO::FETCH_ASSOC);
    if ($datacatco != null)
    {
        $valorunitario = $datacatco["val_unit_c"];
    }
    //buscamos el valor del factor de comercializacion
    $sqlfaccom = 'SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL" ='.$data_cvecat['CVE_POBL'];
    $estadofaccom = $pdo->prepare($sqlfaccom);
    $estadofaccom->execute();
    $datafaccom = $estadofaccom->fetch(PDO::FETCH_ASSOC);
    if ($datafaccom != null)
    {
        $factcom = $datafaccom["FAC_COM"];
    } 

    //calcular el valorneto por cobnstruccion
    $valorneto = $Superficie * $valorunitario * $factdem * $factcom;
    $construcciones .= '<tr style="border-style: dotted;">
    <td align="center" style="font-size:8px; border-left-style: solid;">'.$Categoria.' </td>
    <td align="center" style="font-size:8px; ">'.$Superficie.' </td>
    <td align="center" style="font-size:8px;  ">'.$fmt->formatCurrency($valorunitario,"MXN").' </td>
    <td align="center" style="font-size:8px;  ">'.$estado.' </td>
    <td align="center" style="font-size:8px;  ">'.$Edad.' </td>
    <td align="center" style="font-size:8px;  ">'.$factdem.' </td>
    <td align="center" style="font-size:8px;  ">'.$factcom.' </td>
    <td align="center" style="font-size:8px; border-right-style: solid;">'.$fmt->formatCurrency($valorneto,"MXN").' </td>
    </tr>';

    $SuperficieConstruccionFormateado += $Superficie;
    $ValorConstruccionFormateado += $valorneto;
    }

    //valor catastral del predio total de construccion mas total del terreno
    $ValorCatastralFormateado = $ValorConstruccionFormateado + $valor_total;

    $tblconst = '<span style="font-size:9; color:#632B2B; font-weight:bold;">Datos de Construcción:</span><br><table border="0" cellpadding="1" cellspacing="0" nobr="false">
    <tr style="border-style: solid; background-color:#e3e3e3;">
    <td align="center" style="font-size:7px; border-style: solid; border-left-style: solid;  border-bottom-style: solid;">Categoría</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Superficie M2</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Valor Unitario</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Estado</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Edad</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Fact. Dem.</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Fact. Com.</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid; border-right-style: solid;">Valor Neto ($)</td>
    </tr>
    '.$construcciones.'
    <tr>
    <td colspan="8" style="font-size:8px;  border-left-style: solid; border-right-style: solid; border-bottom-style: solid;"></td>
    </tr>
    <tr>
    <td colspan="1" style="font-size:8; color:#632B2B; font-weight:bold;">Total :</td>
    <td style="font-size:9px; font-weight:bold;" align="center">'.$SuperficieConstruccionFormateado.'</td>
    <td colspan="1" style="font-size:10px;"> </td>
    <td colspan="4" style="font-size:8; color:#632B2B; text-align:right; font-weight:bold;" >Valor Catastral de la Construcción :</td>
    <td colspan="2" style="font-size:9px; font-weight:bold; text-align:right;" >'.$fmt->formatCurrency($ValorConstruccionFormateado,"MXN").'</td>
    </tr>

    </table>
    ';
    $pdf->writeHTML($tblconst, true, false, false, false, '');

    $ValorAnterior = 0.0;
    $ValorAnterior = $data_cvecat['VAL_TERR_A'] + $data_cvecat['VAL_CONST_'];
    $pdf->SetAutoPageBreak(TRUE, 0);

    $tablefooter = '
    <table border="0" cellpadding="2" cellspacing="0" nobr="false">
        <tr>
            <td align="center" width="15%" rowspan="5"></td>
            <td align="left" width="85%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:7; text-align:justify;"><b>Sello digital del trámite :</b> '.base64_encode($signature).'</span></td>
        </tr>

        <tr>
            <td align="left" width="30%" style="display:flex; items-align:center;"><span style="font-size:8;"><b>Autorizado por: </b></span></td>
            <td align="left" width="50%" style="display:flex; items-align:center;"><span style="font-size:8;">Director:  <strong>'.$data_firma['director'].'</strong></span></td>
        </tr>

        <tr>
            <td align="left" width="85%" style="display:flex; items-align:center;"><span style="font-size:8; font-weight:bold;">Valor catastral del predio:</span></td>
        </tr>

        <tr>
            <td align="left" width="30%" style="display:flex; items-align:center;"><span style="font-size:8;">Valor Anterior ($): <strong>'.$fmt->formatCurrency($ValorAnterior,"MXN").'</strong></span></td>
            <td align="center" width="25%" style="display:flex; items-align:center;"><span style="font-size:8;">Efectos : <strong>TRIMESTRE '.$data_cvecat['TRI_EFEC'].'/'.$data_cvecat['ATO_EFEC'].'</strong> </span></td>
            <td align="right" width="30%" style="display:flex; items-align:center;"><span style="font-size:8;">Valor actual : <strong style="font-size:10;">'.$fmt->formatCurrency($ValorCatastralFormateado,"MXN").'</strong> </span></td>
        </tr>
        
    </table>
    <pre>Fecha de Impresión: '.date("m/d/Y h:i:s a", time()).'</pre>
    ';

    $style = array(
        'border' => 1,
        'padding' => 1,
        'fgcolor' => array(0,0,0),
        'bgcolor' => false
    );
    $url_qr = 'http://'.$_SERVER['SERVER_NAME'].'/reportes/pdfavaluo.php?id='.$cvecat;

    $pdf->write2DBarcode($url_qr, 'QRCODE,H',14,235, 28, 28, $style, 'N');

    $pdf->setInfo($tablefooter);

    //el QR en la esquina
    //pie de la pagina
    // new style
    

    //Close and output PDF document
    $pdf->Output('cedula_catastral_'.$folio.'.pdf', 'I');

}
else
    echo "No se enviaron parametros";

?>