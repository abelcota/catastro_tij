function creaTablaDynamica(tabla, idTabla, campos, ini, fin) {
    var col = [];
    for (var key in tabla[0]) {
        col.push(key);
    }
    var table = document.getElementById(idTabla);
    table.innerHTML = "";
    if (tabla.length == 0) {
        return false;
    }
    var header = table.createTHead();
    var tr = header.insertRow(-1);
    if (ini == 1) {
        var th = document.createElement("th");
        th.innerHTML = "";
        tr.appendChild(th);
    }
    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");
        if (campos != null) {
            if (campos[col[i]] != undefined) {
                th.innerHTML = campos[col[i]];
                tr.appendChild(th);
            }
        } else {
            th.innerHTML = col[i];
            tr.appendChild(th);
        }
    }
    if (fin == 1) {
        var th = document.createElement("th");
        th.innerHTML = "";
        tr.appendChild(th);
    }
    var tbody = document.createElement("tbody")
    table.appendChild(tbody);
    for (var i = 0; i < tabla.length; i++) {
        tr = tbody.insertRow(-1);
        if (ini == 1) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = "";
        }
        for (var j = 0; j < col.length; j++) {
            if (campos != null) {
                if (campos[col[j]] != undefined) {
                    var tabCell = tr.insertCell(-1);
                    tabCell.innerHTML = tabla[i][col[j]];
                }
            } else {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = tabla[i][col[j]];
            }
        }
        if (fin == 1) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = "";
        }
    }
}

function creaTablaDynamicaCamposVacios(tabla, idTabla, campos, ini, fin) {
    var col = [];
    for (var key in campos) {
        col.push(key);
    }
    var table = document.getElementById(idTabla);
    table.innerHTML = "";
    var header = table.createTHead();
    var tr = header.insertRow(-1);
    if (ini == 1) {
        var th = document.createElement("th");
        th.innerHTML = "";
        tr.appendChild(th);
    }
    for (var i = 0; i < col.length; i++) {
        var th = document.createElement("th");
        if (campos != null) {
            if (campos[col[i]] != undefined) {
                th.innerHTML = campos[col[i]];
                tr.appendChild(th);
            }
        } else {
            th.innerHTML = col[i];
            tr.appendChild(th);
        }
    }
    if (fin == 1) {
        var th = document.createElement("th");
        th.innerHTML = "";
        tr.appendChild(th);
    }
    if (tabla.length == 0) {
        return false;
    }
    var tbody = document.createElement("tbody")
    table.appendChild(tbody);
    for (var i = 0; i < tabla.length; i++) {
        tr = tbody.insertRow(-1);
        if (ini == 1) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = "";
        }
        for (var j = 0; j < col.length; j++) {
            if (campos != null) {
                if (campos[col[j]] != undefined) {
                    var tabCell = tr.insertCell(-1);
                    tabCell.innerHTML = tabla[i][col[j]];
                }
            } else {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = tabla[i][col[j]];
            }
        }
        if (fin == 1) {
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = "";
        }
    }
}

function aPesos(valor) {
    return "$" + valor.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
}

function formatNumber(n) {
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

function formatCurrency(input, blur) {
    var input_val = input.val();
    if (input_val === "") {
        return;
    }
    var original_len = input_val.length;
    var caret_pos = input.prop("selectionStart");
    if (input_val.indexOf(".") >= 0) {
        var decimal_pos = input_val.indexOf(".");
        var left_side = input_val.substring(0, decimal_pos);
        var right_side = input_val.substring(decimal_pos);
        left_side = formatNumber(left_side);
        right_side = formatNumber(right_side);
        if (blur === "blur") {
            right_side += "00";
        }
        right_side = right_side.substring(0, 2);
        input_val = "$" + left_side + "." + right_side;
    } else {
        input_val = formatNumber(input_val);
        input_val = "$" + input_val;
        if (blur === "blur") {
            input_val += ".00";
        }
    }
    input.val(input_val);
    var updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
}