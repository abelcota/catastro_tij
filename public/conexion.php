<?php
    setlocale(LC_TIME, "spanish");
    date_default_timezone_set('America/Mazatlan');
    $servidor = "localhost"; //localhost
    //$servidor = "catastro_postgres"; //docker
    $puerto = "5432";
    $usuario = "postgres";
    $contra = "postgres";
    $basedatos = "catastro_tij";
    //Creamos la cadena de conexion para conectar al servidor
    $cadena = "pgsql:host=$servidor;port=$puerto;dbname=$basedatos;user=$usuario;password=$contra";
    if ( (! empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') ||
     (! empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ||
     (! empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') ) {
    $protocole = 'https://';
    } else {
        $protocole = 'http://';
    }

    $host = $_SERVER['HTTP_HOST'] . '/';
    $baseurl = $protocole . $host;
    $myappBaseUrl = $baseurl;
    //intentamos conectarnos al servidor de bd
    try
    {
        //creamos la conexion con la base datos
        $pdo = new PDO($cadena);
        //comprobar si se realizo la conexion
        if($pdo == true)
        {
            //echo 'Conexion estblecida correctamente';
        }
    }
    catch(PDOException $error)
    {
        //mostramos el error que se produjo al intentar conectar
        echo $error->getMessage();
    }
?>