<?php
include("../conexion.php");

$u = isset($_GET['user']) ? $_GET['user'] : "";
$p = isset($_GET['password']) ? $_GET['password'] : "";

$query = "SELECT u.id, u.email, u.username, u.password_hash, u.active, g.name as rol, u.privilegio_visor FROM users u
INNER JOIN auth_groups_users aug on u.id = aug.user_id
RIGHT JOIN auth_groups g on aug.group_id = g.id
WHERE u.email = '$u' OR u.username = '$u';";
$estado= $pdo->prepare($query);
$estado->execute();

$res = "";

if ($estado->rowCount() > 0){
    $data = $estado->fetchAll();

    //verificar la contraseña
    $result = password_verify(base64_encode(
        hash('sha384', $p, true)
    ), $data[0]['password_hash']);

    //si la contraseña es correcta mandamos un json con los datos
    if($result){
        //si tiene mas de un resultado quiere decir que esta en mas de un rol
        if ($estado->rowCount() > 1)
        {
            
        }

        $results = array(
            "status"  => 1,
            "message" => "usuario identificado",
            "data" => $data
        );

    }else{
        //si no es correcta la contraseña se manda un mensaje de error
        $results = array(
            "status"  => 2,
            "message" => "password incorrecto",
            "data" => null
        );
    }   

}
else{
    $results = array(
        "status"  => 0,
        "message" => "usuario no existe",
        "data" => null
    );
}
    
header('Content-Type: application/json');
echo json_encode($results, JSON_PRETTY_PRINT);


?>