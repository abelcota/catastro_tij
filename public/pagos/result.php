<?php
    require('../Bbva/Bbva.php');
    require('../conexion.php');
    require('enviar_email.php');


    $id = $_GET['id'];
    $state = "N";
    try {
        
    Bbva::setProductionMode(false);
    $bbva = Bbva::getInstance('mydukacuzwweg11tfj5r', 'sk_0086917f34704797be1a9f7bf5117de2');

    $charge = $bbva->charges->get($id);
    
    if($charge->status == 'failed'){
        //actualizar el estatus de la solicitud
        $quer = "DELETE FROM perito_infcat WHERE cargo = '$charge->id'; DELETE FROM perito_infcat_detalle WHERE informacion_id = '$charge->order_id'";
        $estad= $pdo->prepare($quer);
        $estad->execute();
        
    }
    else{
        //obtener la informacion del cargo para enviar correo y actualizar
        $select = "SELECT * FROM perito_infcat WHERE cargo = '$charge->id';";
        $estad_select= $pdo->prepare($select);
        $estad_select->execute();
        $data_select = $estad_select->fetch(\PDO::FETCH_ASSOC);

        if($data_select['estatus'] != 'completed'){
            //actualizar el estatus de la solicitud
            $quer = "UPDATE perito_infcat SET estatus = '$charge->status' WHERE cargo = '$charge->id';";
            $estad= $pdo->prepare($quer);
            $estad->execute();
            $state = "Y";
        }

        
        

    }
    

    } catch (BbvaApiTransactionError $e) {
        error_log('ERROR on the transaction: ' . $e->getMessage() . 
            ' [error code: ' . $e->getErrorCode() . 
            ', error category: ' . $e->getCategory() . 
            ', HTTP code: '. $e->getHttpCode() . 
            ', request ID: ' . $e->getRequestId() . ']', 0);

    } catch (BbvaApiRequestError $e) {
        error_log('ERROR on the request: ' . $e->getMessage(), 0);

    } catch (BbvaApiConnectionError $e) {
        error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);

    } catch (BbvaApiAuthError $e) {
        error_log('ERROR on the authentication: ' . $e->getMessage(), 0);
        
    } catch (BbvaApiError $e) {
        error_log('ERROR on the API: ' . $e->getMessage(), 0);
        
    } catch (Exception $e) {
        error_log('Error on the script: ' . $e->getMessage(), 0);
    }

    

?>
<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $titulo; ?> Pagos Catastro | Sistema Administrativo Catastral Culiacán</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Sistema de Catastro Culiacán" name="description" />
    <meta content="Direccion de Catastro Municipal Culiacan" name="author" />
    <!-- App favtype -->
    <link rel="shortcut icon" href="../assets/images/favicon.ico">

        <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Georama:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="../assets/css/icons.min.css" rel="stylesheet" type="text/css">
     <!-- Select 2 -->
     <link href="../assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
     <link href="../assets/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap Css -->
    <link href="../assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="../assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
     <!-- DataTables -->
     <link href="../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="../assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
     <!-- Sweet Alert-->
     <link href="../assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
     <!-- open Layers -->
     <link rel="stylesheet" href="../assets/css/ol.css" type="text/css">
     <link rel="stylesheet" href="../assets/css/ol-layerswitcher.css" />

     <link href="../assets/css/ol-contextmenu.min.css" rel="stylesheet">
    <!-- Lightbox css -->
    <link href="../assets/libs/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
    <style>
        /*body {
            background: linear-gradient(rgba(255, 255, 255, 0.95),
            rgba(255, 255, 255, 0.9)),url(../assets/images/fondo2.png");?>);
            background-attachment: fixed;
            background-size: cover;
            padding: 1em 0;
      
        }*/

        .nav-pills > li > a.active {
                background-color: #480912!important;
            }
    </style>
</head>

<body data-layout="detached" data-topbar="colored">

    <div class="container-fluid">
        <!-- Begin page -->
        <div id="layout-wrapper">  
            <header id="page-topbar">
                <div class="navbar-header" style="height:80px !important;">
                    <div class="container-fluid">
                        
                        <div>
                            <!-- LOGO -->
                            <div class="navbar-brand-box">
                                <a href="index.html" class="logo logo-dark">
                                    <span class="logo-sm">
                                        <img src="../assets/images/logo_hw.png" alt="" height="20">
                                    </span>
                                    <span class="logo-lg">
                                        <img src="../assets/images/logo_hw.png" alt="" height="80">
                                    </span>
                                </a>

                                <a href="index.html" class="logo logo-light">
                                    <span class="logo-sm">
                                        <img src="../assets/images/logo_hw.png" alt="" height="20">
                                    </span>
                                    <span class="logo-lg">
                                        <img src="../assets/images/logo_hw.png" alt="" height="80">
                                    </span>
                                </a>
                            </div>                            

                            
                        </div>

                    </div>
                </div>
            </header>

            <div class="main-content">

                <div class="page-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="invoice-title">
                                        <h4 class="float-right font-size-16 text-right">Solicitud # <br> <span class="text-muted"><?php echo $charge->id; ?></span></h4>
                                        <div class="mb-4">
                                        <?php if($charge->status == 'completed') : ?>
                                        <a href="" class="text-reset notification-item">
                                            <div class="media">
                                                <div class="avatar-xs mr-3">
                                                    <span class="avatar-title bg-success rounded-circle font-size-16">
                                                        <i class="bx bx-check"></i>
                                                    </span>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="mt-0 mb-1"><?php echo $charge->description; ?></h6>
                                                    <div class="font-size-12 text-muted">
                                                        <p class="mb-1">La Solicitud de Informacion Catastral Urbana fue completada exitosamente</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <?php else : ?>
                                            <a href="" class="text-reset notification-item">
                                            <div class="media">
                                                <div class="avatar-xs mr-3">
                                                    <span class="avatar-title bg-danger rounded-circle font-size-16">
                                                        <i class="bx bx-x"></i>
                                                    </span>
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="mt-0 mb-1"><?php echo $charge->description; ?></h6>
                                                    <div class="font-size-12 text-muted">
                                                        <p class="mb-1">La Solicitud de Informacion Catastral Urbana fue rechazada</p>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                        <?php endif; ?>

                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <?php echo $res->$error . " " . $res->message; ?>
                                        <div class="col-6">
                                            <address>
                                                    <strong>Inforación del Solicitante:</strong><br>
                                                    <?php echo strtoupper($charge->customer->name); ?><br>
                                                    <?php echo strtoupper($charge->customer->last_name); ?><br>
                                                    
                                                    
                                                </address>
                                        </div>
                                        <div class="col-6 text-right">
                                            <address>
                                                    <strong>Contacto del Solicitante:</strong><br>
                                                    <?php echo strtoupper($charge->customer->email); ?><br>
                                                    <?php echo strtoupper($charge->customer->phone_number); ?>
                                                </address>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-6 mt-3">
                                            <address>
                                                    <strong>Metodo de Pago:</strong><br>
                                                    <?php echo strtoupper($charge->card->type . " ".$charge->card->brand . " " .$charge->card->card_number); ?> <br>
                                                    <?php echo strtoupper($charge->card->holder_name); ?>
                                                </address>
                                        </div>
                                        <div class="col-6 mt-3 text-right">
                                            <address>
                                                    <strong>Fecha de Solicitud:</strong><br>
                                                    <?php echo strftime('%e de %B de %Y',strtotime($charge->operation_date)); ?><br><br>
                                                </address>
                                        </div>
                                    </div>
                                    <?php if($charge->status == 'completed') : ?>
                                    <div class="py-2 mt-3">
                                        <h3 class="font-size-15 font-weight-bold">Resumen de la Solicitud <span class="text-muted">[<?php echo $charge->id; ?>]</span></h3>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="width: 70px;">ID</th>
                                                    <th>Concepto</th>
                                                    <th>Clave Catastral</th>
                                                    <th class="text-right">Importe</th>
                                                    <th class="text-right d-print-none">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    //incluir los datos de la solicitud
                                                    $query = "SELECT pid.* FROM perito_infcat_detalle pid INNER JOIN perito_infcat pi on pid.informacion_id = pi.informacion_id WHERE cargo = '$charge->id'";
                                                    $estado= $pdo->prepare($query);
                                                    $estado->execute();
                                                    $resultado = $estado->fetchAll();
                                                    $body = "";
                                                    $total = 0;
                                                    $sol = "";

                                                    foreach($resultado as $row){
                                                        $body .=  '
                                                            <tr>
                                                                <td class="text-primary">'.$row['uuid'].'</td>
                                                                <td class="">'.$row['concepto'].'</td>
                                                                <td>'.$row['clave_catastral'].'</td>
                                                                <td class="text-right">$ '.$row['importe'].'</td>
                                                                <td class="text-right d-print-none"><a href="'.$myappBaseUrl.'pagos/printpdf.php?uuid='.$row['uuid'].'" class="btn btn-info" target="_new"><i class="mdi mdi-download"></i> Descargar PDF</a></td>
                                                            </tr>
                                                        ';
                                                        $total += $row['importe'];
                                                       
                                                    }
                                                    $body .=  '
                                                            <tr>
                                                            <td class="text-primary"></td>
                                                                <td class=""></td>
                                                                <td class="text-right">TOTAL: </td>
                                                                <td class="text-right"><b>$ '.$total.'</b></td>
                                                                <td class="text-right d-print-none"></td>
                                                            </tr>
                                                            ';
                                                    echo $body;
                                                    if($state == "Y"){
                                                        //enviar el correo
                                                        $res = do_SendMail($charge->customer->email, $charge->id, $body);
                                                    }

                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="d-print-none">
                                        <div class="float-right">
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light"><i class="mdi mdi-printer"></i> Imprimir</a>
                                            <a href="../pagos" class="btn btn-primary w-md waves-effect waves-light"><i class="mdi mdi-arrow-left"></i> Regresar</a>
                                        </div>
                                    </div>
                                    <?php else : ?>
                                        <div class="py-2 mt-3 text-center">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <i class="mdi mdi-block-helper mr-2"></i> <?php echo $charge->error_message; ?>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                            <a class="btn btn-info font-size-20" href="../pagos" ><span class="mdi mdi-arrow-left-bold-outline">Regresar</a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php //echo json_encode((array) $charge); ?>
                    </div>
                    <div>
        </div>
        </div>
    </div>
</body>
<!-- JAVASCRIPT -->
<script src="../assets/libs/jquery/jquery.min.js"></script>
    
    <script src="../assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../assets/libs/metismenu/metisMenu.min.js"></script>
    <script src="../assets/libs/simplebar/simplebar.min.js"></script>
    <script src="../assets/libs/node-waves/waves.min.js"></script>
    <script src="../assets/js/moment.min.js"></script>


     
    <!-- Sweet Alerts js -->
    <script src="../assets/libs/sweetalert2/sweetalert2.min.js"></script>

    <!-- Select 2 js -->
    <script src="../assets/libs/select2/js/select2.min.js"></script>

    <!-- App js -->
    <script src="../assets/js/app.js"></script>
     <!-- Required datatable js -->
    <script src="../assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <script>

        // A $( document ).ready() block.
        $( document ).ready(function() {
            
        });
        
    </script>
</html>