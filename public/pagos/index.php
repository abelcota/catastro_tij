<?php

require('../Bbva/Bbva.php');

$bbva = Bbva::getInstance('mydukacuzwweg11tfj5r', 'sk_0086917f34704797be1a9f7bf5117de2');

function guidv4($data = null)
{
    // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
    $data = $data ?? random_bytes(16);
    assert(strlen($data) == 16);

    // Set version to 0100
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    // Set bits 6-7 to 10
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

    // Output the 36 character UUID.
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

/*if(!isset($_GET['s']))
    $myuuid = $_GET['s'];
  */
$myuuid = guidv4();

?>

<!doctype html>
<html lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Pagos Catastro | Sistema Administrativo Catastral Culiacán</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Sistema de Catastro Culiacán" name="description" />
    <meta content="Direccion de Catastro Municipal Culiacan" name="author" />
    <!-- App favtype -->
    <link rel="shortcut icon" href="../assets/images/favicon.ico">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Georama:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <link href="../assets/css/icons.min.css" rel="stylesheet" type="text/css">
    <!-- Select 2 -->
    <link href="../assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap Css -->
    <link href="../assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="../assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
    <!-- DataTables -->
    <link href="../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="../assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Sweet Alert-->
    <link href="../assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <!-- open Layers -->
    <link rel="stylesheet" href="../assets/css/ol.css" type="text/css">
    <link rel="stylesheet" href="../assets/css/ol-layerswitcher.css" />

    <link href="../assets/css/ol-contextmenu.min.css" rel="stylesheet">
    <!-- Lightbox css -->
    <link href="../assets/libs/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />
    <style>
        /*body {
            background: linear-gradient(rgba(255, 255, 255, 0.95),
            rgba(255, 255, 255, 0.9)),url(../assets/images/fondo2.png");?>);
            background-attachment: fixed;
            background-size: cover;
            padding: 1em 0;
      
        }*/

        .nav-pills>li>a.active {
            background-color: #480912 !important;
        }
    </style>
</head>

<body data-layout="detached" data-topbar="colored">

    <div class="container-fluid">
        <!-- Begin page -->
        <div id="layout-wrapper">
            <header id="page-topbar">
                <div class="navbar-header" style="height:80px !important;">
                    <div class="container-fluid">

                        <div>
                            <!-- LOGO -->
                            <div class="navbar-brand-box">
                                <a href="index.html" class="logo logo-dark">
                                    <span class="logo-sm">
                                        <img src="../assets/images/logo_hw.png" alt="" height="20">
                                    </span>
                                    <span class="logo-lg">
                                        <img src="../assets/images/logo_hw.png" alt="" height="80">
                                    </span>
                                </a>

                                <a href="index.html" class="logo logo-light">
                                    <span class="logo-sm">
                                        <img src="../assets/images/logo_hw.png" alt="" height="20">
                                    </span>
                                    <span class="logo-lg">
                                        <img src="../assets/images/logo_hw.png" alt="" height="80">
                                    </span>
                                </a>
                            </div>


                        </div>

                    </div>
                </div>
            </header>

            <div class="main-content">

                <div class="page-content">

                    <!-- start page title 
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Información catastral urbana y rústica  
                                </h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">MODULO</a></li>
                                        <li class="breadcrumb-item active">PAGOS</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                     end page title -->
                    <div class="row">

                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="page-title mb-0 font-size-18">Solicitud de Información Catastral Urbana
                                    </h4>

                                    <pre class="text-secondary"><?php if (Bbva::getProductionMode()) {
                                                                    echo "Produccion";
                                                                } else {
                                                                    echo "Sandbox";
                                                                }
                                                                ?></pre>
                                    <form id="form_agregar">
                                        <input type="hidden" value="<?php echo $myuuid; ?>" id="uuid">
                                        <input data-val="true" data-val-number="El campo Items debe ser un número." data-val-required="El campo Items es obligatorio." id="Items" name="Items" type="hidden" value="0">

                                        <input data-val="true" data-val-number="El campo Total debe ser un número." data-val-required="El campo Total es obligatorio." id="Total" name="Total" type="hidden" value="0">

                                        <input id="Claves" name="Claves" type="hidden" value="">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label for="Nombre">NOMBRE COMPLETO:</label>
                                                <div class="input-group">

                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="mdi mdi-account"></i></span>
                                                    </div>
                                                    <input class="form-control" data-val="true" data-val-required="El campo Nombre es obligatorio." id="Nombre" name="Nombre" placeholder="NOMBRE COMPLETO" style=" text-transform: uppercase" type="text" value="" required>
                                                    <div class="invalid-tooltip">
                                                        Por favor ingrese un Nombre.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="RFC">REGISTRO FEDERAL DE CONTRIBUYENTES (RFC):</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="mdi mdi-account-badge-horizontal-outline"></i></span>
                                                    </div>
                                                    <input class="form-control" data-val="true" data-val-required="El campo Rfc es obligatorio." id="RFC" name="Rfc" placeholder="RFC" style="text-transform: uppercase" type="text" value="">
                                                    <div class="invalid-tooltip">
                                                        Por favor ingrese un RFC.
                                                    </div>
                                                </div>
                                                <span class="error-form" id="RFC_error_message" style="color: red; display: none;"></span>
                                            </div>
                                            <div class="col-lg-6 mt-2">
                                                <label for="Correo">CORREO ELECTRÓNICO:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="mdi mdi-email"></i></span>
                                                    </div>
                                                    <input class="form-control" data-val="true" data-val-required="El campo Nombre es obligatorio." id="Correo" name="Correo" placeholder="correo@correo.com" type="email" value="">
                                                    <div class="invalid-tooltip">
                                                        Por favor ingrese un Correo.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 mt-2">
                                                <label for="Telefono">TELÉFONO:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="mdi mdi-cellphone"></i></span>
                                                    </div>
                                                    <input class="form-control" data-val="true" data-val-required="El campo Rfc es obligatorio." id="Telefono" name="Telefono" placeholder="0000000000" style="text-transform: uppercase" type="text" value="">
                                                    <div class="invalid-tooltip">
                                                        Por favor ingrese un Telefono.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 mt-2">
                                                <label for="ClaveCatastral1">CLAVE CATASTRAL:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="mdi mdi-map-legend"></i></span>
                                                    </div>
                                                    <input class="form-control" id="cc1" name="ClaveCatastral1" placeholder="(solo números, sin guiones)" style="text-transform: uppercase" type="number" value="">
                                                    <div class="invalid-tooltip">
                                                        Por favor ingrese una Clave.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 mt-2">
                                                <label for="ClaveCatastral2">CONFIRMAR CLAVE CATASTRAL:</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="mdi mdi-map-legend"></i></span>
                                                    </div>
                                                    <input class="form-control" id="cc2" name="ClaveCatastral2" placeholder="(solo números, sin guiones)" style="text-transform: uppercase" type="number" value="">
                                                    <div class="invalid-tooltip">
                                                        Por favor verifique la clave.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 mt-2">
                                                <button type="button" class="btn btn-success float-right" id="btnAgregar"><i class="mdi mdi-plus-circle"></i> Agregar</button>
                                            </div>
                                        </div>

                                        <!--<table class="table">
                                    <tbody>
                                        <tr>
                                            <td style="border-top:hidden;width:18% !important; font-weight: bold;">Nombre Completo:</td>
                                            <td style="border-top:hidden !important"> 
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="mdi mdi-account"></i></span>
                                                        </div>
                                                        <input class="form-control" data-val="true" data-val-required="El campo Nombre es obligatorio." id="Nombre" name="Nombre" placeholder="NOMBRE COMPLETO" style=" text-transform: uppercase" type="text" value="" required>
                                                        <div class="invalid-tooltip">
                                                            Por favor ingrese un Nombre.
                                                        </div>
                                                    </div>
                                             </td>
                                            <td style="border-top:hidden;width:18% !important"><strong>RFC:</strong></td>
                                            <td style="border-top:hidden !important"> 
                                                <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="mdi mdi-account-badge-horizontal-outline"></i></span>
                                                        </div>
                                                        <input class="form-control" data-val="true" data-val-required="El campo Rfc es obligatorio." id="RFC" name="Rfc" placeholder="RFC" style="text-transform: uppercase" type="text" value="">
                                                        <div class="invalid-tooltip">
                                                            Por favor ingrese un RFC.
                                                        </div>
                                                </div>
                                                <span class="error-form" id="RFC_error_message" style="color: red; display: none;"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top:hidden;width:18% !important"><strong>Correo Electrónico:</strong></td>
                                            <td style="border-top:hidden !important"> 
                                                <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="mdi mdi-email"></i></span>
                                                        </div>
                                                        <input class="form-control" data-val="true" data-val-required="El campo Nombre es obligatorio." id="Correo" name="Correo" placeholder="correo@correo.com" type="email" value=""> 
                                                        <div class="invalid-tooltip">
                                                            Por favor ingrese un Correo.
                                                        </div>
                                                </div>
                                               
                                            </td>
                                            <td style="border-top:hidden;width:18% !important"><strong>Telefono:</strong></td>
                                            <td style="border-top:hidden !important"> 
                                                <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="mdi mdi-cellphone"></i></span>
                                                        </div>
                                                        <input class="form-control" data-val="true" data-val-required="El campo Rfc es obligatorio." id="Telefono" name="Telefono" placeholder="0000000000" style="text-transform: uppercase" type="text" value=""> 
                                                        <div class="invalid-tooltip">
                                                            Por favor ingrese un Telefono.
                                                        </div>
                                                </div>
                                                
                                            </td>
                                        </tr>
                                    <tr>
                                    <td style="border-top:hidden;width:18% !important"><strong>Clave catastral:</strong></td>
                                    <td style="border-top:hidden !important">
                                        <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="mdi mdi-map-legend"></i></span>
                                                        </div>
                                                        <input class="form-control" id="cc1" name="ClaveCatastral1" placeholder="(solo números, sin guiones)" style="text-transform: uppercase" type="number" value="" >
                                                        <div class="invalid-tooltip">
                                                            Por favor ingrese una Clave.
                                                        </div>
                                        </div>
                                       
                                    </td>
                                    <td style="border-top:hidden;width:18% !important"><strong>Confirmar clave catastral:</strong></td>
                                    <td style="border-top:hidden !important">
                                        <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="mdi mdi-map-legend"></i></span>
                                                        </div>
                                                        <input class="form-control" id="cc2" name="ClaveCatastral2" placeholder="(solo números, sin guiones)" style="text-transform: uppercase" type="number" value="">
                                                        <div class="invalid-tooltip">
                                                            Por favor verifique la clave.
                                                        </div>
                                        </div>
                                       
                                    </td>
                                    <td><button type="button" class="btn btn-success btn-block" id="btnAgregar"><i class="mdi mdi-plus-circle"></i> Agregar</button> </td>
                                    </tr>
                                    </tbody>
                                </table>-->

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-hover" id="tbldetalle" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 50px;"></th>
                                                            <th style="width: 300px;">CONCEPTO</th>
                                                            <th style="width: 300px;">CLAVE</th>
                                                            <th style="width: 150px;">CANTIDAD</th>
                                                            <th style="width: 150px;">IMPORTE</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tfoot>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tfoot>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End Page-content -->


            </div>

            <div>
            </div>
        </div>
    </div>
</body>
<!-- JAVASCRIPT -->
<script src="../assets/libs/jquery/jquery.min.js"></script>

<script src="../assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../assets/libs/metismenu/metisMenu.min.js"></script>
<script src="../assets/libs/simplebar/simplebar.min.js"></script>
<script src="../assets/libs/node-waves/waves.min.js"></script>
<script src="../assets/js/moment.min.js"></script>



<!-- Sweet Alerts js -->
<script src="../assets/libs/sweetalert2/sweetalert2.min.js"></script>

<!-- Select 2 js -->
<script src="../assets/libs/select2/js/select2.min.js"></script>

<!-- App js -->
<script src="../assets/js/app.js"></script>
<!-- Required datatable js -->
<script src="../assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="../assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="../assets/js/jquery.blockUI.js"></script>
<script>
    $(document).on('click', '#btnPagar', function(e) {
        e.preventDefault();
        var Nombre = $('#Nombre').val();
        var RFC = $('#RFC').val();
        var Correo = $('#Correo').val();
        var Telefono = $('#Telefono').val();
        var aviso = $("#lbltotal").attr("tot");

        if ($('#RFC').hasClass('invalid')) {
            Swal.fire({
                type: 'warning',
                text: 'El RFC no tiene el formato correcto.',
            })
            $('#RFC').focus();
            return;
        }
        if ($.trim(Nombre) == "") {
            Swal.fire({
                type: 'warning',
                text: 'Favor de capturar el nombre.',
            })
            $('#Nombre').focus();
            return;
        }
        if ($.trim(Nombre) == "" || $.trim(RFC) == "") {
            $('#RFC').focus();
            Swal.fire({
                type: 'warning',
                text: 'Favor de capturar el RFC.',
            })
            return;
        }
        if (aviso == "" || aviso == "0") {
            Swal.fire({
                type: 'warning',
                text: 'El importe a pagar debe ser mayor a $0.00.',
            })
            return;
        } else {
            var rowCount = $('#tbldetalle >tbody >tr').length;
            var json = new Object();
            json.uuid = "<?php echo $myuuid; ?>";
            json.NombreCiudadano = Nombre;
            json.Rfc = RFC;
            json.Correo = Correo;
            json.Telefono = Telefono;
            json.Tipo = "INF-CATASTRAL";
            json.Cantidad = rowCount;

            json.Total = aviso;
            //console.log(json);
            enviar(json);
        }
    });

    function formatFriendCode(friendCode, numDigits, delimiter) {
        return Array.from(friendCode).reduce((accum, cur, idx) => {
            return accum += (idx + 1) % numDigits === 0 ? cur + delimiter : cur;
        }, '')
    }


    $(function() {



        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
        });

        var table = $("#tbldetalle").DataTable({
            ajax: "get_items.php?uuid=<?php echo $myuuid; ?>",
            "language": {
                "url": "../assets/Spanish.json",
                "sEmptyTable": "Debe agregar al menos una clave catastral"
            },
            responsive: true,
            paginate: false,
            searching: false,
            info: false,
            columns: [{
                    "data": "informacion_id"
                },
                {
                    "data": "concepto"
                },
                {
                    "data": "clave_catastral"
                },
                {
                    targets: 3,
                    data: null,
                    defaultContent: '<a class="roles text-success" style="cursor:pointer !important;"> 1</a>'
                },
                {
                    "data": "importe"
                },
                {
                    targets: -1,
                    data: null,
                    defaultContent: '<a class="eliminar btn btn-danger text-white">Eliminar </a>'
                }
            ],
            rowCallback: function(row, data, index) {

                $("td", row).addClass('align-middle');
                $("td:eq(0)", row).text(index + 1);
                $("td:eq(1)", row).addClass('text-primary ');
                $("td:eq(4)", row).text(formatter.format(data['importe']));
                $("td:eq(2)", row).text(formatFriendCode(data['clave_catastral'], 3, '-').slice(0, -1));
            },
            footerCallback: function(row, data, start, end, display) {
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                total = api
                    .column(4)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(4).footer()).html(
                    "<span id='lbltotal' class='text-right font-weight-bold mr-3 align-middle font-medium' tot=" + total + ">" + formatter.format(total) + "</span>"
                );

                $(api.column(5).footer()).html(
                    "<button type='button' id='btnPagar' class='btn btn-success btn-block'><i class='mdi mdi-credit-card-plus'></i> Pagar</button> "
                );
            }
        });

        $("#tbldetalle tbody").on('click', '.eliminar', function() {
            if (table.row(this).child.isShown()) {
                var data = table.row(this).data();
            } else {
                var data = table.row($(this).parents("tr")).data();
            }
            //alert( "Eliminar: " + data[0] );
            var id = data["informacion_id"];
            var clave = data["clave_catastral"];

            Swal.fire({
                title: '¿Seguro de eliminar la Clave Catastral: ' + clave + '?',
                type: 'question',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Si, Eliminar!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: 'POST',
                        url: "eliminar_item.php",
                        data: {
                            'uuid': id,
                            'clave': clave
                        },
                        success: function(data) {
                            //console.log(data);
                            $('#tbldetalle').DataTable().ajax.reload(null, false);
                            Swal.fire({
                                type: 'info',
                                title: data,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    });
                }
            })
        });



        $("#RFC_error_message").hide();
        $("#RFC").keyup(function() {
            check_RFC();
        });

        function check_RFC() {
            var mask = new RegExp(/^([A-ZÃ‘\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z\d]{3})?$/i);
            if (mask.test($("#RFC").val())) {
                $("#RFC_error_message").hide();
                $("#RFC").removeClass("invalid");
            } else if ($("#RFC").val() == "") {
                $("#RFC_error_message").hide();
                $("#RFC").removeClass("invalid");
            } else {
                $("#RFC_error_message").html("El RFC no contiene el formato correcto");
                $("#RFC_error_message").show();
                $("#RFC").addClass("invalid");
                error_RFC = true;
            }
        }

        $('#btnAgregar').click(function(e) {
            e.preventDefault();
            var Nombre = $('#Nombre').val();
            var RFC = $('#RFC').val();
            var cc1 = $('#cc1').val();
            var cc2 = $('#cc2').val();
            var uuid = $('#uuid').val();

            if ($.trim(cc1) == "" || $.trim(cc1) == "") {
                $('#cc1').focus();
                Swal.fire({
                    type: 'warning',
                    text: 'Debe proporcionar la clave catastral.',
                })
                return;
            }
            if ($.trim(cc1) != $.trim(cc2)) {
                $('#ClaveCatastral1').focus();
                Swal.fire({
                    type: 'warning',
                    text: 'Las claves catastrales no coinciden, favor de validar.',
                })
                return;
            }
            var json = new Object();
            json.uuid = uuid;
            json.concepto = "Informacion Catastral";
            json.clave_catastral = cc1;
            json.importe = 224;
            json.estatus = "N";

            //console.log(json);
            $.ajax({
                url: "agregar_item.php",
                type: "POST",
                data: json,
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                },
                success: function(data) {
                    console.log(data);
                    if (data === 'Clave agregada') {
                        $('#tbldetalle').DataTable().ajax.reload(null, false);
                        //reset el form
                        //document.getElementById('form_agregar').reset();
                        $('#cc1').val('');
                        $('#cc2').val('');
                        Swal.fire({
                            //title:"Good job!",
                            text: data,
                            type: "success"
                        })
                    } else {
                        Swal.fire({
                            //title:"Good job!",
                            text: data,
                            type: "warning",
                            showCancelButton: !0,
                            confirmButtonColor: "#3b5de7",
                        })
                    }

                }
            });

            //agregarItem(json);
        });
    });

    function eliminar(index) {
        var Nombre = $('#Nombre').val();
        var RFC = $('#RFC').val();
        var cc1 = $('#cc1').val();
        var cc2 = $('#cc2').val();
        var items = $('#Items').val();
        var claves = [];
        for (var i = 0; i < items; i++) {
            claves.push($('input[name="ClavesPagar[' + i + ']"]').val());
        }
        var json = new Object();
        json.Nombre = Nombre;
        json.Rfc = RFC;
        json.ClaveCatastral1 = cc1;
        json.ClaveCatastral2 = cc2;
        json.Claves = claves;
        json.IndexEliminar = index - 1;
        eliminarItem(json);
    }

    function eliminarItem(json) {
        var uri = document.getElementById('EliminarItem').value;
        post_to_url(uri, {
            Nombre: json.Nombre,
            Rfc: json.Rfc,
            ClaveCatastral1: json.ClaveCatastral1,
            ClaveCatastral2: json.ClaveCatastral2,
            Claves: json.Claves,
            IndexEliminar: json.IndexEliminar
        }, 'post');
    }

    function agregarItem(json) {
        var uri = document.getElementById('AgregarItem').value;
        post_to_url(uri, {
            Nombre: json.Nombre,
            Rfc: json.Rfc,
            ClaveCatastral1: json.ClaveCatastral1,
            ClaveCatastral2: json.ClaveCatastral2,
            Claves: json.Claves
        }, 'post');
    }

    function enviar(json) {
        $("#btnPagar").attr("disabled", true);
        $.ajax({
            url: "proceso.php",
            data: json,
            method: 'post',
            beforeSend: function() {
                $.blockUI({
                    fadeIn: 0,
                    fadeOut: 0,
                    showOverlay: false,
                    message: '<img src="../assets/images/p_header.png" height="100"/><br><h3 class="text-primary"> Procesando</h3><br><h5>Espere un momento por favor</h5>',
                    css: {
                        backgroundColor: 'white',
                        border: '0',
                    },
                });
            },
        }).done(function(response) {
            //console.log(response);
            //$('#btnPagar').html("<i class='mdi mdi-spin mdi-loading'></i> Procesando");
            location.href = response;
            /*if(response.length > 0){
                var data = JSON.parse(response);
                location.href = data[0].url;
            }*/
        }).fail(function() {

        });
    }

    function post_to_url(path, params, method) {
        method = method || "post";
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);
        for (var key in params) {
            if (params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);
                form.appendChild(hiddenField);
            }
        }
        document.body.appendChild(form);
        form.submit();
    }
</script>

</html>