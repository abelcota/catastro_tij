<?php
include ('../conexion.php');
$fmt = new NumberFormatter( 'es_MX', NumberFormatter::CURRENCY );

//variables que se obtendran por post
if ( isset($_GET['uuid']) ) {

    $uuid = $_GET['uuid'];

    //se hace la consulta a la base de datos para sacar la clave catastral
    $query_uuid = "select * from perito_infcat_detalle where uuid = '$uuid'";
    $estadouuid = $pdo->prepare($query_uuid);
    $estadouuid->execute();
    $data_uuid = $estadouuid->fetch(\PDO::FETCH_ASSOC);

    $cvecat = substr($data_uuid['clave_catastral'], 3);
    
     //obtener informacion de la clave catastral
     $query_clave = 'SELECT * FROM a_unid A LEFT JOIN a_calle b ON A."CVE_POBL"=b."CVE_POBL" AND A."CVE_CALLE_"=b."CVE_CALLE" LEFT JOIN a_col C ON A."CVE_POBL"=C."CVE_POBL" AND A."CVE_COL_UB"=C."CVE_COL" LEFT JOIN a_prop d ON A."CVE_PROP"=d."CVE_PROP" LEFT JOIN a_usosayto e ON A."CVE_USO"=e."CVE_USO" LEFT JOIN a_regim ar ON A."CVE_REGIM"=ar."CVE_REGIM" WHERE A."CLAVE"=\''.$cvecat.'\'';
     $estado= $pdo->prepare($query_clave);
     $estado->execute();
     $data_cvecat = $estado->fetch(\PDO::FETCH_ASSOC);

     $sqlzona = '
    SELECT * FROM a_zona, a_manz 
    WHERE (a_manz."CVE_POBL"=a_zona."CVE_POBL" AND a_manz."CVE_ZONA"=a_zona."CVE_ZONA")
    AND a_manz."CVE_POBL"='.$data_cvecat["CVE_POBL"].'
    AND a_manz."NUM_CTEL"= '.$data_cvecat["NUM_CTEL"].'
    AND a_manz."NUM_MANZ"= '.$data_cvecat["NUM_MANZ"].'
    ';
    $estadozona = $pdo->prepare($sqlzona);
    $estadozona->execute();
    $datazona = $estadozona->fetch(PDO::FETCH_ASSOC);

     // Include the main TCPDF library (search for installation path).
     require_once('../reportes/extensions/tcpdf/tcpdf.php');

    // Extend the TCPDF class to create custom Header and Footer
    class MYPDF extends TCPDF {

        protected $info;
        protected $folio;
        protected $cve;
        protected $head;

        public function set_data($folio = "", $cve = "", $head = "") {
            $this->folio = $folio;
            $this->cve = $cve;
            $this->head = $head;
        }

        public function setInfo($info){
            $this->info = $info;

        }

        //Page header
        public function Header() {
        
            // Logo
            $this->Image('../assets/images/logo_v.png', 15, 8, 20, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);
            $this->Image('../assets/images/logo_catastro1.jpg', 37, 8, 18, '', '', '', 'T', false, 300, '', false, false, 0, false, false, false);

            $this->SetFont('helvetica', 'N', 10);
            // Set font
            $this->Ln(1);

            $tabla_header = '<table border="0" cellpadding="2" cellspacing="0" nobr="false">
            <tr>
                <td align="left" width="30%"></td>
                <td align="center" width="40%" style="display:flex; items-align:justify;"><strong>GOBIERNO DEL ESTADO DE SINALOA<BR>H. AYUNTAMIENTO DE TIJUANA<BR>DIRECCIÓN DE CATASTRO MUNICIPAL</strong></td>
                <td rowspan="2" align="left" width="30%" style="background-color:#FBFBFA; color:#632B2B; font-size:9; font-weight:bold;"><span style="font-size:9; font-weight:bold; color:black;">FOLIO NO: </span><br><br><span style="font-size: 12;  font-weight:bold; ">'.$this->folio.'</span></td>
            </tr>
            <tr>
                <td width="100%" aligh="center" style="font-size: 14; font-weight:bold; text-align:center;">INFORMACIÓN CATASTRAL</td>
                
            </tr>
            <tr>
            <td align="left" width="22%"></td>
            <td align="center" width="78%">'.$this->head.'</td>
            </tr>
            </table>';
            $this->writeHTML($tabla_header , true, false, false, false, '');
        }

        // Page footer
        public function Footer() {
            // Position at 30 mm from bottom
            $this->SetY(-8);
            $this->writeHTML($this->info, true, false, false, false, '');
        }
    }

    // create new PDF document
    $pdf = new MYPDF('P', 'mm', PDF_PAGE_FORMAT , true, 'UTF-8', false);

    $clave_split = str_split($cvecat, 3);
    
    $head = '<table border="0" cellpadding="2" cellspacing="0" nobr="true">
    <tr style="background-color:#632B2B; color:white; font-size:5;">
        <td align="center" style="display:flex; items-align:center;"><span >OFICINA</span></td>
        <td align="center" style="display:flex; items-align:center;"><span >POBLACION</span></td>
        <td align="center" style="display:flex; items-align:center;"><span >CUARTEL</span></td>
        <td align="center" style="display:flex; items-align:center;"><span >MANZANA</span></td>
        <td align="center" style="display:flex; items-align:center;"><span >PREDIO</span></td>
        <td align="center" style="display:flex; items-align:center;"><span >CONSEC.</span></td>
        <td align="center" style="display:flex; items-align:center;"><span >FOTO</span></td>
        <td align="center" style="display:flex; items-align:center;"><span >LOTE</span></td>
        <td align="center" style="display:flex; items-align:center;"><span >MUINICIPIO</span></td>
        <td align="center" style="display:flex; items-align:center;"><span >RUSTICA</span></td>
        <td align="center" style="display:flex; items-align:center;"><span >CONSEC.</span></td>
        <td align="center" style="display:flex; items-align:center;"><span >PRECIO</span></td>
    </tr>
     <tr style="font-size:10; font-weight:bold;">
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;">007</td>
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;">'.$clave_split[0].'</td>
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;">'.$clave_split[1].'</td>
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;">'.$clave_split[2].'</td>
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;">'.$clave_split[3].'</td>
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;">'.$clave_split[4].'</td>
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;"></td>
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;"></td>
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;"></td>
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;"></td>
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;"></td>
        <td align="center" style="display:flex; items-align:center;border: solid 0.5px #FBFBFA;"></td>
    </tr>
    </table>
    ';

    $pdf->set_data($uuid, $cvecat, $head);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Direccion de Catastro Culiacan');
    $pdf->SetTitle('Informacion Catastral');
    $pdf->SetSubject('Informacion Catastral');
    $pdf->SetKeywords('informacion, catastral, catastro, ayuntamiento, culiacan');
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING,);
    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 

    // set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // add a page
    $pdf->AddPage('P', [216, 279], TRUE);
    $pdf->SetFont('helvetica', 'N', 10);
    $pdf->Ln(14);

    $propietario_nombre = $data_cvecat['NOM_PROP']." ".$data_cvecat['APE_PAT']." ".$data_cvecat['APE_MAT'];


    //obtenemos los valores del terreno
    $sqlterr = '
    SELECT
        trr."CVE_CALLE",
        trr."CVE_TRAMO",
        trr."SUP_TERR",
        0.00 AS ValorUnitario1,
        0.00 AS DemeritoTramo1,
        trr."CVE_CALLE_",
        trr."CVE_TRAMO_",
        trr."SUP_TERR_E",
        0.00 AS ValorUnitario2,
        0.00 AS DemeritoTramo2,
        "SUP_TERR_D",
        CONCAT (
            "CVE_DEM_TE"::varchar(2),
            \'-\',
            "CVE_DEM_T2"::varchar(2),
            \'-\',
        "CVE_DEM_T3" :: VARCHAR ( 2 )) AS demeritos,
        "FAC_DEM_TE",
        0.00 AS ValorNeto 
    FROM
        a_terr AS trr 
    WHERE
        trr.clave = \''.$cvecat.'\' 
    ORDER BY
        "NUM_TERR"
    ';
    $estaterr = $pdo->prepare($sqlterr);
    $estaterr->execute();
    $dataterr = $estaterr->fetchAll(PDO::FETCH_ASSOC);
    $terrenos = "";
    $sup_total = 0.0;
    $valor_total = 0.0;
    foreach ($dataterr as $terr){
    //busqueda de tramos
    $ValorTramoNormal = 0;
    $Dem1 = 0.0;
    $ValorTramoEsquina = 0;
    $Dem2 = 0.0;
    //Valores del terreno 
    $num_zona = $datazona["CVE_ZONA"];
    $valorzona = $datazona["VAL_UNIT_T"];

    //calculamos el valor del unitario general
    $sqltramo1 = 'SELECT * FROM a_tramo WHERE "CVE_POBL"=\''.$data_cvecat["CVE_POBL"].'\' AND "CVE_CALLE"=\''.$terr['CVE_CALLE'].'\' AND "CVE_TRAMO"=\''.$terr['CVE_TRAMO'].'\'';
    $estadotramo1 = $pdo->prepare($sqltramo1);
    $estadotramo1->execute();
    $datatramo1 = $estadotramo1->fetch(PDO::FETCH_ASSOC);
    if ($datatramo1 != null)
    {
        $ValorTramoNormal = $datatramo1["VAL_UNIT_T"];
        $Dem1 = 100 * (1 - $datatramo1["FAC_DEM_VA"]);
    }
    else{
        $ValorTramoNormal = $valorzona;
    }

    //calculamos el valor del unitario de la esquina
    $sqltramo2 = 'SELECT * FROM a_tramo WHERE "CVE_POBL"=\''.$data_cvecat["CVE_POBL"].'\' AND "CVE_CALLE"=\''.$terr['CVE_CALLE_'].'\' AND "CVE_TRAMO"=\''.$terr['CVE_TRAMO_'].'\'';
    $estadotramo2 = $pdo->prepare($sqltramo2);
    $estadotramo2->execute();
    $datatramo2 = $estadotramo2->fetch(PDO::FETCH_ASSOC);
    if ($datatramo2 != null)
    {
        $ValorTramoEsquina = $datatramo2["VAL_UNIT_T"];
        $Dem2 = 100 * (1 - $datatramo2["FAC_DEM_VA"]);
    }
    else{
        $ValorTramoEsquina = $valorzona;
    }


    $valorneto = ($terr["SUP_TERR"] * $ValorTramoNormal * ((100 - $Dem1) / 100) ) + ($terr["SUP_TERR_E"] * 0.25 * $ValorTramoEsquina * ((100 - $Dem2) / 100) ) - ($terr["SUP_TERR_D"]  * $ValorTramoNormal * $terr["FAC_DEM_TE"]);

    $sup_total += $terr["SUP_TERR"];
    $valor_total += $valorneto;
    }

    //empiezo a calcular renglones de construccion
    $SuperficieConstruccionFormateado = 0;
    $ValorConstruccionFormateado = 0;
    $ValorCatastralFormateado = 0;

    $sqlconst = 'SELECT cons."CVE_CAT_CO", cons."SUP_CONST", 
                    0.00 as ValorUnitario, cons."CVE_EDO_CO", 
                    cons."EDD_CONST", 0.00 as FactorDemerito, 
                    1.00 as FactorComercializacion, 0.00 as ValorNeto 
                    FROM a_const AS cons 
                    WHERE cons.clave=\''.$cvecat.'\' ORDER BY "NUM_CONST"';

    $estaconst = $pdo->prepare($sqlconst);
    $estaconst->execute();
    $dataconst = $estaconst->fetchAll(PDO::FETCH_ASSOC);

    $construcciones = "";
    $Categoria = "";
    $Edad = 0;
    $estado = 0;
    foreach ($dataconst as $const){
    //'busquedas de categorias
    $Categoria = $const["CVE_CAT_CO"];
    $Superficie = $const["SUP_CONST"];
    $Edad = $const["EDD_CONST"];
    $estado = $const["CVE_EDO_CO"];
    $valorunitario = $const["valorunitario"];
    $factdem = $const["factordemerito"];
    $factcom = $const["factorcomercializacion"];
    $valorneto = $const["valorneto"];
    //buscamos el valor del factor de demerito de la construccion
    $sqldemc = 'SELECT * FROM a_demc WHERE "CVE_CAT_CO" =\''.$Categoria.'\' AND "CVE_EDO_CO"= '.$estado.' AND "EDD_CONST"= '.$Edad;
    $estadodemc = $pdo->prepare($sqldemc);
    $estadodemc->execute();
    $datademc = $estadodemc->fetch(PDO::FETCH_ASSOC);
    if ($datademc != null)
    {
        $factdem = $datademc["FAC_DEM_CO"];
    }
    //Buscamos el valor unitario de la construccion
    $sqlcatco = 'SELECT * FROM a_cat_mp WHERE "CVE_CAT_CO"=\''.$Categoria.'\'';
    $estadocatco = $pdo->prepare($sqlcatco);
    $estadocatco->execute();
    $datacatco = $estadocatco->fetch(PDO::FETCH_ASSOC);
    if ($datacatco != null)
    {
        $valorunitario = $datacatco["val_unit_c"];
    }
    //buscamos el valor del factor de comercializacion
    $sqlfaccom = 'SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL" ='.$data_cvecat['CVE_POBL'];
    $estadofaccom = $pdo->prepare($sqlfaccom);
    $estadofaccom->execute();
    $datafaccom = $estadofaccom->fetch(PDO::FETCH_ASSOC);
    if ($datafaccom != null)
    {
        $factcom = $datafaccom["FAC_COM"];
    } 

    //calcular el valorneto por cobnstruccion
    $valorneto = $Superficie * $valorunitario * $factdem * $factcom;

    $SuperficieConstruccionFormateado += $Superficie;
    $ValorConstruccionFormateado += $valorneto;
    }

    $ValorCatastralFormateado = $ValorConstruccionFormateado + $valor_total;

    $html0 = '<br><table cellpadding="5" cellspacing="0" nobr="true">
    <tr style="background-color:#FBFBFA; color:#632B2B; font-size:8; font-weight:bold;" >
       <td style="border: solid 0.5px #FBFBFA;">PROPIETARIOS :</td>
       <td style="border: solid 0.5px #FBFBFA;">UBICACIÓN DEL PREDIO : </td>
    </tr>
    <tr style="font-size: 9px;">
       <td style="border: solid 0.5px #FBFBFA;">'.$propietario_nombre.'<br></td>
       <td style="border: solid 0.5px #FBFBFA;">'.$data_cvecat['UBI_PRED'].'<br></td>
    </tr>
    
    <tr style="font-size: 9px;">
        <td width="24%" style="background-color:#FBFBFA; color:#632B2B;  font-weight:bold;"><b>REGIMEN DE PROPIEDAD :</b></td>
        <td width="44%" style="border: solid 0.5px #FBFBFA;">'.$data_cvecat['DES_REGIM'].'</td>
        <td width="18%" style="background-color:#FBFBFA; color:#632B2B;  font-weight:bold;"><b>RESTRICCIÓN LEGAL : </b></td>
        <td width="14%" style="border: solid 0.5px #FBFBFA;">'.$data_cvecat['STT_REST'].'</td>
    </tr>
    <tr style="font-size: 9px;">
        <td width="24%" align="center" style="background-color:#FBFBFA; color:#632B2B;  font-weight:bold;"><b>FECHA DE EXPEDICION</b></td>
        <td width="22%" align="left" style="border: solid 0.5px #FBFBFA;"><b>SUP. DE TERRENO</b> : '.$sup_total.'</td>
        <td width="22%" align="center" style="border: solid 0.5px #FBFBFA;"><b>SUP. CONSTRUIDA</b> : '.$SuperficieConstruccionFormateado.'</td>
        <td width="32%" align="right" style="border: solid 0.5px #FBFBFA;"><b>VALOR CATASTRAL</b> : '.$fmt->formatCurrency($ValorCatastralFormateado,"MXN").'</td>
    </tr>
    <tr style="font-size: 8px;">
        <td width="8%" style="background-color:#FBFBFA; color:#632B2B;  font-weight:bold;">DÍA</td>
        <td width="8%" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">MES</td>
        <td width="8%" style="background-color:#FBFBFA; color:#632B2B;  font-weight:bold;">AÑO</td>
        <td width="10%" style="background-color:#FBFBFA; color:#632B2B;  font-weight:bold;">LUGAR</td>
        <td width="8%" style="background-color:#FBFBFA; color:#632B2B;  font-weight:bold;">INSC.</td>
        <td width="8%" style="background-color:#FBFBFA; color:#632B2B;  font-weight:bold;">LIBRO.</td>
        <td width="8%" style="background-color:#FBFBFA; color:#632B2B;  font-weight:bold;">SECC.</td>
        <td width="8%" style="background-color:#FBFBFA; color:#632B2B;  font-weight:bold;">FOLIO</td>
        <td rowspan="2" width="16%" style="background-color:#FBFBFA; color:#632B2B; font-size:8; font-weight:bold;">NOMBRE Y FIRMA DEL EMPELADO:</td>
    </tr>
    <tr style="font-size: 9px;">
        <td width="8%" style="border: solid 0.5px #FBFBFA;">'.strftime("%d", strtotime("now")).'</td>
        <td width="8%" style="border: solid 0.5px #FBFBFA;">'.strftime("%m", strtotime("now")).'</td>
        <td width="8%" style="border: solid 0.5px #FBFBFA;">'.strftime("%Y", strtotime("now")).'</td>
        <td width="10%" style="border: solid 0.5px #FBFBFA;">INTERNET</td>
        <td width="8%" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="8%" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="8%" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="8%" style="border: solid 0.5px #FBFBFA;"></td>        
    </tr>
    <tr style="font-size: 10px; font-weight:bold;">
        <td width="100%" align="center" style="border: solid 0.5px #FBFBFA;">AUTORIZACIÓN DEFINITIVA PARA EL PAGO DE I.S.A.I.</td>       
    </tr>
    <tr style="font-size: 8px;">
        <td width="30%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">ESCRITURA</td> 
        <td width="15%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">NO. ESCRITURA</td>
        <td width="12%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">VOLUMEN</td>
        <td width="12%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">FECHA</td>
        <td width="31%" align="center" style="background-color:#FBFBFA; color:#632B2B;font-weight:bold;">NOTARIO O AUTORIDAD OTORGANTE</td>            
    </tr>
    <tr style="font-size: 8px;">
        <td width="7%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">PUBLICA</td> 
        <td width="7%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">PRIVADA</td>
        <td width="7%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">TITULO</td>
        <td width="9%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">SENTENCIA</td>
        <td width="15%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="12%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="12%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="31%" align="center" style="border: solid 0.5px #FBFBFA;"></td>           
    </tr>
    <tr style="font-size: 8px;">
        <td width="7%" align="center" style="border: solid 0.5px #FBFBFA;"></td> 
        <td width="7%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="7%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="9%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="27%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;"></td>
        <td width="43%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">NOMBRE DEL ADQUIRIENTE</td>
          
    </tr>
    <tr style="font-size: 8px;">
        <td width="15%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">VENTA</td> 
        <td width="15%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">VALOR</td>
        <td rowspan="2" width="27%" align="center" style="border: solid 0.5px #FBFBFA;"></td>   
        <td rowspan="2" width="43%" align="center" style="border: solid 0.5px #FBFBFA;"></td>     
    </tr>
    <tr style="font-size: 8px;">
        <td rowspan="2" align="center" width="15%" style="border: solid 0.5px #FBFBFA;"><br><br>TOTAL&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:12px;">[&nbsp;&nbsp;&nbsp;&nbsp;]</span> </td>    
        <td width="15%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">OPERACIÓN</td>
        <td></td> 
        <td></td> 
    </tr>
    <tr style="font-size: 8px;">
        <td width="15%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="27%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;"></td>
        <td width="43%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">DOMICILIO PARA NOTIFICACIONES</td>
    </tr>
    <tr style="font-size: 8px;">
        <td rowspan="2" width="15%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="15%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">BANCARIO</td>
        <td rowspan="2" width="27%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td rowspan="2" width="43%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
    </tr>
    <tr style="font-size: 8px;">
        
        <td width="15%" align="center" style="border: solid 0.5px #FBFBFA;"></td>

    </tr>
    <tr style="font-size: 8px;">
        <td width="35%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">REGISTRO FEDERAL DE CONTRIBUYENTES</td>
        <td rowspan="2" width="30%" align="center" style="border: solid 0.5px #FBFBFA;"><br><br>NOMBRE Y FIRMA DEL PERITO</td>
        <td width="35%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">VALOR COMERCIAL AUTORIZADO</td>
    </tr>
    <tr style="font-size: 8px;">
        <td width="35%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td  width="35%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
    </tr>

    <tr style="font-size: 8px;">
        <td width="100%" style="border: solid 0.5px #FBFBFA;">CANTIDAD CON LETRA </td>
    </tr>
    <tr style="font-size: 8px;">
        <td align="center" width="50%" style="border: solid 0.5px #FBFBFA;">AUTORIZACIÓN VALOR DE OPERACIÓN<br><br><br>_______________________________________<br>NOMBRE Y FIRMA DEL EMPLEADO</td>
        <td align="center" width="50%" style="border: solid 0.5px #FBFBFA;"><br><br><br><br>_______________________________________<br>NOMBRE, FIRMA Y SELLO DEL NOTARIO</td>
    </tr>
    <tr style="font-size: 8px;">
        <td width="100%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">MEDIDAS Y COLINDANCIAS </td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="5%" align="center" style="color:#632B2B; font-weight:bold; border-left: solid 0.5px #FBFBFA;">AL</td>
        <td width="20%" align="right">___________________________</td>
        <td width="75%" align="left" style="border-right: solid 0.5px #FBFBFA;">____________________________________________________________________________________________________________</td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="5%" align="center" style="color:#632B2B; font-weight:bold; border-left: solid 0.5px #FBFBFA;">AL</td>
        <td width="20%" align="right">___________________________</td>
        <td width="75%" align="left" style="border-right: solid 0.5px #FBFBFA;">____________________________________________________________________________________________________________</td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="5%" align="center" style="color:#632B2B; font-weight:bold; border-left: solid 0.5px #FBFBFA;">AL</td>
        <td width="20%" align="right">___________________________</td>
        <td width="75%" align="left" style="border-right: solid 0.5px #FBFBFA;">____________________________________________________________________________________________________________</td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="5%" align="center" style="color:#632B2B; font-weight:bold; border-left: solid 0.5px #FBFBFA;">AL</td>
        <td width="20%" align="right">___________________________</td>
        <td width="75%" align="left" style="border-right: solid 0.5px #FBFBFA;">____________________________________________________________________________________________________________</td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="5%" align="center" style="color:#632B2B; font-weight:bold; border-left: solid 0.5px #FBFBFA;">AL</td>
        <td width="20%" align="right">___________________________</td>
        <td width="75%" align="left" style="border-right: solid 0.5px #FBFBFA;">____________________________________________________________________________________________________________</td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="5%" align="center" style="color:#632B2B; font-weight:bold; border-left: solid 0.5px #FBFBFA;">AL</td>
        <td width="20%" align="right">___________________________</td>
        <td width="75%" align="left" style="border-right: solid 0.5px #FBFBFA;">____________________________________________________________________________________________________________</td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="5%" align="center" style="color:#632B2B; font-weight:bold; border-left: solid 0.5px #FBFBFA;">AL</td>
        <td width="20%" align="right">___________________________</td>
        <td width="75%" align="left" style="border-right: solid 0.5px #FBFBFA; border-bottom: solid 0.5px #FBFBFA">____________________________________________________________________________________________________________</td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="70%" align="left">LUGAR: </td>
        <td width="10%" align="center"> DIA:</td>
        <td width="10%" align="center"> MES:</td>
        <td width="10%" align="center"> AÑO:</td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="100%" align="left" style="border: solid 0.5px #FBFBFA; ">OBSERVACIONES:</td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="100%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">DATOS DEL NUEVO PREDIO YA EFECTUADA LA SUBDIVISIÓN:</td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="8%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">OFICINA</td>
        <td width="8%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">CUARTEL</td>
        <td width="8%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">MANZANA</td>
        <td width="8%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">PREDIO</td>
        <td width="12%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">CONSECUTIVO</td>
        <td width="8%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">FOTO</td>
        <td width="8%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">LOTE</td>
        <td width="12%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">MUNICIPIO</td>
        <td width="8%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">RUSTICA</td>
        <td width="12%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">CONSECUTIVO</td>
        <td width="8%" align="center" style="background-color:#FBFBFA; color:#632B2B; font-weight:bold;">PREDIO</td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="8%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="8%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="8%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="8%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="12%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="8%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="8%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="12%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="8%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="12%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
        <td width="8%" align="center" style="border: solid 0.5px #FBFBFA;"></td>
    </tr>
    <tr style="font-size: 8px; ">
        <td width="100%" align="center" style="font-weight:bold;"><i>NOTA: ESTE DOCUMENTO TIENE VIGENCIA DE 6 (SEIS) MESES A PARTIR DE SU FECHA DE EXPEDICIÓN</i></td>
    </tr>
    </table>
    ';

    $pdf->writeHTML($html0, true, false, false, false, '');

    /*    
    /*$clave_split = str_split($cvecat, 3);
    $propietario_nombre = $data_cvecat['NOM_PROP']." ".$data_cvecat['APE_PAT']." ".$data_cvecat['APE_MAT'];
    $tip_pers = "";
    if($data_cvecat['TIP_PERS'] == 'F')
        $tip_pers = 'Persona física';
    else
        $tip_pers = 'Persona moral';
    
    $cod_pos = "";
    if($data_cvecat['COD_POS'] == null)
        $cod_pos = "S/N";
    else
        $cod_pos = $data_cvecat['COD_POS'];

    
    
    $html1 = '<table border="0" cellpadding="2" cellspacing="0" nobr="true">
    <tr>
        <td align="left" width="40%" style="display:flex; items-align:center;"><span style="font-size:8;">Clave catastral:</span></td>
        <td align="left" width="60%" style="display:flex; items-align:center;"><span style="font-size:8;">Titular: <strong>'.$propietario_nombre.'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="40%" style="display:flex; items-align:left;"><span style="font-size:14; font-weight:bold;">007&nbsp;&nbsp;&nbsp;'.$clave_split[0].'&nbsp;&nbsp;'.$clave_split[1].'&nbsp;&nbsp;'.$clave_split[2].'&nbsp;&nbsp;'.$clave_split[3].'&nbsp;&nbsp;'.$clave_split[4].'</span></td>
        <td align="left" width="60%" style="display:flex; items-align:left;"><span style="font-size:8;">Tipo de Persona: <strong>'.$tip_pers.'</strong></span>  <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Uso: <strong>'.$data_cvecat['DES_USO'].'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="40%" style="display:flex; items-align:left;"><span style="font-size:6;">Municipio &nbsp;&nbsp;&nbsp; Población &nbsp;&nbsp; Cuartel &nbsp;&nbsp;&nbsp;&nbsp;  Manzana  &nbsp;&nbsp;&nbsp;&nbsp; Predio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Unidad</span></td>
        <td align="left" width="60%" style="display:flex; items-align:left;"><span style="font-size:8;">Régimen: <strong>'.$data_cvecat['DES_REGIM'].'</strong></span></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Ubicación / Zona</span></td>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Para Notificación</span></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Asentamiento: <strong>'.$data_cvecat['NOM_COL'].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Asentamiento: <strong>'.$data_cvecat['NOM_COL_NO'].'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Calle: <strong>'.$data_cvecat['NOM_CALLE'].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Calle: <strong>'.$data_cvecat['UBI_PRED'].'</strong></span></td>
    </tr>
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Código Postal: <strong>'.$cod_pos.'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Código zona: <strong> '.$datazona["CVE_ZONA"].' - '.$datazona["DES_ZONA"].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Código Postal: <strong>'.$data_cvecat['COD_POST_N'].'</strong> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Entrecalles: <strong> S/N </strong></span></span></td>
    </tr>
    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Núm de Fte: <strong>'.$data_cvecat['NUM_FTES'].'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;  Metros de Fte: <strong> '.$data_cvecat["LON_FTE"].'</strong></span><span style="font-size:8;">&nbsp;&nbsp;&nbsp;  Valor de zona: <strong> '.$datazona["VAL_UNIT_T"].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Referencia: <strong> S/N </strong></span></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"><span style="font-size:8; color:#632B2B; font-weight:bold;">Registro Público de la Propiedad</span></td>
        <td align="left" width="50%" style="display:flex; items-align:center; border-bottom-style: dotted;"></td>
    </tr>

    <tr>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Libro: <strong>'.$data_cvecat['NUM_LIB'].'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Inscripción: <strong> '.$data_cvecat["NUM_INSC"].'</strong></span><span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Sección: <strong> '.$data_cvecat["NUM_SECC"].'</strong></span></td>
        <td align="left" width="50%" style="display:flex; items-align:left;"><span style="font-size:8;">Escritura: <strong>'.str_pad($data_cvecat['NUM_ESC'], 5, "0", STR_PAD_LEFT).'</strong></span> <span style="font-size:8;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Folio Real: <strong> - </strong></span></td>
    </tr>

    <tr>
        <td></td><td></td>
    </tr>
    </table>
    ';
    $pdf->writeHTML($html1, true, false, false, false, '');

    */
    
    /*$pdf->writeHTML('<p style="text-align:center; font-size:16px; "><b>Avalúo</b></p>
    ', true, false, false, false, '');

    //obtenemos los valores del terreno
    $sqlterr = '
    SELECT
        trr."CVE_CALLE",
        trr."CVE_TRAMO",
        trr."SUP_TERR",
        0.00 AS ValorUnitario1,
        0.00 AS DemeritoTramo1,
        trr."CVE_CALLE_",
        trr."CVE_TRAMO_",
        trr."SUP_TERR_E",
        0.00 AS ValorUnitario2,
        0.00 AS DemeritoTramo2,
        "SUP_TERR_D",
        CONCAT (
            "CVE_DEM_TE"::varchar(2),
            \'-\',
            "CVE_DEM_T2"::varchar(2),
            \'-\',
        "CVE_DEM_T3" :: VARCHAR ( 2 )) AS demeritos,
        "FAC_DEM_TE",
        0.00 AS ValorNeto 
    FROM
        a_terr AS trr 
    WHERE
        trr.clave = \''.$cvecat.'\' 
    ORDER BY
        "NUM_TERR"
    ';
    $estaterr = $pdo->prepare($sqlterr);
    $estaterr->execute();
    $dataterr = $estaterr->fetchAll(PDO::FETCH_ASSOC);
    $terrenos = "";
    $sup_total = 0.0;
    $valor_total = 0.0;
    foreach ($dataterr as $terr){
    //busqueda de tramos
    $ValorTramoNormal = 0;
    $Dem1 = 0.0;
    $ValorTramoEsquina = 0;
    $Dem2 = 0.0;
    //Valores del terreno 
    $num_zona = $datazona["CVE_ZONA"];
    $valorzona = $datazona["VAL_UNIT_T"];

    //calculamos el valor del unitario general
    $sqltramo1 = 'SELECT * FROM a_tramo WHERE "CVE_POBL"=\''.$data_cvecat["CVE_POBL"].'\' AND "CVE_CALLE"=\''.$terr['CVE_CALLE'].'\' AND "CVE_TRAMO"=\''.$terr['CVE_TRAMO'].'\'';
    $estadotramo1 = $pdo->prepare($sqltramo1);
    $estadotramo1->execute();
    $datatramo1 = $estadotramo1->fetch(PDO::FETCH_ASSOC);
    if ($datatramo1 != null)
    {
        $ValorTramoNormal = $datatramo1["VAL_UNIT_T"];
        $Dem1 = 100 * (1 - $datatramo1["FAC_DEM_VA"]);
    }
    else{
        $ValorTramoNormal = $valorzona;
    }

    //calculamos el valor del unitario de la esquina
    $sqltramo2 = 'SELECT * FROM a_tramo WHERE "CVE_POBL"=\''.$data_cvecat["CVE_POBL"].'\' AND "CVE_CALLE"=\''.$terr['CVE_CALLE_'].'\' AND "CVE_TRAMO"=\''.$terr['CVE_TRAMO_'].'\'';
    $estadotramo2 = $pdo->prepare($sqltramo2);
    $estadotramo2->execute();
    $datatramo2 = $estadotramo2->fetch(PDO::FETCH_ASSOC);
    if ($datatramo2 != null)
    {
        $ValorTramoEsquina = $datatramo2["VAL_UNIT_T"];
        $Dem2 = 100 * (1 - $datatramo2["FAC_DEM_VA"]);
    }
    else{
        $ValorTramoEsquina = $valorzona;
    }


    $valorneto = ($terr["SUP_TERR"] * $ValorTramoNormal * ((100 - $Dem1) / 100) ) + ($terr["SUP_TERR_E"] * 0.25 * $ValorTramoEsquina * ((100 - $Dem2) / 100) ) - ($terr["SUP_TERR_D"]  * $ValorTramoNormal * $terr["FAC_DEM_TE"]);

    $sup_total += $terr["SUP_TERR"];
    $valor_total += $valorneto;

    $terrenos .= '<tr style="border-style: dotted;">
    <td align="center" style="font-size:8px; border-left-style: solid;">'.$terr["CVE_CALLE"].' </td>
    <td align="center" style="font-size:8px; ">'.$terr["CVE_TRAMO"].' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["SUP_TERR"].'</td>
    <td align="center" style="font-size:8px;  ">'.$fmt->formatCurrency($ValorTramoNormal,"MXN").' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["CVE_CALLE_"].' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["CVE_TRAMO_"].' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["SUP_TERR_E"].' </td>
    <td align="center" style="font-size:8px;  ">'.$fmt->formatCurrency($ValorTramoEsquina,"MXN").' </td>

    <td align="center" style="font-size:8px;  ">'.$terr["SUP_TERR_D"].' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["demeritos"].' </td>
    <td align="center" style="font-size:8px;  ">'.$terr["FAC_DEM_TE"].' </td>
    <td align="center" style="font-size:8px; border-right-style: solid;">'.$fmt->formatCurrency($valorneto,"MXN").' </td>
    </tr>';
    }  

    $tblterreno = '<span style="font-size:9; color:#632B2B; font-weight:bold;">Datos de Terreno:</span><br><table border="0" cellpadding="2" cellspacing="0" nobr="false">
    <tr style="background-color:#e3e3e3;">
    <td colspan="4" align="center" style="font-size:8px;  border-style: solid; border-style: solid; border-left-style: solid;">G E N E R A L</td>
    <td colspan="4" align="center" style="font-size:8px;  border-style: solid; border-left-style: solid; border-right-style: solid;">INCREMENTO POR ESQUINA (25%)</td>
    <td colspan="4" align="center" style="font-size:8px;  border-style: solid; border-style: solid; border-right-style: solid; border-left-style: solid;">D E M E R I T O</td>
    </tr>
    <tr style="border-style: solid; background-color:#e3e3e3;">
    <td align="center" style="font-size:6px; border-style: solid; border-left-style: solid;  border-bottom-style: solid;">Calle</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Tramo</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Sup M2</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Valor Unitario</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Calle</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Tramo</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Superficie M2</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Valor Unitario</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">Superficie M2</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">CV1-CV2-CV3</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid;">% dem</td>
    <td align="center" style="font-size:6px; border-style: solid; border-bottom-style: solid; border-right-style: solid;">Valor Neto</td>
    </tr>
    '.$terrenos.'
    <tr>
    <td colspan="14" style="font-size:8px;  border-left-style: solid; border-right-style: solid; border-bottom-style: solid;"></td>
    </tr>
    <tr>
    <td colspan="2" style="font-size:8; color:#632B2B; font-weight:bold;">Total :</td>
    <td style="font-size:9px; font-weight:bold;">'.$sup_total.'</td>
    <td colspan="3" style="font-size:8px;"> </td>
    <td colspan="4" style="font-size:8; color:#632B2B; font-weight:bold;  text-align:right;" >Valor Catastral del Terreno :</td>
    <td colspan="2" style="font-size:9px; text-align:right; font-weight:bold;" >'.$fmt->formatCurrency($valor_total,"MXN").'</td>
    </tr>

    </table>
    ';
    $pdf->writeHTML($tblterreno, true, false, false, false, '');


    //empiezo a calcular renglones de construccion
    $SuperficieConstruccionFormateado = 0;
    $ValorConstruccionFormateado = 0;
    $ValorCatastralFormateado = 0;

    $sqlconst = 'SELECT cons."CVE_CAT_CO", cons."SUP_CONST", 
                    0.00 as ValorUnitario, cons."CVE_EDO_CO", 
                    cons."EDD_CONST", 0.00 as FactorDemerito, 
                    1.00 as FactorComercializacion, 0.00 as ValorNeto 
                    FROM a_const AS cons 
                    WHERE cons.clave=\''.$cvecat.'\' ORDER BY "NUM_CONST"';

    $estaconst = $pdo->prepare($sqlconst);
    $estaconst->execute();
    $dataconst = $estaconst->fetchAll(PDO::FETCH_ASSOC);

    $construcciones = "";
    $Categoria = "";
    $Edad = 0;
    $estado = 0;
    foreach ($dataconst as $const){
    //'busquedas de categorias
    $Categoria = $const["CVE_CAT_CO"];
    $Superficie = $const["SUP_CONST"];
    $Edad = $const["EDD_CONST"];
    $estado = $const["CVE_EDO_CO"];
    $valorunitario = $const["valorunitario"];
    $factdem = $const["factordemerito"];
    $factcom = $const["factorcomercializacion"];
    $valorneto = $const["valorneto"];
    //buscamos el valor del factor de demerito de la construccion
    $sqldemc = 'SELECT * FROM a_demc WHERE "CVE_CAT_CO" =\''.$Categoria.'\' AND "CVE_EDO_CO"= '.$estado.' AND "EDD_CONST"= '.$Edad;
    $estadodemc = $pdo->prepare($sqldemc);
    $estadodemc->execute();
    $datademc = $estadodemc->fetch(PDO::FETCH_ASSOC);
    if ($datademc != null)
    {
        $factdem = $datademc["FAC_DEM_CO"];
    }
    //Buscamos el valor unitario de la construccion
    $sqlcatco = 'SELECT * FROM a_cat_mp WHERE "CVE_CAT_CO"=\''.$Categoria.'\'';
    $estadocatco = $pdo->prepare($sqlcatco);
    $estadocatco->execute();
    $datacatco = $estadocatco->fetch(PDO::FETCH_ASSOC);
    if ($datacatco != null)
    {
        $valorunitario = $datacatco["val_unit_c"];
    }
    //buscamos el valor del factor de comercializacion
    $sqlfaccom = 'SELECT "FAC_COM" FROM a_pobl WHERE "CVE_POBL" ='.$data_cvecat['CVE_POBL'];
    $estadofaccom = $pdo->prepare($sqlfaccom);
    $estadofaccom->execute();
    $datafaccom = $estadofaccom->fetch(PDO::FETCH_ASSOC);
    if ($datafaccom != null)
    {
        $factcom = $datafaccom["FAC_COM"];
    } 

    //calcular el valorneto por cobnstruccion
    $valorneto = $Superficie * $valorunitario * $factdem * $factcom;
    $construcciones .= '<tr style="border-style: dotted;">
    <td align="center" style="font-size:8px; border-left-style: solid;">'.$Categoria.' </td>
    <td align="center" style="font-size:8px; ">'.$Superficie.' </td>
    <td align="center" style="font-size:8px;  ">'.$fmt->formatCurrency($valorunitario,"MXN").' </td>
    <td align="center" style="font-size:8px;  ">'.$estado.' </td>
    <td align="center" style="font-size:8px;  ">'.$Edad.' </td>
    <td align="center" style="font-size:8px;  ">'.$factdem.' </td>
    <td align="center" style="font-size:8px;  ">'.$factcom.' </td>
    <td align="center" style="font-size:8px; border-right-style: solid;">'.$fmt->formatCurrency($valorneto,"MXN").' </td>
    </tr>';

    $SuperficieConstruccionFormateado += $Superficie;
    $ValorConstruccionFormateado += $valorneto;
    }

    //valor catastral del predio total de construccion mas total del terreno
    $ValorCatastralFormateado = $ValorConstruccionFormateado + $valor_total;

    $tblconst = '<span style="font-size:9; color:#632B2B; font-weight:bold;">Datos de Construcción:</span><br><table border="0" cellpadding="1" cellspacing="0" nobr="false">
    <tr style="border-style: solid; background-color:#e3e3e3;">
    <td align="center" style="font-size:7px; border-style: solid; border-left-style: solid;  border-bottom-style: solid;">Categoría</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Superficie M2</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Valor Unitario</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Estado</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Edad</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Fact. Dem.</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid;">Fact. Com.</td>
    <td align="center" style="font-size:7px; border-style: solid; border-bottom-style: solid; border-right-style: solid;">Valor Neto ($)</td>
    </tr>
    '.$construcciones.'
    <tr>
    <td colspan="8" style="font-size:8px;  border-left-style: solid; border-right-style: solid; border-bottom-style: solid;"></td>
    </tr>
    <tr>
    <td colspan="1" style="font-size:8; color:#632B2B; font-weight:bold;">Total :</td>
    <td style="font-size:9px; font-weight:bold;" align="center">'.$SuperficieConstruccionFormateado.'</td>
    <td colspan="1" style="font-size:10px;"> </td>
    <td colspan="4" style="font-size:8; color:#632B2B; text-align:right; font-weight:bold;" >Valor Catastral de la Construcción :</td>
    <td colspan="2" style="font-size:9px; font-weight:bold; text-align:right;" >'.$fmt->formatCurrency($ValorConstruccionFormateado,"MXN").'</td>
    </tr>

    </table>
    ';
    $pdf->writeHTML($tblconst, true, false, false, false, '');

    $ValorAnterior = 0.0;
    $ValorAnterior = $data_cvecat['VAL_TERR_A'] + $data_cvecat['VAL_CONST_'];
    
    */
    $pdf->SetAutoPageBreak(TRUE, 0);

    $tablefooter = '
    <pre style="text-align: right">Fecha de Impresión: '.date("m/d/Y h:i:s a", time()).'</pre>
    ';

    $style = array(
        'border' => 1,
        'padding' => 1,
        'fgcolor' => array(0,0,0),
        'bgcolor' => false
    );
    $url_qr = 'http://'.$_SERVER['SERVER_NAME'].'/pagos/printpdf.php?uuid='.$uuid;

    $pdf->write2DBarcode($url_qr, 'QRCODE,H',182,10, 19, 19, $style, 'N');

    $pdf->setInfo($tablefooter);

    //el QR en la esquina
    //pie de la pagina
    // new style
    
    

    //Close and output PDF document
    $pdf->Output($uuid.'.pdf', 'I');

}
else
    echo "No se enviaron parametros";

?>