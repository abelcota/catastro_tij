<?php

require('../Bbva/Bbva.php');
include("../conexion.php");

/*if ( (! empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') ||
     (! empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ||
     (! empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') ) {
    $protocole = 'https://';
} else {
    $protocole = 'http://';
}

$host = $_SERVER['HTTP_HOST'] . '/';
$baseurl = $protocole . $host;
$myappBaseUrl = $baseurl.'/';*/
    
//get parameters from client
$uuid = $_POST['uuid'];
$nombre = strtoupper($_POST['NombreCiudadano']);
$rfc = strtoupper($_POST['Rfc']);
$tipo = $_POST['Tipo'];
$cantidad = $_POST['Cantidad'];
$total = $_POST['Total'];
$correo = $_POST['Correo'];
$telefono = $_POST['Telefono'];

$descripcion = "Cargo pago ".$tipo." x ".$cantidad;

try {
    //Modo Sandbox **Cambiar a true para Produccion
    Bbva::setProductionMode(false);
    //Configuracion de Merchant id y Private key
    $bbva = Bbva::getInstance('mydukacuzwweg11tfj5r', 'sk_0086917f34704797be1a9f7bf5117de2');

    //Comercio
    $chargeRequest = array(
        'affiliation_bbva' => '519868',
        'amount' => $total,
        'description' => $descripcion,
        'currency' => 'MXN',
        'order_id' => $uuid,
        'redirect_url' => $myappBaseUrl.'pagos/result.php',
        'customer' => array(
            'name' => $nombre,
            'last_name' => $rfc,
            'email' => $correo,
            'phone_number' => $telefono)
    );

    $charge = $bbva->charges->create($chargeRequest);

    //guardar los datos en la bsae de datos
    //INSERTAR
    $query = "INSERT INTO perito_infcat VALUES('$uuid', '$nombre', '$rfc', '$charge->id' , '$charge->status', '$correo', '$telefono')";
    $estado= $pdo->prepare($query);
    $estado->execute();

    header('Content-Type: application/json');
    echo json_encode($charge->payment_method->url, JSON_PRETTY_PRINT);


} catch (BbvaApiTransactionError $e) {
	/*error_log('ERROR on the transaction: ' . $e->getMessage() . 
	      ' [error code: ' . $e->getErrorCode() . 
	      ', error category: ' . $e->getCategory() . 
	      ', HTTP code: '. $e->getHttpCode() . 
	      ', request ID: ' . $e->getRequestId() . ']', 0);*/

    echo $e->getMessage();

} catch (BbvaApiRequestError $e) {
	/*error_log('ERROR on the request: ' . $e->getMessage(), 0);*/
    echo $e->getMessage();

} catch (BbvaApiConnectionError $e) {
	/*error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);*/
    echo $e->getMessage();

} catch (BbvaApiAuthError $e) {
	/*error_log('ERROR on the authentication: ' . $e->getMessage(), 0);*/
    echo $e->getMessage();
	
} catch (BbvaApiError $e) {
	/*error_log('ERROR on the API: ' . $e->getMessage(), 0);*/
    echo $e->getMessage();
	
} catch (Exception $e) {
	/*error_log('Error on the script: ' . $e->getMessage(), 0);*/
    echo $e->getMessage();
}


?>