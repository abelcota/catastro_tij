<?php
    require('../conexion.php');
    //mailer 
    require("../PHPMailer/src/Exception.php");
    require("../PHPMailer/src/PHPMailer.php");
    require("../PHPMailer/src/SMTP.php");

    /*if ( (! empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') ||
     (! empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ||
     (! empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') ) {
    $protocole = 'https://';
    } else {
        $protocole = 'http://';
    }

    $host = $_SERVER['HTTP_HOST'] . '/';
    $baseurl = $protocole . $host;
    $myappBaseUrl = $baseurl.'/';*/

    use PHPMailer\PHPMailer\PHPMailer;
    /**
     * Send email via PHPMailer
     *
     * @param $email
     * @param $type
     * @param $key
     * @return array $return (contains error code and error message)
     */
    function do_SendMail($email, $uuid, $content)
    {
        $return = [
            'error' => true
        ];

        //Crear una instancia de PHPMailer
        $mail = new PHPMailer();

        // Check configuration for custom SMTP parameters
        try {
            //Definir que vamos a usar SMTP
            $mail->isSMTP();
            //$mail->SMTPDebug = 1;
            //Ahora definimos gmail como servidor que aloja nuestro SMTP
            $mail->Host = 'smtp.gmail.com';
            //El puerto será el 587 ya que usamos encriptación TLS
            $mail->Port = 587;
            //Definmos la seguridad como TLS
            $mail->SMTPSecure = 'tsl';
            //Tenemos que usar gmail autenticados, así que esto a TRUE
            $mail->SMTPAuth   = true;
            //Definimos la cuenta que vamos a usar. Dirección completa de la misma
            $mail->Username   = "abel.dex@gmail.com";
            //Introducimos nuestra contraseña de gmail
            $mail->Password   = "dimasS55";
            //Definimos el remitente (dirección y, opcionalmente, nombre)
            $mail->SetFrom('pagoscatastroculiacan@catastro.gob.mx', 'Pagos - Direccion de Catastro Municipal Culiacan');
            //Y, ahora sí, definimos el destinatario (dirección y, opcionalmente, nombre)
            $mail->AddAddress("$email");
            //Definimos el tema del email
            $mail->Subject = 'Recibo de Pago de Informacion Catastral | Direccion de Catastro Municipal Culiacan';

            $mail->CharSet = 'UTF-8';

            //Content
            $mail->isHTML(true);

            $mail->Body = strtr(file_get_contents('../PHPMailer/template/recibo.html'), array('%1$s'=> $uuid, '%2$s' => $content, '%3$s' => $myappBaseUrl));
            $mail->AltBody = strtr(file_get_contents('../PHPMailer/template/recibo.html'), array('%1$s'=> $uuid, '%2$s' => $content, '%3$s' => $myappBaseUrl));

            if (!$mail->send())
                throw new \Exception($mail->ErrorInfo);

            $return['error'] = false;

        } catch (\Exception $e) {
            $return['message'] = $mail->ErrorInfo;
        }

        return $return;
    }

?>