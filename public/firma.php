<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('src/Internal/BaseConverter.php');
require_once('src/Internal/BaseConverterSequence.php');
require_once('src/Internal/DataArrayTrait.php');
require_once('src/Internal/Key.php');
require_once('src/Internal/LocalFileOpenTrait.php');
require_once('src/Internal/Rfc4514.php');

require_once('src/Certificate.php');
require_once('src/Credential.php');
require_once('src/PemExtractor.php');
require_once('src/Certificate.php');
require_once('src/PrivateKey.php');
require_once('src/PublicKey.php');
require_once('src/SerialNumber.php');

include_once('conexion.php');

$query_firma = 'SELECT * FROM configuracion';

$estadof= $pdo->prepare($query_firma);
$estadof->execute();
$data_firma = $estadof->fetch(\PDO::FETCH_ASSOC);

try{
    $cerFile = 'assets/firma/'.$data_firma['cerfile'];
    $pemKeyFile = 'assets/firma/'.$data_firma['pemkeyfile'];
    $passPhrase = base64_decode($data_firma['passphrase']); // contraseña para abrir la llave privada
    
    $fiel = PhpCfdi\Credentials\Credential::openFiles($cerFile, $pemKeyFile, $passPhrase);
    
    /*$sourceString = '2100034';
    // alias de privateKey/sign/verify
    $signature = $fiel->sign($sourceString);
    echo base64_encode($signature), PHP_EOL;
    
    
    // alias de certificado/publicKey/verify
    $verify = $fiel->verify($sourceString, $signature);
    var_dump($verify); // bool(true)*/
    
    // objeto certificado
    $certificado = $fiel->certificate();
    $results['status'] = '1';
    $results['firma']  = array('RFC' => $certificado->rfc(), 'NOMBRE' => $certificado->legalName(), 'BRANCHNAME' => $certificado->branchName(), 'SERIAL' => $certificado->serialNumber()->bytes());
    
    
    /*echo $certificado->rfc(), PHP_EOL; // el RFC del certificado
    echo $certificado->legalName(), PHP_EOL; // el nombre del propietario del certificado
    echo $certificado->branchName(), PHP_EOL; // el nombre de la sucursal (en CSD, en FIEL está vacía)
    echo $certificado->serialNumber()->bytes(), PHP_EOL; // número de serie del certificado*/
    
}catch (Exception $e) {
    $results['status'] = '0';
    $results['firma']  = array('Error' => $e);
}

header('Content-Type: application/json');
echo json_encode($results, JSON_PRETTY_PRINT);

?>