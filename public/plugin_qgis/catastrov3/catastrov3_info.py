import os
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDockWidget, QListWidgetItem, QMessageBox, QMainWindow
from qgis.PyQt.QtGui import QIntValidator, QRegExpValidator, QPixmap, QIcon
from qgis.PyQt.QtCore import (pyqtSignal, QRegExp, QSettings) 
from qgis.core import (QgsProject, QgsVectorLayer, QgsRasterLayer, QgsDataSourceUri)
import json
from .utils import Utils
from datetime import datetime

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'catastrov3_info_folio_w.ui'))

class CatastroV3Info(QMainWindow, FORM_CLASS):
    def __init__(self, iface, db = None, a="", fol="", cve="", usuario="", parent=None):
        """Constructor."""
        super(CatastroV3Info, self).__init__(parent)
        self.setupUi(self)
        # set the title
        self.setWindowTitle("Informacion del Folio "+a+fol)
        self.db = db
        self.vlayer = None
          
        # setting window icon
        path_logo = os.path.join(os.path.dirname(__file__), 'icon.png') 
        self.setWindowIcon(QIcon(path_logo))
        
        self.iface = iface
        self.utils = Utils(iface)                       
        self.txt_anio.setText(a)
        self.txt_folio.setText(fol)
        self.txt_clave.setText(cve)
        self.a = a
        self.fol = fol
        self.clavecat = cve
        self.usuario = usuario
        self.obtener_info_folio()

        self.btn_abrir_geometrico.clicked.connect(self.abrir_geometria_edicion)
        self.btn_finalizar_edicion.clicked.connect(self.finalizar_edicion)

    def obtener_info_folio(self):
        info = self.db.get_informacion_folio_extended(self.fol, self.a)
        #info = [propietario, solicitante, dom_sol, tel_sol, fecha_captura, total_sup, total_const, tramite, desc_tramite, observaciones]
        self.txt_propietario.setText(info[0][0])
        self.txt_obs.setText(info[0][9])
        fecha_str = info[0][4].toString("dd/MM/yyyy")
        self.txt_fecha_captura.setText(fecha_str)
        self.txt_tramite.setText(info[0][7])
        self.txt_tramite_desc.setText(info[0][8])
        self.txt_solicitante.setText(info[0][1])
        self.txt_dom_sol.setText(info[0][2])
        self.txt_tel_sol.setText(info[0][3])
        self.txt_propietario.setText(info[0][0])
        self.txt_sup_terr.setText(str(info[0][5]))
        self.txt_sup_const.setText(str(info[0][6]))

    def abrir_geometria_edicion(self):
        file = os.path.join(os.path.dirname(__file__), 'config.json')
        if os.path.isfile(file):
            with open(file, "r") as json_file:
                JSON_config = json.load(json_file)
                uri = QgsDataSourceUri()
            
                # set host name, port, database name, username and password
                uri.setConnection(JSON_config["host"], JSON_config["port"], JSON_config["db"], JSON_config["user"],JSON_config["pass"])
                # set database schema, table name, geometry column and optionally
                # subset (WHERE clause)
                
                uri.setDataSource("public", "Culiacan_Join", "geom", "dg_ccat like '"+self.clavecat[:-6]+"%'", "id")
                layer_manzanas = QgsVectorLayer(uri.uri(False), self.clavecat , "postgres")
                uri.setDataSource("public", "manzanas", "geom", "dg_mpcm = '"+self.clavecat[:-6]+"'", "id")
                layer_manzana = QgsVectorLayer(uri.uri(False), "manzana:" +self.clavecat[:-6] , "postgres")
                uri.setDataSource("public", "construccion", "geom", "dg_ccat = '"+self.clavecat+"'", "id")
                layer_construcciones = QgsVectorLayer(uri.uri(False), "construccion:" +self.clavecat , "postgres")
                layers = [layer_construcciones, layer_manzanas, layer_manzana ]
                #self.vlayer.startEditing()
                QgsProject.instance().addMapLayers(layers)
                vl = QgsProject.instance().mapLayersByName(self.clavecat)[0]
                self.iface.setActiveLayer(vl)
                vl.selectByExpression("dg_ccat = '"+self.clavecat+"'", QgsVectorLayer.SetSelection)
                self.iface.actionZoomToSelected().trigger()

                #self.iface.showAttributeTable(self.vlayer)

                #inhabilitar boton abrir y habilitar el boton finalizar
                self.btn_abrir_geometrico.setEnabled(False)
                self.btn_finalizar_edicion.setEnabled(True)
        else:
             self.iface.messageBar().pushCritical('Advertencia:', 'El archivo de configuracion no existe, por favor configure la conexion a la base de datos.')

    def finalizar_edicion(self):
        #inhabilitar boton abrir y habilitar el boton finalizar
        respuesta = QMessageBox.question(self, "Confirmar", "¿Guardar Cambios al Geometrico?", QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
        if respuesta == QMessageBox.Yes:
            self.btn_abrir_geometrico.setEnabled(False)
            self.btn_finalizar_edicion.setEnabled(False)
            QgsProject.instance().removeMapLayer(self.vlayer)

            #guardar en la base de datos
            res = self.db.guardar_barra_captura(self.a, self.fol, self.usuario)
            if res == 'ok':
                QMessageBox.information(self, "Info", "Captura registrada correctamente")
            else:
                QMessageBox.information(self, "Error", "No se pudo guardar el registro en la Captura")
        
