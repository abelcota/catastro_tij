from builtins import object
import os
from qgis.PyQt.QtSql import QSqlDatabase, QSqlQuery
import pickle

class Config(object):

    def __init__(self):
        self.db = self.get_db()
        self.query = QSqlQuery(self.db)

        config = {
            'host': 'localhost',
            'dbname': '',
            'port': 5432,
            'user': '',
            'pswd': '',
            'schema': 'public',
            'layers': ["departamento", "circunscripcion", "seccion", "chacra", "quinta", "fraccion", "manzana", "parcela", "subparcela"],
            'partidos': self.get_partidos()
        }

        self.set_config(config)
        #print(config)

        config2 = self.get_config()
        print(config2)

    def get_db(self):
        if QSqlDatabase.contains("midb"):
            QSqlDatabase.removeDatabase("midb")

        db = QSqlDatabase.addDatabase("QPSQL", "midb")
        db.setHostName('localhost')
        db.setPort(5432)
        db.setDatabaseName('catastrocln')
        db.setUserName('postgres')
        db.setPassword('postgres')
        return db

    def connect(self):
        ok = self.db.open()
        return ok

    def get_folios_para_captura(self):
        folios_list = []
        if self.connect():
            sql = 'select * from barra_captura'
            if self.query.exec_(sql):
                row = self.query.record()
                while self.query.next():
                    anio = self.query.value(row.indexOf("id_anio"))
                    folio = self.query.value(row.indexOf("id_folio"))
                    clave = self.query.value(row.indexOf("clave"))
                    tramite = self.query.value(row.indexOf("id_tramite"))
                    folios = [anio, folio, clave, tramite]
                    folios_list.append[folios]
                print('Cargado todos los folios')
            else:
                print("No se pudo ejecutar la consulta")
        else:
            print('No se pudo conectar a la base de datos')
            
        return folios_list


    def get_partidos(self):
        partidos = []
        if self.connect():
            sql = 'select DISTINCT("AnioDeclaracion") as nombre from "Inco_Barra" order by nombre desc'
            if self.query.exec_(sql):
                row = self.query.record()
                while self.query.next():
                    nombre = self.query.value(row.indexOf("nombre"))
                    partido = [nombre, nombre]
                    partidos.append(partido)
                print('Cargado todos los anios')
            else:
                print('No se pudo ejecutar la consulta')
        else:
            print('No se pudo conectar a la base de datos')

        return partidos

    def get_config(self):
        return pickle.load(open(self.file, 'rb'))

    def set_config(self, config):
        pickle.dump(config, open(self.file, 'wb'), 2)

Config()
