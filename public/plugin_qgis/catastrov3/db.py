# -*- coding: utf-8 -*-

from builtins import str
import os
import json
from builtins import object
from qgis.PyQt.QtSql import QSqlDatabase, QSqlQuery
from qgis.utils import iface

class Db(object):

    def __init__(self, iface, utils):
        self.iface = iface
        self.utils = utils
        self.file = os.path.join(os.path.dirname(__file__), 'config.json')
        self.db = self.get_db()
        self.query = QSqlQuery(self.db)

    def get_db(self):
        if os.path.isfile(self.file):
            with open(self.file, "r") as json_file:
                JSON_config = json.load(json_file)
                self.db = QSqlDatabase.addDatabase("QPSQL", "midb")
                self.db.setHostName(JSON_config["host"])
                self.db.setPort(int(JSON_config["port"]))
                self.db.setDatabaseName(JSON_config["db"])
                self.db.setUserName(JSON_config["user"])
                self.db.setPassword(JSON_config["pass"])
                
        else:
            self.iface.messageBar().pushCritical('Advertencia:', 'El archivo de configuracion no existe, por favor configure la conexion a la base de datos.')
            self.db = QSqlDatabase.addDatabase("QPSQL", "midb")
            self.db.setHostName("")
            self.db.setPort(5432)
            self.db.setDatabaseName("")
            self.db.setUserName("")
            self.db.setPassword("")
            
        return self.db
        #self.query = QSqlQuery(self.db)

    def connect(self):
        """ Abre la conexión a la base de datos """
        ok = self.db.open()
        #return self.db.isValid()
        return ok
    
    def guardar_barra_captura(self, a, fol, usu):
        res = ""
        if self.connect():
            sql = "UPDATE barra_captura SET estatus_verificacion = 'GC', fecha_captura_geom = NOW(), capturista_geom = '"+usu+"' WHERE id_anio = '"+a+"' and id_folio = '000"+fol+"'"
            print(sql)
            if self.query.exec_(sql):
                row = self.query.record()
                res = "ok"
            else:
                print("No se pudo ejecutar la consulta")
                res = "error"    
        else:
            print('No se pudo conectar a la base de datos')
            res = "error"
        
        return res

    def get_informacion_folio_extended(self, fol, a):
        info_list = []
        if self.connect():
            sql = "SELECT ib.*, ic.* FROM \"Inco_Barra\" ib inner join \"Inco_Tramites\" ic on ib.\"IdTramite\" = ic.\"IdTramite\" where \"IdInconformidad\" = '"+fol+"' and \"AnioDeclaracion\" = '"+a+"'"
            if self.query.exec_(sql):
                row = self.query.record()
                while self.query.next():
                    propietario = self.query.value(row.indexOf("NombrePropietario"))
                    solicitante = self.query.value(row.indexOf("NombreSolicitante"))
                    dom_sol = self.query.value(row.indexOf("DomicilioSolicitante"))
                    tel_sol = self.query.value(row.indexOf("TelefonoSolicitante"))
                    fecha_captura = self.query.value(row.indexOf("FechaCapturaInconformidad"))
                    total_sup = self.query.value(row.indexOf("TotalSuperficieTerreno"))
                    total_const = self.query.value(row.indexOf("TotalSuperficieConstruccion"))
                    tramite = self.query.value(row.indexOf("IdTramite"))
                    desc_tramite = self.query.value(row.indexOf("DescripcionTramite"))
                    observaciones = self.query.value(row.indexOf("ObservacionesBarra"))
                    info = [propietario, solicitante, dom_sol, tel_sol, fecha_captura, total_sup, total_const, tramite, desc_tramite, observaciones]
                    info_list.append(info)
            else:
                print("No se pudo ejecutar la consulta")
        else:
            print('No se pudo conectar a la base de datos')
        
        return info_list

    def get_informacion_folio(self, fol, a):
        info_list = []
        if self.connect():
            sql = "select * from \"Inco_Barra\" where \"IdInconformidad\" = '"+fol+"' and \"AnioDeclaracion\" = '"+a+"'"
            if self.query.exec_(sql):
                row = self.query.record()
                while self.query.next():
                    propietario = self.query.value(row.indexOf("NombrePropietario"))
                    solicitante = self.query.value(row.indexOf("NombreSolicitante"))
                    observaciones = self.query.value(row.indexOf("ObservacionesBarra"))
                    info = [propietario, solicitante, observaciones]
                    info_list.append(info)
            else:
                print("No se pudo ejecutar la consulta")

        else:
            print('No se pudo conectar a la base de datos')
            
        return info_list

    def get_folios_para_captura(self, a, fol):
        folios_list = []
        if self.connect():
            sql = 'select * from barra_captura where estatus_verificacion = \'V\' and id_anio = \''+a+'\' and id_folio like \'%'+fol+'%\';'
            if self.query.exec_(sql):
                row = self.query.record()
                while self.query.next():
                    anio = self.query.value(row.indexOf("id_anio"))
                    folio = self.query.value(row.indexOf("id_folio"))
                    clave = self.query.value(row.indexOf("clave"))
                    tramite = self.query.value(row.indexOf("id_tramite"))
                    folios = [anio, folio, clave, tramite]
                    folios_list.append(folios)
                print('Cargado todos los folios')
            else:
                print("No se pudo ejecutar la consulta")
        else:
            print('No se pudo conectar a la base de datos')
            
        return folios_list
        
    
    def get_anios(self):
        anios = []
        if self.connect():
            sql = 'select DISTINCT("AnioDeclaracion") as nombre from "Inco_Barra" order by nombre desc'
            if self.query.exec_(sql):
                row = self.query.record()
                while self.query.next():
                    nombre = self.query.value(row.indexOf("nombre"))
                    partido = [nombre, nombre]
                    anios.append(partido)
                print('Cargado todos los anios')
            else:
                print('No se pudo ejecutar la consulta')
        else:
            print('No se pudo conectar a la base de datos')

        return anios
