# -*- coding: utf-8 -*-
import os
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QDockWidget, QMessageBox, QTableWidget,QTableWidgetItem
from qgis.PyQt.QtGui import QIntValidator, QRegExpValidator, QPixmap, QIcon
from qgis.PyQt.QtCore import (pyqtSignal, QRegExp, QSettings) 
from qgis.core import (QgsProject, QgsVectorLayer, QgsRasterLayer, QgsDataSourceUri)
import urllib.request
from urllib.error import URLError, HTTPError
import json
import base64
import hashlib
from .db import Db
from .web import Web
from .utils import Utils
from .catastrov3_info import CatastroV3Info

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'catastrov3_dockwidget_base.ui'))


class CatastroV3DockWidget(QDockWidget, FORM_CLASS):

    closingPlugin = pyqtSignal()

    def __init__(self, iface, parent=None):
        """Constructor."""
        super(CatastroV3DockWidget, self).__init__(parent)
        self.setupUi(self)

        self.iface = iface
        self.mode = self.getMode()
        self.utils = Utils(iface)
        self.web = Web(iface, self.utils)
        self.db = Db(iface, self.utils)
    
        self.canvas = self.iface.mapCanvas()
        self.file = os.path.join(os.path.dirname(__file__), 'config.json')
        self.path = "MultiPolygon?crs=EPSG:32613&index=yes"
        self.provider = "memory"
        self.username = ""

        self.baseName = ["Resultados Clave Catastral", "Resultados Clave Catastral"]
        self.qml = ['estilo_pda.qml', 'estilo_nom.qml']
        self.initSetting()        
        self.initConfig()

        self.btn_salir.setVisible(False)

        #eventos
        self.tableCaptura.doubleClicked.connect(self.ObtenerInformacionFolio)

        #self.listCaptura.itemClicked.connect(self.ObtenerInformacionFolio2)
        #self.listCaptura.itemDoubleClicked.connect(self.ObtenerInformacionFolio)
        self.settingOk.clicked.connect(self.setSettings)
        self.btn_login.clicked.connect(self.login)
        self.btn_salir.clicked.connect(self.logout)

        self.search1.clicked.connect(self.actualizar_listado)

        self.checkBoxOrtofotoIces.stateChanged.connect(self.cargar_base_ices_2019)
        self.checkBoxIcesBase.stateChanged.connect(self.cargar_mapa_base_ices)
        self.checkBoxConstrucciones.stateChanged.connect(lambda:self.cargar_capa("construccion",self.checkBoxConstrucciones))
        self.checkBoxManzanas.stateChanged.connect(lambda:self.cargar_capa("manzanas",self.checkBoxManzanas))
        self.checkBoxPredios.stateChanged.connect(lambda:self.cargar_capa("predio",self.checkBoxPredios))
        self.checkBoxCuarteles.stateChanged.connect(lambda:self.cargar_capa("cuarteles",self.checkBoxCuarteles))

        #esconder los tabs que no son del login
        self.tabMenu.removeTab(1)
    
    def actualizar_listado(self):
        #self.listCaptura.clear()
        #QMessageBox.information(self, "Info", "Actualizando")
        anio = str(self.pdo1.currentText())
        folio = self.pda.text()
        self.cargar_folios(anio, folio)
        

    def ObtenerInformacionFolio2(self, item):
        #QMessageBox.information(self, "Info", item.text())
        texto = item.text()
        a = texto[:2]
        fol = texto[5:10]
        clavecat = '007' + texto.split("-")[1].strip()     
        info = self.db.get_informacion_folio_extended(fol, a)
        self.text_info_tramite.setText("Traqmite: " +info[0][8] + "\nPropietario: " + info[0][0]+ " \nSolicitante: " + info[0][1] + " \nObservaciones: " + info[0][9])
    
    def ObtenerInformacionFolio(self, item):
        #QMessageBox.information(self, "Info", item.text())
        row = self.tableCaptura.currentRow()
        if row > -1:
            a = self.tableCaptura.item(row, 0).text()
            fol = self.tableCaptura.item(row, 1).text()[3:8]
            clavecat = '007'+self.tableCaptura.item(row, 2).text()
            print("a:"+a+" fol:"+fol+" cve:"+clavecat)
            self.sc = CatastroV3Info(self.iface,self.db, a, fol, clavecat, self.username)
            self.sc.show()
        else:
            QMessageBox.information(self, "Error", "Seleccione un folio por favor")

    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()
        

    def getMode(self):
        qset = QSettings()
        return qset.value("/catastrov3/mode", "local", type=str)

    def setMode(self, mode):
        qset = QSettings()
        qset.setValue("/catastrov3/mode", mode)
        self.mode = mode

    def createLayerDefault(self, baseName, qml):
        layer = QgsVectorLayer(self.path, baseName, self.provider)
        layer.loadNamedStyle(os.path.join(os.path.dirname(__file__), qml))
        self.utils.createFields(layer)
        return layer

    def obtenerGEometria(self, clave):
        if clave == "":
            self.iface.messageBar().pushWarning(u"Error", u"La clave no puede estar vacía")
            return None

        ok = self.utils.obtenerCapaPorAtributos(self._layer1, clave)
        if ok is True:
            return

    def cargar_anios(self):
        anios = self.db.get_anios()
        if anios:
            for i in range(0, len(anios)):
                self.pdo1.addItem(anios[i][0], anios[i][1])
        else:
            self.iface.messageBar().pushWarning('Advertencia', 'No se encontraron registros')
    
    def cargar_folios(self, a, fol):
        self.tableCaptura.setRowCount(0)
        folios = self.db.get_folios_para_captura(a, fol)
        print(folios)
        """for folio in folios:
            print(folio)
            #item = QListWidgetItem("%s" % folio['id_folio'])
            self.listCaptura.addItem(folio[0] + folio[1] + " - " + folio[2] )"""
        self.tableCaptura.setHorizontalHeaderLabels(['Año', 'Folio', 'Clave Catastral', 'Tramite'])
        if(len(folios)>0):
            numcols = len(folios[0])   # ( to get number of columns, count number of values in first row( first row is data[0]
            numrows = len(folios)   # (to get number of rows, count number of values(which are arrays) in data(2D array))
            self.tableCaptura.setColumnCount(numcols)
            self.tableCaptura.setRowCount(numrows)
            # Loops to add values into QTableWidget
            for row in range(numrows):
                for column in range(numcols):
                    # Check if value datatime, if True convert to string 
                    self.tableCaptura.setItem(row, column, QTableWidgetItem((folios[row][column])))
                    self.tableCaptura.setEditTriggers(QTableWidget.NoEditTriggers)
        else:
            QMessageBox.information(self, "Info", "No se encontraron folios para captura")



    def toggleButton(self):
        if self.rbNew.isChecked():
            self.repeat_searchs.setEnabled(False)
            self.estilo_defecto.setEnabled(True)
        else:
            self.repeat_searchs.setEnabled(True)
            self.estilo_defecto.setEnabled(False)
    
    def login(self, user="", password=""):
        user = self.login_user.text()
        password = self.login_password.text()
        host = self.host.text()
        url_api = "http://"+host+"/api/auth.php?user="+user+"&password="+password
        try:
            req = urllib.request.urlopen(url_api)
        except HTTPError as e:
            # do something print('Error code: ', e.code)
            self.iface.messageBar().pushCritical('Respuesta Login:', str(e.code))
        except URLError as e:
            # do something
            self.iface.messageBar().pushCritical('Respuesta Login:', str(e.reason))
        else:
            data = req.read()
            JSON_object = json.loads(data.decode('utf-8'))
            if JSON_object['status'] == 1:
                #self.iface.messageBar().pushSuccess('Respuesta Login:', JSON_object['message'])
                user_data = JSON_object['data'][0]
                #verificar que sea adminnistrador o tenga el rol de captura
                #if JSON_object['data'][5] in ['Administracion', 'Captura']:
                if(user_data['rol'] in ['Administracion', 'Captura']):
                    QMessageBox.information(self, "Login Respuesta", "Bienvenido  " + user_data['username'])
                    self.username = user_data['username']
                    self.tabMenu.insertTab(1, self.tabBusquedas,"Consultas Folios")
                    
                    self.cargar_anios()
                    anio = str(self.pdo1.currentText())
                    fol = self.pda.text()
                    self.cargar_folios(anio, fol)
                    self.btn_login.setEnabled(False)
                    self.login_user.setEnabled(False)
                    self.login_password.setEnabled(False)
                    self.btn_salir.setEnabled(True)
                    self.btn_salir.setVisible(True)
                else:
                    self.iface.messageBar().pushWarning('Respuesta Login:', "El usuario no tiene permisos para el modulo de captura")
            else:
                self.iface.messageBar().pushWarning('Respuesta Login:', JSON_object['message'])

    def logout(self):
        self.tabMenu.removeTab(1)
        self.btn_login.setEnabled(True)
        self.login_user.setEnabled(True)
        self.login_user.clear()
        self.login_password.setEnabled(True)
        self.login_password.clear()
        self.btn_salir.setEnabled(False)
        self.btn_salir.setVisible(False)
    
    def initConfig(self):
        if os.path.isfile(self.file):
            with open(self.file, "r") as json_file:
                JSON_config = json.load(json_file)
                self.host.setText(JSON_config["host"])
                self.port.setText(JSON_config["port"])
                self.dbname.setText(JSON_config["db"])
                self.user.setText(JSON_config["user"])
                self.pswd.setText(JSON_config["pass"])
        else:
            self.iface.messageBar().pushWarning('Advertencia:', 'El archivo de configuracion no existe, por favor configure la conexion a la base de datos.')

    def initSetting(self):
        #cargar el logo en el label
        logod = os.path.join(os.path.dirname(__file__), 'logo_catastro.png') 
        logov = os.path.join(os.path.dirname(__file__), 'logo_v256.png')
        logoc = os.path.join(os.path.dirname(__file__), 'logo_c256.png')
    
        pixmap = QPixmap()
        pixmap2 = QPixmap()
        pixmap3 = QPixmap()
        #pixmap.loadFromData(data)
        pixmap.load(logod)
        pixmap2.load(logov)
        pixmap3.load(logoc)
        self.label_logo.setPixmap(pixmap)
        self.label_logo_2.setPixmap(pixmap2)
        self.label_logo_3.setPixmap(pixmap3)

        
    def setSettings(self):

        data_set = {"host": self.host.text(), "port": self.port.text(), "db" : self.dbname.text(), "user" : self.user.text(), "pass": self.pswd.text()}

        with open(self.file, 'w') as outfile:
            json.dump(data_set, outfile)

        self.initConfig()

        if self.db.connect():
            self.iface.messageBar().pushSuccess('Respuesta Configuracion:', 'Conectado Correctamente a la Base de Datos')
        else:
            self.iface.messageBar().pushCritical('Respuesta Configuracion:', 'No se pudo conectar a la base de datos, verifique la configuracion')

    def cargar_capa(self, capa, control):
        host = self.host.text()
        if control.isChecked():
            url = "crs=EPSG:32613&dpiMode=7&format=image/png&layers="+capa+"&styles&url=http://"+host+":8082/geoserver/catastro/wms"
            
            rlayer = QgsRasterLayer(url, 'catastro:'+capa+'', 'wms')
            if rlayer.isValid():
                QgsProject.instance().addMapLayer(rlayer)
            else:
                self.iface.messageBar().pushCritical('Capas:', 'Capa no es valida')
        else:
            for layer in QgsProject.instance().mapLayers().values():
                if layer.name()=="catastro:"+capa:
                    QgsProject.instance().removeMapLayers( [layer.id()] )

    def cargar_mapa_base_ices(self):
        if self.checkBoxIcesBase.isChecked():
            url = "type=xyz&url=https://tile.openstreetmap.org/%7Bz%7D/%7Bx%7D/%7By%7D.png&zmax=19&zmin=0"
            rlayer = QgsRasterLayer(url, 'MapaBase', 'wms')
            if rlayer.isValid():
                QgsProject.instance().addMapLayer(rlayer)
            else:
                self.iface.messageBar().pushCritical('Capas:', 'Capa no es valida')
        else:
            for layer in QgsProject.instance().mapLayers().values():
                if layer.name()=="MapaBase":
                    QgsProject.instance().removeMapLayers( [layer.id()] )
            
    def cargar_base_ices_2019(self):
        if self.checkBoxOrtofotoIces.isChecked():
            
            urls = "crs=EPSG:4326&dpiMode=7&format=image/png&layers=ortofotos&styles&url=http://localhost:8081/cgi-bin/mapserv.exe?map%3DC:/visualizador/servers/mapserver/ortofotos.map%26VERSION%3D1.1.1%26LAYERS%3Dortofotos%26STYLES%3D%26SRS%3DEPSG:6368%26BBOX%3D245089,2734170,264848,2754943%26WIDTH%3D768%26HEIGHT%3D768"
            
            rlayer = QgsRasterLayer(urls, 'Culiacan2019', 'wms')
            if rlayer.isValid():
                QgsProject.instance().addMapLayer(rlayer)
            else:
                self.iface.messageBar().pushCritical('Capas:', 'Capa no es valida')

        else:
            for layer in QgsProject.instance().mapLayers().values():
                if layer.name()=="Culiacan2019":
                    QgsProject.instance().removeMapLayers( [layer.id()] )
        









    
