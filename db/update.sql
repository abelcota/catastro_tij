
-- ----------------------------
-- Table structure for Inco_Tramites
-- ----------------------------
DROP TABLE "public"."Inco_Tramites";
CREATE TABLE "public"."Inco_Tramites" (
  "IdTramite" varchar(4) COLLATE "pg_catalog"."default",
  "DescripcionTramite" varchar(80) COLLATE "pg_catalog"."default",
  "Requisito" text COLLATE "pg_catalog"."default",
  "DiasMinimoRespuesta" numeric(18,0),
  "DiasMaximoRespuesta" numeric(18,0),
  "cve_concep" float8,
  "idDeclaracion" varchar(2) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."Inco_Tramites" OWNER TO "postgres";

-- ----------------------------
-- Records of Inco_Tramites
-- ----------------------------
BEGIN;
INSERT INTO "public"."Inco_Tramites" VALUES ('16.0', 'HOMONIMOS', '- COPIA CREDENCIAL DE ELECTOR
- COPIA COMPROBANTE DE DOMICILIO
- COPIA DE ESTADO DE CUENTA DE PREDIAL', 0, 0, 73, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('17.0', 'POR CORRECCION DE REGISTRO DE CLAVES', '- COPIA DE ESCRITURA
- COPIA DE ESTADO DE CUENTA PREDIAL', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('18.0', 'INCLUIR CONSTRUCCION', '-RECIBO DE PREDIAL', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('2.1', 'POR CORRECCION DE CATEGORIA DE CONSTRUCCION', '- SOLICITUD DE DECLARACION DE PREDIOS
- 5 FOTOGRAFIAS DE INTERIORES Y EXTERIORES
- 2 COPIAS DE PLANO O CROQUIS SEÑALANDO LAS DIFERENTES CATEGORIAS.', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('2.2', 'POR CORRECCION DE SUPERFICIE DE TERRENO', '- SOLICITUD DE DECLARACION DE PREDIOS
- 2 COPIAS DE PLANO O CROQUIS ACOTADO', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('2.3', 'POR CORRECCION DE SUPERFICIE CONSTRUIDA', '- SOLICITUD DE DECLARACION DE PREDIOS
- 2 COPIAS ACOTADO DEL PLANO O CROQUIS DE LAS CONSTRUCCIONES.', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('2.4', 'POR CORRECCION DE USOS', '- LA DECLARACION DE PREDIOS', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('2.5', 'POR CORRECCION DE DEMERITOS A LAS CONSTRUCCIONES', '- DECLARACION DE PREDIOS
- 5 FOTOGRAFIAS DE INTERIORES Y EXTERIORES', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('2.6', 'POR CORRECCION DE VALORES UNITARIOS', '- DECLARACION DE PREDIOS URBANOS', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('20.0', 'PRIMER REGISTRO', '- TITULO DE PROPIEDAD
- CARTA DE POSESION
- CONTRATO DE COMPRA-VENTA', 7, 15, 67, '08');
INSERT INTO "public"."Inco_Tramites" VALUES ('22.0', 'POR DESCONTAR DEMERITO DE TERRENO', '- DECLARACION DE PREDIOS RUSTICOS
- 5 FOTOGRAFIAS DE INTERIORES Y EXTERIORES
- RECIBO DE PAGO DE SERVICIO CATASTRAL', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('23.0', 'POR DESCONTAR SUPERFICIE CONTRUIDA', '- SOLICITUD DE DECLARACION DE PREDIOS
- 2 COPIAS ACOTADO DEL PLANO O CROQUIS DE LAS CONSTRUCCIONES.', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('24.0', 'POR MEJORA A LA SUPERFICIE CONSTRUIDA', '- DECLARACION DE PREDIOS RUSTICOS
- 5 FOTOGRAFIAS DE INTERIORES Y EXTERIORES
- RECIBO DE PAGO DE SERVICIO CATASTRAL', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('3.1', 'POR CORRECCION DE CATEGORIAS DEL SUELO', '- DECLARACION DE PREDIO RUSTICO
- 4 FOTOGRAFIAS
- LEVANTAMIENTO TOPOGRAFICO (SI SON 2 Ó MAS CATEGORIAS)', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('4.0', 'DESMANCOMUNACIONES CON ESCRITURAS EXTEMPORANEAS', '- DECLARACION DE PREDIOS
- 2 COPIAS DE ESCRITURAS CON EL SELLO DEL R.P.P. LEGIBLES
- 2 COPIAS DE CROQUIS ACOTADO, SEÑALANDO LA FRACCION DEL TERRENO ORIGINAL
- RECIBO DE PAGO DE SERVICIO CATASTRAL', 5, 13, 76, '08');
INSERT INTO "public"."Inco_Tramites" VALUES ('5.0', 'DESMANCOMUNACION CON CONSTANCIA (EJIDAL, COMUNIDAD O MUN)', '- DECLARACION DE PREDIOS
-ORIGINAL Y 2 COPIAS DE LA CONSTANCIA DE LAS AUTORIDADES CORRESPONDIENTES DE FECHA RECIENTE. (SELLO) MAXIMO UN MES AL RECIBIRLO
- 2 COPIAS DE CROQUIS ACOTADO, SEÑALANDO LA FRACCION DEL TERRENO ORIGINAL
- RECIBO DE PAGO DE SERVICIO CATASTRAL', 5, 13, 76, '08');
INSERT INTO "public"."Inco_Tramites" VALUES ('6.0', 'FUSIONES CON ESCRITURAS EXTEMPORANEAS', '- DECLARACION DE PREDIOS
- 2 COPIAS DE ESCRITURAS CON SELLO DEL R.P.P. LEGIBLES
- RECIBO DE PAGO DE SERVICIO CATASTRAL', 6, 14, 75, '08');
INSERT INTO "public"."Inco_Tramites" VALUES ('8.0', 'CORRECCION DE DATOS GRALES. (NOMBRE, UBICACION, Nº OFICIAL Y DOMICILIO PARA NOT.', '- DECLARACION DE PREDIOS
- 2 COPIAS DE COMPROBANTE DE DOMICILIO CON LOS DATOS CORRECTOS LEGIBLES', 9, 17, 73, '08');
INSERT INTO "public"."Inco_Tramites" VALUES ('25.0', 'POR CORRECCION DE DEMERITO AL TERRENO', NULL, 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('21.0', 'REESTABLECER CLAVE', '- TITULO DE PROPIEDAD', 7, 15, 67, '08');
INSERT INTO "public"."Inco_Tramites" VALUES ('9.0', 'CANCELACION DE CLAVE', '- DECLARACION DE PREDIOS', 7, 15, 67, '08');
INSERT INTO "public"."Inco_Tramites" VALUES ('2.7', 'CORRECTA LOCALIZACION', '- COPIA DE ESCRITURA - COPIA RECIBO DE PREDIAL', 7, 15, 67, '03');
INSERT INTO "public"."Inco_Tramites" VALUES ('26.0', 'AVALUO FISICO POR PERITO', NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for Inco_TramitesRequisitos
-- ----------------------------
DROP TABLE "public"."Inco_TramitesRequisitos";
CREATE TABLE "public"."Inco_TramitesRequisitos" (
  "IdTramite" varchar(4) COLLATE "pg_catalog"."default",
  "IdRequisito" varchar(2) COLLATE "pg_catalog"."default",
  "DescripcionRequisito" varchar(255) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."Inco_TramitesRequisitos" OWNER TO "postgres";

-- ----------------------------
-- Records of Inco_TramitesRequisitos
-- ----------------------------
BEGIN;
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('1.0', '1', 'RECIBO DE PAGO DE SERVICIO CATASTRAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('1.0', '2', 'RECIBO DE PAGO DE RECAUDACION DE RENTAS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.1', '1', 'SOLICITUD DE DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.1', '2', '5 FOTOGRAFIAS DE INTERIORES Y EXTERIORES');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.1', '3', '2 COPIAS DE PLANO O CROQUIS SEÑALANDO LAS DIFERENTES CATEGORIAS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.2', '1', 'SOLICITUD DE DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.2', '2', '2 COPIAS DE PLANO O CROQUIS ACOTADO');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.3', '1', 'SOLICITUD DE DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.3', '2', '2 COPIAS ACOTADO DEL PLANO O CROQUIS DE LAS CONSTRUCCIONES');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.4', '1', 'SOLICITUD DE DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.5', '1', 'SOLICITUD DE DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.5', '2', '5 FOTOGRAFIAS DE INTERIORES Y EXTERIORES');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.6', '1', 'SOLICITUD DE DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.1', '1', 'DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.1', '2', '4 FOTOGRAFIAS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.1', '3', 'LEVANTAMIENTO TOPOGRAFICO (SI SON 2 O MAS CATEGORIAS)');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.2', '1', 'DECLARACION DE PREDIOS ');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.2', '2', 'LEVANTAMIENTO TOPOGRAFICO CON CUADRO DE MEDIDAS Y COLINDANCIAS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.3', '1', 'DELEGACION DE PREDIOS ');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.3', '2', '2 COPIAS DE CROQUIS O PLANO DE LA SUPERFICIE CONSTRUIDA');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.4', '1', 'DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.4', '2', '5 FOTOGRAFIAS DE INTERIORES Y EXTERIORES');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.4', '3', '2 COPIAS DE PLANOS O CROQUIS SEÑALANDO LAS DIFERENTES CATEGORIAS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.5', '1', 'DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.5', '2', '5 FOTOGRAFIAS DE INTERIORES Y EXTERIORES');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('3.5', '3', 'RECIBO DE PAGO DE SERVICIO CATASTRAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('4.0', '1', 'DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('4.0', '2', '2 COPIAS DE ESCRITURAS CON EL SELLO DEL R.P.P. LEGIBLES');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('4.0', '3', '2 COPIAS DE CROQUIS ACOTADO, SEÑALANDO LA FRACCION DEL TERRENO ORIGINAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('4.0', '4', 'RECIBO DE PAGO DE SERVICIO CATASTRAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('5.0', '1', 'DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('5.0', '2', 'ORIGINAL Y 2 COPIAS DE LA CONSTANCIA DE FECHA RECIENTE (SELLO) MAX. UN MES AL RECIBIRLO');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('5.0', '3', '2 COPIAS DE CROQUIS ACOTADO, SEÑALANDO LA FRACCION DEL TERRENO ORIGINAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('5.0', '4', 'RECIBO DE PAGO DE SERVICIO CATASTRAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('6.0', '1', 'DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('6.0', '2', '2 COPIAS DE ESCRITURAS CON SELLO DEL R.P.P. LEGIBLES');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('6.0', '3', 'RECIBO DE PAGO DE SERVICIO CATASTRAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('7.0', '1', 'DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('7.0', '2', 'ORIGINAL Y 2 COPIAS DE LA CONSTANCIA DE FECHA RECIENTE (SELLO) MAX. UN MES AL RECIBIRLO');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('7.0', '3', 'RECIBO DE PAGO DE SERVICIO CATASTRAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('8.0', '1', 'DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('8.0', '2', '2 COPIAS DE COMPROBANTE DE DOMICILIO CON LOS DATOS CORRECTOS LEGIBLES');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('9.0', '1', 'DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.7', '1', '- COPIA DE ESCRITURA');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('10.0', '1', '2 COPIAS DE RECIBO DE PAGO DE PREDIAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('10.0', '2', '2 COPIAS DEL REQUERIMIENTO SI FUE REQUERIDA LA PERSONA');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('11.0', '1', 'RECIBO DE PAGO DE SERVICIO CATASTRAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('11.0', '2', 'ORIGINAL Y COPIA DEL ACTA DE NACIMIENTO (CUANDO SEAN HOMONIMOS)');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('11.0', '3', 'COPIA DE IDENTIFICACION');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('12.0', '1', 'RECIBO DE PAGO DE SERVICIO CATASTRAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('12.0', '2', 'CLAVE CATASTRAL Y/O NOMBRE');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('13.0', '1', 'RECIBO DE PAGO DE SERVICIO CATASTRAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('13.0', '2', 'CLAVE CATASTRAL Y/O NOMBRE');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('14.0', '1', 'INFORMACION CATASTRAL INCOMPLEMENTADA');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('14.0', '2', 'COPIA DE MINUTA O ESCRITURA O DOCUMENTO DE PROPIETARIO');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('14.0', '3', 'COPIA DE AVALUO COMERCIAL SI LO REQUIERE (DEBERA CONTENER FOTOGRAFIAS)');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('14.0', '4', 'CROQUIS DE UBICACION. CROQUIS DE LEVANTAMIENTO ACOTADO DE TERR Y CONTR');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('15.0', '1', 'ESCRITURA DE ADQUIS. DE TERR Y SI ES REGIMEN EN CONDOMINIO ESCRITURA Y PLANO');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('15.0', '2', 'OFICIO DE VALOR EN VENTA POR M2 DE TERR Y SERVS PUBLS CON QUE CONTARA EL FRACC');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('15.0', '3', 'OFICIO DE APROBACION DE CABILDO (AYTO)');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('15.0', '4', 'FIANZA DE TERMINACION DE URBANIZACION');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('15.0', '5', 'PLANO GRAL DE LOTIFICACION, AUTORIZADO POR AYTO');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('15.0', '6', 'PLANOS DE C/MANZANA ESCALA 1:400');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('15.0', '7', 'PLANO GRAL. DEL FRACCIONAMIENTO ESCALA 1:2000');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('15.0', '8', 'PLANO GRAL. DEL FRACCIONAMIENTO ESCALA 1:10000 PARA MOCHIS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('15.0', '9', 'NUMERO OFICIAL DE CADA LOTE');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('15.0', '10', 'RECIBO DE PAGO DE ACTUALIZACION POR LOS SERVICIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('16.0', '1', 'COPIA CREDENCIAL DE ELECTOR');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('16.0', '2', 'COPIA COMPROBANTE DE DOMICILIO');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('2.7', '2', '- COPIA RECIBO DE PREDIAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('16.0', '3', 'COPIA DE ESTADO DE CUENTA DE PREDIAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('17.0', '1', 'COPIA DE ESCRITURA');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('17.0', '2', 'COPIA DE ESTADO DE CUENTA DE PREDIAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('18.0', '1', 'RECIBO DE PREDIAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('20.0', '1', 'TITULO DE PROPIEDAD
');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('21.0', '1', 'TITULO DE PROPIEDAD');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('21.0', '2', 'COPIA DE ESCRITURA');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('22.0', '1', 'DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('22.0', '2', '5 FOTOGRAFIAS DE INTERIORES Y EXTERIORES');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('22.0', '3', 'RECIBO DE PAGO DE SERVICIO CATASTRAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('23.0', '1', 'SOLICITUD DE DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('23.0', '2', '2 COPIAS ACOTADO DEL PLANO O CROQUIS DE LAS CONSTRUCCIONES');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('24.0', '1', 'DECLARACION DE PREDIOS');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('24.0', '2', '5 FOTOGRAFIAS DE INTERIORES Y EXTERIORES');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('24.0', '3', 'RECIBO DE PAGO DE SERVICIO CATASTRAL');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('20.0', '2', 'CARTA DE POSESION');
INSERT INTO "public"."Inco_TramitesRequisitos" VALUES ('20.0', '3', 'CONTRATO DE COMPRA-VENTA');
COMMIT;

-- ----------------------------
-- Table structure for Barra_Seguimiento
-- ----------------------------
DROP TABLE "public"."Barra_Seguimiento";
CREATE TABLE "public"."Barra_Seguimiento" (
  "Id_FolioBarra" varchar(8) COLLATE "pg_catalog"."default",
  "F_Captura" timestamp(6),
  "IdDeclaracion" varchar(2) COLLATE "pg_catalog"."default",
  "F_EnvioInspeccion" timestamp(6),
  "F_InspeccionCampo" timestamp(6),
  "FechaLimiteEnInspeccion" timestamp(6),
  "F_EnvioGraficacion" timestamp(6),
  "F_AsignadoGraficador" timestamp(6),
  "F_AtendidoGraficacion" timestamp(6),
  "FechaLimiteEnGraficacion" timestamp(6),
  "F_EnvioIces" timestamp(6),
  "NumeroOficioEnvioIces" varchar(20) COLLATE "pg_catalog"."default",
  "FechaLimiteEnIces" timestamp(6),
  "F_AvaluoManual" timestamp(6),
  "F_EntregaContribuyente" timestamp(6),
  "Id_EstadoActual" varchar(2) COLLATE "pg_catalog"."default",
  "Id_QuienLoTiene" varchar(2) COLLATE "pg_catalog"."default",
  "Se_Urgente" varchar(1) COLLATE "pg_catalog"."default",
  "No_VecesDevuelto" int4,
  "Estado" varchar(20) COLLATE "pg_catalog"."default",
  "Id_Anio" varchar(2) COLLATE "pg_catalog"."default",
  "Nombre_Propietario" varchar(250) COLLATE "pg_catalog"."default",
  "Pobl" varchar(3) COLLATE "pg_catalog"."default",
  "Ctel" varchar(3) COLLATE "pg_catalog"."default",
  "Manz" varchar(3) COLLATE "pg_catalog"."default",
  "Pred" varchar(3) COLLATE "pg_catalog"."default",
  "Unid" varchar(3) COLLATE "pg_catalog"."default",
  "Observaciones" varchar(250) COLLATE "pg_catalog"."default",
  "ProcedioSN" varchar(1) COLLATE "pg_catalog"."default",
  "clave" varchar(15) COLLATE "pg_catalog"."default",
  "Val_Terr" float8,
  "Val_Const" float8,
  "ValCatInicio" float8,
  "PredialInicio" float8,
  "ValCatDespues" float8,
  "PredialDespues" float8,
  "TramiteNotario" varchar(3) COLLATE "pg_catalog"."default",
  "Fol_Mov" float8,
  "TramiteIces" varchar(5) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."Barra_Seguimiento" OWNER TO "postgres";



-- ----------------------------
-- Table structure for Catalogo_EstadoActual
-- ----------------------------
DROP TABLE "public"."Catalogo_EstadoActual";
CREATE TABLE "public"."Catalogo_EstadoActual" (
  "Id_EstadoActual" varchar(2) COLLATE "pg_catalog"."default",
  "Descripcion" varchar(50) COLLATE "pg_catalog"."default",
  "Estado" varchar(50) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."Catalogo_EstadoActual" OWNER TO "postgres";

-- ----------------------------
-- Records of Catalogo_EstadoActual
-- ----------------------------
BEGIN;
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('TC', 'Trámite en Curso', 'Trámite en Curso');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('MR', 'Mov. Realizado', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('NP', 'No Procede', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('CA', 'Cancelado', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('FD', 'Faltan Datos', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('NL', 'No hay lámina', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('NE', 'Predio no encontrado', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('NC', 'Contrib. No Cooperó', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('CO', 'Concluido Normalmente', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('RE', 'Regresado', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('GR', 'Graficado', 'Trámite en Curso');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('FC', 'Falta Croquis', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('EI', 'Enviado Inspección', 'Trámite en Curso');
INSERT INTO "public"."Catalogo_EstadoActual" VALUES ('RI', 'Regresado por Ices', 'Trámite Detenido');
COMMIT;

-- ----------------------------
-- Table structure for Catalogo_MovimientoGraficacion
-- ----------------------------
DROP TABLE "public"."Catalogo_MovimientoGraficacion";
CREATE TABLE "public"."Catalogo_MovimientoGraficacion" (
  "Id_MovimientoGraficacion" varchar(2) COLLATE "pg_catalog"."default",
  "Descripcion" varchar(50) COLLATE "pg_catalog"."default",
  "Grupo" varchar(1) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."Catalogo_MovimientoGraficacion" OWNER TO "postgres";

-- ----------------------------
-- Records of Catalogo_MovimientoGraficacion
-- ----------------------------
BEGIN;
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('A', 'Construcciones Nuevas', '1');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('AP', 'Construcciones Nuevas', '1');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('B', 'Construcciones Modificadas(Ampliacion)', '2');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('C', 'Correccion de Sup. Const. (Abajo)', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('D', 'Correccion  de Categoria (Abajo)', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('E', 'Mejora a la Construccion', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('F', 'Construcciones en Ruinas', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('G', 'Construcciones Demolidas', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('H', 'Correccion de Demerito', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('I', 'Cambio de Uso', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('J', 'Correc. Ubicacion de Const.', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('K', 'Terreno Incorpo.', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('L', 'Terreno Modificado', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('M', 'Desmancomunaciones', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('N', 'Numero Oficial', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('O', 'Fusion', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('R', 'Correc. Ubicacion de Clave', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('U', 'Nuevo Registro', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('V', 'Otros', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('W', 'Asignacion clave Corett', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('P', 'Croquis', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('S', 'Cambio de Inscripcion', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('Q', 'Proyectos', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('T', 'Const. Inexistente', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('X', 'Fraccionamiento', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('BP', 'Construcciones Modificadas(Ampliacion)', '2');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('CP', 'Correccion de Sup. Const. (Abajo)', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('DP', 'Correccion  de Categoria (Abajo)', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('EP', 'Mejora a la Construccion', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('FP', 'Construcciones en Ruinas', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('GP', 'Construcciones Demolidas', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('HP', 'Correccion de Demerito', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('IP', 'Cambio de Uso', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('JP', 'Correc. Ubicacion de Const.', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('KP', 'Terreno Incorpo.', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('LP', 'Terreno Modificado', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('MP', 'Desmancomunaciones', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('NP', 'Numero Oficial', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('TP', 'Const. Inexistente', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('QP', 'Proyectos', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('SP', 'Cambio de Inscripcion', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('PP', 'Croquis', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('XP', 'Fraccionamiento', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('WP', 'Asignacion clave Corett', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('VP', 'Otros', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('UP', 'Nuevo Registro', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('RP', 'Correc. Ubicacion de Clave', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('OP', 'Fusion', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('01', 'Inconformidades', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('02', 'Altas', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('03', 'Desman. Sin Control', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('04', 'Desman. Con Control', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('05', 'Por incluir Construcción', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('06', 'Fusiones', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('07', 'Terreno modificado c/control', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('08', 'Fusión con Control', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('09', 'Terreno Modificado', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('31', 'Desman. C/Tras. Dom. S/Ctrl.', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('32', 'Desman. S/Tras. Dom. S/Ctrl.', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('61', 'Fusión Total S/Tras. Dom.', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('62', 'Fusión Total C/Tras. Dom.', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('63', 'Fusión Parcial S/Tras. Dom.', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('64', 'Fusión Parcial C/Tras. Dom.', '4');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('AI', 'Construcciones Nuevas', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('BI', 'Construcciones Modificadas (Ampliacion)', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('CI', 'Correccion de Sup. Const. (Abajo)', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('DI', 'Corrección de Categoria (Abajo)', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('EI', 'Mejora a la Construccion', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('FI', 'Construcciones en Ruinas', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('GI', 'Construcciones Demolidas', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('HI', 'Correcion de Demerito', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('II', 'Cambio de Uso', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('JI', 'Correc. Ubicacion de Const.', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('KI', 'Terreno Incorpo.', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('LI', 'Terreno Modificado', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('MI', 'Desmancomunaciones', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('NI', 'Numero Oficial', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('OI', 'Fusion', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('PI', 'Croquis', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('QI', 'Proyectos', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('RI', 'Correc. Ubicacion de Clave', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('SI', 'Cambio de Inscripcion', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('TI', 'Const. Inexistente', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('UI', 'Nuevo Resgistro', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('VI', 'Otros', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('XI', 'Fraccionamiento', NULL);
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('Y', 'Licencias-Construcciones Nuevas', '3');
INSERT INTO "public"."Catalogo_MovimientoGraficacion" VALUES ('Z', 'Licencias-Modificaciones/Ampliaciones', '3');
COMMIT;

-- ----------------------------
-- Table structure for Catalogo_MovimientoInspeccion
-- ----------------------------
DROP TABLE "public"."Catalogo_MovimientoInspeccion";
CREATE TABLE "public"."Catalogo_MovimientoInspeccion" (
  "Id_MovimientoInspeccion" varchar(2) COLLATE "pg_catalog"."default",
  "Descripcion" varchar(50) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."Catalogo_MovimientoInspeccion" OWNER TO "postgres";

-- ----------------------------
-- Records of Catalogo_MovimientoInspeccion
-- ----------------------------
BEGIN;
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('10', 'Terreno Incorporado');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('11', 'Terreno Modificado');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('12', 'Se Tomó Renta');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('13', 'Se Tomó Numero Oficial');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('14', 'Otros');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('15', 'Primer Registro');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('16', 'Fusión');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('17', 'Nueva Planificación');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('18', 'Rest. x estar canc. erroneamente');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('19', 'Por correcta localización');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('20', 'Por descontar Sup. Terr.');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('21', 'X Desc. Sup. C. Inexistente');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('22', 'Error en Cálculo');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('51', 'Incluir Construcción');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('52', 'Corrección de Superficie de Terreno');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('53', 'Descontar Superficie Construida');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('54', 'Corrección de Categoría');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('55', 'Corrección de Usos');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('56', 'Corrección de Valores Unitarios');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('57', 'Corrección de Superficie Construida');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('58', 'Homónimos');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('59', 'No procedió');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('60', 'Desmancomunación');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('61', 'Corrección de demerito');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('01', 'Construcciones Nuevas');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('02', 'Construcciones Modificadas');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('03', 'Corrección de Categoría (Abajo)');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('04', 'Mejoras a la Construcción');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('05', 'Construcción en Ruinas');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('06', 'Construcción Demolida');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('07', 'Corrección de Demérito');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('08', 'Cambios de Uso');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('09', 'Correcta Ubicación de Construcción');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('00', 'Sin Movimientos');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('23', 'EPC (Desarrollo Urbano)');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('24', 'Licencias-Construcciones Nuevas');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('25', 'Licencias-Modificaciones/Ampliaciones');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('26', 'Licencias-Descontar/Incluir Nueva');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('95', 'Información Correcta');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('96', 'Construcción Modificada');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('97', 'Corrección de Categoría');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('98', 'Construcción Demolida');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('99', 'Corrección de Uso');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('94', 'Resuelta por Ices');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('93', 'Se encontró solo');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('92', 'No dejó medir');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('91', 'Casa deshabitada');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('81', 'Construcciones Nuevas (VUELO)');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('82', 'Lona');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('83', 'Malla Sombra');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('84', 'Construcción Demolida');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('85', 'Construcción Inexistente');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('86', 'Proceso de Construcción');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('27', 'Cancelación de Clave');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('28', 'Por Corrección de Registro de Clave');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('29', 'Reestablecer Clave');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('87', 'Baldío');
INSERT INTO "public"."Catalogo_MovimientoInspeccion" VALUES ('62', 'CORRECTA LOCALIZACION');
COMMIT;

-- ----------------------------
-- Table structure for Catalogo_QuienLoTiene
-- ----------------------------
DROP TABLE "public"."Catalogo_QuienLoTiene";
CREATE TABLE "public"."Catalogo_QuienLoTiene" (
  "Id_QuienLoTiene" varchar(2) COLLATE "pg_catalog"."default",
  "Descripcion" varchar(50) COLLATE "pg_catalog"."default",
  "Estado" varchar(50) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."Catalogo_QuienLoTiene" OWNER TO "postgres";

-- ----------------------------
-- Records of Catalogo_QuienLoTiene
-- ----------------------------
BEGIN;
INSERT INTO "public"."Catalogo_QuienLoTiene" VALUES ('DA', 'Depto. Administrativo', 'Trámite en Curso');
INSERT INTO "public"."Catalogo_QuienLoTiene" VALUES ('DT', 'Depto. Trámites', 'Trámite en Curso');
INSERT INTO "public"."Catalogo_QuienLoTiene" VALUES ('DG', 'Depto. Graficación', 'Trámite en Curso');
INSERT INTO "public"."Catalogo_QuienLoTiene" VALUES ('DI', 'Depto. Inspección', 'Trámite en Curso');
INSERT INTO "public"."Catalogo_QuienLoTiene" VALUES ('CO', 'Concluido Normalmente', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_QuienLoTiene" VALUES ('CN', 'Concluido No procedió', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_QuienLoTiene" VALUES ('CH', 'Concluido por Homónimo', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_QuienLoTiene" VALUES ('CM', 'Concluido Movimiento Realizado', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_QuienLoTiene" VALUES ('DC', 'Depto. Captura', 'Trámite en Curso');
INSERT INTO "public"."Catalogo_QuienLoTiene" VALUES ('EI', 'Enviado al Verificacion', 'Trámite en Curso');
COMMIT;

-- ----------------------------
-- Table structure for Catalogo_ResultadoGraficacion
-- ----------------------------
DROP TABLE "public"."Catalogo_ResultadoGraficacion";
CREATE TABLE "public"."Catalogo_ResultadoGraficacion" (
  "Id_ResultadoGraficacion" varchar(2) COLLATE "pg_catalog"."default",
  "Descripcion" varchar(50) COLLATE "pg_catalog"."default",
  "Estado" varchar(50) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."Catalogo_ResultadoGraficacion" OWNER TO "postgres";

-- ----------------------------
-- Records of Catalogo_ResultadoGraficacion
-- ----------------------------
BEGIN;
INSERT INTO "public"."Catalogo_ResultadoGraficacion" VALUES ('MR', 'Mov. Realizado', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_ResultadoGraficacion" VALUES ('NP', 'No Procede', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_ResultadoGraficacion" VALUES ('FD', 'Faltan Datos', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_ResultadoGraficacion" VALUES ('NL', 'No hay lámina', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_ResultadoGraficacion" VALUES ('GR', 'Graficado', 'Trámite en Curso');
INSERT INTO "public"."Catalogo_ResultadoGraficacion" VALUES ('FC', 'Falta Croquis', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_ResultadoGraficacion" VALUES ('RE', 'Regresado', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_ResultadoGraficacion" VALUES ('EI', 'Enviado Inspección', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_ResultadoGraficacion" VALUES ('NE', 'No Expediente', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_ResultadoGraficacion" VALUES ('NC', 'No Coopero', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_ResultadoGraficacion" VALUES ('RI', 'Regresado por Validacion', 'Trámite Detenido');
COMMIT;

-- ----------------------------
-- Table structure for Catalogo_ResultadoInspeccion
-- ----------------------------
DROP TABLE "public"."Catalogo_ResultadoInspeccion";
CREATE TABLE "public"."Catalogo_ResultadoInspeccion" (
  "Id_ResultadoInspeccion" varchar(2) COLLATE "pg_catalog"."default",
  "Descripcion" varchar(50) COLLATE "pg_catalog"."default",
  "Estado" varchar(50) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."Catalogo_ResultadoInspeccion" OWNER TO "postgres";

-- ----------------------------
-- Records of Catalogo_ResultadoInspeccion
-- ----------------------------
BEGIN;
INSERT INTO "public"."Catalogo_ResultadoInspeccion" VALUES ('IN', 'Inspeccionado', 'Trámite en Curso');
INSERT INTO "public"."Catalogo_ResultadoInspeccion" VALUES ('NP', 'No Procede', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_ResultadoInspeccion" VALUES ('CA', 'Cancelado', 'Trámite Concluido');
INSERT INTO "public"."Catalogo_ResultadoInspeccion" VALUES ('FD', 'Faltan Datos', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_ResultadoInspeccion" VALUES ('NL', 'No hay lámina', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_ResultadoInspeccion" VALUES ('NE', 'Predio no encontrado', 'Trámite Detenido');
INSERT INTO "public"."Catalogo_ResultadoInspeccion" VALUES ('NC', 'Contrib. No Cooperó', 'Trámite Detenido');
COMMIT;



-- ----------------------------
-- Table structure for Inco_Barra
-- ----------------------------
DROP TABLE "public"."Inco_Barra";
CREATE TABLE "public"."Inco_Barra" (
  "IdInconformidad" varchar(5) COLLATE "pg_catalog"."default",
  "IdTramite" varchar(4) COLLATE "pg_catalog"."default",
  "Poblacion" int4,
  "Cuartel" varchar(32) COLLATE "pg_catalog"."default",
  "Manzana" int4,
  "Predio" int4,
  "Unidad" int4,
  "Clave" varchar(15) COLLATE "pg_catalog"."default",
  "NombrePropietario" varchar(255) COLLATE "pg_catalog"."default",
  "NombreSolicitante" varchar(255) COLLATE "pg_catalog"."default",
  "DomicilioSolicitante" varchar(255) COLLATE "pg_catalog"."default",
  "IdCalleSolicitante" float4,
  "NumeroOficialSolicitante" varchar(15) COLLATE "pg_catalog"."default",
  "OrientacionSolicitante" varchar(15) COLLATE "pg_catalog"."default",
  "IdColoniaSolicitante" float4,
  "CodigoPostalSolicitante" varchar(15) COLLATE "pg_catalog"."default",
  "TelefonoSolicitante" varchar(30) COLLATE "pg_catalog"."default",
  "IdPropietario" float8,
  "IdColonia" float8,
  "IdCalle" float8,
  "NumeroOficial" varchar(15) COLLATE "pg_catalog"."default",
  "ValorCatastral" float4,
  "IdUso" varchar(4) COLLATE "pg_catalog"."default",
  "ObservacionesBarra" varchar(200) COLLATE "pg_catalog"."default",
  "IdDeclaracion" varchar(2) COLLATE "pg_catalog"."default",
  "IdEmpleadoBarra" varchar(4) COLLATE "pg_catalog"."default",
  "FechaCapturaInconformidad" timestamp(6),
  "OtrosDocumentos" varchar(100) COLLATE "pg_catalog"."default",
  "TotalSuperficieTerreno" varchar(53) COLLATE "pg_catalog"."default",
  "TotalSuperficieConstruccion" varchar(53) COLLATE "pg_catalog"."default",
  "IdDocumento1" int4,
  "IdDocumento2" int4,
  "IdDocumento3" int4,
  "IdDocumento4" int4,
  "IdDocumento5" int4,
  "IdDocumento6" int4,
  "Fol_Mov" float8,
  "Fec_Ava" timestamp(6),
  "cve_concep" float8,
  "observa01" varchar(255) COLLATE "pg_catalog"."default",
  "fec_Avaluo" float8,
  "AnioDeclaracion" varchar(2) COLLATE "pg_catalog"."default",
  "TramiteNotario" varchar(3) COLLATE "pg_catalog"."default",
  "FechaEntregaTramite" timestamp(6),
  "TramiteICES" varchar(5) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."Inco_Barra" OWNER TO "postgres";



-- ----------------------------
-- Table structure for Gen_Empleados
-- ----------------------------
DROP TABLE "public"."Gen_Empleados";
CREATE TABLE "public"."Gen_Empleados" (
  "IdEmpleado" varchar(4) COLLATE "pg_catalog"."default",
  "Login" varchar(15) COLLATE "pg_catalog"."default",
  "PassWord" varchar(15) COLLATE "pg_catalog"."default",
  "Nombre" varchar(50) COLLATE "pg_catalog"."default",
  "ApellidoPaterno" varchar(50) COLLATE "pg_catalog"."default",
  "ApellidoMaterno" varchar(50) COLLATE "pg_catalog"."default",
  "IdPuesto" varchar(2) COLLATE "pg_catalog"."default",
  "IdNivel" varchar(2) COLLATE "pg_catalog"."default"
)
;
ALTER TABLE "public"."Gen_Empleados" OWNER TO "postgres";

-- ----------------------------
-- Records of Gen_Empleados
-- ----------------------------
BEGIN;
INSERT INTO "public"."Gen_Empleados" VALUES ('0149', 'SAMANTA', '»§µ®¨¦¸', 'EMPLEADO', 'PRUEBA', 'CATASTRO', '05', '08');
COMMIT;

